﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct GenericVirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericVirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericVirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericVirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericVirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericVirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct InterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct InterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct InterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct InterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R>
struct GenericInterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct GenericInterfaceFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct GenericInterfaceFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct GenericInterfaceFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct GenericInterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct GenericInterfaceActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// Splitter
struct Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.Action`3<System.Object,System.Object,System.Object>
struct Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Int32>[]
struct EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Int32>
struct KeyCollection_t666396E67E50284D48938851873CE562083D67F2;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Int32>
struct ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Collections.Generic.List`1<vp_GlobalCallbackReturn`1<System.Object>>
struct List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718;
// System.Collections.Generic.List`1<vp_GlobalCallbackReturn`2<System.Object,System.Object>>
struct List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091;
// System.Collections.Generic.List`1<vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>>
struct List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735;
// System.Collections.Generic.List`1<vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>>
struct List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0;
// System.Collections.Generic.List`1<vp_GlobalCallback`1<System.Object>>
struct List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3;
// System.Collections.Generic.List`1<vp_GlobalCallback`2<System.Object,System.Object>>
struct List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E;
// System.Collections.Generic.List`1<vp_GlobalCallback`3<System.Object,System.Object,System.Object>>
struct List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>
struct Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>
struct Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.Hashtable/bucket[]
struct bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A;
// System.Collections.ICollection
struct ICollection_tA3BAB2482E28132A7CA9E0E21393027353C28B54;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`1<System.Object>
struct Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032;
// System.Func`4<System.Object,System.Object,System.Object,System.Object>
struct Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>
struct UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>
struct UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0;
// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>
struct ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39;
// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;
// ValuesSplitter`1<System.Int32>
struct ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895;
// ValuesSplitter`1<System.Object>
struct ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134;
// ValuesSplitter`1<UnityEngine.Color>
struct ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21;
// vp_Activity
struct vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94;
// vp_Activity/Callback
struct Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D;
// vp_Activity/Condition
struct Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A;
// vp_Activity`1<System.Object>
struct vp_Activity_1_t19E97BA506EE5DCBB057302B947C3A9485DB47F0;
// vp_Attempt
struct vp_Attempt_t28537319AB5D1247932A58A0DFE65B8353FEA615;
// vp_Attempt/Tryer
struct Tryer_tCAC3063BFA7408FF3256C77068D8881299B4DD7E;
// vp_Attempt`1/Tryer`1<System.Object,System.Object>
struct Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF;
// vp_Attempt`1<System.Object>
struct vp_Attempt_1_t5F55710F74CD633CA4E9717FA9613A936464E68F;
// vp_Event
struct vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A;
// vp_GlobalCallbackReturn`1<System.Object>
struct vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED;
// vp_GlobalCallbackReturn`1<System.Object>[]
struct vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6;
// vp_GlobalCallbackReturn`2<System.Object,System.Object>
struct vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C;
// vp_GlobalCallbackReturn`2<System.Object,System.Object>[]
struct vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6;
// vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>
struct vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F;
// vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>[]
struct vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA;
// vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>
struct vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8;
// vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>[]
struct vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767;
// vp_GlobalCallback`1<System.Object>
struct vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159;
// vp_GlobalCallback`1<System.Object>[]
struct vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8;
// vp_GlobalCallback`2<System.Object,System.Object>
struct vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8;
// vp_GlobalCallback`2<System.Object,System.Object>[]
struct vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051;
// vp_GlobalCallback`3<System.Object,System.Object,System.Object>
struct vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD;
// vp_GlobalCallback`3<System.Object,System.Object,System.Object>[]
struct vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644;
// vp_GlobalEventInternal/SendException
struct SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A;
// vp_GlobalEventInternal/UnregisterException
struct UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA;
// vp_Message
struct vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989;
// vp_Message/Sender
struct Sender_t71699D3956D8A1B096FAD0228565F659FEDDF888;
// vp_Message`1/Sender`1<System.Object,System.Object>
struct Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59;
// vp_Message`1<System.Object>
struct vp_Message_1_t07E944A5FFD733133B2B1970B4A9AC638975BE3A;
// vp_Message`2/Sender`2<System.Object,System.Object,System.Object,System.Object>
struct Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80;
// vp_Message`2<System.Object,System.Object>
struct vp_Message_2_t30D2E82656A74C71DF80691139A062CF3308E8EE;
// vp_Timer/Handle
struct Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381;
// vp_Value`1/Getter`1<System.Object,System.Object>
struct Getter_1_t937722DADE2AAED1621E908646316A6E193A266B;
// vp_Value`1/Setter`1<System.Object,System.Object>
struct Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255;
// vp_Value`1<System.Object>
struct vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479;

IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Delegate_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral04231B44477132B3DBEFE7768A921AE5A13A00FC;
IL2CPP_EXTERN_C String_t* _stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9;
IL2CPP_EXTERN_C String_t* _stringLiteral2FC7D48A5356042700B80D3BAEE0C5E913825A7D;
IL2CPP_EXTERN_C String_t* _stringLiteral448AB73BA1C21E671E218FB91F2644C834F0C16F;
IL2CPP_EXTERN_C String_t* _stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787;
IL2CPP_EXTERN_C String_t* _stringLiteral5615DEB602F940728B39CAED37FECE24A8E5E21C;
IL2CPP_EXTERN_C String_t* _stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C;
IL2CPP_EXTERN_C String_t* _stringLiteral72677028B4D0D41AF475041FDBE030F7C7146D2C;
IL2CPP_EXTERN_C String_t* _stringLiteral85FC3B2ACC52E958D0512287C2D44629E7D2CBAF;
IL2CPP_EXTERN_C String_t* _stringLiteral91A7B1383D7D83FD757F5147D796C39CED8E3E3B;
IL2CPP_EXTERN_C String_t* _stringLiteral9BC2575C3930437E80555F78757B783C842E8E66;
IL2CPP_EXTERN_C String_t* _stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9;
IL2CPP_EXTERN_C String_t* _stringLiteralACDB94344573594F5AE5956EF0E2477E3C8C2B07;
IL2CPP_EXTERN_C String_t* _stringLiteralB38FACF4A8F9F6E8FC7192D1CBC326386647F4C1;
IL2CPP_EXTERN_C String_t* _stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830;
IL2CPP_EXTERN_C String_t* _stringLiteralBFFFD736CDDD08A4EEE689949C3399CB61DA773B;
IL2CPP_EXTERN_C String_t* _stringLiteralD419E9940160787BF27E633820F14FCF86FF6221;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE044DB5CACC7C1E1DED3C45FA7472331FE5E6246;
IL2CPP_EXTERN_C String_t* _stringLiteralE075B7F92BF500D83412F32E8E2BB56C1176DA27;
IL2CPP_EXTERN_C String_t* _stringLiteralFEA43415268E389C0DE3D578A5E51EAF3900BB19;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_1_Register_m9267D1714DF192E02EEF7CFC667CA9CD1629007A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_1_Send_m65932A48D6E7714790169F961B61359F11487A0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_1_Unregister_m367761F3D9A8BEDAF895CC87178965D5A3D5787B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_2_Register_m65F5A02295B8DAB588E6AD98D0CD0ED1CF9D9979_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_2_Send_mA8EA70105B38F57E1AEE089ACEA6ADA5FEE4F718_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_2_Unregister_mFFC1B7889EA18F8910084CEFA3FA7D928A576786_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_3_Register_m623AEF48AC44C458224D8B5BC938B2B78D427652_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_3_Send_m53BF34DD0041FAD79B5E877AC5D3992446787EB5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_3_Unregister_mA7371437A5820946B3B160009A4CA66B8A831D96_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_4_Register_mCABDC7AF7ADC056A07F0BFF4583EDA80FA020487_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEventReturn_4_Unregister_m5834FBF25CBBAA2D9673C24D873D100CCEE3456D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_1_Register_m0C35CD0E2D37D4538B19E15618CE12ADA4A38169_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_1_Send_m0E8BD5CE11E352A4C0D2A5FEE5F00581654796BE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_1_Unregister_m1201A989E77B0ABA197E41B3AC02409707D26587_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_2_Register_mF7331093FE8FB0056E02990FE3D49F16E347B0B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_2_Send_m5B777A199A27FAF9D55E45AAA843200FB804A6EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_2_Unregister_m8343F8C1A2CBA5850A44B7226093C0892FF09D8E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_3_Register_m1FD293106F3DC543B6358EDD23718BCD3D705172_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* vp_GlobalEvent_3_Unregister_m0A74D31E18A206FC06B0743A70295AA42B638AA4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Boolean_tB53F6830F670160873277339AA58F15CAED4399C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Getter_1_tB9D2902E8954106EBBDFF14735F86C73EE7B7014_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Sender_1_tDFCB1052068B848B279D11A58C6D4839CBF60347_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Sender_2_t557F66C2707C7BCB2312ED78EB6DB3F796FDE3AC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Setter_1_t00D8F1B4DA85B1105AA10CBB32D04F58B802D121_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Tryer_1_tE69F8ACC696CEB81EE6A8BE0A01682F330580866_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t ObjectPool_1_Release_mEF76D0678288FA4D4D8D81C57B3FEBF7AE87BD74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TweenRunner_1_StartTween_m8637A776CD96BAB0EDC8A8FA7479127E2BEC92C4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TweenRunner_1_StartTween_mE0CB96AF945209ABC26F2AA9899CB9794A64D92D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m17A6A3B6131EFE960C4DA7784DDE816250347E99_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ec__Iterator0_MoveNext_mB6BE65FBF43A13162E304F0F012BAA539F3D0C9F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_UpdateSize_m88288884CC9DB6CF2F34B61CA6E3403BB201C193_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_UpdateSize_mADE8D1E4EBD5325AC7FC10F496E95C8B0729FA97_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_UpdateSize_mF42B8F5F1EE0EE49971226E1B85A8C7AA47060BD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_get_SplitterValues_m2A5FEFF018D4A2B26970D6161099C7C02A8603CD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_get_SplitterValues_m87160358A768530241817773945AD38BC64FF7AB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_get_SplitterValues_mF7382AF214D51C90BF442ECEB0E77DBC047F2E7A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_set_SplitterValues_m1D0C7006179C11D457C4F51EDC7AE522B011BD4B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_set_SplitterValues_mA2BB08E07E9CBA122999FD5534B09A724351C01A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ValuesSplitter_1_set_SplitterValues_mA5EA6991C9FC68B9DAA394CFF6DC0C40C5C9898F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Attempt_1_InitFields_mE66712276CE59403FB9092ECE400F1FF356311F3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Attempt_1_Register_m8EA5B549BE894D22D2A97E0582ECE5E6BCFC2879_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_1_Register_m9267D1714DF192E02EEF7CFC667CA9CD1629007A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_1_Send_m65932A48D6E7714790169F961B61359F11487A0A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_1_Unregister_m367761F3D9A8BEDAF895CC87178965D5A3D5787B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_1__cctor_m8313D94E56FB3E4963AC3B968D77A3A6C6C6EAC9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_2_Register_m65F5A02295B8DAB588E6AD98D0CD0ED1CF9D9979_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_2_Send_mA8EA70105B38F57E1AEE089ACEA6ADA5FEE4F718_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_2_Unregister_mFFC1B7889EA18F8910084CEFA3FA7D928A576786_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_2__cctor_m37FE26F32E9C5259667FE0AB6F34421EBEBB6531_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_3_Register_m623AEF48AC44C458224D8B5BC938B2B78D427652_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_3_Send_m53BF34DD0041FAD79B5E877AC5D3992446787EB5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_3_Unregister_mA7371437A5820946B3B160009A4CA66B8A831D96_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_3__cctor_mFD51E060AAF6726148E517A2CCF33699FC889D05_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_4_Register_mCABDC7AF7ADC056A07F0BFF4583EDA80FA020487_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_4_Unregister_m5834FBF25CBBAA2D9673C24D873D100CCEE3456D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEventReturn_4__cctor_m5B64DD7C06F04FA8284829E44C703BEA57A42DBA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_1_Register_m0C35CD0E2D37D4538B19E15618CE12ADA4A38169_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_1_Send_m0E8BD5CE11E352A4C0D2A5FEE5F00581654796BE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_1_Unregister_m1201A989E77B0ABA197E41B3AC02409707D26587_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_1__cctor_m8C71D551E610B679C9B01160BF0E3070F6F5499E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_2_Register_mF7331093FE8FB0056E02990FE3D49F16E347B0B2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_2_Send_m5B777A199A27FAF9D55E45AAA843200FB804A6EE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_2_Unregister_m8343F8C1A2CBA5850A44B7226093C0892FF09D8E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_2__cctor_m59EEE797AFF3BE633FB88833C1C08575FFCA20F2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_3_Register_m1FD293106F3DC543B6358EDD23718BCD3D705172_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_3_Unregister_m0A74D31E18A206FC06B0743A70295AA42B638AA4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_GlobalEvent_3__cctor_m59ABA036DB2D978891CF7FB11877A54CE62ED7CC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Message_1_InitFields_m2FF45187A9344F1507111BEB6C12C1E134993287_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Message_2_InitFields_m7310D8CCD00D76EB075DFA2C58FB566E0007996A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_1_Register_m0FB46C04A3BE32207A707A4876567027CA2743F2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_1_SendUpwards_m767FDA538F65CA8AD887A3B0BF7E15887E9851B7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_1_Send_mEA282C4FCC4DB09854FCDBEBCFDB729FB02503E1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_1_Unregister_m469F0813AE9A4A752CCEC06AF95E09A79269CCE3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_1_Unregister_m943E81775CE445EF3BCDC2A22A99158C2EBA64CC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_1_Unregister_mA54E066D4A9558C9B2D85A083BD0CBAF2DD79659_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_2_Register_m3C9392F59BA09F5809754E122A54C8CD5A0F8789_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_2_SendUpwards_mA4BEC7B1A4DB61610AB8E7B851C9A9CB60788AC7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_2_Send_mF71934FC138D1E8205983A52C03F45D5939B4213_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_2_Unregister_m1EB26964D306927832BE535538797EFD39B0534A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_2_Unregister_m928B4EDD7C6F24678815CD19192F17543DA324BE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_3_Register_mE9CADC56337B0DB60F2031A5AD8DBBF5788A04ED_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_3_SendUpwards_m1044D8330CE51AEE84CA7D368608FEA5EE9173E5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_3_Send_m5B1641CFEC457D75678CA7F0C2F7F651ABA7592E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_3_Unregister_m981527B87DEDE801D932FA05E486D5CA1E5AD937_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_3_Unregister_mC6499B4F1490E5D5922E25E210FC7B0C49D32795_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_4_Register_m12DA30F5069C305A184975243D563643B2D345A8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_4_SendUpwards_mE2A7AF6484398084184D8A2DEBEF33995BAF26E4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_4_Send_m5824C02E1489177550A07B4E9899AD0693991F6A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_4_Unregister_m9E9646F122A7A1664598747675E5A043942677E7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEventReturn_4_Unregister_mD41416B1FB2E4C2D16A70DC95C80F58DA77D6340_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_1_Register_mDEC5F0CB1A3157D51B9AD2E6CCBBB8A9D4367D8E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_1_SendUpwards_m7B41D6CA1AE1F4EBA0C5868CA3C4DAB066E92D2B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_1_Send_m00ECA85A639711ABDB10FDB6814B2B2096839A1F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_1_Unregister_m5AABE7B7ABE1027AD028EAC28778D7B0B2246FA4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_1_Unregister_mE8F50534CAD6ACB131C352A8FC9BCF2965EBF462_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_2_Register_mFFA414B55C3D7802ABACF85B34A73815854272A4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_2_SendUpwards_m2FBD71B9B6DB566EB6BECA6FE7CFE89DEE1497CB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_2_Send_m6F626C51DCE56C52072D4189297AF7E84EA2E6A7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_2_Unregister_m3B58496214F2DA0651A83EF689CF9D81EE287B6E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_2_Unregister_mE280B2398E29ED4C314F37F7878D56E6742A819B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_3_Register_m1C8C60BE8E73FF61BD98A05E95ABFD5EBFDAFA72_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_3_SendUpwards_m4687AE23C56E2A679DBA9C7F3C96ABF7B8CEA2E4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_3_Send_m3E96761380D34EE709938D3BB8878E61DF4E5351_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_3_Unregister_m5E18276E850303977BA5BD095B9761B62467A4DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_TargetEvent_3_Unregister_m9F51B5AE991AD52AB668788029CFD11753D74A7D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Value_1_InitFields_m79BCE6D0E34002CA25EB4A0DA15D2B559226FC41_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE;
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5;
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___entries_1)); }
	inline EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___keys_7)); }
	inline KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t666396E67E50284D48938851873CE562083D67F2 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___values_8)); }
	inline ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * get_values_8() const { return ___values_8; }
	inline ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____items_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Color32>
struct  List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____items_1)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get__items_1() const { return ____items_1; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5_StaticFields, ____emptyArray_5)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct  List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____items_1)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get__items_1() const { return ____items_1; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554_StaticFields, ____emptyArray_5)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____items_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____items_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____items_1)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955_StaticFields, ____emptyArray_5)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<vp_GlobalCallbackReturn`1<System.Object>>
struct  List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718, ____items_1)); }
	inline vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6* get__items_1() const { return ____items_1; }
	inline vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718_StaticFields, ____emptyArray_5)); }
	inline vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(vp_GlobalCallbackReturn_1U5BU5D_t4813988997E2547994303AB431A08AB77ED2EEB6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<vp_GlobalCallbackReturn`2<System.Object,System.Object>>
struct  List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091, ____items_1)); }
	inline vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6* get__items_1() const { return ____items_1; }
	inline vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091_StaticFields, ____emptyArray_5)); }
	inline vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(vp_GlobalCallbackReturn_2U5BU5D_t93882107C62ED18D3B14792E1B033D551D86E2C6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>>
struct  List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735, ____items_1)); }
	inline vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA* get__items_1() const { return ____items_1; }
	inline vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735_StaticFields, ____emptyArray_5)); }
	inline vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(vp_GlobalCallbackReturn_3U5BU5D_t15D325BF5DB4CB655428478D2ED30A7DF39905FA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>>
struct  List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0, ____items_1)); }
	inline vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767* get__items_1() const { return ____items_1; }
	inline vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0_StaticFields, ____emptyArray_5)); }
	inline vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767* get__emptyArray_5() const { return ____emptyArray_5; }
	inline vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(vp_GlobalCallbackReturn_4U5BU5D_t511C17280DE81403CA31F8C9429AD0CBF7E00767* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<vp_GlobalCallback`1<System.Object>>
struct  List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3, ____items_1)); }
	inline vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8* get__items_1() const { return ____items_1; }
	inline vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3_StaticFields, ____emptyArray_5)); }
	inline vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8* get__emptyArray_5() const { return ____emptyArray_5; }
	inline vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(vp_GlobalCallback_1U5BU5D_t50114B277F1A4A5EFAFD52A584A927878D5F7EC8* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<vp_GlobalCallback`2<System.Object,System.Object>>
struct  List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E, ____items_1)); }
	inline vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051* get__items_1() const { return ____items_1; }
	inline vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E_StaticFields, ____emptyArray_5)); }
	inline vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051* get__emptyArray_5() const { return ____emptyArray_5; }
	inline vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(vp_GlobalCallback_2U5BU5D_tFE4CDBA78D2D530A8091E63137CC29BCDE8EB051* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<vp_GlobalCallback`3<System.Object,System.Object,System.Object>>
struct  List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE, ____items_1)); }
	inline vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644* get__items_1() const { return ____items_1; }
	inline vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE_StaticFields, ____emptyArray_5)); }
	inline vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644* get__emptyArray_5() const { return ____emptyArray_5; }
	inline vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(vp_GlobalCallback_3U5BU5D_tC97D275DC4ACF60EB02F5C09DE74C069D0C68644* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Stack`1<System.Object>
struct  Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275, ____array_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct  TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1::m_CoroutineContainer
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1::m_Tween
	RuntimeObject* ___m_Tween_1;

public:
	inline static int32_t get_offset_of_m_CoroutineContainer_0() { return static_cast<int32_t>(offsetof(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172, ___m_CoroutineContainer_0)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_m_CoroutineContainer_0() const { return ___m_CoroutineContainer_0; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_m_CoroutineContainer_0() { return &___m_CoroutineContainer_0; }
	inline void set_m_CoroutineContainer_0(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___m_CoroutineContainer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CoroutineContainer_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tween_1() { return static_cast<int32_t>(offsetof(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172, ___m_Tween_1)); }
	inline RuntimeObject* get_m_Tween_1() const { return ___m_Tween_1; }
	inline RuntimeObject** get_address_of_m_Tween_1() { return &___m_Tween_1; }
	inline void set_m_Tween_1(RuntimeObject* value)
	{
		___m_Tween_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tween_1), (void*)value);
	}
};


// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct  TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1::m_CoroutineContainer
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1::m_Tween
	RuntimeObject* ___m_Tween_1;

public:
	inline static int32_t get_offset_of_m_CoroutineContainer_0() { return static_cast<int32_t>(offsetof(TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF, ___m_CoroutineContainer_0)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_m_CoroutineContainer_0() const { return ___m_CoroutineContainer_0; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_m_CoroutineContainer_0() { return &___m_CoroutineContainer_0; }
	inline void set_m_CoroutineContainer_0(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___m_CoroutineContainer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CoroutineContainer_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tween_1() { return static_cast<int32_t>(offsetof(TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF, ___m_Tween_1)); }
	inline RuntimeObject* get_m_Tween_1() const { return ___m_Tween_1; }
	inline RuntimeObject** get_address_of_m_Tween_1() { return &___m_Tween_1; }
	inline void set_m_Tween_1(RuntimeObject* value)
	{
		___m_Tween_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tween_1), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<System.Int32>
struct  ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__mgU24cache0_1), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<System.Object>
struct  ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__mgU24cache0_1), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.Color32>
struct  ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__mgU24cache0_1), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>
struct  ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__mgU24cache0_1), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.Vector2>
struct  ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__mgU24cache0_1), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.Vector3>
struct  ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__mgU24cache0_1), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.Vector4>
struct  ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * ___s_ListPool_0;
	// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::<>f__mgU24cache0
	UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3Ef__mgU24cache0_1), (void*)value);
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct  ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A, ___m_Stack_0)); }
	inline Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t8E8DE8B40F82D27FFD37A645E4DE60FF495EAE00 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A, ___m_ActionOnGet_1)); }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>
struct  ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B, ___m_Stack_0)); }
	inline Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t83EEA6C9C9B43999F0FB28B378D20F56711A9DB7 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct  ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07, ___m_Stack_0)); }
	inline Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tA8954C7914CA3BFAB547D0F09691E26AFB6138A3 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct  ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5, ___m_Stack_0)); }
	inline Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t24347DA243AA3515E9AE6C68F02B953993FF98D8 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct  ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49, ___m_Stack_0)); }
	inline Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tE4D0BA5034D7B700F0C2C183AC42BD2C969E4A6C * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2, ___m_Stack_0)); }
	inline Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tA30706EF9E94D43BFD43FE4AE0AB0499A9A1682B * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39, ___m_Stack_0)); }
	inline Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tAB502A0341E85CB581B7AF26F8E45C60127741F9 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Object>
struct  ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD, ___m_Stack_0)); }
	inline Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// vp_Event
struct  vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A  : public RuntimeObject
{
public:
	// System.String vp_Event::m_Name
	String_t* ___m_Name_0;
	// System.Type vp_Event::m_Type
	Type_t * ___m_Type_1;
	// System.Type vp_Event::m_ArgumentType
	Type_t * ___m_ArgumentType_2;
	// System.Type vp_Event::m_ReturnType
	Type_t * ___m_ReturnType_3;
	// System.Reflection.FieldInfo[] vp_Event::m_Fields
	FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* ___m_Fields_4;
	// System.Type[] vp_Event::m_DelegateTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___m_DelegateTypes_5;
	// System.Reflection.MethodInfo[] vp_Event::m_DefaultMethods
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* ___m_DefaultMethods_6;
	// System.String[] vp_Event::InvokerFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___InvokerFieldNames_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> vp_Event::Prefixes
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___Prefixes_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_Type_1)); }
	inline Type_t * get_m_Type_1() const { return ___m_Type_1; }
	inline Type_t ** get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(Type_t * value)
	{
		___m_Type_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ArgumentType_2() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_ArgumentType_2)); }
	inline Type_t * get_m_ArgumentType_2() const { return ___m_ArgumentType_2; }
	inline Type_t ** get_address_of_m_ArgumentType_2() { return &___m_ArgumentType_2; }
	inline void set_m_ArgumentType_2(Type_t * value)
	{
		___m_ArgumentType_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ArgumentType_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReturnType_3() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_ReturnType_3)); }
	inline Type_t * get_m_ReturnType_3() const { return ___m_ReturnType_3; }
	inline Type_t ** get_address_of_m_ReturnType_3() { return &___m_ReturnType_3; }
	inline void set_m_ReturnType_3(Type_t * value)
	{
		___m_ReturnType_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReturnType_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Fields_4() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_Fields_4)); }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* get_m_Fields_4() const { return ___m_Fields_4; }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE** get_address_of_m_Fields_4() { return &___m_Fields_4; }
	inline void set_m_Fields_4(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* value)
	{
		___m_Fields_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Fields_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_DelegateTypes_5() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_DelegateTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_m_DelegateTypes_5() const { return ___m_DelegateTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_m_DelegateTypes_5() { return &___m_DelegateTypes_5; }
	inline void set_m_DelegateTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___m_DelegateTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DelegateTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultMethods_6() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_DefaultMethods_6)); }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* get_m_DefaultMethods_6() const { return ___m_DefaultMethods_6; }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B** get_address_of_m_DefaultMethods_6() { return &___m_DefaultMethods_6; }
	inline void set_m_DefaultMethods_6(MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* value)
	{
		___m_DefaultMethods_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultMethods_6), (void*)value);
	}

	inline static int32_t get_offset_of_InvokerFieldNames_7() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___InvokerFieldNames_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_InvokerFieldNames_7() const { return ___InvokerFieldNames_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_InvokerFieldNames_7() { return &___InvokerFieldNames_7; }
	inline void set_InvokerFieldNames_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___InvokerFieldNames_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InvokerFieldNames_7), (void*)value);
	}

	inline static int32_t get_offset_of_Prefixes_8() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___Prefixes_8)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_Prefixes_8() const { return ___Prefixes_8; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_Prefixes_8() { return &___Prefixes_8; }
	inline void set_Prefixes_8(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___Prefixes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Prefixes_8), (void*)value);
	}
};


// vp_GlobalEventInternal
struct  vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D  : public RuntimeObject
{
public:

public:
};

struct vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields
{
public:
	// System.Collections.Hashtable vp_GlobalEventInternal::Callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___Callbacks_0;

public:
	inline static int32_t get_offset_of_Callbacks_0() { return static_cast<int32_t>(offsetof(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields, ___Callbacks_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_Callbacks_0() const { return ___Callbacks_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_Callbacks_0() { return &___Callbacks_0; }
	inline void set_Callbacks_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___Callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Callbacks_0), (void*)value);
	}
};


// vp_GlobalEventReturn`1<System.Object>
struct  vp_GlobalEventReturn_1_tAF2F1E815BC70E244630A6F27841766EF2F338D9  : public RuntimeObject
{
public:

public:
};

struct vp_GlobalEventReturn_1_tAF2F1E815BC70E244630A6F27841766EF2F338D9_StaticFields
{
public:
	// System.Collections.Hashtable vp_GlobalEventReturn`1::m_Callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_Callbacks_0;

public:
	inline static int32_t get_offset_of_m_Callbacks_0() { return static_cast<int32_t>(offsetof(vp_GlobalEventReturn_1_tAF2F1E815BC70E244630A6F27841766EF2F338D9_StaticFields, ___m_Callbacks_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_Callbacks_0() const { return ___m_Callbacks_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_Callbacks_0() { return &___m_Callbacks_0; }
	inline void set_m_Callbacks_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_Callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Callbacks_0), (void*)value);
	}
};


// vp_GlobalEventReturn`2<System.Object,System.Object>
struct  vp_GlobalEventReturn_2_tB56FD4BCAB9452523F648DD091D2A2C667F189E1  : public RuntimeObject
{
public:

public:
};

struct vp_GlobalEventReturn_2_tB56FD4BCAB9452523F648DD091D2A2C667F189E1_StaticFields
{
public:
	// System.Collections.Hashtable vp_GlobalEventReturn`2::m_Callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_Callbacks_0;

public:
	inline static int32_t get_offset_of_m_Callbacks_0() { return static_cast<int32_t>(offsetof(vp_GlobalEventReturn_2_tB56FD4BCAB9452523F648DD091D2A2C667F189E1_StaticFields, ___m_Callbacks_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_Callbacks_0() const { return ___m_Callbacks_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_Callbacks_0() { return &___m_Callbacks_0; }
	inline void set_m_Callbacks_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_Callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Callbacks_0), (void*)value);
	}
};


// vp_GlobalEventReturn`3<System.Object,System.Object,System.Object>
struct  vp_GlobalEventReturn_3_t0D2E65F80187163C7E074E25C569E68C3A6D72A6  : public RuntimeObject
{
public:

public:
};

struct vp_GlobalEventReturn_3_t0D2E65F80187163C7E074E25C569E68C3A6D72A6_StaticFields
{
public:
	// System.Collections.Hashtable vp_GlobalEventReturn`3::m_Callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_Callbacks_0;

public:
	inline static int32_t get_offset_of_m_Callbacks_0() { return static_cast<int32_t>(offsetof(vp_GlobalEventReturn_3_t0D2E65F80187163C7E074E25C569E68C3A6D72A6_StaticFields, ___m_Callbacks_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_Callbacks_0() const { return ___m_Callbacks_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_Callbacks_0() { return &___m_Callbacks_0; }
	inline void set_m_Callbacks_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_Callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Callbacks_0), (void*)value);
	}
};


// vp_GlobalEventReturn`4<System.Object,System.Object,System.Object,System.Object>
struct  vp_GlobalEventReturn_4_t70B37E514169414D4CBA831FF242537C6E61E751  : public RuntimeObject
{
public:

public:
};

struct vp_GlobalEventReturn_4_t70B37E514169414D4CBA831FF242537C6E61E751_StaticFields
{
public:
	// System.Collections.Hashtable vp_GlobalEventReturn`4::m_Callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_Callbacks_0;

public:
	inline static int32_t get_offset_of_m_Callbacks_0() { return static_cast<int32_t>(offsetof(vp_GlobalEventReturn_4_t70B37E514169414D4CBA831FF242537C6E61E751_StaticFields, ___m_Callbacks_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_Callbacks_0() const { return ___m_Callbacks_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_Callbacks_0() { return &___m_Callbacks_0; }
	inline void set_m_Callbacks_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_Callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Callbacks_0), (void*)value);
	}
};


// vp_GlobalEvent`1<System.Object>
struct  vp_GlobalEvent_1_tBB56B018FE03878F52DDD05CF63495FE7B7CBF0F  : public RuntimeObject
{
public:

public:
};

struct vp_GlobalEvent_1_tBB56B018FE03878F52DDD05CF63495FE7B7CBF0F_StaticFields
{
public:
	// System.Collections.Hashtable vp_GlobalEvent`1::m_Callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_Callbacks_0;

public:
	inline static int32_t get_offset_of_m_Callbacks_0() { return static_cast<int32_t>(offsetof(vp_GlobalEvent_1_tBB56B018FE03878F52DDD05CF63495FE7B7CBF0F_StaticFields, ___m_Callbacks_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_Callbacks_0() const { return ___m_Callbacks_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_Callbacks_0() { return &___m_Callbacks_0; }
	inline void set_m_Callbacks_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_Callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Callbacks_0), (void*)value);
	}
};


// vp_GlobalEvent`2<System.Object,System.Object>
struct  vp_GlobalEvent_2_t12287246C36DFF3C8C0AAE2326EFA5217F510174  : public RuntimeObject
{
public:

public:
};

struct vp_GlobalEvent_2_t12287246C36DFF3C8C0AAE2326EFA5217F510174_StaticFields
{
public:
	// System.Collections.Hashtable vp_GlobalEvent`2::m_Callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_Callbacks_0;

public:
	inline static int32_t get_offset_of_m_Callbacks_0() { return static_cast<int32_t>(offsetof(vp_GlobalEvent_2_t12287246C36DFF3C8C0AAE2326EFA5217F510174_StaticFields, ___m_Callbacks_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_Callbacks_0() const { return ___m_Callbacks_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_Callbacks_0() { return &___m_Callbacks_0; }
	inline void set_m_Callbacks_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_Callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Callbacks_0), (void*)value);
	}
};


// vp_GlobalEvent`3<System.Object,System.Object,System.Object>
struct  vp_GlobalEvent_3_tF49037CAC8AA47874CE01660782E0BC9CB40896A  : public RuntimeObject
{
public:

public:
};

struct vp_GlobalEvent_3_tF49037CAC8AA47874CE01660782E0BC9CB40896A_StaticFields
{
public:
	// System.Collections.Hashtable vp_GlobalEvent`3::m_Callbacks
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___m_Callbacks_0;

public:
	inline static int32_t get_offset_of_m_Callbacks_0() { return static_cast<int32_t>(offsetof(vp_GlobalEvent_3_tF49037CAC8AA47874CE01660782E0BC9CB40896A_StaticFields, ___m_Callbacks_0)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_m_Callbacks_0() const { return ___m_Callbacks_0; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_m_Callbacks_0() { return &___m_Callbacks_0; }
	inline void set_m_Callbacks_0(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___m_Callbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Callbacks_0), (void*)value);
	}
};


// vp_TargetEventReturn`1<System.Object>
struct  vp_TargetEventReturn_1_t18616D984653D31B032A3077B90BBF709809608C  : public RuntimeObject
{
public:

public:
};


// vp_TargetEventReturn`2<System.Object,System.Object>
struct  vp_TargetEventReturn_2_tA86816A778A183469BC080069C97962F40004116  : public RuntimeObject
{
public:

public:
};


// vp_TargetEventReturn`3<System.Object,System.Object,System.Object>
struct  vp_TargetEventReturn_3_t8134F2BD2CC8D32F8E8020A4253E52A500D1000A  : public RuntimeObject
{
public:

public:
};


// vp_TargetEventReturn`4<System.Object,System.Object,System.Object,System.Object>
struct  vp_TargetEventReturn_4_tEBCB54A574E6959E9E3BB3B42CF483677D78E79C  : public RuntimeObject
{
public:

public:
};


// vp_TargetEvent`1<System.Object>
struct  vp_TargetEvent_1_tFD0995BCCBC1B24477810900E7C72DFD568C960D  : public RuntimeObject
{
public:

public:
};


// vp_TargetEvent`2<System.Object,System.Object>
struct  vp_TargetEvent_2_tAC19C28AD06D122D1DEC81229358508DFC1D913F  : public RuntimeObject
{
public:

public:
};


// vp_TargetEvent`3<System.Object,System.Object,System.Object>
struct  vp_TargetEvent_3_tA6CA0EFF04B6825B82A8AFE345A68AD3055A315B  : public RuntimeObject
{
public:

public:
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<vp_GlobalCallbackReturn`1<System.Object>>
struct  Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372, ___list_0)); }
	inline List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * get_list_0() const { return ___list_0; }
	inline List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372, ___current_3)); }
	inline vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * get_current_3() const { return ___current_3; }
	inline vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<vp_GlobalCallbackReturn`2<System.Object,System.Object>>
struct  Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC, ___list_0)); }
	inline List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * get_list_0() const { return ___list_0; }
	inline List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC, ___current_3)); }
	inline vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * get_current_3() const { return ___current_3; }
	inline vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>>
struct  Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689, ___list_0)); }
	inline List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * get_list_0() const { return ___list_0; }
	inline List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689, ___current_3)); }
	inline vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * get_current_3() const { return ___current_3; }
	inline vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>>
struct  Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564, ___list_0)); }
	inline List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * get_list_0() const { return ___list_0; }
	inline List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564, ___current_3)); }
	inline vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * get_current_3() const { return ___current_3; }
	inline vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<vp_GlobalCallback`1<System.Object>>
struct  Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21, ___list_0)); }
	inline List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * get_list_0() const { return ___list_0; }
	inline List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21, ___current_3)); }
	inline vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * get_current_3() const { return ___current_3; }
	inline vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<vp_GlobalCallback`2<System.Object,System.Object>>
struct  Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A, ___list_0)); }
	inline List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * get_list_0() const { return ___list_0; }
	inline List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A, ___current_3)); }
	inline vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * get_current_3() const { return ___current_3; }
	inline vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<vp_GlobalCallback`3<System.Object,System.Object,System.Object>>
struct  Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE, ___list_0)); }
	inline List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * get_list_0() const { return ___list_0; }
	inline List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE, ___current_3)); }
	inline vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * get_current_3() const { return ___current_3; }
	inline vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Double
struct  Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Reflection.FieldInfo
struct  FieldInfo_t  : public MemberInfo_t
{
public:

public:
};


// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// System.UInt32
struct  UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.UI.CoroutineTween.FloatTween
struct  FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A 
{
public:
	// UnityEngine.UI.CoroutineTween.FloatTween_FloatTweenCallback UnityEngine.UI.CoroutineTween.FloatTween::m_Target
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_Target_0)); }
	inline FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A_marshaled_pinvoke
{
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A_marshaled_com
{
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// vp_Activity
struct  vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94  : public vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A
{
public:
	// vp_Activity_Callback vp_Activity::StartCallbacks
	Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * ___StartCallbacks_9;
	// vp_Activity_Callback vp_Activity::StopCallbacks
	Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * ___StopCallbacks_10;
	// vp_Activity_Condition vp_Activity::StartConditions
	Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A * ___StartConditions_11;
	// vp_Activity_Condition vp_Activity::StopConditions
	Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A * ___StopConditions_12;
	// vp_Activity_Callback vp_Activity::FailStartCallbacks
	Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * ___FailStartCallbacks_13;
	// vp_Activity_Callback vp_Activity::FailStopCallbacks
	Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * ___FailStopCallbacks_14;
	// vp_Timer_Handle vp_Activity::m_ForceStopTimer
	Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * ___m_ForceStopTimer_15;
	// System.Object vp_Activity::m_Argument
	RuntimeObject * ___m_Argument_16;
	// System.Boolean vp_Activity::m_Active
	bool ___m_Active_17;
	// System.Single vp_Activity::NextAllowedStartTime
	float ___NextAllowedStartTime_18;
	// System.Single vp_Activity::NextAllowedStopTime
	float ___NextAllowedStopTime_19;
	// System.Single vp_Activity::m_MinPause
	float ___m_MinPause_20;
	// System.Single vp_Activity::m_MinDuration
	float ___m_MinDuration_21;
	// System.Single vp_Activity::m_MaxDuration
	float ___m_MaxDuration_22;

public:
	inline static int32_t get_offset_of_StartCallbacks_9() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___StartCallbacks_9)); }
	inline Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * get_StartCallbacks_9() const { return ___StartCallbacks_9; }
	inline Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D ** get_address_of_StartCallbacks_9() { return &___StartCallbacks_9; }
	inline void set_StartCallbacks_9(Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * value)
	{
		___StartCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StartCallbacks_9), (void*)value);
	}

	inline static int32_t get_offset_of_StopCallbacks_10() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___StopCallbacks_10)); }
	inline Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * get_StopCallbacks_10() const { return ___StopCallbacks_10; }
	inline Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D ** get_address_of_StopCallbacks_10() { return &___StopCallbacks_10; }
	inline void set_StopCallbacks_10(Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * value)
	{
		___StopCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StopCallbacks_10), (void*)value);
	}

	inline static int32_t get_offset_of_StartConditions_11() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___StartConditions_11)); }
	inline Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A * get_StartConditions_11() const { return ___StartConditions_11; }
	inline Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A ** get_address_of_StartConditions_11() { return &___StartConditions_11; }
	inline void set_StartConditions_11(Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A * value)
	{
		___StartConditions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StartConditions_11), (void*)value);
	}

	inline static int32_t get_offset_of_StopConditions_12() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___StopConditions_12)); }
	inline Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A * get_StopConditions_12() const { return ___StopConditions_12; }
	inline Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A ** get_address_of_StopConditions_12() { return &___StopConditions_12; }
	inline void set_StopConditions_12(Condition_tAB84EB270C46F45A247E2A9F410CC65626A4C39A * value)
	{
		___StopConditions_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StopConditions_12), (void*)value);
	}

	inline static int32_t get_offset_of_FailStartCallbacks_13() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___FailStartCallbacks_13)); }
	inline Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * get_FailStartCallbacks_13() const { return ___FailStartCallbacks_13; }
	inline Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D ** get_address_of_FailStartCallbacks_13() { return &___FailStartCallbacks_13; }
	inline void set_FailStartCallbacks_13(Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * value)
	{
		___FailStartCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FailStartCallbacks_13), (void*)value);
	}

	inline static int32_t get_offset_of_FailStopCallbacks_14() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___FailStopCallbacks_14)); }
	inline Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * get_FailStopCallbacks_14() const { return ___FailStopCallbacks_14; }
	inline Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D ** get_address_of_FailStopCallbacks_14() { return &___FailStopCallbacks_14; }
	inline void set_FailStopCallbacks_14(Callback_tC1F58628A5A091AD708A1BFD76BA3E9E54B1394D * value)
	{
		___FailStopCallbacks_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FailStopCallbacks_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_ForceStopTimer_15() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___m_ForceStopTimer_15)); }
	inline Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * get_m_ForceStopTimer_15() const { return ___m_ForceStopTimer_15; }
	inline Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 ** get_address_of_m_ForceStopTimer_15() { return &___m_ForceStopTimer_15; }
	inline void set_m_ForceStopTimer_15(Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * value)
	{
		___m_ForceStopTimer_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ForceStopTimer_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_Argument_16() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___m_Argument_16)); }
	inline RuntimeObject * get_m_Argument_16() const { return ___m_Argument_16; }
	inline RuntimeObject ** get_address_of_m_Argument_16() { return &___m_Argument_16; }
	inline void set_m_Argument_16(RuntimeObject * value)
	{
		___m_Argument_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Argument_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_Active_17() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___m_Active_17)); }
	inline bool get_m_Active_17() const { return ___m_Active_17; }
	inline bool* get_address_of_m_Active_17() { return &___m_Active_17; }
	inline void set_m_Active_17(bool value)
	{
		___m_Active_17 = value;
	}

	inline static int32_t get_offset_of_NextAllowedStartTime_18() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___NextAllowedStartTime_18)); }
	inline float get_NextAllowedStartTime_18() const { return ___NextAllowedStartTime_18; }
	inline float* get_address_of_NextAllowedStartTime_18() { return &___NextAllowedStartTime_18; }
	inline void set_NextAllowedStartTime_18(float value)
	{
		___NextAllowedStartTime_18 = value;
	}

	inline static int32_t get_offset_of_NextAllowedStopTime_19() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___NextAllowedStopTime_19)); }
	inline float get_NextAllowedStopTime_19() const { return ___NextAllowedStopTime_19; }
	inline float* get_address_of_NextAllowedStopTime_19() { return &___NextAllowedStopTime_19; }
	inline void set_NextAllowedStopTime_19(float value)
	{
		___NextAllowedStopTime_19 = value;
	}

	inline static int32_t get_offset_of_m_MinPause_20() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___m_MinPause_20)); }
	inline float get_m_MinPause_20() const { return ___m_MinPause_20; }
	inline float* get_address_of_m_MinPause_20() { return &___m_MinPause_20; }
	inline void set_m_MinPause_20(float value)
	{
		___m_MinPause_20 = value;
	}

	inline static int32_t get_offset_of_m_MinDuration_21() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___m_MinDuration_21)); }
	inline float get_m_MinDuration_21() const { return ___m_MinDuration_21; }
	inline float* get_address_of_m_MinDuration_21() { return &___m_MinDuration_21; }
	inline void set_m_MinDuration_21(float value)
	{
		___m_MinDuration_21 = value;
	}

	inline static int32_t get_offset_of_m_MaxDuration_22() { return static_cast<int32_t>(offsetof(vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94, ___m_MaxDuration_22)); }
	inline float get_m_MaxDuration_22() const { return ___m_MaxDuration_22; }
	inline float* get_address_of_m_MaxDuration_22() { return &___m_MaxDuration_22; }
	inline void set_m_MaxDuration_22(float value)
	{
		___m_MaxDuration_22 = value;
	}
};


// vp_Attempt
struct  vp_Attempt_t28537319AB5D1247932A58A0DFE65B8353FEA615  : public vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A
{
public:
	// vp_Attempt_Tryer vp_Attempt::Try
	Tryer_tCAC3063BFA7408FF3256C77068D8881299B4DD7E * ___Try_9;

public:
	inline static int32_t get_offset_of_Try_9() { return static_cast<int32_t>(offsetof(vp_Attempt_t28537319AB5D1247932A58A0DFE65B8353FEA615, ___Try_9)); }
	inline Tryer_tCAC3063BFA7408FF3256C77068D8881299B4DD7E * get_Try_9() const { return ___Try_9; }
	inline Tryer_tCAC3063BFA7408FF3256C77068D8881299B4DD7E ** get_address_of_Try_9() { return &___Try_9; }
	inline void set_Try_9(Tryer_tCAC3063BFA7408FF3256C77068D8881299B4DD7E * value)
	{
		___Try_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Try_9), (void*)value);
	}
};


// vp_GlobalEventInternal_SendException
struct  SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A  : public Exception_t
{
public:

public:
};


// vp_GlobalEventInternal_UnregisterException
struct  UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA  : public Exception_t
{
public:

public:
};


// vp_Message
struct  vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989  : public vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A
{
public:
	// vp_Message_Sender vp_Message::Send
	Sender_t71699D3956D8A1B096FAD0228565F659FEDDF888 * ___Send_9;

public:
	inline static int32_t get_offset_of_Send_9() { return static_cast<int32_t>(offsetof(vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989, ___Send_9)); }
	inline Sender_t71699D3956D8A1B096FAD0228565F659FEDDF888 * get_Send_9() const { return ___Send_9; }
	inline Sender_t71699D3956D8A1B096FAD0228565F659FEDDF888 ** get_address_of_Send_9() { return &___Send_9; }
	inline void set_Send_9(Sender_t71699D3956D8A1B096FAD0228565F659FEDDF888 * value)
	{
		___Send_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Send_9), (void*)value);
	}
};


// vp_Value`1<System.Object>
struct  vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479  : public vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A
{
public:
	// vp_Value`1_Getter`1<V,V> vp_Value`1::Get
	Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * ___Get_9;
	// vp_Value`1_Setter`1<V,V> vp_Value`1::Set
	Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * ___Set_10;

public:
	inline static int32_t get_offset_of_Get_9() { return static_cast<int32_t>(offsetof(vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479, ___Get_9)); }
	inline Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * get_Get_9() const { return ___Get_9; }
	inline Getter_1_t937722DADE2AAED1621E908646316A6E193A266B ** get_address_of_Get_9() { return &___Get_9; }
	inline void set_Get_9(Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * value)
	{
		___Get_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Get_9), (void*)value);
	}

	inline static int32_t get_offset_of_Set_10() { return static_cast<int32_t>(offsetof(vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479, ___Set_10)); }
	inline Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * get_Set_10() const { return ___Set_10; }
	inline Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 ** get_address_of_Set_10() { return &___Set_10; }
	inline void set_Set_10(Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * value)
	{
		___Set_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Set_10), (void*)value);
	}
};


// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.Collections.Hashtable
struct  Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9  : public RuntimeObject
{
public:
	// System.Collections.Hashtable_bucket[] System.Collections.Hashtable::buckets
	bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A* ___buckets_0;
	// System.Int32 System.Collections.Hashtable::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Hashtable::occupancy
	int32_t ___occupancy_2;
	// System.Int32 System.Collections.Hashtable::loadsize
	int32_t ___loadsize_3;
	// System.Single System.Collections.Hashtable::loadFactor
	float ___loadFactor_4;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Hashtable::version
	int32_t ___version_5;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Collections.Hashtable::isWriterInProgress
	bool ___isWriterInProgress_6;
	// System.Collections.ICollection System.Collections.Hashtable::keys
	RuntimeObject* ___keys_7;
	// System.Collections.IEqualityComparer System.Collections.Hashtable::_keycomparer
	RuntimeObject* ____keycomparer_8;
	// System.Object System.Collections.Hashtable::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___buckets_0)); }
	inline bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A* get_buckets_0() const { return ___buckets_0; }
	inline bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(bucketU5BU5D_t6FF2C2C4B21F2206885CD19A78F68B874C8DC84A* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_occupancy_2() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___occupancy_2)); }
	inline int32_t get_occupancy_2() const { return ___occupancy_2; }
	inline int32_t* get_address_of_occupancy_2() { return &___occupancy_2; }
	inline void set_occupancy_2(int32_t value)
	{
		___occupancy_2 = value;
	}

	inline static int32_t get_offset_of_loadsize_3() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___loadsize_3)); }
	inline int32_t get_loadsize_3() const { return ___loadsize_3; }
	inline int32_t* get_address_of_loadsize_3() { return &___loadsize_3; }
	inline void set_loadsize_3(int32_t value)
	{
		___loadsize_3 = value;
	}

	inline static int32_t get_offset_of_loadFactor_4() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___loadFactor_4)); }
	inline float get_loadFactor_4() const { return ___loadFactor_4; }
	inline float* get_address_of_loadFactor_4() { return &___loadFactor_4; }
	inline void set_loadFactor_4(float value)
	{
		___loadFactor_4 = value;
	}

	inline static int32_t get_offset_of_version_5() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___version_5)); }
	inline int32_t get_version_5() const { return ___version_5; }
	inline int32_t* get_address_of_version_5() { return &___version_5; }
	inline void set_version_5(int32_t value)
	{
		___version_5 = value;
	}

	inline static int32_t get_offset_of_isWriterInProgress_6() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___isWriterInProgress_6)); }
	inline bool get_isWriterInProgress_6() const { return ___isWriterInProgress_6; }
	inline bool* get_address_of_isWriterInProgress_6() { return &___isWriterInProgress_6; }
	inline void set_isWriterInProgress_6(bool value)
	{
		___isWriterInProgress_6 = value;
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ___keys_7)); }
	inline RuntimeObject* get_keys_7() const { return ___keys_7; }
	inline RuntimeObject** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(RuntimeObject* value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of__keycomparer_8() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ____keycomparer_8)); }
	inline RuntimeObject* get__keycomparer_8() const { return ____keycomparer_8; }
	inline RuntimeObject** get_address_of__keycomparer_8() { return &____keycomparer_8; }
	inline void set__keycomparer_8(RuntimeObject* value)
	{
		____keycomparer_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____keycomparer_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode
struct  ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F 
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>
struct  U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286  : public RuntimeObject
{
public:
	// T UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::tweenInfo
	FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___tweenInfo_0;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_1;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::<percentage>__1
	float ___U3CpercentageU3E__1_2;
	// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_tweenInfo_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___tweenInfo_0)); }
	inline FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  get_tweenInfo_0() const { return ___tweenInfo_0; }
	inline FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * get_address_of_tweenInfo_0() { return &___tweenInfo_0; }
	inline void set_tweenInfo_0(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  value)
	{
		___tweenInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___tweenInfo_0))->___m_Target_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U3CelapsedTimeU3E__0_1)); }
	inline float get_U3CelapsedTimeU3E__0_1() const { return ___U3CelapsedTimeU3E__0_1; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_1() { return &___U3CelapsedTimeU3E__0_1; }
	inline void set_U3CelapsedTimeU3E__0_1(float value)
	{
		___U3CelapsedTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpercentageU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U3CpercentageU3E__1_2)); }
	inline float get_U3CpercentageU3E__1_2() const { return ___U3CpercentageU3E__1_2; }
	inline float* get_address_of_U3CpercentageU3E__1_2() { return &___U3CpercentageU3E__1_2; }
	inline void set_U3CpercentageU3E__1_2(float value)
	{
		___U3CpercentageU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U24current_3), (void*)value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};


// vp_Activity`1<System.Object>
struct  vp_Activity_1_t19E97BA506EE5DCBB057302B947C3A9485DB47F0  : public vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94
{
public:

public:
};


// vp_Attempt`1<System.Object>
struct  vp_Attempt_1_t5F55710F74CD633CA4E9717FA9613A936464E68F  : public vp_Attempt_t28537319AB5D1247932A58A0DFE65B8353FEA615
{
public:
	// vp_Attempt`1_Tryer`1<V,V> vp_Attempt`1::Try
	Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF * ___Try_10;

public:
	inline static int32_t get_offset_of_Try_10() { return static_cast<int32_t>(offsetof(vp_Attempt_1_t5F55710F74CD633CA4E9717FA9613A936464E68F, ___Try_10)); }
	inline Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF * get_Try_10() const { return ___Try_10; }
	inline Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF ** get_address_of_Try_10() { return &___Try_10; }
	inline void set_Try_10(Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF * value)
	{
		___Try_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Try_10), (void*)value);
	}
};


// vp_GlobalEventMode
struct  vp_GlobalEventMode_tA1BE6057FEF1D4D7BD269C00F2BD44920956E4E1 
{
public:
	// System.Int32 vp_GlobalEventMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(vp_GlobalEventMode_tA1BE6057FEF1D4D7BD269C00F2BD44920956E4E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// vp_Message`1<System.Object>
struct  vp_Message_1_t07E944A5FFD733133B2B1970B4A9AC638975BE3A  : public vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989
{
public:
	// vp_Message`1_Sender`1<V,V> vp_Message`1::Send
	Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59 * ___Send_10;

public:
	inline static int32_t get_offset_of_Send_10() { return static_cast<int32_t>(offsetof(vp_Message_1_t07E944A5FFD733133B2B1970B4A9AC638975BE3A, ___Send_10)); }
	inline Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59 * get_Send_10() const { return ___Send_10; }
	inline Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59 ** get_address_of_Send_10() { return &___Send_10; }
	inline void set_Send_10(Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59 * value)
	{
		___Send_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Send_10), (void*)value);
	}
};


// vp_Message`2<System.Object,System.Object>
struct  vp_Message_2_t30D2E82656A74C71DF80691139A062CF3308E8EE  : public vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989
{
public:
	// vp_Message`2_Sender`2<V,VResult,V,VResult> vp_Message`2::Send
	Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80 * ___Send_10;

public:
	inline static int32_t get_offset_of_Send_10() { return static_cast<int32_t>(offsetof(vp_Message_2_t30D2E82656A74C71DF80691139A062CF3308E8EE, ___Send_10)); }
	inline Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80 * get_Send_10() const { return ___Send_10; }
	inline Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80 ** get_address_of_Send_10() { return &___Send_10; }
	inline void set_Send_10(Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80 * value)
	{
		___Send_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Send_10), (void*)value);
	}
};


// vp_TargetEventOptions
struct  vp_TargetEventOptions_t52EAC66E7A901D1A4DD7F2B2393D9D9D5DA155CA 
{
public:
	// System.Int32 vp_TargetEventOptions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(vp_TargetEventOptions_t52EAC66E7A901D1A4DD7F2B2393D9D9D5DA155CA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// UnityEngine.UI.CoroutineTween.ColorTween
struct  ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 
{
public:
	// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenCallback UnityEngine.UI.CoroutineTween.ColorTween::m_Target
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_StartColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_TargetColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single UnityEngine.UI.CoroutineTween.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_Target_0)); }
	inline ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_StartColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_TargetColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228_marshaled_pinvoke
{
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228_marshaled_com
{
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};

// Splitter
struct  Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// System.Int32 Splitter::arrCount
	int32_t ___arrCount_4;
	// System.Single[] Splitter::arr
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* ___arr_5;

public:
	inline static int32_t get_offset_of_arrCount_4() { return static_cast<int32_t>(offsetof(Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE, ___arrCount_4)); }
	inline int32_t get_arrCount_4() const { return ___arrCount_4; }
	inline int32_t* get_address_of_arrCount_4() { return &___arrCount_4; }
	inline void set_arrCount_4(int32_t value)
	{
		___arrCount_4 = value;
	}

	inline static int32_t get_offset_of_arr_5() { return static_cast<int32_t>(offsetof(Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE, ___arr_5)); }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* get_arr_5() const { return ___arr_5; }
	inline SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5** get_address_of_arr_5() { return &___arr_5; }
	inline void set_arr_5(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* value)
	{
		___arr_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arr_5), (void*)value);
	}
};


// System.Action`1<System.Object>
struct  Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`2<System.Object,System.Object>
struct  Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`3<System.Object,System.Object,System.Object>
struct  Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`1<System.Object>
struct  Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Object>
struct  Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`3<System.Object,System.Object,System.Object>
struct  Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`4<System.Object,System.Object,System.Object,System.Object>
struct  Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>
struct  UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>
struct  UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct  UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct  UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct  UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Object>
struct  UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct  U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07  : public RuntimeObject
{
public:
	// T UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::tweenInfo
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___tweenInfo_0;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::<elapsedTime>__0
	float ___U3CelapsedTimeU3E__0_1;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::<percentage>__1
	float ___U3CpercentageU3E__1_2;
	// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24current
	RuntimeObject * ___U24current_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24disposing
	bool ___U24disposing_4;
	// System.Int32 UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0::U24PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_tweenInfo_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___tweenInfo_0)); }
	inline ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  get_tweenInfo_0() const { return ___tweenInfo_0; }
	inline ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * get_address_of_tweenInfo_0() { return &___tweenInfo_0; }
	inline void set_tweenInfo_0(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  value)
	{
		___tweenInfo_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___tweenInfo_0))->___m_Target_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U3CelapsedTimeU3E__0_1)); }
	inline float get_U3CelapsedTimeU3E__0_1() const { return ___U3CelapsedTimeU3E__0_1; }
	inline float* get_address_of_U3CelapsedTimeU3E__0_1() { return &___U3CelapsedTimeU3E__0_1; }
	inline void set_U3CelapsedTimeU3E__0_1(float value)
	{
		___U3CelapsedTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CpercentageU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U3CpercentageU3E__1_2)); }
	inline float get_U3CpercentageU3E__1_2() const { return ___U3CpercentageU3E__1_2; }
	inline float* get_address_of_U3CpercentageU3E__1_2() { return &___U3CpercentageU3E__1_2; }
	inline void set_U3CpercentageU3E__1_2(float value)
	{
		___U3CpercentageU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U24current_3), (void*)value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};


// vp_Attempt`1_Tryer`1<System.Object,System.Object>
struct  Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF  : public MulticastDelegate_t
{
public:

public:
};


// vp_GlobalCallbackReturn`1<System.Object>
struct  vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED  : public MulticastDelegate_t
{
public:

public:
};


// vp_GlobalCallbackReturn`2<System.Object,System.Object>
struct  vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C  : public MulticastDelegate_t
{
public:

public:
};


// vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>
struct  vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F  : public MulticastDelegate_t
{
public:

public:
};


// vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>
struct  vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8  : public MulticastDelegate_t
{
public:

public:
};


// vp_GlobalCallback`1<System.Object>
struct  vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159  : public MulticastDelegate_t
{
public:

public:
};


// vp_GlobalCallback`2<System.Object,System.Object>
struct  vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8  : public MulticastDelegate_t
{
public:

public:
};


// vp_GlobalCallback`3<System.Object,System.Object,System.Object>
struct  vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD  : public MulticastDelegate_t
{
public:

public:
};


// vp_Message`1_Sender`1<System.Object,System.Object>
struct  Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59  : public MulticastDelegate_t
{
public:

public:
};


// vp_Message`2_Sender`2<System.Object,System.Object,System.Object,System.Object>
struct  Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80  : public MulticastDelegate_t
{
public:

public:
};


// vp_Value`1_Getter`1<System.Object,System.Object>
struct  Getter_1_t937722DADE2AAED1621E908646316A6E193A266B  : public MulticastDelegate_t
{
public:

public:
};


// vp_Value`1_Setter`1<System.Object,System.Object>
struct  Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// ValuesSplitter`1<System.Int32>
struct  ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895  : public Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE
{
public:
	// T[] ValuesSplitter`1::labels
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___labels_6;

public:
	inline static int32_t get_offset_of_labels_6() { return static_cast<int32_t>(offsetof(ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895, ___labels_6)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_labels_6() const { return ___labels_6; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_labels_6() { return &___labels_6; }
	inline void set_labels_6(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___labels_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___labels_6), (void*)value);
	}
};


// ValuesSplitter`1<System.Object>
struct  ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134  : public Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE
{
public:
	// T[] ValuesSplitter`1::labels
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___labels_6;

public:
	inline static int32_t get_offset_of_labels_6() { return static_cast<int32_t>(offsetof(ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134, ___labels_6)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_labels_6() const { return ___labels_6; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_labels_6() { return &___labels_6; }
	inline void set_labels_6(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___labels_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___labels_6), (void*)value);
	}
};


// ValuesSplitter`1<UnityEngine.Color>
struct  ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21  : public Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE
{
public:
	// T[] ValuesSplitter`1::labels
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* ___labels_6;

public:
	inline static int32_t get_offset_of_labels_6() { return static_cast<int32_t>(offsetof(ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21, ___labels_6)); }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* get_labels_6() const { return ___labels_6; }
	inline ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399** get_address_of_labels_6() { return &___labels_6; }
	inline void set_labels_6(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* value)
	{
		___labels_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___labels_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  m_Items[1];

public:
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FieldInfo_t * m_Items[1];

public:
	inline FieldInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline FieldInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MethodInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m56FBD260A4D190AD833E9B108B1E80A574AA62C4_gshared (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m786A1D72D4E499C0776742D3B2921F47E3A54545_gshared (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ColorTween_ValidTarget_m847E9D6C8B97F1C9039BF80AD69EEFC74C989079 (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ColorTween_get_ignoreTimeScale_mD27F5C7D70D340DBDFAE972BBE3857A26E29747A (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2 (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ColorTween_get_duration_mE4A9B4FFAB11CCF25EAACF5777991AB6749020B0 (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B (float p0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorTween_TweenValue_mF6B10FEA49EB758AD37D95A7DD577D6AA9C32110 (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, float ___floatPercentage0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::ValidTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FloatTween_ValidTarget_m921F88A58CCB09A4D55DBB714F3538677363FAE6 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::get_ignoreTimeScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FloatTween_get_ignoreTimeScale_mCA3DA664CF6F78735BF3ED6301900FB849B49C34 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_duration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float FloatTween_get_duration_mBC42C5053BCB1A1315430E3E21ECE1597BB0B314 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::TweenValue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatTween_TweenValue_m4ADF9CF3356268D7AD1CFF358BA252F1E52226F3 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, float ___floatPercentage0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 Splitter::GetIndexOfChance(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Splitter_GetIndexOfChance_mE527221462A0EEAD95827B03679E0FE5C25877F3 (Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE * __this, float ___f0, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384 (float p0, float p1, const RuntimeMethod* method);
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m2B1556CFBBD088D285A0B0EA280F82D3A4344DC3 (float* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* String_Split_m13262358217AD2C119FD1B9733C3C0289D608512 (String_t* __this, CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* p0, const RuntimeMethod* method);
// System.Double System.Convert::ToDouble(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Convert_ToDouble_mAB1A9AC95EC05C079529253797362E319C342A6F (String_t* p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8 (const RuntimeMethod* method);
// UnityEngine.Vector2 Splitter::GetRange(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Splitter_GetRange_m72C3859C7FC21EEBAEACE317B549A496023C48D7 (Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE * __this, int32_t ___elementIndex0, const RuntimeMethod* method);
// System.Void Splitter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Splitter__ctor_mFA7A1B4FAE29B30C71E7FB447DB1187F9E57654D (Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE * __this, const RuntimeMethod* method);
// System.Void vp_Activity::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Activity__ctor_mB9C79F12E59E862CCA79D2ECBEECF19B0C4F484B (vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void vp_Attempt::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Attempt__ctor_m3118443A4EB35B402D253E8FAE20B39A99096F26 (vp_Attempt_t28537319AB5D1247932A58A0DFE65B8353FEA615 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Type vp_Event::get_Type()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, const RuntimeMethod* method);
// System.Reflection.FieldInfo System.Type::GetField(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FieldInfo_t * Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5 (Type_t * __this, String_t* p0, const RuntimeMethod* method);
// System.Void vp_Event::StoreInvokerFieldNames()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  p0, const RuntimeMethod* method);
// System.Reflection.MethodInfo vp_Event::GetStaticGenericMethod(System.Type,System.String,System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MethodInfo_t * vp_Event_GetStaticGenericMethod_mC5A9BB11EAE19B27D7A4F37BEDB9C0C38BCA98F7 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, Type_t * ___e0, String_t* ___name1, Type_t * ___parameterType2, Type_t * ___returnType3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor()
inline void Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62 (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *, const RuntimeMethod*))Dictionary_2__ctor_m56FBD260A4D190AD833E9B108B1E80A574AA62C4_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1)
inline void Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76 (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * __this, String_t* p0, int32_t p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_Add_m786A1D72D4E499C0776742D3B2921F47E3A54545_gshared)(__this, p0, p1, method);
}
// System.Boolean System.Reflection.MethodInfo::op_Inequality(System.Reflection.MethodInfo,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237 (MethodInfo_t * p0, MethodInfo_t * p1, const RuntimeMethod* method);
// System.Type vp_Event::MakeGenericType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void vp_Event::SetFieldToLocalMethod(System.Reflection.FieldInfo,System.Reflection.MethodInfo,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, FieldInfo_t * ___field0, MethodInfo_t * ___method1, Type_t * ___type2, const RuntimeMethod* method);
// System.Reflection.MethodInfo System.Delegate::get_Method()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MethodInfo_t * Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048 (Delegate_t * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E (String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String vp_Event::get_EventName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* vp_Event_get_EventName_m62614B846C0E4BB14613C4FBFC79CB8DABAF4E42 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void vp_Event::SetFieldToExternalMethod(System.Object,System.Reflection.FieldInfo,System.String,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_SetFieldToExternalMethod_mA9E3D6BBEDCBA637A2FA457E5DC0820B5BF3D9FE (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, RuntimeObject * ___target0, FieldInfo_t * ___field1, String_t* ___method2, Type_t * ___type3, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229 (String_t* p0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* p0, const RuntimeMethod* method);
// vp_GlobalEventInternal/UnregisterException vp_GlobalEventInternal::ShowUnregisterException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA * vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD (String_t* ___name0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<vp_GlobalCallbackReturn`1<System.Object>>::get_Current()
inline vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * Enumerator_get_Current_mA1EA693ACC6D3A81C670C07B7F2B7BDA29139AF0 (Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 * __this, const RuntimeMethod* method)
{
	return ((  vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * (*) (Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<vp_GlobalCallbackReturn`1<System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_m4EC116B65FCCFBCB40BC5D26C067FD299BC0A9FE (Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// vp_GlobalEventInternal/SendException vp_GlobalEventInternal::ShowSendException(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A * vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146 (String_t* ___name0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<vp_GlobalCallbackReturn`2<System.Object,System.Object>>::get_Current()
inline vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * Enumerator_get_Current_m45346515CF29AD0B024D593BB6EEA63F1556037B (Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC * __this, const RuntimeMethod* method)
{
	return ((  vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * (*) (Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<vp_GlobalCallbackReturn`2<System.Object,System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_mDE1913E33F8F8E14045089C9242E05134FDA5777 (Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>>::get_Current()
inline vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * Enumerator_get_Current_m318CF4304C8F59DFBCE9FE716FB9871D81671A1F (Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 * __this, const RuntimeMethod* method)
{
	return ((  vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * (*) (Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_m80822B43BFD7010343E4AE303D6D9D78A1BEE78A (Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>>::get_Current()
inline vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * Enumerator_get_Current_m81131B42FF894AF40643860B33F53767A087981E (Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 * __this, const RuntimeMethod* method)
{
	return ((  vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * (*) (Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_m584975834A266F016DBEDEBDEBD0CE7FA81E8CEF (Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<vp_GlobalCallback`1<System.Object>>::get_Current()
inline vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * Enumerator_get_Current_mBF8BBA4FF63BA9CA90910DDCB11B2A21BFC25767 (Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 * __this, const RuntimeMethod* method)
{
	return ((  vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * (*) (Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<vp_GlobalCallback`1<System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_mAA65D37DCA9DD3E0CD391F63F380C140D1307A65 (Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<vp_GlobalCallback`2<System.Object,System.Object>>::get_Current()
inline vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * Enumerator_get_Current_m0AE5488682304FE7CE9D3A7A11F489DEF51B532C (Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A * __this, const RuntimeMethod* method)
{
	return ((  vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * (*) (Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<vp_GlobalCallback`2<System.Object,System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_mD72AB98A229985796153A06794D713352EE64C8D (Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<vp_GlobalCallback`3<System.Object,System.Object,System.Object>>::get_Current()
inline vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * Enumerator_get_Current_m825CC53DDF810CFE7D2ACB6ED12604CCE71E3FB9 (Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE * __this, const RuntimeMethod* method)
{
	return ((  vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * (*) (Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<vp_GlobalCallback`3<System.Object,System.Object,System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_m4948DA95F24F1481F1865E9A73E30BE584D858F5 (Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void vp_Message::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message__ctor_m940941685CF5B3BC416C682A02E2B3B9FAE373D8 (vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void vp_Event::AddExternalMethodToField(System.Object,System.Reflection.FieldInfo,System.String,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_AddExternalMethodToField_m167005BC363AE73C25FF8679197A5C8E13EF63E7 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, RuntimeObject * ___target0, FieldInfo_t * ___field1, String_t* ___method2, Type_t * ___type3, const RuntimeMethod* method);
// System.Void vp_Event::Refresh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, const RuntimeMethod* method);
// System.Void vp_Event::RemoveExternalMethodFromField(System.Object,System.Reflection.FieldInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_RemoveExternalMethodFromField_mE1961E2F41C102B5B7FDC0C24539E47F7B5D5041 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, RuntimeObject * ___target0, FieldInfo_t * ___field1, const RuntimeMethod* method);
// System.Void vp_TargetEventHandler::Register(System.Object,System.String,System.Delegate,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA (RuntimeObject * ___target0, String_t* ___eventName1, Delegate_t * ___callback2, int32_t ___dictionary3, const RuntimeMethod* method);
// System.Void vp_TargetEventHandler::Unregister(System.Object,System.String,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC (RuntimeObject * ___target0, String_t* ___eventName1, Delegate_t * ___callback2, const RuntimeMethod* method);
// System.Void vp_TargetEventHandler::Unregister(UnityEngine.Component)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventHandler_Unregister_m8455936D2115A5A13E469F8FFBF9BA879F2587E6 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, const RuntimeMethod* method);
// System.Delegate vp_TargetEventHandler::GetCallback(System.Object,System.String,System.Boolean,System.Int32,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603 (RuntimeObject * ___target0, String_t* ___eventName1, bool ___upwards2, int32_t ___d3, int32_t ___options4, const RuntimeMethod* method);
// System.Void vp_TargetEventHandler::OnNoReceiver(System.String,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F (String_t* ___eventName0, int32_t ___options1, const RuntimeMethod* method);
// System.Void vp_Event::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event__ctor_mBAC4F17FDA57FE7F9766E60433C5A10BF198D4E7 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, String_t* ___name0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0__ctor_m7C7663DF2836474A845DD182E23E6A3DD3D6BCEC_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartU3Ec__Iterator0_MoveNext_m17A6A3B6131EFE960C4DA7784DDE816250347E99_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m17A6A3B6131EFE960C4DA7784DDE816250347E99_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d5;
			}
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_2 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		bool L_3 = ColorTween_ValidTarget_m847E9D6C8B97F1C9039BF80AD69EEFC74C989079((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_010f;
	}

IL_003d:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00d6;
	}

IL_004d:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_5 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		bool L_6 = ColorTween_get_ignoreTimeScale_mD27F5C7D70D340DBDFAE972BBE3857A26E29747A((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)(__this));
			goto IL_0075;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2(/*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		float L_8 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)il2cpp_codegen_add((float)G_B8_1, (float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_10 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		float L_11 = ColorTween_get_duration_mE4A9B4FFAB11CCF25EAACF5777991AB6749020B0((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B((float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_13 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		ColorTween_TweenValue_mF6B10FEA49EB758AD37D95A7DD577D6AA9C32110((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_17 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		float L_18 = ColorTween_get_duration_mE4A9B4FFAB11CCF25EAACF5777991AB6749020B0((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_19 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_0();
		ColorTween_TweenValue_mF6B10FEA49EB758AD37D95A7DD577D6AA9C32110((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m10DD8AC5B8A3A0C73F0A8D9699C5D5220FECC227_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m680D4804588601644224E48606E6D66465689171_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0_Dispose_mF26FE6FE2B6669E490946F8EF355DC0CCAC860F7_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_gshared (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CStartU3Ec__Iterator0_Reset_m439E251EE39AF2851415914D8F11867A65D9E63F_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0__ctor_m8B54A1BBDA9AFF06C9286D672F19BDB1001C320D_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartU3Ec__Iterator0_MoveNext_mB6BE65FBF43A13162E304F0F012BAA539F3D0C9F_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_mB6BE65FBF43A13162E304F0F012BAA539F3D0C9F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	float G_B7_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * G_B7_1 = NULL;
	float G_B6_0 = 0.0f;
	U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * G_B8_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_5();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_5((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00d5;
			}
		}
	}
	{
		goto IL_010f;
	}

IL_0021:
	{
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_2 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		bool L_3 = FloatTween_ValidTarget_m921F88A58CCB09A4D55DBB714F3538677363FAE6((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_010f;
	}

IL_003d:
	{
		__this->set_U3CelapsedTimeU3E__0_1((0.0f));
		goto IL_00d6;
	}

IL_004d:
	{
		float L_4 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_5 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		bool L_6 = FloatTween_get_ignoreTimeScale_mCA3DA664CF6F78735BF3ED6301900FB849B49C34((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_5, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		G_B6_1 = ((U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)(__this));
		if (!L_6)
		{
			G_B7_0 = L_4;
			G_B7_1 = ((U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)(__this));
			goto IL_0075;
		}
	}
	{
		float L_7 = Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2(/*hidden argument*/NULL);
		G_B8_0 = L_7;
		G_B8_1 = G_B6_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)(G_B6_1));
		goto IL_007a;
	}

IL_0075:
	{
		float L_8 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		G_B8_0 = L_8;
		G_B8_1 = G_B7_0;
		G_B8_2 = ((U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)(G_B7_1));
	}

IL_007a:
	{
		NullCheck(G_B8_2);
		G_B8_2->set_U3CelapsedTimeU3E__0_1(((float)il2cpp_codegen_add((float)G_B8_1, (float)G_B8_0)));
		float L_9 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_10 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		float L_11 = FloatTween_get_duration_mBC42C5053BCB1A1315430E3E21ECE1597BB0B314((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_12 = Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B((float)((float)((float)L_9/(float)L_11)), /*hidden argument*/NULL);
		__this->set_U3CpercentageU3E__1_2(L_12);
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_13 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		float L_14 = (float)__this->get_U3CpercentageU3E__1_2();
		FloatTween_TweenValue_m4ADF9CF3356268D7AD1CFF358BA252F1E52226F3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_13, (float)L_14, /*hidden argument*/NULL);
		__this->set_U24current_3(NULL);
		bool L_15 = (bool)__this->get_U24disposing_4();
		if (L_15)
		{
			goto IL_00d0;
		}
	}
	{
		__this->set_U24PC_5(1);
	}

IL_00d0:
	{
		goto IL_0111;
	}

IL_00d5:
	{
	}

IL_00d6:
	{
		float L_16 = (float)__this->get_U3CelapsedTimeU3E__0_1();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_17 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		float L_18 = FloatTween_get_duration_mBC42C5053BCB1A1315430E3E21ECE1597BB0B314((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_004d;
		}
	}
	{
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_19 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_0();
		FloatTween_TweenValue_m4ADF9CF3356268D7AD1CFF358BA252F1E52226F3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		__this->set_U24PC_5((-1));
	}

IL_010f:
	{
		return (bool)0;
	}

IL_0111:
	{
		return (bool)1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m528DCE76B49785C679B66AAB4D42407B127A9B38_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m38831D13E816549A5D51699B9594609F833670F9_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U24current_3();
		V_0 = (RuntimeObject *)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		RuntimeObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0_Dispose_m5D0C71B726544207A28FAE9F57B9D7401512C3C8_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_4((bool)1);
		__this->set_U24PC_5((-1));
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.FloatTween>::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_gshared (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CStartU3Ec__Iterator0_Reset_m7703121C2007A9FF4EC664BC758BC00DF683B629_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1__ctor_m2D6244FBF370723CF73C376DC4D2E44D3EEEE290_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TweenRunner_1_Start_m756F514128966B0D57875B80DB12696B2DB60B97_gshared (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___tweenInfo0, const RuntimeMethod* method)
{
	U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * L_0 = (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 *)L_0;
		U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * L_1 = V_0;
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_tE6C906B4CE3463E1E9016DA76194239D06531E07 * L_3 = V_0;
		V_1 = (RuntimeObject*)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_Init_mA29C09ADC3EB6959A0F1572D48D84170443B670E_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___coroutineContainer0, const RuntimeMethod* method)
{
	{
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_StartTween_mE0CB96AF945209ABC26F2AA9899CB9794A64D92D_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_mE0CB96AF945209ABC26F2AA9899CB9794A64D92D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C((Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568((RuntimeObject *)_stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0022:
	{
		NullCheck((TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *)__this);
		((  void (*) (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_2 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		ColorTween_TweenValue_mF6B10FEA49EB758AD37D95A7DD577D6AA9C32110((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0055:
	{
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  L_5 = ___info0;
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_7 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7, (RuntimeObject*)L_8, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StopTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_StopTween_m861C40714D7A8C4B7EF8A7CC781B06C600877A5F_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_1 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1);
		MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		__this->set_m_Tween_1((RuntimeObject*)NULL);
	}

IL_0026:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1__ctor_m63EA04E5FC2204138452DCD5D7C9A7918B7AD5E6_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TweenRunner_1_Start_m2CFC7722714F904EC8123AA4B7DCB974E466FD83_gshared (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___tweenInfo0, const RuntimeMethod* method)
{
	U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * L_0 = (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_0 = (U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 *)L_0;
		U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * L_1 = V_0;
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_0(L_2);
		U3CStartU3Ec__Iterator0_t468FE95258205EFB6F39EECD7F7D00F74B696286 * L_3 = V_0;
		V_1 = (RuntimeObject*)L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RuntimeObject* L_4 = V_1;
		return L_4;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_Init_m39096EAEB1B5BFC0E2FEDF4A946593353F8981CC_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___coroutineContainer0, const RuntimeMethod* method)
{
	{
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_StartTween_m8637A776CD96BAB0EDC8A8FA7479127E2BEC92C4_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m8637A776CD96BAB0EDC8A8FA7479127E2BEC92C4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C((Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568((RuntimeObject *)_stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9, /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0022:
	{
		NullCheck((TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *)__this);
		((  void (*) (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_2 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0055;
		}
	}
	{
		FloatTween_TweenValue_m4ADF9CF3356268D7AD1CFF358BA252F1E52226F3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		goto IL_0073;
	}

IL_0055:
	{
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  L_5 = ___info0;
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_7 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7, (RuntimeObject*)L_8, /*hidden argument*/NULL);
	}

IL_0073:
	{
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StopTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_StopTween_m9EDE8CC585AD166D4520BE8B374CA2169CDD0E8E_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_1 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1);
		MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		__this->set_m_Tween_1((RuntimeObject*)NULL);
	}

IL_0026:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_mD973571E32F104549B24EC4C48FE9C290C6223BB_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_0 = ___l0;
		NullCheck((List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_0);
		((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ListPool_1_Get_mAFB1F76A280309F30C0897B1FBABCC37A783344C_gshared (const RuntimeMethod* method)
{
	List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * L_0 = ((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)L_0);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_1 = ((  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * (*) (ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m34FF8B9650A82F8B5ECF477604AEF059FB85A153_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * L_0 = ((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)L_0);
		((  void (*) (ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *, List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)L_0, (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_mF27548A30BD87F6F4CE445FC8E518043FB0B295E_gshared (const RuntimeMethod* method)
{
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * L_0 = ((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * L_1 = (UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 * L_2 = ((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A * L_3 = (ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tC08E18CA4686E07104774795FF479D68B6B2889A *, UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *, UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *)G_B2_0, (UnityAction_1_tA99D005A9C291926F1FC4F9D3A8FABD18D895689 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tFA23B363858EAC800B614A18D05C359F72028407_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m740111DCF3FFCAA14F5FB747FAAF0ED39C11D126_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = ___l0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ListPool_1_Get_m4C328048C1479EE1450837A0CF1BF5F18FF77C88_gshared (const RuntimeMethod* method)
{
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * L_0 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)L_0);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = ((  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * (*) (ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m2F58A45DA9F2BBE95A654A426FF72F0CA75D5B7E_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * L_0 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)L_0);
		((  void (*) (ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)L_0, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_mF042EAEBF32922CBFC61DBE6E1DA0BF06A9DBC26_gshared (const RuntimeMethod* method)
{
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * L_0 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * L_1 = (UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 * L_2 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B * L_3 = (ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tDADAC46065A7A2E2176ACF5FA7C3142B1AF9517B *, UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *, UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *)G_B2_0, (UnityAction_1_t8AD8F8E44992547CC00157160617BE8482809363 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m9F04DC60F78A36A41D42EEDD9688FDD685667DB6_gshared (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_0 = ___l0;
		NullCheck((List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_0);
		((  void (*) (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ListPool_1_Get_m1549C4AC8324D6BBB9CAA80B70EE13A6AC0617B7_gshared (const RuntimeMethod* method)
{
	List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * L_0 = ((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)L_0);
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_1 = ((  List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * (*) (ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m52E37BEE7C08F871746F8E3B28556641EC754324_gshared (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * L_0 = ((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)L_0);
		((  void (*) (ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *, List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)L_0, (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m7AEC73CEB51505546BA51E71698753674FE8781B_gshared (const RuntimeMethod* method)
{
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * L_0 = ((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * L_1 = (UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 * L_2 = ((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 * L_3 = (ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t6E42C9408E003E775EC7139A3F1EFC1346440D07 *, UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *, UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *)G_B2_0, (UnityAction_1_t7F49A8FC841AC905861BD019CAAAE81F5DE4EEF4 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tBA324F10FC7E73FB9F71457FFE143CD03160D463_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m2208C1BA3B3E3790C5EE59E69407637C41F10844_gshared (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_0 = ___l0;
		NullCheck((List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_0);
		((  void (*) (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ListPool_1_Get_m3D24B0F028698E7FA9EAB826563501BFF986BBFD_gshared (const RuntimeMethod* method)
{
	List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * L_0 = ((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)L_0);
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_1 = ((  List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * (*) (ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m4FBBEE3FE54B6B374CD3F3CA7586029E579FAD6C_gshared (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * L_0 = ((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)L_0);
		((  void (*) (ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *, List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)L_0, (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m96D7F2BB0CC611C7E4C5828B2E10AE50192A4FCE_gshared (const RuntimeMethod* method)
{
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * L_0 = ((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * L_1 = (UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 * L_2 = ((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 * L_3 = (ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tB641A4FECBF1E01BBA0C252F01EDE98D41033CF5 *, UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *, UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *)G_B2_0, (UnityAction_1_t7B2376CCD306AEB0D24B3479F62CE812058041D0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tAFD0E4E019381064EBBF5E8710B0F1E0B85EBB56_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m6432D570E705BF537C4FB85407A31C9AAD6A99E6_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___l0, const RuntimeMethod* method)
{
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_0 = ___l0;
		NullCheck((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_0);
		((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ListPool_1_Get_m4322F57FCB5B11C10CAEDE17A609B9E29401797F_gshared (const RuntimeMethod* method)
{
	List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * L_0 = ((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)L_0);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_1 = ((  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * (*) (ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m3339B7710487E883FD2DB198643084787D08BBDD_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * L_0 = ((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)L_0);
		((  void (*) (ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *, List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)L_0, (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m361000F7CAFBC65FFD4FC5D55BA85108853E0376_gshared (const RuntimeMethod* method)
{
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * L_0 = ((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * L_1 = (UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 * L_2 = ((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 * L_3 = (ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t77D90EC466D5DC3CD8703898D0D3206B7D320D49 *, UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *, UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *)G_B2_0, (UnityAction_1_t5070210D9B8F86C2EDBB6772A8295FAD8FC32821 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tAB2044BEC36628D346141AEA4743A824A6FB688C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m8B0E6BEA4C6F4445E8F5BADD3C577054A3445893_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ___l0;
		NullCheck((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_0);
		((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ListPool_1_Get_m0DA6F4FA9B55233F3BCC582E1B5D1B82048B72DC_gshared (const RuntimeMethod* method)
{
	List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * L_0 = ((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)L_0);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = ((  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * (*) (ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m427896919DD66BB0E8CF7274C8AE76036D36D3BD_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * L_0 = ((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)L_0);
		((  void (*) (ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)L_0, (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m4BEC87EBA6EA67DFC457FDB45282F75A64C6C347_gshared (const RuntimeMethod* method)
{
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * L_0 = ((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * L_1 = (UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E * L_2 = ((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 * L_3 = (ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t9CC17CF511664D2F103A4C4F73C9BD8820B88DF2 *, UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *, UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *)G_B2_0, (UnityAction_1_t68BCED570CE215DF78AAA225E29C0959286C1A0E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tC0119DB1C2EC9C29F424EC953509E2CDC3995059_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m0CCFC711C25B7AAC542D2ABBA0E1B4BFE8D037EA_gshared (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___l0, const RuntimeMethod* method)
{
	{
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_0 = ___l0;
		NullCheck((List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_0);
		((  void (*) (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ListPool_1_Get_mECE6D9ED10FB4EEF28A6BE1F9B445CEE4312A937_gshared (const RuntimeMethod* method)
{
	List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * L_0 = ((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)L_0);
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_1 = ((  List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * (*) (ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_1;
		goto IL_0011;
	}

IL_0011:
	{
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m1252D062655820C50CAC05E08676836EBC1A7BA0_gshared (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___toRelease0, const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * L_0 = ((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)L_0);
		((  void (*) (ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *, List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)L_0, (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_mFC21D520E6A974F00E7D85540B0973269E58626A_gshared (const RuntimeMethod* method)
{
	RuntimeObject * G_B2_0 = NULL;
	RuntimeObject * G_B1_0 = NULL;
	{
		UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * L_0 = ((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		G_B1_0 = NULL;
		if (L_0)
		{
			G_B2_0 = NULL;
			goto IL_0019;
		}
	}
	{
		UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * L_1 = (UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_1, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_U3CU3Ef__mgU24cache0_1(L_1);
		G_B2_0 = G_B1_0;
	}

IL_0019:
	{
		UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB * L_2 = ((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_U3CU3Ef__mgU24cache0_1();
		ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 * L_3 = (ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t89359398AF2898F35015A1938357AD5AC70B2C39 *, UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *, UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_3, (UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *)G_B2_0, (UnityAction_1_t3C41FEE79AC7F1373BDB9F3C424BB5EEC7365BEB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tD0E00B3B8CFB855678B750B02E1ACEB57D4FC67A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_3);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPool_1__ctor_mE14E9596DD21AFBA47727BDC5226C7F24FA234C7_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___actionOnGet0, UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___actionOnRelease1, const RuntimeMethod* method)
{
	{
		Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * L_0 = (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_m_Stack_0(L_0);
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_1 = ___actionOnGet0;
		__this->set_m_ActionOnGet_1(L_1);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_2 = ___actionOnRelease1;
		__this->set_m_ActionOnRelease_2(L_2);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countAll_m2336F21CA9AFFCBF50463DD1836B14E663836D58_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_U3CcountAllU3Ek__BackingField_3();
		V_0 = (int32_t)L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPool_1_set_countAll_mF68D62D98B4F6AFACAC85C02E4DB858DA7196F64_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CcountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countActive_m12221F42D2CD25F418A1746CFEE20C65A630B6F1_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		NullCheck((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this);
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		NullCheck((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this);
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countInactive_mF6EBCB20814CAE2E818F846689640BF4485E810F_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * L_0 = (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		V_0 = (int32_t)L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ObjectPool_1_Get_m82BA8AF24CE96731493EC3C52B1218DB2C3C934F_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	{
		Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * L_0 = (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_0 = (RuntimeObject *)L_2;
		NullCheck((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this);
		int32_t L_3 = ((  int32_t (*) (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		NullCheck((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this);
		((  void (*) (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD *)__this, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		goto IL_003a;
	}

IL_002c:
	{
		Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * L_4 = (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_4);
		RuntimeObject * L_5 = ((  RuntimeObject * (*) (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		V_0 = (RuntimeObject *)L_5;
	}

IL_003a:
	{
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_6 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnGet_1();
		if (!L_6)
		{
			goto IL_0051;
		}
	}
	{
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_7 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnGet_1();
		RuntimeObject * L_8 = V_0;
		NullCheck((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_7);
		((  void (*) (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
	}

IL_0051:
	{
		RuntimeObject * L_9 = V_0;
		V_1 = (RuntimeObject *)L_9;
		goto IL_0058;
	}

IL_0058:
	{
		RuntimeObject * L_10 = V_1;
		return L_10;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPool_1_Release_mEF76D0678288FA4D4D8D81C57B3FEBF7AE87BD74_gshared (ObjectPool_1_t642A3D701C6162F913D9252AB3E5BEB96161F6BD * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_mEF76D0678288FA4D4D8D81C57B3FEBF7AE87BD74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * L_0 = (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * L_2 = (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_2);
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		RuntimeObject * L_4 = ___element0;
		bool L_5 = il2cpp_codegen_object_reference_equals((RuntimeObject *)L_3, (RuntimeObject *)L_4);
		if (!L_5)
		{
			goto IL_003c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29((RuntimeObject *)_stringLiteral04231B44477132B3DBEFE7768A921AE5A13A00FC, /*hidden argument*/NULL);
	}

IL_003c:
	{
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_6 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnRelease_2();
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_7 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnRelease_2();
		RuntimeObject * L_8 = ___element0;
		NullCheck((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_7);
		((  void (*) (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_7, (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
	}

IL_0053:
	{
		Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 * L_9 = (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)__this->get_m_Stack_0();
		RuntimeObject * L_10 = ___element0;
		NullCheck((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_9);
		((  void (*) (Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((Stack_1_t4A8378BFCCA917C44CD055D0B4DB470EB7FEC275 *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// T ValuesSplitter`1<System.Int32>::GetValueOfChance(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ValuesSplitter_1_GetValueOfChance_m78B0119C752F299097517DD6AC1511F371749931_gshared (ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 * __this, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		int32_t L_1 = Splitter_GetIndexOfChance_mE527221462A0EEAD95827B03679E0FE5C25877F3((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (float)L_0, /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_2 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)__this->get_labels_6();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// T ValuesSplitter`1<System.Int32>::GetRandomValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ValuesSplitter_1_GetRandomValue_m91553C92AD6559D203C696325B36D2AE7E655C06_gshared (ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((float)(0.0f), (float)(1.0f), /*hidden argument*/NULL);
		NullCheck((ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 *)__this);
		int32_t L_1 = ((  int32_t (*) (ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return L_1;
	}
}
// System.String ValuesSplitter`1<System.Int32>::get_SplitterValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValuesSplitter_1_get_SplitterValues_mF7382AF214D51C90BF442ECEB0E77DBC047F2E7A_gshared (ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_get_SplitterValues_mF7382AF214D51C90BF442ECEB0E77DBC047F2E7A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* G_B3_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	{
		V_0 = (String_t*)_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		V_1 = (int32_t)0;
		goto IL_0046;
	}

IL_000a:
	{
		String_t* L_0 = V_0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		String_t* L_3 = Single_ToString_m2B1556CFBBD088D285A0B0EA280F82D3A4344DC3((float*)(float*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), /*hidden argument*/NULL);
		String_t* L_4 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_0, (String_t*)L_3, /*hidden argument*/NULL);
		V_0 = (String_t*)L_4;
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_7 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_7);
		G_B2_0 = L_5;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))), (int32_t)1)))))
		{
			G_B3_0 = L_5;
			goto IL_0037;
		}
	}
	{
		G_B4_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B4_1 = G_B2_0;
		goto IL_003c;
	}

IL_0037:
	{
		G_B4_0 = _stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9;
		G_B4_1 = G_B3_0;
	}

IL_003c:
	{
		String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)G_B4_1, (String_t*)G_B4_0, /*hidden argument*/NULL);
		V_0 = (String_t*)L_8;
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_10 = V_1;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_11 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))))
		{
			goto IL_000a;
		}
	}
	{
		String_t* L_12 = V_0;
		return L_12;
	}
}
// System.Void ValuesSplitter`1<System.Int32>::set_SplitterValues(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1_set_SplitterValues_mA5EA6991C9FC68B9DAA394CFF6DC0C40C5C9898F_gshared (ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_set_SplitterValues_mA5EA6991C9FC68B9DAA394CFF6DC0C40C5C9898F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_2 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck((String_t*)L_0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = String_Split_m13262358217AD2C119FD1B9733C3C0289D608512((String_t*)L_0, (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)L_2, /*hidden argument*/NULL);
		V_0 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)L_3;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = V_0;
		NullCheck(L_4);
		if (!(((RuntimeArray*)L_4)->max_length))
		{
			goto IL_003e;
		}
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = V_0;
		NullCheck(L_5);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		VirtActionInvoker1< int32_t >::Invoke(4 /* System.Void Splitter::UpdateSize(System.Int32) */, (Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length)))));
		V_1 = (int32_t)0;
		goto IL_0038;
	}

IL_0023:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_6 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_7 = V_1;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var);
		double L_12 = Convert_ToDouble_mAB1A9AC95EC05C079529253797362E319C342A6F((String_t*)L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)(((float)((float)L_12))));
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0038:
	{
		int32_t L_14 = V_1;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = V_0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length)))))))
		{
			goto IL_0023;
		}
	}

IL_003e:
	{
		return;
	}
}
// System.Void ValuesSplitter`1<System.Int32>::UpdateSize(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1_UpdateSize_mF42B8F5F1EE0EE49971226E1B85A8C7AA47060BD_gshared (ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 * __this, int32_t ___newSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_UpdateSize_mF42B8F5F1EE0EE49971226E1B85A8C7AA47060BD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* V_2 = NULL;
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	int32_t G_B8_0 = 0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  G_B15_0;
	memset((&G_B15_0), 0, sizeof(G_B15_0));
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_0);
		if ((((RuntimeArray*)L_0)->max_length))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_1 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_2 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)L_1);
		((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->set_arr_5(L_2);
		int32_t L_3 = ___newSize0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_4 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		__this->set_labels_6(L_4);
		int32_t L_5 = ___newSize0;
		V_0 = (float)((float)((float)(1.0f)/(float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)))))));
		V_1 = (int32_t)0;
		goto IL_0044;
	}

IL_0032:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_6 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_7 = V_1;
		float L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)((float)il2cpp_codegen_multiply((float)L_8, (float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1))))))));
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = ___newSize0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0049:
	{
		int32_t L_13 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_14 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)L_13);
		V_2 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)L_14;
		int32_t L_15 = ___newSize0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_16 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1)));
		V_3 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)L_16;
		int32_t L_17 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_18 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_18);
		if ((((int32_t)L_17) > ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length)))))))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_19 = ___newSize0;
		G_B8_0 = L_19;
		goto IL_006f;
	}

IL_0067:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_20 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_20);
		G_B8_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))));
	}

IL_006f:
	{
		V_4 = (int32_t)G_B8_0;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_21 = V_3;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_22 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)__this->get_labels_6();
		NullCheck(L_22);
		int32_t L_23 = 0;
		int32_t L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (int32_t)L_24);
		V_5 = (int32_t)0;
		goto IL_00b5;
	}

IL_0089:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_25 = V_2;
		int32_t L_26 = V_5;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_27 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_28 = V_5;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		float L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (float)L_30);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_31 = V_3;
		int32_t L_32 = V_5;
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_33 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)__this->get_labels_6();
		int32_t L_34 = V_5;
		NullCheck(L_33);
		int32_t L_35 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
		int32_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1))), (int32_t)L_36);
		int32_t L_37 = V_5;
		V_5 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1));
	}

IL_00b5:
	{
		int32_t L_38 = V_5;
		int32_t L_39 = V_4;
		if ((((int32_t)L_38) < ((int32_t)L_39)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_40 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_41 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_41);
		if ((((int32_t)L_40) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_41)->max_length)))))))
		{
			goto IL_0134;
		}
	}
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_42 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_42);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_42)->max_length))))) >= ((int32_t)1)))
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		G_B15_0 = L_43;
		goto IL_00e6;
	}

IL_00d8:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_44 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_44);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_45 = Splitter_GetRange_m72C3859C7FC21EEBAEACE317B549A496023C48D7((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_44)->max_length)))), /*hidden argument*/NULL);
		G_B15_0 = L_45;
	}

IL_00e6:
	{
		float L_46 = (float)G_B15_0.get_x_0();
		V_6 = (float)L_46;
		float L_47 = V_6;
		int32_t L_48 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_49 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_49);
		V_7 = (float)((float)((float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_47))/(float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_48, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_49)->max_length)))))), (int32_t)1)))))));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_50 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_50);
		V_8 = (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_50)->max_length))));
		goto IL_012f;
	}

IL_0111:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_51 = V_2;
		int32_t L_52 = V_8;
		float L_53 = V_6;
		float L_54 = V_7;
		int32_t L_55 = V_8;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_56 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_56);
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(L_52), (float)((float)il2cpp_codegen_add((float)L_53, (float)((float)il2cpp_codegen_multiply((float)L_54, (float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_55, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_56)->max_length)))))), (int32_t)1))))))))));
		int32_t L_57 = V_8;
		V_8 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)1));
	}

IL_012f:
	{
		int32_t L_58 = V_8;
		int32_t L_59 = ___newSize0;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0111;
		}
	}

IL_0134:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_60 = V_2;
		((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->set_arr_5(L_60);
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_61 = V_3;
		__this->set_labels_6(L_61);
		return;
	}
}
// System.Void ValuesSplitter`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1__ctor_m342D5158B6B387D5135B905ADBCC95E60D87CEBB_gshared (ValuesSplitter_1_t0221D4D70F3779BDFB27CD26766F17F091B08895 * __this, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* L_0 = (Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)1);
		__this->set_labels_6(L_0);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		Splitter__ctor_mFA7A1B4FAE29B30C71E7FB447DB1187F9E57654D((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// T ValuesSplitter`1<System.Object>::GetValueOfChance(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ValuesSplitter_1_GetValueOfChance_m8352AB6EE092B0DA15460C3CD008C087A0A00263_gshared (ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 * __this, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		int32_t L_1 = Splitter_GetIndexOfChance_mE527221462A0EEAD95827B03679E0FE5C25877F3((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (float)L_0, /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get_labels_6();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// T ValuesSplitter`1<System.Object>::GetRandomValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ValuesSplitter_1_GetRandomValue_m8160CC92D65E9ED4035E8256F8A10B479080BE3E_gshared (ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((float)(0.0f), (float)(1.0f), /*hidden argument*/NULL);
		NullCheck((ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 *)__this);
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return L_1;
	}
}
// System.String ValuesSplitter`1<System.Object>::get_SplitterValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValuesSplitter_1_get_SplitterValues_m2A5FEFF018D4A2B26970D6161099C7C02A8603CD_gshared (ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_get_SplitterValues_m2A5FEFF018D4A2B26970D6161099C7C02A8603CD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* G_B3_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	{
		V_0 = (String_t*)_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		V_1 = (int32_t)0;
		goto IL_0046;
	}

IL_000a:
	{
		String_t* L_0 = V_0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		String_t* L_3 = Single_ToString_m2B1556CFBBD088D285A0B0EA280F82D3A4344DC3((float*)(float*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), /*hidden argument*/NULL);
		String_t* L_4 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_0, (String_t*)L_3, /*hidden argument*/NULL);
		V_0 = (String_t*)L_4;
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_7 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_7);
		G_B2_0 = L_5;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))), (int32_t)1)))))
		{
			G_B3_0 = L_5;
			goto IL_0037;
		}
	}
	{
		G_B4_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B4_1 = G_B2_0;
		goto IL_003c;
	}

IL_0037:
	{
		G_B4_0 = _stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9;
		G_B4_1 = G_B3_0;
	}

IL_003c:
	{
		String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)G_B4_1, (String_t*)G_B4_0, /*hidden argument*/NULL);
		V_0 = (String_t*)L_8;
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_10 = V_1;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_11 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))))
		{
			goto IL_000a;
		}
	}
	{
		String_t* L_12 = V_0;
		return L_12;
	}
}
// System.Void ValuesSplitter`1<System.Object>::set_SplitterValues(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1_set_SplitterValues_m1D0C7006179C11D457C4F51EDC7AE522B011BD4B_gshared (ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_set_SplitterValues_m1D0C7006179C11D457C4F51EDC7AE522B011BD4B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_2 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck((String_t*)L_0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = String_Split_m13262358217AD2C119FD1B9733C3C0289D608512((String_t*)L_0, (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)L_2, /*hidden argument*/NULL);
		V_0 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)L_3;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = V_0;
		NullCheck(L_4);
		if (!(((RuntimeArray*)L_4)->max_length))
		{
			goto IL_003e;
		}
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = V_0;
		NullCheck(L_5);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		VirtActionInvoker1< int32_t >::Invoke(4 /* System.Void Splitter::UpdateSize(System.Int32) */, (Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length)))));
		V_1 = (int32_t)0;
		goto IL_0038;
	}

IL_0023:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_6 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_7 = V_1;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var);
		double L_12 = Convert_ToDouble_mAB1A9AC95EC05C079529253797362E319C342A6F((String_t*)L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)(((float)((float)L_12))));
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0038:
	{
		int32_t L_14 = V_1;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = V_0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length)))))))
		{
			goto IL_0023;
		}
	}

IL_003e:
	{
		return;
	}
}
// System.Void ValuesSplitter`1<System.Object>::UpdateSize(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1_UpdateSize_m88288884CC9DB6CF2F34B61CA6E3403BB201C193_gshared (ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 * __this, int32_t ___newSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_UpdateSize_m88288884CC9DB6CF2F34B61CA6E3403BB201C193_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* V_2 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	int32_t G_B8_0 = 0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  G_B15_0;
	memset((&G_B15_0), 0, sizeof(G_B15_0));
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_0);
		if ((((RuntimeArray*)L_0)->max_length))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_1 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_2 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)L_1);
		((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->set_arr_5(L_2);
		int32_t L_3 = ___newSize0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		__this->set_labels_6(L_4);
		int32_t L_5 = ___newSize0;
		V_0 = (float)((float)((float)(1.0f)/(float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)))))));
		V_1 = (int32_t)0;
		goto IL_0044;
	}

IL_0032:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_6 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_7 = V_1;
		float L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)((float)il2cpp_codegen_multiply((float)L_8, (float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1))))))));
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = ___newSize0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0049:
	{
		int32_t L_13 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_14 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)L_13);
		V_2 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)L_14;
		int32_t L_15 = ___newSize0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_16 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1)));
		V_3 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_16;
		int32_t L_17 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_18 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_18);
		if ((((int32_t)L_17) > ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length)))))))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_19 = ___newSize0;
		G_B8_0 = L_19;
		goto IL_006f;
	}

IL_0067:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_20 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_20);
		G_B8_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))));
	}

IL_006f:
	{
		V_4 = (int32_t)G_B8_0;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_21 = V_3;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_22 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get_labels_6();
		NullCheck(L_22);
		int32_t L_23 = 0;
		RuntimeObject * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_24);
		V_5 = (int32_t)0;
		goto IL_00b5;
	}

IL_0089:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_25 = V_2;
		int32_t L_26 = V_5;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_27 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_28 = V_5;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		float L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (float)L_30);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_31 = V_3;
		int32_t L_32 = V_5;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_33 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get_labels_6();
		int32_t L_34 = V_5;
		NullCheck(L_33);
		int32_t L_35 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
		RuntimeObject * L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1))), (RuntimeObject *)L_36);
		int32_t L_37 = V_5;
		V_5 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1));
	}

IL_00b5:
	{
		int32_t L_38 = V_5;
		int32_t L_39 = V_4;
		if ((((int32_t)L_38) < ((int32_t)L_39)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_40 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_41 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_41);
		if ((((int32_t)L_40) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_41)->max_length)))))))
		{
			goto IL_0134;
		}
	}
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_42 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_42);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_42)->max_length))))) >= ((int32_t)1)))
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		G_B15_0 = L_43;
		goto IL_00e6;
	}

IL_00d8:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_44 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_44);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_45 = Splitter_GetRange_m72C3859C7FC21EEBAEACE317B549A496023C48D7((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_44)->max_length)))), /*hidden argument*/NULL);
		G_B15_0 = L_45;
	}

IL_00e6:
	{
		float L_46 = (float)G_B15_0.get_x_0();
		V_6 = (float)L_46;
		float L_47 = V_6;
		int32_t L_48 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_49 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_49);
		V_7 = (float)((float)((float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_47))/(float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_48, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_49)->max_length)))))), (int32_t)1)))))));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_50 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_50);
		V_8 = (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_50)->max_length))));
		goto IL_012f;
	}

IL_0111:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_51 = V_2;
		int32_t L_52 = V_8;
		float L_53 = V_6;
		float L_54 = V_7;
		int32_t L_55 = V_8;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_56 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_56);
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(L_52), (float)((float)il2cpp_codegen_add((float)L_53, (float)((float)il2cpp_codegen_multiply((float)L_54, (float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_55, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_56)->max_length)))))), (int32_t)1))))))))));
		int32_t L_57 = V_8;
		V_8 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)1));
	}

IL_012f:
	{
		int32_t L_58 = V_8;
		int32_t L_59 = ___newSize0;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0111;
		}
	}

IL_0134:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_60 = V_2;
		((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->set_arr_5(L_60);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_61 = V_3;
		__this->set_labels_6(L_61);
		return;
	}
}
// System.Void ValuesSplitter`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1__ctor_mFCA9CFC36477EC51513597085C22D0C60F55DD25_gshared (ValuesSplitter_1_tF0D944E0E8C33BF2E60FB976DA82F469BF251134 * __this, const RuntimeMethod* method)
{
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)1);
		__this->set_labels_6(L_0);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		Splitter__ctor_mFA7A1B4FAE29B30C71E7FB447DB1187F9E57654D((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// T ValuesSplitter`1<UnityEngine.Color>::GetValueOfChance(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ValuesSplitter_1_GetValueOfChance_m976AE16FACD58E63D76836CA20790A9F903EE00A_gshared (ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 * __this, float ___f0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		int32_t L_1 = Splitter_GetIndexOfChance_mE527221462A0EEAD95827B03679E0FE5C25877F3((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (float)L_0, /*hidden argument*/NULL);
		V_0 = (int32_t)L_1;
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_2 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)__this->get_labels_6();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		return L_5;
	}
}
// T ValuesSplitter`1<UnityEngine.Color>::GetRandomValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ValuesSplitter_1_GetRandomValue_m1B53EA4F800F0AEE0DD44907409245A9C1B5B474_gshared (ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = Random_Range_m2844A4A71C86BDF83A12D97FC6DD95278E87E384((float)(0.0f), (float)(1.0f), /*hidden argument*/NULL);
		NullCheck((ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 *)__this);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_1 = ((  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  (*) (ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		return L_1;
	}
}
// System.String ValuesSplitter`1<UnityEngine.Color>::get_SplitterValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* ValuesSplitter_1_get_SplitterValues_m87160358A768530241817773945AD38BC64FF7AB_gshared (ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_get_SplitterValues_m87160358A768530241817773945AD38BC64FF7AB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* G_B3_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	{
		V_0 = (String_t*)_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		V_1 = (int32_t)0;
		goto IL_0046;
	}

IL_000a:
	{
		String_t* L_0 = V_0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_1 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		String_t* L_3 = Single_ToString_m2B1556CFBBD088D285A0B0EA280F82D3A4344DC3((float*)(float*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), /*hidden argument*/NULL);
		String_t* L_4 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_0, (String_t*)L_3, /*hidden argument*/NULL);
		V_0 = (String_t*)L_4;
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_7 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_7);
		G_B2_0 = L_5;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))), (int32_t)1)))))
		{
			G_B3_0 = L_5;
			goto IL_0037;
		}
	}
	{
		G_B4_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		G_B4_1 = G_B2_0;
		goto IL_003c;
	}

IL_0037:
	{
		G_B4_0 = _stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9;
		G_B4_1 = G_B3_0;
	}

IL_003c:
	{
		String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)G_B4_1, (String_t*)G_B4_0, /*hidden argument*/NULL);
		V_0 = (String_t*)L_8;
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_10 = V_1;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_11 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length)))))))
		{
			goto IL_000a;
		}
	}
	{
		String_t* L_12 = V_0;
		return L_12;
	}
}
// System.Void ValuesSplitter`1<UnityEngine.Color>::set_SplitterValues(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1_set_SplitterValues_mA2BB08E07E9CBA122999FD5534B09A724351C01A_gshared (ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_set_SplitterValues_mA2BB08E07E9CBA122999FD5534B09A724351C01A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_0 = NULL;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___value0;
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_1 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)SZArrayNew(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* L_2 = (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)L_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)58));
		NullCheck((String_t*)L_0);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = String_Split_m13262358217AD2C119FD1B9733C3C0289D608512((String_t*)L_0, (CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2*)L_2, /*hidden argument*/NULL);
		V_0 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)L_3;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_4 = V_0;
		NullCheck(L_4);
		if (!(((RuntimeArray*)L_4)->max_length))
		{
			goto IL_003e;
		}
	}
	{
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_5 = V_0;
		NullCheck(L_5);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		VirtActionInvoker1< int32_t >::Invoke(4 /* System.Void Splitter::UpdateSize(System.Int32) */, (Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length)))));
		V_1 = (int32_t)0;
		goto IL_0038;
	}

IL_0023:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_6 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_7 = V_1;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		String_t* L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1C7A851BFB2F0782FD7F72F6AA1DCBB7B53A9C7E_il2cpp_TypeInfo_var);
		double L_12 = Convert_ToDouble_mAB1A9AC95EC05C079529253797362E319C342A6F((String_t*)L_11, /*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)(((float)((float)L_12))));
		int32_t L_13 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_13, (int32_t)1));
	}

IL_0038:
	{
		int32_t L_14 = V_1;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_15 = V_0;
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length)))))))
		{
			goto IL_0023;
		}
	}

IL_003e:
	{
		return;
	}
}
// System.Void ValuesSplitter`1<UnityEngine.Color>::UpdateSize(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1_UpdateSize_mADE8D1E4EBD5325AC7FC10F496E95C8B0729FA97_gshared (ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 * __this, int32_t ___newSize0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ValuesSplitter_1_UpdateSize_mADE8D1E4EBD5325AC7FC10F496E95C8B0729FA97_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* V_2 = NULL;
	ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	int32_t V_8 = 0;
	int32_t G_B8_0 = 0;
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  G_B15_0;
	memset((&G_B15_0), 0, sizeof(G_B15_0));
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_0 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_0);
		if ((((RuntimeArray*)L_0)->max_length))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_1 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_2 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)L_1);
		((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->set_arr_5(L_2);
		int32_t L_3 = ___newSize0;
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_4 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)1)));
		__this->set_labels_6(L_4);
		int32_t L_5 = ___newSize0;
		V_0 = (float)((float)((float)(1.0f)/(float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1)))))));
		V_1 = (int32_t)0;
		goto IL_0044;
	}

IL_0032:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_6 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_7 = V_1;
		float L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)((float)il2cpp_codegen_multiply((float)L_8, (float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1))))))));
		int32_t L_10 = V_1;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = ___newSize0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0032;
		}
	}
	{
		return;
	}

IL_0049:
	{
		int32_t L_13 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_14 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)SZArrayNew(SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5_il2cpp_TypeInfo_var, (uint32_t)L_13);
		V_2 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)L_14;
		int32_t L_15 = ___newSize0;
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_16 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1)));
		V_3 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)L_16;
		int32_t L_17 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_18 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_18);
		if ((((int32_t)L_17) > ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length)))))))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_19 = ___newSize0;
		G_B8_0 = L_19;
		goto IL_006f;
	}

IL_0067:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_20 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_20);
		G_B8_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))));
	}

IL_006f:
	{
		V_4 = (int32_t)G_B8_0;
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_21 = V_3;
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_22 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)__this->get_labels_6();
		NullCheck(L_22);
		int32_t L_23 = 0;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck(L_21);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 )L_24);
		V_5 = (int32_t)0;
		goto IL_00b5;
	}

IL_0089:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_25 = V_2;
		int32_t L_26 = V_5;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_27 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		int32_t L_28 = V_5;
		NullCheck(L_27);
		int32_t L_29 = L_28;
		float L_30 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck(L_25);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (float)L_30);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_31 = V_3;
		int32_t L_32 = V_5;
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_33 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)__this->get_labels_6();
		int32_t L_34 = V_5;
		NullCheck(L_33);
		int32_t L_35 = ((int32_t)il2cpp_codegen_add((int32_t)L_34, (int32_t)1));
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_31);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1))), (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 )L_36);
		int32_t L_37 = V_5;
		V_5 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1));
	}

IL_00b5:
	{
		int32_t L_38 = V_5;
		int32_t L_39 = V_4;
		if ((((int32_t)L_38) < ((int32_t)L_39)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_40 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_41 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_41);
		if ((((int32_t)L_40) <= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_41)->max_length)))))))
		{
			goto IL_0134;
		}
	}
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_42 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_42);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_42)->max_length))))) >= ((int32_t)1)))
		{
			goto IL_00d8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_43 = Vector2_get_zero_mFE0C3213BB698130D6C5247AB4B887A59074D0A8(/*hidden argument*/NULL);
		G_B15_0 = L_43;
		goto IL_00e6;
	}

IL_00d8:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_44 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_44);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_45 = Splitter_GetRange_m72C3859C7FC21EEBAEACE317B549A496023C48D7((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_44)->max_length)))), /*hidden argument*/NULL);
		G_B15_0 = L_45;
	}

IL_00e6:
	{
		float L_46 = (float)G_B15_0.get_x_0();
		V_6 = (float)L_46;
		float L_47 = V_6;
		int32_t L_48 = ___newSize0;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_49 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_49);
		V_7 = (float)((float)((float)((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_47))/(float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_48, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_49)->max_length)))))), (int32_t)1)))))));
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_50 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_50);
		V_8 = (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_50)->max_length))));
		goto IL_012f;
	}

IL_0111:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_51 = V_2;
		int32_t L_52 = V_8;
		float L_53 = V_6;
		float L_54 = V_7;
		int32_t L_55 = V_8;
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_56 = (SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5*)((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->get_arr_5();
		NullCheck(L_56);
		NullCheck(L_51);
		(L_51)->SetAt(static_cast<il2cpp_array_size_t>(L_52), (float)((float)il2cpp_codegen_add((float)L_53, (float)((float)il2cpp_codegen_multiply((float)L_54, (float)(((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_55, (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_56)->max_length)))))), (int32_t)1))))))))));
		int32_t L_57 = V_8;
		V_8 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_57, (int32_t)1));
	}

IL_012f:
	{
		int32_t L_58 = V_8;
		int32_t L_59 = ___newSize0;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_0111;
		}
	}

IL_0134:
	{
		SingleU5BU5D_tA7139B7CAA40EAEF9178E2C386C8A5993754FDD5* L_60 = V_2;
		((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this)->set_arr_5(L_60);
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_61 = V_3;
		__this->set_labels_6(L_61);
		return;
	}
}
// System.Void ValuesSplitter`1<UnityEngine.Color>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ValuesSplitter_1__ctor_m53384AE30E33261A635C2238AD9CF5B1AA73A760_gshared (ValuesSplitter_1_tBC46F80093B03E6D432B364A7F3B242A15804C21 * __this, const RuntimeMethod* method)
{
	{
		ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399* L_0 = (ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)(ColorU5BU5D_t166D390E0E6F24360F990D1F81881A72B73CA399*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), (uint32_t)1);
		__this->set_labels_6(L_0);
		NullCheck((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this);
		Splitter__ctor_mFA7A1B4FAE29B30C71E7FB447DB1187F9E57654D((Splitter_t083F03B17F8F0D1F439B0E3B66FE064F343005FE *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Activity`1<System.Object>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Activity_1__ctor_m0DF090CA2E8A3CE7FA3F3882C2BC6A8AF1D8B0F8_gshared (vp_Activity_1_t19E97BA506EE5DCBB057302B947C3A9485DB47F0 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		NullCheck((vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94 *)__this);
		vp_Activity__ctor_mB9C79F12E59E862CCA79D2ECBEECF19B0C4F484B((vp_Activity_t0A31ADCF7F54411FC33C0D3527E8C0BC98253F94 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Attempt`1_Tryer`1<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tryer_1__ctor_mE30FA669FC46A1E757B82A15F85A848238CF1F49_gshared (Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean vp_Attempt`1_Tryer`1<System.Object,System.Object>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Tryer_1_Invoke_mCDDEBAD741F2626FBD98DE16328F86EDB4AF4B8E_gshared (Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	bool result = false;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef bool (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
			}
			else
			{
				// closed
				typedef bool (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___value0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker0< bool >::Invoke(targetMethod, ___value0);
					else
						result = GenericVirtFuncInvoker0< bool >::Invoke(targetMethod, ___value0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker0< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___value0);
					else
						result = VirtFuncInvoker0< bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___value0);
				}
			}
			else
			{
				typedef bool (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef bool (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< bool, RuntimeObject * >::Invoke(targetMethod, targetThis, ___value0);
					else
						result = GenericVirtFuncInvoker1< bool, RuntimeObject * >::Invoke(targetMethod, targetThis, ___value0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< bool, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___value0);
					else
						result = VirtFuncInvoker1< bool, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___value0);
				}
			}
			else
			{
				typedef bool (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___value0, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult vp_Attempt`1_Tryer`1<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Tryer_1_BeginInvoke_m2623BA67AB54A9985FDCC7DEE16560665B7A8E83_gshared (Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF * __this, RuntimeObject * ___value0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Boolean vp_Attempt`1_Tryer`1<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Tryer_1_EndInvoke_mD197A2324D5515C22B6F52C742BDC5B2DA5AB1CE_gshared (Tryer_1_tC0E90EE6E225A81C2B118719E7AE8BA3350207DF * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((RuntimeObject*)__result);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Attempt`1<System.Object>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Attempt_1__ctor_m02EA88722E337724AAD2BF14635FE12C19A0246D_gshared (vp_Attempt_1_t5F55710F74CD633CA4E9717FA9613A936464E68F * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		NullCheck((vp_Attempt_t28537319AB5D1247932A58A0DFE65B8353FEA615 *)__this);
		vp_Attempt__ctor_m3118443A4EB35B402D253E8FAE20B39A99096F26((vp_Attempt_t28537319AB5D1247932A58A0DFE65B8353FEA615 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_Attempt`1<System.Object>::InitFields()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Attempt_1_InitFields_mE66712276CE59403FB9092ECE400F1FF356311F3_gshared (vp_Attempt_1_t5F55710F74CD633CA4E9717FA9613A936464E68F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Attempt_1_InitFields_mE66712276CE59403FB9092ECE400F1FF356311F3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)SZArrayNew(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE_il2cpp_TypeInfo_var, (uint32_t)1);
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_1 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)L_0;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_2 = vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		FieldInfo_t * L_3 = Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5((Type_t *)L_2, (String_t*)_stringLiteral72677028B4D0D41AF475041FDBE030F7C7146D2C, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (FieldInfo_t *)L_3);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_Fields_4(L_1);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_4 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)(MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)SZArrayNew(MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B_il2cpp_TypeInfo_var, (uint32_t)1);
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_5 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)L_4;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_6 = vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		Type_t * L_7 = (Type_t *)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_ArgumentType_2();
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_8 = { reinterpret_cast<intptr_t> (Boolean_tB53F6830F670160873277339AA58F15CAED4399C_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_8, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		MethodInfo_t * L_10 = vp_Event_GetStaticGenericMethod_mC5A9BB11EAE19B27D7A4F37BEDB9C0C38BCA98F7((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_6, (String_t*)_stringLiteral91A7B1383D7D83FD757F5147D796C39CED8E3E3B, (Type_t *)L_7, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_10);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (MethodInfo_t *)L_10);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_DefaultMethods_6(L_5);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_11 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)1);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_12 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_11;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_13 = { reinterpret_cast<intptr_t> (Tryer_1_tE69F8ACC696CEB81EE6A8BE0A01682F330580866_0_0_0_var) };
		Type_t * L_14 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_14);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_DelegateTypes_5(L_12);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_15 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)il2cpp_codegen_object_new(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62(L_15, /*hidden argument*/Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62_RuntimeMethod_var);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_16 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_15;
		NullCheck((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_16);
		Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_16, (String_t*)_stringLiteral5615DEB602F940728B39CAED37FECE24A8E5E21C, (int32_t)0, /*hidden argument*/Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_Prefixes_8(L_16);
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_17 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_17)
		{
			goto IL_00c1;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_18 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_18);
		int32_t L_19 = 0;
		MethodInfo_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		bool L_21 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_20, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00c1;
		}
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_22 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_22);
		int32_t L_23 = 0;
		FieldInfo_t * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_25 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_25);
		int32_t L_26 = 0;
		MethodInfo_t * L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_28 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_28);
		int32_t L_29 = 0;
		Type_t * L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_31 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_30, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_24, (MethodInfo_t *)L_27, (Type_t *)L_31, /*hidden argument*/NULL);
	}

IL_00c1:
	{
		return;
	}
}
// System.Void vp_Attempt`1<System.Object>::Register(System.Object,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Attempt_1_Register_m8EA5B549BE894D22D2A97E0582ECE5E6BCFC2879_gshared (vp_Attempt_1_t5F55710F74CD633CA4E9717FA9613A936464E68F * __this, RuntimeObject * ___t0, String_t* ___m1, int32_t ___v2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Attempt_1_Register_m8EA5B549BE894D22D2A97E0582ECE5E6BCFC2879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		int32_t L_1 = ___v2;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		FieldInfo_t * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck((FieldInfo_t *)L_3);
		RuntimeObject * L_4 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(19 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, (FieldInfo_t *)L_3, (RuntimeObject *)__this);
		NullCheck((Delegate_t *)((Delegate_t *)Castclass((RuntimeObject*)L_4, Delegate_t_il2cpp_TypeInfo_var)));
		MethodInfo_t * L_5 = Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048((Delegate_t *)((Delegate_t *)Castclass((RuntimeObject*)L_4, Delegate_t_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_5);
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_7 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		int32_t L_8 = ___v2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		MethodInfo_t * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((MemberInfo_t *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_10);
		bool L_12 = String_op_Inequality_m0BD184A74F453A72376E81CC6CAEE2556B80493E((String_t*)L_6, (String_t*)L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_004b;
		}
	}
	{
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		String_t* L_13 = vp_Event_get_EventName_m62614B846C0E4BB14613C4FBFC79CB8DABAF4E42((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		String_t* L_14 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923((String_t*)_stringLiteralD419E9940160787BF27E633820F14FCF86FF6221, (String_t*)L_13, (String_t*)_stringLiteralFEA43415268E389C0DE3D578A5E51EAF3900BB19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568((RuntimeObject *)L_14, /*hidden argument*/NULL);
	}

IL_004b:
	{
		String_t* L_15 = ___m1;
		if (!L_15)
		{
			goto IL_006c;
		}
	}
	{
		RuntimeObject * L_16 = ___t0;
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_17 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_17);
		int32_t L_18 = 0;
		FieldInfo_t * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		String_t* L_20 = ___m1;
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_21 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		int32_t L_22 = ___v2;
		NullCheck(L_21);
		int32_t L_23 = L_22;
		Type_t * L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_25 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_24, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToExternalMethod_mA9E3D6BBEDCBA637A2FA457E5DC0820B5BF3D9FE((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (RuntimeObject *)L_16, (FieldInfo_t *)L_19, (String_t*)L_20, (Type_t *)L_25, /*hidden argument*/NULL);
	}

IL_006c:
	{
		return;
	}
}
// System.Void vp_Attempt`1<System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Attempt_1_Unregister_mD663C81AA5D88179118E97A748F850B53306EE80_gshared (vp_Attempt_1_t5F55710F74CD633CA4E9717FA9613A936464E68F * __this, RuntimeObject * ___t0, const RuntimeMethod* method)
{
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_0 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_1 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_1);
		int32_t L_2 = 0;
		MethodInfo_t * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		bool L_4 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_3, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_5 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_5);
		int32_t L_6 = 0;
		FieldInfo_t * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_8 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_8);
		int32_t L_9 = 0;
		MethodInfo_t * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_11 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_11);
		int32_t L_12 = 0;
		Type_t * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_14 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_13, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_7, (MethodInfo_t *)L_10, (Type_t *)L_14, /*hidden argument*/NULL);
	}

IL_003c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalCallbackReturn`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallbackReturn_1__ctor_m77D4F05EFFCC224CFED668314D427B9036A9116B_gshared (vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R vp_GlobalCallbackReturn`1<System.Object>::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalCallbackReturn_1_Invoke_m9102EFD075CF3D5F749C645E19D27A1CFD2ED994_gshared (vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * __this, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, targetThis);
					else
						result = GenericVirtFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						result = VirtFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (void*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult vp_GlobalCallbackReturn`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* vp_GlobalCallbackReturn_1_BeginInvoke_m15BDC3FB12DCBF2656C4C4A5C305198BD5134C3C_gshared (vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * __this, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// R vp_GlobalCallbackReturn`1<System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalCallbackReturn_1_EndInvoke_m69A08AD8617924F41AC8A113409C2614E17DB4E2_gshared (vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalCallbackReturn`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallbackReturn_2__ctor_m1FCC87020480AA81C83AAB3401FF504C273A5A13_gshared (vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R vp_GlobalCallbackReturn`2<System.Object,System.Object>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalCallbackReturn_2_Invoke_mBE3DF71554F54DBE65E4E0CCAC205401E9A61AE5_gshared (vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * __this, RuntimeObject * ___arg10, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, ___arg10);
					else
						result = GenericVirtFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, ___arg10);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10);
					else
						result = VirtFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10);
					else
						result = GenericVirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10);
					else
						result = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult vp_GlobalCallbackReturn`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* vp_GlobalCallbackReturn_2_BeginInvoke_mBECFDC9164385BBB3E6545F6E7F94C3646B3AA4E_gshared (vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * __this, RuntimeObject * ___arg10, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// R vp_GlobalCallbackReturn`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalCallbackReturn_2_EndInvoke_mD9E3C2FF74BC89F4D56183DF446CEC1FBFC57FA5_gshared (vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallbackReturn_3__ctor_mCDBC46A198E8F111DAFC73D6A5BAC927A9B67FD4_gshared (vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>::Invoke(T,U)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalCallbackReturn_3_Invoke_m88AD6B6234907CEEAAC3B166EFD5F9A6A78E71D4_gshared (vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21);
					else
						result = GenericVirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21);
					else
						result = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21);
					else
						result = GenericVirtFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21);
					else
						result = VirtFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>::BeginInvoke(T,U,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* vp_GlobalCallbackReturn_3_BeginInvoke_mEA3BA0976BCC0F174545F02269EDDC286244B5E9_gshared (vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// R vp_GlobalCallbackReturn`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalCallbackReturn_3_EndInvoke_m756190AF36417BB56C2689D277F3555A6634D2E0_gshared (vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallbackReturn_4__ctor_mC69974002490BA2EA083752CBD738C8BDEA8951F_gshared (vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>::Invoke(T,U,V)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalCallbackReturn_4_Invoke_mDB7BAB354214F8073AB471F001A42A327AB50360_gshared (vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32);
					else
						result = GenericVirtFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32);
					else
						result = VirtFuncInvoker2< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32);
					else
						result = GenericVirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32);
					else
						result = VirtFuncInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T,U,V,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* vp_GlobalCallbackReturn_4_BeginInvoke_m92521B547D04EDEFA652C05882BD48A65EB83EBB_gshared (vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// R vp_GlobalCallbackReturn`4<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalCallbackReturn_4_EndInvoke_m3A23A8092B01A0C6559AACF97CC958E87BEBE79B_gshared (vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_1__ctor_mEB6CCD680B5B181CA6566AE71B51A8E9EA01D9A8_gshared (vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void vp_GlobalCallback`1<System.Object>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_1_Invoke_mFAD73E18EBD1B432D995F0DF340F025239828899_gshared (vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * __this, RuntimeObject * ___arg10, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___arg10, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___arg10);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___arg10);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___arg10, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___arg10, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10);
					else
						GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10);
					else
						VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, targetMethod);
			}
		}
	}
}
// System.IAsyncResult vp_GlobalCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* vp_GlobalCallback_1_BeginInvoke_mB3624C1F364B9E259E80EE3466B9DD3B99651E46_gshared (vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * __this, RuntimeObject * ___arg10, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void vp_GlobalCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_1_EndInvoke_mC7E4AA0B6F93BB4F2A8BF23D96A92FD4AF0BA27A_gshared (vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalCallback`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_2__ctor_m944EBB6DECED341D005F05AF01719C0FEE4862E2_gshared (vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void vp_GlobalCallback`2<System.Object,System.Object>::Invoke(T,U)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_2_Invoke_mBF16743DA8E3F1C1B23E5377BB1F8BE3593E39D5_gshared (vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 2)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, targetMethod);
			}
		}
		else if (___parameterCount != 2)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21);
					else
						GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21);
					else
						VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21);
					else
						GenericVirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21);
					else
						VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, targetMethod);
			}
		}
	}
}
// System.IAsyncResult vp_GlobalCallback`2<System.Object,System.Object>::BeginInvoke(T,U,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* vp_GlobalCallback_2_BeginInvoke_mF4F76DCBCB23D89C7B441C861BC3ADFB58DBA551_gshared (vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void vp_GlobalCallback`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_2_EndInvoke_m0CF0A6C1A1677DEB15D60081EBA8404246A53A24_gshared (vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalCallback`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_3__ctor_m88F7B0711DF27459DB0DFBEAC917AD754714FDF7_gshared (vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void vp_GlobalCallback`3<System.Object,System.Object,System.Object>::Invoke(T,U,V)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_3_Invoke_mB99F9765FCB59252ABDEF8B923585ED4B9A36455_gshared (vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 3)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, targetMethod);
			}
		}
		else if (___parameterCount != 3)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32);
					else
						GenericVirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, ___arg10, ___arg21, ___arg32);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___arg10, ___arg21, ___arg32);
					else
						VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___arg10, ___arg21, ___arg32);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___arg10, ___arg21, ___arg32, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32);
					else
						GenericVirtActionInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___arg10, ___arg21, ___arg32);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___arg10, ___arg21, ___arg32);
					else
						VirtActionInvoker3< RuntimeObject *, RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___arg10, ___arg21, ___arg32);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___arg10, ___arg21, ___arg32, targetMethod);
			}
		}
	}
}
// System.IAsyncResult vp_GlobalCallback`3<System.Object,System.Object,System.Object>::BeginInvoke(T,U,V,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* vp_GlobalCallback_3_BeginInvoke_m1E408A66D271BBFCEF0E2C39922786F792B82264_gshared (vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, RuntimeObject * ___arg32, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback3, RuntimeObject * ___object4, const RuntimeMethod* method)
{
	void *__d_args[4] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	__d_args[2] = ___arg32;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback3, (RuntimeObject*)___object4);
}
// System.Void vp_GlobalCallback`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalCallback_3_EndInvoke_m6354601D5C2F23A9BF4B73445184E9F3C0CB8C34_gshared (vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalEventReturn`1<System.Object>::Register(System.String,vp_GlobalCallbackReturn`1<R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_1_Register_m9267D1714DF192E02EEF7CFC667CA9CD1629007A_gshared (String_t* ___name0, vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_1_Register_m9267D1714DF192E02EEF7CFC667CA9CD1629007A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_1_Register_m9267D1714DF192E02EEF7CFC667CA9CD1629007A_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_1_Register_m9267D1714DF192E02EEF7CFC667CA9CD1629007A_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_1_tAF2F1E815BC70E244630A6F27841766EF2F338D9_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * L_9 = (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((  void (*) (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_10 = ((vp_GlobalEventReturn_1_tAF2F1E815BC70E244630A6F27841766EF2F338D9_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_11 = ___name0;
		List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * L_12 = V_0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10);
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(15 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10, (RuntimeObject *)L_11, (RuntimeObject *)L_12);
	}

IL_0047:
	{
		List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * L_13 = V_0;
		vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * L_14 = ___callback1;
		NullCheck((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)L_13);
		((  void (*) (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *, vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)L_13, (vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void vp_GlobalEventReturn`1<System.Object>::Unregister(System.String,vp_GlobalCallbackReturn`1<R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_1_Unregister_m367761F3D9A8BEDAF895CC87178965D5A3D5787B_gshared (String_t* ___name0, vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_1_Unregister_m367761F3D9A8BEDAF895CC87178965D5A3D5787B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_1_Unregister_m367761F3D9A8BEDAF895CC87178965D5A3D5787B_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_1_Unregister_m367761F3D9A8BEDAF895CC87178965D5A3D5787B_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_1_tAF2F1E815BC70E244630A6F27841766EF2F338D9_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * L_9 = V_0;
		vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * L_10 = ___callback1;
		NullCheck((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)L_9);
		((  bool (*) (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *, vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)->methodPointer)((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)L_9, (vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return;
	}

IL_003e:
	{
		String_t* L_11 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA * L_12 = vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD((String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, vp_GlobalEventReturn_1_Unregister_m367761F3D9A8BEDAF895CC87178965D5A3D5787B_RuntimeMethod_var);
	}
}
// R vp_GlobalEventReturn`1<System.Object>::Send(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalEventReturn_1_Send_mFC55BDB0734A1BA7DADC4ECC821D84E2771533EA_gshared (String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		RuntimeObject * L_1 = ((  RuntimeObject * (*) (String_t*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((String_t*)L_0, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		return L_1;
	}
}
// R vp_GlobalEventReturn`1<System.Object>::Send(System.String,vp_GlobalEventMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalEventReturn_1_Send_m65932A48D6E7714790169F961B61359F11487A0A_gshared (String_t* ___name0, int32_t ___mode1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_1_Send_m65932A48D6E7714790169F961B61359F11487A0A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372  V_2;
	memset((&V_2), 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_1_Send_m65932A48D6E7714790169F961B61359F11487A0A_RuntimeMethod_var);
	}

IL_0013:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_3 = ((vp_GlobalEventReturn_1_tAF2F1E815BC70E244630A6F27841766EF2F338D9_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_4 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_3);
		RuntimeObject * L_5 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_3, (RuntimeObject *)L_4);
		V_0 = (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * L_6 = V_0;
		if (!L_6)
		{
			goto IL_0060;
		}
	}
	{
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 * L_7 = V_0;
		NullCheck((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)L_7);
		Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372  L_8 = ((  Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372  (*) (List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)((List_1_t3B9317E4FC5A978EAC1A86C9D3325852D7D0D718 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		V_2 = (Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 )L_8;
	}

IL_0036:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0045;
		}

IL_0038:
		{
			vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED * L_9 = Enumerator_get_Current_mA1EA693ACC6D3A81C670C07B7F2B7BDA29139AF0((Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 *)(Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
			NullCheck((vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED *)L_9);
			RuntimeObject * L_10 = ((  RuntimeObject * (*) (vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)((vp_GlobalCallbackReturn_1_tE3CBE1B4DAEA368EFADB1C5E9E559BCFB8F10CED *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
			V_1 = (RuntimeObject *)L_10;
		}

IL_0045:
		{
			bool L_11 = Enumerator_MoveNext_m4EC116B65FCCFBCB40BC5D26C067FD299BC0A9FE((Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 *)(Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
			if (L_11)
			{
				goto IL_0038;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5E, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		Il2CppFakeBox<Enumerator_t8835400FFE28E8FC2DEA3CAD2194B896A0052372 > L_12(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 10), (&V_2));
		const VirtualInvokeData& il2cpp_virtual_invoke_data__88 = il2cpp_codegen_get_interface_invoke_data(0, (&L_12), IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var);
		((  void (*) (RuntimeObject*, const RuntimeMethod*))il2cpp_virtual_invoke_data__88.methodPtr)((RuntimeObject*)(&L_12), /*hidden argument*/il2cpp_virtual_invoke_data__88.method);
		V_2 = L_12.m_Value;
		IL2CPP_RESET_LEAVE(0x5E);
		IL2CPP_END_FINALLY(80)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005e:
	{
		RuntimeObject * L_13 = V_1;
		return L_13;
	}

IL_0060:
	{
		int32_t L_14 = ___mode1;
		if ((!(((uint32_t)L_14) == ((uint32_t)1))))
		{
			goto IL_006b;
		}
	}
	{
		String_t* L_15 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A * L_16 = vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146((String_t*)L_15, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16, NULL, vp_GlobalEventReturn_1_Send_m65932A48D6E7714790169F961B61359F11487A0A_RuntimeMethod_var);
	}

IL_006b:
	{
		il2cpp_codegen_initobj((&V_3), sizeof(RuntimeObject *));
		RuntimeObject * L_17 = V_3;
		return L_17;
	}
}
// System.Void vp_GlobalEventReturn`1<System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_1__cctor_m8313D94E56FB3E4963AC3B968D77A3A6C6C6EAC9_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_1__cctor_m8313D94E56FB3E4963AC3B968D77A3A6C6C6EAC9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_0 = ((vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields*)il2cpp_codegen_static_fields_for(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var))->get_Callbacks_0();
		((vp_GlobalEventReturn_1_tAF2F1E815BC70E244630A6F27841766EF2F338D9_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->set_m_Callbacks_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalEventReturn`2<System.Object,System.Object>::Register(System.String,vp_GlobalCallbackReturn`2<T,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_2_Register_m65F5A02295B8DAB588E6AD98D0CD0ED1CF9D9979_gshared (String_t* ___name0, vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_2_Register_m65F5A02295B8DAB588E6AD98D0CD0ED1CF9D9979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_2_Register_m65F5A02295B8DAB588E6AD98D0CD0ED1CF9D9979_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_2_Register_m65F5A02295B8DAB588E6AD98D0CD0ED1CF9D9979_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_2_tB56FD4BCAB9452523F648DD091D2A2C667F189E1_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * L_9 = (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((  void (*) (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_10 = ((vp_GlobalEventReturn_2_tB56FD4BCAB9452523F648DD091D2A2C667F189E1_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_11 = ___name0;
		List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * L_12 = V_0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10);
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(15 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10, (RuntimeObject *)L_11, (RuntimeObject *)L_12);
	}

IL_0047:
	{
		List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * L_13 = V_0;
		vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * L_14 = ___callback1;
		NullCheck((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)L_13);
		((  void (*) (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *, vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)L_13, (vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void vp_GlobalEventReturn`2<System.Object,System.Object>::Unregister(System.String,vp_GlobalCallbackReturn`2<T,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_2_Unregister_mFFC1B7889EA18F8910084CEFA3FA7D928A576786_gshared (String_t* ___name0, vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_2_Unregister_mFFC1B7889EA18F8910084CEFA3FA7D928A576786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_2_Unregister_mFFC1B7889EA18F8910084CEFA3FA7D928A576786_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_2_Unregister_mFFC1B7889EA18F8910084CEFA3FA7D928A576786_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_2_tB56FD4BCAB9452523F648DD091D2A2C667F189E1_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * L_9 = V_0;
		vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * L_10 = ___callback1;
		NullCheck((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)L_9);
		((  bool (*) (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *, vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)->methodPointer)((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)L_9, (vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return;
	}

IL_003e:
	{
		String_t* L_11 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA * L_12 = vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD((String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, vp_GlobalEventReturn_2_Unregister_mFFC1B7889EA18F8910084CEFA3FA7D928A576786_RuntimeMethod_var);
	}
}
// R vp_GlobalEventReturn`2<System.Object,System.Object>::Send(System.String,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalEventReturn_2_Send_m3F78088A2F1F2CFF5EEBEB42FB74DC8BA302998C_gshared (String_t* ___name0, RuntimeObject * ___arg11, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		RuntimeObject * L_1 = ___arg11;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (String_t*, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((String_t*)L_0, (RuntimeObject *)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		return L_2;
	}
}
// R vp_GlobalEventReturn`2<System.Object,System.Object>::Send(System.String,T,vp_GlobalEventMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalEventReturn_2_Send_mA8EA70105B38F57E1AEE089ACEA6ADA5FEE4F718_gshared (String_t* ___name0, RuntimeObject * ___arg11, int32_t ___mode2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_2_Send_mA8EA70105B38F57E1AEE089ACEA6ADA5FEE4F718_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC  V_2;
	memset((&V_2), 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_2_Send_mA8EA70105B38F57E1AEE089ACEA6ADA5FEE4F718_RuntimeMethod_var);
	}

IL_0013:
	{
		RuntimeObject * L_3 = ___arg11;
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralE044DB5CACC7C1E1DED3C45FA7472331FE5E6246, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_2_Send_mA8EA70105B38F57E1AEE089ACEA6ADA5FEE4F718_RuntimeMethod_var);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_2_tB56FD4BCAB9452523F648DD091D2A2C667F189E1_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_0074;
		}
	}
	{
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 * L_9 = V_0;
		NullCheck((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)L_9);
		Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC  L_10 = ((  Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC  (*) (List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7)->methodPointer)((List_1_t42123C5FE1993CF583DCE8DE2A55EA3647E25091 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		V_2 = (Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC )L_10;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0059;
		}

IL_004b:
		{
			vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C * L_11 = Enumerator_get_Current_m45346515CF29AD0B024D593BB6EEA63F1556037B((Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC *)(Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
			RuntimeObject * L_12 = ___arg11;
			NullCheck((vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C *)L_11);
			RuntimeObject * L_13 = ((  RuntimeObject * (*) (vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9)->methodPointer)((vp_GlobalCallbackReturn_2_t08B97800806AAEDBCD6BF6C7550FBD2AB7BD8B0C *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
			V_1 = (RuntimeObject *)L_13;
		}

IL_0059:
		{
			bool L_14 = Enumerator_MoveNext_mDE1913E33F8F8E14045089C9242E05134FDA5777((Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC *)(Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 10));
			if (L_14)
			{
				goto IL_004b;
			}
		}

IL_0062:
		{
			IL2CPP_LEAVE(0x72, FINALLY_0064);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0064;
	}

FINALLY_0064:
	{ // begin finally (depth: 1)
		Il2CppFakeBox<Enumerator_tE4F395F5942D66B92F4B7EF8298FB252A6D756FC > L_15(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 11), (&V_2));
		const VirtualInvokeData& il2cpp_virtual_invoke_data__108 = il2cpp_codegen_get_interface_invoke_data(0, (&L_15), IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var);
		((  void (*) (RuntimeObject*, const RuntimeMethod*))il2cpp_virtual_invoke_data__108.methodPtr)((RuntimeObject*)(&L_15), /*hidden argument*/il2cpp_virtual_invoke_data__108.method);
		V_2 = L_15.m_Value;
		IL2CPP_RESET_LEAVE(0x72);
		IL2CPP_END_FINALLY(100)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(100)
	{
		IL2CPP_JUMP_TBL(0x72, IL_0072)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0072:
	{
		RuntimeObject * L_16 = V_1;
		return L_16;
	}

IL_0074:
	{
		int32_t L_17 = ___mode2;
		if ((!(((uint32_t)L_17) == ((uint32_t)1))))
		{
			goto IL_007f;
		}
	}
	{
		String_t* L_18 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A * L_19 = vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146((String_t*)L_18, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, NULL, vp_GlobalEventReturn_2_Send_mA8EA70105B38F57E1AEE089ACEA6ADA5FEE4F718_RuntimeMethod_var);
	}

IL_007f:
	{
		il2cpp_codegen_initobj((&V_3), sizeof(RuntimeObject *));
		RuntimeObject * L_20 = V_3;
		return L_20;
	}
}
// System.Void vp_GlobalEventReturn`2<System.Object,System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_2__cctor_m37FE26F32E9C5259667FE0AB6F34421EBEBB6531_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_2__cctor_m37FE26F32E9C5259667FE0AB6F34421EBEBB6531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_0 = ((vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields*)il2cpp_codegen_static_fields_for(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var))->get_Callbacks_0();
		((vp_GlobalEventReturn_2_tB56FD4BCAB9452523F648DD091D2A2C667F189E1_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->set_m_Callbacks_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalEventReturn`3<System.Object,System.Object,System.Object>::Register(System.String,vp_GlobalCallbackReturn`3<T,U,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_3_Register_m623AEF48AC44C458224D8B5BC938B2B78D427652_gshared (String_t* ___name0, vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_3_Register_m623AEF48AC44C458224D8B5BC938B2B78D427652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_3_Register_m623AEF48AC44C458224D8B5BC938B2B78D427652_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_3_Register_m623AEF48AC44C458224D8B5BC938B2B78D427652_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_3_t0D2E65F80187163C7E074E25C569E68C3A6D72A6_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * L_9 = (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((  void (*) (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_10 = ((vp_GlobalEventReturn_3_t0D2E65F80187163C7E074E25C569E68C3A6D72A6_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_11 = ___name0;
		List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * L_12 = V_0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10);
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(15 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10, (RuntimeObject *)L_11, (RuntimeObject *)L_12);
	}

IL_0047:
	{
		List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * L_13 = V_0;
		vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * L_14 = ___callback1;
		NullCheck((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)L_13);
		((  void (*) (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *, vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)L_13, (vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void vp_GlobalEventReturn`3<System.Object,System.Object,System.Object>::Unregister(System.String,vp_GlobalCallbackReturn`3<T,U,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_3_Unregister_mA7371437A5820946B3B160009A4CA66B8A831D96_gshared (String_t* ___name0, vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_3_Unregister_mA7371437A5820946B3B160009A4CA66B8A831D96_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_3_Unregister_mA7371437A5820946B3B160009A4CA66B8A831D96_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_3_Unregister_mA7371437A5820946B3B160009A4CA66B8A831D96_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_3_t0D2E65F80187163C7E074E25C569E68C3A6D72A6_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * L_9 = V_0;
		vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * L_10 = ___callback1;
		NullCheck((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)L_9);
		((  bool (*) (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *, vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)->methodPointer)((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)L_9, (vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return;
	}

IL_003e:
	{
		String_t* L_11 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA * L_12 = vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD((String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, vp_GlobalEventReturn_3_Unregister_mA7371437A5820946B3B160009A4CA66B8A831D96_RuntimeMethod_var);
	}
}
// R vp_GlobalEventReturn`3<System.Object,System.Object,System.Object>::Send(System.String,T,U)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalEventReturn_3_Send_m167073A2C1745CEBC7C0A87438807730BD66410C_gshared (String_t* ___name0, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		RuntimeObject * L_1 = ___arg11;
		RuntimeObject * L_2 = ___arg22;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (String_t*, RuntimeObject *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((String_t*)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		return L_3;
	}
}
// R vp_GlobalEventReturn`3<System.Object,System.Object,System.Object>::Send(System.String,T,U,vp_GlobalEventMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalEventReturn_3_Send_m53BF34DD0041FAD79B5E877AC5D3992446787EB5_gshared (String_t* ___name0, RuntimeObject * ___arg11, RuntimeObject * ___arg22, int32_t ___mode3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_3_Send_m53BF34DD0041FAD79B5E877AC5D3992446787EB5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689  V_2;
	memset((&V_2), 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_3_Send_m53BF34DD0041FAD79B5E877AC5D3992446787EB5_RuntimeMethod_var);
	}

IL_0013:
	{
		RuntimeObject * L_3 = ___arg11;
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralE044DB5CACC7C1E1DED3C45FA7472331FE5E6246, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_3_Send_m53BF34DD0041FAD79B5E877AC5D3992446787EB5_RuntimeMethod_var);
	}

IL_0026:
	{
		RuntimeObject * L_5 = ___arg22;
		if (L_5)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_6 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_6, (String_t*)_stringLiteralB38FACF4A8F9F6E8FC7192D1CBC326386647F4C1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, vp_GlobalEventReturn_3_Send_m53BF34DD0041FAD79B5E877AC5D3992446787EB5_RuntimeMethod_var);
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_7 = ((vp_GlobalEventReturn_3_t0D2E65F80187163C7E074E25C569E68C3A6D72A6_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_8 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_7);
		RuntimeObject * L_9 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_7, (RuntimeObject *)L_8);
		V_0 = (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)Castclass((RuntimeObject*)L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * L_10 = V_0;
		if (!L_10)
		{
			goto IL_0088;
		}
	}
	{
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 * L_11 = V_0;
		NullCheck((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)L_11);
		Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689  L_12 = ((  Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689  (*) (List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)((List_1_t31DDDBB214B238F41A87CA34E963F15EA72C7735 *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		V_2 = (Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 )L_12;
	}

IL_005c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006d;
		}

IL_005e:
		{
			vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F * L_13 = Enumerator_get_Current_m318CF4304C8F59DFBCE9FE716FB9871D81671A1F((Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 *)(Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
			RuntimeObject * L_14 = ___arg11;
			RuntimeObject * L_15 = ___arg22;
			NullCheck((vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F *)L_13);
			RuntimeObject * L_16 = ((  RuntimeObject * (*) (vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 10)->methodPointer)((vp_GlobalCallbackReturn_3_tC09DD1A3CB9F685B2E43EA948076AD7B3703258F *)L_13, (RuntimeObject *)L_14, (RuntimeObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 10));
			V_1 = (RuntimeObject *)L_16;
		}

IL_006d:
		{
			bool L_17 = Enumerator_MoveNext_m80822B43BFD7010343E4AE303D6D9D78A1BEE78A((Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 *)(Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 11));
			if (L_17)
			{
				goto IL_005e;
			}
		}

IL_0076:
		{
			IL2CPP_LEAVE(0x86, FINALLY_0078);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0078;
	}

FINALLY_0078:
	{ // begin finally (depth: 1)
		Il2CppFakeBox<Enumerator_t50BDC68B47C065E6B4B91DBE1D3B46228DF71689 > L_18(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 12), (&V_2));
		const VirtualInvokeData& il2cpp_virtual_invoke_data__128 = il2cpp_codegen_get_interface_invoke_data(0, (&L_18), IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var);
		((  void (*) (RuntimeObject*, const RuntimeMethod*))il2cpp_virtual_invoke_data__128.methodPtr)((RuntimeObject*)(&L_18), /*hidden argument*/il2cpp_virtual_invoke_data__128.method);
		V_2 = L_18.m_Value;
		IL2CPP_RESET_LEAVE(0x86);
		IL2CPP_END_FINALLY(120)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(120)
	{
		IL2CPP_JUMP_TBL(0x86, IL_0086)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0086:
	{
		RuntimeObject * L_19 = V_1;
		return L_19;
	}

IL_0088:
	{
		int32_t L_20 = ___mode3;
		if ((!(((uint32_t)L_20) == ((uint32_t)1))))
		{
			goto IL_0093;
		}
	}
	{
		String_t* L_21 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A * L_22 = vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146((String_t*)L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22, NULL, vp_GlobalEventReturn_3_Send_m53BF34DD0041FAD79B5E877AC5D3992446787EB5_RuntimeMethod_var);
	}

IL_0093:
	{
		il2cpp_codegen_initobj((&V_3), sizeof(RuntimeObject *));
		RuntimeObject * L_23 = V_3;
		return L_23;
	}
}
// System.Void vp_GlobalEventReturn`3<System.Object,System.Object,System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_3__cctor_mFD51E060AAF6726148E517A2CCF33699FC889D05_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_3__cctor_mFD51E060AAF6726148E517A2CCF33699FC889D05_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_0 = ((vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields*)il2cpp_codegen_static_fields_for(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var))->get_Callbacks_0();
		((vp_GlobalEventReturn_3_t0D2E65F80187163C7E074E25C569E68C3A6D72A6_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->set_m_Callbacks_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalEventReturn`4<System.Object,System.Object,System.Object,System.Object>::Register(System.String,vp_GlobalCallbackReturn`4<T,U,V,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_4_Register_mCABDC7AF7ADC056A07F0BFF4583EDA80FA020487_gshared (String_t* ___name0, vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_4_Register_mCABDC7AF7ADC056A07F0BFF4583EDA80FA020487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_4_Register_mCABDC7AF7ADC056A07F0BFF4583EDA80FA020487_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_4_Register_mCABDC7AF7ADC056A07F0BFF4583EDA80FA020487_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_4_t70B37E514169414D4CBA831FF242537C6E61E751_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * L_9 = (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((  void (*) (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_10 = ((vp_GlobalEventReturn_4_t70B37E514169414D4CBA831FF242537C6E61E751_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_11 = ___name0;
		List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * L_12 = V_0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10);
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(15 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10, (RuntimeObject *)L_11, (RuntimeObject *)L_12);
	}

IL_0047:
	{
		List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * L_13 = V_0;
		vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * L_14 = ___callback1;
		NullCheck((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)L_13);
		((  void (*) (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *, vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)L_13, (vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void vp_GlobalEventReturn`4<System.Object,System.Object,System.Object,System.Object>::Unregister(System.String,vp_GlobalCallbackReturn`4<T,U,V,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_4_Unregister_m5834FBF25CBBAA2D9673C24D873D100CCEE3456D_gshared (String_t* ___name0, vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_4_Unregister_m5834FBF25CBBAA2D9673C24D873D100CCEE3456D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_4_Unregister_m5834FBF25CBBAA2D9673C24D873D100CCEE3456D_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_4_Unregister_m5834FBF25CBBAA2D9673C24D873D100CCEE3456D_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEventReturn_4_t70B37E514169414D4CBA831FF242537C6E61E751_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * L_9 = V_0;
		vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * L_10 = ___callback1;
		NullCheck((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)L_9);
		((  bool (*) (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *, vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)->methodPointer)((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)L_9, (vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return;
	}

IL_003e:
	{
		String_t* L_11 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA * L_12 = vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD((String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, vp_GlobalEventReturn_4_Unregister_m5834FBF25CBBAA2D9673C24D873D100CCEE3456D_RuntimeMethod_var);
	}
}
// R vp_GlobalEventReturn`4<System.Object,System.Object,System.Object,System.Object>::Send(System.String,T,U,V)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalEventReturn_4_Send_mAB9CCC11D32957F320232C4F7831308880D4D088_gshared (String_t* ___name0, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		RuntimeObject * L_1 = ___arg11;
		RuntimeObject * L_2 = ___arg22;
		RuntimeObject * L_3 = ___arg33;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (String_t*, RuntimeObject *, RuntimeObject *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((String_t*)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		return L_4;
	}
}
// R vp_GlobalEventReturn`4<System.Object,System.Object,System.Object,System.Object>::Send(System.String,T,U,V,vp_GlobalEventMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_gshared (String_t* ___name0, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, int32_t ___mode4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564  V_2;
	memset((&V_2), 0, sizeof(V_2));
	RuntimeObject * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_RuntimeMethod_var);
	}

IL_0013:
	{
		RuntimeObject * L_3 = ___arg11;
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralE044DB5CACC7C1E1DED3C45FA7472331FE5E6246, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_RuntimeMethod_var);
	}

IL_0026:
	{
		RuntimeObject * L_5 = ___arg22;
		if (L_5)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_6 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_6, (String_t*)_stringLiteralB38FACF4A8F9F6E8FC7192D1CBC326386647F4C1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_RuntimeMethod_var);
	}

IL_0039:
	{
		RuntimeObject * L_7 = ___arg33;
		if (L_7)
		{
			goto IL_004c;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_8 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_8, (String_t*)_stringLiteralACDB94344573594F5AE5956EF0E2477E3C8C2B07, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, NULL, vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_RuntimeMethod_var);
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_9 = ((vp_GlobalEventReturn_4_t70B37E514169414D4CBA831FF242537C6E61E751_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_10 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_9);
		RuntimeObject * L_11 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_9, (RuntimeObject *)L_10);
		V_0 = (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * L_12 = V_0;
		if (!L_12)
		{
			goto IL_009c;
		}
	}
	{
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 * L_13 = V_0;
		NullCheck((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)L_13);
		Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564  L_14 = ((  Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564  (*) (List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9)->methodPointer)((List_1_tB77B157C925F80853DEA7EBAE32D4A669F16CEC0 *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		V_2 = (Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 )L_14;
	}

IL_006f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0081;
		}

IL_0071:
		{
			vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 * L_15 = Enumerator_get_Current_m81131B42FF894AF40643860B33F53767A087981E((Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 *)(Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 10));
			RuntimeObject * L_16 = ___arg11;
			RuntimeObject * L_17 = ___arg22;
			RuntimeObject * L_18 = ___arg33;
			NullCheck((vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 *)L_15);
			RuntimeObject * L_19 = ((  RuntimeObject * (*) (vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 11)->methodPointer)((vp_GlobalCallbackReturn_4_t1B2CB00514ED41EF62B596A9F31F5C311C5FACE8 *)L_15, (RuntimeObject *)L_16, (RuntimeObject *)L_17, (RuntimeObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 11));
			V_1 = (RuntimeObject *)L_19;
		}

IL_0081:
		{
			bool L_20 = Enumerator_MoveNext_m584975834A266F016DBEDEBDEBD0CE7FA81E8CEF((Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 *)(Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 *)(&V_2), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12));
			if (L_20)
			{
				goto IL_0071;
			}
		}

IL_008a:
		{
			IL2CPP_LEAVE(0x9A, FINALLY_008c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_008c;
	}

FINALLY_008c:
	{ // begin finally (depth: 1)
		Il2CppFakeBox<Enumerator_tD7C91B0E7250F25F9EDBC5AFDFF11091AF627564 > L_21(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 13), (&V_2));
		const VirtualInvokeData& il2cpp_virtual_invoke_data__148 = il2cpp_codegen_get_interface_invoke_data(0, (&L_21), IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var);
		((  void (*) (RuntimeObject*, const RuntimeMethod*))il2cpp_virtual_invoke_data__148.methodPtr)((RuntimeObject*)(&L_21), /*hidden argument*/il2cpp_virtual_invoke_data__148.method);
		V_2 = L_21.m_Value;
		IL2CPP_RESET_LEAVE(0x9A);
		IL2CPP_END_FINALLY(140)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(140)
	{
		IL2CPP_JUMP_TBL(0x9A, IL_009a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_009a:
	{
		RuntimeObject * L_22 = V_1;
		return L_22;
	}

IL_009c:
	{
		int32_t L_23 = ___mode4;
		if ((!(((uint32_t)L_23) == ((uint32_t)1))))
		{
			goto IL_00a8;
		}
	}
	{
		String_t* L_24 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A * L_25 = vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146((String_t*)L_24, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_25, NULL, vp_GlobalEventReturn_4_Send_m272946A97917EA6E7C1661F3ECC29E5FEA9C00A2_RuntimeMethod_var);
	}

IL_00a8:
	{
		il2cpp_codegen_initobj((&V_3), sizeof(RuntimeObject *));
		RuntimeObject * L_26 = V_3;
		return L_26;
	}
}
// System.Void vp_GlobalEventReturn`4<System.Object,System.Object,System.Object,System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEventReturn_4__cctor_m5B64DD7C06F04FA8284829E44C703BEA57A42DBA_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEventReturn_4__cctor_m5B64DD7C06F04FA8284829E44C703BEA57A42DBA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_0 = ((vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields*)il2cpp_codegen_static_fields_for(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var))->get_Callbacks_0();
		((vp_GlobalEventReturn_4_t70B37E514169414D4CBA831FF242537C6E61E751_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->set_m_Callbacks_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalEvent`1<System.Object>::Register(System.String,vp_GlobalCallback`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_1_Register_m0C35CD0E2D37D4538B19E15618CE12ADA4A38169_gshared (String_t* ___name0, vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_1_Register_m0C35CD0E2D37D4538B19E15618CE12ADA4A38169_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_1_Register_m0C35CD0E2D37D4538B19E15618CE12ADA4A38169_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_1_Register_m0C35CD0E2D37D4538B19E15618CE12ADA4A38169_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEvent_1_tBB56B018FE03878F52DDD05CF63495FE7B7CBF0F_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * L_8 = V_0;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * L_9 = (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((  void (*) (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_10 = ((vp_GlobalEvent_1_tBB56B018FE03878F52DDD05CF63495FE7B7CBF0F_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_11 = ___name0;
		List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * L_12 = V_0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10);
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(15 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10, (RuntimeObject *)L_11, (RuntimeObject *)L_12);
	}

IL_0047:
	{
		List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * L_13 = V_0;
		vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * L_14 = ___callback1;
		NullCheck((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)L_13);
		((  void (*) (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *, vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)L_13, (vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void vp_GlobalEvent`1<System.Object>::Unregister(System.String,vp_GlobalCallback`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_1_Unregister_m1201A989E77B0ABA197E41B3AC02409707D26587_gshared (String_t* ___name0, vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_1_Unregister_m1201A989E77B0ABA197E41B3AC02409707D26587_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_1_Unregister_m1201A989E77B0ABA197E41B3AC02409707D26587_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_1_Unregister_m1201A989E77B0ABA197E41B3AC02409707D26587_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEvent_1_tBB56B018FE03878F52DDD05CF63495FE7B7CBF0F_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * L_9 = V_0;
		vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * L_10 = ___callback1;
		NullCheck((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)L_9);
		((  bool (*) (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *, vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)->methodPointer)((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)L_9, (vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return;
	}

IL_003e:
	{
		String_t* L_11 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA * L_12 = vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD((String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, vp_GlobalEvent_1_Unregister_m1201A989E77B0ABA197E41B3AC02409707D26587_RuntimeMethod_var);
	}
}
// System.Void vp_GlobalEvent`1<System.Object>::Send(System.String,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_1_Send_m2DF1CF009BB47D9027922578A6A00DF042660E3A_gshared (String_t* ___name0, RuntimeObject * ___arg11, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		RuntimeObject * L_1 = ___arg11;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (String_t*, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((String_t*)L_0, (RuntimeObject *)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		return;
	}
}
// System.Void vp_GlobalEvent`1<System.Object>::Send(System.String,T,vp_GlobalEventMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_1_Send_m0E8BD5CE11E352A4C0D2A5FEE5F00581654796BE_gshared (String_t* ___name0, RuntimeObject * ___arg11, int32_t ___mode2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_1_Send_m0E8BD5CE11E352A4C0D2A5FEE5F00581654796BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * V_0 = NULL;
	Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_1_Send_m0E8BD5CE11E352A4C0D2A5FEE5F00581654796BE_RuntimeMethod_var);
	}

IL_0013:
	{
		RuntimeObject * L_3 = ___arg11;
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralE044DB5CACC7C1E1DED3C45FA7472331FE5E6246, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_1_Send_m0E8BD5CE11E352A4C0D2A5FEE5F00581654796BE_RuntimeMethod_var);
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEvent_1_tBB56B018FE03878F52DDD05CF63495FE7B7CBF0F_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * L_8 = V_0;
		if (!L_8)
		{
			goto IL_0069;
		}
	}
	{
		List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 * L_9 = V_0;
		NullCheck((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)L_9);
		Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21  L_10 = ((  Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21  (*) (List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7)->methodPointer)((List_1_t4208A46DCEF9248267644BEC76A2011791D1B4F3 *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		V_1 = (Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 )L_10;
	}

IL_0041:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0050;
		}

IL_0043:
		{
			vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 * L_11 = Enumerator_get_Current_mBF8BBA4FF63BA9CA90910DDCB11B2A21BFC25767((Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 *)(Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
			RuntimeObject * L_12 = ___arg11;
			NullCheck((vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 *)L_11);
			((  void (*) (vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9)->methodPointer)((vp_GlobalCallback_1_t2895A762BA91D35D452BCF09ABE72E01C1564159 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		}

IL_0050:
		{
			bool L_13 = Enumerator_MoveNext_mAA65D37DCA9DD3E0CD391F63F380C140D1307A65((Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 *)(Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 10));
			if (L_13)
			{
				goto IL_0043;
			}
		}

IL_0059:
		{
			IL2CPP_LEAVE(0x74, FINALLY_005b);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_005b;
	}

FINALLY_005b:
	{ // begin finally (depth: 1)
		Il2CppFakeBox<Enumerator_tB635E5E7A59A2A3EA1E55E91FDADD7791A52CD21 > L_14(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 11), (&V_1));
		const VirtualInvokeData& il2cpp_virtual_invoke_data__99 = il2cpp_codegen_get_interface_invoke_data(0, (&L_14), IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var);
		((  void (*) (RuntimeObject*, const RuntimeMethod*))il2cpp_virtual_invoke_data__99.methodPtr)((RuntimeObject*)(&L_14), /*hidden argument*/il2cpp_virtual_invoke_data__99.method);
		V_1 = L_14.m_Value;
		IL2CPP_RESET_LEAVE(0x74);
		IL2CPP_END_FINALLY(91)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(91)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0069:
	{
		int32_t L_15 = ___mode2;
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_0074;
		}
	}
	{
		String_t* L_16 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A * L_17 = vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146((String_t*)L_16, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17, NULL, vp_GlobalEvent_1_Send_m0E8BD5CE11E352A4C0D2A5FEE5F00581654796BE_RuntimeMethod_var);
	}

IL_0074:
	{
		return;
	}
}
// System.Void vp_GlobalEvent`1<System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_1__cctor_m8C71D551E610B679C9B01160BF0E3070F6F5499E_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_1__cctor_m8C71D551E610B679C9B01160BF0E3070F6F5499E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_0 = ((vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields*)il2cpp_codegen_static_fields_for(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var))->get_Callbacks_0();
		((vp_GlobalEvent_1_tBB56B018FE03878F52DDD05CF63495FE7B7CBF0F_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->set_m_Callbacks_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalEvent`2<System.Object,System.Object>::Register(System.String,vp_GlobalCallback`2<T,U>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_2_Register_mF7331093FE8FB0056E02990FE3D49F16E347B0B2_gshared (String_t* ___name0, vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_2_Register_mF7331093FE8FB0056E02990FE3D49F16E347B0B2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_2_Register_mF7331093FE8FB0056E02990FE3D49F16E347B0B2_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_2_Register_mF7331093FE8FB0056E02990FE3D49F16E347B0B2_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEvent_2_t12287246C36DFF3C8C0AAE2326EFA5217F510174_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * L_8 = V_0;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * L_9 = (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((  void (*) (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_10 = ((vp_GlobalEvent_2_t12287246C36DFF3C8C0AAE2326EFA5217F510174_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_11 = ___name0;
		List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * L_12 = V_0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10);
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(15 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10, (RuntimeObject *)L_11, (RuntimeObject *)L_12);
	}

IL_0047:
	{
		List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * L_13 = V_0;
		vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * L_14 = ___callback1;
		NullCheck((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)L_13);
		((  void (*) (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *, vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)L_13, (vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void vp_GlobalEvent`2<System.Object,System.Object>::Unregister(System.String,vp_GlobalCallback`2<T,U>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_2_Unregister_m8343F8C1A2CBA5850A44B7226093C0892FF09D8E_gshared (String_t* ___name0, vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_2_Unregister_m8343F8C1A2CBA5850A44B7226093C0892FF09D8E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_2_Unregister_m8343F8C1A2CBA5850A44B7226093C0892FF09D8E_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_2_Unregister_m8343F8C1A2CBA5850A44B7226093C0892FF09D8E_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEvent_2_t12287246C36DFF3C8C0AAE2326EFA5217F510174_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * L_8 = V_0;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * L_9 = V_0;
		vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * L_10 = ___callback1;
		NullCheck((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)L_9);
		((  bool (*) (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *, vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)->methodPointer)((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)L_9, (vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return;
	}

IL_003e:
	{
		String_t* L_11 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA * L_12 = vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD((String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, vp_GlobalEvent_2_Unregister_m8343F8C1A2CBA5850A44B7226093C0892FF09D8E_RuntimeMethod_var);
	}
}
// System.Void vp_GlobalEvent`2<System.Object,System.Object>::Send(System.String,T,U)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_2_Send_m419A1627246757D90848E603AE9D747B4708F20B_gshared (String_t* ___name0, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		RuntimeObject * L_1 = ___arg11;
		RuntimeObject * L_2 = ___arg22;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (String_t*, RuntimeObject *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((String_t*)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		return;
	}
}
// System.Void vp_GlobalEvent`2<System.Object,System.Object>::Send(System.String,T,U,vp_GlobalEventMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_2_Send_m5B777A199A27FAF9D55E45AAA843200FB804A6EE_gshared (String_t* ___name0, RuntimeObject * ___arg11, RuntimeObject * ___arg22, int32_t ___mode3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_2_Send_m5B777A199A27FAF9D55E45AAA843200FB804A6EE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * V_0 = NULL;
	Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_2_Send_m5B777A199A27FAF9D55E45AAA843200FB804A6EE_RuntimeMethod_var);
	}

IL_0013:
	{
		RuntimeObject * L_3 = ___arg11;
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralE044DB5CACC7C1E1DED3C45FA7472331FE5E6246, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_2_Send_m5B777A199A27FAF9D55E45AAA843200FB804A6EE_RuntimeMethod_var);
	}

IL_0026:
	{
		RuntimeObject * L_5 = ___arg22;
		if (L_5)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_6 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_6, (String_t*)_stringLiteralB38FACF4A8F9F6E8FC7192D1CBC326386647F4C1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, vp_GlobalEvent_2_Send_m5B777A199A27FAF9D55E45AAA843200FB804A6EE_RuntimeMethod_var);
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_7 = ((vp_GlobalEvent_2_t12287246C36DFF3C8C0AAE2326EFA5217F510174_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_8 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_7);
		RuntimeObject * L_9 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_7, (RuntimeObject *)L_8);
		V_0 = (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)Castclass((RuntimeObject*)L_9, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * L_10 = V_0;
		if (!L_10)
		{
			goto IL_007d;
		}
	}
	{
		List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E * L_11 = V_0;
		NullCheck((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)L_11);
		Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A  L_12 = ((  Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A  (*) (List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)((List_1_tB0721389AEA4DBEF3E102DB29D3874AB8CC76F9E *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		V_1 = (Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A )L_12;
	}

IL_0054:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0064;
		}

IL_0056:
		{
			vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 * L_13 = Enumerator_get_Current_m0AE5488682304FE7CE9D3A7A11F489DEF51B532C((Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A *)(Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
			RuntimeObject * L_14 = ___arg11;
			RuntimeObject * L_15 = ___arg22;
			NullCheck((vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 *)L_13);
			((  void (*) (vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 10)->methodPointer)((vp_GlobalCallback_2_tEA975C1BCC69328C2F87E0067B7BB8C19DBFD6C8 *)L_13, (RuntimeObject *)L_14, (RuntimeObject *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 10));
		}

IL_0064:
		{
			bool L_16 = Enumerator_MoveNext_mD72AB98A229985796153A06794D713352EE64C8D((Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A *)(Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 11));
			if (L_16)
			{
				goto IL_0056;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x88, FINALLY_006f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_006f;
	}

FINALLY_006f:
	{ // begin finally (depth: 1)
		Il2CppFakeBox<Enumerator_t29F029ADF1255FA658898295F6DB133EBE50E71A > L_17(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 12), (&V_1));
		const VirtualInvokeData& il2cpp_virtual_invoke_data__119 = il2cpp_codegen_get_interface_invoke_data(0, (&L_17), IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var);
		((  void (*) (RuntimeObject*, const RuntimeMethod*))il2cpp_virtual_invoke_data__119.methodPtr)((RuntimeObject*)(&L_17), /*hidden argument*/il2cpp_virtual_invoke_data__119.method);
		V_1 = L_17.m_Value;
		IL2CPP_RESET_LEAVE(0x88);
		IL2CPP_END_FINALLY(111)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(111)
	{
		IL2CPP_JUMP_TBL(0x88, IL_0088)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_007d:
	{
		int32_t L_18 = ___mode3;
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_0088;
		}
	}
	{
		String_t* L_19 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A * L_20 = vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146((String_t*)L_19, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20, NULL, vp_GlobalEvent_2_Send_m5B777A199A27FAF9D55E45AAA843200FB804A6EE_RuntimeMethod_var);
	}

IL_0088:
	{
		return;
	}
}
// System.Void vp_GlobalEvent`2<System.Object,System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_2__cctor_m59EEE797AFF3BE633FB88833C1C08575FFCA20F2_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_2__cctor_m59EEE797AFF3BE633FB88833C1C08575FFCA20F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_0 = ((vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields*)il2cpp_codegen_static_fields_for(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var))->get_Callbacks_0();
		((vp_GlobalEvent_2_t12287246C36DFF3C8C0AAE2326EFA5217F510174_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->set_m_Callbacks_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_GlobalEvent`3<System.Object,System.Object,System.Object>::Register(System.String,vp_GlobalCallback`3<T,U,V>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_3_Register_m1FD293106F3DC543B6358EDD23718BCD3D705172_gshared (String_t* ___name0, vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_3_Register_m1FD293106F3DC543B6358EDD23718BCD3D705172_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_3_Register_m1FD293106F3DC543B6358EDD23718BCD3D705172_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_3_Register_m1FD293106F3DC543B6358EDD23718BCD3D705172_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEvent_3_tF49037CAC8AA47874CE01660782E0BC9CB40896A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * L_8 = V_0;
		if (L_8)
		{
			goto IL_0047;
		}
	}
	{
		List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * L_9 = (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((  void (*) (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)(L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		V_0 = (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)L_9;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_10 = ((vp_GlobalEvent_3_tF49037CAC8AA47874CE01660782E0BC9CB40896A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_11 = ___name0;
		List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * L_12 = V_0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10);
		VirtActionInvoker2< RuntimeObject *, RuntimeObject * >::Invoke(15 /* System.Void System.Collections.Hashtable::Add(System.Object,System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_10, (RuntimeObject *)L_11, (RuntimeObject *)L_12);
	}

IL_0047:
	{
		List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * L_13 = V_0;
		vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * L_14 = ___callback1;
		NullCheck((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)L_13);
		((  void (*) (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *, vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)L_13, (vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return;
	}
}
// System.Void vp_GlobalEvent`3<System.Object,System.Object,System.Object>::Unregister(System.String,vp_GlobalCallback`3<T,U,V>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_3_Unregister_m0A74D31E18A206FC06B0743A70295AA42B638AA4_gshared (String_t* ___name0, vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * ___callback1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_3_Unregister_m0A74D31E18A206FC06B0743A70295AA42B638AA4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * V_0 = NULL;
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_3_Unregister_m0A74D31E18A206FC06B0743A70295AA42B638AA4_RuntimeMethod_var);
	}

IL_0013:
	{
		vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * L_3 = ___callback1;
		if (L_3)
		{
			goto IL_0021;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralB4D5B37BF7A986C138EDE89E0806F366B5CB1830, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_3_Unregister_m0A74D31E18A206FC06B0743A70295AA42B638AA4_RuntimeMethod_var);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_5 = ((vp_GlobalEvent_3_tF49037CAC8AA47874CE01660782E0BC9CB40896A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_6 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5);
		RuntimeObject * L_7 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_5, (RuntimeObject *)L_6);
		V_0 = (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * L_8 = V_0;
		if (!L_8)
		{
			goto IL_003e;
		}
	}
	{
		List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * L_9 = V_0;
		vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * L_10 = ___callback1;
		NullCheck((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)L_9);
		((  bool (*) (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *, vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)->methodPointer)((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)L_9, (vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return;
	}

IL_003e:
	{
		String_t* L_11 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		UnregisterException_t102BAD418D8101D30DFF72F6406E5B4D71DDAEFA * L_12 = vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD((String_t*)L_11, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12, NULL, vp_GlobalEvent_3_Unregister_m0A74D31E18A206FC06B0743A70295AA42B638AA4_RuntimeMethod_var);
	}
}
// System.Void vp_GlobalEvent`3<System.Object,System.Object,System.Object>::Send(System.String,T,U,V)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_3_Send_mA45EBE8784E925183657600D6EC121357F14A926_gshared (String_t* ___name0, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		RuntimeObject * L_1 = ___arg11;
		RuntimeObject * L_2 = ___arg22;
		RuntimeObject * L_3 = ___arg33;
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (String_t*, RuntimeObject *, RuntimeObject *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5)->methodPointer)((String_t*)L_0, (RuntimeObject *)L_1, (RuntimeObject *)L_2, (RuntimeObject *)L_3, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		return;
	}
}
// System.Void vp_GlobalEvent`3<System.Object,System.Object,System.Object>::Send(System.String,T,U,V,vp_GlobalEventMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_gshared (String_t* ___name0, RuntimeObject * ___arg11, RuntimeObject * ___arg22, RuntimeObject * ___arg33, int32_t ___mode4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * V_0 = NULL;
	Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		bool L_1 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229((String_t*)L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_2 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_2, (String_t*)_stringLiteral6AE999552A0D2DCA14D62E2BC8B764D377B1DD6C, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, NULL, vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_RuntimeMethod_var);
	}

IL_0013:
	{
		RuntimeObject * L_3 = ___arg11;
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_4 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_4, (String_t*)_stringLiteralE044DB5CACC7C1E1DED3C45FA7472331FE5E6246, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, NULL, vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_RuntimeMethod_var);
	}

IL_0026:
	{
		RuntimeObject * L_5 = ___arg22;
		if (L_5)
		{
			goto IL_0039;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_6 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_6, (String_t*)_stringLiteralB38FACF4A8F9F6E8FC7192D1CBC326386647F4C1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6, NULL, vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_RuntimeMethod_var);
	}

IL_0039:
	{
		RuntimeObject * L_7 = ___arg33;
		if (L_7)
		{
			goto IL_004c;
		}
	}
	{
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_8 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_8, (String_t*)_stringLiteralACDB94344573594F5AE5956EF0E2477E3C8C2B07, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, NULL, vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_RuntimeMethod_var);
	}

IL_004c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_9 = ((vp_GlobalEvent_3_tF49037CAC8AA47874CE01660782E0BC9CB40896A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->get_m_Callbacks_0();
		String_t* L_10 = ___name0;
		NullCheck((Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_9);
		RuntimeObject * L_11 = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(21 /* System.Object System.Collections.Hashtable::get_Item(System.Object) */, (Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 *)L_9, (RuntimeObject *)L_10);
		V_0 = (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)));
		List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * L_12 = V_0;
		if (!L_12)
		{
			goto IL_0091;
		}
	}
	{
		List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE * L_13 = V_0;
		NullCheck((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)L_13);
		Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE  L_14 = ((  Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE  (*) (List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9)->methodPointer)((List_1_tF6FB4804504C4BCA0677BCB1ADB871B5FE8242FE *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 9));
		V_1 = (Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE )L_14;
	}

IL_0067:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0078;
		}

IL_0069:
		{
			vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD * L_15 = Enumerator_get_Current_m825CC53DDF810CFE7D2ACB6ED12604CCE71E3FB9((Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE *)(Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 10));
			RuntimeObject * L_16 = ___arg11;
			RuntimeObject * L_17 = ___arg22;
			RuntimeObject * L_18 = ___arg33;
			NullCheck((vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD *)L_15);
			((  void (*) (vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 11)->methodPointer)((vp_GlobalCallback_3_t0E96DEBFE7E951C9E38FCF40601F533B1E621CBD *)L_15, (RuntimeObject *)L_16, (RuntimeObject *)L_17, (RuntimeObject *)L_18, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 11));
		}

IL_0078:
		{
			bool L_19 = Enumerator_MoveNext_m4948DA95F24F1481F1865E9A73E30BE584D858F5((Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE *)(Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE *)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 12));
			if (L_19)
			{
				goto IL_0069;
			}
		}

IL_0081:
		{
			IL2CPP_LEAVE(0x9D, FINALLY_0083);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0083;
	}

FINALLY_0083:
	{ // begin finally (depth: 1)
		Il2CppFakeBox<Enumerator_t05F0DFF54C1D19719816D659E8B8F5E72C6E03EE > L_20(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 13), (&V_1));
		const VirtualInvokeData& il2cpp_virtual_invoke_data__139 = il2cpp_codegen_get_interface_invoke_data(0, (&L_20), IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var);
		((  void (*) (RuntimeObject*, const RuntimeMethod*))il2cpp_virtual_invoke_data__139.methodPtr)((RuntimeObject*)(&L_20), /*hidden argument*/il2cpp_virtual_invoke_data__139.method);
		V_1 = L_20.m_Value;
		IL2CPP_RESET_LEAVE(0x9D);
		IL2CPP_END_FINALLY(131)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(131)
	{
		IL2CPP_JUMP_TBL(0x9D, IL_009d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0091:
	{
		int32_t L_21 = ___mode4;
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_009d;
		}
	}
	{
		String_t* L_22 = ___name0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		SendException_t5961FDC91656A4978A9BDDF31F34AAFE0CC56D8A * L_23 = vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146((String_t*)L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23, NULL, vp_GlobalEvent_3_Send_m5952115843B3538079FBA5497073EE827CA35943_RuntimeMethod_var);
	}

IL_009d:
	{
		return;
	}
}
// System.Void vp_GlobalEvent`3<System.Object,System.Object,System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_GlobalEvent_3__cctor_m59ABA036DB2D978891CF7FB11877A54CE62ED7CC_gshared (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_GlobalEvent_3__cctor_m59ABA036DB2D978891CF7FB11877A54CE62ED7CC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var);
		Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * L_0 = ((vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_StaticFields*)il2cpp_codegen_static_fields_for(vp_GlobalEventInternal_tFF031D87D1D13BE8935C7EDEA8878A3E8890FB8D_il2cpp_TypeInfo_var))->get_Callbacks_0();
		((vp_GlobalEvent_3_tF49037CAC8AA47874CE01660782E0BC9CB40896A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0)))->set_m_Callbacks_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Message`1_Sender`1<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Sender_1__ctor_m293B445E01906DCD31BF9CF2C7D70FB5A2C34685_gshared (Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void vp_Message`1_Sender`1<System.Object,System.Object>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Sender_1_Invoke_m69F3D9DC4411E223EC8ABF71619D611FE346F3D0_gshared (Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___value0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___value0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___value0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___value0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___value0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___value0);
					else
						GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___value0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___value0);
					else
						VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___value0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___value0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult vp_Message`1_Sender`1<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Sender_1_BeginInvoke_m3FA68AFEE762495C26ECA686808A495E6B1A4D03_gshared (Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59 * __this, RuntimeObject * ___value0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void vp_Message`1_Sender`1<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Sender_1_EndInvoke_m9E1E4ED0DD1CC8DC90782CB02BE2AF40C7842228_gshared (Sender_1_t7C77DCD4ADDBD98CCD1E6AF4562E723E9FB9CE59 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Message`1<System.Object>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message_1__ctor_m3BEE109C3CAC8F729A69CF509468BE1A8449429E_gshared (vp_Message_1_t07E944A5FFD733133B2B1970B4A9AC638975BE3A * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		NullCheck((vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989 *)__this);
		vp_Message__ctor_m940941685CF5B3BC416C682A02E2B3B9FAE373D8((vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_Message`1<System.Object>::InitFields()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message_1_InitFields_m2FF45187A9344F1507111BEB6C12C1E134993287_gshared (vp_Message_1_t07E944A5FFD733133B2B1970B4A9AC638975BE3A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Message_1_InitFields_m2FF45187A9344F1507111BEB6C12C1E134993287_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)SZArrayNew(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE_il2cpp_TypeInfo_var, (uint32_t)1);
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_1 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)L_0;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_2 = vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		FieldInfo_t * L_3 = Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5((Type_t *)L_2, (String_t*)_stringLiteral9BC2575C3930437E80555F78757B783C842E8E66, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (FieldInfo_t *)L_3);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_Fields_4(L_1);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)1);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (Sender_1_tDFCB1052068B848B279D11A58C6D4839CBF60347_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_DelegateTypes_5(L_5);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_8 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)il2cpp_codegen_object_new(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62(L_8, /*hidden argument*/Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62_RuntimeMethod_var);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_9 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_8;
		NullCheck((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_9);
		Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_9, (String_t*)_stringLiteralE075B7F92BF500D83412F32E8E2BB56C1176DA27, (int32_t)0, /*hidden argument*/Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_Prefixes_8(L_9);
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_10 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_10)
		{
			goto IL_0091;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_11 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_11);
		int32_t L_12 = 0;
		MethodInfo_t * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		bool L_14 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_13, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_15 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_15);
		int32_t L_16 = 0;
		FieldInfo_t * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_18 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_18);
		int32_t L_19 = 0;
		MethodInfo_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_21 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_21);
		int32_t L_22 = 0;
		Type_t * L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_24 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_23, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_17, (MethodInfo_t *)L_20, (Type_t *)L_24, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void vp_Message`1<System.Object>::Register(System.Object,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message_1_Register_mFD55557054769ECF432EC6703B92B5B2DDB4DAD2_gshared (vp_Message_1_t07E944A5FFD733133B2B1970B4A9AC638975BE3A * __this, RuntimeObject * ___t0, String_t* ___m1, int32_t ___v2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___m1;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		RuntimeObject * L_1 = ___t0;
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_2 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		int32_t L_3 = ___v2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		FieldInfo_t * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		String_t* L_6 = ___m1;
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_7 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		int32_t L_8 = ___v2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Type_t * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_11 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_10, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_AddExternalMethodToField_m167005BC363AE73C25FF8679197A5C8E13EF63E7((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (RuntimeObject *)L_1, (FieldInfo_t *)L_5, (String_t*)L_6, (Type_t *)L_11, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_Message`1<System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message_1_Unregister_mC1C972A1A59F252158400277821422A47BE655AE_gshared (vp_Message_1_t07E944A5FFD733133B2B1970B4A9AC638975BE3A * __this, RuntimeObject * ___t0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___t0;
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_1 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_1);
		int32_t L_2 = 0;
		FieldInfo_t * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_RemoveExternalMethodFromField_mE1961E2F41C102B5B7FDC0C24539E47F7B5D5041((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (RuntimeObject *)L_0, (FieldInfo_t *)L_3, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Message`2_Sender`2<System.Object,System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Sender_2__ctor_mEBEE5C43111008499DAE68F37534A46DE3E7A7D2_gshared (Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult vp_Message`2_Sender`2<System.Object,System.Object,System.Object,System.Object>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Sender_2_Invoke_mA9920DF684E56BB48DC35752D92083B6B27CC6CF_gshared (Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___value0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, ___value0);
					else
						result = GenericVirtFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, ___value0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___value0);
					else
						result = VirtFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___value0);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef RuntimeObject * (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					result = ((FunctionPointerType)targetMethodPointer)(___value0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___value0);
					else
						result = GenericVirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(targetMethod, targetThis, ___value0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___value0);
					else
						result = VirtFuncInvoker1< RuntimeObject *, RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___value0);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, ___value0, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult vp_Message`2_Sender`2<System.Object,System.Object,System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Sender_2_BeginInvoke_m99204D384B89D9A34A057B55D87C6D21376F27C4_gshared (Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80 * __this, RuntimeObject * ___value0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// TResult vp_Message`2_Sender`2<System.Object,System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Sender_2_EndInvoke_m96E19684980DA78BE9F4B238952B48596E0DB52D_gshared (Sender_2_tF443C4D679AC08966285D806C9EC4378C63D2C80 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Message`2<System.Object,System.Object>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message_2__ctor_mC6D559BBF491C83EA6DA00988E2AFA9C407D3B5E_gshared (vp_Message_2_t30D2E82656A74C71DF80691139A062CF3308E8EE * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		NullCheck((vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989 *)__this);
		vp_Message__ctor_m940941685CF5B3BC416C682A02E2B3B9FAE373D8((vp_Message_tD0216E33C61FB73DDC383FC13A99AE45B8C97989 *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_Message`2<System.Object,System.Object>::InitFields()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message_2_InitFields_m7310D8CCD00D76EB075DFA2C58FB566E0007996A_gshared (vp_Message_2_t30D2E82656A74C71DF80691139A062CF3308E8EE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Message_2_InitFields_m7310D8CCD00D76EB075DFA2C58FB566E0007996A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)SZArrayNew(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE_il2cpp_TypeInfo_var, (uint32_t)1);
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_1 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)L_0;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_2 = vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		FieldInfo_t * L_3 = Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5((Type_t *)L_2, (String_t*)_stringLiteral9BC2575C3930437E80555F78757B783C842E8E66, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (FieldInfo_t *)L_3);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_Fields_4(L_1);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_4 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)1);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_5 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_4;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_6 = { reinterpret_cast<intptr_t> (Sender_2_t557F66C2707C7BCB2312ED78EB6DB3F796FDE3AC_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_7);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_DelegateTypes_5(L_5);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_8 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)il2cpp_codegen_object_new(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62(L_8, /*hidden argument*/Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62_RuntimeMethod_var);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_9 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_8;
		NullCheck((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_9);
		Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_9, (String_t*)_stringLiteralE075B7F92BF500D83412F32E8E2BB56C1176DA27, (int32_t)0, /*hidden argument*/Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_Prefixes_8(L_9);
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_10 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_10)
		{
			goto IL_0091;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_11 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_11);
		int32_t L_12 = 0;
		MethodInfo_t * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		bool L_14 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_13, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0091;
		}
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_15 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_15);
		int32_t L_16 = 0;
		FieldInfo_t * L_17 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_18 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_18);
		int32_t L_19 = 0;
		MethodInfo_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_21 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_21);
		int32_t L_22 = 0;
		Type_t * L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_24 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_23, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_17, (MethodInfo_t *)L_20, (Type_t *)L_24, /*hidden argument*/NULL);
	}

IL_0091:
	{
		return;
	}
}
// System.Void vp_Message`2<System.Object,System.Object>::Register(System.Object,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message_2_Register_mB005EF8F211182B6367C620CB7FD4BA6537E7B4A_gshared (vp_Message_2_t30D2E82656A74C71DF80691139A062CF3308E8EE * __this, RuntimeObject * ___t0, String_t* ___m1, int32_t ___v2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___m1;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		RuntimeObject * L_1 = ___t0;
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_2 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_2);
		int32_t L_3 = 0;
		FieldInfo_t * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		String_t* L_5 = ___m1;
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_6 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_6);
		int32_t L_7 = 0;
		Type_t * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_9 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_8, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_AddExternalMethodToField_m167005BC363AE73C25FF8679197A5C8E13EF63E7((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (RuntimeObject *)L_1, (FieldInfo_t *)L_4, (String_t*)L_5, (Type_t *)L_9, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_Message`2<System.Object,System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Message_2_Unregister_mF504BCFBD38AF026AB64D1CE3A7D925788E8D0D9_gshared (vp_Message_2_t30D2E82656A74C71DF80691139A062CF3308E8EE * __this, RuntimeObject * ___t0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___t0;
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_1 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_1);
		int32_t L_2 = 0;
		FieldInfo_t * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_RemoveExternalMethodFromField_mE1961E2F41C102B5B7FDC0C24539E47F7B5D5041((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (RuntimeObject *)L_0, (FieldInfo_t *)L_3, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_TargetEventReturn`1<System.Object>::Register(System.Object,System.String,System.Func`1<R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_1_Register_m0FB46C04A3BE32207A707A4876567027CA2743F2_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_1_Register_m0FB46C04A3BE32207A707A4876567027CA2743F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, (int32_t)4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`1<System.Object>::Unregister(System.Object,System.String,System.Func`1<R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_1_Unregister_m469F0813AE9A4A752CCEC06AF95E09A79269CCE3_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_1_Unregister_m469F0813AE9A4A752CCEC06AF95E09A79269CCE3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`1<System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_1_Unregister_mA54E066D4A9558C9B2D85A083BD0CBAF2DD79659_gshared (RuntimeObject * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_1_Unregister_mA54E066D4A9558C9B2D85A083BD0CBAF2DD79659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)NULL, (Delegate_t *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`1<System.Object>::Unregister(UnityEngine.Component)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_1_Unregister_m943E81775CE445EF3BCDC2A22A99158C2EBA64CC_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___component0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_1_Unregister_m943E81775CE445EF3BCDC2A22A99158C2EBA64CC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___component0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m8455936D2115A5A13E469F8FFBF9BA879F2587E6((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_0, /*hidden argument*/NULL);
		return;
	}
}
// R vp_TargetEventReturn`1<System.Object>::Send(System.Object,System.String,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_TargetEventReturn_1_Send_mEA282C4FCC4DB09854FCDBEBCFDB729FB02503E1_gshared (RuntimeObject * ___target0, String_t* ___eventName1, int32_t ___options2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_1_Send_mEA282C4FCC4DB09854FCDBEBCFDB729FB02503E1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)0, (int32_t)4, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_001f:
	{
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_8 = V_0;
		NullCheck((Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *)((Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *)((Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_1 = (RuntimeObject *)L_9;
		goto IL_003e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002e;
		throw e;
	}

CATCH_002e:
	{ // begin catch(System.Object)
		String_t* L_10 = ___eventName1;
		String_t* L_11 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_10, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_11;
		goto IL_0000;
	} // end catch (depth: 1)

IL_003e:
	{
		RuntimeObject * L_12 = V_1;
		return L_12;
	}
}
// R vp_TargetEventReturn`1<System.Object>::SendUpwards(UnityEngine.Component,System.String,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_TargetEventReturn_1_SendUpwards_m767FDA538F65CA8AD887A3B0BF7E15887E9851B7_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target0, String_t* ___eventName1, int32_t ___options2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_1_SendUpwards_m767FDA538F65CA8AD887A3B0BF7E15887E9851B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)1, (int32_t)4, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_001f:
	{
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_8 = V_0;
		NullCheck((Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *)((Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		RuntimeObject * L_9 = ((  RuntimeObject * (*) (Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *)((Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_1 = (RuntimeObject *)L_9;
		goto IL_003e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002e;
		throw e;
	}

CATCH_002e:
	{ // begin catch(System.Object)
		String_t* L_10 = ___eventName1;
		String_t* L_11 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_10, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_11;
		goto IL_0000;
	} // end catch (depth: 1)

IL_003e:
	{
		RuntimeObject * L_12 = V_1;
		return L_12;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_TargetEventReturn`2<System.Object,System.Object>::Register(System.Object,System.String,System.Func`2<T,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_2_Register_m3C9392F59BA09F5809754E122A54C8CD5A0F8789_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_2_Register_m3C9392F59BA09F5809754E122A54C8CD5A0F8789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, (int32_t)5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`2<System.Object,System.Object>::Unregister(System.Object,System.String,System.Func`2<T,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_2_Unregister_m928B4EDD7C6F24678815CD19192F17543DA324BE_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_2_Unregister_m928B4EDD7C6F24678815CD19192F17543DA324BE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`2<System.Object,System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_2_Unregister_m1EB26964D306927832BE535538797EFD39B0534A_gshared (RuntimeObject * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_2_Unregister_m1EB26964D306927832BE535538797EFD39B0534A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)NULL, (Delegate_t *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// R vp_TargetEventReturn`2<System.Object,System.Object>::Send(System.Object,System.String,T,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_TargetEventReturn_2_Send_mF71934FC138D1E8205983A52C03F45D5939B4213_gshared (RuntimeObject * ___target0, String_t* ___eventName1, RuntimeObject * ___arg2, int32_t ___options3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_2_Send_mF71934FC138D1E8205983A52C03F45D5939B4213_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)0, (int32_t)5, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_001f:
	{
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_8 = V_0;
		RuntimeObject * L_9 = ___arg2;
		NullCheck((Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)((Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)((Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_1 = (RuntimeObject *)L_10;
		goto IL_003f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002f;
		throw e;
	}

CATCH_002f:
	{ // begin catch(System.Object)
		String_t* L_11 = ___eventName1;
		String_t* L_12 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_11, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_12;
		goto IL_0000;
	} // end catch (depth: 1)

IL_003f:
	{
		RuntimeObject * L_13 = V_1;
		return L_13;
	}
}
// R vp_TargetEventReturn`2<System.Object,System.Object>::SendUpwards(UnityEngine.Component,System.String,T,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_TargetEventReturn_2_SendUpwards_mA4BEC7B1A4DB61610AB8E7B851C9A9CB60788AC7_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target0, String_t* ___eventName1, RuntimeObject * ___arg2, int32_t ___options3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_2_SendUpwards_mA4BEC7B1A4DB61610AB8E7B851C9A9CB60788AC7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)1, (int32_t)5, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_001f:
	{
	}

IL_0020:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_8 = V_0;
		RuntimeObject * L_9 = ___arg2;
		NullCheck((Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)((Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		RuntimeObject * L_10 = ((  RuntimeObject * (*) (Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)((Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_1 = (RuntimeObject *)L_10;
		goto IL_003f;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002f;
		throw e;
	}

CATCH_002f:
	{ // begin catch(System.Object)
		String_t* L_11 = ___eventName1;
		String_t* L_12 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_11, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_12;
		goto IL_0000;
	} // end catch (depth: 1)

IL_003f:
	{
		RuntimeObject * L_13 = V_1;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_TargetEventReturn`3<System.Object,System.Object,System.Object>::Register(System.Object,System.String,System.Func`3<T,U,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_3_Register_mE9CADC56337B0DB60F2031A5AD8DBBF5788A04ED_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_3_Register_mE9CADC56337B0DB60F2031A5AD8DBBF5788A04ED_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, (int32_t)6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`3<System.Object,System.Object,System.Object>::Unregister(System.Object,System.String,System.Func`3<T,U,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_3_Unregister_mC6499B4F1490E5D5922E25E210FC7B0C49D32795_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_3_Unregister_mC6499B4F1490E5D5922E25E210FC7B0C49D32795_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`3<System.Object,System.Object,System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_3_Unregister_m981527B87DEDE801D932FA05E486D5CA1E5AD937_gshared (RuntimeObject * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_3_Unregister_m981527B87DEDE801D932FA05E486D5CA1E5AD937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)NULL, (Delegate_t *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// R vp_TargetEventReturn`3<System.Object,System.Object,System.Object>::Send(System.Object,System.String,T,U,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_TargetEventReturn_3_Send_m5B1641CFEC457D75678CA7F0C2F7F651ABA7592E_gshared (RuntimeObject * ___target0, String_t* ___eventName1, RuntimeObject * ___arg12, RuntimeObject * ___arg23, int32_t ___options4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_3_Send_m5B1641CFEC457D75678CA7F0C2F7F651ABA7592E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options4;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)0, (int32_t)6, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options4;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_0021:
	{
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_8 = V_0;
		RuntimeObject * L_9 = ___arg12;
		RuntimeObject * L_10 = ___arg23;
		NullCheck((Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *)((Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *)((Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_1 = (RuntimeObject *)L_11;
		goto IL_0042;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0032;
		throw e;
	}

CATCH_0032:
	{ // begin catch(System.Object)
		String_t* L_12 = ___eventName1;
		String_t* L_13 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_12, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_13;
		goto IL_0000;
	} // end catch (depth: 1)

IL_0042:
	{
		RuntimeObject * L_14 = V_1;
		return L_14;
	}
}
// R vp_TargetEventReturn`3<System.Object,System.Object,System.Object>::SendUpwards(UnityEngine.Component,System.String,T,U,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_TargetEventReturn_3_SendUpwards_m1044D8330CE51AEE84CA7D368608FEA5EE9173E5_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target0, String_t* ___eventName1, RuntimeObject * ___arg12, RuntimeObject * ___arg23, int32_t ___options4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_3_SendUpwards_m1044D8330CE51AEE84CA7D368608FEA5EE9173E5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options4;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)1, (int32_t)6, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options4;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_0021:
	{
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_8 = V_0;
		RuntimeObject * L_9 = ___arg12;
		RuntimeObject * L_10 = ___arg23;
		NullCheck((Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *)((Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		RuntimeObject * L_11 = ((  RuntimeObject * (*) (Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *)((Func_3_t2FD707432BF597CE55512B222DEF706BDEBAE032 *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_1 = (RuntimeObject *)L_11;
		goto IL_0042;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0032;
		throw e;
	}

CATCH_0032:
	{ // begin catch(System.Object)
		String_t* L_12 = ___eventName1;
		String_t* L_13 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_12, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_13;
		goto IL_0000;
	} // end catch (depth: 1)

IL_0042:
	{
		RuntimeObject * L_14 = V_1;
		return L_14;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_TargetEventReturn`4<System.Object,System.Object,System.Object,System.Object>::Register(System.Object,System.String,System.Func`4<T,U,V,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_4_Register_m12DA30F5069C305A184975243D563643B2D345A8_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_4_Register_m12DA30F5069C305A184975243D563643B2D345A8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, (int32_t)7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`4<System.Object,System.Object,System.Object,System.Object>::Unregister(System.Object,System.String,System.Func`4<T,U,V,R>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_4_Unregister_mD41416B1FB2E4C2D16A70DC95C80F58DA77D6340_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_4_Unregister_mD41416B1FB2E4C2D16A70DC95C80F58DA77D6340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEventReturn`4<System.Object,System.Object,System.Object,System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEventReturn_4_Unregister_m9E9646F122A7A1664598747675E5A043942677E7_gshared (RuntimeObject * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_4_Unregister_m9E9646F122A7A1664598747675E5A043942677E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)NULL, (Delegate_t *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// R vp_TargetEventReturn`4<System.Object,System.Object,System.Object,System.Object>::Send(System.Object,System.String,T,U,V,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_TargetEventReturn_4_Send_m5824C02E1489177550A07B4E9899AD0693991F6A_gshared (RuntimeObject * ___target0, String_t* ___eventName1, RuntimeObject * ___arg12, RuntimeObject * ___arg23, RuntimeObject * ___arg34, int32_t ___options5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_4_Send_m5824C02E1489177550A07B4E9899AD0693991F6A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options5;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)0, (int32_t)7, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options5;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_0021:
	{
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_8 = V_0;
		RuntimeObject * L_9 = ___arg12;
		RuntimeObject * L_10 = ___arg23;
		RuntimeObject * L_11 = ___arg34;
		NullCheck((Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *)((Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		RuntimeObject * L_12 = ((  RuntimeObject * (*) (Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *)((Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_9, (RuntimeObject *)L_10, (RuntimeObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_1 = (RuntimeObject *)L_12;
		goto IL_0044;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0034;
		throw e;
	}

CATCH_0034:
	{ // begin catch(System.Object)
		String_t* L_13 = ___eventName1;
		String_t* L_14 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_13, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_14;
		goto IL_0000;
	} // end catch (depth: 1)

IL_0044:
	{
		RuntimeObject * L_15 = V_1;
		return L_15;
	}
}
// R vp_TargetEventReturn`4<System.Object,System.Object,System.Object,System.Object>::SendUpwards(UnityEngine.Component,System.String,T,U,V,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * vp_TargetEventReturn_4_SendUpwards_mE2A7AF6484398084184D8A2DEBEF33995BAF26E4_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target0, String_t* ___eventName1, RuntimeObject * ___arg12, RuntimeObject * ___arg23, RuntimeObject * ___arg34, int32_t ___options5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEventReturn_4_SendUpwards_mE2A7AF6484398084184D8A2DEBEF33995BAF26E4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options5;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)1, (int32_t)7, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options5;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_1), sizeof(RuntimeObject *));
		RuntimeObject * L_7 = V_1;
		return L_7;
	}

IL_0021:
	{
	}

IL_0022:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_8 = V_0;
		RuntimeObject * L_9 = ___arg12;
		RuntimeObject * L_10 = ___arg23;
		RuntimeObject * L_11 = ___arg34;
		NullCheck((Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *)((Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		RuntimeObject * L_12 = ((  RuntimeObject * (*) (Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *)((Func_4_t34EB2F617667F35179FF0E6462B57DB1C4CB16AA *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_9, (RuntimeObject *)L_10, (RuntimeObject *)L_11, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		V_1 = (RuntimeObject *)L_12;
		goto IL_0044;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0034;
		throw e;
	}

CATCH_0034:
	{ // begin catch(System.Object)
		String_t* L_13 = ___eventName1;
		String_t* L_14 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_13, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_14;
		goto IL_0000;
	} // end catch (depth: 1)

IL_0044:
	{
		RuntimeObject * L_15 = V_1;
		return L_15;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_TargetEvent`1<System.Object>::Register(System.Object,System.String,System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_1_Register_mDEC5F0CB1A3157D51B9AD2E6CCBBB8A9D4367D8E_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_1_Register_mDEC5F0CB1A3157D51B9AD2E6CCBBB8A9D4367D8E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, (int32_t)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`1<System.Object>::Unregister(System.Object,System.String,System.Action`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_1_Unregister_mE8F50534CAD6ACB131C352A8FC9BCF2965EBF462_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_1_Unregister_mE8F50534CAD6ACB131C352A8FC9BCF2965EBF462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`1<System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_1_Unregister_m5AABE7B7ABE1027AD028EAC28778D7B0B2246FA4_gshared (RuntimeObject * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_1_Unregister_m5AABE7B7ABE1027AD028EAC28778D7B0B2246FA4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)NULL, (Delegate_t *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`1<System.Object>::Send(System.Object,System.String,T,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_1_Send_m00ECA85A639711ABDB10FDB6814B2B2096839A1F_gshared (RuntimeObject * ___target0, String_t* ___eventName1, RuntimeObject * ___arg2, int32_t ___options3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_1_Send_m00ECA85A639711ABDB10FDB6814B2B2096839A1F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)0, (int32_t)1, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_7 = V_0;
		RuntimeObject * L_8 = ___arg2;
		NullCheck((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		((  void (*) (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		goto IL_0035;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0025;
		throw e;
	}

CATCH_0025:
	{ // begin catch(System.Object)
		String_t* L_9 = ___eventName1;
		String_t* L_10 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_9, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_10;
		goto IL_0000;
	} // end catch (depth: 1)

IL_0035:
	{
		return;
	}
}
// System.Void vp_TargetEvent`1<System.Object>::SendUpwards(UnityEngine.Component,System.String,T,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_1_SendUpwards_m7B41D6CA1AE1F4EBA0C5868CA3C4DAB066E92D2B_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target0, String_t* ___eventName1, RuntimeObject * ___arg2, int32_t ___options3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_1_SendUpwards_m7B41D6CA1AE1F4EBA0C5868CA3C4DAB066E92D2B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)1, (int32_t)1, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0016;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_7 = V_0;
		RuntimeObject * L_8 = ___arg2;
		NullCheck((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		((  void (*) (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)((Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		goto IL_0035;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0025;
		throw e;
	}

CATCH_0025:
	{ // begin catch(System.Object)
		String_t* L_9 = ___eventName1;
		String_t* L_10 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_9, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_10;
		goto IL_0000;
	} // end catch (depth: 1)

IL_0035:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_TargetEvent`2<System.Object,System.Object>::Register(System.Object,System.String,System.Action`2<T,U>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_2_Register_mFFA414B55C3D7802ABACF85B34A73815854272A4_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_2_Register_mFFA414B55C3D7802ABACF85B34A73815854272A4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, (int32_t)2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`2<System.Object,System.Object>::Unregister(System.Object,System.String,System.Action`2<T,U>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_2_Unregister_mE280B2398E29ED4C314F37F7878D56E6742A819B_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_2_Unregister_mE280B2398E29ED4C314F37F7878D56E6742A819B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`2<System.Object,System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_2_Unregister_m3B58496214F2DA0651A83EF689CF9D81EE287B6E_gshared (RuntimeObject * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_2_Unregister_m3B58496214F2DA0651A83EF689CF9D81EE287B6E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)NULL, (Delegate_t *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`2<System.Object,System.Object>::Send(System.Object,System.String,T,U,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_2_Send_m6F626C51DCE56C52072D4189297AF7E84EA2E6A7_gshared (RuntimeObject * ___target0, String_t* ___eventName1, RuntimeObject * ___arg12, RuntimeObject * ___arg23, int32_t ___options4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_2_Send_m6F626C51DCE56C52072D4189297AF7E84EA2E6A7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options4;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)0, (int32_t)2, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options4;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_7 = V_0;
		RuntimeObject * L_8 = ___arg12;
		RuntimeObject * L_9 = ___arg23;
		NullCheck((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		((  void (*) (Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		goto IL_0038;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0028;
		throw e;
	}

CATCH_0028:
	{ // begin catch(System.Object)
		String_t* L_10 = ___eventName1;
		String_t* L_11 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_10, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_11;
		goto IL_0000;
	} // end catch (depth: 1)

IL_0038:
	{
		return;
	}
}
// System.Void vp_TargetEvent`2<System.Object,System.Object>::SendUpwards(UnityEngine.Component,System.String,T,U,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_2_SendUpwards_m2FBD71B9B6DB566EB6BECA6FE7CFE89DEE1497CB_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target0, String_t* ___eventName1, RuntimeObject * ___arg12, RuntimeObject * ___arg23, int32_t ___options4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_2_SendUpwards_m2FBD71B9B6DB566EB6BECA6FE7CFE89DEE1497CB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options4;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)1, (int32_t)2, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options4;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_7 = V_0;
		RuntimeObject * L_8 = ___arg12;
		RuntimeObject * L_9 = ___arg23;
		NullCheck((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		((  void (*) (Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)((Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		goto IL_0038;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_0028;
		throw e;
	}

CATCH_0028:
	{ // begin catch(System.Object)
		String_t* L_10 = ___eventName1;
		String_t* L_11 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_10, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_11;
		goto IL_0000;
	} // end catch (depth: 1)

IL_0038:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_TargetEvent`3<System.Object,System.Object,System.Object>::Register(System.Object,System.String,System.Action`3<T,U,V>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_3_Register_m1C8C60BE8E73FF61BD98A05E95ABFD5EBFDAFA72_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_3_Register_m1C8C60BE8E73FF61BD98A05E95ABFD5EBFDAFA72_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, (int32_t)3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`3<System.Object,System.Object,System.Object>::Unregister(System.Object,System.String,System.Action`3<T,U,V>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_3_Unregister_m9F51B5AE991AD52AB668788029CFD11753D74A7D_gshared (RuntimeObject * ___target0, String_t* ___eventName1, Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * ___callback2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_3_Unregister_m9F51B5AE991AD52AB668788029CFD11753D74A7D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 * L_2 = ___callback2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)L_1, (Delegate_t *)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`3<System.Object,System.Object,System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_3_Unregister_m5E18276E850303977BA5BD095B9761B62467A4DC_gshared (RuntimeObject * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_3_Unregister_m5E18276E850303977BA5BD095B9761B62467A4DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC((RuntimeObject *)L_0, (String_t*)NULL, (Delegate_t *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_TargetEvent`3<System.Object,System.Object,System.Object>::Send(System.Object,System.String,T,U,V,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_3_Send_m3E96761380D34EE709938D3BB8878E61DF4E5351_gshared (RuntimeObject * ___target0, String_t* ___eventName1, RuntimeObject * ___arg12, RuntimeObject * ___arg23, RuntimeObject * ___arg34, int32_t ___options5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_3_Send_m3E96761380D34EE709938D3BB8878E61DF4E5351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		RuntimeObject * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options5;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)0, (int32_t)3, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options5;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_7 = V_0;
		RuntimeObject * L_8 = ___arg12;
		RuntimeObject * L_9 = ___arg23;
		RuntimeObject * L_10 = ___arg34;
		NullCheck((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		((  void (*) (Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_8, (RuntimeObject *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.Object)
		String_t* L_11 = ___eventName1;
		String_t* L_12 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_11, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_12;
		goto IL_0000;
	} // end catch (depth: 1)

IL_003a:
	{
		return;
	}
}
// System.Void vp_TargetEvent`3<System.Object,System.Object,System.Object>::SendUpwards(UnityEngine.Component,System.String,T,U,V,vp_TargetEventOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_TargetEvent_3_SendUpwards_m4687AE23C56E2A679DBA9C7F3C96ABF7B8CEA2E4_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target0, String_t* ___eventName1, RuntimeObject * ___arg12, RuntimeObject * ___arg23, RuntimeObject * ___arg34, int32_t ___options5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_TargetEvent_3_SendUpwards_m4687AE23C56E2A679DBA9C7F3C96ABF7B8CEA2E4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	{
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___target0;
		String_t* L_1 = ___eventName1;
		int32_t L_2 = ___options5;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		Delegate_t * L_3 = vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603((RuntimeObject *)L_0, (String_t*)L_1, (bool)1, (int32_t)3, (int32_t)L_2, /*hidden argument*/NULL);
		V_0 = (Delegate_t *)L_3;
		Delegate_t * L_4 = V_0;
		if (L_4)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_5 = ___eventName1;
		int32_t L_6 = ___options5;
		IL2CPP_RUNTIME_CLASS_INIT(vp_TargetEventHandler_t3C2FEE62306C5A2B26E2EAB64078F33629A4377E_il2cpp_TypeInfo_var);
		vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
	}

IL_0019:
	try
	{ // begin try (depth: 1)
		Delegate_t * L_7 = V_0;
		RuntimeObject * L_8 = ___arg12;
		RuntimeObject * L_9 = ___arg23;
		RuntimeObject * L_10 = ___arg34;
		NullCheck((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))));
		((  void (*) (Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)((Action_3_t18AB8FA585A6EC4D52B8AA1EB7193DD21CBE7849 *)Castclass((RuntimeObject*)L_7, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0))), (RuntimeObject *)L_8, (RuntimeObject *)L_9, (RuntimeObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		goto IL_003a;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_002a;
		throw e;
	}

CATCH_002a:
	{ // begin catch(System.Object)
		String_t* L_11 = ___eventName1;
		String_t* L_12 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)L_11, (String_t*)_stringLiteral53A0ACFAD59379B3E050338BF9F23CFC172EE787, /*hidden argument*/NULL);
		___eventName1 = (String_t*)L_12;
		goto IL_0000;
	} // end catch (depth: 1)

IL_003a:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Value`1_Getter`1<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Getter_1__ctor_m9189A9180229556B8DF6F067FE101426DF20A8C4_gshared (Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T vp_Value`1_Getter`1<System.Object,System.Object>::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Getter_1_Invoke_m88D218C9ED4C1D1F43EEC09CACB79DCB7958E7C2_gshared (Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * __this, const RuntimeMethod* method)
{
	RuntimeObject * result = NULL;
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef RuntimeObject * (*FunctionPointerType) (const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef RuntimeObject * (*FunctionPointerType) (void*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = GenericInterfaceFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, targetThis);
					else
						result = GenericVirtFuncInvoker0< RuntimeObject * >::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						result = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						result = VirtFuncInvoker0< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef RuntimeObject * (*FunctionPointerType) (void*, const RuntimeMethod*);
				result = ((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
	return result;
}
// System.IAsyncResult vp_Value`1_Getter`1<System.Object,System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Getter_1_BeginInvoke_m23970EC172F65E5ADF6423D6EBB245EF51C2B127_gshared (Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * __this, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// T vp_Value`1_Getter`1<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Getter_1_EndInvoke_m795D27E7380BD46FB0DDF47698D0EDBBEE3DD0A0_gshared (Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	RuntimeObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (RuntimeObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Value`1_Setter`1<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Setter_1__ctor_m2DDE9E8A55BAF07C4A4333D99F2C2108E1C21523_gshared (Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void vp_Value`1_Setter`1<System.Object,System.Object>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Setter_1_Invoke_mF5BB8CE97927F58322D3B7CD804B453C84F7AAE0_gshared (Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___o0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___o0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___o0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___o0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___o0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___o0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___o0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___o0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___o0);
					else
						GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___o0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___o0);
					else
						VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___o0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___o0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult vp_Value`1_Setter`1<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Setter_1_BeginInvoke_m5C8C661010544E9C1311D782ED0596DB50E8C3C6_gshared (Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * __this, RuntimeObject * ___o0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___o0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void vp_Value`1_Setter`1<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Setter_1_EndInvoke_mC5F905B27EE5CE687BEA972298718795B2BE66D1_gshared (Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Reflection.FieldInfo[] vp_Value`1<System.Object>::get_Fields()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* vp_Value_1_get_Fields_mD18B7CB77F625798B9671CCC0B8EF4B27D7D164C_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, const RuntimeMethod* method)
{
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		return L_0;
	}
}
// System.Void vp_Value`1<System.Object>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Value_1__ctor_m9D02D3D7AB6D8230F2907B56DE63C3F125B15C81_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event__ctor_mBAC4F17FDA57FE7F9766E60433C5A10BF198D4E7((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		VirtActionInvoker0::Invoke(6 /* System.Void vp_Event::InitFields() */, (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		return;
	}
}
// System.Void vp_Value`1<System.Object>::InitFields()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Value_1_InitFields_m79BCE6D0E34002CA25EB4A0DA15D2B559226FC41_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Value_1_InitFields_m79BCE6D0E34002CA25EB4A0DA15D2B559226FC41_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)SZArrayNew(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE_il2cpp_TypeInfo_var, (uint32_t)2);
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_1 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)L_0;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_2 = vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		FieldInfo_t * L_3 = Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5((Type_t *)L_2, (String_t*)_stringLiteralBFFFD736CDDD08A4EEE689949C3399CB61DA773B, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (FieldInfo_t *)L_3);
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_4 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)L_1;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_5 = vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		FieldInfo_t * L_6 = Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5((Type_t *)L_5, (String_t*)_stringLiteral448AB73BA1C21E671E218FB91F2644C834F0C16F, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (FieldInfo_t *)L_6);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_Fields_4(L_4);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_7 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_7;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (Getter_1_tB9D2902E8954106EBBDFF14735F86C73EE7B7014_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_10);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_11 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_12 = { reinterpret_cast<intptr_t> (Setter_1_t00D8F1B4DA85B1105AA10CBB32D04F58B802D121_0_0_0_var) };
		Type_t * L_13 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_13);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_DelegateTypes_5(L_11);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_14 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)il2cpp_codegen_object_new(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62(L_14, /*hidden argument*/Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62_RuntimeMethod_var);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_15 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_14;
		NullCheck((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_15);
		Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_15, (String_t*)_stringLiteral85FC3B2ACC52E958D0512287C2D44629E7D2CBAF, (int32_t)0, /*hidden argument*/Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_16 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_15;
		NullCheck((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_16);
		Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_16, (String_t*)_stringLiteral2FC7D48A5356042700B80D3BAEE0C5E913825A7D, (int32_t)1, /*hidden argument*/Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_Prefixes_8(L_16);
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_17 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_17)
		{
			goto IL_00bd;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_18 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_18);
		int32_t L_19 = 0;
		MethodInfo_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		bool L_21 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_20, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00bd;
		}
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_22 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_22);
		int32_t L_23 = 0;
		FieldInfo_t * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_25 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_25);
		int32_t L_26 = 0;
		MethodInfo_t * L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_28 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_28);
		int32_t L_29 = 0;
		Type_t * L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_31 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_30, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_24, (MethodInfo_t *)L_27, (Type_t *)L_31, /*hidden argument*/NULL);
	}

IL_00bd:
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_32 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_32)
		{
			goto IL_00f9;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_33 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_33);
		int32_t L_34 = 1;
		MethodInfo_t * L_35 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		bool L_36 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_35, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00f9;
		}
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_37 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_37);
		int32_t L_38 = 1;
		FieldInfo_t * L_39 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_40 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_40);
		int32_t L_41 = 1;
		MethodInfo_t * L_42 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_43 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_43);
		int32_t L_44 = 1;
		Type_t * L_45 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_44));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_46 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_45, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_39, (MethodInfo_t *)L_42, (Type_t *)L_46, /*hidden argument*/NULL);
	}

IL_00f9:
	{
		return;
	}
}
// System.Void vp_Value`1<System.Object>::Register(System.Object,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Value_1_Register_m771D9AB76A1D367D1E5E1D913418FD28E7EFD261_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, RuntimeObject * ___t0, String_t* ___m1, int32_t ___v2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___m1;
		if (L_0)
		{
			goto IL_0004;
		}
	}
	{
		return;
	}

IL_0004:
	{
		RuntimeObject * L_1 = ___t0;
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_2 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		int32_t L_3 = ___v2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		FieldInfo_t * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		String_t* L_6 = ___m1;
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_7 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		int32_t L_8 = ___v2;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		Type_t * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_11 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_10, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToExternalMethod_mA9E3D6BBEDCBA637A2FA457E5DC0820B5BF3D9FE((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (RuntimeObject *)L_1, (FieldInfo_t *)L_5, (String_t*)L_6, (Type_t *)L_11, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_Value`1<System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Value_1_Unregister_mEB03FE35DCAAB0D3434615633CDADE335697E3EA_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, RuntimeObject * ___t0, const RuntimeMethod* method)
{
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_0 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_1 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_1);
		int32_t L_2 = 0;
		MethodInfo_t * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		bool L_4 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_3, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_5 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_5);
		int32_t L_6 = 0;
		FieldInfo_t * L_7 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_8 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_8);
		int32_t L_9 = 0;
		MethodInfo_t * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_11 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_11);
		int32_t L_12 = 0;
		Type_t * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_14 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_13, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_7, (MethodInfo_t *)L_10, (Type_t *)L_14, /*hidden argument*/NULL);
	}

IL_003c:
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_15 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_15)
		{
			goto IL_0078;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_16 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_16);
		int32_t L_17 = 1;
		MethodInfo_t * L_18 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		bool L_19 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_18, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0078;
		}
	}
	{
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_20 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_20);
		int32_t L_21 = 1;
		FieldInfo_t * L_22 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_23 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_23);
		int32_t L_24 = 1;
		MethodInfo_t * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_26 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_26);
		int32_t L_27 = 1;
		Type_t * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_29 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_28, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_22, (MethodInfo_t *)L_25, (Type_t *)L_29, /*hidden argument*/NULL);
	}

IL_0078:
	{
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
