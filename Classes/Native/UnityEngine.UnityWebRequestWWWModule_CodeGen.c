﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.AudioClip UnityEngine.Networking.WebRequestWWW::InternalCreateAudioClipUsingDH(UnityEngine.Networking.DownloadHandler,System.String,System.Boolean,System.Boolean,UnityEngine.AudioType)
extern void WebRequestWWW_InternalCreateAudioClipUsingDH_m75A4AAC6E1310B046AAE668CB3CCC5123957BAB0 ();
// 0x00000002 System.Void UnityEngine.WWW::.ctor(System.String)
extern void WWW__ctor_m855BBB40089401B7BE6DE7A19FAD81EB070A2196 ();
// 0x00000003 System.String UnityEngine.WWW::get_error()
extern void WWW_get_error_mED42EEAAE7847167CCEEFF2098563F78A79F8C2A ();
// 0x00000004 System.Boolean UnityEngine.WWW::get_isDone()
extern void WWW_get_isDone_m7D3800B83136885374E82BFC6ACB63BA3F6AB5DF ();
// 0x00000005 UnityEngine.Texture2D UnityEngine.WWW::CreateTextureFromDownloadedData(System.Boolean)
extern void WWW_CreateTextureFromDownloadedData_mE9F1C08ABBBAACD3D92E8173999D1CC020C7A47E ();
// 0x00000006 UnityEngine.Texture2D UnityEngine.WWW::get_texture()
extern void WWW_get_texture_mD513AF1C1A59301515DFBC972E4530B886842C01 ();
// 0x00000007 System.String UnityEngine.WWW::get_url()
extern void WWW_get_url_m201062308E01D69B4AED10B7BBA6B345F8E42089 ();
// 0x00000008 System.Boolean UnityEngine.WWW::get_keepWaiting()
extern void WWW_get_keepWaiting_m2C52E54F48964EFD711C55A514E738CDF5D2EEB4 ();
// 0x00000009 System.Void UnityEngine.WWW::Dispose()
extern void WWW_Dispose_m481A801C09AEF9D4B7EE5BD680ECE80C0B1E0F66 ();
// 0x0000000A UnityEngine.Object UnityEngine.WWW::GetAudioClipInternal(System.Boolean,System.Boolean,System.Boolean,UnityEngine.AudioType)
extern void WWW_GetAudioClipInternal_mEE288AF0C6376DA80332CCB0B9505D1D4369335B ();
// 0x0000000B UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean,System.Boolean,UnityEngine.AudioType)
extern void WWW_GetAudioClip_m06DB54A063B83E071686E1D6B1452FB35ABE4C46 ();
// 0x0000000C System.Boolean UnityEngine.WWW::WaitUntilDoneIfPossible()
extern void WWW_WaitUntilDoneIfPossible_mBCA182F05B2F71D4F1816951748AFAF1C4FFD5D0 ();
static Il2CppMethodPointer s_methodPointers[12] = 
{
	WebRequestWWW_InternalCreateAudioClipUsingDH_m75A4AAC6E1310B046AAE668CB3CCC5123957BAB0,
	WWW__ctor_m855BBB40089401B7BE6DE7A19FAD81EB070A2196,
	WWW_get_error_mED42EEAAE7847167CCEEFF2098563F78A79F8C2A,
	WWW_get_isDone_m7D3800B83136885374E82BFC6ACB63BA3F6AB5DF,
	WWW_CreateTextureFromDownloadedData_mE9F1C08ABBBAACD3D92E8173999D1CC020C7A47E,
	WWW_get_texture_mD513AF1C1A59301515DFBC972E4530B886842C01,
	WWW_get_url_m201062308E01D69B4AED10B7BBA6B345F8E42089,
	WWW_get_keepWaiting_m2C52E54F48964EFD711C55A514E738CDF5D2EEB4,
	WWW_Dispose_m481A801C09AEF9D4B7EE5BD680ECE80C0B1E0F66,
	WWW_GetAudioClipInternal_mEE288AF0C6376DA80332CCB0B9505D1D4369335B,
	WWW_GetAudioClip_m06DB54A063B83E071686E1D6B1452FB35ABE4C46,
	WWW_WaitUntilDoneIfPossible_mBCA182F05B2F71D4F1816951748AFAF1C4FFD5D0,
};
static const int32_t s_InvokerIndices[12] = 
{
	1444,
	4,
	14,
	17,
	308,
	14,
	14,
	17,
	13,
	1445,
	1446,
	17,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestWWWModule.dll",
	12,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
