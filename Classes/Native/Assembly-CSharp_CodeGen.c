﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void AutoFlip::Start()
extern void AutoFlip_Start_m01AB668B7BE49B6B8D5E8AE67DEA6F2A16612223 ();
// 0x00000002 System.Void AutoFlip::FlipRightPage()
extern void AutoFlip_FlipRightPage_mA997777EC4EEBD7F40028396CD1BBE715CC11D30 ();
// 0x00000003 System.Void AutoFlip::FlipLeftPage()
extern void AutoFlip_FlipLeftPage_m46E84CBA2EB43508D3AB06E5D98B7A26BD98BCE1 ();
// 0x00000004 System.Void AutoFlip::StartFlipping()
extern void AutoFlip_StartFlipping_m31A7BF6B5820BFF2159090B8F35762AA9581A36C ();
// 0x00000005 System.Void AutoFlip::Update()
extern void AutoFlip_Update_mF1346CE4DD20B9B3FFF797BC733BE0F66DBD8ADC ();
// 0x00000006 System.Void AutoFlip::.ctor()
extern void AutoFlip__ctor_mF21E55DD008C4DF704AB05E018FD4E3901C47FC8 ();
// 0x00000007 System.Void AutoFlip::<FlipRightPage>b__11_0()
extern void AutoFlip_U3CFlipRightPageU3Eb__11_0_m554CF55CEF4979B505B85F051A724E82C61E23BA ();
// 0x00000008 System.Void AutoFlip::<FlipLeftPage>b__12_0()
extern void AutoFlip_U3CFlipLeftPageU3Eb__12_0_mCBDB6DDCBA4B83A5B8AC16E6447D3DC5C11A0EF6 ();
// 0x00000009 System.Void AutoFlip::<Update>b__14_0()
extern void AutoFlip_U3CUpdateU3Eb__14_0_mE1D065A8F4FEA087C9CCFF2064143274B96A2387 ();
// 0x0000000A System.Int32 BookPro::get_CurrentPaper()
extern void BookPro_get_CurrentPaper_m4BA16E66F8940CA2D55212061318D1F384FD27CB ();
// 0x0000000B System.Void BookPro::set_CurrentPaper(System.Int32)
extern void BookPro_set_CurrentPaper_mE57503571C7DD82218AC2B867D77D40EED64ED44 ();
// 0x0000000C UnityEngine.Vector3 BookPro::get_EndBottomLeft()
extern void BookPro_get_EndBottomLeft_m0C537E707C07B779A42BC8A57FB9BFE48A341C71 ();
// 0x0000000D UnityEngine.Vector3 BookPro::get_EndBottomRight()
extern void BookPro_get_EndBottomRight_mC4CE8871749C59A6DB60D7B63FA2F84EA80DFE91 ();
// 0x0000000E System.Single BookPro::get_Height()
extern void BookPro_get_Height_m5F7AF0E7CC2BF72C20ED117E0F0EF680FECB6006 ();
// 0x0000000F System.Void BookPro::Start()
extern void BookPro_Start_m0016FC2B02C8DDCD51E1614B0911095D42F83D05 ();
// 0x00000010 UnityEngine.Vector3 BookPro::transformPoint(UnityEngine.Vector3)
extern void BookPro_transformPoint_m2DA13110CA5832CBC626F6320510EFE729607565 ();
// 0x00000011 UnityEngine.Vector3 BookPro::transformPointMousePosition(UnityEngine.Vector3)
extern void BookPro_transformPointMousePosition_mF5CC6865A721DFB72F01BFB7D829E71AB5F4DFF0 ();
// 0x00000012 System.Void BookPro::UpdatePages()
extern void BookPro_UpdatePages_m0B48A85F5F20D11B4E81CFF80DE77E9A4349DB3E ();
// 0x00000013 System.Void BookPro::OnMouseDragRightPage()
extern void BookPro_OnMouseDragRightPage_mFA5E474B1B84DE2607B1418E0F541F2E3B718179 ();
// 0x00000014 System.Void BookPro::DragRightPageToPoint(UnityEngine.Vector3)
extern void BookPro_DragRightPageToPoint_mAF63C0F9B712A1AEC041CD138D7A1C0C3507212F ();
// 0x00000015 System.Void BookPro::OnMouseDragLeftPage()
extern void BookPro_OnMouseDragLeftPage_mB75CA39C0A47C89E92B73BC11CB158D4FB09D27D ();
// 0x00000016 System.Void BookPro::DragLeftPageToPoint(UnityEngine.Vector3)
extern void BookPro_DragLeftPageToPoint_mEA29B04AF2F9FBE2116CAFA8920B63FDA2A3A9A3 ();
// 0x00000017 System.Void BookPro::OnMouseRelease()
extern void BookPro_OnMouseRelease_mA7DF0235E63ECEDB72D259D99006EB54A1734D05 ();
// 0x00000018 System.Void BookPro::ReleasePage()
extern void BookPro_ReleasePage_mE9311368B4F7D3543E43403ED8C1D20A2F90ED73 ();
// 0x00000019 System.Void BookPro::Update()
extern void BookPro_Update_mECC4660231134BEC1615B204F77FD9B8382A7F52 ();
// 0x0000001A System.Void BookPro::UpdateBook()
extern void BookPro_UpdateBook_m652F9BC6088195DFFE85FC4C00D724D420357B41 ();
// 0x0000001B System.Void BookPro::Flip()
extern void BookPro_Flip_mCE8D5618B901DA709DE02736CCD28B5DD2388E95 ();
// 0x0000001C System.Void BookPro::TweenForward()
extern void BookPro_TweenForward_m336192BE2436C71FE6B5C2E68291C92762850D4A ();
// 0x0000001D System.Void BookPro::TweenUpdate(UnityEngine.Vector3)
extern void BookPro_TweenUpdate_m2C448DC3998556D3DA983784B3F1F7B4756B5730 ();
// 0x0000001E System.Void BookPro::TweenBack()
extern void BookPro_TweenBack_m8F91AAD6C5D61490BD1BEAF6857D51BCC2E45610 ();
// 0x0000001F System.Void BookPro::CalcCurlCriticalPoints()
extern void BookPro_CalcCurlCriticalPoints_mC18BE87EC3DC60AC081FDC4458E883CB92B76DEE ();
// 0x00000020 System.Void BookPro::UpdateBookRTLToPoint(UnityEngine.Vector3)
extern void BookPro_UpdateBookRTLToPoint_m4F160C1B58849500274987B942CEAD50B80AE0E7 ();
// 0x00000021 System.Void BookPro::UpdateBookLTRToPoint(UnityEngine.Vector3)
extern void BookPro_UpdateBookLTRToPoint_m1385692494C44BFE4BD9C4C08C191E85793459D1 ();
// 0x00000022 System.Single BookPro::Calc_T0_T1_Angle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void BookPro_Calc_T0_T1_Angle_mBCC8F28ED8B2F154B364DF1602464E7A490B63D4 ();
// 0x00000023 System.Single BookPro::normalizeT1X(System.Single,UnityEngine.Vector3,UnityEngine.Vector3)
extern void BookPro_normalizeT1X_m8ED16A7194521FB39B79CB43EBCE09FD170D6147 ();
// 0x00000024 UnityEngine.Vector3 BookPro::Calc_C_Position(UnityEngine.Vector3)
extern void BookPro_Calc_C_Position_m2E9A873F01885FC57F9058F872D1DCF49564DADF ();
// 0x00000025 System.Void BookPro::.ctor()
extern void BookPro__ctor_mDB2E9ECFB22FFB243455DF64DD90A16EFF360237 ();
// 0x00000026 System.Void BookPro::<TweenForward>b__44_0()
extern void BookPro_U3CTweenForwardU3Eb__44_0_m66C2692C442AB36FD15B652405E61FB0D8886684 ();
// 0x00000027 System.Void BookPro::<TweenForward>b__44_1()
extern void BookPro_U3CTweenForwardU3Eb__44_1_m04B59166F3241893DE9D839730616CC8DC91FC14 ();
// 0x00000028 System.Void BookPro::<TweenBack>b__46_0()
extern void BookPro_U3CTweenBackU3Eb__46_0_m6578FD266D90B73EFE45ACA3F46772BD5441BBE8 ();
// 0x00000029 System.Void BookPro::<TweenBack>b__46_1()
extern void BookPro_U3CTweenBackU3Eb__46_1_m1DEEB6F9362D840A30BD2030143189C53F6E567B ();
// 0x0000002A System.Void Paper::.ctor()
extern void Paper__ctor_m0506238F94550527B163FDE556824D5090B4D30B ();
// 0x0000002B System.Void BookUtility::ShowPage(UnityEngine.GameObject)
extern void BookUtility_ShowPage_m154A4C7E83373A1BACAC225EDC112240CF69E701 ();
// 0x0000002C System.Void BookUtility::HidePage(UnityEngine.GameObject)
extern void BookUtility_HidePage_m583D3D5EAAEE7FB227E39CF08DE930A54A7D1CB9 ();
// 0x0000002D System.Void BookUtility::CopyTransform(UnityEngine.Transform,UnityEngine.Transform)
extern void BookUtility_CopyTransform_m51AA42F868F62126F30C45D064305CAC70859D5E ();
// 0x0000002E System.Void PageFlipper::FlipPage(BookPro,System.Single,FlipMode,System.Action)
extern void PageFlipper_FlipPage_mA0ECB1485A0BC35AA73D7D4A7F01D59FB6F9B024 ();
// 0x0000002F System.Void PageFlipper::Update()
extern void PageFlipper_Update_m93B38C5AAE76A901CA463B9AD02F1E3FC3D0CD50 ();
// 0x00000030 System.Void PageFlipper::.ctor()
extern void PageFlipper__ctor_mAD3B51264788BB7125B8321B5ACBFA3FB7D2DABC ();
// 0x00000031 UnityEngine.Vector3 Tween::ValueTo(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Action`1<UnityEngine.Vector3>,System.Action)
extern void Tween_ValueTo_m7B9736EF9C900313F010745FDCD43B733B4AF832 ();
// 0x00000032 UnityEngine.Vector3 Tween::QuadOut(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Tween_QuadOut_m45498F438E4793BC40E82F9542C8B9D5A6A6BFB6 ();
// 0x00000033 System.Void Tween::Update()
extern void Tween_Update_mDD53C69E707081BEEF17715DBD9AC8C2101EA085 ();
// 0x00000034 System.Void Tween::.ctor()
extern void Tween__ctor_m965A158BB8A20C8C0DC8418B58B51BDA94780CCF ();
// 0x00000035 System.Void AdmobAdsProvider::add_BannerClick(System.Action`1<System.String>)
extern void AdmobAdsProvider_add_BannerClick_mF91D4A4A39F7B63308919D45CBA733AA88A3C868 ();
// 0x00000036 System.Void AdmobAdsProvider::remove_BannerClick(System.Action`1<System.String>)
extern void AdmobAdsProvider_remove_BannerClick_mDB26A5DC70FB98662DE29C33A17E377A5A1342E6 ();
// 0x00000037 System.Void AdmobAdsProvider::add_InterstitialShow(System.Action)
extern void AdmobAdsProvider_add_InterstitialShow_m3F6247B376CFF267DC9DFA581A12139178BD949C ();
// 0x00000038 System.Void AdmobAdsProvider::remove_InterstitialShow(System.Action)
extern void AdmobAdsProvider_remove_InterstitialShow_mE447677FE34930D0E37B58803264ADA3653B647F ();
// 0x00000039 System.Void AdmobAdsProvider::add_InterstitialShown(System.Action`1<System.String>)
extern void AdmobAdsProvider_add_InterstitialShown_m608C1CA3CD4C502179F0557C898D54CD54BF1CD9 ();
// 0x0000003A System.Void AdmobAdsProvider::remove_InterstitialShown(System.Action`1<System.String>)
extern void AdmobAdsProvider_remove_InterstitialShown_m756A822DAE9307391F28323F749869F08E507AF4 ();
// 0x0000003B System.Void AdmobAdsProvider::add_InterstitialClick(System.Action`1<System.String>)
extern void AdmobAdsProvider_add_InterstitialClick_mA093622799F59B31DEF6D56E2B0F465044B848C6 ();
// 0x0000003C System.Void AdmobAdsProvider::remove_InterstitialClick(System.Action`1<System.String>)
extern void AdmobAdsProvider_remove_InterstitialClick_mDCB995AC76B3DCB25DD7F20ED502FB0FF908E6C5 ();
// 0x0000003D System.Void AdmobAdsProvider::add_InterstitialClosed(System.Action)
extern void AdmobAdsProvider_add_InterstitialClosed_mCD1A24CF8BB52ECE50372900802B0649E61820E4 ();
// 0x0000003E System.Void AdmobAdsProvider::remove_InterstitialClosed(System.Action)
extern void AdmobAdsProvider_remove_InterstitialClosed_m4A97797A1BB3D5485578FDFF2F7B18A23ACDBC1A ();
// 0x0000003F System.Void AdmobAdsProvider::add_RewardedShown(System.Action`1<System.String>)
extern void AdmobAdsProvider_add_RewardedShown_m1B38C6481FF532D11CDAC2A9618D77AAF870D136 ();
// 0x00000040 System.Void AdmobAdsProvider::remove_RewardedShown(System.Action`1<System.String>)
extern void AdmobAdsProvider_remove_RewardedShown_m45D6426B596B943D5E09AB6C350B2E013E80CDFC ();
// 0x00000041 System.Void AdmobAdsProvider::add_RewardedComplete(System.Action`1<System.String>)
extern void AdmobAdsProvider_add_RewardedComplete_m92F3FE4D1C58B1BAE4CE40BE6E410E09A384DCA5 ();
// 0x00000042 System.Void AdmobAdsProvider::remove_RewardedComplete(System.Action`1<System.String>)
extern void AdmobAdsProvider_remove_RewardedComplete_m3F894115B3539CEDEFE5DA67EE175DE37F3708CA ();
// 0x00000043 System.Void AdmobAdsProvider::add_RewardedClick(System.Action`1<System.String>)
extern void AdmobAdsProvider_add_RewardedClick_mE5AE970353DF8526F7D930BB73FE2FFC87A41A81 ();
// 0x00000044 System.Void AdmobAdsProvider::remove_RewardedClick(System.Action`1<System.String>)
extern void AdmobAdsProvider_remove_RewardedClick_m5DB606A784B4D6A8F4F75EC13A3DD3847DDA4ACA ();
// 0x00000045 System.Void AdmobAdsProvider::add_RewardedClose(System.Action)
extern void AdmobAdsProvider_add_RewardedClose_m342B970DEB40568BABCE9799ADC28852686261F8 ();
// 0x00000046 System.Void AdmobAdsProvider::remove_RewardedClose(System.Action)
extern void AdmobAdsProvider_remove_RewardedClose_m94171A61060F961DE0F8B8D4D2B75AB654C5AA93 ();
// 0x00000047 NativeAdsPanel[] AdmobAdsProvider::get_adsPanels()
extern void AdmobAdsProvider_get_adsPanels_mB2AB155E0223EC9F7701384DF5288E35D8621731 ();
// 0x00000048 System.Void AdmobAdsProvider::InitModule()
extern void AdmobAdsProvider_InitModule_mEF5B8813680DDF40BA0E118C408BE947380D3405 ();
// 0x00000049 System.Void AdmobAdsProvider::InitNativeAds()
extern void AdmobAdsProvider_InitNativeAds_mDFE2B4021FA6116A19FE10928E67E59DEFDB016A ();
// 0x0000004A System.Void AdmobAdsProvider::LoadNativeAds()
extern void AdmobAdsProvider_LoadNativeAds_m566B8268E39BF03A6B24BE1ECD6F5A6980865801 ();
// 0x0000004B System.Void AdmobAdsProvider::LoadNativeAds(System.String)
extern void AdmobAdsProvider_LoadNativeAds_mD4757D82F8DE1F17558C021D7C9791EB4C1C4282 ();
// 0x0000004C NativeAdsPanel AdmobAdsProvider::GetNativePanel(System.String)
extern void AdmobAdsProvider_GetNativePanel_m6C3D5B3F44F1F1800B6CFC138A770AC8B7AFD4DC ();
// 0x0000004D System.Void AdmobAdsProvider::NativeAdsLoaded(GoogleMobileAds.Api.UnifiedNativeAd)
extern void AdmobAdsProvider_NativeAdsLoaded_m9CBE837178F4657DFE21C3FD17FD8190D324E808 ();
// 0x0000004E System.Void AdmobAdsProvider::HandleCustomNativeAdLoaded(System.Object,GoogleMobileAds.Api.UnifiedNativeAdEventArgs)
extern void AdmobAdsProvider_HandleCustomNativeAdLoaded_mC8120DB3765C8DD0A64771DC0CAA1823CEDC6A9C ();
// 0x0000004F System.Void AdmobAdsProvider::HandleNativeAdFailedToLoad(System.Object,GoogleMobileAds.Api.AdFailedToLoadEventArgs)
extern void AdmobAdsProvider_HandleNativeAdFailedToLoad_mD53EC132BEEACB0CBFD610C3B2F823EF9BB009FE ();
// 0x00000050 System.Void AdmobAdsProvider::HideBanner(System.String)
extern void AdmobAdsProvider_HideBanner_m457D242BD7C7336877078D6335E731B41C916DC4 ();
// 0x00000051 System.Void AdmobAdsProvider::ShowBanner(System.String)
extern void AdmobAdsProvider_ShowBanner_mAF4252E100D82011424352F953190D3ABE068837 ();
// 0x00000052 System.Boolean AdmobAdsProvider::IsInterstitialReady(System.String)
extern void AdmobAdsProvider_IsInterstitialReady_m9128BC235AE4DFE438D0686782BE0942CD20767A ();
// 0x00000053 System.Void AdmobAdsProvider::CacheInterstitial(System.String)
extern void AdmobAdsProvider_CacheInterstitial_mBF1466AFD50B7F03FC0F95837DF178B4A55BE85F ();
// 0x00000054 System.Void AdmobAdsProvider::CacheRewarded(System.String)
extern void AdmobAdsProvider_CacheRewarded_mE597AD518AB7FD5D0D2F0648CF809B815E6230C2 ();
// 0x00000055 System.Boolean AdmobAdsProvider::IsRewardedReady(System.String)
extern void AdmobAdsProvider_IsRewardedReady_m9E39B80B9A0CDE69017D2BA63224180291B039C1 ();
// 0x00000056 System.Void AdmobAdsProvider::SetAdsEnable(System.Boolean)
extern void AdmobAdsProvider_SetAdsEnable_mF9D1D5E270CEE48D0814874F54C248B927A4C04F ();
// 0x00000057 System.Boolean AdmobAdsProvider::ShowInterstitial(System.String)
extern void AdmobAdsProvider_ShowInterstitial_m33D20D84A9CC5115B57DC4E672C8FF844E55D271 ();
// 0x00000058 System.Void AdmobAdsProvider::ShowRewardedVideo(System.String)
extern void AdmobAdsProvider_ShowRewardedVideo_m0AF4E869FB73FF148A195B4EEBB48F918DA91C4F ();
// 0x00000059 System.Void AdmobAdsProvider::SetAdsSoundsMuted(System.Boolean)
extern void AdmobAdsProvider_SetAdsSoundsMuted_mDEAE62DE91ADE38B8D710C0FC625B2F513201AE6 ();
// 0x0000005A System.Boolean AdmobAdsProvider::IsBannerHidden(System.String)
extern void AdmobAdsProvider_IsBannerHidden_mB9696A5FCE647B420E1AB0C9A41873303750FF07 ();
// 0x0000005B System.Void AdmobAdsProvider::OnRewardedCompelte()
extern void AdmobAdsProvider_OnRewardedCompelte_m29023C8FE6753CABD67B90394ABF3A42819932C2 ();
// 0x0000005C System.Void AdmobAdsProvider::OnInterstitialClosed()
extern void AdmobAdsProvider_OnInterstitialClosed_m48FD620F4408CAADCBF1FE513BF737816CC5BE49 ();
// 0x0000005D System.Void AdmobAdsProvider::.ctor()
extern void AdmobAdsProvider__ctor_m641BC5FCDC26CE08AD29CDA3877EA2F058C04C9D ();
// 0x0000005E System.Void AdsPlacement::add_load(System.Action`1<System.Boolean>)
extern void AdsPlacement_add_load_m4BA367D7343AE575485AE0B2737CC4C64286DA2A ();
// 0x0000005F System.Void AdsPlacement::remove_load(System.Action`1<System.Boolean>)
extern void AdsPlacement_remove_load_m47924DF76851F14B77DAD16D880B160571B04B9D ();
// 0x00000060 System.String AdsPlacement::get_Key()
extern void AdsPlacement_get_Key_m1EDB9139B2919E5A7A923B2038412C678C8417A1 ();
// 0x00000061 System.Boolean AdsPlacement::get_IsLoaded()
extern void AdsPlacement_get_IsLoaded_m9F0055E58EC09166662EE6312FCBF3D0175AD9E0 ();
// 0x00000062 System.Void AdsPlacement::set_IsLoaded(System.Boolean)
extern void AdsPlacement_set_IsLoaded_m864448E3DB080CBEBA7E6C63FF3A8C78DA47BE1B ();
// 0x00000063 System.Void AdsPlacement::.ctor()
extern void AdsPlacement__ctor_mB70E961744DBC5147B2375E920D54EF8ABACD44F ();
// 0x00000064 System.Void CrashTest::Crash2()
extern void CrashTest_Crash2_mE8B990BF86B0700FD69D722C863AB9922F72EE79 ();
// 0x00000065 System.Void CrashTest::CrashInfinityLoop()
extern void CrashTest_CrashInfinityLoop_m41AE71F8B3DFC55E21FD88CC7D619859928EED2C ();
// 0x00000066 System.Void CrashTest::.ctor()
extern void CrashTest__ctor_m4E053781DADF81F35932A544056B20D59BB13326 ();
// 0x00000067 System.Void PromoObject::.ctor()
extern void PromoObject__ctor_m6FC77B94401E3B8B4501D414ED5F3FECAD7C22EA ();
// 0x00000068 System.Void PromoPanel::Awake()
extern void PromoPanel_Awake_m0F03E086F05376D2F0504037A492E73B9AD38438 ();
// 0x00000069 System.Void PromoPanel::InitPromo(PromoObject)
extern void PromoPanel_InitPromo_m0D10FBA8F8C6D686CCB7D2D12EE89229CEC6E98D ();
// 0x0000006A System.Void PromoPanel::Open()
extern void PromoPanel_Open_m915DCF411341A33E54BB1704BFB2CFA109C8E924 ();
// 0x0000006B System.Void PromoPanel::.ctor()
extern void PromoPanel__ctor_mC3322BDC6A8E2494456142957F5F5FC0772CAD52 ();
// 0x0000006C System.Void SmallCrosspromo::Awake()
extern void SmallCrosspromo_Awake_m6F32B5C2781242914457C6DEE32809A9DF2385E4 ();
// 0x0000006D System.Void SmallCrosspromo::UpdatePromo()
extern void SmallCrosspromo_UpdatePromo_m2465F1E742BC05A73F21AEB2BC8C78E67130B9E9 ();
// 0x0000006E System.Void SmallCrosspromo::.ctor()
extern void SmallCrosspromo__ctor_m36E83FE4CB7ACC6F449D45DCDEE96F946B52A2F7 ();
// 0x0000006F System.Void GDPRController::Awake()
extern void GDPRController_Awake_m1CA639148D2634F7BF572D06F64D1F3D80840789 ();
// 0x00000070 System.Void GDPRController::LoadMainScene()
extern void GDPRController_LoadMainScene_m6D83D18EFCEF1A1DCC64E424FCEC1D3156D285E2 ();
// 0x00000071 System.Collections.IEnumerator GDPRController::LoadScene()
extern void GDPRController_LoadScene_m2489F9B6C5F402E75514AC9745AF4BF306C2E5B3 ();
// 0x00000072 System.Void GDPRController::.ctor()
extern void GDPRController__ctor_mFCB466CCCFA17F83BD69E7C5E6621647AFC903C7 ();
// 0x00000073 GetObjectType AbstractOpenObjectMethods::AvailableGetType(GetObjectSettings)
extern void AbstractOpenObjectMethods_AvailableGetType_mE6B8D6750396BFACB17B9E70C2D7932ECEA4600A ();
// 0x00000074 System.Boolean AbstractOpenObjectMethods::VideoIsReady(AdsPlacement)
extern void AbstractOpenObjectMethods_VideoIsReady_m557003849904002C51341FFDD315AC6B46BE44F5 ();
// 0x00000075 System.Boolean AbstractOpenObjectMethods::HasCoins(System.Int32)
// 0x00000076 System.Void AbstractOpenObjectMethods::.ctor()
extern void AbstractOpenObjectMethods__ctor_m5277ABE30ADA4C88E0A123AFA0440EF64F16BD34 ();
// 0x00000077 System.Void GetObjectSettings::.ctor()
extern void GetObjectSettings__ctor_mC7C7EF30DA5B38210602DB29D18632B22F438EEC ();
// 0x00000078 System.Void Analytics::Start()
extern void Analytics_Start_m6CA75838A74C03B6B5F8286E02F87BB020E37DA1 ();
// 0x00000079 System.Void Analytics::SendEvent(System.String,System.Single,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Analytics_SendEvent_mBE2C61E768FE0AAB6F9A2ADA699F60283B23FEC9 ();
// 0x0000007A System.Void Analytics::SendEvent(AnalyticsSystems,System.String,System.Single,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Analytics_SendEvent_mF7E9CB226853EEC6FDF04197E0F0848CEFC1D43F ();
// 0x0000007B System.Void Analytics::LevelEvent(AnalyticsSystems,ProgressionType,System.Int32,System.Int32)
extern void Analytics_LevelEvent_m457E4F71BC0CC2E03C98110EEBA0D907434EC8CE ();
// 0x0000007C System.Void Analytics::FacebookEvent(System.String,System.Single,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Analytics_FacebookEvent_mB052A9900347839D1AC566BDC47AC70A95E978AA ();
// 0x0000007D System.Void Analytics::AppMetricEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Analytics_AppMetricEvent_m8A407BCA94D7F1AD351DABBF2BA31D0C632A178E ();
// 0x0000007E System.String Analytics::DictionaryToJson(System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern void Analytics_DictionaryToJson_m27088D9589299470475D5EB4D8A6D5D72EE82084 ();
// 0x0000007F System.Void Analytics::.ctor()
extern void Analytics__ctor_m64150A191B65F9CC3C3B7AE379B8495940178B91 ();
// 0x00000080 System.Void AppMetricaInitialize::Start()
extern void AppMetricaInitialize_Start_m51F6172769E74C9A2529AB5F7942BD8CF7458AB8 ();
// 0x00000081 System.Void AppMetricaInitialize::.ctor()
extern void AppMetricaInitialize__ctor_mB29A56BDD92C80629E56551D572F07E2B7972249 ();
// 0x00000082 System.Void AppsFlyerController::Awake()
extern void AppsFlyerController_Awake_m848995739119937BA031DC759E3DB1EB435004BA ();
// 0x00000083 System.Void AppsFlyerController::Purchase(System.String,System.String,System.String)
extern void AppsFlyerController_Purchase_m6835F0189DCDFF1C491FBC22F84E8AF35AF456BB ();
// 0x00000084 System.Void AppsFlyerController::.ctor()
extern void AppsFlyerController__ctor_mE9EAF2B4448B4C6BEE08251B50463607DECC053F ();
// 0x00000085 System.Void GameAnalyticsInitialize::Awake()
extern void GameAnalyticsInitialize_Awake_m481E8A10A67233F674982E61E502A3EC08E45411 ();
// 0x00000086 System.Void GameAnalyticsInitialize::.ctor()
extern void GameAnalyticsInitialize__ctor_mB0D29CF70357CC63CE02873AD37D90F91806A41B ();
// 0x00000087 System.Void AndroidNotificationProvider::Init()
extern void AndroidNotificationProvider_Init_m9F64A8AB168228C02439AE411542F1116D13B468 ();
// 0x00000088 System.Void AndroidNotificationProvider::CancelAll()
extern void AndroidNotificationProvider_CancelAll_m1BF32348326161DCE1CF0C3C7E81E24DC1DE1991 ();
// 0x00000089 System.Void AndroidNotificationProvider::Schedule(System.DateTime,System.String,System.String)
extern void AndroidNotificationProvider_Schedule_m9535727B25B8F428C25B1F49737BF6BA99B004B6 ();
// 0x0000008A System.Void AndroidNotificationProvider::ScheduleNotification(System.DateTime,System.String,System.String)
extern void AndroidNotificationProvider_ScheduleNotification_m88CFA28F78D97D65C8A3B08C7C5EA541AC037EED ();
// 0x0000008B System.Void AndroidNotificationProvider::.ctor()
extern void AndroidNotificationProvider__ctor_mFF58DF87D230F63F3418473875E7AFEC93E97311 ();
// 0x0000008C System.String LocalNotification::get_bundleIdentifier()
extern void LocalNotification_get_bundleIdentifier_m22D7ECC8C83BD0BDC3E013BB7A81DA3EFDDE49EC ();
// 0x0000008D System.Int32 LocalNotification::SendNotification(System.TimeSpan,System.String,System.String,UnityEngine.Color32,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.String,LocalNotification_Action[])
extern void LocalNotification_SendNotification_mF5C2332E3363491AB791451D7300290CF1665AB7 ();
// 0x0000008E System.Int32 LocalNotification::SendNotification(System.Int32,System.TimeSpan,System.String,System.String,UnityEngine.Color32,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.String,LocalNotification_Action[])
extern void LocalNotification_SendNotification_mDBCA5AD594ED1E5856040FC56FA6417301CBD13C ();
// 0x0000008F System.Int32 LocalNotification::SendNotification(System.Int32,System.Int64,System.String,System.String,UnityEngine.Color32,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.String,LocalNotification_Action[])
extern void LocalNotification_SendNotification_mECBF898CA837B9BF973E975FF79BD889CF62A7A8 ();
// 0x00000090 System.Int32 LocalNotification::SendRepeatingNotification(System.TimeSpan,System.TimeSpan,System.String,System.String,UnityEngine.Color32,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.String,LocalNotification_Action[])
extern void LocalNotification_SendRepeatingNotification_m5C5AB2FFEDC30825AA29E484669D127F1B60A228 ();
// 0x00000091 System.Int32 LocalNotification::SendRepeatingNotification(System.Int32,System.TimeSpan,System.TimeSpan,System.String,System.String,UnityEngine.Color32,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.String,LocalNotification_Action[])
extern void LocalNotification_SendRepeatingNotification_mFDDE60CD888F8BC23A92848DB35E955BDB810785 ();
// 0x00000092 System.Int32 LocalNotification::SendRepeatingNotification(System.Int32,System.Int64,System.Int64,System.String,System.String,UnityEngine.Color32,System.Boolean,System.Boolean,System.Boolean,System.String,System.String,System.String,LocalNotification_Action[])
extern void LocalNotification_SendRepeatingNotification_m2EF52557CFCD0928E61A58A449EC4FC15FE4976F ();
// 0x00000093 System.Void LocalNotification::CancelNotification(System.Int32)
extern void LocalNotification_CancelNotification_m316C5F79AB8AA74F26800EB83D147CB703F3A1B1 ();
// 0x00000094 System.Void LocalNotification::ClearNotifications()
extern void LocalNotification_ClearNotifications_m1299558C57C44D2238494C886D9DD7DD60F85272 ();
// 0x00000095 System.Void LocalNotification::CreateChannel(System.String,System.String,System.String,UnityEngine.Color32,System.Boolean,System.String,LocalNotification_Importance,System.Boolean,System.Int64[])
extern void LocalNotification_CreateChannel_m93DBBF02AB3FFE4DDE244ECD790328437F165AA8 ();
// 0x00000096 System.Int32 LocalNotification::ToInt(UnityEngine.Color32)
extern void LocalNotification_ToInt_mEB9AE16A95389ECB2B097CC11FFD5E264C911718 ();
// 0x00000097 System.Void LocalNotification::.ctor()
extern void LocalNotification__ctor_mCEEF8F5CB896AF2FD2AF59B781DF90EEE85222B4 ();
// 0x00000098 System.Boolean INotifiaction::IsNeedToSchedule()
// 0x00000099 System.Void INotificationProvider::Init()
// 0x0000009A System.Void INotificationProvider::CancelAll()
// 0x0000009B System.Void INotificationProvider::Schedule(System.DateTime,System.String,System.String)
// 0x0000009C System.Void DesktopNotificationProvider::CancelAll()
extern void DesktopNotificationProvider_CancelAll_m10E4A13B737A9A0D0443F16F317D8DCD22D0E69A ();
// 0x0000009D System.Void DesktopNotificationProvider::Init()
extern void DesktopNotificationProvider_Init_mC44616D69A218E7EC4F832B2F6CCF061B14E7FD6 ();
// 0x0000009E System.Void DesktopNotificationProvider::Schedule(System.DateTime,System.String,System.String)
extern void DesktopNotificationProvider_Schedule_mC6631AF6EBC6459D7B4D441BE849C4E45FAC8F8B ();
// 0x0000009F System.Void DesktopNotificationProvider::.ctor()
extern void DesktopNotificationProvider__ctor_m533BA3436CC3F78F5F495EAE9A5AD2D67160DB82 ();
// 0x000000A0 System.Void IosNotificationProvider::Init()
extern void IosNotificationProvider_Init_mD5BE945ECCE0922B5F1D200E058AD77572B34EBC ();
// 0x000000A1 System.Void IosNotificationProvider::CancelAll()
extern void IosNotificationProvider_CancelAll_mF23C01C15292A788A66073BBE45B00953E886E19 ();
// 0x000000A2 System.Void IosNotificationProvider::Schedule(System.DateTime,System.String,System.String)
extern void IosNotificationProvider_Schedule_m4D0EAEAB8B0A2D964F309E5D08B742815DB6411A ();
// 0x000000A3 System.Void IosNotificationProvider::ScheduleNotification(System.DateTime,System.String,System.String)
extern void IosNotificationProvider_ScheduleNotification_mB40C85E3CA84AF0A21F3A8BC3BB1D4CCAF7D7CAB ();
// 0x000000A4 System.Void IosNotificationProvider::.ctor()
extern void IosNotificationProvider__ctor_mEA641CCF0705147D5F7DC10AC2A376DEF48DA570 ();
// 0x000000A5 System.Boolean Notification::IsNeedToSchedule()
extern void Notification_IsNeedToSchedule_m270A375DB85E44F4F42786E2D4A875FCD9B90E94 ();
// 0x000000A6 System.Void Notification::.ctor()
extern void Notification__ctor_m9F0C402670F8031E2026708896013C8D0DDD00FC ();
// 0x000000A7 System.Void Condition::.ctor()
extern void Condition__ctor_mD9035D258C3585B729579CB52CBD5E6AD2907893 ();
// 0x000000A8 System.Boolean NotificationConditions::AlwaysTrue()
extern void NotificationConditions_AlwaysTrue_m3471A830BD4FBAF7B3EA600959D3E16A876969A0 ();
// 0x000000A9 System.Boolean NotificationConditions::HasCoinsForSkin()
extern void NotificationConditions_HasCoinsForSkin_mB59932BFCD045F5A9C4B2547EFD2E84F6B52B73A ();
// 0x000000AA System.Boolean NotificationConditions::CloseToHighScore()
extern void NotificationConditions_CloseToHighScore_mBD4B565B2EB73481EFA420A78B0CECCDC692BFC6 ();
// 0x000000AB System.Boolean NotificationConditions::CloseToCompleteMission()
extern void NotificationConditions_CloseToCompleteMission_m1D9558CE3BF5769414A7A90E7F4CDD03FCD23C90 ();
// 0x000000AC System.Void NotificationConditions::.ctor()
extern void NotificationConditions__ctor_m52267CD99A77FEA3117C55D96EE0A9A65C5A2EF4 ();
// 0x000000AD System.Void NotificationDatabse::.ctor()
extern void NotificationDatabse__ctor_mA88105913BC223CEFBECA6A2B16427C9FF27D2CC ();
// 0x000000AE LocalizedFrase NotificationDay::GetContent()
extern void NotificationDay_GetContent_m00E7AD57EBE70A6ACB6B1C52ACC04F62A942FC4A_AdjustorThunk ();
// 0x000000AF INotificationProvider NotificationManager::get_Provider()
extern void NotificationManager_get_Provider_m6E75DECFA0EF273B889E59D6F838E158AEF7D4CC ();
// 0x000000B0 System.Void NotificationManager::Awake()
extern void NotificationManager_Awake_mCB2197ACC711DF07F51B9695226D2DB4DCAB7612 ();
// 0x000000B1 System.Void NotificationManager::Start()
extern void NotificationManager_Start_m264342E06D8E4167BA1A15441EDB54B48931D25F ();
// 0x000000B2 System.Void NotificationManager::InitProvider()
extern void NotificationManager_InitProvider_m6F8344E38735441D7D85E1C06B9FB532CB90930B ();
// 0x000000B3 System.Void NotificationManager::Init()
extern void NotificationManager_Init_m4351004C520F0BEA59157DE0C43C322FCA85F7B4 ();
// 0x000000B4 System.Void NotificationManager::UpdateAllNotification()
extern void NotificationManager_UpdateAllNotification_m25F85A4E6FF661066C1637A73184DE76E7DB9A89 ();
// 0x000000B5 System.Void NotificationManager::RegitserNotification(System.Int32,LocalizedFrase)
extern void NotificationManager_RegitserNotification_m682EA29D60B38529B28FF1D018ECCF18442C0A0A ();
// 0x000000B6 System.Boolean NotificationManager::IsAllowableTime(System.DateTime)
extern void NotificationManager_IsAllowableTime_mA4A25D0F03DB9893EA6BB922A87F264D60A46390 ();
// 0x000000B7 System.Void NotificationManager::.ctor()
extern void NotificationManager__ctor_mA5D17B8FBA3E2E413E8CB767722581E3BC5C99D0 ();
// 0x000000B8 System.Void PrivacyButtons::Start()
extern void PrivacyButtons_Start_m87DDDF1B31CCCB6B72EA76BC9FF54E3638289A84 ();
// 0x000000B9 System.Void PrivacyButtons::Privacy()
extern void PrivacyButtons_Privacy_mC197CD7290951DD18F216739727553FAA5690CD2 ();
// 0x000000BA System.Void PrivacyButtons::Terms()
extern void PrivacyButtons_Terms_mBFD9FCCEDF5FB3D9EE95C156E09809D6F70E7B79 ();
// 0x000000BB System.Void PrivacyButtons::.ctor()
extern void PrivacyButtons__ctor_mA49F1E8E28F5567C63CE200D231F42CD725E6FC0 ();
// 0x000000BC System.Void Purchaser::Start()
extern void Purchaser_Start_mCD122FAB158727577ED229C323774D10737D1835 ();
// 0x000000BD System.Void Purchaser::BuyConsumable(System.String)
extern void Purchaser_BuyConsumable_m337D51C7A828D939DDFD67D25A47741770617A20 ();
// 0x000000BE System.Void Purchaser::BuyNoAds()
extern void Purchaser_BuyNoAds_mF6A2D2079F487DB864106CDCFBAE226443D2220C ();
// 0x000000BF System.Void Purchaser::BuyProductID(System.String)
extern void Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E ();
// 0x000000C0 System.Void Purchaser::RestorePurchases()
extern void Purchaser_RestorePurchases_m24A1E9C7782A272240B01EB09EBC8828ED550846 ();
// 0x000000C1 System.Void Purchaser::.ctor()
extern void Purchaser__ctor_m526162775A909925C95DBABA1943472F883DFDDA ();
// 0x000000C2 System.String BuyProduct::get_Key()
extern void BuyProduct_get_Key_m03CB5B8BF0ACE2E4A080DC9DA7819E962D85F6BC ();
// 0x000000C3 System.Void BuyProduct::.ctor()
extern void BuyProduct__ctor_mD54F1EE2FFDF1C4A7CA829BD426A7754E648F7B3 ();
// 0x000000C4 System.Void RateController::Rate()
extern void RateController_Rate_mC373682F066ACBBAD430220E13196816D3A3D9FA ();
// 0x000000C5 System.Void RateController::Rate(System.Int32)
extern void RateController_Rate_m8E81D21FE03B946D4EDF47BDC708B4B945BCAD51 ();
// 0x000000C6 System.Boolean RateController::get_IsRated()
extern void RateController_get_IsRated_mEF0EB181CDB60E284FBE653D25E6375A20F62C9A ();
// 0x000000C7 System.Void RateController::set_IsRated(System.Boolean)
extern void RateController_set_IsRated_mB42C0A4E0FAA30E1ED996C49186BAA2EE6DE3461 ();
// 0x000000C8 System.Void RateController::.ctor()
extern void RateController__ctor_mFE520BA9C072290F3C91C73258425F81A69BD1CB ();
// 0x000000C9 System.Void RateController::.cctor()
extern void RateController__cctor_mE7BCA0EFF6348620B4D8454BB6748DF2DB1E976C ();
// 0x000000CA System.Boolean RateStarsPanel::get_IsRated()
extern void RateStarsPanel_get_IsRated_m48B1E188C500A9403468942BC418DBE97250F42C ();
// 0x000000CB System.Int32 RateStarsPanel::get_RateNumber()
extern void RateStarsPanel_get_RateNumber_m1C5DAED06D3392BE824D57C4E247E01B6F523AB5 ();
// 0x000000CC System.Void RateStarsPanel::Awake()
extern void RateStarsPanel_Awake_mFC52278B39221E7C33464BABA2FF2B217A017B13 ();
// 0x000000CD System.Void RateStarsPanel::InitButtons()
extern void RateStarsPanel_InitButtons_m45C0ED6930F9A5C3A9446E3B659BD43C11739B37 ();
// 0x000000CE System.Void RateStarsPanel::Reset()
extern void RateStarsPanel_Reset_m57FEC7BE5502DB158A6906DF7F4552F901692E21 ();
// 0x000000CF System.Void RateStarsPanel::Rate(System.Int32)
extern void RateStarsPanel_Rate_mC53B303B9DD0E33B9EDFA4EE1AB34E6DF1EF8C20 ();
// 0x000000D0 System.Void RateStarsPanel::SelectStars(System.Int32)
extern void RateStarsPanel_SelectStars_mC4E5D328EECC84C84A6EEACD719E6D8195781514 ();
// 0x000000D1 System.Void RateStarsPanel::Clear()
extern void RateStarsPanel_Clear_m9A35C388B595349C3797FDBB4FDE464CB703E18F ();
// 0x000000D2 System.Void RateStarsPanel::.ctor()
extern void RateStarsPanel__ctor_mDBB548B175EB25FFA800E7D080BE1D9E5E499E55 ();
// 0x000000D3 System.Void RateStarsPanel::<InitButtons>b__10_0()
extern void RateStarsPanel_U3CInitButtonsU3Eb__10_0_mB56F0367D361B3E1C7D14E333791984DE4D8D9EA ();
// 0x000000D4 System.Void RateStarsPanel::<InitButtons>b__10_1()
extern void RateStarsPanel_U3CInitButtonsU3Eb__10_1_mEF6E780A8A3241829E1219D37611E1A514D94133 ();
// 0x000000D5 System.Void RateStarsPanel::<InitButtons>b__10_2()
extern void RateStarsPanel_U3CInitButtonsU3Eb__10_2_mBEBC145F3A09C899AFF4577928AE3A4E9BC744A8 ();
// 0x000000D6 System.Void RateStarsPanel::<InitButtons>b__10_3()
extern void RateStarsPanel_U3CInitButtonsU3Eb__10_3_m89921336493EDA4A359A17EFDAEA12E341DE818D ();
// 0x000000D7 System.Void RateStarsPanel::<InitButtons>b__10_4()
extern void RateStarsPanel_U3CInitButtonsU3Eb__10_4_m388AF14D95CAE08260A9A229C7FA6FF2AE3C998B ();
// 0x000000D8 System.Void ServiceData::.ctor()
extern void ServiceData__ctor_m832551047FDB100BD15FE1E07E50FBEB4C05DC46 ();
// 0x000000D9 System.Void IService::Login()
// 0x000000DA System.Void IService::Logout()
// 0x000000DB System.Void IService::ShowTop()
// 0x000000DC System.Void IService::SetTop(System.Int32)
// 0x000000DD System.Void IService::ShowAchivs()
// 0x000000DE System.Void IService::Rate()
// 0x000000DF System.Void IService::Share()
// 0x000000E0 System.Void IService::Share(System.String)
// 0x000000E1 System.Void IService::Share(System.String,System.String)
// 0x000000E2 System.Void Service::Login()
// 0x000000E3 System.Void Service::Logout()
// 0x000000E4 System.Void Service::Rate()
// 0x000000E5 System.Void Service::SetTop(System.Int32)
// 0x000000E6 System.Void Service::Share()
// 0x000000E7 System.Void Service::Share(System.String)
// 0x000000E8 System.Void Service::Share(System.String,System.String)
// 0x000000E9 System.Void Service::ShowAchivs()
// 0x000000EA System.Void Service::ShowTop()
// 0x000000EB System.Void Service::.ctor()
extern void Service__ctor_mD0EE68016BA53028B56AB0A1DA9FC274FA0228BF ();
// 0x000000EC System.Int32 ServiceController::get_ShareCount()
extern void ServiceController_get_ShareCount_mA310E86F97344B853D996A132A5A0EF2FA8E7463 ();
// 0x000000ED System.Void ServiceController::set_ShareCount(System.Int32)
extern void ServiceController_set_ShareCount_m2EDE8709E934A3FECC75D8CCA2319908711561EA ();
// 0x000000EE System.Boolean ServiceController::get_RateState()
extern void ServiceController_get_RateState_m50CA2CD5D14F2BED7C1380C9870FEA31EB044430 ();
// 0x000000EF System.Void ServiceController::set_RateState(System.Boolean)
extern void ServiceController_set_RateState_m127187BBDCA7A6321E6FE9163D1A84BDE3261DF8 ();
// 0x000000F0 System.Void ServiceController::Awake()
extern void ServiceController_Awake_m984499D373A823726EE8FCED41686D3927BE8635 ();
// 0x000000F1 System.Void ServiceController::InitShareCounter()
extern void ServiceController_InitShareCounter_m7C096F1468A97E1D12F9FB0D6BD81A245029A60D ();
// 0x000000F2 System.Void ServiceController::Start()
extern void ServiceController_Start_m84BE68C2C00B774811BB9D98E0F43AEFF80CB3A0 ();
// 0x000000F3 System.Void ServiceController::Init(IService)
extern void ServiceController_Init_m9DA7E973ECE8416226608CAA83243F7CCBAEEB8F ();
// 0x000000F4 System.Void ServiceController::Login()
extern void ServiceController_Login_m4419FECA65454D0F20D0D0865B577BEAEC33218F ();
// 0x000000F5 System.Void ServiceController::Logout()
extern void ServiceController_Logout_mA0AF0352E7635D2ED2136E8D9D7D529DB4F30D8E ();
// 0x000000F6 System.Void ServiceController::ShowTop()
extern void ServiceController_ShowTop_m4E4E9CBD599905F1BFFD7757EB1EC5A1F91D0EAA ();
// 0x000000F7 System.Void ServiceController::SetTop(System.Int32)
extern void ServiceController_SetTop_m8D20AC64E981EDD675C83F95181559F57C5DCDE0 ();
// 0x000000F8 System.Void ServiceController::ShowAchivs()
extern void ServiceController_ShowAchivs_m54BAD7B2213CEE0EDD4F7C7E17C33F87A50E40F8 ();
// 0x000000F9 System.Void ServiceController::Rate()
extern void ServiceController_Rate_m70324EDA33209BD86E547B004C0149BAE06BA3FE ();
// 0x000000FA System.Void ServiceController::Share()
extern void ServiceController_Share_mF1F934A034A08FB81672EFDD1C75996A56B5ED57 ();
// 0x000000FB System.Void ServiceController::Share(System.String)
extern void ServiceController_Share_m702012FC1693E73085E4A48B56A5EAB62021418F ();
// 0x000000FC System.Void ServiceController::Share(System.String,System.String)
extern void ServiceController_Share_mB9539E869D856CB0C23333512452BB58EC080DD8 ();
// 0x000000FD System.Void ServiceController::.ctor()
extern void ServiceController__ctor_mE2F9FFC917B3ADDECED24F3BD434001518F47A39 ();
// 0x000000FE System.Void ServiceController::.cctor()
extern void ServiceController__cctor_mB115A2AD7245D6F3F3458DE3FEEC8CA8E22B0197 ();
// 0x000000FF System.Void AndroidService::Login()
extern void AndroidService_Login_mBFD962A647BBACC0256DFA85F6CDCAF376BA1D8C ();
// 0x00000100 System.Void AndroidService::Logout()
extern void AndroidService_Logout_m83A5088C9BC4A09731C08519C94160F747C61B49 ();
// 0x00000101 System.Void AndroidService::ShowTop()
extern void AndroidService_ShowTop_mCBFE77F9DF3917CF96D0A75DA34D476E49C4C698 ();
// 0x00000102 System.Void AndroidService::SetTop(System.Int32)
extern void AndroidService_SetTop_m1BC9B4108E9C5134E5C162994D5D4C2BC383648C ();
// 0x00000103 System.Void AndroidService::ShowAchivs()
extern void AndroidService_ShowAchivs_m9FE93AF6424AC905B8B70843F95727C03C0981A5 ();
// 0x00000104 System.String AndroidService::GetLoadUrl()
extern void AndroidService_GetLoadUrl_m282ECF635CE1888C144891C2E5A03036B41BD171 ();
// 0x00000105 System.Void AndroidService::Rate()
extern void AndroidService_Rate_mD8D731AE4EA737E34347679DCCD6149F7051D731 ();
// 0x00000106 System.Void AndroidService::Share()
extern void AndroidService_Share_m1AFD2DF6E235C02A73E4E4B6DFC4696DDBA98559 ();
// 0x00000107 System.Void AndroidService::Share(System.String)
extern void AndroidService_Share_m93009612F11243F7D1138870508018B05D9909F3 ();
// 0x00000108 System.Void AndroidService::Share(System.String,System.String)
extern void AndroidService_Share_m60ED46C237D1C101AB5D7364A7693BE174D969A2 ();
// 0x00000109 System.String AndroidService::CreateText()
extern void AndroidService_CreateText_m73D60C846057526D4F505C9A75D3A9679F81946C ();
// 0x0000010A System.Void AndroidService::AndroidShare(System.String,System.String)
extern void AndroidService_AndroidShare_m1592ADE8187C6F7CF6B4E07A800A5FAED107203F ();
// 0x0000010B System.Void AndroidService::.ctor()
extern void AndroidService__ctor_m8ED9F93D992C45DB02A4DCDF2BA47BA0A2483F56 ();
// 0x0000010C System.Void AppleService::Login()
extern void AppleService_Login_m4D36739A0F4D949FE5D896330C6B06F0324AC710 ();
// 0x0000010D System.Void AppleService::ShowTop()
extern void AppleService_ShowTop_mDB0C709246F95277C270854F2762FFD274A2CD8C ();
// 0x0000010E System.Void AppleService::SetTop(System.Int32)
extern void AppleService_SetTop_m1BE231B6898FEBDB3A9E52B1EEEF587E8C6FB341 ();
// 0x0000010F System.Void AppleService::Logout()
extern void AppleService_Logout_mA17AA76487A66D4360B30152C19855085A909D64 ();
// 0x00000110 System.Void AppleService::ShowAchivs()
extern void AppleService_ShowAchivs_m9591BC68FD34122FBDA42B509D2ACFFF1F6B62C7 ();
// 0x00000111 System.String AppleService::GetLoadUrl()
extern void AppleService_GetLoadUrl_m9F7EFDBFF7C62F49C87FFE3E58B05A8E9DABD0DC ();
// 0x00000112 System.Void AppleService::Rate()
extern void AppleService_Rate_m408DCCF74CA8BE9386B9FED45ECA4B269DE24DA0 ();
// 0x00000113 System.Void AppleService::Share()
extern void AppleService_Share_m5C1D6B4FA2B5D7B5FBFC6FF76A677A4B4A0BBB64 ();
// 0x00000114 System.Void AppleService::Share(System.String)
extern void AppleService_Share_mF457014AFDDE3C0C4C30B6832A3BAB6FECF1883F ();
// 0x00000115 System.Void AppleService::Share(System.String,System.String)
extern void AppleService_Share_m067110B6C8008AC4EFD3EE621D2F1A3CB778D1D1 ();
// 0x00000116 System.Void AppleService::ShareMsg(System.String)
extern void AppleService_ShareMsg_m418F3A762B19706E139437807BBF5751E7FD247E ();
// 0x00000117 System.Void AppleService::ShareMsgAndTexture(System.String,System.String)
extern void AppleService_ShareMsgAndTexture_mCB2270252E3A5C5B7E3CD55D3D8930AEF334F836 ();
// 0x00000118 System.String AppleService::CreateText()
extern void AppleService_CreateText_mF6F45823E9F30E78405EE60EC58F4E56F3153859 ();
// 0x00000119 System.Void AppleService::.ctor()
extern void AppleService__ctor_m98802E4CED4A51DF16659B9A9DE0FA8A0589E48E ();
// 0x0000011A System.Void DesktopService::Login()
extern void DesktopService_Login_mF4EFD0B66CF3E437107A6178DEC1A5032BF35AA6 ();
// 0x0000011B System.Void DesktopService::Logout()
extern void DesktopService_Logout_m0F802F8D45EA638A9EDD97C3F2C5F42218DBDD3C ();
// 0x0000011C System.Void DesktopService::Rate()
extern void DesktopService_Rate_m8CE79BF4691C4EB5A110E3679AD6FC33A79B9458 ();
// 0x0000011D System.Void DesktopService::SetTop(System.Int32)
extern void DesktopService_SetTop_m9266AF4EA73DE801F9A5151281DE70E288E56C9E ();
// 0x0000011E System.Void DesktopService::Share()
extern void DesktopService_Share_mBD13BBF9D3653D07AC7AC14E89C64D1DF369318D ();
// 0x0000011F System.Void DesktopService::Share(System.String)
extern void DesktopService_Share_m25752E9EF62116F0F00B6A3B553E43241883F89D ();
// 0x00000120 System.Void DesktopService::Share(System.String,System.String)
extern void DesktopService_Share_mBE755DCA647E419061BBFF219779A3D74EC92FCD ();
// 0x00000121 System.Void DesktopService::ShowAchivs()
extern void DesktopService_ShowAchivs_mFEDC9F40D372101E95221140164CBC745B4C6327 ();
// 0x00000122 System.Void DesktopService::ShowTop()
extern void DesktopService_ShowTop_mD26D65C62131241F3BEDF5301F99DAF09F15C878 ();
// 0x00000123 System.Void DesktopService::.ctor()
extern void DesktopService__ctor_m769B0C9EDC9E609D5D4E56340B8D189DFD18CC72 ();
// 0x00000124 System.Void MusicButtonController::Awake()
extern void MusicButtonController_Awake_mFCA7AFAC9631FFE73B0197E7D84E9B2627B733CA ();
// 0x00000125 System.Void MusicButtonController::Click()
extern void MusicButtonController_Click_mA9B12F01201CB1B6404DEB76BC25D6CA7865C156 ();
// 0x00000126 System.Void MusicButtonController::CheckImage()
extern void MusicButtonController_CheckImage_m70178D2FC4585003FFAFFB08B560661A56330193 ();
// 0x00000127 System.Void MusicButtonController::.ctor()
extern void MusicButtonController__ctor_m94B4CA0EE304545F78FD039267C2B091C5BF034C ();
// 0x00000128 System.Boolean MusicManager::get_Music()
extern void MusicManager_get_Music_m3CFC65B4B577F7DCDCA7AB1A2533E0DF221C534D ();
// 0x00000129 System.Void MusicManager::set_Music(System.Boolean)
extern void MusicManager_set_Music_m5EFE605773E19B1DFE4D72D47AE8D8138464C128 ();
// 0x0000012A System.Void MusicManager::Awake()
extern void MusicManager_Awake_mADDA160267BBC2EAF3DDE1F8E9EBA58031DC6693 ();
// 0x0000012B System.Void MusicManager::Start()
extern void MusicManager_Start_mF77913A780CD343FC5CC6FE204CF7AFCD8043272 ();
// 0x0000012C System.Void MusicManager::Pause()
extern void MusicManager_Pause_mD4286D00D332087F3A8495CD04DB4A4AEF63D1FA ();
// 0x0000012D System.Void MusicManager::UnPause()
extern void MusicManager_UnPause_m1A921FB699F8718AF6F88EE9E09921F37E5119CB ();
// 0x0000012E System.Void MusicManager::Mute()
extern void MusicManager_Mute_m3F2604CAD54BCF2BDDD5E845B33072F74006204C ();
// 0x0000012F System.Void MusicManager::UnMute()
extern void MusicManager_UnMute_m63AB004E2D0D527BDCB2066466EC7019C3A5EFBE ();
// 0x00000130 System.Void MusicManager::SetMute(System.Boolean)
extern void MusicManager_SetMute_m46E95835E2FE107ECA8A384923D15DC7911C349D ();
// 0x00000131 System.Void MusicManager::Check()
extern void MusicManager_Check_mF96F5AA5E5B7815BBB5607E55CE532479C224AAE ();
// 0x00000132 System.Void MusicManager::AdsStart()
extern void MusicManager_AdsStart_m17A38DEDED2AD39A474B2ECDFCDC04F91285DC4B ();
// 0x00000133 System.Void MusicManager::AdsFinish()
extern void MusicManager_AdsFinish_m2F4933100A79B5EEF96DF88961238733893BB563 ();
// 0x00000134 System.Void MusicManager::.ctor()
extern void MusicManager__ctor_m0910B32A2F10526CE5FCB5831384F261A434549B ();
// 0x00000135 System.Void SoundButtonController::Awake()
extern void SoundButtonController_Awake_m6CC890FAED1D5A2035E0DF094D48DEBF8D0D5BFB ();
// 0x00000136 System.Void SoundButtonController::Click()
extern void SoundButtonController_Click_m4B5AA0FC60F4157EAB2756E75F5F066C214833C8 ();
// 0x00000137 System.Void SoundButtonController::ClickAll()
extern void SoundButtonController_ClickAll_mB614E51A48AD0EC720BD171A5FAFA40F86F34007 ();
// 0x00000138 System.Void SoundButtonController::CheckImage()
extern void SoundButtonController_CheckImage_mFAE8B3560410E0AC212E73E5EC2890494146835C ();
// 0x00000139 System.Void SoundButtonController::.ctor()
extern void SoundButtonController__ctor_m47C03C3AE594A7476BF7867CD2955B3524B4A002 ();
// 0x0000013A UnityEngine.AudioSource SoundController::get_Audio()
extern void SoundController_get_Audio_mC2072AA5CA65BD09C510B8F9EF6A4C73E1233447 ();
// 0x0000013B System.Void SoundController::Awake()
extern void SoundController_Awake_m7D4556CC74ED46B01483640F957997A2F5BC903D ();
// 0x0000013C System.Void SoundController::InitSound()
extern void SoundController_InitSound_m5DF6881D341789E871F292506AE79D2A322C0CB7 ();
// 0x0000013D System.Void SoundController::Play()
extern void SoundController_Play_m446C595ED7953A99C2D603DE872CC30C8681CA2F ();
// 0x0000013E System.Void SoundController::Stop()
extern void SoundController_Stop_mA3FD56D4C15C5164FA3D893D7288AFE4C5FE0699 ();
// 0x0000013F System.Void SoundController::Play(UnityEngine.AudioClip)
extern void SoundController_Play_m738AA37918DC26C17A0F8B3EDB2D25FF19C066B9 ();
// 0x00000140 System.Void SoundController::Mute()
extern void SoundController_Mute_m04646E7CB24FCEB2E4567B220F2BFC5B1EAC9865 ();
// 0x00000141 System.Void SoundController::UnMute()
extern void SoundController_UnMute_m4F664DFE1DECAA03FB3DDAE718C68A836A237D1A ();
// 0x00000142 System.Void SoundController::Pause()
extern void SoundController_Pause_m28CB244AF5FD9D4861FE4E995AB35EDC5CB45113 ();
// 0x00000143 System.Void SoundController::UnPause()
extern void SoundController_UnPause_m36E21E8AD736783D9F921B22A81A4BC5CF94224B ();
// 0x00000144 System.Void SoundController::.ctor()
extern void SoundController__ctor_m6D8FCBBD8F2A1B7AC1D0D87211405ED5CEE72EDB ();
// 0x00000145 System.Void AbstractSoundController::Play()
// 0x00000146 System.Void AbstractSoundController::Stop()
// 0x00000147 System.Void AbstractSoundController::Mute()
// 0x00000148 System.Void AbstractSoundController::UnMute()
// 0x00000149 System.Void AbstractSoundController::Pause()
// 0x0000014A System.Void AbstractSoundController::UnPause()
// 0x0000014B System.Void AbstractSoundController::.ctor()
extern void AbstractSoundController__ctor_m81237B6F992471992F2D96E24606F9E73C185DC8 ();
// 0x0000014C System.Boolean SoundManager::get_Sounds()
extern void SoundManager_get_Sounds_m377A4A05228E38A85C232420C004255E35295D9E ();
// 0x0000014D System.Void SoundManager::set_Sounds(System.Boolean)
extern void SoundManager_set_Sounds_mE69D99293F44DF21B8535E84F1AC718C3C1AF069 ();
// 0x0000014E System.Void SoundManager::Awake()
extern void SoundManager_Awake_mFAD1AEBF8B8957E85CE445F65107AC6A5DDDFEEC ();
// 0x0000014F System.Void SoundManager::Pause()
extern void SoundManager_Pause_mC5E8C368906F319F528B804061EED2796F4377FB ();
// 0x00000150 System.Void SoundManager::UnPause()
extern void SoundManager_UnPause_mF061B9F7B64299E1DD828FB00EE5B19DE77F4A0C ();
// 0x00000151 System.Void SoundManager::Mute()
extern void SoundManager_Mute_mA2C18D5940898B0A056534C5AE3EB234B53BD541 ();
// 0x00000152 System.Void SoundManager::UnMute()
extern void SoundManager_UnMute_m8C067EFF4B80DE9546B278FFA112BB90A67A46F0 ();
// 0x00000153 System.Void SoundManager::PlaySound(System.String)
extern void SoundManager_PlaySound_mF0B7231A196F591C25BC19455496627A3FD52FB4 ();
// 0x00000154 System.Void SoundManager::Play(System.String)
extern void SoundManager_Play_mC6F1EE6E97A7983831C3709D42089F61EC0FAFF4 ();
// 0x00000155 System.Void SoundManager::PlayAndSave(System.String)
extern void SoundManager_PlayAndSave_m11AE985ED651689317E007711FA5194498863AF0 ();
// 0x00000156 System.Void SoundManager::PlayLast()
extern void SoundManager_PlayLast_mE65B6686557B5DD69BC3A7A773C14295A6CAB961 ();
// 0x00000157 System.Void SoundManager::Check()
extern void SoundManager_Check_m8E0330C0D003E513D29A7513C4403CAE60B57253 ();
// 0x00000158 System.Void SoundManager::AdsStart()
extern void SoundManager_AdsStart_mA13866A4BC2FFF61DDA2D84BB50611CC86E4991A ();
// 0x00000159 System.Void SoundManager::AdsFinish()
extern void SoundManager_AdsFinish_m0E7190D4826B3B4D02543B324DA75EBABA92916A ();
// 0x0000015A System.Void SoundManager::.ctor()
extern void SoundManager__ctor_mFF8C696A5B666ABC1E2344581FE7FB06E038D422 ();
// 0x0000015B System.Void TypeEditor::.ctor()
extern void TypeEditor__ctor_m2F33E56E5B2A2AA7942DEE43FA819CC9A367804B ();
// 0x0000015C UnityEngine.Vector2 UiMoveToCenter::get_mySize()
extern void UiMoveToCenter_get_mySize_m440F76CE0E856E0A93B1F87E299092E0B7921956 ();
// 0x0000015D UnityEngine.Vector2 UiMoveToCenter::get_HorizontalBorders()
extern void UiMoveToCenter_get_HorizontalBorders_mB03DA147E3AC9927C12110CACD610290138C801E ();
// 0x0000015E System.Void UiMoveToCenter::Awake()
extern void UiMoveToCenter_Awake_m8BE4620CB582C4967CFDF08D80D0EDEFE78745D6 ();
// 0x0000015F System.Void UiMoveToCenter::Update()
extern void UiMoveToCenter_Update_m268E1BD6D9A54DF34F0C4CC0F539DAFF408C2268 ();
// 0x00000160 System.Void UiMoveToCenter::UpdatePosition()
extern void UiMoveToCenter_UpdatePosition_mF5B0CBA6CD0DE0167D30812AA4939A740AB44242 ();
// 0x00000161 System.Void UiMoveToCenter::MoveToCenterOfScreen()
extern void UiMoveToCenter_MoveToCenterOfScreen_mEEC12112D1A08430DDB3B38BDA3DDDBE85F0CD15 ();
// 0x00000162 System.Void UiMoveToCenter::MoveToLeftOfParent()
extern void UiMoveToCenter_MoveToLeftOfParent_m6D8926211E7C18CDFCEF8F453D12A7DC1B83ACB5 ();
// 0x00000163 System.Void UiMoveToCenter::MoveToRightOfParent()
extern void UiMoveToCenter_MoveToRightOfParent_m510E6CF2BA46217CC2CE101807C5EDDC6AEF97F5 ();
// 0x00000164 System.Void UiMoveToCenter::MoveToCenterOfParent()
extern void UiMoveToCenter_MoveToCenterOfParent_m04BDA9C3826E53D1427F726E7EF379D8872C265B ();
// 0x00000165 System.Void UiMoveToCenter::MoveTo(UnityEngine.Vector3)
extern void UiMoveToCenter_MoveTo_mF7EE4FC6AD65497E597B80858DD407B5A44418C4 ();
// 0x00000166 System.Void UiMoveToCenter::MoveToPosition(System.Single)
extern void UiMoveToCenter_MoveToPosition_mAED5EE04D1DAAF0ECAE9F1BEA67B50877343CFB2 ();
// 0x00000167 System.Void UiMoveToCenter::.ctor()
extern void UiMoveToCenter__ctor_m708928E4B860E340C69547298CB62A5AA4E4139D ();
// 0x00000168 UnityEngine.Vector3 UIUtils::GetCenterPosition(UnityEngine.RectTransform,System.Single)
extern void UIUtils_GetCenterPosition_m5B9F0760FCB17E047C40AC587BA57624E8FE043D ();
// 0x00000169 UnityEngine.Vector2 UIUtils::get_ScreenCenter()
extern void UIUtils_get_ScreenCenter_m02176DAC8E9980DC1FD992A05EEEF54F515C9520 ();
// 0x0000016A System.Void UIUtils::.cctor()
extern void UIUtils__cctor_m1D3C3B9DF1D9FCAAD652B92B4D5140B50706B7BD ();
// 0x0000016B System.Collections.IEnumerator CoroutineExtention::WaitAndStart(UnityEngine.MonoBehaviour,System.Single,System.Action)
extern void CoroutineExtention_WaitAndStart_mDDA6FB19C39CD2E7D9D349AFB499EB0CFA3DAF18 ();
// 0x0000016C System.Collections.IEnumerator CoroutineExtention::WaitEndOfFrameAndStart(UnityEngine.MonoBehaviour,System.Action)
extern void CoroutineExtention_WaitEndOfFrameAndStart_m00C7D00B93AFA36C0DC111FEE68E864E160E0CF0 ();
// 0x0000016D System.Collections.IEnumerator CoroutineExtention::StartLerp(UnityEngine.MonoBehaviour,System.Single,System.Action`1<System.Single>,System.Action)
extern void CoroutineExtention_StartLerp_m3A727FA7E154581A38903D6A7668FF3DB369B1DA ();
// 0x0000016E System.Collections.IEnumerator CoroutineExtention::WaitAndPlay(System.Single,System.Action)
extern void CoroutineExtention_WaitAndPlay_m3DBB69F972409FB0881EFB0C167C1BAF6EFF700C ();
// 0x0000016F System.Collections.IEnumerator CoroutineExtention::WaitAndOfFramePlay(System.Action)
extern void CoroutineExtention_WaitAndOfFramePlay_m82A9CFC80E3EF6A3A4828A1E2D90BA57207D43FB ();
// 0x00000170 System.Collections.IEnumerator CoroutineExtention::Lerp(System.Single,System.Action`1<System.Single>,System.Action)
extern void CoroutineExtention_Lerp_mD0BE507B0538EAB128A0F52582B707ACBD4D4FFE ();
// 0x00000171 System.Int32 SessionsCounter::get_Sessions()
extern void SessionsCounter_get_Sessions_mD1946F0ABE19453AE81D1BEC6D4D11F29DF04529 ();
// 0x00000172 System.Void SessionsCounter::set_Sessions(System.Int32)
extern void SessionsCounter_set_Sessions_m0F62F9781C171429775C43E226D5EB98EFC4DF44 ();
// 0x00000173 System.Void SessionsCounter::Awake()
extern void SessionsCounter_Awake_mE4B58141A35F713412CB3A5F0D85DD0688747CF6 ();
// 0x00000174 System.Void SessionsCounter::Start()
extern void SessionsCounter_Start_mFD924F5BB6CCAF13EDC35B12DE4673B3E269BFA0 ();
// 0x00000175 System.Void SessionsCounter::SendDataToAnalytic()
extern void SessionsCounter_SendDataToAnalytic_m2BF02AD8069496E83579868D964C6698EB1FD026 ();
// 0x00000176 System.Void SessionsCounter::.ctor()
extern void SessionsCounter__ctor_mE12217A8A0CBDBC39495E5C693311863EAF09D53 ();
// 0x00000177 System.Void DevLogs::Log(System.String,UnityEngine.Color)
extern void DevLogs_Log_m43F965BDAA60B013A15DA6168AD04B58D65E855E ();
// 0x00000178 System.Void DevLogs::Log(System.String)
extern void DevLogs_Log_mD14BD431CB72EA489C496029839BB59B0E644971 ();
// 0x00000179 System.Void DevLogs::LogError(System.String)
extern void DevLogs_LogError_mC57A1EEEFC741E751327A7EF2F4B0DB6F4226BB7 ();
// 0x0000017A System.Void DevLogs::LogEvent(System.String,UnityEngine.Color)
extern void DevLogs_LogEvent_m774F075AF5F9D5A3D4AD9EBF6393618D5C56C36B ();
// 0x0000017B System.Void DevLogs::LogEvent(System.String)
extern void DevLogs_LogEvent_mCA3FFECED59A147FD0482C67FD4872C9E1062B36 ();
// 0x0000017C System.Void DevLogs::LogData(System.String,UnityEngine.Color)
extern void DevLogs_LogData_mB44B6D038B30592AD981B810E4231E6B6320456A ();
// 0x0000017D System.Void DevLogs::LogData(System.String)
extern void DevLogs_LogData_mE513523D2ECF2D9E5088AAE3FC5E0F766DACEC86 ();
// 0x0000017E System.Void DontDestroyComponent::Awake()
extern void DontDestroyComponent_Awake_mAE5A6523863A8525E6D246239B3C0D53FFDCDF9C ();
// 0x0000017F System.Void DontDestroyComponent::.ctor()
extern void DontDestroyComponent__ctor_mA976254583C9655FB2F8319584B561A8D9DF5F77 ();
// 0x00000180 System.Void GameObjectsUtility::UpOnHierarchy(UnityEngine.Transform)
extern void GameObjectsUtility_UpOnHierarchy_mD336C0B8F76440EB4AA6B1FCCD31CD4BB14CB373 ();
// 0x00000181 System.Void GameObjectsUtility::DownOnHierarchy(UnityEngine.Transform)
extern void GameObjectsUtility_DownOnHierarchy_mF41E49D2BF11FCED993286FB8D7DDCBC97BCC1CC ();
// 0x00000182 System.Void GameObjectsUtility::UnderTargetOnHierarchy(UnityEngine.Transform,UnityEngine.Transform)
extern void GameObjectsUtility_UnderTargetOnHierarchy_m29689323B6BE4005A625C7EF19F8776CB6154576 ();
// 0x00000183 System.Guid GuidObject::get_guid()
extern void GuidObject_get_guid_m786BE73B53F1005C7E7B2B55417C0E53F5F21A1B ();
// 0x00000184 System.Void GuidObject::Generate()
extern void GuidObject_Generate_m7406BB4B5FDFB7EEA45C398D69415CC992CB625B ();
// 0x00000185 System.Void GuidObject::OnEnable()
extern void GuidObject_OnEnable_mA13A63365E58C19A8655823F871E59FF418E5B07 ();
// 0x00000186 System.Void GuidObject::OnValidate()
extern void GuidObject_OnValidate_mC21295F229124552E836C2559E99A4FA53CDB0A8 ();
// 0x00000187 System.Void GuidObject::.ctor()
extern void GuidObject__ctor_m3705EB457F4A8C8F944F6E63A5CD8A6C6F4710D2 ();
// 0x00000188 System.Void ConditionalFieldAttribute::.ctor(System.String,System.Object)
extern void ConditionalFieldAttribute__ctor_mBC1F544EBF98448EE92A6CB8446C3FA75E98A7C4 ();
// 0x00000189 System.Void ConditionalFieldAttribute::.ctor(System.String,System.Object[])
extern void ConditionalFieldAttribute__ctor_m51BD30BC1B1E2E5FFF2B00C7E544EBF38BB2CC0C ();
// 0x0000018A System.Void Field::.ctor(System.String,System.Object)
extern void Field__ctor_mBE57624831A929B7965804B943EFFAD041EE4C00 ();
// 0x0000018B System.Void ConditionalHideAttribute::.ctor(System.String)
extern void ConditionalHideAttribute__ctor_mC4D45E6B80839B169402F47880FEEE5BC552C9F5 ();
// 0x0000018C System.Void ConditionalHideAttribute::.ctor(System.String,System.Boolean)
extern void ConditionalHideAttribute__ctor_m4EE8D31175B6C1192F47E3204DADF4BDF2C86C69 ();
// 0x0000018D System.Void ConditionalHideAttribute::.ctor(System.String,System.Boolean,System.Boolean)
extern void ConditionalHideAttribute__ctor_m4D7F45A915DCC213567EAAD6F37F94C98B1FD91A ();
// 0x0000018E System.Void ConditionalHideAttribute::.ctor(System.Boolean)
extern void ConditionalHideAttribute__ctor_m646B25A1D570E19127AFDAD45650AEC3B3AA8D91 ();
// 0x0000018F System.Void InputController::add_swipeX(System.Action`1<System.Single>)
extern void InputController_add_swipeX_mF0B8F9C68FE4296C35DEA528C6B98EDCE6D3304A ();
// 0x00000190 System.Void InputController::remove_swipeX(System.Action`1<System.Single>)
extern void InputController_remove_swipeX_m27E8CB1ECBBE131F37DCB2D8EFBE9F02ED8AB071 ();
// 0x00000191 System.Void InputController::add_swipeY(System.Action`1<System.Single>)
extern void InputController_add_swipeY_m04A6A3B62E171D641620EC3DDA2A98B66130069E ();
// 0x00000192 System.Void InputController::remove_swipeY(System.Action`1<System.Single>)
extern void InputController_remove_swipeY_m7EDEBF2290CD6CFE579382D0A668D4A0C0BB973D ();
// 0x00000193 System.Void InputController::add_touch(System.Action`1<UnityEngine.Vector3>)
extern void InputController_add_touch_mDB5470CE9BEA65E9F555339709685CF5710C8665 ();
// 0x00000194 System.Void InputController::remove_touch(System.Action`1<UnityEngine.Vector3>)
extern void InputController_remove_touch_m87AD8498377A84890C13C0FF76D55188C6E5C314 ();
// 0x00000195 System.Void InputController::add_touchDown(System.Action)
extern void InputController_add_touchDown_mB9EC81A8B1543D6DCC3129F7021FF18A269755AA ();
// 0x00000196 System.Void InputController::remove_touchDown(System.Action)
extern void InputController_remove_touchDown_mF5AF2AAB0C9CC8FD2695361E2135BEBEDD77C4C5 ();
// 0x00000197 System.Void InputController::add_click(System.Action)
extern void InputController_add_click_mDC5276BD655855E2B43E1B90BEF58EE24C4E143A ();
// 0x00000198 System.Void InputController::remove_click(System.Action)
extern void InputController_remove_click_mD8FADE9873A15028B1629B59CD7CC5601B57DC66 ();
// 0x00000199 System.Void InputController::add_touchUp(System.Action`1<UnityEngine.Vector3>)
extern void InputController_add_touchUp_m33EFE563583F5D92F01A7237A8D93121B4CF092D ();
// 0x0000019A System.Void InputController::remove_touchUp(System.Action`1<UnityEngine.Vector3>)
extern void InputController_remove_touchUp_mF0B84CE802A9AA7ADF9389891B0BED0A83244140 ();
// 0x0000019B System.Void InputController::SubscribeOnSwipeX(System.Action`1<System.Single>)
extern void InputController_SubscribeOnSwipeX_m40BA3C566BC0FC55A70B014E6D65654C7804458F ();
// 0x0000019C System.Void InputController::UnsubscribeOnSwipeX(System.Action`1<System.Single>)
extern void InputController_UnsubscribeOnSwipeX_m17FCF57DC2EC6042A50CE1C2E60B80A5B2269DDB ();
// 0x0000019D System.Void InputController::SubscribeOnSwipeY(System.Action`1<System.Single>)
extern void InputController_SubscribeOnSwipeY_m49A9F991002F1C83DB2F4F1ED122894BA364253A ();
// 0x0000019E System.Void InputController::UnsubscribeOnSwipeY(System.Action`1<System.Single>)
extern void InputController_UnsubscribeOnSwipeY_mC6B8BA90CBB8B9DBE1D52E516E4C519E0045E09E ();
// 0x0000019F System.Void InputController::SubscribeOnTouch(System.Action`1<UnityEngine.Vector3>)
extern void InputController_SubscribeOnTouch_mEF5E4DF74E647268957394C02513AEA7C0DE50C7 ();
// 0x000001A0 System.Void InputController::UnsubscribeOnTouch(System.Action`1<UnityEngine.Vector3>)
extern void InputController_UnsubscribeOnTouch_m6D4817B299B52171CFF0BD4D9D4C3885DE5344B4 ();
// 0x000001A1 System.Void InputController::SubscribeOnTouchDown(System.Action)
extern void InputController_SubscribeOnTouchDown_mDB80DBCD65735389FBBE0759FCE1BFDF04407E54 ();
// 0x000001A2 System.Void InputController::UnsubscribeOnTouchDown(System.Action)
extern void InputController_UnsubscribeOnTouchDown_m64941D513809904B5CEF2A816991E493FC1B905E ();
// 0x000001A3 System.Void InputController::SubscribeOnTouchUp(System.Action`1<UnityEngine.Vector3>)
extern void InputController_SubscribeOnTouchUp_m6DD555F9EC0E86FF62591DA75E2E8AE952C30340 ();
// 0x000001A4 System.Void InputController::UnsubscribeOnTouchUp(System.Action`1<UnityEngine.Vector3>)
extern void InputController_UnsubscribeOnTouchUp_m7CF09321167120D83525134B0367FEC34E01E627 ();
// 0x000001A5 System.Void InputController::Start()
extern void InputController_Start_mF29B2ACC31D1AE10D448EB64E1F1DD7843D85583 ();
// 0x000001A6 System.Void InputController::Update()
extern void InputController_Update_mC6D90E8BA29328DC1D52D36DBAF7282E7817B69E ();
// 0x000001A7 System.Void InputController::FixedUpdate()
extern void InputController_FixedUpdate_mD586509262AA97C61AD36A2C50895D3CA59F6DE1 ();
// 0x000001A8 System.Boolean InputController::HasUiOnTouch()
extern void InputController_HasUiOnTouch_mD4EA7E9576A3D4E14BE2AEF4EA9ED0C486CD8A6C ();
// 0x000001A9 System.Void InputController::.ctor()
extern void InputController__ctor_m7DD1C972F88216307D215651602075F4A4000355 ();
// 0x000001AA System.Void NativeShare::.ctor()
extern void NativeShare__ctor_m4F97EF40BE83F4E69A330C5B2B8CC064AD26E4D7 ();
// 0x000001AB NativeShare NativeShare::SetSubject(System.String)
extern void NativeShare_SetSubject_m4B9B7E447C2FA1B53821BB6D9A1A08DDF8116F72 ();
// 0x000001AC NativeShare NativeShare::SetText(System.String)
extern void NativeShare_SetText_m73B4F78EAB07B4ECAC0DDDC041FC917B7311ED7A ();
// 0x000001AD NativeShare NativeShare::SetTitle(System.String)
extern void NativeShare_SetTitle_m331DC8B2DA3E8E0DF9ABD0EB421EDE13193C8480 ();
// 0x000001AE NativeShare NativeShare::SetTarget(System.String,System.String)
extern void NativeShare_SetTarget_mDAFC39AF5273D396ADDCB5DA2B1BBB9064284BA6 ();
// 0x000001AF NativeShare NativeShare::AddFile(System.String,System.String)
extern void NativeShare_AddFile_m4E424757402F103C17EB778C15E4BFA82B949CB4 ();
// 0x000001B0 System.Void NativeShare::Share()
extern void NativeShare_Share_mA50E72CF1EC13DCF69341E47A40C4635120251CF ();
// 0x000001B1 System.Boolean NativeShare::TargetExists(System.String,System.String)
extern void NativeShare_TargetExists_mE4D5C90044C666BC46B7E571DE483C8EB8942811 ();
// 0x000001B2 System.Boolean PlayerPrefsUtility::IsEncryptedKey(System.String)
extern void PlayerPrefsUtility_IsEncryptedKey_m23EAF5D4E911E063154F35C2C31F2DFE63B0FEDD ();
// 0x000001B3 System.String PlayerPrefsUtility::DecryptKey(System.String)
extern void PlayerPrefsUtility_DecryptKey_mBD8EF37CF3681750B8375F3B5F73C313938961AC ();
// 0x000001B4 System.Void PlayerPrefsUtility::SetEncryptedFloat(System.String,System.Single)
extern void PlayerPrefsUtility_SetEncryptedFloat_mE9553831B3CF6E1CB18C1EB8871BEE0156C2C60E ();
// 0x000001B5 System.Void PlayerPrefsUtility::SetEncryptedInt(System.String,System.Int32)
extern void PlayerPrefsUtility_SetEncryptedInt_mE764BB1EBC28C026B9781DF58FBAACD09424AD08 ();
// 0x000001B6 System.Void PlayerPrefsUtility::SetEncryptedString(System.String,System.String)
extern void PlayerPrefsUtility_SetEncryptedString_m22FE88DF758B29F9225518219BE4A9EF2460ACB4 ();
// 0x000001B7 System.Object PlayerPrefsUtility::GetEncryptedValue(System.String,System.String)
extern void PlayerPrefsUtility_GetEncryptedValue_m21A1E2983297B03300AD724EB9432C8CF22E7E26 ();
// 0x000001B8 System.Single PlayerPrefsUtility::GetEncryptedFloat(System.String,System.Single)
extern void PlayerPrefsUtility_GetEncryptedFloat_m27F51A1DD8463DD3EAB7BD7C902811CF529DB9D7 ();
// 0x000001B9 System.Int32 PlayerPrefsUtility::GetEncryptedInt(System.String,System.Int32)
extern void PlayerPrefsUtility_GetEncryptedInt_mBD711FBAAE2DA81757E577405309C919188FF4F2 ();
// 0x000001BA System.String PlayerPrefsUtility::GetEncryptedString(System.String,System.String)
extern void PlayerPrefsUtility_GetEncryptedString_m65C7FBDB5F58C3C5D84A5AFC1D7F03697B0A9BCB ();
// 0x000001BB System.Void PlayerPrefsUtility::SetBool(System.String,System.Boolean)
extern void PlayerPrefsUtility_SetBool_m96615FCD6D0182E9CC3AB74D26011B2604ABE738 ();
// 0x000001BC System.Boolean PlayerPrefsUtility::GetBool(System.String,System.Boolean)
extern void PlayerPrefsUtility_GetBool_mBF2A9A26A1324970B35E277C461FBE2779A43FAB ();
// 0x000001BD System.Void PlayerPrefsUtility::SetEnum(System.String,System.Enum)
extern void PlayerPrefsUtility_SetEnum_m3B7BB83069907F088F73FF01C4EAE199EC555E59 ();
// 0x000001BE T PlayerPrefsUtility::GetEnum(System.String,T)
// 0x000001BF System.Object PlayerPrefsUtility::GetEnum(System.String,System.Type,System.Object)
extern void PlayerPrefsUtility_GetEnum_m460014287E03B31DAEF84723875EB30DDC5E91FE ();
// 0x000001C0 System.Void PlayerPrefsUtility::SetDateTime(System.String,System.DateTime)
extern void PlayerPrefsUtility_SetDateTime_mC9B22968B6E337BCCB5524F34D825BCA4E7E3E24 ();
// 0x000001C1 System.DateTime PlayerPrefsUtility::GetDateTime(System.String,System.DateTime)
extern void PlayerPrefsUtility_GetDateTime_m8333C41CDA4D8C13730846DEB612A900FA6D14EA ();
// 0x000001C2 System.Void PlayerPrefsUtility::SetTimeSpan(System.String,System.TimeSpan)
extern void PlayerPrefsUtility_SetTimeSpan_m8CE72AE327105109D7F21B03F529BBC8603D3F1B ();
// 0x000001C3 System.TimeSpan PlayerPrefsUtility::GetTimeSpan(System.String,System.TimeSpan)
extern void PlayerPrefsUtility_GetTimeSpan_mA74507D75E3A2680446B0EED6FCCD47E16D7A0BE ();
// 0x000001C4 T Pool`1::Instantiate()
// 0x000001C5 System.Void Pool`1::.ctor()
// 0x000001C6 System.Void SimplePool`1::Start()
// 0x000001C7 System.Void SimplePool`1::Init()
// 0x000001C8 T SimplePool`1::Instantiate()
// 0x000001C9 T SimplePool`1::Spawn(UnityEngine.Vector3,UnityEngine.Quaternion)
// 0x000001CA T SimplePool`1::Spawn()
// 0x000001CB System.Void SimplePool`1::Despawn(T)
// 0x000001CC System.Void SimplePool`1::ClearAllEmptyObjects()
// 0x000001CD System.Void SimplePool`1::ClearScene()
// 0x000001CE System.Void SimplePool`1::Reset()
// 0x000001CF System.Void SimplePool`1::.ctor()
// 0x000001D0 T UiPool`1::Instantiate()
// 0x000001D1 System.Void UiPool`1::.ctor()
// 0x000001D2 System.Void ImagePool::.ctor()
extern void ImagePool__ctor_m0EEAF2330CF435557065DC2581DC654FAEDC43BE ();
// 0x000001D3 System.Void MonoBehaviourPool::.ctor()
extern void MonoBehaviourPool__ctor_mD0EE8199D2A001CD1CFF3F8192D0FF063C5C179B ();
// 0x000001D4 UnityEngine.ParticleSystem ParticlesPool::Spawn()
extern void ParticlesPool_Spawn_m850772CC32D0FCCB4ABDB69033DE556A636329D2 ();
// 0x000001D5 System.Void ParticlesPool::Despawn(UnityEngine.ParticleSystem)
extern void ParticlesPool_Despawn_m9143C790A7DF258165CC0C5CB642D6195E3CD983 ();
// 0x000001D6 System.Void ParticlesPool::.ctor()
extern void ParticlesPool__ctor_m5DFCDCCCB4828CC37878887E0AED28A821A30D44 ();
// 0x000001D7 System.Void TextPool::.ctor()
extern void TextPool__ctor_mC2D89A6043E0650CCC6911C79C46D059F37CA3E7 ();
// 0x000001D8 System.Object RandomUtility::GetRandom(System.Object[])
extern void RandomUtility_GetRandom_mCF1A329EEAF4623156FE59D5AB03A2604478B617 ();
// 0x000001D9 System.Single RandomUtility::GetRandom(System.Single)
extern void RandomUtility_GetRandom_m05A1147D22E7FFB68FC58939F600A71BA4DD4A36 ();
// 0x000001DA System.Boolean ScreenOrientation::get_IsVertical()
extern void ScreenOrientation_get_IsVertical_mC8EAA6475439CB955476868606688F2F00F76012 ();
// 0x000001DB UnityEngine.Vector2 ScreenOrientation::GetMainGameViewSize()
extern void ScreenOrientation_GetMainGameViewSize_mA5B38393754887DC824399E681FA9B41A623BB74 ();
// 0x000001DC System.Void CameraScreenshooter::.ctor()
extern void CameraScreenshooter__ctor_m92DA049F11F3345EC6E0A93C9502781753C3DCC4 ();
// 0x000001DD System.Void TargetConstraintAttribute::.ctor(System.Type)
extern void TargetConstraintAttribute__ctor_m46B1A1BC2C6C2966E070AF063E50132B7AF10532 ();
// 0x000001DE TReturn InvokableCallback`1::Invoke()
// 0x000001DF TReturn InvokableCallback`1::Invoke(System.Object[])
// 0x000001E0 System.Void InvokableCallback`1::.ctor(System.Object,System.String)
// 0x000001E1 TReturn InvokableCallback`2::Invoke(T0)
// 0x000001E2 TReturn InvokableCallback`2::Invoke(System.Object[])
// 0x000001E3 System.Void InvokableCallback`2::.ctor(System.Object,System.String)
// 0x000001E4 TReturn InvokableCallback`3::Invoke(T0,T1)
// 0x000001E5 TReturn InvokableCallback`3::Invoke(System.Object[])
// 0x000001E6 System.Void InvokableCallback`3::.ctor(System.Object,System.String)
// 0x000001E7 TReturn InvokableCallback`4::Invoke(T0,T1,T2)
// 0x000001E8 TReturn InvokableCallback`4::Invoke(System.Object[])
// 0x000001E9 System.Void InvokableCallback`4::.ctor(System.Object,System.String)
// 0x000001EA TReturn InvokableCallback`5::Invoke(T0,T1,T2,T3)
// 0x000001EB TReturn InvokableCallback`5::Invoke(System.Object[])
// 0x000001EC System.Void InvokableCallback`5::.ctor(System.Object,System.String)
// 0x000001ED TReturn InvokableCallbackBase`1::Invoke(System.Object[])
// 0x000001EE System.Void InvokableCallbackBase`1::.ctor()
// 0x000001EF System.Void InvokableEvent::Invoke()
extern void InvokableEvent_Invoke_m20BB6875C0AE752AFAFAE44C2F626F8E66A2E862 ();
// 0x000001F0 System.Void InvokableEvent::Invoke(System.Object[])
extern void InvokableEvent_Invoke_m8673CE9D075B952E7389D9FAA829BC815B9BD44F ();
// 0x000001F1 System.Void InvokableEvent::.ctor(System.Object,System.String)
extern void InvokableEvent__ctor_m3BC3009ED31135D4AF34A436158469393A70D650 ();
// 0x000001F2 System.Void InvokableEvent`1::Invoke(T0)
// 0x000001F3 System.Void InvokableEvent`1::Invoke(System.Object[])
// 0x000001F4 System.Void InvokableEvent`1::.ctor(System.Object,System.String)
// 0x000001F5 System.Void InvokableEvent`2::Invoke(T0,T1)
// 0x000001F6 System.Void InvokableEvent`2::Invoke(System.Object[])
// 0x000001F7 System.Void InvokableEvent`2::.ctor(System.Object,System.String)
// 0x000001F8 System.Void InvokableEvent`3::Invoke(T0,T1,T2)
// 0x000001F9 System.Void InvokableEvent`3::Invoke(System.Object[])
// 0x000001FA System.Void InvokableEvent`3::.ctor(System.Object,System.String)
// 0x000001FB System.Void InvokableEvent`4::Invoke(T0,T1,T2,T3)
// 0x000001FC System.Void InvokableEvent`4::Invoke(System.Object[])
// 0x000001FD System.Void InvokableEvent`4::.ctor(System.Object,System.String)
// 0x000001FE System.Void InvokableEventBase::Invoke(System.Object[])
// 0x000001FF System.Void InvokableEventBase::.ctor()
extern void InvokableEventBase__ctor_m3D3870E953D0DCDA9A06BB6DE5FAD288D8C79428 ();
// 0x00000200 TReturn SerializableCallback`1::Invoke()
// 0x00000201 System.Void SerializableCallback`1::Cache()
// 0x00000202 System.Void SerializableCallback`1::.ctor()
// 0x00000203 TReturn SerializableCallback`2::Invoke(T0)
// 0x00000204 System.Void SerializableCallback`2::Cache()
// 0x00000205 System.Void SerializableCallback`2::.ctor()
// 0x00000206 TReturn SerializableCallback`3::Invoke(T0,T1)
// 0x00000207 System.Void SerializableCallback`3::Cache()
// 0x00000208 System.Void SerializableCallback`3::.ctor()
// 0x00000209 TReturn SerializableCallback`4::Invoke(T0,T1,T2)
// 0x0000020A System.Void SerializableCallback`4::Cache()
// 0x0000020B System.Void SerializableCallback`4::.ctor()
// 0x0000020C TReturn SerializableCallback`5::Invoke(T0,T1,T2,T3)
// 0x0000020D System.Void SerializableCallback`5::Cache()
// 0x0000020E System.Void SerializableCallback`5::.ctor()
// 0x0000020F System.Void SerializableCallbackBase`1::ClearCache()
// 0x00000210 InvokableCallbackBase`1<TReturn> SerializableCallbackBase`1::GetPersistentMethod()
// 0x00000211 System.Void SerializableCallbackBase`1::.ctor()
// 0x00000212 UnityEngine.Object SerializableCallbackBase::get_target()
extern void SerializableCallbackBase_get_target_mD7C4940393FA0D66A7DAA96AADCDE9312B4A8F19 ();
// 0x00000213 System.Void SerializableCallbackBase::set_target(UnityEngine.Object)
extern void SerializableCallbackBase_set_target_m3F4BE939AFA6440BE133C54F60E98D1C0B5A3324 ();
// 0x00000214 System.String SerializableCallbackBase::get_methodName()
extern void SerializableCallbackBase_get_methodName_mDBF935E0552D4242A8116EF1B1DA7B6683B517E4 ();
// 0x00000215 System.Void SerializableCallbackBase::set_methodName(System.String)
extern void SerializableCallbackBase_set_methodName_m55478D20291E555F9CD27CF5FDF3A3257E61FDC2 ();
// 0x00000216 System.Object[] SerializableCallbackBase::get_Args()
extern void SerializableCallbackBase_get_Args_mE2540440CD6B8C2664EA692F14B214B28236778B ();
// 0x00000217 System.Type[] SerializableCallbackBase::get_ArgTypes()
extern void SerializableCallbackBase_get_ArgTypes_m43D4E0E895546DCFDF7CA01632EFE40BC5C76CBD ();
// 0x00000218 System.Boolean SerializableCallbackBase::get_dynamic()
extern void SerializableCallbackBase_get_dynamic_m59E43E27B0DD1B889DC7DE466C863142C2DCCB0A ();
// 0x00000219 System.Void SerializableCallbackBase::set_dynamic(System.Boolean)
extern void SerializableCallbackBase_set_dynamic_mF1E2F025F26D48595B4CC8A144DF91F2A3C35F17 ();
// 0x0000021A System.Void SerializableCallbackBase::ClearCache()
extern void SerializableCallbackBase_ClearCache_mC38BFF1D60BB2F8871B83FD35AD8A3A591C0C281 ();
// 0x0000021B System.Void SerializableCallbackBase::SetMethod(UnityEngine.Object,System.String,System.Boolean,Arg[])
extern void SerializableCallbackBase_SetMethod_m0F58A4494D86F3B8B6809D245A7C205F49F9080B ();
// 0x0000021C System.Void SerializableCallbackBase::Cache()
// 0x0000021D System.Void SerializableCallbackBase::OnBeforeSerialize()
extern void SerializableCallbackBase_OnBeforeSerialize_m89E573793043132B90660076F3B7B97F09ABF843 ();
// 0x0000021E System.Void SerializableCallbackBase::OnAfterDeserialize()
extern void SerializableCallbackBase_OnAfterDeserialize_m8DE492E4E800D46946D6EC872B289C01B204F711 ();
// 0x0000021F System.Void SerializableCallbackBase::.ctor()
extern void SerializableCallbackBase__ctor_mD14E4A83A68F826ABF7869024D711FFE24F09E95 ();
// 0x00000220 System.Object Arg::GetValue()
extern void Arg_GetValue_mD74C36F6C755AB044DC20ECF0CD4D5396896EFA8_AdjustorThunk ();
// 0x00000221 System.Object Arg::GetValue(Arg_ArgType)
extern void Arg_GetValue_mD70CB98B049A3BE216251B26833C98D02E24942C_AdjustorThunk ();
// 0x00000222 System.Type Arg::RealType(Arg_ArgType)
extern void Arg_RealType_m2A2F631F1878EB212A7465D164230068DB32079C ();
// 0x00000223 Arg_ArgType Arg::FromRealType(System.Type)
extern void Arg_FromRealType_m1A86C61ED0F7E81413C7844C2A4A46108F3F5173 ();
// 0x00000224 System.Boolean Arg::IsSupported(System.Type)
extern void Arg_IsSupported_mCDF0DB7E36D6FFE5FE79A0A63C71982B183EB903 ();
// 0x00000225 System.Void SerializableEvent::Invoke()
extern void SerializableEvent_Invoke_mE60EF64DA2071762602A8E6D2E36CCD630E859A6 ();
// 0x00000226 System.Void SerializableEvent::Cache()
extern void SerializableEvent_Cache_m410988D57ADC667418639D1C1AEE894CB1D6C6E2 ();
// 0x00000227 System.Void SerializableEvent::.ctor()
extern void SerializableEvent__ctor_m83DC30062D0F27C66F02CC78B6EE7AE79FD55E73 ();
// 0x00000228 System.Void SerializableEvent`1::Invoke(T0)
// 0x00000229 System.Void SerializableEvent`1::Cache()
// 0x0000022A System.Void SerializableEvent`1::.ctor()
// 0x0000022B System.Void SerializableEvent`2::Invoke(T0,T1)
// 0x0000022C System.Void SerializableEvent`2::Cache()
// 0x0000022D System.Void SerializableEvent`2::.ctor()
// 0x0000022E System.Void SerializableEvent`3::Invoke(T0,T1,T2)
// 0x0000022F System.Void SerializableEvent`3::Cache()
// 0x00000230 System.Void SerializableEvent`3::.ctor()
// 0x00000231 System.Void SerializableEvent`4::Invoke(T0,T1,T2,T3)
// 0x00000232 System.Void SerializableEvent`4::Cache()
// 0x00000233 System.Void SerializableEvent`4::.ctor()
// 0x00000234 System.Void SerializableEventBase::ClearCache()
extern void SerializableEventBase_ClearCache_m84253DAAFD81F37E4AC12B3627C609CAD7D118E8 ();
// 0x00000235 InvokableEventBase SerializableEventBase::GetPersistentMethod()
extern void SerializableEventBase_GetPersistentMethod_m07A3B8FAE4050AF2DEF91E41A88336B09B267118 ();
// 0x00000236 System.Void SerializableEventBase::.ctor()
extern void SerializableEventBase__ctor_m0BEC3D730F07C5E3A7DFBB18E481385106629BE9 ();
// 0x00000237 System.Void ServiceUtils::OpenUrl(UnityEngine.MonoBehaviour,System.String)
extern void ServiceUtils_OpenUrl_m289BC09F15CCB13AA2CFC859B6318964CB2291B9 ();
// 0x00000238 T Singletone`1::get_Instance()
// 0x00000239 System.Void Singletone`1::.ctor()
// 0x0000023A System.Void StandardShaderUtils::ChangeRenderMode(UnityEngine.Material,StandardShaderUtils_BlendMode)
extern void StandardShaderUtils_ChangeRenderMode_mB9CF3B509DBDE18954C1464C9B315FE051B05A8E ();
// 0x0000023B System.Void BoolStorageData::.ctor(System.String)
extern void BoolStorageData__ctor_mD151C642C30D2BB439F1092C0DD20B5FC5D9D943 ();
// 0x0000023C System.Void BoolStorageData::.ctor(System.String,System.Boolean)
extern void BoolStorageData__ctor_mBA223D0250F4353CBB0793B290254E5E96075CDE ();
// 0x0000023D System.Void BoolStorageData::.ctor(System.String,System.Boolean,System.Boolean)
extern void BoolStorageData__ctor_m3A6C4BD2787FA8DAF0D908B2E94278AE677A445A ();
// 0x0000023E System.Void BoolStorageData::Init()
extern void BoolStorageData_Init_m7D115A6E379845BD4FC9AF74B77C86A6A0336D26 ();
// 0x0000023F System.Void BoolStorageData::Save()
extern void BoolStorageData_Save_m3F1C81550B43682DC2E7EA7BEEA603BC7710827D ();
// 0x00000240 System.Void StorageData`1::add_getter(StorageData`1_GetterDelegate<T>)
// 0x00000241 System.Void StorageData`1::remove_getter(StorageData`1_GetterDelegate<T>)
// 0x00000242 System.Void StorageData`1::add_setter(StorageData`1_SetterDelegate<T>)
// 0x00000243 System.Void StorageData`1::remove_setter(StorageData`1_SetterDelegate<T>)
// 0x00000244 System.Void StorageData`1::add_setValue(StorageData`1_SetsListenerDelegate<T>)
// 0x00000245 System.Void StorageData`1::remove_setValue(StorageData`1_SetsListenerDelegate<T>)
// 0x00000246 T StorageData`1::get_Value()
// 0x00000247 System.Void StorageData`1::set_Value(T)
// 0x00000248 System.Void StorageData`1::.ctor(System.String,System.Boolean)
// 0x00000249 System.Void StorageData`1::RegisterSetValueCallback(StorageData`1_SetsListenerDelegate<T>)
// 0x0000024A System.Void StorageData`1::UnregisterSetValueCallback(StorageData`1_SetsListenerDelegate<T>)
// 0x0000024B System.Void StorageData`1::RegisterGetter(StorageData`1_GetterDelegate<T>)
// 0x0000024C System.Void StorageData`1::UnregisterGetter(StorageData`1_GetterDelegate<T>)
// 0x0000024D System.Void StorageData`1::RegisterSetter(StorageData`1_SetterDelegate<T>)
// 0x0000024E System.Void StorageData`1::UnregisterSetter(StorageData`1_SetterDelegate<T>)
// 0x0000024F System.Void StorageData`1::Init()
// 0x00000250 System.Void StorageData`1::Save()
// 0x00000251 System.Void DateTimeStorageData::.ctor(System.String)
extern void DateTimeStorageData__ctor_mBCCFE90C68EDAAEFBEF621119CC8841A08F8462D ();
// 0x00000252 System.Void DateTimeStorageData::.ctor(System.String,System.Boolean)
extern void DateTimeStorageData__ctor_m89C4BF7244E93BA4BE4A06DCF15F29149C47D9BE ();
// 0x00000253 System.Void DateTimeStorageData::Init()
extern void DateTimeStorageData_Init_m05068D2D5F6BB86718BE39CBFFAAA853C984A576 ();
// 0x00000254 System.Void DateTimeStorageData::Save()
extern void DateTimeStorageData_Save_m234DDE18B926562A9E5422811692DEEFF4428D65 ();
// 0x00000255 System.Void FloatStorageData::.ctor(System.String)
extern void FloatStorageData__ctor_mBB4A3BCDCF8B56948D1FA47A1AC93E52A510770B ();
// 0x00000256 System.Void FloatStorageData::.ctor(System.String,System.Boolean)
extern void FloatStorageData__ctor_mF20E9E37D7AB58FF362D3F4EC1BB6AD4FF764E4F ();
// 0x00000257 System.Void FloatStorageData::Init()
extern void FloatStorageData_Init_mCE58B40959A4148E22C0EB3FECDEA8AED807F579 ();
// 0x00000258 System.Void FloatStorageData::Save()
extern void FloatStorageData_Save_m1961DE9E7575E485924C54AAFA9E72A8291DCAAB ();
// 0x00000259 System.Void IntStorageData::.ctor(System.String,System.Int32,System.Boolean)
extern void IntStorageData__ctor_m4635B6F0F8421C4E1E334A64D4957512341F3C62 ();
// 0x0000025A System.Void IntStorageData::Init()
extern void IntStorageData_Init_m89529AD6BFB757A9476FE273B38B9A25D1D7A105 ();
// 0x0000025B System.Void IntStorageData::Save()
extern void IntStorageData_Save_mD052ACEA756F910700912192298CB931F2C706DC ();
// 0x0000025C System.Void StringStorageData::.ctor(System.String)
extern void StringStorageData__ctor_mD982C2B866CCDEB211422C052C1AFDC4B6E82D0D ();
// 0x0000025D System.Void StringStorageData::.ctor(System.String,System.Boolean)
extern void StringStorageData__ctor_m2481FBA08EDA6B7193940B2661C343AC4B7B5978 ();
// 0x0000025E System.Void StringStorageData::.ctor(System.String,System.Boolean,System.String)
extern void StringStorageData__ctor_m81A17949BBB1C9AF49ECCE50CFFDA215F29337A5 ();
// 0x0000025F System.Void StringStorageData::Init()
extern void StringStorageData_Init_m27C4FDB0A7F80BCD4C97D1971D6373D248A0C1BA ();
// 0x00000260 System.Void StringStorageData::Save()
extern void StringStorageData_Save_mC96CA10EB2F5DA271C9292E29F234EB35A186BAD ();
// 0x00000261 DateTimeStorageData Timer::get_TriggerDate()
extern void Timer_get_TriggerDate_m5179FD168A24F54B94DC1323B7B1153C0C1A0980 ();
// 0x00000262 System.Void Timer::add_timerTriggerAction(System.Action)
extern void Timer_add_timerTriggerAction_mA9FA7FA0B68CA2903CC26845812F02EBEE20C595 ();
// 0x00000263 System.Void Timer::remove_timerTriggerAction(System.Action)
extern void Timer_remove_timerTriggerAction_m650C0F4EB2F70C611132DFE753395EC6CA24066C ();
// 0x00000264 System.Void Timer::add_timerUpdateAction(System.Action`1<System.Int32>)
extern void Timer_add_timerUpdateAction_mA6061193C68DDA2DEE634FD6AB4139D6B93E2467 ();
// 0x00000265 System.Void Timer::remove_timerUpdateAction(System.Action`1<System.Int32>)
extern void Timer_remove_timerUpdateAction_m5C8B5AE51AAB428970A8A6C11B6207E8A1C488B5 ();
// 0x00000266 System.Int32 Timer::get_Seconds()
extern void Timer_get_Seconds_m3BA996BF755EFD6DB2B59641D1CBD4696D472E57 ();
// 0x00000267 System.Boolean Timer::get_IsFirstLaunch()
extern void Timer_get_IsFirstLaunch_m928E1294BD4642770120EA9A14952F35E4EF5A0D ();
// 0x00000268 System.Boolean Timer::get_IsStarted()
extern void Timer_get_IsStarted_m925CCA799EFFCCC164629379367083CEFBE81181 ();
// 0x00000269 System.Void Timer::.ctor(System.String,System.Int32,System.Boolean,System.Boolean)
extern void Timer__ctor_m15C77C4F2E9DFA1ECF01BB4692A650F5523C6C79 ();
// 0x0000026A System.Void Timer::Start()
extern void Timer_Start_mEFDCF17412717C132D11A41F14C8F9E96A4B78E4 ();
// 0x0000026B System.Void Timer::Tick()
extern void Timer_Tick_mDEA0EABC83566AEFA72ECAF67011774B88E110B3 ();
// 0x0000026C System.Int32 Timer::SecondsToTrigger()
extern void Timer_SecondsToTrigger_m779D80779CB5634D86B1E769EF823CE401FE1FD6 ();
// 0x0000026D System.Void Timer::SetNextTriggerTime(System.DateTime)
extern void Timer_SetNextTriggerTime_m1938E93B3FFE1A7C33D7724D4F9DF00BB5EE1F76 ();
// 0x0000026E System.Void Timer::InitDate(System.Int32)
extern void Timer_InitDate_mA09EAD975CC83FD8CB30CE87B18FC221767F422F ();
// 0x0000026F System.Void Timer::InitEvents()
extern void Timer_InitEvents_m15CAA532FD224DC4C04720E861554886FCE10CE4 ();
// 0x00000270 System.Void Timer::RegisterNextTriggerDate()
extern void Timer_RegisterNextTriggerDate_m50A0BF798FC89118058C66FD6C94B1EF611879A7 ();
// 0x00000271 System.Void Timer::<InitEvents>b__27_0()
extern void Timer_U3CInitEventsU3Eb__27_0_m92C1657F316FE1E69E3BE691D18173A6BD999F3A ();
// 0x00000272 System.Void TimerManager::Register(Timer)
extern void TimerManager_Register_m96ECADBC8F2294868F5D9ECFB9C7C202BBB526B5 ();
// 0x00000273 System.Void TimerManager::RegisterTimer(Timer)
extern void TimerManager_RegisterTimer_m8D0494786E76F783C8E3A0C562767C1FBC01632A ();
// 0x00000274 System.Boolean TimerManager::IsStarted(Timer)
extern void TimerManager_IsStarted_m1B61C536FCF962C161B368C6673E3A90796117F0 ();
// 0x00000275 System.Void TimerManager::StartTicker()
extern void TimerManager_StartTicker_m7C6AA77F6E57D1F4A995532706A83EEBB7972CC9 ();
// 0x00000276 System.Collections.IEnumerator TimerManager::Ticker()
extern void TimerManager_Ticker_mD17229B13330973D3F4130CA9904597F859B1E08 ();
// 0x00000277 System.Void TimerManager::.ctor()
extern void TimerManager__ctor_mCBA279B0F7D07D6FAF9511FCC5FD739927B72AA3 ();
// 0x00000278 System.Void vp_Activity::Empty()
extern void vp_Activity_Empty_m1CCC6F70B1E97A3193847E35DF652ADF4C572093 ();
// 0x00000279 System.Boolean vp_Activity::AlwaysOK()
extern void vp_Activity_AlwaysOK_m2C4ADE579DB601543E98BF65E5733E78C6CF6B35 ();
// 0x0000027A System.Void vp_Activity::.ctor(System.String)
extern void vp_Activity__ctor_mB9C79F12E59E862CCA79D2ECBEECF19B0C4F484B ();
// 0x0000027B System.Single vp_Activity::get_MinPause()
extern void vp_Activity_get_MinPause_mEF6E369277C5FFCAFD1F01F7CA84DDF124800A5D ();
// 0x0000027C System.Void vp_Activity::set_MinPause(System.Single)
extern void vp_Activity_set_MinPause_mDA5C90ADED913F2CBD7CE4FA29429E2F821FA8B1 ();
// 0x0000027D System.Single vp_Activity::get_MinDuration()
extern void vp_Activity_get_MinDuration_m201E2CAB9F6EBF53C69CB52D383091A931FD28BE ();
// 0x0000027E System.Void vp_Activity::set_MinDuration(System.Single)
extern void vp_Activity_set_MinDuration_m833A8C332C4D9B8F9426DE4BCDFF8FAD20EC613E ();
// 0x0000027F System.Single vp_Activity::get_AutoDuration()
extern void vp_Activity_get_AutoDuration_m83DBE1A23C39CA13D18BB0AC9DE1D1F5C84AD72C ();
// 0x00000280 System.Void vp_Activity::set_AutoDuration(System.Single)
extern void vp_Activity_set_AutoDuration_mE8DB9262C7F9E826DDCC266DB773996C621710DB ();
// 0x00000281 System.Object vp_Activity::get_Argument()
extern void vp_Activity_get_Argument_mC62FDB8F72F4770FC18D51649AC14BA2DE075A0F ();
// 0x00000282 System.Void vp_Activity::set_Argument(System.Object)
extern void vp_Activity_set_Argument_m0302CDD118A3C3794CA7D14E3FAB6C58471F7032 ();
// 0x00000283 System.Void vp_Activity::InitFields()
extern void vp_Activity_InitFields_m7430B8B5D663D5823542CBE37529E1AD2A8D2DDE ();
// 0x00000284 System.Void vp_Activity::Register(System.Object,System.String,System.Int32)
extern void vp_Activity_Register_m025A7BD21446A5F1520A4F7AB4BF0A9E9BD3DF6D ();
// 0x00000285 System.Void vp_Activity::Unregister(System.Object)
extern void vp_Activity_Unregister_m52DB0C66F687DE2C090BD70E262DCEB998D58A13 ();
// 0x00000286 System.Boolean vp_Activity::TryStart(System.Boolean)
extern void vp_Activity_TryStart_m0EC8FAE9DDF548D26F1BFD594989AA816B0057A8 ();
// 0x00000287 System.Boolean vp_Activity::TryStop(System.Boolean)
extern void vp_Activity_TryStop_mE37B0E95A65E7A3B7EF398BD852E13E3B99E1617 ();
// 0x00000288 System.Void vp_Activity::set_Active(System.Boolean)
extern void vp_Activity_set_Active_m3066D0EC9BABFA7CC9FDEA4D127029D181571FFB ();
// 0x00000289 System.Boolean vp_Activity::get_Active()
extern void vp_Activity_get_Active_m86244ADC8E6FAB827A025DB6529BC1380CE915DE ();
// 0x0000028A System.Void vp_Activity::Start(System.Single)
extern void vp_Activity_Start_mE6A5F6FCD03AB51038647E2C886667EA3A4BF330 ();
// 0x0000028B System.Void vp_Activity::Stop(System.Single)
extern void vp_Activity_Stop_m35325F560384D71C36E272DCF497D5F56E6B3FED ();
// 0x0000028C System.Void vp_Activity::Disallow(System.Single)
extern void vp_Activity_Disallow_mB5372995AF5AAF24F510592F58BD6242407DCFE9 ();
// 0x0000028D System.Void vp_Activity::<set_Active>b__37_0()
extern void vp_Activity_U3Cset_ActiveU3Eb__37_0_m9ADE28DE480CE352EDC151D875333C65ABB8F253 ();
// 0x0000028E System.Void vp_Activity`1::.ctor(System.String)
// 0x0000028F System.Boolean vp_Activity`1::TryStart(T)
// 0x00000290 System.Boolean vp_Attempt::AlwaysOK()
extern void vp_Attempt_AlwaysOK_m80CC125F243D6FA1583EBBCD1D182B0DA2092A0E ();
// 0x00000291 System.Void vp_Attempt::.ctor(System.String)
extern void vp_Attempt__ctor_m3118443A4EB35B402D253E8FAE20B39A99096F26 ();
// 0x00000292 System.Void vp_Attempt::InitFields()
extern void vp_Attempt_InitFields_mDCCE6F2DAC058EC1FCA58CC9E923891ED655A610 ();
// 0x00000293 System.Void vp_Attempt::Register(System.Object,System.String,System.Int32)
extern void vp_Attempt_Register_mAE67F6C5DD17EE988742057D09097A37F596C5F0 ();
// 0x00000294 System.Void vp_Attempt::Unregister(System.Object)
extern void vp_Attempt_Unregister_mE6C437049AA0E089AABE43E7678AFB590871701E ();
// 0x00000295 System.Boolean vp_Attempt`1::AlwaysOK(T)
// 0x00000296 System.Void vp_Attempt`1::.ctor(System.String)
// 0x00000297 System.Void vp_Attempt`1::InitFields()
// 0x00000298 System.Void vp_Attempt`1::Register(System.Object,System.String,System.Int32)
// 0x00000299 System.Void vp_Attempt`1::Unregister(System.Object)
// 0x0000029A System.String vp_Event::get_EventName()
extern void vp_Event_get_EventName_m62614B846C0E4BB14613C4FBFC79CB8DABAF4E42 ();
// 0x0000029B System.Type vp_Event::get_Type()
extern void vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183 ();
// 0x0000029C System.Type vp_Event::get_ArgumentType()
extern void vp_Event_get_ArgumentType_mE22F5DF43AA062B9D94CFBA26DBD26DF4F6F6958 ();
// 0x0000029D System.Type vp_Event::get_ReturnType()
extern void vp_Event_get_ReturnType_m2F546B33B4B596CF789400F4AFB23B9B360F8F06 ();
// 0x0000029E System.Void vp_Event::Register(System.Object,System.String,System.Int32)
// 0x0000029F System.Void vp_Event::Unregister(System.Object)
// 0x000002A0 System.Void vp_Event::InitFields()
// 0x000002A1 System.Void vp_Event::.ctor(System.String)
extern void vp_Event__ctor_mBAC4F17FDA57FE7F9766E60433C5A10BF198D4E7 ();
// 0x000002A2 System.Void vp_Event::StoreInvokerFieldNames()
extern void vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF ();
// 0x000002A3 System.Type vp_Event::MakeGenericType(System.Type)
extern void vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403 ();
// 0x000002A4 System.Void vp_Event::SetFieldToExternalMethod(System.Object,System.Reflection.FieldInfo,System.String,System.Type)
extern void vp_Event_SetFieldToExternalMethod_mA9E3D6BBEDCBA637A2FA457E5DC0820B5BF3D9FE ();
// 0x000002A5 System.Void vp_Event::AddExternalMethodToField(System.Object,System.Reflection.FieldInfo,System.String,System.Type)
extern void vp_Event_AddExternalMethodToField_m167005BC363AE73C25FF8679197A5C8E13EF63E7 ();
// 0x000002A6 System.Void vp_Event::SetFieldToLocalMethod(System.Reflection.FieldInfo,System.Reflection.MethodInfo,System.Type)
extern void vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C ();
// 0x000002A7 System.Void vp_Event::RemoveExternalMethodFromField(System.Object,System.Reflection.FieldInfo)
extern void vp_Event_RemoveExternalMethodFromField_mE1961E2F41C102B5B7FDC0C24539E47F7B5D5041 ();
// 0x000002A8 System.Reflection.MethodInfo vp_Event::GetStaticGenericMethod(System.Type,System.String,System.Type,System.Type)
extern void vp_Event_GetStaticGenericMethod_mC5A9BB11EAE19B27D7A4F37BEDB9C0C38BCA98F7 ();
// 0x000002A9 System.Type vp_Event::get_GetArgumentType()
extern void vp_Event_get_GetArgumentType_mC46CB784CDC20810B34182F12FF38DE1536611D4 ();
// 0x000002AA System.Type vp_Event::get_GetGenericReturnType()
extern void vp_Event_get_GetGenericReturnType_mC788697DC4D5638D32A31F774CCF112AA22F2B16 ();
// 0x000002AB System.Type vp_Event::GetParameterType(System.Int32)
extern void vp_Event_GetParameterType_mDBFB1CE4D2C3A1BC89BF0103976F24AF0FF2148A ();
// 0x000002AC System.Type vp_Event::GetReturnType(System.Int32)
extern void vp_Event_GetReturnType_mB691B6F9F31097CC9B6F14A0EA44D21900DC70BF ();
// 0x000002AD System.Void vp_Event::Refresh()
extern void vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D ();
// 0x000002AE System.String vp_EventDump::Dump(vp_EventHandler,System.String[])
extern void vp_EventDump_Dump_mA71BDE5B4877A8388A569E50608A2AAB8D0B153B ();
// 0x000002AF System.String vp_EventDump::DumpEventsOfType(System.String,System.String,vp_EventHandler)
extern void vp_EventDump_DumpEventsOfType_m41DA6EEEEC4BF8BDDCC2183F2B5702FDFFAB9AB2 ();
// 0x000002B0 System.String vp_EventDump::DumpEventListeners(System.Object,System.String[])
extern void vp_EventDump_DumpEventListeners_mE78660A26959E527A851634D572E8B26600538AC ();
// 0x000002B1 System.String[] vp_EventDump::GetMethodNames(System.Delegate[])
extern void vp_EventDump_GetMethodNames_m9C32399E4F83E07AC1EB8D21EF07AF4993382410 ();
// 0x000002B2 System.Delegate[] vp_EventDump::RemoveDelegatesFromList(System.Delegate[])
extern void vp_EventDump_RemoveDelegatesFromList_mBC7EACE77CD372CFF33AF3DF4F665E7116A01A20 ();
// 0x000002B3 System.String vp_EventDump::DumpDelegateNames(System.String[])
extern void vp_EventDump_DumpDelegateNames_m49FFFF6E77963E45B7FADF29CE27B632E6E762FD ();
// 0x000002B4 System.Void vp_EventDump::.ctor()
extern void vp_EventDump__ctor_mA5DAF99C1397ECE410E7DD3135E34FF3A9498AF8 ();
// 0x000002B5 System.Void vp_EventHandler::Awake()
extern void vp_EventHandler_Awake_m71C0B2100844C710B13F313A90C45402850714F6 ();
// 0x000002B6 System.Void vp_EventHandler::StoreHandlerEvents()
extern void vp_EventHandler_StoreHandlerEvents_mB9C45AC4A615E27D8FAC220F66D1F00D51045FF0 ();
// 0x000002B7 System.Collections.Generic.List`1<System.Reflection.FieldInfo> vp_EventHandler::GetFields()
extern void vp_EventHandler_GetFields_m5737E607BCE7CA8A0CAA50A665ECE9636FC9C5C4 ();
// 0x000002B8 System.Void vp_EventHandler::Register(System.Object)
extern void vp_EventHandler_Register_m97E15AE2CBA236F376306CAF1B88A6CA8A52DD3A ();
// 0x000002B9 System.Void vp_EventHandler::Unregister(System.Object)
extern void vp_EventHandler_Unregister_mCD10136B77EE7EB6DC70F39A4694755C4D13C8E0 ();
// 0x000002BA System.Boolean vp_EventHandler::CompareMethodSignatures(System.Reflection.MethodInfo,System.Type,System.Type)
extern void vp_EventHandler_CompareMethodSignatures_m3E143FC4F85E6441AADE35DCC93C83624065B65C ();
// 0x000002BB vp_EventHandler_ScriptMethods vp_EventHandler::GetScriptMethods(System.Object)
extern void vp_EventHandler_GetScriptMethods_m283D015F14209320667DED777FD89F5F9582C8E9 ();
// 0x000002BC System.Void vp_EventHandler::.ctor()
extern void vp_EventHandler__ctor_m9AA2633E5AC8D8B862D0B21D2A50880758A7EBD5 ();
// 0x000002BD System.Void vp_EventHandler::.cctor()
extern void vp_EventHandler__cctor_mA6869EA1932DF2FBED78E01A282E0103F6F4D39C ();
// 0x000002BE System.Void vp_GlobalCallback::.ctor(System.Object,System.IntPtr)
extern void vp_GlobalCallback__ctor_m5C8660C98B8E87B2CDA39E537FC8EFFC1E0D7C4E ();
// 0x000002BF System.Void vp_GlobalCallback::Invoke()
extern void vp_GlobalCallback_Invoke_mCF95398C8DFBF4B64CA962CF5EE074C52F7D9CD2 ();
// 0x000002C0 System.IAsyncResult vp_GlobalCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void vp_GlobalCallback_BeginInvoke_m0DD17507DF7F8F5576EB7F2524E86DE02E21D86F ();
// 0x000002C1 System.Void vp_GlobalCallback::EndInvoke(System.IAsyncResult)
extern void vp_GlobalCallback_EndInvoke_m6D5C22925F7D6F5E41F9370301C938D1FD8FC7F3 ();
// 0x000002C2 System.Void vp_GlobalCallback`1::.ctor(System.Object,System.IntPtr)
// 0x000002C3 System.Void vp_GlobalCallback`1::Invoke(T)
// 0x000002C4 System.IAsyncResult vp_GlobalCallback`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000002C5 System.Void vp_GlobalCallback`1::EndInvoke(System.IAsyncResult)
// 0x000002C6 System.Void vp_GlobalCallback`2::.ctor(System.Object,System.IntPtr)
// 0x000002C7 System.Void vp_GlobalCallback`2::Invoke(T,U)
// 0x000002C8 System.IAsyncResult vp_GlobalCallback`2::BeginInvoke(T,U,System.AsyncCallback,System.Object)
// 0x000002C9 System.Void vp_GlobalCallback`2::EndInvoke(System.IAsyncResult)
// 0x000002CA System.Void vp_GlobalCallback`3::.ctor(System.Object,System.IntPtr)
// 0x000002CB System.Void vp_GlobalCallback`3::Invoke(T,U,V)
// 0x000002CC System.IAsyncResult vp_GlobalCallback`3::BeginInvoke(T,U,V,System.AsyncCallback,System.Object)
// 0x000002CD System.Void vp_GlobalCallback`3::EndInvoke(System.IAsyncResult)
// 0x000002CE System.Void vp_GlobalCallbackReturn`1::.ctor(System.Object,System.IntPtr)
// 0x000002CF R vp_GlobalCallbackReturn`1::Invoke()
// 0x000002D0 System.IAsyncResult vp_GlobalCallbackReturn`1::BeginInvoke(System.AsyncCallback,System.Object)
// 0x000002D1 R vp_GlobalCallbackReturn`1::EndInvoke(System.IAsyncResult)
// 0x000002D2 System.Void vp_GlobalCallbackReturn`2::.ctor(System.Object,System.IntPtr)
// 0x000002D3 R vp_GlobalCallbackReturn`2::Invoke(T)
// 0x000002D4 System.IAsyncResult vp_GlobalCallbackReturn`2::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000002D5 R vp_GlobalCallbackReturn`2::EndInvoke(System.IAsyncResult)
// 0x000002D6 System.Void vp_GlobalCallbackReturn`3::.ctor(System.Object,System.IntPtr)
// 0x000002D7 R vp_GlobalCallbackReturn`3::Invoke(T,U)
// 0x000002D8 System.IAsyncResult vp_GlobalCallbackReturn`3::BeginInvoke(T,U,System.AsyncCallback,System.Object)
// 0x000002D9 R vp_GlobalCallbackReturn`3::EndInvoke(System.IAsyncResult)
// 0x000002DA System.Void vp_GlobalCallbackReturn`4::.ctor(System.Object,System.IntPtr)
// 0x000002DB R vp_GlobalCallbackReturn`4::Invoke(T,U,V)
// 0x000002DC System.IAsyncResult vp_GlobalCallbackReturn`4::BeginInvoke(T,U,V,System.AsyncCallback,System.Object)
// 0x000002DD R vp_GlobalCallbackReturn`4::EndInvoke(System.IAsyncResult)
// 0x000002DE vp_GlobalEventInternal_UnregisterException vp_GlobalEventInternal::ShowUnregisterException(System.String)
extern void vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD ();
// 0x000002DF vp_GlobalEventInternal_SendException vp_GlobalEventInternal::ShowSendException(System.String)
extern void vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146 ();
// 0x000002E0 System.Void vp_GlobalEventInternal::.cctor()
extern void vp_GlobalEventInternal__cctor_m1B8C0006AF7A3634EC11278B4381162061FB1F81 ();
// 0x000002E1 System.Void vp_GlobalEvent::Register(System.String,vp_GlobalCallback)
extern void vp_GlobalEvent_Register_m18794A689E9DFE6B77F25016D958DA0CF3B25979 ();
// 0x000002E2 System.Void vp_GlobalEvent::Unregister(System.String,vp_GlobalCallback)
extern void vp_GlobalEvent_Unregister_mF3CE96C87744C19E2BDAD568C5C2394DEB6CB292 ();
// 0x000002E3 System.Void vp_GlobalEvent::Send(System.String)
extern void vp_GlobalEvent_Send_mB5B8C3AEED968C3EC07B5ACE2E75083EF5E32A68 ();
// 0x000002E4 System.Void vp_GlobalEvent::Send(System.String,vp_GlobalEventMode)
extern void vp_GlobalEvent_Send_mD5E8C75F156165C85A2A0BFD573B28B21CE637E8 ();
// 0x000002E5 System.Void vp_GlobalEvent::.cctor()
extern void vp_GlobalEvent__cctor_m084F833C446A30B2DAC8720820C6DEF0A738412C ();
// 0x000002E6 System.Void vp_GlobalEvent`1::Register(System.String,vp_GlobalCallback`1<T>)
// 0x000002E7 System.Void vp_GlobalEvent`1::Unregister(System.String,vp_GlobalCallback`1<T>)
// 0x000002E8 System.Void vp_GlobalEvent`1::Send(System.String,T)
// 0x000002E9 System.Void vp_GlobalEvent`1::Send(System.String,T,vp_GlobalEventMode)
// 0x000002EA System.Void vp_GlobalEvent`1::.cctor()
// 0x000002EB System.Void vp_GlobalEvent`2::Register(System.String,vp_GlobalCallback`2<T,U>)
// 0x000002EC System.Void vp_GlobalEvent`2::Unregister(System.String,vp_GlobalCallback`2<T,U>)
// 0x000002ED System.Void vp_GlobalEvent`2::Send(System.String,T,U)
// 0x000002EE System.Void vp_GlobalEvent`2::Send(System.String,T,U,vp_GlobalEventMode)
// 0x000002EF System.Void vp_GlobalEvent`2::.cctor()
// 0x000002F0 System.Void vp_GlobalEvent`3::Register(System.String,vp_GlobalCallback`3<T,U,V>)
// 0x000002F1 System.Void vp_GlobalEvent`3::Unregister(System.String,vp_GlobalCallback`3<T,U,V>)
// 0x000002F2 System.Void vp_GlobalEvent`3::Send(System.String,T,U,V)
// 0x000002F3 System.Void vp_GlobalEvent`3::Send(System.String,T,U,V,vp_GlobalEventMode)
// 0x000002F4 System.Void vp_GlobalEvent`3::.cctor()
// 0x000002F5 System.Void vp_GlobalEventReturn`1::Register(System.String,vp_GlobalCallbackReturn`1<R>)
// 0x000002F6 System.Void vp_GlobalEventReturn`1::Unregister(System.String,vp_GlobalCallbackReturn`1<R>)
// 0x000002F7 R vp_GlobalEventReturn`1::Send(System.String)
// 0x000002F8 R vp_GlobalEventReturn`1::Send(System.String,vp_GlobalEventMode)
// 0x000002F9 System.Void vp_GlobalEventReturn`1::.cctor()
// 0x000002FA System.Void vp_GlobalEventReturn`2::Register(System.String,vp_GlobalCallbackReturn`2<T,R>)
// 0x000002FB System.Void vp_GlobalEventReturn`2::Unregister(System.String,vp_GlobalCallbackReturn`2<T,R>)
// 0x000002FC R vp_GlobalEventReturn`2::Send(System.String,T)
// 0x000002FD R vp_GlobalEventReturn`2::Send(System.String,T,vp_GlobalEventMode)
// 0x000002FE System.Void vp_GlobalEventReturn`2::.cctor()
// 0x000002FF System.Void vp_GlobalEventReturn`3::Register(System.String,vp_GlobalCallbackReturn`3<T,U,R>)
// 0x00000300 System.Void vp_GlobalEventReturn`3::Unregister(System.String,vp_GlobalCallbackReturn`3<T,U,R>)
// 0x00000301 R vp_GlobalEventReturn`3::Send(System.String,T,U)
// 0x00000302 R vp_GlobalEventReturn`3::Send(System.String,T,U,vp_GlobalEventMode)
// 0x00000303 System.Void vp_GlobalEventReturn`3::.cctor()
// 0x00000304 System.Void vp_GlobalEventReturn`4::Register(System.String,vp_GlobalCallbackReturn`4<T,U,V,R>)
// 0x00000305 System.Void vp_GlobalEventReturn`4::Unregister(System.String,vp_GlobalCallbackReturn`4<T,U,V,R>)
// 0x00000306 R vp_GlobalEventReturn`4::Send(System.String,T,U,V)
// 0x00000307 R vp_GlobalEventReturn`4::Send(System.String,T,U,V,vp_GlobalEventMode)
// 0x00000308 System.Void vp_GlobalEventReturn`4::.cctor()
// 0x00000309 System.Void vp_Message::.ctor(System.String)
extern void vp_Message__ctor_m940941685CF5B3BC416C682A02E2B3B9FAE373D8 ();
// 0x0000030A System.Void vp_Message::InitFields()
extern void vp_Message_InitFields_m1CEDD9995A593C2344716725E4B278F6888DEC42 ();
// 0x0000030B System.Void vp_Message::Register(System.Object,System.String,System.Int32)
extern void vp_Message_Register_m5CBF6BB8FBA90640DE2004608E8EB9BB2C602E66 ();
// 0x0000030C System.Void vp_Message::Unregister(System.Object)
extern void vp_Message_Unregister_mD7641886B494A6B19A8A6BF8A71847404DAACC1D ();
// 0x0000030D System.Void vp_Message`1::.ctor(System.String)
// 0x0000030E System.Void vp_Message`1::InitFields()
// 0x0000030F System.Void vp_Message`1::Register(System.Object,System.String,System.Int32)
// 0x00000310 System.Void vp_Message`1::Unregister(System.Object)
// 0x00000311 System.Void vp_Message`2::.ctor(System.String)
// 0x00000312 System.Void vp_Message`2::InitFields()
// 0x00000313 System.Void vp_Message`2::Register(System.Object,System.String,System.Int32)
// 0x00000314 System.Void vp_Message`2::Unregister(System.Object)
// 0x00000315 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.Dictionary`2<System.String,System.Delegate>>> vp_TargetEventHandler::get_TargetDict()
extern void vp_TargetEventHandler_get_TargetDict_m9CE29A757BCB5A120FCB159FC908CCF2F815DA1D ();
// 0x00000316 System.Void vp_TargetEventHandler::Register(System.Object,System.String,System.Delegate,System.Int32)
extern void vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA ();
// 0x00000317 System.Void vp_TargetEventHandler::Unregister(System.Object,System.String,System.Delegate)
extern void vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC ();
// 0x00000318 System.Void vp_TargetEventHandler::Unregister(UnityEngine.Component)
extern void vp_TargetEventHandler_Unregister_m8455936D2115A5A13E469F8FFBF9BA879F2587E6 ();
// 0x00000319 System.Void vp_TargetEventHandler::Unregister(System.Int32,UnityEngine.Component)
extern void vp_TargetEventHandler_Unregister_mF4B3CDD0C6A342B4A9A7C9065726EA454AAF91FD ();
// 0x0000031A System.Void vp_TargetEventHandler::Unregister(System.Object,System.Int32,System.String,System.Delegate)
extern void vp_TargetEventHandler_Unregister_mED0A2341DE0F53D91987FAF0A99F4D8C30A1AACF ();
// 0x0000031B System.Void vp_TargetEventHandler::UnregisterAll()
extern void vp_TargetEventHandler_UnregisterAll_m255675AD648C47A425CF17D2B3F29326B8B49B03 ();
// 0x0000031C System.Delegate vp_TargetEventHandler::GetCallback(System.Object,System.String,System.Boolean,System.Int32,vp_TargetEventOptions)
extern void vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603 ();
// 0x0000031D System.Void vp_TargetEventHandler::OnNoReceiver(System.String,vp_TargetEventOptions)
extern void vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F ();
// 0x0000031E System.String vp_TargetEventHandler::Dump()
extern void vp_TargetEventHandler_Dump_m81C5615B9AAE7D7AEAAB587A163BB239429A3E11 ();
// 0x0000031F System.Void vp_TargetEventHandler::.cctor()
extern void vp_TargetEventHandler__cctor_m0190AEA27131B57C6CD02774ED5CBAA9D9023679 ();
// 0x00000320 System.Void vp_TargetEvent::Register(System.Object,System.String,System.Action)
extern void vp_TargetEvent_Register_mF601E2295D6DD4E3C877C6F6AACC5E1F3644BA3B ();
// 0x00000321 System.Void vp_TargetEvent::Unregister(System.Object,System.String,System.Action)
extern void vp_TargetEvent_Unregister_mC6189B616A3806D03B00D03E90B616BD01FAEDDA ();
// 0x00000322 System.Void vp_TargetEvent::Unregister(System.Object)
extern void vp_TargetEvent_Unregister_m5732856C8E935B78FFF383E4DBB212AFE8463B18 ();
// 0x00000323 System.Void vp_TargetEvent::Unregister(UnityEngine.Component)
extern void vp_TargetEvent_Unregister_mCD1BD34A01620CB41F71E758E482E8B3DDC44E3F ();
// 0x00000324 System.Void vp_TargetEvent::Send(System.Object,System.String,vp_TargetEventOptions)
extern void vp_TargetEvent_Send_m3C0C8C3E5A862D21D2D3B60D2E483AC62990A400 ();
// 0x00000325 System.Void vp_TargetEvent::SendUpwards(UnityEngine.Component,System.String,vp_TargetEventOptions)
extern void vp_TargetEvent_SendUpwards_m9A3E5E0BE42370BDEB05413505B4E0F3B4392112 ();
// 0x00000326 System.Void vp_TargetEvent`1::Register(System.Object,System.String,System.Action`1<T>)
// 0x00000327 System.Void vp_TargetEvent`1::Unregister(System.Object,System.String,System.Action`1<T>)
// 0x00000328 System.Void vp_TargetEvent`1::Unregister(System.Object)
// 0x00000329 System.Void vp_TargetEvent`1::Send(System.Object,System.String,T,vp_TargetEventOptions)
// 0x0000032A System.Void vp_TargetEvent`1::SendUpwards(UnityEngine.Component,System.String,T,vp_TargetEventOptions)
// 0x0000032B System.Void vp_TargetEvent`2::Register(System.Object,System.String,System.Action`2<T,U>)
// 0x0000032C System.Void vp_TargetEvent`2::Unregister(System.Object,System.String,System.Action`2<T,U>)
// 0x0000032D System.Void vp_TargetEvent`2::Unregister(System.Object)
// 0x0000032E System.Void vp_TargetEvent`2::Send(System.Object,System.String,T,U,vp_TargetEventOptions)
// 0x0000032F System.Void vp_TargetEvent`2::SendUpwards(UnityEngine.Component,System.String,T,U,vp_TargetEventOptions)
// 0x00000330 System.Void vp_TargetEvent`3::Register(System.Object,System.String,System.Action`3<T,U,V>)
// 0x00000331 System.Void vp_TargetEvent`3::Unregister(System.Object,System.String,System.Action`3<T,U,V>)
// 0x00000332 System.Void vp_TargetEvent`3::Unregister(System.Object)
// 0x00000333 System.Void vp_TargetEvent`3::Send(System.Object,System.String,T,U,V,vp_TargetEventOptions)
// 0x00000334 System.Void vp_TargetEvent`3::SendUpwards(UnityEngine.Component,System.String,T,U,V,vp_TargetEventOptions)
// 0x00000335 System.Void vp_TargetEventReturn`1::Register(System.Object,System.String,System.Func`1<R>)
// 0x00000336 System.Void vp_TargetEventReturn`1::Unregister(System.Object,System.String,System.Func`1<R>)
// 0x00000337 System.Void vp_TargetEventReturn`1::Unregister(System.Object)
// 0x00000338 System.Void vp_TargetEventReturn`1::Unregister(UnityEngine.Component)
// 0x00000339 R vp_TargetEventReturn`1::Send(System.Object,System.String,vp_TargetEventOptions)
// 0x0000033A R vp_TargetEventReturn`1::SendUpwards(UnityEngine.Component,System.String,vp_TargetEventOptions)
// 0x0000033B System.Void vp_TargetEventReturn`2::Register(System.Object,System.String,System.Func`2<T,R>)
// 0x0000033C System.Void vp_TargetEventReturn`2::Unregister(System.Object,System.String,System.Func`2<T,R>)
// 0x0000033D System.Void vp_TargetEventReturn`2::Unregister(System.Object)
// 0x0000033E R vp_TargetEventReturn`2::Send(System.Object,System.String,T,vp_TargetEventOptions)
// 0x0000033F R vp_TargetEventReturn`2::SendUpwards(UnityEngine.Component,System.String,T,vp_TargetEventOptions)
// 0x00000340 System.Void vp_TargetEventReturn`3::Register(System.Object,System.String,System.Func`3<T,U,R>)
// 0x00000341 System.Void vp_TargetEventReturn`3::Unregister(System.Object,System.String,System.Func`3<T,U,R>)
// 0x00000342 System.Void vp_TargetEventReturn`3::Unregister(System.Object)
// 0x00000343 R vp_TargetEventReturn`3::Send(System.Object,System.String,T,U,vp_TargetEventOptions)
// 0x00000344 R vp_TargetEventReturn`3::SendUpwards(UnityEngine.Component,System.String,T,U,vp_TargetEventOptions)
// 0x00000345 System.Void vp_TargetEventReturn`4::Register(System.Object,System.String,System.Func`4<T,U,V,R>)
// 0x00000346 System.Void vp_TargetEventReturn`4::Unregister(System.Object,System.String,System.Func`4<T,U,V,R>)
// 0x00000347 System.Void vp_TargetEventReturn`4::Unregister(System.Object)
// 0x00000348 R vp_TargetEventReturn`4::Send(System.Object,System.String,T,U,V,vp_TargetEventOptions)
// 0x00000349 R vp_TargetEventReturn`4::SendUpwards(UnityEngine.Component,System.String,T,U,V,vp_TargetEventOptions)
// 0x0000034A System.Reflection.FieldInfo[] vp_Value`1::get_Fields()
// 0x0000034B System.Void vp_Value`1::.ctor(System.String)
// 0x0000034C System.Void vp_Value`1::InitFields()
// 0x0000034D System.Void vp_Value`1::Register(System.Object,System.String,System.Int32)
// 0x0000034E System.Void vp_Value`1::Unregister(System.Object)
// 0x0000034F System.Void vp_AudioUtility::PlayRandomSound(UnityEngine.AudioSource,System.Collections.Generic.List`1<UnityEngine.AudioClip>,UnityEngine.Vector2)
extern void vp_AudioUtility_PlayRandomSound_m718D90E2F96884AE683DB5C6238BC081A688D679 ();
// 0x00000350 System.Void vp_AudioUtility::PlayRandomSound(UnityEngine.AudioSource,System.Collections.Generic.List`1<UnityEngine.AudioClip>)
extern void vp_AudioUtility_PlayRandomSound_mB4EBABD957F1B1A623B11BA162A9D5CF44544B85 ();
// 0x00000351 UnityEngine.Collider vp_Gizmo::get_Collider()
extern void vp_Gizmo_get_Collider_m50838D4AEE5208DA3558B7B2C7B2190A8E8C01AB ();
// 0x00000352 System.Void vp_Gizmo::OnDrawGizmos()
extern void vp_Gizmo_OnDrawGizmos_m72D9B85D54E4245CB6E0B18D33FE2FD88CDF2487 ();
// 0x00000353 System.Void vp_Gizmo::OnDrawGizmosSelected()
extern void vp_Gizmo_OnDrawGizmosSelected_m328B883A4ABA6BA3A4512DAF805FF08A2A652BD8 ();
// 0x00000354 System.Void vp_Gizmo::.ctor()
extern void vp_Gizmo__ctor_mEA37331D262875D90CBDE1425220C1C0B64F16CE ();
// 0x00000355 System.Single vp_MathUtility::NaNSafeFloat(System.Single,System.Single)
extern void vp_MathUtility_NaNSafeFloat_mC2E450334305656976CD7023307543F73B36CA1E ();
// 0x00000356 UnityEngine.Vector2 vp_MathUtility::NaNSafeVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern void vp_MathUtility_NaNSafeVector2_mB76FBE51DC33256052F1EF4DFC82B52B63D5C91A ();
// 0x00000357 UnityEngine.Vector3 vp_MathUtility::NaNSafeVector3(UnityEngine.Vector3,UnityEngine.Vector3)
extern void vp_MathUtility_NaNSafeVector3_mFDF04B17988F61F88472AA9D09966A19B57167C3 ();
// 0x00000358 UnityEngine.Quaternion vp_MathUtility::NaNSafeQuaternion(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void vp_MathUtility_NaNSafeQuaternion_m990106F4E6243B39B88331AB6B4A92A69A51C113 ();
// 0x00000359 UnityEngine.Vector3 vp_MathUtility::SnapToZero(UnityEngine.Vector3,System.Single)
extern void vp_MathUtility_SnapToZero_mB6888CB6174E67467052E7FF4293258F9056D75B ();
// 0x0000035A System.Single vp_MathUtility::SnapToZero(System.Single,System.Single)
extern void vp_MathUtility_SnapToZero_m636A89CA5826C9E460B24BC0384A656E77C85DBF ();
// 0x0000035B System.Single vp_MathUtility::ReduceDecimals(System.Single,System.Single)
extern void vp_MathUtility_ReduceDecimals_m73D14324760C853842688538EDD1B2D1BC199A5B ();
// 0x0000035C System.Boolean vp_MathUtility::IsOdd(System.Int32)
extern void vp_MathUtility_IsOdd_m1E4C718EDF739E54B2FE5EED375042F1099FFDFF ();
// 0x0000035D System.Boolean vp_MathUtility::IsUniform(UnityEngine.Vector3)
extern void vp_MathUtility_IsUniform_m9B9629499D7ECB547E33652E3CF8A8C2347086E0 ();
// 0x0000035E System.Boolean vp_MathUtility::IsUniform(UnityEngine.Vector3,System.Single)
extern void vp_MathUtility_IsUniform_m8AEC1B14193A1B72DF426E127305FF02C6B1E5DF ();
// 0x0000035F System.Single vp_MathUtility::Sinus(System.Single,System.Single,System.Single)
extern void vp_MathUtility_Sinus_m0C8B44D6C3263A2691776CE76ABFAC17F13E7DDF ();
// 0x00000360 System.Void vp_MathUtility::SetSeed(System.Int32)
extern void vp_MathUtility_SetSeed_mB8283134E10A3DDDC23BA7D2EF6ACE6DA4AB6FA8 ();
// 0x00000361 System.Int32 vp_MathUtility::SetSeed(UnityEngine.Vector3)
extern void vp_MathUtility_SetSeed_m09FB77883391E271D8D980AA3C0AE9D4E1A186C6 ();
// 0x00000362 UnityEngine.Vector2 vp_MathUtility::RadianToVector2(System.Single)
extern void vp_MathUtility_RadianToVector2_m993F3109FD44B127F7FBA812326AFE2242FC7DB0 ();
// 0x00000363 UnityEngine.Vector2 vp_MathUtility::DegreeToVector2(System.Single)
extern void vp_MathUtility_DegreeToVector2_m095F40861602E639E339051F9C4036B140B54223 ();
// 0x00000364 vp_PoolManager vp_PoolManager::get_Instance()
extern void vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076 ();
// 0x00000365 System.Void vp_PoolManager::Awake()
extern void vp_PoolManager_Awake_m7D8D67DA29554A79E318327316CF971A843E9932 ();
// 0x00000366 System.Void vp_PoolManager::Start()
extern void vp_PoolManager_Start_m09371DAA63223410A778DD3B460D030E80FB339A ();
// 0x00000367 System.Void vp_PoolManager::AddToPool(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern void vp_PoolManager_AddToPool_m8195887F0F99262373A1B9D0E91C349D42C120D8 ();
// 0x00000368 UnityEngine.GameObject vp_PoolManager::Spawn(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void vp_PoolManager_Spawn_mCC73C86FEAD328B569F91955193CD4FB0299867B ();
// 0x00000369 UnityEngine.GameObject vp_PoolManager::SpawnInternal(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void vp_PoolManager_SpawnInternal_m9E77BCA2021A7EAB46CCB03ECCF513E2D11838DE ();
// 0x0000036A System.Void vp_PoolManager::Despawn(UnityEngine.GameObject,System.Single)
extern void vp_PoolManager_Despawn_m9870CB2ADCBE296E93D5EF5D220359A2FD2FCE37 ();
// 0x0000036B System.Void vp_PoolManager::Despawn(UnityEngine.GameObject)
extern void vp_PoolManager_Despawn_mC1BC1EA7AD2EDDA5739DF4AD58ED8050FAC4E5DF ();
// 0x0000036C System.Void vp_PoolManager::DespawnInternal(UnityEngine.GameObject)
extern void vp_PoolManager_DespawnInternal_mBB770EAFF2D1C835704B87A261330296F185EB88 ();
// 0x0000036D System.Void vp_PoolManager::AddIgnoredPrefab(UnityEngine.GameObject)
extern void vp_PoolManager_AddIgnoredPrefab_m9535A7B6DDB8C1E1DDC96DC93633FCADE2371A67 ();
// 0x0000036E System.String vp_PoolManager::GetUniqueNameOf(UnityEngine.GameObject)
extern void vp_PoolManager_GetUniqueNameOf_m05FB19A94767E15279CB27ABD8C83001530D4215 ();
// 0x0000036F System.Void vp_PoolManager::.ctor()
extern void vp_PoolManager__ctor_m760A92216A6665930C39F5BD09931EE6D4791D7B ();
// 0x00000370 System.Void vp_PoolManager::.cctor()
extern void vp_PoolManager__cctor_m7EFD7CD996A1E3C88C5D4E1D4496CC7655BE9BC5 ();
// 0x00000371 System.Boolean vp_Timer::get_WasAddedCorrectly()
extern void vp_Timer_get_WasAddedCorrectly_mCB27C1BD5AC27B6A186B8FA5980F4A321CE8AA77 ();
// 0x00000372 System.Void vp_Timer::Awake()
extern void vp_Timer_Awake_m734E356C6199AC96E3802473310E601640AC9F7B ();
// 0x00000373 System.Void vp_Timer::OnEnable()
extern void vp_Timer_OnEnable_mBED9298A21C89E42C7EF8FB3B534C14449A10E6D ();
// 0x00000374 System.Void vp_Timer::OnDisable()
extern void vp_Timer_OnDisable_m0154E0C36FCCF3FECACC1AEB9B5EFB4F65C701EE ();
// 0x00000375 System.Void vp_Timer::Update()
extern void vp_Timer_Update_mCADAC09F16FA853E322CCBAF10445FFB37EA654C ();
// 0x00000376 System.Void vp_Timer::In(System.Single,vp_Timer_Callback,vp_Timer_Handle)
extern void vp_Timer_In_m6C38B0240730B0B7637A6ABE382178AD62AE600D ();
// 0x00000377 System.Void vp_Timer::In(System.Single,vp_Timer_Callback,System.Int32,vp_Timer_Handle)
extern void vp_Timer_In_mB9BADD2F31DAFDE8AC00B9DB14F0B3FE0A079C86 ();
// 0x00000378 System.Void vp_Timer::In(System.Single,vp_Timer_Callback,System.Int32,System.Single,vp_Timer_Handle)
extern void vp_Timer_In_m45D94814124056CCA4F15DB39851538AA8C56FA5 ();
// 0x00000379 System.Void vp_Timer::In(System.Single,vp_Timer_ArgCallback,System.Object,vp_Timer_Handle)
extern void vp_Timer_In_m7B539EDFB4CAC928F5B3BA918334A91A48B653A0 ();
// 0x0000037A System.Void vp_Timer::In(System.Single,vp_Timer_ArgCallback,System.Object,System.Int32,vp_Timer_Handle)
extern void vp_Timer_In_m1CE790478236A71309DBD4E95E6942C1C569DC3B ();
// 0x0000037B System.Void vp_Timer::In(System.Single,vp_Timer_ArgCallback,System.Object,System.Int32,System.Single,vp_Timer_Handle)
extern void vp_Timer_In_mEF0C1B2CF80800DB8A069329E795AABCAD88F132 ();
// 0x0000037C System.Void vp_Timer::Start(vp_Timer_Handle)
extern void vp_Timer_Start_mB29B8806258029B318D347D4737073C30381CC94 ();
// 0x0000037D System.Void vp_Timer::Schedule(System.Single,vp_Timer_Callback,vp_Timer_ArgCallback,System.Object,vp_Timer_Handle,System.Int32,System.Single)
extern void vp_Timer_Schedule_mFA0E037BA960CF5A56850CB830E47D66E104DD51 ();
// 0x0000037E System.Void vp_Timer::Cancel(vp_Timer_Handle)
extern void vp_Timer_Cancel_mF17D30C62DBCF46555779B803D5F8098120BD3EB ();
// 0x0000037F System.Void vp_Timer::CancelAll()
extern void vp_Timer_CancelAll_m3D5F986AC5424082974E9D063C01E2520A59A1BC ();
// 0x00000380 System.Void vp_Timer::CancelAll(System.String)
extern void vp_Timer_CancelAll_m303B8675E314A56E8C8CA289D722CAF0C859D9E4 ();
// 0x00000381 System.Void vp_Timer::DestroyAll()
extern void vp_Timer_DestroyAll_mC5F253CF9832EB2F7791460CE29F36523565C1C5 ();
// 0x00000382 System.Void vp_Timer::OnLevelLoad(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void vp_Timer_OnLevelLoad_m8AEC3C9B0E4D301F84509FD6E721088729533D1B ();
// 0x00000383 vp_Timer_Stats vp_Timer::EditorGetStats()
extern void vp_Timer_EditorGetStats_m28F9B475F1EC5A417A92F9577EFE5D168B523A1A ();
// 0x00000384 System.String vp_Timer::EditorGetMethodInfo(System.Int32)
extern void vp_Timer_EditorGetMethodInfo_m9E7A3690DFB13744FA2EC27383327956B3772A09 ();
// 0x00000385 System.Int32 vp_Timer::EditorGetMethodId(System.Int32)
extern void vp_Timer_EditorGetMethodId_m6DF107E430DF2469FBF032C47E80F59E43A55D1C ();
// 0x00000386 System.Void vp_Timer::OnApplicationQuit()
extern void vp_Timer_OnApplicationQuit_m6DE892CB30A7EADE81EEB772E4200981EE336A78 ();
// 0x00000387 System.Void vp_Timer::.ctor()
extern void vp_Timer__ctor_mF24734A1699F33BA69395AD871501D198E510E9B ();
// 0x00000388 System.Void vp_Timer::.cctor()
extern void vp_Timer__cctor_m5061568939E4753BAF3FEC384682A0EFC1C20D2B ();
// 0x00000389 System.Single vp_TimeUtility::get_TimeScale()
extern void vp_TimeUtility_get_TimeScale_mF03190B70748141A530A01FA8854E71C73F9F861 ();
// 0x0000038A System.Void vp_TimeUtility::set_TimeScale(System.Single)
extern void vp_TimeUtility_set_TimeScale_mB4E1922238BEAB40F01F686915C79F22A8014BA3 ();
// 0x0000038B System.Single vp_TimeUtility::get_AdjustedTimeScale()
extern void vp_TimeUtility_get_AdjustedTimeScale_m7D08D72631625292E84165D84B110EB71F63E834 ();
// 0x0000038C System.Void vp_TimeUtility::FadeTimeScale(System.Single,System.Single)
extern void vp_TimeUtility_FadeTimeScale_m1A76222655DB398DF7454ACAAF7CA42ED7B01C4D ();
// 0x0000038D System.Single vp_TimeUtility::ClampTimeScale(System.Single)
extern void vp_TimeUtility_ClampTimeScale_mF25E1E3C8A3E3FBA4A7527C258B1C05D74BA11F0 ();
// 0x0000038E System.Boolean vp_TimeUtility::get_Paused()
extern void vp_TimeUtility_get_Paused_m7D499A7C137BE65A65EF3B04752A5718920782E2 ();
// 0x0000038F System.Void vp_TimeUtility::set_Paused(System.Boolean)
extern void vp_TimeUtility_set_Paused_m201DFD8830D9C4E1B618090EA20C3F350138BD41 ();
// 0x00000390 System.Void vp_TimeUtility::.cctor()
extern void vp_TimeUtility__cctor_mC36EE9FFDF64B061EE1B5989A6AA96181520CC3C ();
// 0x00000391 System.String vp_Utility::GetErrorLocation(System.Int32,System.Boolean)
extern void vp_Utility_GetErrorLocation_m6F3429DFF080043CBB6719F998CEDF3D76E631B9 ();
// 0x00000392 System.String vp_Utility::GetTypeAlias(System.Type)
extern void vp_Utility_GetTypeAlias_m0B105E1F6666B82F789B1ED67B46ED98EED4832B ();
// 0x00000393 System.Void vp_Utility::Activate(UnityEngine.GameObject,System.Boolean)
extern void vp_Utility_Activate_m1D822960AA0CE9BC101420C0E0C683D92A0F91F4 ();
// 0x00000394 System.Boolean vp_Utility::IsActive(UnityEngine.GameObject)
extern void vp_Utility_IsActive_m774BACB2A81DCC0F782DFE3A6783917DF9EE2BC5 ();
// 0x00000395 System.Boolean vp_Utility::get_LockCursor()
extern void vp_Utility_get_LockCursor_m856042AC6591FFF4A2D95322877B06E00C612030 ();
// 0x00000396 System.Void vp_Utility::set_LockCursor(System.Boolean)
extern void vp_Utility_set_LockCursor_mFB089E7E19E8FE41EADE38F7D5C743B7CCC97ADF ();
// 0x00000397 System.Void vp_Utility::RandomizeList(System.Collections.Generic.List`1<T>)
// 0x00000398 T vp_Utility::RandomObject(System.Collections.Generic.List`1<T>)
// 0x00000399 System.Collections.Generic.List`1<T> vp_Utility::ChildComponentsToList(UnityEngine.Transform)
// 0x0000039A System.Boolean vp_Utility::IsDescendant(UnityEngine.Transform,UnityEngine.Transform)
extern void vp_Utility_IsDescendant_m2383C0A44C9B19957CF9C867D00DF32E10CEEDAD ();
// 0x0000039B UnityEngine.Component vp_Utility::GetParent(UnityEngine.Component)
extern void vp_Utility_GetParent_m23DF7AA74CBF3783F4D11A3C8159BCFD32C629C3 ();
// 0x0000039C UnityEngine.Transform vp_Utility::GetTransformByNameInChildren(UnityEngine.Transform,System.String,System.Boolean,System.Boolean)
extern void vp_Utility_GetTransformByNameInChildren_m4BAEFE6AB1211944BBC646B73A3C319EA810BE8A ();
// 0x0000039D UnityEngine.Transform vp_Utility::GetTransformByNameInAncestors(UnityEngine.Transform,System.String,System.Boolean,System.Boolean)
extern void vp_Utility_GetTransformByNameInAncestors_m85889B83D8D4B16EFBB1C417E7964480C825AEE4 ();
// 0x0000039E UnityEngine.Object vp_Utility::Instantiate(UnityEngine.Object)
extern void vp_Utility_Instantiate_m40BDAB73E95DD2EA41F4C96D424CFE9E08B4E30E ();
// 0x0000039F UnityEngine.Object vp_Utility::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void vp_Utility_Instantiate_mB3FC1BB39D06ACF32E0409CAA385DDB0915B65E7 ();
// 0x000003A0 System.Void vp_Utility::Destroy(UnityEngine.Object)
extern void vp_Utility_Destroy_m4AE89615D99B095E1DAAF80E913F5AA6B9C4539F ();
// 0x000003A1 System.Void vp_Utility::Destroy(UnityEngine.Object,System.Single)
extern void vp_Utility_Destroy_mEF5255E8C4A8FA62DC4D9C33A1FB6F48B7B22B27 ();
// 0x000003A2 System.Int32 vp_Utility::get_UniqueID()
extern void vp_Utility_get_UniqueID_mAE63167533A8BA058CFD0BFE90DF746219A3384F ();
// 0x000003A3 System.Void vp_Utility::ClearUniqueIDs()
extern void vp_Utility_ClearUniqueIDs_m559A9F7D03CF00423358623E578D00A918B328DC ();
// 0x000003A4 System.Int32 vp_Utility::PositionToID(UnityEngine.Vector3)
extern void vp_Utility_PositionToID_m872F990B5C216FA657F53092D380CE04F341467A ();
// 0x000003A5 System.Void vp_Utility::PlayRandomSound(UnityEngine.AudioSource,System.Collections.Generic.List`1<UnityEngine.AudioClip>,UnityEngine.Vector2)
extern void vp_Utility_PlayRandomSound_mD2386BAC0DBFDC710BD1D921264DF06C8002BB52 ();
// 0x000003A6 System.Void vp_Utility::PlayRandomSound(UnityEngine.AudioSource,System.Collections.Generic.List`1<UnityEngine.AudioClip>)
extern void vp_Utility_PlayRandomSound_m963E0566C1B780DA5B3025C846FE02239E98AC2B ();
// 0x000003A7 UnityEngine.Sprite vp_Utility::GetSprite(UnityEngine.Texture2D)
extern void vp_Utility_GetSprite_m269DD9BE9682187F7237CA44522DB667D6373F69 ();
// 0x000003A8 System.Void vp_Utility::.cctor()
extern void vp_Utility__cctor_m9B1303501DD378DBFA457D8B8A5C3F0F3AE0E5B5 ();
// 0x000003A9 System.Void vp_WaypointGizmo::OnDrawGizmos()
extern void vp_WaypointGizmo_OnDrawGizmos_m9CF12CE032519F0999581DCA4C6755F0ACA65028 ();
// 0x000003AA System.Void vp_WaypointGizmo::OnDrawGizmosSelected()
extern void vp_WaypointGizmo_OnDrawGizmosSelected_m5424ACE12543A130DF01A80B4079580B9C37A740 ();
// 0x000003AB System.Void vp_WaypointGizmo::.ctor()
extern void vp_WaypointGizmo__ctor_m6C7A345F5F27B690591D1B7EF78FD13769B8A25B ();
// 0x000003AC System.Void UIEventsUtility::AddEvent(UnityEngine.EventSystems.EventTrigger,UnityEngine.EventSystems.EventTriggerType,System.Action`1<UnityEngine.EventSystems.PointerEventData>)
extern void UIEventsUtility_AddEvent_mC95ABDE52F1129FEE122C3411C64E804121CA6E0 ();
// 0x000003AD UnityEngine.Vector2 Splitter::GetRange(System.Int32)
extern void Splitter_GetRange_m72C3859C7FC21EEBAEACE317B549A496023C48D7 ();
// 0x000003AE System.Single Splitter::GetLength(System.Int32)
extern void Splitter_GetLength_m36B893414D7C5C147779E594A1DEBC2D3337A926 ();
// 0x000003AF System.Int32 Splitter::GetIndexOfChance(System.Single)
extern void Splitter_GetIndexOfChance_mE527221462A0EEAD95827B03679E0FE5C25877F3 ();
// 0x000003B0 System.Void Splitter::UpdatePointValue(System.Int32,System.Single)
extern void Splitter_UpdatePointValue_mD0804DDDB95C9221D8E4355CE601B8406FDFFA67 ();
// 0x000003B1 System.Boolean Splitter::IndexOnRange(System.Int32)
extern void Splitter_IndexOnRange_m03EDB3D8142EE8FFBEA70EDE558C3830B3D136C5 ();
// 0x000003B2 System.Void Splitter::UpdateSize(System.Int32)
// 0x000003B3 System.Void Splitter::.ctor()
extern void Splitter__ctor_mFA7A1B4FAE29B30C71E7FB447DB1187F9E57654D ();
// 0x000003B4 T ValuesSplitter`1::GetValueOfChance(System.Single)
// 0x000003B5 T ValuesSplitter`1::GetRandomValue()
// 0x000003B6 System.String ValuesSplitter`1::get_SplitterValues()
// 0x000003B7 System.Void ValuesSplitter`1::set_SplitterValues(System.String)
// 0x000003B8 System.Void ValuesSplitter`1::UpdateSize(System.Int32)
// 0x000003B9 System.Void ValuesSplitter`1::.ctor()
// 0x000003BA System.Void ValueIntSplitter::.ctor()
extern void ValueIntSplitter__ctor_m74D45F149EA4AA417FB7988792415E4E21FFC33D ();
// 0x000003BB System.Void ValuesColorSplitter::.ctor()
extern void ValuesColorSplitter__ctor_mF1CAF44414449EF470EE35F74E288A15863657D2 ();
// 0x000003BC System.Void ValuesStringSplitter::.ctor()
extern void ValuesStringSplitter__ctor_m2670D0AEE133BD7CF5621C07FE6751AAD0B85F14 ();
// 0x000003BD System.Void AndroidVibrate::Play(VibrateType)
extern void AndroidVibrate_Play_m4C3192115457819789396F9217482D22007AD27B ();
// 0x000003BE System.Void AndroidVibrate::Vibrate()
extern void AndroidVibrate_Vibrate_m443F3B6730BB9CA30E423538C94268C959897C63 ();
// 0x000003BF System.Void AndroidVibrate::Vibrate(System.Int64)
extern void AndroidVibrate_Vibrate_m470978482D0207E81589CACF8DE3377BE2E5B99C ();
// 0x000003C0 System.Void AndroidVibrate::Vibrate(System.Int64[],System.Int32)
extern void AndroidVibrate_Vibrate_m1F614D0C0E1BB1A94A043991FC6C61CD4F937DFC ();
// 0x000003C1 System.Void AndroidVibrate::Cancel()
extern void AndroidVibrate_Cancel_mF05CC89E2E9CC91C14D2D165BDADC7AD14ED6D53 ();
// 0x000003C2 System.Void Vibrate::Play(VibrateType)
extern void Vibrate_Play_m278053BB1618451EEEB0FFE9D1F8B9C74DBF23C5 ();
// 0x000003C3 System.Boolean Vibrate::get_Vibro()
extern void Vibrate_get_Vibro_m3E1731591C30819C69C668B7389DC73064035566 ();
// 0x000003C4 System.Void Vibrate::set_Vibro(System.Boolean)
extern void Vibrate_set_Vibro_m59B5C889697523DF195C5E4609F33623939D3378 ();
// 0x000003C5 System.Void VibrateButton::Play()
extern void VibrateButton_Play_m80475E24A84451B2F85BBFDB7608FC0CD8413763 ();
// 0x000003C6 System.Void VibrateButton::.ctor()
extern void VibrateButton__ctor_mA73649857FDEEC68D9023F36103DE921BB1E99D9 ();
// 0x000003C7 System.Void VibroButtonController::Awake()
extern void VibroButtonController_Awake_m35BF5CAEEC17D16BCD6952B0789103E5FEF18EB6 ();
// 0x000003C8 System.Void VibroButtonController::Click()
extern void VibroButtonController_Click_m61BE639058E8D9B897F5444460A3C8BD7C73A22F ();
// 0x000003C9 System.Void VibroButtonController::CheckImage()
extern void VibroButtonController_CheckImage_m2B1335F9903891166523BF6F1349B85A78BDDF11 ();
// 0x000003CA System.Void VibroButtonController::.ctor()
extern void VibroButtonController__ctor_m761F78EEE9B47774A0CBD790A679527D8B96808E ();
// 0x000003CB System.Void AmbientSwitcher::Prepare(UnityEngine.Color,UnityEngine.Color)
extern void AmbientSwitcher_Prepare_mFE7BF5473BA926732833B40D5F3755602FF6ACA8 ();
// 0x000003CC System.Void AmbientSwitcher::SetVisual(System.Single)
extern void AmbientSwitcher_SetVisual_mCD45EB5AFF872F31E5432FAA358B6FD1B796C442 ();
// 0x000003CD System.Void AmbientSwitcher::.ctor()
extern void AmbientSwitcher__ctor_m13FB52A42608898047241091D0AC039C7598B2AE ();
// 0x000003CE System.Void DirectionLightSwitcher::Prepare(DirectionLightPreset,DirectionLightPreset)
extern void DirectionLightSwitcher_Prepare_m82A92E9046E295225971D4000C1DBA3FB53D5491 ();
// 0x000003CF System.Void DirectionLightSwitcher::SetVisual(System.Single)
extern void DirectionLightSwitcher_SetVisual_m051F1DC388F7E1C2CC42681A2ECDFAAF26EF9522 ();
// 0x000003D0 System.Void DirectionLightSwitcher::.ctor()
extern void DirectionLightSwitcher__ctor_mC82786F6B8C0BB77E420CEC1BAED8ADA8BCB3386 ();
// 0x000003D1 System.Void FlashLampSwitcher::AddLight(FlashLightController)
extern void FlashLampSwitcher_AddLight_m5CA0AAA357CF73ABCB398875ADAC385F0DBBDE22 ();
// 0x000003D2 System.Void FlashLampSwitcher::RemoveLight(FlashLightController)
extern void FlashLampSwitcher_RemoveLight_m903A342D975B639E0EBB3AB4C054424D80DCA785 ();
// 0x000003D3 System.Void FlashLampSwitcher::Prepare(TimeState,TimeState)
extern void FlashLampSwitcher_Prepare_m6DFC0AC90B2994F20E64E81C66742D9C1B509817 ();
// 0x000003D4 System.Void FlashLampSwitcher::SetVisual(System.Single)
extern void FlashLampSwitcher_SetVisual_mC5D3C0255EDE672B6104A2BFAB151BB2BA615E78 ();
// 0x000003D5 System.Void FlashLampSwitcher::SetActiveLight(System.Boolean,System.Single)
extern void FlashLampSwitcher_SetActiveLight_m326F0D5B8E8FF0FB2A9E1C28965B42C890FBE51A ();
// 0x000003D6 System.Void FlashLampSwitcher::SetIntensityLight(System.Single)
extern void FlashLampSwitcher_SetIntensityLight_m4EA148B7B7D80192B7A675E0E50E43A245769AC7 ();
// 0x000003D7 System.Boolean FlashLampSwitcher::IsEnableLight(TimeState)
extern void FlashLampSwitcher_IsEnableLight_m6A2F95A19D9D306E4B1B12BCF15096CB6B0A8F0B ();
// 0x000003D8 System.Void FlashLampSwitcher::.ctor()
extern void FlashLampSwitcher__ctor_mB36B5B49B82E87B5B80C9DF9E1FF7287D3F7BDF9 ();
// 0x000003D9 System.Void FlashLightController::OnEnable()
extern void FlashLightController_OnEnable_m1C073FC5FA19C6BFE18700D0740E474861CA8ABA ();
// 0x000003DA System.Void FlashLightController::OnDisable()
extern void FlashLightController_OnDisable_m867C796A0982128770CF0D34C73194C75C153EE6 ();
// 0x000003DB System.Void FlashLightController::SetState(System.Boolean)
extern void FlashLightController_SetState_m071A94625E272BE17708000833CC0324C0DADAA9 ();
// 0x000003DC System.Void FlashLightController::SetIntensity(System.Single)
extern void FlashLightController_SetIntensity_mBBD91DC8F93FE270EBB2D26739BCF68418132609 ();
// 0x000003DD System.Void FlashLightController::.ctor()
extern void FlashLightController__ctor_mF1C2CFD7B49F8C04CB67B25C927010FEF5067638 ();
// 0x000003DE System.Void MaterialsSwitcher::Prepare(MaterialPreset,MaterialPreset)
extern void MaterialsSwitcher_Prepare_m06CD8EA3CD81ED54C619CA0B3E19E5F13C148013 ();
// 0x000003DF System.Void MaterialsSwitcher::SetVisual(System.Single)
extern void MaterialsSwitcher_SetVisual_mBA045F788590786ABAC71971580F5F9C914D0565 ();
// 0x000003E0 System.Void MaterialsSwitcher::SetColor(System.String,UnityEngine.Color)
extern void MaterialsSwitcher_SetColor_mA2640A2CB47AA558AC719252745039A498D0CA2A ();
// 0x000003E1 System.Void MaterialsSwitcher::SetFloat(System.String,System.Single)
extern void MaterialsSwitcher_SetFloat_m694379818804B98436772C950908288FB32A9733 ();
// 0x000003E2 MaterialColor[] MaterialsSwitcher::GetColorFields(System.String[])
extern void MaterialsSwitcher_GetColorFields_m6620977D6EE7DBC5A29B6D69EAFD01F99B8E8F01 ();
// 0x000003E3 MaterialFloat[] MaterialsSwitcher::GetFloatFields(System.String[])
extern void MaterialsSwitcher_GetFloatFields_mC136C374AFDCEBEAEB4352D5DC741A26D0DE309F ();
// 0x000003E4 System.Void MaterialsSwitcher::.ctor()
extern void MaterialsSwitcher__ctor_m6D1B82953B6C7529C9F2851DCD537DED6F39EDA8 ();
// 0x000003E5 System.Void MaterialColor::.ctor(System.String,UnityEngine.Color)
extern void MaterialColor__ctor_m9C0D28AFB2F3E7C1F051F2C637A1685998EBB4B6_AdjustorThunk ();
// 0x000003E6 System.Void MaterialFloat::.ctor(System.String,System.Single)
extern void MaterialFloat__ctor_m4F5C48D61FF899A376B92A499A661F73C43B7B18_AdjustorThunk ();
// 0x000003E7 System.Void VisualProvider::Prepare(Palette)
extern void VisualProvider_Prepare_m4E0A1E3F83A4F3B360F9298D72A0D4BFCFDEAECF ();
// 0x000003E8 System.Void VisualProvider::SetVisual(System.Single)
// 0x000003E9 System.Void VisualProvider::SaveToPalette()
// 0x000003EA System.Void VisualProvider::.ctor()
extern void VisualProvider__ctor_m24BA09E0D18B5C1951359FFEDEF09A562F461B30 ();
// 0x000003EB System.Void VisualSetter::Awake()
extern void VisualSetter_Awake_m9518B0754CBD18526C9D0884B740AADD356F593E ();
// 0x000003EC System.Void VisualSetter::Start()
extern void VisualSetter_Start_m910DF5AD57D53B7F729625B73F0F369367638B10 ();
// 0x000003ED System.Void VisualSetter::Next()
extern void VisualSetter_Next_m332E8078B15C299B86E1C54C0C4A322F099B3EB1 ();
// 0x000003EE System.Void VisualSetter::NextVisual()
extern void VisualSetter_NextVisual_m402EA6DAA7836D0B7A5179B6E8C91FC999F8CEEB ();
// 0x000003EF System.Void VisualSetter::UpdateCurrentPalette()
extern void VisualSetter_UpdateCurrentPalette_mCE0468F15EC6BAA58055896C115D15DFBB475BC1 ();
// 0x000003F0 System.Void VisualSetter::UpdateVisual(Palette)
extern void VisualSetter_UpdateVisual_mF6ABF28C7573C17FE6EB0E5C003EE96629BA9EFE ();
// 0x000003F1 System.Void VisualSetter::UpdateVisual(System.Int32)
extern void VisualSetter_UpdateVisual_mB7F7C47D20D4C3612644D1F336D66A3C3A9D29D3 ();
// 0x000003F2 System.Void VisualSetter::PrepareVisual(System.Int32)
extern void VisualSetter_PrepareVisual_m5A8770D3643B1511B9A7826D269651E259B3BD8D ();
// 0x000003F3 System.Void VisualSetter::SetVisual(System.Single)
extern void VisualSetter_SetVisual_m4588932365DBC0E97EF3F3E45FA2062EDC6F9616 ();
// 0x000003F4 System.Collections.IEnumerator VisualSetter::SwithcVisual()
extern void VisualSetter_SwithcVisual_mC4DC7F2C5F09D3866CBEC43159393A36BB17BBDA ();
// 0x000003F5 System.Void VisualSetter::.ctor()
extern void VisualSetter__ctor_m91DABE3B976CC8F5DE594815C326247615854973 ();
// 0x000003F6 System.Void Palette::.ctor()
extern void Palette__ctor_m42E818B0D090CAE72F647EF026DDD934EE1D91E5 ();
// 0x000003F7 UnityEngine.WWW iOSMusic::get_MusicLoader()
extern void iOSMusic_get_MusicLoader_m8B10885E281BDA7ADD9792969558D8F308BFECF4 ();
// 0x000003F8 System.Void iOSMusic::set_MusicLoader(UnityEngine.WWW)
extern void iOSMusic_set_MusicLoader_m19295714103C9930B7CEE0EAAD1A63897201FC34 ();
// 0x000003F9 UnityEngine.AudioSource iOSMusic::get_iOSMusicAudioSource()
extern void iOSMusic_get_iOSMusicAudioSource_m9F49589F3FFCB0429696F7F7B2483FDC1D5673BE ();
// 0x000003FA System.Void iOSMusic::set_iOSMusicAudioSource(UnityEngine.AudioSource)
extern void iOSMusic_set_iOSMusicAudioSource_m993D0656B954CAA02AED952A14744CBB714E2DA1 ();
// 0x000003FB UnityEngine.AudioClip iOSMusic::get_iOSMusicClip()
extern void iOSMusic_get_iOSMusicClip_m39A590849F0B06FA6DFEBCC2018AEA548F90D827 ();
// 0x000003FC System.Void iOSMusic::set_iOSMusicClip(UnityEngine.AudioClip)
extern void iOSMusic_set_iOSMusicClip_m31C1A83A7E093B8A4DECD778AA26BB4D530DAA9C ();
// 0x000003FD System.Boolean iOSMusic::get_HasAudioClipStartedPlaying()
extern void iOSMusic_get_HasAudioClipStartedPlaying_mCACEF6B9C3483C85E7B271C6C85734B910A33C0C ();
// 0x000003FE System.Void iOSMusic::set_HasAudioClipStartedPlaying(System.Boolean)
extern void iOSMusic_set_HasAudioClipStartedPlaying_m97B195A060373D26B7A18649D5F6A0B64B60F1F6 ();
// 0x000003FF System.Boolean iOSMusic::get_IsAudioClipPaused()
extern void iOSMusic_get_IsAudioClipPaused_m639A9EA784B1C2BCCE6F9636B9D18C8948BEEB4C ();
// 0x00000400 System.Void iOSMusic::set_IsAudioClipPaused(System.Boolean)
extern void iOSMusic_set_IsAudioClipPaused_m2F61FA5DF748F7CEB7AE3480D50D2D21930324C3 ();
// 0x00000401 System.Boolean iOSMusic::get_ShouldAppendToPlaylist()
extern void iOSMusic_get_ShouldAppendToPlaylist_m6EC1216BE54ED3BC89F7B95E15EE6C96067D26AD ();
// 0x00000402 System.Void iOSMusic::set_ShouldAppendToPlaylist(System.Boolean)
extern void iOSMusic_set_ShouldAppendToPlaylist_mBEE9914912C2FE1DBCE54800D91F437A0841686E ();
// 0x00000403 System.Void iOSMusic::Awake()
extern void iOSMusic_Awake_mDAEB3E61B7036F6BB08DA7C7D3F132E2F1FFD68F ();
// 0x00000404 System.Void iOSMusic::Start()
extern void iOSMusic_Start_m597F0BD7435A8768FDD1BE8A3A3EE479E8230E7B ();
// 0x00000405 System.Void iOSMusic::Update()
extern void iOSMusic_Update_mE0A629BFAA7DDD24B5B5B6ADF3C0917187AC0DD1 ();
// 0x00000406 System.Void iOSMusic::CheckAudioSourcePlayback()
extern void iOSMusic_CheckAudioSourcePlayback_m9C63AF1C24663247FA6FDFF2E36512462A912016 ();
// 0x00000407 System.Void iOSMusic::PauseAudio()
extern void iOSMusic_PauseAudio_m1DD9659BE72D033A169C443CD18C3024CA468D8D ();
// 0x00000408 System.Void iOSMusic::UnPauseAudio()
extern void iOSMusic_UnPauseAudio_m7ABBC1C0ADBA12002250D1E4A480301B8D192F7B ();
// 0x00000409 System.Void iOSMusic::HandleAppendToggleChange(System.Boolean)
extern void iOSMusic_HandleAppendToggleChange_mAC20656B9EE6323CE152686679560D56940CBE6C ();
// 0x0000040A System.Void iOSMusic::ResetButtonStates()
extern void iOSMusic_ResetButtonStates_mF34677F66D9434189A3F97A04B8CE1EEF46DFBC6 ();
// 0x0000040B System.Void iOSMusic::LoadAudioClip()
extern void iOSMusic_LoadAudioClip_m89B77832AFD6B18D8EB6C49D5CEBEE7E4FD57265 ();
// 0x0000040C System.Collections.IEnumerator iOSMusic::LoadMusic(System.String)
extern void iOSMusic_LoadMusic_m69A96886113ABDDBA2628C5B5CE59C6633B99CC6 ();
// 0x0000040D System.Void iOSMusic::ExtractTitle(System.String)
extern void iOSMusic_ExtractTitle_m155852802CF14CDAB5F3B09D79FFE30C26718B42 ();
// 0x0000040E System.Void iOSMusic::ExtractArtist(System.String)
extern void iOSMusic_ExtractArtist_m1D405DF45B0FAD108A2F4F9B3B9ECBEB5E1266C9 ();
// 0x0000040F System.Void iOSMusic::ExtractAlbumTitle(System.String)
extern void iOSMusic_ExtractAlbumTitle_m403F8AFED5A02CABEA2D432E1956875335416223 ();
// 0x00000410 System.Void iOSMusic::ExtractBPM(System.String)
extern void iOSMusic_ExtractBPM_m84D947BC9425524187EF0DAFA3F893A65BACBED6 ();
// 0x00000411 System.Void iOSMusic::ExtractGenre(System.String)
extern void iOSMusic_ExtractGenre_m4EC57A8C49A3F8514BC6C3510CED06E32AE48AED ();
// 0x00000412 System.Void iOSMusic::ExtractLyrics(System.String)
extern void iOSMusic_ExtractLyrics_mC1158A1AA168089FCBC3846945B0FEF2A6B84143 ();
// 0x00000413 System.Void iOSMusic::ExtractDuration(System.String)
extern void iOSMusic_ExtractDuration_m41DA794440E24FDD43B76438546201CA83B3CAF6 ();
// 0x00000414 System.Void iOSMusic::ExtractArtwork()
extern void iOSMusic_ExtractArtwork_mCE2EF273C5F89A01929C1C2DB6C76263821C25A4 ();
// 0x00000415 System.Void iOSMusic::UserDidCancel()
extern void iOSMusic_UserDidCancel_m3A5B01D6CADCAE864A5F9A6198EBD2A13F669269 ();
// 0x00000416 System.Void iOSMusic::.ctor()
extern void iOSMusic__ctor_m36E10D6E5FE742C14B07C4EE26869E414C4D50D6 ();
// 0x00000417 System.Void iOSMusic::.cctor()
extern void iOSMusic__cctor_mE5F8433E085CC9594A2D37A8A85928B8FAD1D471 ();
// 0x00000418 System.Void iOSMusicGUI::ShowMusicLibrary()
extern void iOSMusicGUI_ShowMusicLibrary_mB5D1F5A300EADB30426A89790B1FDC308E614B0E ();
// 0x00000419 System.Void iOSMusicGUI::PlayPause()
extern void iOSMusicGUI_PlayPause_m2454AB6E7786A020CA3021B06395554AFA6C8931 ();
// 0x0000041A System.Void iOSMusicGUI::PauseAudioSource()
extern void iOSMusicGUI_PauseAudioSource_m2867055A59421712C47869F58B3848F0D990B8B0 ();
// 0x0000041B System.Void iOSMusicGUI::UnPauseAudioSource()
extern void iOSMusicGUI_UnPauseAudioSource_mC7F5A2EF096D1BCE6DD12C1164F52D90856922A9 ();
// 0x0000041C System.Void iOSMusicGUI::LoadAudioClip()
extern void iOSMusicGUI_LoadAudioClip_m12C092D403D0428183A9034A46DE648EEF9E3B68 ();
// 0x0000041D System.Void iOSMusicGUI::NextSong()
extern void iOSMusicGUI_NextSong_m6A00E2507B6A005994AE4C9C3ACF7D0EECC1EB51 ();
// 0x0000041E System.Void iOSMusicGUI::PreviousSong()
extern void iOSMusicGUI_PreviousSong_m426D9927F088616AB019331E78DB4ACD92A3B2E9 ();
// 0x0000041F System.Void iOSMusicGUI::DisableButtons()
extern void iOSMusicGUI_DisableButtons_m9C0EA4819C2F3C01DA79F44FC7CE993950A3F6C8 ();
// 0x00000420 System.Void iOSMusicGUI::GoToScene2()
extern void iOSMusicGUI_GoToScene2_mB8648D2A95ABE59BE08003C953E0440F86C10B89 ();
// 0x00000421 System.Void iOSMusicGUI::GoToMainScene()
extern void iOSMusicGUI_GoToMainScene_m698E579204B142C87FEC4E0C6C2F95964AA107D0 ();
// 0x00000422 System.Void iOSMusicGUI::.ctor()
extern void iOSMusicGUI__ctor_m0D4ABE334B6AE15AD31262FBEB37360A3BFBCAE4 ();
// 0x00000423 System.Void musicManager::_openNativeMusicPicker(System.Boolean)
extern void musicManager__openNativeMusicPicker_m42D5CA4E039D2754C5E39F24BB0D98E4DF92F11D ();
// 0x00000424 System.Void musicManager::openNativeMusicPicker(System.Boolean)
extern void musicManager_openNativeMusicPicker_m74EE859F41D88D7F36969ADA1B2DFE138033F240 ();
// 0x00000425 System.Void musicManager::_playPause()
extern void musicManager__playPause_mAEF69B31AE2458E981774E91733B25AD95B0DC8E ();
// 0x00000426 System.Void musicManager::playPause()
extern void musicManager_playPause_m0276C3B7C47209A31CFED839A7820D7E52C10319 ();
// 0x00000427 System.Void musicManager::_loadAudioClip(System.Boolean)
extern void musicManager__loadAudioClip_m48E843591B5EB97E9AB966199BFDDC278ADEAFAB ();
// 0x00000428 System.Void musicManager::loadAudioClip(System.Boolean)
extern void musicManager_loadAudioClip_m176F786929C7A84DD2AC43C9B8885DCC8A15BF54 ();
// 0x00000429 System.Void musicManager::_previousSong()
extern void musicManager__previousSong_m73E9EF2453C040E2B721424A0392E3DDB834E213 ();
// 0x0000042A System.Void musicManager::previousSong()
extern void musicManager_previousSong_m9C52822B1E2F07B11D2A92C48E526A65372CC8EB ();
// 0x0000042B System.Void musicManager::_nextSong()
extern void musicManager__nextSong_m7A659B7A19CF0EC46D275592CBDF177594EA8A4D ();
// 0x0000042C System.Void musicManager::nextSong()
extern void musicManager_nextSong_m4445C1E59AF31A332B321A9FBCC07AFDFA41C578 ();
// 0x0000042D System.Void musicManager::_queryAppleMusic(System.String)
extern void musicManager__queryAppleMusic_mAEFE8D9E49BAC81EA8C662B8B4F6FA06B7D248FA ();
// 0x0000042E System.Void musicManager::queryAppleMusic(System.String)
extern void musicManager_queryAppleMusic_m7ABB23AFCE1EA38DA6C880094827B7149422C3F8 ();
// 0x0000042F System.Void musicManager::_stopAppleMusic()
extern void musicManager__stopAppleMusic_m1D04FA6900EF505436DA28976C73BFB7699B4EDF ();
// 0x00000430 System.Void musicManager::stopAppleMusic()
extern void musicManager_stopAppleMusic_mC3EE745D3F6907B7261CCA6A33FB2CFA526CCF1E ();
// 0x00000431 System.Void musicManager::.ctor()
extern void musicManager__ctor_m399CB614BF0BF0C2F7DB10B7EE31DC6BB08C2477 ();
// 0x00000432 System.Void AbstractLocalizedTextController::Awake()
extern void AbstractLocalizedTextController_Awake_mB3EF38A853B5DA5D765ED81C2CC312044EA5B279 ();
// 0x00000433 System.Void AbstractLocalizedTextController::InitController()
extern void AbstractLocalizedTextController_InitController_m614C34A50B8F0C72B0A8E53B692CC37DE3D920C1 ();
// 0x00000434 System.Void AbstractLocalizedTextController::SetText(System.String)
// 0x00000435 System.Void AbstractLocalizedTextController::SetLanguage()
extern void AbstractLocalizedTextController_SetLanguage_m8C246B55A184750C158BAC7E36EFDD04C40B34BD ();
// 0x00000436 System.Void AbstractLocalizedTextController::.ctor()
extern void AbstractLocalizedTextController__ctor_m91D1C2A31026FE4F5A894D8B7997E3AFD5C33C86 ();
// 0x00000437 UnityEngine.SystemLanguage LanguageController::get_Language()
extern void LanguageController_get_Language_m42952E381F0EA58E179B88FE0B2307BD78A97FBF ();
// 0x00000438 System.Int32 LanguageController::get_SavedLanguage()
extern void LanguageController_get_SavedLanguage_mEC0984116E11AD3D6906CDF30F2097C6D2B4B132 ();
// 0x00000439 System.Void LanguageController::set_SavedLanguage(System.Int32)
extern void LanguageController_set_SavedLanguage_m1BECBEFB6B606893E2D616C5FFE10954CC45BFC9 ();
// 0x0000043A System.Void LanguageController::Setlanguage(UnityEngine.SystemLanguage)
extern void LanguageController_Setlanguage_mADFA97CFAAA90435B2F4186629F377ED339008E3 ();
// 0x0000043B System.Void LanguageController::.ctor()
extern void LanguageController__ctor_m61CBB3F8B8347AF091E19176601B962611D69E51 ();
// 0x0000043C System.String LocalizedFrase::get_Value()
extern void LocalizedFrase_get_Value_mA7C5609F537C7026147FFA86052655751C3C3452 ();
// 0x0000043D System.String LocalizedFrase::GetValue(System.Object[])
extern void LocalizedFrase_GetValue_m4AA1D55306AAC7D2F89E43067A196B66A529939F ();
// 0x0000043E System.Int32 LocalizedFrase::GetLanguageId(UnityEngine.SystemLanguage)
extern void LocalizedFrase_GetLanguageId_mF6672182F1CD572BAFC6FC69B5BFF85BE7EA8FE1 ();
// 0x0000043F System.Boolean LocalizedFrase::HasLanguage(UnityEngine.SystemLanguage)
extern void LocalizedFrase_HasLanguage_mC22F1927F134FC8F749CEBEC1A070E07D12C9E19 ();
// 0x00000440 System.Void LocalizedFrase::AddLanguage(UnityEngine.SystemLanguage,System.String)
extern void LocalizedFrase_AddLanguage_m2AFB688B5BB1B8ABA76CA650BC7FF04A8732EF55 ();
// 0x00000441 System.String LocalizedFrase::GetValueByLang(UnityEngine.SystemLanguage)
extern void LocalizedFrase_GetValueByLang_m68E73B47CC258C6AE66A14BDD1E4DC212E0FA36A ();
// 0x00000442 System.Void LocalizedFrase::SetLangValue(UnityEngine.SystemLanguage,System.String)
extern void LocalizedFrase_SetLangValue_m60681A116537E07DA01DD4B0F447167E4165C906 ();
// 0x00000443 System.String LocalizedFrase::GetValue(System.String)
extern void LocalizedFrase_GetValue_m2B66BAD333324B812F02C065EFAD209FE7B924EE ();
// 0x00000444 UnityEngine.Font LocalizedFrase::get_Font()
extern void LocalizedFrase_get_Font_m1F4ADBD14104475772F80D79A2A6FFBD00A3B38F ();
// 0x00000445 System.Void LocalizedFrase::.ctor()
extern void LocalizedFrase__ctor_m0B12FC3B3BFC04540040EF1B2ACCDF6A057BEAC0 ();
// 0x00000446 System.Void LocalizedFrasesDatabase::add_setLanguage(System.Action)
extern void LocalizedFrasesDatabase_add_setLanguage_m924E1637BE1CB43AF2392B13558B67848B0E9E74 ();
// 0x00000447 System.Void LocalizedFrasesDatabase::remove_setLanguage(System.Action)
extern void LocalizedFrasesDatabase_remove_setLanguage_mE7C41A587DFEE94A305C0A0709AA03CFC29604C5 ();
// 0x00000448 System.Void LocalizedFrasesDatabase::AddSetLanguageEvent(System.Action)
extern void LocalizedFrasesDatabase_AddSetLanguageEvent_m20317898CA1F857C509727E9B80E855E7FA51366 ();
// 0x00000449 System.Void LocalizedFrasesDatabase::Setlanguage()
extern void LocalizedFrasesDatabase_Setlanguage_mC749DFA13867B375BE09422BF1FE1BD19D9BD698 ();
// 0x0000044A LocalizedFrase LocalizedFrasesDatabase::GetFrase(System.String)
extern void LocalizedFrasesDatabase_GetFrase_m28ED5007B896C5A8B48277250CECDBDAB1B7BB11 ();
// 0x0000044B System.Void LocalizedFrasesDatabase::.ctor()
extern void LocalizedFrasesDatabase__ctor_mA495DC622D2A1AB1C1D833F5CFCDA93618A593F7 ();
// 0x0000044C TMPro.TextMeshPro LocalizedMeshController::get_FraseText()
extern void LocalizedMeshController_get_FraseText_mE74311269BFF53B082BC1C2B8BC528672420F525 ();
// 0x0000044D System.Void LocalizedMeshController::SetText(System.String)
extern void LocalizedMeshController_SetText_m7A6EE491246448819552E798A689A77CE101F81A ();
// 0x0000044E System.Void LocalizedMeshController::.ctor()
extern void LocalizedMeshController__ctor_m868E848B4B9FDA3F5611725330B1A486F422D83E ();
// 0x0000044F UnityEngine.UI.Text LocalizedTextController::get_FraseText()
extern void LocalizedTextController_get_FraseText_m99ED2012B021E8A15C496284FC0CBD2D0EF2DF59 ();
// 0x00000450 System.Void LocalizedTextController::SetText(System.String)
extern void LocalizedTextController_SetText_m1BCB8AAD7FE6325583FFDD200BD3D02A7AC111DF ();
// 0x00000451 System.Void LocalizedTextController::.ctor()
extern void LocalizedTextController__ctor_m2E2C94E118225BB58C6A9A69D5E1002F417C899C ();
// 0x00000452 TMPro.TextMeshProUGUI LocalizedTextMeshController::get_FraseText()
extern void LocalizedTextMeshController_get_FraseText_m4A8978D27DF4D7570A44C78E6652C216A7E5B72B ();
// 0x00000453 System.Void LocalizedTextMeshController::SetText(System.String)
extern void LocalizedTextMeshController_SetText_m29DD6C644B7A154A9DC54DCADAF3D675A32BEE5E ();
// 0x00000454 System.Void LocalizedTextMeshController::.ctor()
extern void LocalizedTextMeshController__ctor_mD9478751059335D9C418B641488C549306A8A820 ();
// 0x00000455 System.Void AppleSubscriptions::ShowSubscriptionScreen(System.String)
extern void AppleSubscriptions_ShowSubscriptionScreen_m4B31192D087AA1BD6037774DEC800C80118681A8 ();
// 0x00000456 System.Void AppleSubscriptions::ShowSubscription(System.String)
extern void AppleSubscriptions_ShowSubscription_m61BC58C340F43DB951354A42BC57F23DAA676730 ();
// 0x00000457 System.Void CrashlyticInit::Start()
extern void CrashlyticInit_Start_m378A4F28AC380BBAD658185D65857DF5E434D23A ();
// 0x00000458 System.Void CrashlyticInit::.ctor()
extern void CrashlyticInit__ctor_mD135467CCC1E57B58314665D648493B48E8706D5 ();
// 0x00000459 System.Void FindButtons::Find()
extern void FindButtons_Find_m57EFDA38BFC6B0A83A5F9DD06B7FAC26D8DA3FC1 ();
// 0x0000045A System.Void FindButtons::Start()
extern void FindButtons_Start_m3979CA10C44CF73054046CAC2D79AE258A0081E7 ();
// 0x0000045B System.Void FindButtons::.ctor()
extern void FindButtons__ctor_mDAAF20627F316FD3262581BA7B7BE11F91543712 ();
// 0x0000045C System.Void FirebaseManager::.ctor()
extern void FirebaseManager__ctor_m195CD6E7DD27AD966EBEB150DC1D161C2A7EF38E ();
// 0x0000045D BookState Book::get_CurrentState()
extern void Book_get_CurrentState_mA7CFA752B79E5189ECDD2147799F48A9E6D73B0C ();
// 0x0000045E UnityEngine.Sprite Book::get_CurrentStateIcon()
extern void Book_get_CurrentStateIcon_m7F336D495D9E75EB5F5FF17183CA9D2FD7543924 ();
// 0x0000045F System.Void Book::.ctor()
extern void Book__ctor_mC5EF0C93595AD5E495DEE999F4E5D7E75F5FFEEB ();
// 0x00000460 System.Void BookPage::.ctor()
extern void BookPage__ctor_m2787C765538695D75C8C12F93187D24BA014EC95 ();
// 0x00000461 System.Void MiniGameData::.ctor()
extern void MiniGameData__ctor_m10576CB492664E21AD6851F9194B85EE7E511954 ();
// 0x00000462 System.Void GameController::Awake()
extern void GameController_Awake_m0F7A2663599EE488BA03E9010D12DF3D9EF623BE ();
// 0x00000463 System.Void GameController::Start()
extern void GameController_Start_m229BE9A914ADF299ACFF1EFD5F15F27FF146D3C0 ();
// 0x00000464 System.Void GameController::InitUIEvents()
extern void GameController_InitUIEvents_mD712073351E17B5FB03A6CAE743B721F42DB16FB ();
// 0x00000465 System.Void GameController::MainFromSettings()
extern void GameController_MainFromSettings_m27275DDEC5BCE9DD2CDFB453AF4BA79F46FC8DB5 ();
// 0x00000466 System.Void GameController::MainFromSouvenirs()
extern void GameController_MainFromSouvenirs_m38E730873B8287C568E8BB32C576DB4EC2C85610 ();
// 0x00000467 System.Void GameController::MainFromShop()
extern void GameController_MainFromShop_mAAD4EA08A2A050127ACB1F4C10C7C08DB9A46C92 ();
// 0x00000468 System.Void GameController::MainFromBook()
extern void GameController_MainFromBook_m366A997DE313E725E536FD7477A80BC7D863299F ();
// 0x00000469 System.Void GameController::ShopFromMain()
extern void GameController_ShopFromMain_m5131CF9D5B501A6BBA071B944E4A0BED2B89855F ();
// 0x0000046A System.Void GameController::ShopFromShopShopBookPreview()
extern void GameController_ShopFromShopShopBookPreview_mB056A0C36811F9ECA60B56DEE93AFB16D7C730A9 ();
// 0x0000046B System.Void GameController::BookPreviewFromBookContent()
extern void GameController_BookPreviewFromBookContent_m3DFDC2325A56CEA1A486077F39B71BBF2C119A5E ();
// 0x0000046C System.Void GameController::BookFromBookContent()
extern void GameController_BookFromBookContent_m1147D1707BCF9EAEB2D2076EF1F1C9AA48DB6076 ();
// 0x0000046D System.Void GameController::BookContentFromBook()
extern void GameController_BookContentFromBook_m1BDB14674F3D7FC44991B9D99014EF13230991B2 ();
// 0x0000046E System.Void GameController::SettingsFromMain()
extern void GameController_SettingsFromMain_m5D3CAEBC5C1C27004F8E471C675E9B336DE639A4 ();
// 0x0000046F System.Void GameController::SettingsFromLocalization()
extern void GameController_SettingsFromLocalization_mE88F090CBA7CEB9EA51510B29CB723E50677C802 ();
// 0x00000470 System.Void GameController::SettingsFromAbout()
extern void GameController_SettingsFromAbout_m8AAA24238F4AB5066D654B20CDA397C35408944D ();
// 0x00000471 System.Void GameController::AboutFromSettings()
extern void GameController_AboutFromSettings_m95A2352F24EB85FC0D962E19AD806E0F8C38CA8F ();
// 0x00000472 System.Void GameController::SouvenirsFromMain()
extern void GameController_SouvenirsFromMain_m7547EAAF632FCF38E39972B3E1F3A581D2736182 ();
// 0x00000473 System.Void GameController::LocalizationFromSettings()
extern void GameController_LocalizationFromSettings_mBEE2B2C165108951C02EB8392D1F28BC94D3D366 ();
// 0x00000474 System.Void GameController::.ctor()
extern void GameController__ctor_m839A9CFB9635B009C1DA139BEAD0E38467E57464 ();
// 0x00000475 System.Void DragableElement::TouchUp(System.Boolean)
extern void DragableElement_TouchUp_m9743F990C60A3A6B494998CBE3EFBA787FA08684 ();
// 0x00000476 System.Void DragableElement::Init(UnityEngine.Sprite)
extern void DragableElement_Init_m2F3EE9099D5C8A5A9920FF86E27415F4530313F7 ();
// 0x00000477 System.Void DragableElement::Fall(System.Single)
extern void DragableElement_Fall_m25109712514B83AFB8BD4A2976F42874DE962C67 ();
// 0x00000478 System.Void DragableElement::.ctor()
extern void DragableElement__ctor_m1DB02098A80CC0BA43B95015F2198AACA2AF9F2E ();
// 0x00000479 System.Void DragableElement::<TouchUp>b__5_0()
extern void DragableElement_U3CTouchUpU3Eb__5_0_m981583A5D35D2A82B928B5B8B6F1006361969FE2 ();
// 0x0000047A System.Void ImageController::add_clickAction(System.Action)
extern void ImageController_add_clickAction_m90D85E2AD366713B5E2B5EE2CEA401A8999ABB81 ();
// 0x0000047B System.Void ImageController::remove_clickAction(System.Action)
extern void ImageController_remove_clickAction_m8038C213433C33AFB2EDC5FA0747151F855C7F16 ();
// 0x0000047C System.Void ImageController::add_firstClickAction(System.Action)
extern void ImageController_add_firstClickAction_m04EBC59B15F7A1D7A479B6E565F0CFE88521BB39 ();
// 0x0000047D System.Void ImageController::remove_firstClickAction(System.Action)
extern void ImageController_remove_firstClickAction_mB257D872C6CB36CA0BAC8D996F2D99DB5367F1F9 ();
// 0x0000047E System.Void ImageController::Reset()
extern void ImageController_Reset_m82B007748A013D447A863BEFF52DCBE94B9DFB1C ();
// 0x0000047F System.Void ImageController::Click()
extern void ImageController_Click_m3D8630E042B3157212611D2743F9DD86E8D80E22 ();
// 0x00000480 System.Void ImageController::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void ImageController_OnPointerClick_mA834FB02FB295FED043892B78156186447C384D6 ();
// 0x00000481 System.Void ImageController::.ctor()
extern void ImageController__ctor_m4C6CA6E7CB52930B2A60BC1BABFE1C243169534B ();
// 0x00000482 System.Void MiniGameAbstractController::PlayLevel(MiniGameData,System.Int32)
extern void MiniGameAbstractController_PlayLevel_mF23DD7A356C616037F1C842A59EAFCFF3A05E198 ();
// 0x00000483 System.Void MiniGameAbstractController::Close()
extern void MiniGameAbstractController_Close_mE627B18ADE52989B98F6F8AC56C7BB4BF0265478 ();
// 0x00000484 System.Void MiniGameAbstractController::ToLevelsList()
extern void MiniGameAbstractController_ToLevelsList_m425DA0B9236EC249FC298CB76C75A00246262A78 ();
// 0x00000485 System.Void MiniGameAbstractController::ResizeRect(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void MiniGameAbstractController_ResizeRect_mA7FE3AE3E07BEBE563F0A1E0E725B3CE6F20EA5E ();
// 0x00000486 System.Void MiniGameAbstractController::InitButtons()
extern void MiniGameAbstractController_InitButtons_mB24FF9D465A789ED8F4A5DEB6437E7B71719B74C ();
// 0x00000487 System.Void MiniGameAbstractController::InitGame(MiniGameData,System.Int32,UnityEngine.Vector2)
extern void MiniGameAbstractController_InitGame_m2FEEC8ABAC3AE8D00269835FF9A8252467A801F1 ();
// 0x00000488 System.Void MiniGameAbstractController::InitLevel(System.Int32)
// 0x00000489 System.Void MiniGameAbstractController::Reset()
// 0x0000048A System.Void MiniGameAbstractController::StartGame()
extern void MiniGameAbstractController_StartGame_mBE7951FE61141018AFE536AC4CE9B18D716AA733 ();
// 0x0000048B System.Void MiniGameAbstractController::RestartGame()
// 0x0000048C System.Void MiniGameAbstractController::CheckGameState()
// 0x0000048D System.Void MiniGameAbstractController::NextLevel()
extern void MiniGameAbstractController_NextLevel_m4922C386AFA325B8AF02CBBE7504B63A5C55AFC2 ();
// 0x0000048E System.Void MiniGameAbstractController::.ctor()
extern void MiniGameAbstractController__ctor_m3C597F284CF2D168E10887E08103C86342478E0C ();
// 0x0000048F System.Void MiniGamePreset::.ctor()
extern void MiniGamePreset__ctor_m30EDB015DE08F758F654602ED6A965B89535224F ();
// 0x00000490 System.Void FindPictureField::FindImages()
extern void FindPictureField_FindImages_mEF3519293F31DEFBCEEF62CD39C27B53C32A1E22 ();
// 0x00000491 System.Void FindPictureField::.ctor()
extern void FindPictureField__ctor_mE85C42763D55F8EF651593E0E8B3A51E3F236FA0 ();
// 0x00000492 System.Void FindPictureImageController::OpenHiddenElement()
extern void FindPictureImageController_OpenHiddenElement_mF5A185505B2EDFCD07A672D81B8042C8896D4E4E ();
// 0x00000493 System.Void FindPictureImageController::.ctor()
extern void FindPictureImageController__ctor_m367A8A87BEB0280D3CC88BF45C4DF4B583D62855 ();
// 0x00000494 System.Void FindPictureList::Reset()
extern void FindPictureList_Reset_m7E7F208E1173554AC18C2E2B473C7062D840CDB4 ();
// 0x00000495 FindPictureListElement FindPictureList::AddElement(UnityEngine.Sprite)
extern void FindPictureList_AddElement_mC627EF70488B95961CCEE14908A7A02DF085984D ();
// 0x00000496 System.Void FindPictureList::.ctor()
extern void FindPictureList__ctor_mE78DA7DC17364977DB82A7B5925852295211CC0D ();
// 0x00000497 System.Boolean FindPictureListElement::get_IsOpen()
extern void FindPictureListElement_get_IsOpen_mF80F7422E5B47F77A8E243DFDC96629D11B4A9E1 ();
// 0x00000498 System.Void FindPictureListElement::set_IsOpen(System.Boolean)
extern void FindPictureListElement_set_IsOpen_mD29D538B1AA28752CC966F7066DBB078E4C161D2 ();
// 0x00000499 System.Void FindPictureListElement::.ctor()
extern void FindPictureListElement__ctor_m6BF17C06228D6FFE54A1F43A04DE91A900BEF410 ();
// 0x0000049A System.Void FindPicturePreset::.ctor()
extern void FindPicturePreset__ctor_mBBE4E68FBF89064DFC97EE84B6C7B685A934FAD3 ();
// 0x0000049B System.Void MiniGameFindPicture::InitGame(MiniGameData,System.Int32,UnityEngine.Vector2)
extern void MiniGameFindPicture_InitGame_mBA32BA8C1F87A4198D97FECAAA799D7CA444B42E ();
// 0x0000049C System.Void MiniGameFindPicture::InitLevel(System.Int32)
extern void MiniGameFindPicture_InitLevel_mE6565443776B0663C646588F8C18A0276ECC6A72 ();
// 0x0000049D System.Void MiniGameFindPicture::InitLevel(FindPicturePreset)
extern void MiniGameFindPicture_InitLevel_m9D002439374AB00DCCBC9EFB195889476208BCED ();
// 0x0000049E System.Void MiniGameFindPicture::InitElementsOnField(FindPictureField)
extern void MiniGameFindPicture_InitElementsOnField_m1FFCB24F4828B45E23F48125A20D2C1DF5A01CAF ();
// 0x0000049F System.Void MiniGameFindPicture::OpenElement()
extern void MiniGameFindPicture_OpenElement_mDEEA54D18CF848A2923ED8CE08BDA2963B688A60 ();
// 0x000004A0 System.Void MiniGameFindPicture::Reset()
extern void MiniGameFindPicture_Reset_mFF179DFF7B34C6397F28619675914C03DAC09D57 ();
// 0x000004A1 System.Void MiniGameFindPicture::RestartGame()
extern void MiniGameFindPicture_RestartGame_mD7CE2DAD5411AC6C28C4D671F451C97A9B4E9DD6 ();
// 0x000004A2 System.Void MiniGameFindPicture::CheckGameState()
extern void MiniGameFindPicture_CheckGameState_mD86092E7765CE82ADB9B9364B0719775B56393E0 ();
// 0x000004A3 System.Void MiniGameFindPicture::.ctor()
extern void MiniGameFindPicture__ctor_m50ECA599803E245B766329528EC6C48DDAE0669D ();
// 0x000004A4 System.Void PuzzleAbstractField::Start()
extern void PuzzleAbstractField_Start_m6CCA11B8574641E514EF738E8DFD9CD4866BA404 ();
// 0x000004A5 System.Void PuzzleAbstractField::InitMiniGame()
extern void PuzzleAbstractField_InitMiniGame_m282F06AF131D7359E12856B733DAD81C7CB20254 ();
// 0x000004A6 System.Void PuzzleAbstractField::Reset()
// 0x000004A7 System.Void PuzzleAbstractField::Init(MiniGamePreset)
// 0x000004A8 System.Void PuzzleAbstractField::.ctor()
extern void PuzzleAbstractField__ctor_mD460EFC133C9C9C6BD10A0704BAFB2DC18AF7012 ();
// 0x000004A9 System.Void PuzzleDragableElement::.ctor()
extern void PuzzleDragableElement__ctor_mF49301612B9ADEFC43FF9248EDA77E28DCE5EEAB ();
// 0x000004AA System.Boolean PuzzleElement::get_IsOpen()
extern void PuzzleElement_get_IsOpen_m38A1E7F16E57EC5A0D3BC2A7C6DD027F760B3C53 ();
// 0x000004AB System.Void PuzzleElement::set_IsOpen(System.Boolean)
extern void PuzzleElement_set_IsOpen_mD0986B98264A5C37C173DEEACC29D5725961A7F2 ();
// 0x000004AC System.Boolean PuzzleElement::TryOpen(System.Int32)
extern void PuzzleElement_TryOpen_mBA56A69CC9FAE69658CAF16D1DB6788483C466CE ();
// 0x000004AD System.Void PuzzleElement::Init(PuzzleMiniGame,System.Int32,UnityEngine.Sprite)
extern void PuzzleElement_Init_mBE9FD4E1E814F737B7ED744F27C15B2FE1C9D470 ();
// 0x000004AE System.Void PuzzleElement::DebugOpen()
extern void PuzzleElement_DebugOpen_mF0CB5BE5280D37C1F1293AD77709F32A6BFB9909 ();
// 0x000004AF System.Void PuzzleElement::.ctor()
extern void PuzzleElement__ctor_m2416D8EC5481DD3E7963791F23107E6BBF4839F1 ();
// 0x000004B0 System.Void PuzzleElementsList::Reset()
extern void PuzzleElementsList_Reset_m67C05CDB46FD42143194A705F62D3E6B528EF0C3 ();
// 0x000004B1 System.Void PuzzleElementsList::Init(MiniGamePreset)
extern void PuzzleElementsList_Init_m73195F564ED491B1B48FF1987AB321EBD8422DB2 ();
// 0x000004B2 System.Void PuzzleElementsList::Shuffle()
extern void PuzzleElementsList_Shuffle_mE02AEAD70A3AB34DE344CA8D56DDADE242217598 ();
// 0x000004B3 System.Void PuzzleElementsList::ItemSelected()
extern void PuzzleElementsList_ItemSelected_mB2F24A50DF6FF216CA8AE722CF37AFC5C081374F ();
// 0x000004B4 System.Void PuzzleElementsList::ItemUnselect()
extern void PuzzleElementsList_ItemUnselect_m8551DA25070E2139552877B45D418834B3988C2A ();
// 0x000004B5 System.Void PuzzleElementsList::ResetoreElement(PuzzleListElement)
extern void PuzzleElementsList_ResetoreElement_mA2DD8C7A7F2329AA080CE7CF3D230AE99E73E526 ();
// 0x000004B6 System.Void PuzzleElementsList::ResetoreElement(System.Int32)
extern void PuzzleElementsList_ResetoreElement_m3D6688DE7FB29A4B58F95A2F614BFAA17C92FE2D ();
// 0x000004B7 System.Void PuzzleElementsList::.ctor()
extern void PuzzleElementsList__ctor_m12E939FF7A5BCEB5E333138E4E2E4588A66B7C92 ();
// 0x000004B8 System.Void PuzzleField::Reset()
extern void PuzzleField_Reset_m5A0FC0428916DEFD10AD83C2A5484700C430CB24 ();
// 0x000004B9 System.Void PuzzleField::Init(MiniGamePreset)
extern void PuzzleField_Init_mB42A732CC5ECCC34A41FB0EB23217B3DE1074AF5 ();
// 0x000004BA System.Void PuzzleField::InitSettings(System.Int32)
extern void PuzzleField_InitSettings_m382ABEB5797060535EABB6AE209F9531D4ECD027 ();
// 0x000004BB System.Void PuzzleField::TryOpen(System.Int32)
extern void PuzzleField_TryOpen_m48EDD9E3A87271D64E7EEEFE5CC51B32587687BD ();
// 0x000004BC System.Void PuzzleField::.ctor()
extern void PuzzleField__ctor_m356E547F7971680A258A7085252A0B16F9FB5DDA ();
// 0x000004BD System.Void PuzzleListElement::Init(PuzzleMiniGame,System.Int32,UnityEngine.Sprite)
extern void PuzzleListElement_Init_m4729C2DCEAB3BEB83515F5B62EA504DFEE47E293 ();
// 0x000004BE System.Void PuzzleListElement::Click()
extern void PuzzleListElement_Click_m32D3F85834B4D9E68A9CD76F5ABE0C0A7B0784A2 ();
// 0x000004BF System.Void PuzzleListElement::MoveToBottom()
extern void PuzzleListElement_MoveToBottom_m715D8E43BB3194C5BA84017DEF35D2F07876A655 ();
// 0x000004C0 System.Void PuzzleListElement::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void PuzzleListElement_OnPointerDown_m79283E6D3BABAB1DC5C25393FC814333130361F0 ();
// 0x000004C1 System.Void PuzzleListElement::.ctor()
extern void PuzzleListElement__ctor_m897F49723BD269A228218AF89EEF71DBC52BBBAD ();
// 0x000004C2 System.Void PuzzleMiniGame::Start()
extern void PuzzleMiniGame_Start_mD27B608C9118F1EAB1848AF9204536FA628748FF ();
// 0x000004C3 System.Void PuzzleMiniGame::InitGame(MiniGameData,System.Int32,UnityEngine.Vector2)
extern void PuzzleMiniGame_InitGame_m57FA432443C7F00463F0142596B4E24BE824E9CF ();
// 0x000004C4 System.Void PuzzleMiniGame::InitLevel(System.Int32)
extern void PuzzleMiniGame_InitLevel_m2088C691EC1214109B75B63CEF3B0572EC2FBA4B ();
// 0x000004C5 System.Void PuzzleMiniGame::InitLevel(PuzzlePreset)
extern void PuzzleMiniGame_InitLevel_mAF934C325E059D0E1C9345439B3F745AF234F238 ();
// 0x000004C6 System.Void PuzzleMiniGame::CheckGameState()
extern void PuzzleMiniGame_CheckGameState_m9AB0EFAFCA57BEC6283EF588E0E9776F14C98ADC ();
// 0x000004C7 System.Void PuzzleMiniGame::FindedPuzzle()
extern void PuzzleMiniGame_FindedPuzzle_mB28B44A26B6492FEE1FAC586E1DEEC2A3F4B2993 ();
// 0x000004C8 System.Void PuzzleMiniGame::SelectElement(System.Int32)
extern void PuzzleMiniGame_SelectElement_mC767CB400E6D434FBB0396272951D338006357FA ();
// 0x000004C9 System.Void PuzzleMiniGame::Update()
extern void PuzzleMiniGame_Update_m6619EE8A58964E89276252DE51EE0681A9B72C34 ();
// 0x000004CA System.Void PuzzleMiniGame::TouchUp(UnityEngine.Vector3)
extern void PuzzleMiniGame_TouchUp_m3C7A0E1D8D4A54D2CED03EED935833C264D78066 ();
// 0x000004CB System.Void PuzzleMiniGame::Reset()
extern void PuzzleMiniGame_Reset_mAA9497DB046B1EBB562CF555B7D6BEAA3640FD0C ();
// 0x000004CC System.Void PuzzleMiniGame::RestartGame()
extern void PuzzleMiniGame_RestartGame_mB30C955B2B1C7599EA80299E5B0E0098E5A1C363 ();
// 0x000004CD System.Void PuzzleMiniGame::.ctor()
extern void PuzzleMiniGame__ctor_m189CF17B1DCD4767FFDC5E61CEABE46BF620E7BD ();
// 0x000004CE System.Void PuzzlePreset::.ctor()
extern void PuzzlePreset__ctor_m441EEC0E918E00132D854C4FDB61799F86F7565E ();
// 0x000004CF System.Void RoadsCharacter::.ctor()
extern void RoadsCharacter__ctor_m8161CFE5FB0A4CAA4183DEFBBE76C51432E4D9F3 ();
// 0x000004D0 RoadsCeil RoadsElement::get_Ceil()
extern void RoadsElement_get_Ceil_m0414C61B96FC7017CBD603C326D7A97AE0EEAFE5 ();
// 0x000004D1 System.Void RoadsElement::Init(RoadsMinigame,RoadsCeil,UnityEngine.Vector2,RoadsSpritesPreset)
extern void RoadsElement_Init_mA0884313602E31F63A522FF9EF78763A60B7DCE9 ();
// 0x000004D2 System.Void RoadsElement::DrawCeil()
extern void RoadsElement_DrawCeil_m8D508D86D962847B7F602F27F75B739DE0114F9A ();
// 0x000004D3 System.Void RoadsElement::Rotate()
extern void RoadsElement_Rotate_m192E60C971C2C5BD92DE262760F81E50D2EDB063 ();
// 0x000004D4 System.Void RoadsElement::Click()
extern void RoadsElement_Click_mC45D5F0942BB180CD6EF33D0D73D05C59B0DC215 ();
// 0x000004D5 System.Void RoadsElement::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void RoadsElement_OnPointerDown_m6DD8C58D733ADB7E7D5E25543104F5034B897392 ();
// 0x000004D6 System.Void RoadsElement::.ctor()
extern void RoadsElement__ctor_m5BED9D2824D09A790181E95BB5026D7F5DA681DA ();
// 0x000004D7 System.Int32 RoadsField::get_RowsCount()
extern void RoadsField_get_RowsCount_m295C2B32EDABFF70E8C1960A3A21AD34923DC4FF ();
// 0x000004D8 UnityEngine.Vector3[] RoadsField::get_WinRoad()
extern void RoadsField_get_WinRoad_m2387EAA899E49BEC308319A31189A809954E71AF ();
// 0x000004D9 System.Boolean RoadsField::get_IsCompleted()
extern void RoadsField_get_IsCompleted_m82170DD4C3355A987B402248A79CA167CD2E0878 ();
// 0x000004DA System.Boolean RoadsField::FieldState()
extern void RoadsField_FieldState_mE4ACA5BAF4B2F538F3CC34F56F04013CF46D2DAA ();
// 0x000004DB System.Boolean RoadsField::IsConnectedCeils(UnityEngine.Vector2,UnityEngine.Vector2)
extern void RoadsField_IsConnectedCeils_m3A4E96058DBAF0DAEC740D090EC08179B9873DD8 ();
// 0x000004DC System.Boolean RoadsField::TryMoveTo(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,UnityEngine.Vector2[])
extern void RoadsField_TryMoveTo_m98537C7AE3DBAD6C0477777512052A4DC6E4579D ();
// 0x000004DD System.Boolean RoadsField::HasCeil(UnityEngine.Vector2[],UnityEngine.Vector2)
extern void RoadsField_HasCeil_m965B4EF30E9C0B56616B6CC9DCD4254C70E0825B ();
// 0x000004DE RoadsCeil RoadsField::GetCeil(UnityEngine.Vector2)
extern void RoadsField_GetCeil_m8983A615A1247C989E7AA585AB4C81D13FCA5021 ();
// 0x000004DF System.Void RoadsField::PlayAnimation(System.Single)
extern void RoadsField_PlayAnimation_mBB129F431B4A6B0365333D91EFF7BF04610145EA ();
// 0x000004E0 System.Void RoadsField::AnimationLerp(System.Single)
extern void RoadsField_AnimationLerp_m82BF4247951287335B52DE6BE81A4509FF5973F8 ();
// 0x000004E1 System.Void RoadsField::InitMiniGame()
extern void RoadsField_InitMiniGame_m59FDAB0D055538723F43DE4FAEB288853DFABB18 ();
// 0x000004E2 System.Void RoadsField::Reset()
extern void RoadsField_Reset_m3502006A622C077F193657BC60EE74B7BDCD9391 ();
// 0x000004E3 System.Void RoadsField::Init(MiniGamePreset)
extern void RoadsField_Init_m6C344F5705F478EE6CDCCA438E49316A52A0915B ();
// 0x000004E4 System.Void RoadsField::InitCharacters(RoadsPreset)
extern void RoadsField_InitCharacters_mE76DECF810403226AFECB94E19E6B508B6F972E1 ();
// 0x000004E5 System.Void RoadsField::InitSettings(System.Int32)
extern void RoadsField_InitSettings_m8D99715F8458FF048981A52A89FA2741EA48D301 ();
// 0x000004E6 System.Void RoadsField::.ctor()
extern void RoadsField__ctor_m17FF3BB2B998FBEA736E5BE20A37BA2B0C07AB22 ();
// 0x000004E7 System.Void RoadsMinigame::CheckGameState()
extern void RoadsMinigame_CheckGameState_m06FCD6873D5C06616568057B28BE713A3CA0FEE8 ();
// 0x000004E8 System.Void RoadsMinigame::InitGame(MiniGameData,System.Int32,UnityEngine.Vector2)
extern void RoadsMinigame_InitGame_m8A282A620FE7F6842460CAF1A006ADB8F9E1D4F3 ();
// 0x000004E9 System.Void RoadsMinigame::InitLevel(System.Int32)
extern void RoadsMinigame_InitLevel_mC13E8E8FC4CB6C7D131A0B638CD66CAE0C913DE9 ();
// 0x000004EA System.Void RoadsMinigame::InitLevel(RoadsPreset)
extern void RoadsMinigame_InitLevel_m51731BD6EE81ECF742A3D331CD37E3F1302558EC ();
// 0x000004EB System.Void RoadsMinigame::Reset()
extern void RoadsMinigame_Reset_m5E78FB3C7019B6C10D53D7560C708416B14D1EB0 ();
// 0x000004EC System.Void RoadsMinigame::RestartGame()
extern void RoadsMinigame_RestartGame_m1B6DB8FA48674ADD0281533025F023C444EEF995 ();
// 0x000004ED System.Void RoadsMinigame::.ctor()
extern void RoadsMinigame__ctor_m68E9953E7341C7648A36AC7CF05BFD19A2D9CCD7 ();
// 0x000004EE System.Void RoadsMinigame::<CheckGameState>b__3_0()
extern void RoadsMinigame_U3CCheckGameStateU3Eb__3_0_m2C851C277FA8434074F52C826EA616B4F496DD1C ();
// 0x000004EF System.Void RoadsPreset::Generate()
extern void RoadsPreset_Generate_m7A24ACB1527590252D290343A6E17FDA37C87138 ();
// 0x000004F0 System.Void RoadsPreset::.ctor()
extern void RoadsPreset__ctor_m83707AF4299FFC2E2E852871D47B15E669358E50 ();
// 0x000004F1 System.Void RoadsGrid::Generate(System.Int32)
extern void RoadsGrid_Generate_m43DDDD4E5FF3C0644A5933311F76AFD8FEFF4BF0 ();
// 0x000004F2 System.Void RoadsGrid::Generate(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern void RoadsGrid_Generate_m450398321582926D65E9C80AABF2EA6FC2AEFC87 ();
// 0x000004F3 System.Void RoadsGrid::Prepare(System.Int32)
extern void RoadsGrid_Prepare_mD97111D775D350B003994F38EB4DD13AD1F4247B ();
// 0x000004F4 System.Void RoadsGrid::GenerateAndConnect(UnityEngine.Vector2,UnityEngine.Vector2)
extern void RoadsGrid_GenerateAndConnect_m9CA9F7DB4B5F263C8E71C5B7CE37A68C56FD2BFD ();
// 0x000004F5 System.Void RoadsGrid::Shuffle(UnityEngine.Vector2,UnityEngine.Vector2)
extern void RoadsGrid_Shuffle_m05B9445CECFA67AAE64047F07E422EBD0DEBDA2E ();
// 0x000004F6 System.Boolean RoadsGrid::IsConnectedCeils(UnityEngine.Vector2,UnityEngine.Vector2)
extern void RoadsGrid_IsConnectedCeils_m5A5AD221A8760A673BE082AC4E170BD43665876F ();
// 0x000004F7 System.Boolean RoadsGrid::TryMoveTo(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void RoadsGrid_TryMoveTo_m7BCDE03EDB59EA9BF36303045E6F07CCB1DB09B0 ();
// 0x000004F8 RoadsCeil RoadsGrid::GetCeil(UnityEngine.Vector2)
extern void RoadsGrid_GetCeil_m50FC227B16FC2A13E4184F6FC7C76AD6D892F74A ();
// 0x000004F9 System.Void RoadsGrid::.ctor()
extern void RoadsGrid__ctor_m89EBD187DA4349DEC57E6E485F6B2714AC453FA6 ();
// 0x000004FA System.Int32 RoadsCeil::get_WayCount()
extern void RoadsCeil_get_WayCount_m683B11F1EF86A4253AA463FA48CFC0251C33DBDE ();
// 0x000004FB System.Single RoadsCeil::get_ImageAngle()
extern void RoadsCeil_get_ImageAngle_mACDF93184272C86E356774224EC6870459798212 ();
// 0x000004FC System.Single RoadsCeil::get_Chance()
extern void RoadsCeil_get_Chance_m973B158864D1E8B66C37570EE61513B13C61B337 ();
// 0x000004FD System.Void RoadsCeil::InitRandom()
extern void RoadsCeil_InitRandom_mCAB9505A448B5ADCADC328C74250764E2AA808CE ();
// 0x000004FE System.Void RoadsCeil::Rotate()
extern void RoadsCeil_Rotate_mA4B3B82134A306134542151267C81043F842D40A ();
// 0x000004FF RoadsCeil RoadsCeil::Clone()
extern void RoadsCeil_Clone_m4C231C055375EC4FD74FDC9DB792FFBEBE1247BF ();
// 0x00000500 System.Void RoadsCeil::.ctor()
extern void RoadsCeil__ctor_m99E999DAF60A8B8892D2D665E488B613E6A5125C ();
// 0x00000501 UnityEngine.Sprite RoadsSpritesPreset::GetRoad(System.Single&,RoadsCeil)
extern void RoadsSpritesPreset_GetRoad_m3A71751E12303908B160821BCE8376C92DA06828 ();
// 0x00000502 UnityEngine.Sprite RoadsSpritesPreset::GetBackgroung(System.Single&,System.Int32,System.Int32,System.Int32)
extern void RoadsSpritesPreset_GetBackgroung_m5DECD9DD24FCCA98BDF80DB6CBB120C4158252F5 ();
// 0x00000503 System.Void RoadsSpritesPreset::.ctor()
extern void RoadsSpritesPreset__ctor_m769E93F366451910D198FA569A5E17C6682AF0D0 ();
// 0x00000504 System.Void RotatePicturesElement::Init(RotatePicturesMiniGame,UnityEngine.Sprite)
extern void RotatePicturesElement_Init_m140EE2612A6F51203ABB20615F538A62B9550310 ();
// 0x00000505 System.Void RotatePicturesElement::Shuffle()
extern void RotatePicturesElement_Shuffle_m7480567E0A06FA2A26C429C8B58A41A136353C1F ();
// 0x00000506 System.Void RotatePicturesElement::Rotate()
extern void RotatePicturesElement_Rotate_m91DF233A4D4CD4AA45CE31C4B6F5ADCD5DC793C0 ();
// 0x00000507 UnityEngine.Vector3 RotatePicturesElement::GetNextRotation()
extern void RotatePicturesElement_GetNextRotation_m6AE5778223EFB3E6164A1C30DFF929094936581D ();
// 0x00000508 System.Void RotatePicturesElement::Click()
extern void RotatePicturesElement_Click_mA662C17BBFC06073CF55A6B62FF1B400A7A676AC ();
// 0x00000509 System.Void RotatePicturesElement::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void RotatePicturesElement_OnPointerDown_mCD93DE4DCB50491AE0B19542E87D580E0D3961F5 ();
// 0x0000050A System.Void RotatePicturesElement::.ctor()
extern void RotatePicturesElement__ctor_mBBC2D9661D4113A5CC76EA60A6DF6E95EE61F07C ();
// 0x0000050B System.Boolean RotatePicturesField::get_IsCompleted()
extern void RotatePicturesField_get_IsCompleted_m165B79A933FB10E14B851ADCC42738CAE0E355C8 ();
// 0x0000050C System.Void RotatePicturesField::PlayAnimation(System.Single)
extern void RotatePicturesField_PlayAnimation_m8D28C2ECE32B805D1F91221E58447470B35D3022 ();
// 0x0000050D System.Void RotatePicturesField::AnimationLerp(System.Single)
extern void RotatePicturesField_AnimationLerp_m618D85272F87BDAF2F45A69EE5352473F65D4C58 ();
// 0x0000050E System.Void RotatePicturesField::InitMiniGame()
extern void RotatePicturesField_InitMiniGame_mFF70398119817B92C8F84A0EB2FBA11C5356C66D ();
// 0x0000050F System.Void RotatePicturesField::Reset()
extern void RotatePicturesField_Reset_m6E0AAF527E7B979DA46639277DEB3F052B1AE209 ();
// 0x00000510 System.Void RotatePicturesField::Init(MiniGamePreset)
extern void RotatePicturesField_Init_m15C887AC55D604B41717E98CD239CE009BB92F49 ();
// 0x00000511 System.Void RotatePicturesField::InitCharacters(RotatePicturesPreset)
extern void RotatePicturesField_InitCharacters_mDE9914340B406EF211F4FF2A2C7CD0C704A2E3AE ();
// 0x00000512 UnityEngine.Vector3 RotatePicturesField::GetCeilPosition(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern void RotatePicturesField_GetCeilPosition_mFE482B20BADA0732A1DC5BD34B46E1EB9A962293 ();
// 0x00000513 System.Void RotatePicturesField::InitSettings(System.Int32)
extern void RotatePicturesField_InitSettings_m6A30F8DB001EEB9B41B41C7C2F3211213A2C6D19 ();
// 0x00000514 System.Void RotatePicturesField::.ctor()
extern void RotatePicturesField__ctor_m5F5CA040EC9805B2754669A4BE0593D2E6A9A586 ();
// 0x00000515 System.Void RotatePicturesMiniGame::CheckGameState()
extern void RotatePicturesMiniGame_CheckGameState_mC8A1E199893BD17C290C66542B796ABD80A1BD72 ();
// 0x00000516 System.Void RotatePicturesMiniGame::InitGame(MiniGameData,System.Int32,UnityEngine.Vector2)
extern void RotatePicturesMiniGame_InitGame_m2AE4AD856DFFB2674FAEC0F4C33E862ED6998A9B ();
// 0x00000517 System.Void RotatePicturesMiniGame::InitLevel(System.Int32)
extern void RotatePicturesMiniGame_InitLevel_mEC4ED164F7AA5175274FFC5E9085FD43119B623B ();
// 0x00000518 System.Void RotatePicturesMiniGame::InitLevel(RotatePicturesPreset)
extern void RotatePicturesMiniGame_InitLevel_mE11F98F251065C4C39341A6B5F8302F0313756A1 ();
// 0x00000519 System.Void RotatePicturesMiniGame::Reset()
extern void RotatePicturesMiniGame_Reset_m836BE583BA2AAEB08902567177EA74A548071757 ();
// 0x0000051A System.Void RotatePicturesMiniGame::RestartGame()
extern void RotatePicturesMiniGame_RestartGame_m22EEE478EAC8EB3A4E936FFC444486F285BE5FF0 ();
// 0x0000051B System.Void RotatePicturesMiniGame::.ctor()
extern void RotatePicturesMiniGame__ctor_m5837BE5699C53B2A0182B6EE350273E5AE33A889 ();
// 0x0000051C System.Void RotatePicturesMiniGame::<CheckGameState>b__3_0()
extern void RotatePicturesMiniGame_U3CCheckGameStateU3Eb__3_0_m1A1EA191872EBF507C3B0B936D04B266B839B280 ();
// 0x0000051D System.Void RotatePicturesPreset::.ctor()
extern void RotatePicturesPreset__ctor_m348714746CE961CBA41F8DD07335CFEDC111BF4A ();
// 0x0000051E System.Void CharacterPresetPosition::.ctor()
extern void CharacterPresetPosition__ctor_m85BDE4A914D20D6A44361FE91E5CEDA3DE56187E ();
// 0x0000051F System.Void StickersDragableElement::.ctor()
extern void StickersDragableElement__ctor_mA96E2710C814ED88F3391496DFFC9577D4C04614 ();
// 0x00000520 System.Boolean StickersElement::get_IsOpen()
extern void StickersElement_get_IsOpen_mA644FAB2E97BEF63E6A3A53C86BCFD88619FE15E ();
// 0x00000521 System.Void StickersElement::set_IsOpen(System.Boolean)
extern void StickersElement_set_IsOpen_mDA6C548DEAF950A1E9D97130FE7C3A30C4665319 ();
// 0x00000522 System.Boolean StickersElement::TryOpen(System.Int32)
extern void StickersElement_TryOpen_mB5B945067FC5BE44C12EFE4CC7B0033D4A92833D ();
// 0x00000523 System.Void StickersElement::Init(StickersMiniGame,System.Int32)
extern void StickersElement_Init_m92764F2C9BCED64ED119447FFF588C9DD075AB22 ();
// 0x00000524 System.Void StickersElement::DebugOpen()
extern void StickersElement_DebugOpen_mB612BAF31D2F18857368B95188B524C3303D1A08 ();
// 0x00000525 System.Void StickersElement::DebugInit()
extern void StickersElement_DebugInit_mA9BD8F8D36E94FF7F4DC14D9CBC00A67BA7909D7 ();
// 0x00000526 System.Void StickersElement::.ctor()
extern void StickersElement__ctor_m98BD06CB56D5782B733121406F057D6611251FEB ();
// 0x00000527 System.Void StickersElementsList::Reset()
extern void StickersElementsList_Reset_m2AAFA1DF0D79D17C0FEDCF2615B8EEE244017F41 ();
// 0x00000528 System.Void StickersElementsList::Init(MiniGamePreset)
extern void StickersElementsList_Init_m39A4C738D3E6799F59568060A4A00CF8B9317906 ();
// 0x00000529 System.Void StickersElementsList::Shuffle()
extern void StickersElementsList_Shuffle_m2C467A4662239F87FC7002EAEB8A3199552C0C2F ();
// 0x0000052A System.Void StickersElementsList::ItemSelected()
extern void StickersElementsList_ItemSelected_m239E82D7863142BE7BBBD17C6FFE573BF83584DD ();
// 0x0000052B System.Void StickersElementsList::ItemUnselect()
extern void StickersElementsList_ItemUnselect_m0F107154A910CE28E4D86D9B98187FB0C3EDC5F7 ();
// 0x0000052C System.Void StickersElementsList::ResetoreElement(StickersListElement)
extern void StickersElementsList_ResetoreElement_m4520764B8D1B4A1315B1D9779E256A41D1988938 ();
// 0x0000052D System.Void StickersElementsList::ResetoreElement(System.Int32)
extern void StickersElementsList_ResetoreElement_mFE53702FBE8090E44DBF02E7A8A95131FC36F789 ();
// 0x0000052E System.Void StickersElementsList::.ctor()
extern void StickersElementsList__ctor_mE28A0EC89FCAE56FFFDE8FF05523A772397719B3 ();
// 0x0000052F System.Void StickersField::Init(StickersMiniGame)
extern void StickersField_Init_m91D8632E726B847E321DDE54E441605C9AB4F44A ();
// 0x00000530 System.Void StickersField::InitAnchors()
extern void StickersField_InitAnchors_m447AB784B9B978CFA8E40208D2155AEE8EC04641 ();
// 0x00000531 System.Void StickersField::.ctor()
extern void StickersField__ctor_mADBE3872C35F2006530D8E4D72CB71958B8DE016 ();
// 0x00000532 System.Void StickersListElement::Init(StickersMiniGame,System.Int32,UnityEngine.Sprite,System.Boolean)
extern void StickersListElement_Init_mF2546B578EB50889EDD38C720D279A1A024636C8 ();
// 0x00000533 System.Void StickersListElement::Click()
extern void StickersListElement_Click_mF10F15AAD2B7904A9CF6AC40A1675E1104036AEE ();
// 0x00000534 System.Void StickersListElement::MoveToBottom()
extern void StickersListElement_MoveToBottom_m731B04EB2F9266B572DB5BD6B8CFC368D2B8F49D ();
// 0x00000535 System.Void StickersListElement::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void StickersListElement_OnPointerDown_mD4D8A5D4031CBE45D17F4AF5422A1ED073B05D78 ();
// 0x00000536 System.Void StickersListElement::.ctor()
extern void StickersListElement__ctor_mBD1DE945B2DA4CB86115F3D9BA66F403D6A83031 ();
// 0x00000537 System.Void StickersMiniGame::Start()
extern void StickersMiniGame_Start_m01DC39389EFCC6659E2EBA0F8FBDB6552D40D118 ();
// 0x00000538 System.Void StickersMiniGame::InitGame(MiniGameData,System.Int32,UnityEngine.Vector2)
extern void StickersMiniGame_InitGame_m52921DCCD1E939EC44005F758AB19246AB7AA59E ();
// 0x00000539 System.Void StickersMiniGame::InitLevel(System.Int32)
extern void StickersMiniGame_InitLevel_m54A4CECE797B7FCC49EE9BAD67D736CA0ABD3C01 ();
// 0x0000053A System.Void StickersMiniGame::InitLevel(StickersPreset)
extern void StickersMiniGame_InitLevel_m834D37D0B8C501BEFDFE3476E47EAB6A9D9C7F75 ();
// 0x0000053B System.Void StickersMiniGame::CheckGameState()
extern void StickersMiniGame_CheckGameState_m5F810C2001744D84E058C58519387A8EC1CEE542 ();
// 0x0000053C System.Void StickersMiniGame::FindedPuzzle()
extern void StickersMiniGame_FindedPuzzle_mA544EDAB5A65461E3CC3759F0D05AA0B88DF53DF ();
// 0x0000053D System.Void StickersMiniGame::SelectElement(System.Int32,System.Boolean)
extern void StickersMiniGame_SelectElement_mA0EE99250D2636B71D8BB7F778088367F1357737 ();
// 0x0000053E System.Void StickersMiniGame::Update()
extern void StickersMiniGame_Update_mF7E1B4B56E333372576D412558A5BD6216FC84B1 ();
// 0x0000053F System.Void StickersMiniGame::TouchUp(UnityEngine.Vector3)
extern void StickersMiniGame_TouchUp_m0E6E66A9F38DA5E1D141513BFB25BF8A87BF2B76 ();
// 0x00000540 System.Void StickersMiniGame::Reset()
extern void StickersMiniGame_Reset_m78AC47BD41C778CE6BF40CADCC75DED25344F051 ();
// 0x00000541 System.Void StickersMiniGame::RestartGame()
extern void StickersMiniGame_RestartGame_m7E7CAC1FBB4151B97EB812925CF0EFD61EA7B95A ();
// 0x00000542 System.Void StickersMiniGame::.ctor()
extern void StickersMiniGame__ctor_m61C910FBE21D14F9AAAB27A7985296986586184E ();
// 0x00000543 System.Void StickersPreset::.ctor()
extern void StickersPreset__ctor_m9332A6A03A5C5E09D18EE564AD9AFE27B3B91917 ();
// 0x00000544 System.Boolean NativeAdsController::get_IsShowedNative()
extern void NativeAdsController_get_IsShowedNative_m5FCE899AAFA75FA76DDBEB37B230138E066DA53D ();
// 0x00000545 System.Void NativeAdsController::set_IsShowedNative(System.Boolean)
extern void NativeAdsController_set_IsShowedNative_m19FCE37F450163E36A45431A0208043E14DCB9E6 ();
// 0x00000546 System.Void NativeAdsController::Start()
extern void NativeAdsController_Start_mD4C8522CDB79D737D270F4192BAE4EFE376BC17F ();
// 0x00000547 System.Void NativeAdsController::ShowNative()
extern void NativeAdsController_ShowNative_m9DD1A0AC4A890D1DB5E01C4C8ECF19CF1219D1FA ();
// 0x00000548 System.Void NativeAdsController::HideNative()
extern void NativeAdsController_HideNative_mFDAEDEDA067288E7D777B6ED75B359A9000A18DF ();
// 0x00000549 System.Void NativeAdsController::.ctor()
extern void NativeAdsController__ctor_m5CF03B296BA7B6B59C32B1B9FCD1DDDF9E0FC441 ();
// 0x0000054A System.Void PlayerActivityController::FixedUpdate()
extern void PlayerActivityController_FixedUpdate_m27347879F7EACDFAD5897AEF07E62DE5597E7847 ();
// 0x0000054B System.Void PlayerActivityController::.ctor()
extern void PlayerActivityController__ctor_m7CD3FCF4B9618F4CE3A540B374EC9B928C93B111 ();
// 0x0000054C GameSettingsData GameSettings::get_Data()
extern void GameSettings_get_Data_m3DD3435C94F29A159BA6CF7D53BE1C58C9401ACF ();
// 0x0000054D ServiceData GameSettings::get_ServiceData()
extern void GameSettings_get_ServiceData_m6E5E7F6BDB79D8C115F14ECEA7A83DB43121F45A ();
// 0x0000054E UIScreenController GameSettings::get_UI()
extern void GameSettings_get_UI_m32B6364E127C138CB50478CF86DC6080A4835BEF ();
// 0x0000054F System.Void GameSettings::Awake()
extern void GameSettings_Awake_m9808AD61E6CC8098228E5D4D5BBB40067FB5C048 ();
// 0x00000550 System.Void GameSettings::.ctor()
extern void GameSettings__ctor_m01D23515D458DD3A0BBD6CD02F9AA5418AAE8DAF ();
// 0x00000551 InitableSettings[] GameSettingsData::get_InitableSettings()
extern void GameSettingsData_get_InitableSettings_mE5AE769E3DEBC0041EEB10DEE007318CF2E821E7 ();
// 0x00000552 System.Void GameSettingsData::.ctor()
extern void GameSettingsData__ctor_mD80D7677A8C336E63461267E01C88B46604F7FB7 ();
// 0x00000553 System.Void RemoteSettingsCallbacksInitializer::InitCallbacks()
extern void RemoteSettingsCallbacksInitializer_InitCallbacks_m4E5DFAF80E88CF41DC4779E1E1E892CD4FB9AE4C ();
// 0x00000554 System.Void RemoteSettingsCallbacksInitializer::UpdateConfigVersion()
extern void RemoteSettingsCallbacksInitializer_UpdateConfigVersion_mFB09ACE7F31C3E23F03EF2769A6FE58207BF0362 ();
// 0x00000555 System.Void RemoteSettingsCallbacksInitializer::Updat(System.String)
extern void RemoteSettingsCallbacksInitializer_Updat_m1CB5D4B3B13FC2FE9EFCFB76CC9987D0E7097D48 ();
// 0x00000556 System.Collections.IEnumerator RemoteSettingsCallbacksInitializer::WaitEndOfFrame(System.Action)
extern void RemoteSettingsCallbacksInitializer_WaitEndOfFrame_m9AFDD481B04AC211B3A5938D38A273FCECF534ED ();
// 0x00000557 System.Void RemoteSettingsCallbacksInitializer::.ctor()
extern void RemoteSettingsCallbacksInitializer__ctor_m0E1BEF33C1C395A00FCF7042DDD1775EA1704F35 ();
// 0x00000558 System.Void InitableSettings::Init()
// 0x00000559 System.Void InitableSettings::.ctor()
extern void InitableSettings__ctor_m391A42B42D211528D7F49D87B8D7BF0E06406A93 ();
// 0x0000055A System.Void DebugSettings::Init()
extern void DebugSettings_Init_mA0097759B4D0F80B35F0FB23F7B954F614602ED6 ();
// 0x0000055B System.Void DebugSettings::.ctor()
extern void DebugSettings__ctor_mF032F7D6B6837B546A3DF276663AA4FCA37A9D50 ();
// 0x0000055C System.Void GamePlaySettings::Init()
extern void GamePlaySettings_Init_m64DE8C3FEC95FDCCC681F6DE6AF5D9C19A636626 ();
// 0x0000055D System.Void GamePlaySettings::.ctor()
extern void GamePlaySettings__ctor_mB8632AA5A01FB47960FD319F93565F83D240B963 ();
// 0x0000055E System.Single CameraClippingPerformance::get_DeltaClippingDistance()
extern void CameraClippingPerformance_get_DeltaClippingDistance_mD77D70A56FA5BCCCE41F90D75EACCC294F9F3616 ();
// 0x0000055F System.Void CameraClippingPerformance::Start()
extern void CameraClippingPerformance_Start_m92DEE5F109CE0F1751644ECBD04638CB75B6DEBC ();
// 0x00000560 System.Void CameraClippingPerformance::SetDistance()
extern void CameraClippingPerformance_SetDistance_m5AFAC9123FC15F6A5E0A98CC87F4C7BDE584546A ();
// 0x00000561 System.Void CameraClippingPerformance::.ctor()
extern void CameraClippingPerformance__ctor_m7C270BEAA24F194747014A277891E73775B30C6D ();
// 0x00000562 System.Void DirectionLightPerformance::Start()
extern void DirectionLightPerformance_Start_mDDF130714F50E988AC5102F3775DBD701136CAEC ();
// 0x00000563 System.Void DirectionLightPerformance::UpdateQuality()
extern void DirectionLightPerformance_UpdateQuality_mF9093FF8FBEB1F914735A3A1405123B22225F3AC ();
// 0x00000564 System.Void DirectionLightPerformance::.ctor()
extern void DirectionLightPerformance__ctor_m6044B42DE918A4C4CC3434D62E495772F176F4C0 ();
// 0x00000565 System.Int32 PerfomanceSettings::get_TargetFPS()
extern void PerfomanceSettings_get_TargetFPS_mD2A9A10E4267753F7DC742E5C26673151FBA0CB9 ();
// 0x00000566 System.Int32 PerfomanceSettings::get_MinFPS()
extern void PerfomanceSettings_get_MinFPS_mABEC4E177FBB7E67BA2BC9CD09DEF50A9846901F ();
// 0x00000567 System.Single PerfomanceSettings::get_CurrentResolutionScale()
extern void PerfomanceSettings_get_CurrentResolutionScale_mA780F8339791094E4B0BAED3D6FA8E54897ED500 ();
// 0x00000568 System.Void PerfomanceSettings::Init()
extern void PerfomanceSettings_Init_m131D24EDC3AAE190581DA7C8D417F6BEE47B20BE ();
// 0x00000569 System.Void PerfomanceSettings::DecreeseResolution()
extern void PerfomanceSettings_DecreeseResolution_mD138F3F93C7E1612CFA3CE9ACD4301773A29F320 ();
// 0x0000056A System.Void PerfomanceSettings::SetResolutionScale(System.Single)
extern void PerfomanceSettings_SetResolutionScale_m4287BD355EAE84E8A8D0F6A50D1020D128800AD9 ();
// 0x0000056B PerformancePriorityInstructions PerfomanceSettings::GetPriority(System.Int32)
extern void PerfomanceSettings_GetPriority_mFB3AF5D948A8C3CFBE644BAE5825E56CD30F2EB4 ();
// 0x0000056C System.Void PerfomanceSettings::.ctor()
extern void PerfomanceSettings__ctor_m697A96205237C8C96037011FA5A1052EBC7A16E2 ();
// 0x0000056D System.Void PerformancePriorityInstructions::.ctor()
extern void PerformancePriorityInstructions__ctor_mB2C605B5778464A0F0D928F41664665AFBAD4D5D ();
// 0x0000056E System.Boolean PerformanceController::get_EnabledPerformanceController()
extern void PerformanceController_get_EnabledPerformanceController_m08BB0A1D84CF06AF45CB8FFAA0088A3980AE10C6 ();
// 0x0000056F System.Void PerformanceController::set_EnabledPerformanceController(System.Boolean)
extern void PerformanceController_set_EnabledPerformanceController_m5C85B1AC81BD207E90B7CD7315D8495AF3BC2CBF ();
// 0x00000570 System.Void PerformanceController::Start()
extern void PerformanceController_Start_m17F87E05ED27C9FD624EAE4A6CE4006015D41DA9 ();
// 0x00000571 System.Void PerformanceController::StartController()
extern void PerformanceController_StartController_m76FA3EF4452531270BEAD47EA6335221787787AF ();
// 0x00000572 System.Void PerformanceController::StopController()
extern void PerformanceController_StopController_m01752451638F30F8DD680CDA7EFA72F97C828D7E ();
// 0x00000573 System.Collections.IEnumerator PerformanceController::PerformanceControlRoutine()
extern void PerformanceController_PerformanceControlRoutine_m0BC775CAAFBEDB88D6834F3BCF2E2C749B174368 ();
// 0x00000574 System.Void PerformanceController::StepForUpPerformance(PerformancePriorityInstructions)
extern void PerformanceController_StepForUpPerformance_mD52FEEF99B073341736D3B5F7EB20F355BD03576 ();
// 0x00000575 System.Void PerformanceController::NextPerformanceStep()
extern void PerformanceController_NextPerformanceStep_mF19B2E02CDA6E94E07967798527499D8812A761D ();
// 0x00000576 System.Void PerformanceController::ResolutionScale()
extern void PerformanceController_ResolutionScale_m3B75481F52DBA5382AC2AF8447DB9BAC6527F566 ();
// 0x00000577 System.Void PerformanceController::ShadowsQuality()
extern void PerformanceController_ShadowsQuality_m27DF7B43FDD8CCE36F1C50D78E3280B5FB915979 ();
// 0x00000578 System.Void PerformanceController::ClippingPlaneDistance()
extern void PerformanceController_ClippingPlaneDistance_mBBDCD0AFA3F4E8B0C759697DA92020945AE27E2B ();
// 0x00000579 System.Void PerformanceController::.ctor()
extern void PerformanceController__ctor_m06E47BDABFCB2AA4C7F8D90A348C5E95FF3FFE06 ();
// 0x0000057A System.String RemoteSettings::get_ProductsAdress()
extern void RemoteSettings_get_ProductsAdress_m7D753A76B0BA1498A61353D00871032B47C8BE52 ();
// 0x0000057B System.String RemoteSettings::get_AlbumsAdress()
extern void RemoteSettings_get_AlbumsAdress_mB165CDB81C304428D46A3D2B8A6846F8C7F2EEF5 ();
// 0x0000057C System.Void RemoteSettings::Init()
extern void RemoteSettings_Init_m6CCBA2305AFD237CFB4A9585852FCB41696623B7 ();
// 0x0000057D System.Void RemoteSettings::.ctor()
extern void RemoteSettings__ctor_mD5221A1AC741ACD5616400987B54BC75975DA5D4 ();
// 0x0000057E System.Void UISettings::Init()
extern void UISettings_Init_m6971E9379602F21C3C630A475853AD96CC0B705E ();
// 0x0000057F System.Void UISettings::.ctor()
extern void UISettings__ctor_m02AC0D0225C14350248CAF0FA4EB07BFDFB43D19 ();
// 0x00000580 System.Boolean AbstractScreen::get_IsShow()
extern void AbstractScreen_get_IsShow_m92E88A49BAEC447CF66ADFC8882FE1922A34E372 ();
// 0x00000581 System.Void AbstractScreen::Awake()
extern void AbstractScreen_Awake_m8B61461FE6C8B334BF081E61A30588B448A9B5C8 ();
// 0x00000582 System.Void AbstractScreen::UIAwake()
extern void AbstractScreen_UIAwake_mBC57B452BFAAC07402686E36CB3A4849784EAA1C ();
// 0x00000583 System.Void AbstractScreen::Start()
extern void AbstractScreen_Start_mC223D37C1F6D15FC8FFF2897A0E48CB1B61BE93C ();
// 0x00000584 System.Void AbstractScreen::UIStart()
extern void AbstractScreen_UIStart_m8FD85444A56EFCD469AA6E9C83843DB62A7618BF ();
// 0x00000585 System.Void AbstractScreen::Show()
// 0x00000586 System.Void AbstractScreen::Hide()
// 0x00000587 System.Void AbstractScreen::InitButtons()
extern void AbstractScreen_InitButtons_m8D04EF4F7B52910556EC7A21768B77AA605D9D88 ();
// 0x00000588 System.Void AbstractScreen::.ctor()
extern void AbstractScreen__ctor_mCACCEE2F5869CAB7A748BC9ED5181E16A3E41FDA ();
// 0x00000589 System.Void AbstractUIScreen::Awake()
extern void AbstractUIScreen_Awake_m0A4AB13784B8937670045771EEC6E56F4CF93F1A ();
// 0x0000058A System.Void AbstractUIScreen::Init()
extern void AbstractUIScreen_Init_m508314953ED8AA851252A5304318BFEF85458D09 ();
// 0x0000058B System.Void AbstractUIScreen::FindAllPages()
extern void AbstractUIScreen_FindAllPages_m2F1451FB2A5A8343FC22C79DBD27202592E3D9F4 ();
// 0x0000058C CachedTransitionScreen AbstractUIScreen::GetTransitionScreen(System.Type)
extern void AbstractUIScreen_GetTransitionScreen_m6A25E69C48653D467316904F60475CD4026FCFD9 ();
// 0x0000058D T AbstractUIScreen::GetScreen()
// 0x0000058E System.Void AbstractUIScreen::Show()
// 0x0000058F System.Void AbstractUIScreen::Hide()
// 0x00000590 System.Void AbstractUIScreen::.ctor()
extern void AbstractUIScreen__ctor_m9C70A32F8CED09CAAAFB62FCFB9BC89459D5066B ();
// 0x00000591 CachedTransitionScreen AbstractUIScreenController::get_CurrentScreen()
extern void AbstractUIScreenController_get_CurrentScreen_m30680D995DB68112CFD191C148DE97455ECDF595 ();
// 0x00000592 System.Void AbstractUIScreenController::set_CurrentScreen(CachedTransitionScreen)
extern void AbstractUIScreenController_set_CurrentScreen_mBAB36560B1E293CAFA640A08BF4631CB50BB419A ();
// 0x00000593 T AbstractUIScreenController::Screen()
// 0x00000594 System.Void AbstractUIScreenController::.ctor()
extern void AbstractUIScreenController__ctor_m362B44D1AAC9657AE350000261C2F833743CEFD7 ();
// 0x00000595 System.Void AnimatedScreen::Hide()
extern void AnimatedScreen_Hide_m0A5F6AA19CF548011390CC23DF3911182D1C1350 ();
// 0x00000596 System.Void AnimatedScreen::Show()
extern void AnimatedScreen_Show_m466070CA4336905EE9435A521BD1DBDEE4A9B63F ();
// 0x00000597 System.Void AnimatedScreen::UIAwake()
extern void AnimatedScreen_UIAwake_mF252AB2792A7FFA16DCF31D2586FD3571CA1D7B3 ();
// 0x00000598 System.Void AnimatedScreen::InitAnimatorParameters()
extern void AnimatedScreen_InitAnimatorParameters_m40158E0A7D8536A39CAB72827D0B9B3835AE4745 ();
// 0x00000599 System.Void AnimatedScreen::.ctor()
extern void AnimatedScreen__ctor_m6542A85D76EDE01B70EA6F3F76F6E776F6730B09 ();
// 0x0000059A System.Void AnimatedScreen::<Hide>b__3_0()
extern void AnimatedScreen_U3CHideU3Eb__3_0_m3C7BA0ACC50F176C16399CFCC0D278512F2AFB36 ();
// 0x0000059B System.Void CachedTransitionScreen::UIAwake()
extern void CachedTransitionScreen_UIAwake_m6F8EB57E442308F55724B0AB869C320390483B0D ();
// 0x0000059C System.Void CachedTransitionScreen::Show(CachedTransitionScreen)
extern void CachedTransitionScreen_Show_mBE5A7D66E3D400F5D3CFA55BBAF868938FE88673 ();
// 0x0000059D System.Void CachedTransitionScreen::ShowRule(CachedTransitionScreen)
// 0x0000059E System.Void CachedTransitionScreen::BackRule()
extern void CachedTransitionScreen_BackRule_mE9CA4A4463C8D5DD55DD396DE0B09F64417CF45A ();
// 0x0000059F System.Void CachedTransitionScreen::Back()
extern void CachedTransitionScreen_Back_m40B2093331738A2C041875A764006CF725B2CDA9 ();
// 0x000005A0 System.Void CachedTransitionScreen::BecomeCurrentScreen()
extern void CachedTransitionScreen_BecomeCurrentScreen_m4DC90FB9D4FF5A2CEF12FEE87C15F93FE0685C42 ();
// 0x000005A1 System.Void CachedTransitionScreen::.ctor()
extern void CachedTransitionScreen__ctor_m6EA1C82D42236CAF771E035CA5D84F0115D7A5AA ();
// 0x000005A2 UnityEngine.RectTransform ButtonAndAction::get_Rect()
extern void ButtonAndAction_get_Rect_m2EE10F62B37EB1ADA17BFB1FEF8C83149EE3439D ();
// 0x000005A3 System.Void ButtonAndAction::RegisterAction(System.Action)
extern void ButtonAndAction_RegisterAction_m9129D456790373AC1918A2CCAD3786159F4E7786 ();
// 0x000005A4 System.Void ButtonAndAction::UnregisterAction()
extern void ButtonAndAction_UnregisterAction_m769C484DEA53A6A0DEFF2D5CAB6C19E71FD8F5EE ();
// 0x000005A5 System.Void ButtonAndAction::ClickAction()
extern void ButtonAndAction_ClickAction_m4FAB35779E39DFD7BEB2666CCB12504D19545A60 ();
// 0x000005A6 System.Void ButtonAndAction::.ctor()
extern void ButtonAndAction__ctor_m07CE44EA88D2F5BD6F9ECC819559CD24B9C44B61 ();
// 0x000005A7 System.Void GridResizer::Start()
extern void GridResizer_Start_m0E3286DA02B7E723593D39786AB8C1DCCE7BB00C ();
// 0x000005A8 System.Void GridResizer::Resize(System.Boolean)
extern void GridResizer_Resize_m2E62C86B4E29EC3F60FEDE47F3E85D0FAF735044 ();
// 0x000005A9 System.Void GridResizer::SetGridSize(UnityEngine.Vector2)
extern void GridResizer_SetGridSize_m174EEBF66266239DDDA507FD268098FED168FA96 ();
// 0x000005AA System.Void GridResizer::SetCeilsSize(UnityEngine.Vector2,System.Int32)
extern void GridResizer_SetCeilsSize_m3DDB19CBDF16415F7413EEBEB7E1A41408C16104 ();
// 0x000005AB System.Void GridResizer::.ctor()
extern void GridResizer__ctor_m0C246A42BF2614F44DB0DA4DBD8089B06405ECE7 ();
// 0x000005AC System.Void IScreen::Show()
// 0x000005AD System.Void IScreen::Hide()
// 0x000005AE System.Void ProgressController::Start()
extern void ProgressController_Start_m55EB43E49082303F5B9B646DF9340C7A6661D9E6 ();
// 0x000005AF System.Void ProgressController::Reset()
extern void ProgressController_Reset_m8D7E35C6EBB4C50611E95F9B4BCC0765DBE67E14 ();
// 0x000005B0 System.Void ProgressController::MyReset()
extern void ProgressController_MyReset_m04CA8C07F8A265B1928E25187E976F18EDC802F9 ();
// 0x000005B1 System.Void ProgressController::UpdatePercents(System.Single)
extern void ProgressController_UpdatePercents_mF52A987C5A6AACC6F82AE5EBB291C97F435EDAED ();
// 0x000005B2 System.Void ProgressController::MoveArrow(System.Single)
extern void ProgressController_MoveArrow_m9CE49A405B0ED1D258183B35675F3AD762D30690 ();
// 0x000005B3 System.Void ProgressController::SetPercents(System.Single)
extern void ProgressController_SetPercents_m52C321A9128F795F8F2EF34F9B45E9C8FC4611E1 ();
// 0x000005B4 System.Void ProgressController::Update()
extern void ProgressController_Update_mE9A54B5A3FB460358CC790BDA65F3561930B8B4F ();
// 0x000005B5 System.Void ProgressController::.ctor()
extern void ProgressController__ctor_mF2BF60EBF6C3888D0D0848E03E02769B899BEAE2 ();
// 0x000005B6 System.Void ResizeController::Start()
extern void ResizeController_Start_m985853BE8C1677830F9A2A0CB4C4902A1A24E750 ();
// 0x000005B7 System.Void ResizeController::Resize()
extern void ResizeController_Resize_m8E98468BD64A34A311A4A250CF2FE87F82DEF7AF ();
// 0x000005B8 System.Void ResizeController::Save()
extern void ResizeController_Save_mCAAE9B23C69B1B34FC45EC9C74B888E9E9BAE1B2 ();
// 0x000005B9 System.Void ResizeController::.ctor()
extern void ResizeController__ctor_m1CE0A9D81C38E053DAA6D61B526DE684582D2A55 ();
// 0x000005BA System.Void ResolutionSettings::Resize()
extern void ResolutionSettings_Resize_m22E8A43A276DECE9E90FBC3E445DF48260B3D8A6 ();
// 0x000005BB System.Void ResolutionSettings::Save()
extern void ResolutionSettings_Save_m906A8E2CF8B9C69318647CCBF856F3F44B62D06D ();
// 0x000005BC System.Single ResolutionSettings::get_ScreenCoeff()
extern void ResolutionSettings_get_ScreenCoeff_mB419912325DBD299B022B9676A1D4C51149FCC86 ();
// 0x000005BD System.Boolean ResolutionSettings::get_IsScreenSize()
extern void ResolutionSettings_get_IsScreenSize_m8241F0A04E53D9A299722615EC1D8685BEE293F5 ();
// 0x000005BE UnityEngine.Vector2 ResolutionSettings::get_SystemSize()
extern void ResolutionSettings_get_SystemSize_m0E40214E225476807BEF249638CA25D5EDC154DE ();
// 0x000005BF UnityEngine.Vector2 ResolutionSettings::get_CurrentSize()
extern void ResolutionSettings_get_CurrentSize_m13A4667BE230222B3B057BB23CE13FBD15922E43 ();
// 0x000005C0 System.Void ResolutionSettings::.ctor()
extern void ResolutionSettings__ctor_m31FB973C6E73B89A8449DF9F5C83FA531C1C5544 ();
// 0x000005C1 System.Void ObjectForResize::Resize(System.Single)
extern void ObjectForResize_Resize_m4ED8227ED7C7D553D9DF6EE31826E2913F8F8107 ();
// 0x000005C2 System.Void ObjectForResize::Save(System.Single)
extern void ObjectForResize_Save_mB2622CDCB003093701F6B66FABA4A0C945791178 ();
// 0x000005C3 System.Void ObjectForResize::.ctor()
extern void ObjectForResize__ctor_mB632B47E2A231A5251C1F9EEE469D2C40E617DEE ();
// 0x000005C4 System.Void GridForResize::Resize(System.Single)
extern void GridForResize_Resize_mDF94C7791AD18A3FB81B45E56E516DF55A20AED2 ();
// 0x000005C5 System.Void GridForResize::Save(System.Single)
extern void GridForResize_Save_m4B7BF56BA2E577D335F73862B8518C790BD719EA ();
// 0x000005C6 System.Void GridForResize::.ctor()
extern void GridForResize__ctor_m531CA6FC8110687EA08413778498CE1292B3CB5E ();
// 0x000005C7 System.Void AboutScreen::InitButtons()
extern void AboutScreen_InitButtons_m5B659544CFF560DB87DB9CEECFEAB46D4BF41279 ();
// 0x000005C8 System.Void AboutScreen::ShowRule(CachedTransitionScreen)
extern void AboutScreen_ShowRule_m878379B24F5B7EF2687B80CC8ED71AA8E2F2B3BE ();
// 0x000005C9 System.Void AboutScreen::.ctor()
extern void AboutScreen__ctor_m32EB273FD90A18E625A9785602C8735DEDD2DCE4 ();
// 0x000005CA System.String AppleAlbumPreview::get_affiliateToken()
extern void AppleAlbumPreview_get_affiliateToken_mE34BECC115ED8A8C245C30998E7766542583A7FD ();
// 0x000005CB System.Boolean AppleAlbumPreview::get_Loading()
extern void AppleAlbumPreview_get_Loading_m48178171D20E6A069AD434E61A9634FF847A1824 ();
// 0x000005CC System.Void AppleAlbumPreview::set_Loading(System.Boolean)
extern void AppleAlbumPreview_set_Loading_mF6901497AEEA20C776C4162FF26C6E2E0D9E506F ();
// 0x000005CD System.Void AppleAlbumPreview::PlayClip(UnityEngine.AudioClip)
extern void AppleAlbumPreview_PlayClip_m1FE8B99F73E64D1F12D969C7A98D9D448D2F885F ();
// 0x000005CE System.Void AppleAlbumPreview::StopSongs()
extern void AppleAlbumPreview_StopSongs_mAA242A00B64CBC1B03FFFE686375C89FB6B0C2D1 ();
// 0x000005CF System.Void AppleAlbumPreview::UnloadAudio()
extern void AppleAlbumPreview_UnloadAudio_mF1F03A7FFCB109BCD36C1761922540C3D259B907 ();
// 0x000005D0 System.Void AppleAlbumPreview::Init(System.String)
extern void AppleAlbumPreview_Init_m208E7E6DDBBBB4DFE6B57B34B57156CA689B17C4 ();
// 0x000005D1 System.Void AppleAlbumPreview::RedirectToAppleMusic()
extern void AppleAlbumPreview_RedirectToAppleMusic_m9BE203B975666297B4F1C76E25E74A32F9E41E27 ();
// 0x000005D2 System.Void AppleAlbumPreview::LoadAlbumData()
extern void AppleAlbumPreview_LoadAlbumData_m24E4590D273885BAAD513F270A215409580D8BEC ();
// 0x000005D3 System.Void AppleAlbumPreview::InitAlbumData(SimpleJSON.JSONNode)
extern void AppleAlbumPreview_InitAlbumData_mA8436F73B77D27C01E6C109BB02B96D4BA9A3EEE ();
// 0x000005D4 System.Void AppleAlbumPreview::InitSongData(SimpleJSON.JSONNode)
extern void AppleAlbumPreview_InitSongData_m3B0DA64C2961F06DD1972A9E4EBEC5500FA96204 ();
// 0x000005D5 System.Collections.IEnumerator AppleAlbumPreview::LoadAlbumDataRoutine(System.String)
extern void AppleAlbumPreview_LoadAlbumDataRoutine_m1E2ECC165E61E9C023F9B04398C4301AB12BEB69 ();
// 0x000005D6 System.Void AppleAlbumPreview::.ctor()
extern void AppleAlbumPreview__ctor_m728E15E22215CA38BE6E68F1F29C8C60222BCCBC ();
// 0x000005D7 System.Void AppleAlbumPreview::<Init>b__30_0()
extern void AppleAlbumPreview_U3CInitU3Eb__30_0_mEE5279F741B91AA9AD3654D22DE499ED97B5F70F ();
// 0x000005D8 AppleMusicAlbumsData AppleMusicAlbumsData::ParceFromJSON(System.String)
extern void AppleMusicAlbumsData_ParceFromJSON_mF4E2057A5AA40F6A6121336A261101FDCF5A3B09 ();
// 0x000005D9 System.Void AppleMusicAlbumsData::.ctor()
extern void AppleMusicAlbumsData__ctor_m035A58012AC8144778ECDD286269552B4ED23192 ();
// 0x000005DA System.Void AppleMusicAlbumsData::.cctor()
extern void AppleMusicAlbumsData__cctor_m3EE11843D0D5594B94EB3C264B372A03485A17E3 ();
// 0x000005DB System.Void AppleMusicScreen::InitButtons()
extern void AppleMusicScreen_InitButtons_mD7199705941BDDA3DA4DA106CCAE9D2E3E758DC7 ();
// 0x000005DC System.Void AppleMusicScreen::Show()
extern void AppleMusicScreen_Show_mDA29AD0DD8016A38EEFE2F2C5DDFD5128015CA59 ();
// 0x000005DD System.Void AppleMusicScreen::ShowRule(CachedTransitionScreen)
extern void AppleMusicScreen_ShowRule_mC0C5C1FA4AF23373971062A299C79D131051C2B7 ();
// 0x000005DE System.Void AppleMusicScreen::Hide()
extern void AppleMusicScreen_Hide_mE7D1C898C7636D5EB8802B5E5CBE4ABF6E58BABE ();
// 0x000005DF System.Void AppleMusicScreen::InitAlbumsLocal()
extern void AppleMusicScreen_InitAlbumsLocal_mAA2D482E7F7AE3E984FCC61BB6849428E0E5D4D5 ();
// 0x000005E0 System.Void AppleMusicScreen::InitAlbumsRemote(AppleMusicAlbumsData)
extern void AppleMusicScreen_InitAlbumsRemote_m16E8166AEFD50BA44524DA622A0086FE8258BA3D ();
// 0x000005E1 System.Void AppleMusicScreen::LoadAlbum(System.String)
extern void AppleMusicScreen_LoadAlbum_mBAD2D005A34CB794A9C56D66498CB61D86FF565B ();
// 0x000005E2 System.Void AppleMusicScreen::OpenTestScene()
extern void AppleMusicScreen_OpenTestScene_m1EB2303B19E91239BD784D2E7734CA1DD60C1C7A ();
// 0x000005E3 System.Void AppleMusicScreen::PauseAudio()
extern void AppleMusicScreen_PauseAudio_mFC0BD6959F1E6BFA8B93817D4302B6455435E48C ();
// 0x000005E4 System.Void AppleMusicScreen::UnloadAudio()
extern void AppleMusicScreen_UnloadAudio_m757D089CBBE883A1D9732ABB8BBDD9E696F43DA4 ();
// 0x000005E5 System.Collections.IEnumerator AppleMusicScreen::LoadAlbumsWWW()
extern void AppleMusicScreen_LoadAlbumsWWW_mE2A511065C9B5DF97B27BB21421D7C08209D516A ();
// 0x000005E6 System.Void AppleMusicScreen::.ctor()
extern void AppleMusicScreen__ctor_mCAE51A452D41D690497CF4E8AA4ACEF08B1E4614 ();
// 0x000005E7 System.String AppleMusicUtility::BuildAbumDataUrl(System.String,System.Boolean,System.Int32)
extern void AppleMusicUtility_BuildAbumDataUrl_m09388B4036A8029D533111CDA98B61F38B9F3CEC ();
// 0x000005E8 System.Void AppleMusicUtility::GoToAlbum(System.String,System.String)
extern void AppleMusicUtility_GoToAlbum_mFAA2C296010BD09D6669D39EB3AFB27D5D2FD17B ();
// 0x000005E9 System.Void AppleMusicUtility::.ctor()
extern void AppleMusicUtility__ctor_m38AADA1227762739166D210E940A372A31B90631 ();
// 0x000005EA System.Void AppleSongPanel::Init(SimpleJSON.JSONNode,AppleAlbumPreview)
extern void AppleSongPanel_Init_mFCCE82CF7C642C844BF505FC6B7F113BACE93F7D ();
// 0x000005EB System.Void AppleSongPanel::Click()
extern void AppleSongPanel_Click_m40D39CB9F338B7F109A12F85F5DB42A06B9A6E8C ();
// 0x000005EC System.Collections.IEnumerator AppleSongPanel::LoadClip(System.String)
extern void AppleSongPanel_LoadClip_mB91CEAF69B32A19EB70B0145BA31E1CC0B584C93 ();
// 0x000005ED System.Void AppleSongPanel::UnloadClip()
extern void AppleSongPanel_UnloadClip_m7CFAD8800CBCB508872CB4314C3E57127A7590C4 ();
// 0x000005EE System.Void AppleSongPanel::StopPlay()
extern void AppleSongPanel_StopPlay_m89FACA4871EAC02F321002C180BA44F2DE3F9E4A ();
// 0x000005EF System.Void AppleSongPanel::PlayClip()
extern void AppleSongPanel_PlayClip_m0AB6C754827AF79077E7BD3B3B9CF0B07802101E ();
// 0x000005F0 System.Void AppleSongPanel::.ctor()
extern void AppleSongPanel__ctor_mD0C726449C7304BBF1233C34439A365374D7EECF ();
// 0x000005F1 System.Void BookContentScreen::Reset()
extern void BookContentScreen_Reset_m391B661F3159A65FF644E6B00041035732F9CC47 ();
// 0x000005F2 System.Void BookContentScreen::InitButtons()
extern void BookContentScreen_InitButtons_m2836AC5A6E9A8748B56042529C57766D19A7839F ();
// 0x000005F3 System.Void BookContentScreen::ShowRule(CachedTransitionScreen)
extern void BookContentScreen_ShowRule_m047388095169354AF46B5D97B34CDF9A231A7E9D ();
// 0x000005F4 System.Void BookContentScreen::Show(Book)
extern void BookContentScreen_Show_m77C0BA7F8023B2F32D32259AAEA52385CCF69AF5 ();
// 0x000005F5 System.Void BookContentScreen::InitContent()
extern void BookContentScreen_InitContent_m01F0D421DF47DCD736042B3B080F73AE714648E2 ();
// 0x000005F6 System.Void BookContentScreen::.ctor()
extern void BookContentScreen__ctor_m61DFA4B8734EEC87C3991D8E34443E4FB2AD190E ();
// 0x000005F7 System.Void BookPagePreview::Start()
extern void BookPagePreview_Start_m3011D77050A03A8BC91F68E14C5BBD4941DA96F8 ();
// 0x000005F8 System.Void BookPagePreview::Init(Book,System.Int32)
extern void BookPagePreview_Init_m48293F3B7DCD3130A2006A14D3B5771E5BBED170 ();
// 0x000005F9 System.Void BookPagePreview::ReadPage()
extern void BookPagePreview_ReadPage_mCFE2AED56C1D4F214C113975D4A078990CF6B92B ();
// 0x000005FA System.Void BookPagePreview::.ctor()
extern void BookPagePreview__ctor_m1DAE7311F1A6B6878FFAEBCAB27A302CDBC25349 ();
// 0x000005FB System.Void BookPagesList::Reset()
extern void BookPagesList_Reset_mD4FD3F22A9F30989ED98DE8E8FCE6CCAACDC642C ();
// 0x000005FC System.Void BookPagesList::Init(Book)
extern void BookPagesList_Init_m2E6EE145F716699657672B00A61BA06C04003A04 ();
// 0x000005FD System.Void BookPagesList::.ctor()
extern void BookPagesList__ctor_m7E2F01746C1E643F8DE6E1A06DB4021D59EC9C32 ();
// 0x000005FE System.Void BookPreviewScreen::InitButtons()
extern void BookPreviewScreen_InitButtons_m75788D5D4499188EC35C319EDE30DA84C8E5996A ();
// 0x000005FF System.Void BookPreviewScreen::ShowRule(CachedTransitionScreen)
extern void BookPreviewScreen_ShowRule_mA40AAE0D740EBBFB4B6B5BE93FBDE4CD29AE30BE ();
// 0x00000600 System.Void BookPreviewScreen::Show(Book)
extern void BookPreviewScreen_Show_m5890CA7C6D3104B1F31027BB1E9E90474482B2BB ();
// 0x00000601 System.Void BookPreviewScreen::InitGames(Book)
extern void BookPreviewScreen_InitGames_m989A490B9CA935183AFA126FF1438B8378EB0E32 ();
// 0x00000602 System.Void BookPreviewScreen::ReadBook()
extern void BookPreviewScreen_ReadBook_m53494A0FA0539B95DE7FCC0A0551723A473525D6 ();
// 0x00000603 System.Void BookPreviewScreen::PlayGame(System.Int32)
extern void BookPreviewScreen_PlayGame_mF8074571D569F1123AF916E92486BBB52A14BB6A ();
// 0x00000604 System.Void BookPreviewScreen::.ctor()
extern void BookPreviewScreen__ctor_mEC694384CE230D3FA40532F5E04078DA97106AB0 ();
// 0x00000605 System.Void BookPreviewScreen::<InitButtons>b__7_0()
extern void BookPreviewScreen_U3CInitButtonsU3Eb__7_0_mD97BB31B984B8CA66248BE981812A9F78280DB42 ();
// 0x00000606 System.Void BookPreviewScreen::<InitButtons>b__7_1()
extern void BookPreviewScreen_U3CInitButtonsU3Eb__7_1_mBFC39D416AAE8DB8D5420BB9AC058A06A2CB8F56 ();
// 0x00000607 System.Void BookPreviewScreen::<InitButtons>b__7_2()
extern void BookPreviewScreen_U3CInitButtonsU3Eb__7_2_m455F384B2FE521D14058028DB8E2E8AE9948E510 ();
// 0x00000608 System.Void BookPreviewScreen::<InitButtons>b__7_3()
extern void BookPreviewScreen_U3CInitButtonsU3Eb__7_3_m5AFFB70FDC955DA3322844B2D16D815568D15555 ();
// 0x00000609 System.Void GamePreviewPanel::InitGame(MiniGameData)
extern void GamePreviewPanel_InitGame_mB6977834171667A954697D43A3A15F09727BA22C ();
// 0x0000060A System.Void GamePreviewPanel::.ctor()
extern void GamePreviewPanel__ctor_m49E34DEE69DA28501AB66D62EEB78C883077EBA4 ();
// 0x0000060B System.Void BookPageController::Reset()
extern void BookPageController_Reset_m2E727FF6BAC7BA817484DED14FEF9EEF45C53453 ();
// 0x0000060C System.Void BookPageController::Init(BookPage)
extern void BookPageController_Init_mEF922176FE14E7954302FBFAE44D89F3D1DCD234 ();
// 0x0000060D System.Void BookPageController::ClearText()
extern void BookPageController_ClearText_m7E996D8581213D656194D4DC4CFB52081761E50A ();
// 0x0000060E System.Void BookPageController::.ctor()
extern void BookPageController__ctor_mD7EEC0A26174A3961404FC0EF1AEA2E5C7ADE540 ();
// 0x0000060F Book BookScreen::get_CurrentBook()
extern void BookScreen_get_CurrentBook_mDDF3A25D782E2E9C8A31146F57DDC874BDCFE1C4 ();
// 0x00000610 System.Void BookScreen::Reset()
extern void BookScreen_Reset_m637BAD1BDA2AEA7B027A19E7FAEEEB826002F09A ();
// 0x00000611 System.Void BookScreen::InitButtons()
extern void BookScreen_InitButtons_m597B787C07287DC745AACA267D20FCF280AA3271 ();
// 0x00000612 System.Void BookScreen::ShowRule(CachedTransitionScreen)
extern void BookScreen_ShowRule_m26A2B295C551491842B00F726AA3D28C691FD940 ();
// 0x00000613 System.Void BookScreen::Hide()
extern void BookScreen_Hide_mBCE5A88A309666C9A6C4C8680F25A514BB1B2AA1 ();
// 0x00000614 System.Void BookScreen::Show(Book,System.Int32)
extern void BookScreen_Show_m02AB3B5D49F61D92DDF4245A384C338911C901F4 ();
// 0x00000615 System.Void BookScreen::InitContent()
extern void BookScreen_InitContent_m03645CB16B5C065166C3211FDF72B1E6B13F40BF ();
// 0x00000616 System.Void BookScreen::InitPagesPositions()
extern void BookScreen_InitPagesPositions_mD19929418FD5693CA5D1BDACEBAE3A5BC66EB196 ();
// 0x00000617 System.Void BookScreen::CheckButtonsState()
extern void BookScreen_CheckButtonsState_mCF5E0622C1CC17530EC6C70D53E6BCBD89F0801B ();
// 0x00000618 System.Void BookScreen::SwapLeft()
extern void BookScreen_SwapLeft_m80E31D87FB64469A0E3EBA19A81B9284ACB610B3 ();
// 0x00000619 System.Void BookScreen::SwapRight()
extern void BookScreen_SwapRight_mBD3074FA6A359232D71EBD848F6E5BA0BC70D78F ();
// 0x0000061A System.Collections.IEnumerator BookScreen::Swap()
extern void BookScreen_Swap_m87BF4DEB8D1698ECCF5F926ECB69CB3222CFB9EF ();
// 0x0000061B System.Void BookScreen::Update()
extern void BookScreen_Update_mC1664C65DBBDA9E69B5ED1051B4FC1FAC2A51A61 ();
// 0x0000061C System.Void BookScreen::SwipeProcess(System.Single)
extern void BookScreen_SwipeProcess_mA557731B625AD08E4ABCC86C6DBAA797BA98CA19 ();
// 0x0000061D System.Void BookScreen::SwipeStart()
extern void BookScreen_SwipeStart_m919883CB81D1A2D9C189ACBAC4838FFCB9365FA4 ();
// 0x0000061E System.Void BookScreen::SwipeStop()
extern void BookScreen_SwipeStop_m599C4EA07C2A85E3E57372B9595E9FA02668F7F6 ();
// 0x0000061F System.Void BookScreen::AnimationLerp(System.Single)
extern void BookScreen_AnimationLerp_mEDF6C655FE6871CB0E7A8262A9E8497D35F383B7 ();
// 0x00000620 System.Void BookScreen::SetButtonInteractable(System.Boolean)
extern void BookScreen_SetButtonInteractable_mC377F21CCFD2B0C851CE4BD4C1EE03A27458193B ();
// 0x00000621 System.Void BookScreen::.ctor()
extern void BookScreen__ctor_m262F09B2D32E61F725C9D8EE808583FAC6EB2DBC ();
// 0x00000622 System.Void ConfettiEffectScreen::ShowRule(CachedTransitionScreen)
extern void ConfettiEffectScreen_ShowRule_m49991BA3CE60C66C5AA42F1B9C751CCF782A6743 ();
// 0x00000623 System.Void ConfettiEffectScreen::Show()
extern void ConfettiEffectScreen_Show_m1274B6E20D9BE2397E2549165290F2595A209030 ();
// 0x00000624 System.Void ConfettiEffectScreen::Hide()
extern void ConfettiEffectScreen_Hide_mC040B28C7026918981D131CEB5651BED2A226F24 ();
// 0x00000625 System.Void ConfettiEffectScreen::.ctor()
extern void ConfettiEffectScreen__ctor_m658D9333446C79E9DA72DE05376C544B9257579A ();
// 0x00000626 System.Single GlobalMainScreen::get_DistanceToCenter()
extern void GlobalMainScreen_get_DistanceToCenter_mC69D6ABD5AC5E2EB4E74E7D6CF3E27C93FF8FBEA ();
// 0x00000627 System.Boolean GlobalMainScreen::get_ActiveScroll()
extern void GlobalMainScreen_get_ActiveScroll_m1781D40978F84920924034506AF82A2427FDE60E ();
// 0x00000628 System.Void GlobalMainScreen::set_ActiveScroll(System.Boolean)
extern void GlobalMainScreen_set_ActiveScroll_m9CE0B31C53223013EF6A61865614F38D580DF88D ();
// 0x00000629 System.Void GlobalMainScreen::ShowRule(CachedTransitionScreen)
extern void GlobalMainScreen_ShowRule_m282561979D8FC26C3CEEC25D74274FFD271B0865 ();
// 0x0000062A System.Void GlobalMainScreen::InitButtons()
extern void GlobalMainScreen_InitButtons_mFA620DD597231AC3C14324A3C7D66002D6236608 ();
// 0x0000062B System.Void GlobalMainScreen::CalculateAnchorPoints()
extern void GlobalMainScreen_CalculateAnchorPoints_m1AADED996E711DA6034727EE9507AAF1F5F3FDCE ();
// 0x0000062C System.Void GlobalMainScreen::SetScrollPosition(System.Single)
extern void GlobalMainScreen_SetScrollPosition_mF4D13F7FADA79D2A7CD96E25752C370FBEA06151 ();
// 0x0000062D System.Void GlobalMainScreen::ResetSenterPosition()
extern void GlobalMainScreen_ResetSenterPosition_m0C0D7E9904F8F72D5FD3A7144BCE1C3CB405989B ();
// 0x0000062E System.Void GlobalMainScreen::SwipeLerp(System.Single)
extern void GlobalMainScreen_SwipeLerp_mFF06E0D32E84FC4FE787EC252CAF57957FD144E4 ();
// 0x0000062F System.Void GlobalMainScreen::AutoSwipe(System.Single)
extern void GlobalMainScreen_AutoSwipe_mC294F1E41D76D9BFF5FFC442D8F930E807371F54 ();
// 0x00000630 System.Void GlobalMainScreen::SouvenirsClick()
extern void GlobalMainScreen_SouvenirsClick_mFEE8DABB3E11F62882115E4BE640A449B21B3AFA ();
// 0x00000631 System.Void GlobalMainScreen::ShopClick()
extern void GlobalMainScreen_ShopClick_m869334180E3C7376EFE700A1A3ADBF8B81F128F3 ();
// 0x00000632 System.Void GlobalMainScreen::OnBeginDrag()
extern void GlobalMainScreen_OnBeginDrag_mEC914F613246B01D844461BC507D64F5F35306D2 ();
// 0x00000633 System.Void GlobalMainScreen::Drag()
extern void GlobalMainScreen_Drag_m71C47D72E1826721AA3019E8D563F5F8A1FA5D68 ();
// 0x00000634 System.Void GlobalMainScreen::SwipeAction(UnityEngine.Vector2)
extern void GlobalMainScreen_SwipeAction_mA0CAE525D4766162521FEC37DBAC62A2D6B8549F ();
// 0x00000635 System.Void GlobalMainScreen::LoadSouvenirs(System.Int32)
extern void GlobalMainScreen_LoadSouvenirs_m1B18D9BB371FCFF44CF9BABD8EE72D0777D1047F ();
// 0x00000636 System.Collections.IEnumerator GlobalMainScreen::LoadPageWWW(System.Int32)
extern void GlobalMainScreen_LoadPageWWW_m845C99379E87EB47690133FF124D01E82D5CEA0C ();
// 0x00000637 System.Void GlobalMainScreen::Update()
extern void GlobalMainScreen_Update_m3FE7D6E6D5ECCC0F35758E77209C0AB5B1163556 ();
// 0x00000638 System.Void GlobalMainScreen::InitLoadedSouvenirs(SouvenirsData)
extern void GlobalMainScreen_InitLoadedSouvenirs_m6F4D24722200F385A2937385E775D28DB737C940 ();
// 0x00000639 System.Void GlobalMainScreen::InitSouvenir(SouvenirData)
extern void GlobalMainScreen_InitSouvenir_m5A847A0D25781F41717B732803D8F3546392CA9D ();
// 0x0000063A System.Void GlobalMainScreen::Show(Book)
extern void GlobalMainScreen_Show_m6586C1649180B0E0D980CAEF1CDD32737B7D42F7 ();
// 0x0000063B System.Void GlobalMainScreen::Show()
extern void GlobalMainScreen_Show_mC32977611DBFA8B1E7606294E8C3AE542971AACE ();
// 0x0000063C System.Void GlobalMainScreen::InitGames(Book)
extern void GlobalMainScreen_InitGames_m42C43ABE71A744B9EE5246CFFE7A2ABF7744A8CE ();
// 0x0000063D System.Void GlobalMainScreen::ReadBook()
extern void GlobalMainScreen_ReadBook_m68820834B6EB88848C40DBBACE435A6142DA9A88 ();
// 0x0000063E System.Void GlobalMainScreen::PlayGame(System.Int32)
extern void GlobalMainScreen_PlayGame_mC4F7D99045B7CE25F810A1BE78CBE36A13EA55AC ();
// 0x0000063F System.Void GlobalMainScreen::.ctor()
extern void GlobalMainScreen__ctor_mFC7354D41024BE964A1391D1A9A8C2678EC53799 ();
// 0x00000640 System.Void GlobalMainScreen::<InitButtons>b__34_0()
extern void GlobalMainScreen_U3CInitButtonsU3Eb__34_0_m2E54C78E41672E8162512826DFCA71794911FED3 ();
// 0x00000641 System.Void GlobalMainScreen::<InitButtons>b__34_1()
extern void GlobalMainScreen_U3CInitButtonsU3Eb__34_1_mBD94D4305775579D06635D9BFB027D62E8FCD66A ();
// 0x00000642 System.Void GlobalMainScreen::<InitButtons>b__34_2()
extern void GlobalMainScreen_U3CInitButtonsU3Eb__34_2_mB11DE6CAEC99871942A3BEE750EF562C4EC7161B ();
// 0x00000643 System.Void GlobalMainScreen::<InitButtons>b__34_3()
extern void GlobalMainScreen_U3CInitButtonsU3Eb__34_3_m159602D530F2B85873967AF99058E62B8A647DD1 ();
// 0x00000644 System.Void GlobalMainScreen::<AutoSwipe>b__39_0()
extern void GlobalMainScreen_U3CAutoSwipeU3Eb__39_0_m14653CDCA5FF1BB57726B608E1E1B12CE31B54F6 ();
// 0x00000645 System.Void LocalizationScreen::InitButtons()
extern void LocalizationScreen_InitButtons_mA930FDA5A6B773FB0D70C71BAAC8B37CDE9BF565 ();
// 0x00000646 System.Void LocalizationScreen::ShowRule(CachedTransitionScreen)
extern void LocalizationScreen_ShowRule_m23AB9C73628FB0D7EF15E0FDDC35A610003E1834 ();
// 0x00000647 System.Void LocalizationScreen::InitSetLanguageButtons()
extern void LocalizationScreen_InitSetLanguageButtons_m1D9BFFE7072CCC12A5D8C2DFEC34EC77205BA18D ();
// 0x00000648 System.Void LocalizationScreen::CheckSelectedButton()
extern void LocalizationScreen_CheckSelectedButton_mED78401A9B476941289E95CB03D1D97A658AEA63 ();
// 0x00000649 System.Void LocalizationScreen::.ctor()
extern void LocalizationScreen__ctor_mE338667793BADEF6DC09DCEC050F60CF870BC775 ();
// 0x0000064A System.Void SetLanguageButton::Awake()
extern void SetLanguageButton_Awake_m2B092EDADD726F8E554E0C20D39AE84CA6A8CC00 ();
// 0x0000064B System.Void SetLanguageButton::Init(UnityEngine.SystemLanguage,System.String,UnityEngine.Sprite)
extern void SetLanguageButton_Init_m9A2EE998C8AEBDF94056C9815CD5B0E49A4DEBD1 ();
// 0x0000064C System.Void SetLanguageButton::CheckIsSelected()
extern void SetLanguageButton_CheckIsSelected_m60908D50BE8181DBADEFBF60709F2A00C3A9DE41 ();
// 0x0000064D System.Void SetLanguageButton::Click()
extern void SetLanguageButton_Click_mAC48343370C587D017D788026542C0E672369729 ();
// 0x0000064E System.Void SetLanguageButton::.ctor()
extern void SetLanguageButton__ctor_m421393F7A097E54CF6D8FEC8E8C49834EB54E372 ();
// 0x0000064F System.Void LanguagePresset::.ctor()
extern void LanguagePresset__ctor_m585DE85CD0A3075BDD91209FA92F8D54675A71F0 ();
// 0x00000650 System.Void BookSmallPreview::Awake()
extern void BookSmallPreview_Awake_m8026CD4825660CDA05FEBA93FA53C9BA854E6891 ();
// 0x00000651 System.Void BookSmallPreview::InitBook(Book)
extern void BookSmallPreview_InitBook_m63AA09F84B34FF59DFF7421BE9C7169DA4A197CF ();
// 0x00000652 System.Void BookSmallPreview::UpdateValues()
extern void BookSmallPreview_UpdateValues_m2E1E940F38E2930E490B37EC99EBB69C19FBE13F ();
// 0x00000653 System.Void BookSmallPreview::OpenBookPreview()
extern void BookSmallPreview_OpenBookPreview_mA72A76A96BA12A47FA1E3842D10D114446295C83 ();
// 0x00000654 System.Void BookSmallPreview::.ctor()
extern void BookSmallPreview__ctor_mC97307BB44408334D8BD4B5F0297D633B7EC2A32 ();
// 0x00000655 System.Void BooksPanelController::Init()
extern void BooksPanelController_Init_mCBC63B13DB8C9A0AE52CE2EC8725169E22C03918 ();
// 0x00000656 System.Void BooksPanelController::Refresh()
extern void BooksPanelController_Refresh_m5091393D729585F847A88367E6BF37B22E9E20E8 ();
// 0x00000657 System.Void BooksPanelController::.ctor()
extern void BooksPanelController__ctor_m65C8D007BE767C137E27F13C563528281B0B5E40 ();
// 0x00000658 System.Void MainScreen::InitButtons()
extern void MainScreen_InitButtons_mCCF6D755DC43DC7B8732FEE12FCBECF63C88B19D ();
// 0x00000659 System.Void MainScreen::ShowRule(CachedTransitionScreen)
extern void MainScreen_ShowRule_mA37ED6342B5A783FBC1EB23B673AAEF60061D266 ();
// 0x0000065A System.Void MainScreen::Show(Book)
extern void MainScreen_Show_mBBD39021CD75F5CFA5FF3C9288A6A121CDF8E249 ();
// 0x0000065B System.Void MainScreen::InitGames(Book)
extern void MainScreen_InitGames_m033DF8CA08FE6F8B7E11981AF69B26ADCF100264 ();
// 0x0000065C System.Void MainScreen::ReadBook()
extern void MainScreen_ReadBook_m79C2CBB707B6C364BD4522C51E4C31407E62DA30 ();
// 0x0000065D System.Void MainScreen::PlayGame(System.Int32)
extern void MainScreen_PlayGame_mD24DA6CBEB0B797DDC665ECB3AE16C9A61BB4AD2 ();
// 0x0000065E System.Void MainScreen::Show()
extern void MainScreen_Show_m8CF769428C3165719DB59874A690243E16097409 ();
// 0x0000065F System.Void MainScreen::Hide2()
extern void MainScreen_Hide2_m1B3844763C008A17E5A5BE6A0FD397709F380CE3 ();
// 0x00000660 System.Void MainScreen::Hide3()
extern void MainScreen_Hide3_mDAE53CDD25777EB09895B17FD53281194D3FF5C6 ();
// 0x00000661 System.Void MainScreen::InitAnimatorParameters()
extern void MainScreen_InitAnimatorParameters_m70D1FC4945B37D530DDD2F93ED324461DBD9F1D0 ();
// 0x00000662 System.Void MainScreen::.ctor()
extern void MainScreen__ctor_mDEB4484ACEBC62EAE5EE73C12D8025C37A69F9B4 ();
// 0x00000663 System.Void MainScreen::<InitButtons>b__12_0()
extern void MainScreen_U3CInitButtonsU3Eb__12_0_mA5DC60B2C4B6B684CE8D3B7889A5331F6BCE1B2C ();
// 0x00000664 System.Void MainScreen::<InitButtons>b__12_1()
extern void MainScreen_U3CInitButtonsU3Eb__12_1_mBBD4F70C1503713EF12F4A46DF6CD385BE95F1FD ();
// 0x00000665 System.Void MainScreen::<InitButtons>b__12_2()
extern void MainScreen_U3CInitButtonsU3Eb__12_2_mC4275219B7FA830BFD70C7CAFEF3293B0C87A19D ();
// 0x00000666 System.Void MainScreen::<InitButtons>b__12_3()
extern void MainScreen_U3CInitButtonsU3Eb__12_3_m9D6AEF79C1D5F92E1A714B0C4044ECC0C6A6E946 ();
// 0x00000667 System.Void ClickManager::Update()
extern void ClickManager_Update_m2543286AFDEE71E29CD5A7A82CA5C5B742D9439C ();
// 0x00000668 System.Void ClickManager::.ctor()
extern void ClickManager__ctor_m3D021C5A2EDCE75C51BA0134ED9E090B2DEA1D1A ();
// 0x00000669 System.Boolean NativeAdsPanel::get_Inited()
extern void NativeAdsPanel_get_Inited_mE1A6C62912AECB9B46D14C53277329F649C8E2C8 ();
// 0x0000066A System.Void NativeAdsPanel::set_Inited(System.Boolean)
extern void NativeAdsPanel_set_Inited_mC255445B322A2E090C8DF804B0C5272A849339B1 ();
// 0x0000066B System.Boolean NativeAdsPanel::get_Interactable()
extern void NativeAdsPanel_get_Interactable_m05C3037E20948362416A7C20B4CF66DA33AA0BBE ();
// 0x0000066C System.Void NativeAdsPanel::set_Interactable(System.Boolean)
extern void NativeAdsPanel_set_Interactable_mB903D8203D23D57C241038202AEC798CCEDC5230 ();
// 0x0000066D System.Single NativeAdsPanel::get_Alpha()
extern void NativeAdsPanel_get_Alpha_m81DCADCCF3387B15E8A60BE9280CB1C25815F184 ();
// 0x0000066E System.Void NativeAdsPanel::set_Alpha(System.Single)
extern void NativeAdsPanel_set_Alpha_mF429D57FCACAE020CECF543B28D7B61CA509774E ();
// 0x0000066F System.Void NativeAdsPanel::InitCollidePos()
extern void NativeAdsPanel_InitCollidePos_mD35861FA29A772A49C530F835A6EF46183A56DEF ();
// 0x00000670 System.Void NativeAdsPanel::.ctor()
extern void NativeAdsPanel__ctor_m3D58F1424554E03B112BABE48C40FD3C155536FA ();
// 0x00000671 System.Int32 NativeAdsScreen::get_Selected()
extern void NativeAdsScreen_get_Selected_m37218932829AA808F7749B618583EC5EBD324642 ();
// 0x00000672 System.Void NativeAdsScreen::set_Selected(System.Int32)
extern void NativeAdsScreen_set_Selected_m417E55893EC15BFEEDD20E5A9FDD2572C3F270F6 ();
// 0x00000673 System.Void NativeAdsScreen::ShowRule(CachedTransitionScreen)
extern void NativeAdsScreen_ShowRule_mB88E4AACC308BD345E08ED313EC2256732D122DC ();
// 0x00000674 System.Void NativeAdsScreen::Show()
extern void NativeAdsScreen_Show_mE9CD18E7396E34971884E670FF332D6C7D4456AB ();
// 0x00000675 System.Void NativeAdsScreen::InitButtons()
extern void NativeAdsScreen_InitButtons_m6EA6D0814D1D7D86C943E9960FB861090F5AB92D ();
// 0x00000676 System.Void NativeAdsScreen::InitIndicators()
extern void NativeAdsScreen_InitIndicators_m8EF012E06B72EDB5ADFFA2264E9F1177EC5EC5B6 ();
// 0x00000677 System.Void NativeAdsScreen::Updateindicators()
extern void NativeAdsScreen_Updateindicators_m6365AA628A49E1CAD9DB27B666F1198BFA5F86CE ();
// 0x00000678 System.Void NativeAdsScreen::Update()
extern void NativeAdsScreen_Update_mD4A8A296FECEDA05F586B61DB8043BFB8B081F17 ();
// 0x00000679 System.Void NativeAdsScreen::SwipeProcess(System.Single)
extern void NativeAdsScreen_SwipeProcess_m7CBB88C8859362E7DDD55C2CB6F517B6285350FC ();
// 0x0000067A System.Void NativeAdsScreen::SwipeStart()
extern void NativeAdsScreen_SwipeStart_mD85708797FEEB4DD352ACD9936E978B863BA7248 ();
// 0x0000067B System.Void NativeAdsScreen::SwipeStop()
extern void NativeAdsScreen_SwipeStop_m73F07966CBAE93AD82477DA4C2920EE24C1307F4 ();
// 0x0000067C System.Void NativeAdsScreen::AnimationLerp(System.Single)
extern void NativeAdsScreen_AnimationLerp_m2EE85C50D0D0FBF15DE5580E76D9F48CC24316A1 ();
// 0x0000067D System.Void NativeAdsScreen::.ctor()
extern void NativeAdsScreen__ctor_m488EDA06817CA9C2F79549BAA8CA775FAB9C0BC8 ();
// 0x0000067E System.Void LevelPreviewPanel::InitLevel(MiniGamePreset,LevelsPanelController,System.Int32)
extern void LevelPreviewPanel_InitLevel_m4CAFA565EAE708FD8D9E335136C5929DCEAA9E36 ();
// 0x0000067F System.Void LevelPreviewPanel::.ctor()
extern void LevelPreviewPanel__ctor_mD9F78DA641CDF83318F4FB926752C822B5AAD665 ();
// 0x00000680 System.Void LevelsPanelController::Init(MiniGameData)
extern void LevelsPanelController_Init_mB37E31C3C901AD4576AD36EED9E66595E3C71CE7 ();
// 0x00000681 System.Void LevelsPanelController::ClearPanel()
extern void LevelsPanelController_ClearPanel_mD07329C98762879AB4EE45CB6ADF735A18BE8F8B ();
// 0x00000682 System.Void LevelsPanelController::PlayGame(System.Int32)
extern void LevelsPanelController_PlayGame_m3ABC3C81AEA6B72956C4C5A0ED47E90B35B49C2F ();
// 0x00000683 System.Void LevelsPanelController::.ctor()
extern void LevelsPanelController__ctor_mA3B690893634CDF6336B20A02DA265B5974C5D74 ();
// 0x00000684 System.Void SelectLevelScreen::InitButtons()
extern void SelectLevelScreen_InitButtons_m476B978255DFBBBC9671586421338BD74B74D022 ();
// 0x00000685 System.Void SelectLevelScreen::ShowRule(CachedTransitionScreen)
extern void SelectLevelScreen_ShowRule_mC81BB8AD7972554E842486E1B39573CB9044EADB ();
// 0x00000686 System.Void SelectLevelScreen::Show(MiniGameData)
extern void SelectLevelScreen_Show_mD7B8DCD8AD13D196093BAEF9209432AAA772D4E5 ();
// 0x00000687 System.Void SelectLevelScreen::Back()
extern void SelectLevelScreen_Back_mD94E7A46778C7758A82BBC9B537CE690A5E1B3A4 ();
// 0x00000688 System.Void SelectLevelScreen::.ctor()
extern void SelectLevelScreen__ctor_mA800D4B5C5D20A6CB9D32F8BEF5B1018E5F54BBE ();
// 0x00000689 System.Void SettingsButtonsScreen::InitButtons()
extern void SettingsButtonsScreen_InitButtons_mC2B0CD46D4A0B7B38736B8921C5E94E72B86F42B ();
// 0x0000068A System.Void SettingsButtonsScreen::ShowRule(CachedTransitionScreen)
extern void SettingsButtonsScreen_ShowRule_m09F5F5C7A679A50B21E95B1931AC6492CFF47C8A ();
// 0x0000068B System.Void SettingsButtonsScreen::.ctor()
extern void SettingsButtonsScreen__ctor_m9240C83C93D28D124104A7CF26C462B19C68D38C ();
// 0x0000068C System.Void SettingsScreen::InitButtons()
extern void SettingsScreen_InitButtons_mBBB724757FAA705385DCC764CDBF1C675F666603 ();
// 0x0000068D System.Void SettingsScreen::ShowRule(CachedTransitionScreen)
extern void SettingsScreen_ShowRule_mD33B83CC54DBD319ADC573C305ADCD1DE4EC4FD4 ();
// 0x0000068E System.Void SettingsScreen::.ctor()
extern void SettingsScreen__ctor_m0A1661824FB957CC3FEC0B710A38771DA2D30F85 ();
// 0x0000068F System.Void ShopScreen::InitButtons()
extern void ShopScreen_InitButtons_mECB21DCB4A44B06B8FD6162DC2C8FB6C08871D19 ();
// 0x00000690 System.Void ShopScreen::ShowRule(CachedTransitionScreen)
extern void ShopScreen_ShowRule_mB81BDABD8E9D021A677DC18A8143B6157A2D470E ();
// 0x00000691 System.Void ShopScreen::Show()
extern void ShopScreen_Show_m63847FCD91F45410522F8F578286D12115A77474 ();
// 0x00000692 System.Void ShopScreen::Hide()
extern void ShopScreen_Hide_m8A905C44B9A74FED74A4E029C93092036A49570C ();
// 0x00000693 System.Void ShopScreen::.ctor()
extern void ShopScreen__ctor_m64C014F7CBF5FE12C521D935CF54E55C016FF9B7 ();
// 0x00000694 System.Void ShopBookPreviewScreen::InitButtons()
extern void ShopBookPreviewScreen_InitButtons_mC3C6A9DCD97152D5A7CE35986CF46C615CC56B94 ();
// 0x00000695 System.Void ShopBookPreviewScreen::ShowRule(CachedTransitionScreen)
extern void ShopBookPreviewScreen_ShowRule_m1B58F154D8DE31560D34BD05FE8FF8782CF17922 ();
// 0x00000696 System.Void ShopBookPreviewScreen::Show(Book)
extern void ShopBookPreviewScreen_Show_m035D151D14CC52D549037E44D5DE7F6B867AF4D2 ();
// 0x00000697 System.Void ShopBookPreviewScreen::InitButton()
extern void ShopBookPreviewScreen_InitButton_m3F05A2679E57FEDCEDCDEC403C5B313CCF62CC1B ();
// 0x00000698 System.Void ShopBookPreviewScreen::Read()
extern void ShopBookPreviewScreen_Read_m3E3C35B6D511CD126236E886FCA0145A03D61DF3 ();
// 0x00000699 System.Void ShopBookPreviewScreen::Buy()
extern void ShopBookPreviewScreen_Buy_mC776467A42AD3EB7C1FB286CAD4D5D160A55E743 ();
// 0x0000069A System.Void ShopBookPreviewScreen::InitBook()
extern void ShopBookPreviewScreen_InitBook_m15F4FAC4259AFAEEBD3B79BDB5A46A81ED718E3E ();
// 0x0000069B System.Void ShopBookPreviewScreen::Show()
extern void ShopBookPreviewScreen_Show_mE1BC33956C350E2198140ABD3D0C396ED139EBBF ();
// 0x0000069C System.Void ShopBookPreviewScreen::Hide2()
extern void ShopBookPreviewScreen_Hide2_mDACCCBEC3A32B2F46320A33EB1F872B592A8ACC3 ();
// 0x0000069D System.Void ShopBookPreviewScreen::InitAnimatorParameters()
extern void ShopBookPreviewScreen_InitAnimatorParameters_mE7F7F4764CBE71FB3D27D3E784825A02C4B71CE3 ();
// 0x0000069E System.Void ShopBookPreviewScreen::.ctor()
extern void ShopBookPreviewScreen__ctor_mAC31DD250B23324F38F014168ACD57504CBEF8ED ();
// 0x0000069F System.Void SouvenirPanel::Awake()
extern void SouvenirPanel_Awake_m11C0C645F5DE25DBE4790AE11C0BC5D53C43002A ();
// 0x000006A0 System.Void SouvenirPanel::StopLoading()
extern void SouvenirPanel_StopLoading_m4E53E9C8DD7BA9C1FF8FBD815BD8CC3CAB38F1FE ();
// 0x000006A1 System.Void SouvenirPanel::Load()
extern void SouvenirPanel_Load_mF24AEAE4965F73C5867BC12B235CABA3678D3624 ();
// 0x000006A2 System.Void SouvenirPanel::Load(SouvenirData)
extern void SouvenirPanel_Load_mC0D27888D841254662889331A6A64C84AE65CCE6 ();
// 0x000006A3 System.Void SouvenirPanel::OpenLink()
extern void SouvenirPanel_OpenLink_m04DC7991ED6C5AC90A31B60D3F68A91671C86032 ();
// 0x000006A4 System.Collections.IEnumerator SouvenirPanel::LoadImageRoutine(System.String)
extern void SouvenirPanel_LoadImageRoutine_m638E1A97C506D993F04362FD5040DF3E97CE374D ();
// 0x000006A5 System.Void SouvenirPanel::.ctor()
extern void SouvenirPanel__ctor_m1E4D1C0B0D66EC93192BBCF871DFDD0C92DD9EA2 ();
// 0x000006A6 SouvenirsData SouvenirsData::ParceFromJSON(System.String)
extern void SouvenirsData_ParceFromJSON_mA586C12F6CA433FB8D60E920A7FD133DC5E12DA4 ();
// 0x000006A7 System.Void SouvenirsData::.ctor()
extern void SouvenirsData__ctor_m11843D8F79E5BBA82A87BA2B8BEA52A744907987 ();
// 0x000006A8 PaginationData PaginationData::ParceFromJSON(System.String)
extern void PaginationData_ParceFromJSON_m453A11F22765695A6224F96F44087A3F3139D239 ();
// 0x000006A9 System.Void PaginationData::.ctor()
extern void PaginationData__ctor_m9B45F12EC39A94245DFE34B65E7EE5E9B2CE1927 ();
// 0x000006AA SouvenirData SouvenirData::ParceFromJSON(System.String)
extern void SouvenirData_ParceFromJSON_m80053471E25AAB87AA6C71F80291B71DE3CDE658 ();
// 0x000006AB System.Void SouvenirData::.ctor()
extern void SouvenirData__ctor_m2E0728DAC746015CA76FA356FB8146586746FEA3 ();
// 0x000006AC System.Boolean SouvenirsScreen::get_IsLoading()
extern void SouvenirsScreen_get_IsLoading_m44D5DCA25944EF4F43EF84D066CADE8D1E5F5183 ();
// 0x000006AD System.Void SouvenirsScreen::set_IsLoading(System.Boolean)
extern void SouvenirsScreen_set_IsLoading_m7C840EDE9ED0E2CD608AD247CA23213C30D88285 ();
// 0x000006AE System.Void SouvenirsScreen::InitButtons()
extern void SouvenirsScreen_InitButtons_m2D9227C1B565651E246265080EB3EA0B1FFCD57D ();
// 0x000006AF System.Void SouvenirsScreen::ShowRule(CachedTransitionScreen)
extern void SouvenirsScreen_ShowRule_m986BF9D0F53ACA2E5A2B582F7FD87C528E5DC9D7 ();
// 0x000006B0 System.Boolean SouvenirsScreen::get_HasNextPage()
extern void SouvenirsScreen_get_HasNextPage_mE628C9F728192E737457B1348B61D9A54DC3FF58 ();
// 0x000006B1 System.Boolean SouvenirsScreen::get_HasPreviousPage()
extern void SouvenirsScreen_get_HasPreviousPage_mD4E1BD2C4AC27E2B386184D3970B47BA4E14160F ();
// 0x000006B2 System.Void SouvenirsScreen::CheckShowButtonsState()
extern void SouvenirsScreen_CheckShowButtonsState_mC2D4DCCB9EA21879E97A9925B837D11BCDD8632F ();
// 0x000006B3 System.Void SouvenirsScreen::PreviousPage()
extern void SouvenirsScreen_PreviousPage_mFF8B07766628A602119AFA86743FAF3D576C2B06 ();
// 0x000006B4 System.Void SouvenirsScreen::NextPage()
extern void SouvenirsScreen_NextPage_m77CE1DB4F34D90D4E1A059A16004A8ECE48E045E ();
// 0x000006B5 System.Void SouvenirsScreen::HideAllSouvenirs()
extern void SouvenirsScreen_HideAllSouvenirs_mD77BF9E5D94F129157E2258621C9748063560D12 ();
// 0x000006B6 System.Void SouvenirsScreen::Show()
extern void SouvenirsScreen_Show_m57999321E3A4BFFA4DF641D1400C5797D04D850D ();
// 0x000006B7 System.Void SouvenirsScreen::Hide()
extern void SouvenirsScreen_Hide_mF4DE824E9A5AD91A4669F6DEA1B9E928148F52DA ();
// 0x000006B8 System.Void SouvenirsScreen::LoadPage(System.Int32)
extern void SouvenirsScreen_LoadPage_m623C4E4246E1A3D4E7B8718DE6B97A0474B5BE6D ();
// 0x000006B9 System.Void SouvenirsScreen::ShowSouvenirs(System.Int32)
extern void SouvenirsScreen_ShowSouvenirs_m616A104CAC8B7A608CE6CF4188202A1F7443B268 ();
// 0x000006BA System.Void SouvenirsScreen::LoadSouvenirs(System.Int32)
extern void SouvenirsScreen_LoadSouvenirs_mAB3D9004E21F0202858682E646A680B85581AD36 ();
// 0x000006BB System.Collections.IEnumerator SouvenirsScreen::LoadPageWWW(System.Int32)
extern void SouvenirsScreen_LoadPageWWW_m54635DDBA8567C9DC7D2FE03888D1CF30313B7E5 ();
// 0x000006BC System.Collections.IEnumerator SouvenirsScreen::LoadTotalPagesWWW()
extern void SouvenirsScreen_LoadTotalPagesWWW_m0C2405F14B741A3D4E502049B653B79343ED5886 ();
// 0x000006BD System.Void SouvenirsScreen::InitLoadedSouvenirs(SouvenirsData)
extern void SouvenirsScreen_InitLoadedSouvenirs_mCDD9780C00F5655D78E720FEF1A4724B05CDFB54 ();
// 0x000006BE System.Void SouvenirsScreen::InitSouvenir(SouvenirData)
extern void SouvenirsScreen_InitSouvenir_m3037073ADB738611F477BA6F755CCC97BF076AF7 ();
// 0x000006BF System.Void SouvenirsScreen::.ctor()
extern void SouvenirsScreen__ctor_mB69BF4E33BC8085C1131E5327F71FA5D81AD59F8 ();
// 0x000006C0 System.Void ElementController::Init(System.Object)
extern void ElementController_Init_m46522E8FEE8FFD3B802E0A4DE6CF70B94B1FD39B ();
// 0x000006C1 System.Void ElementController::Click()
// 0x000006C2 System.Boolean ElementController::Select()
// 0x000006C3 System.Void ElementController::UnSelect()
// 0x000006C4 System.Void ElementController::.ctor()
extern void ElementController__ctor_m2741CE9DD3D1B3B9FF217ECBA477BFE64F03F75E ();
// 0x000006C5 System.Void IElement::Init(System.Object)
// 0x000006C6 System.Boolean IElement::Select()
// 0x000006C7 System.Void IElement::UnSelect()
// 0x000006C8 System.Void ElementData::.ctor()
extern void ElementData__ctor_m95BEC41782F46165CEAECE88932A74A93CA37576 ();
// 0x000006C9 System.Collections.Generic.List`1<UnityEngine.GameObject> ScrollController::get_Elements()
extern void ScrollController_get_Elements_m524C91A0E432F8955BB59EF703DAD3CF00CD427C ();
// 0x000006CA System.Int32 ScrollController::get_CountElements()
extern void ScrollController_get_CountElements_m77C0425D4B3FAFA6BCA7D385406CB076503EE11B ();
// 0x000006CB System.Single ScrollController::get_MinDistance()
extern void ScrollController_get_MinDistance_m9AD91B5466612F14031E4621FB480839022B977C ();
// 0x000006CC System.Int32 ScrollController::get_SelectedElement()
extern void ScrollController_get_SelectedElement_m7516811E6B14BA6FD190B5225FC374EB7037422E ();
// 0x000006CD System.Void ScrollController::set_SelectedElement(System.Int32)
extern void ScrollController_set_SelectedElement_m5D2E0A4680ED388546527E6DAC13A4BFE4DCB12D ();
// 0x000006CE System.Boolean ScrollController::get_Drag()
extern void ScrollController_get_Drag_m42AF87D4062ED95047CF8834F7E3FCE08CF63E85 ();
// 0x000006CF System.Void ScrollController::set_Drag(System.Boolean)
extern void ScrollController_set_Drag_m8125F800243DD41936C29FDEE5E4115C295DE72F ();
// 0x000006D0 System.Boolean ScrollController::get_Active()
extern void ScrollController_get_Active_mA69617C3EFDD447794E82166F2661D40616CCD04 ();
// 0x000006D1 System.Void ScrollController::set_Active(System.Boolean)
extern void ScrollController_set_Active_m75FD8BD9C7573E08D6186628567CF66226F8C51C ();
// 0x000006D2 System.Void ScrollController::Start()
extern void ScrollController_Start_m0F7BB9EFE6E2FED3910729F583FD0CCFC499AE54 ();
// 0x000006D3 System.Void ScrollController::InitScrollContainer(System.Collections.Generic.List`1<T>)
// 0x000006D4 System.Void ScrollController::SetState(ScrollState)
extern void ScrollController_SetState_mFECDE0DB69F5CDDC86DEA6E492E0618F7C768731 ();
// 0x000006D5 System.Void ScrollController::Update()
extern void ScrollController_Update_m8AB5B791EB30A39535C87E78C5290AE21B12E3D2 ();
// 0x000006D6 System.Void ScrollController::HandleSwipe()
extern void ScrollController_HandleSwipe_mEA9F4DD223E4C88F2276EA365C4F9CD4143D390B ();
// 0x000006D7 System.Void ScrollController::MoveScrollContainer(System.Single)
extern void ScrollController_MoveScrollContainer_mA1E53B55F73B79B31C0BA557D9E15840717BBEA0 ();
// 0x000006D8 System.Void ScrollController::DefaultState()
extern void ScrollController_DefaultState_m9C25694D341CAC2046DF3AFBAC4AC0B005509DE9 ();
// 0x000006D9 System.Void ScrollController::MoveState()
extern void ScrollController_MoveState_mC464C80C550DD6F154DA13A6FB51F81932FB52D7 ();
// 0x000006DA System.Void ScrollController::MoveToNext()
extern void ScrollController_MoveToNext_mDE022874226C17756EF04D9382A60F331BEAAE36 ();
// 0x000006DB System.Void ScrollController::MoveToConcrete()
extern void ScrollController_MoveToConcrete_m4B44DAF544D6168ED541ADC93C953B26A976ADBB ();
// 0x000006DC System.Void ScrollController::SetIndexAndPosition(System.Int32)
extern void ScrollController_SetIndexAndPosition_m9FFEF3E255CEC30D146849ED842A8C48CFF57F0D ();
// 0x000006DD System.Void ScrollController::ClaculateDistance()
extern void ScrollController_ClaculateDistance_m0111165AE231BFC1CAD11EB23F9B8D4D4EB1E424 ();
// 0x000006DE System.Void ScrollController::UpdateSelectedElement()
extern void ScrollController_UpdateSelectedElement_m7AF8388A17C7AEFE5F70DF5C38458A4DB240EE55 ();
// 0x000006DF System.Int32 ScrollController::GetSelectedIndex(System.Single&)
extern void ScrollController_GetSelectedIndex_mF153F1907379F8581198F6EA41785B6CA4851FCE ();
// 0x000006E0 System.Void ScrollController::LerpToElement(System.Int32)
extern void ScrollController_LerpToElement_mC790E3A6847FF3C83DE0C4C3FFC0449FEBDB89E4 ();
// 0x000006E1 System.Single ScrollController::GetIndexPosition(System.Int32)
extern void ScrollController_GetIndexPosition_mA43B827CDDB3715E44DE53F0657B9D097098B44D ();
// 0x000006E2 System.Void ScrollController::UpdatePosition(System.Single)
extern void ScrollController_UpdatePosition_m4E8219421AFADC9ECF0B5F39503E5849F4A6D27B ();
// 0x000006E3 System.Void ScrollController::.ctor()
extern void ScrollController__ctor_mEF7841B8C2874853CEAF28FF88156BE189C0478D ();
// 0x000006E4 System.Void UIScreenController::Init()
extern void UIScreenController_Init_mDBE33C55996AD574517FD9EBAA0B498D9B19B8FA ();
// 0x000006E5 System.Void UIScreenController::Start()
extern void UIScreenController_Start_m9ACFE52D2D1D82F576B1F6E087E153BD3574816E ();
// 0x000006E6 System.Void UIScreenController::Update()
extern void UIScreenController_Update_mE66631F107F8A2C0635B5CD95CCD9F98AAF3C3DD ();
// 0x000006E7 System.Void UIScreenController::.ctor()
extern void UIScreenController__ctor_m130BBB871C70778511D9843C15E2D6095B7FBFE2 ();
// 0x000006E8 GoogleMobileAds.Common.IBannerClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildBannerClient()
extern void GoogleMobileAdsClientFactory_BuildBannerClient_mA8AF2737B3F09009A1404FFC62A28214B93C7C72 ();
// 0x000006E9 GoogleMobileAds.Common.IInterstitialClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildInterstitialClient()
extern void GoogleMobileAdsClientFactory_BuildInterstitialClient_mBF9E81825CF751719C531957E81D6639D026958B ();
// 0x000006EA GoogleMobileAds.Common.IRewardBasedVideoAdClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildRewardBasedVideoAdClient()
extern void GoogleMobileAdsClientFactory_BuildRewardBasedVideoAdClient_mFB5500581014FDE1CD791874FF848A3BCFAF66E0 ();
// 0x000006EB GoogleMobileAds.Common.IRewardedAdClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildRewardedAdClient()
extern void GoogleMobileAdsClientFactory_BuildRewardedAdClient_m298968A6201AC11F7680F8C170C66626D9D252FB ();
// 0x000006EC GoogleMobileAds.Common.IAdLoaderClient GoogleMobileAds.GoogleMobileAdsClientFactory::BuildAdLoaderClient(GoogleMobileAds.Api.AdLoader)
extern void GoogleMobileAdsClientFactory_BuildAdLoaderClient_mEC47887E28349071E4AD72A333A3E28513B10C10 ();
// 0x000006ED GoogleMobileAds.Common.IMobileAdsClient GoogleMobileAds.GoogleMobileAdsClientFactory::MobileAdsInstance()
extern void GoogleMobileAdsClientFactory_MobileAdsInstance_mB25F2EE6B6E324BC739E76B74DF8CEC55A5317AE ();
// 0x000006EE System.Void GoogleMobileAds.GoogleMobileAdsClientFactory::.ctor()
extern void GoogleMobileAdsClientFactory__ctor_mCCE39F56BB450D301B0F962D5AE78538C659A70A ();
// 0x000006EF System.Void Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::SetupProvider()
extern void SimpleEncryption_SetupProvider_mC863A52E9C7526DFF2E603FC96B8C732D2DCD2B4 ();
// 0x000006F0 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptString(System.String)
extern void SimpleEncryption_EncryptString_mEE6A4BBF3C99429C2FC716ABE124ABB2063AD9F9 ();
// 0x000006F1 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptString(System.String)
extern void SimpleEncryption_DecryptString_m6F6B5937F650C14138BC52FC2D1420DC6673F863 ();
// 0x000006F2 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptFloat(System.Single)
extern void SimpleEncryption_EncryptFloat_m67214DD4F1D52A66C76ABBE941F34295BC59BD9B ();
// 0x000006F3 System.String Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::EncryptInt(System.Int32)
extern void SimpleEncryption_EncryptInt_m50E64A6BB1E8E8045A53EF9B7C7673F5A600182D ();
// 0x000006F4 System.Single Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptFloat(System.String)
extern void SimpleEncryption_DecryptFloat_m0DCADF0D1C2981796D2D18CC008997CA8BF1CFB0 ();
// 0x000006F5 System.Int32 Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::DecryptInt(System.String)
extern void SimpleEncryption_DecryptInt_m4D82D2479B7486926F0F9C7D0821EC494AD25493 ();
// 0x000006F6 System.Void Sabresaurus.PlayerPrefsExtensions.SimpleEncryption::.cctor()
extern void SimpleEncryption__cctor_m8D7B9FA5FFCCD79515F2710F2B8194A79256174E ();
// 0x000006F7 SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x000006F8 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_mB1C047B24BE0AF89233BA913B7296D715ECECE66 ();
// 0x000006F9 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m9A59D1E938897BB96CDB88A894BBF986DB55BB81 ();
// 0x000006FA SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_mB58A2DB9757AD59BB8D7D059C628C6A8C2D31656 ();
// 0x000006FB System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m041CFF2071DA73C5EEB8EF1E19656D288C244805 ();
// 0x000006FC System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m8D7724B6B2ABC5543A2040DC3B3225917C9F470E ();
// 0x000006FD System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mD543D0CF821CBC592A531B6470267EBCC33B74C1 ();
// 0x000006FE System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_mD636C660E848709A976C52D654CDA38AD8B60DB3 ();
// 0x000006FF System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m7CD6ADBF3C7AA462BE8A7C590CF446F6BDEBE455 ();
// 0x00000700 System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mC3F677750EDA5702CE80E204B3CC982FB2BE012B ();
// 0x00000701 System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mE4ED5ED271F80376D089C731A702F2B7DA621AAA ();
// 0x00000702 System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m043B2335CE7DED87923518A2F7A22556629B09EA ();
// 0x00000703 System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m6E7E190C4ACE70EF73C6B5661F1339282B72BE0E ();
// 0x00000704 System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_mA64A31CFC4EE01CD89631773CE9687A40A1DA97A ();
// 0x00000705 System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m24EABC4751202A6E6891F91AB42C29C2AD029C67 ();
// 0x00000706 System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_mA3761A6300453FEEA6906DC9556F868DA2B5775F ();
// 0x00000707 System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_mBF1590D1428C7E27030229A0E89DD2DD39565FE8 ();
// 0x00000708 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_mB5CBFEBDFCB6751B5AA62F68795E4DF199575D8C ();
// 0x00000709 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m2D15754A3173CA5108A343620D269E85FBD1E6AA ();
// 0x0000070A SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m865852918806702BF99DFB1D45CEF588F5073C2F ();
// 0x0000070B SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mEC2EA47AADD914B3468F912E6EED1DA02821D118 ();
// 0x0000070C System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_m55A54D8FB3B8A6F40CCF0AE53B453009EEE65D98 ();
// 0x0000070D System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_mEF156F8DF47F0D0A7374565ACE2853D43518238E ();
// 0x0000070E System.Boolean SimpleJSON.JSONNode::HasKey(System.String)
extern void JSONNode_HasKey_m4921F263458F57F6FD46978C498A4DB2E06E790B ();
// 0x0000070F SimpleJSON.JSONNode SimpleJSON.JSONNode::GetValueOrDefault(System.String,SimpleJSON.JSONNode)
extern void JSONNode_GetValueOrDefault_m6D65C7DAA5D98388BBD231CA2A2EA4B84ACF048B ();
// 0x00000710 System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_mAC01EBB7D1362129DA253A871849052E332E0377 ();
// 0x00000711 System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m9E8E9ABDDE0890D23693A750409993A36096EB89 ();
// 0x00000712 System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x00000713 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x00000714 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m784D6111F5ACCE6368133742D1813D8E753A8249 ();
// 0x00000715 SimpleJSON.JSONNode_KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_mAEC584E7C7F1CCC000C51E279CEA435552023ED3 ();
// 0x00000716 SimpleJSON.JSONNode_ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_m647ABB3BC12AFD94CE4C23531628EFC29A7981D5 ();
// 0x00000717 System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_mC9E31CD6FE5BD5E3A6C530431FDFA573055AC868 ();
// 0x00000718 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_m358FF197E7599255C7D54081760A6BD741BF9FCF ();
// 0x00000719 System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m40A4B4D25131548ABAD1B970733AA945A9E21C3D ();
// 0x0000071A System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m7E6C063E6F82171FE5CC7B3C84E731663B097FDD ();
// 0x0000071B System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_mC9EEF428EBB07D9DE9A1F333D69D77693BDB7F6A ();
// 0x0000071C System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_mA5447099D7BA4A3A1BC5CB30DFC975538386376E ();
// 0x0000071D System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m7FB9DFADD7A844A974CC3CF30C182E3BE29A0EE5 ();
// 0x0000071E System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_mCF8C1B1F1AD2055C3113AC99AFAD75EC55874A50 ();
// 0x0000071F System.Int64 SimpleJSON.JSONNode::get_AsLong()
extern void JSONNode_get_AsLong_mAF124ADAAD7EFD2BC48FF366677F7FCA1D125E36 ();
// 0x00000720 System.Void SimpleJSON.JSONNode::set_AsLong(System.Int64)
extern void JSONNode_set_AsLong_m4CDD763725E88AAA9B846E200E12FD48C9CC54FC ();
// 0x00000721 SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m7DF6AB373218A86EFF6A9478A824D5BB8C01421A ();
// 0x00000722 SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m8BC40A325C24DE488E73E7DAFEA530B270BBD95B ();
// 0x00000723 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m40DE306D66005FAA0D687E7EE5A12F42B339B9B7 ();
// 0x00000724 System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE47E54F268F957AB45154A95C2220957638643AB ();
// 0x00000725 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m18C4C44499594B370DD624777FFE07A175061F95 ();
// 0x00000726 System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mA3673761129AAC5ECBE1615E23720682B076A979 ();
// 0x00000727 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_mDDC8416832229DDFFC10D0277679BA61C70592B6 ();
// 0x00000728 System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m10EEE49EFF8F8A60DBA8E69B37C0CF0B6F3DFE89 ();
// 0x00000729 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_mE55A9825BA41EA4F747100D3FCC73AD487B0BAA5 ();
// 0x0000072A System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m02B4DE0D9177961B8B6873C4D70DF3CDDD4F5675 ();
// 0x0000072B SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int64)
extern void JSONNode_op_Implicit_m4B504E4E4C1728B756DF77CE4DBACB4BF3B1264C ();
// 0x0000072C System.Int64 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mD00FCC82967070F98EE01F1FB872C01128224D96 ();
// 0x0000072D SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_m6C8D9D6D66148F4D1CDB43116466494932C50E62 ();
// 0x0000072E System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m807DE24F0FBC154EEB0AD5CB7E219D4B6BC1B565 ();
// 0x0000072F SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_m3E839EC521DE01F951029A6D2D61E373B9820795 ();
// 0x00000730 System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_m3A6677DC55026CC5AA512264155DFE1B7063191A ();
// 0x00000731 System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m0CA548C7D983A4500B0995949573E7BB5B306C17 ();
// 0x00000732 System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_m5861E3F57661E1A8A4FA327AA82FA1C6701BA297 ();
// 0x00000733 System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mD2E8E05BCE5DB4A68102650F1A73A8421CB79E7D ();
// 0x00000734 System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_m9F97D45BF2608A59C0C833C6369AA07F7E3CE4B0 ();
// 0x00000735 System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m9EEEF7BF606E6143EB14ADC81BF29B34B0692E97 ();
// 0x00000736 SimpleJSON.JSONNode SimpleJSON.JSONNode::ParseElement(System.String,System.Boolean)
extern void JSONNode_ParseElement_m2E4B9B42B7BDE678F8939103A7B16B0EDDB51556 ();
// 0x00000737 SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_mE166F5AC3F23BEA21858D3BF96FE725694E995E5 ();
// 0x00000738 System.Void SimpleJSON.JSONNode::SerializeBinary(System.IO.BinaryWriter)
// 0x00000739 System.Void SimpleJSON.JSONNode::SaveToBinaryStream(System.IO.Stream)
extern void JSONNode_SaveToBinaryStream_m801BA0D52C73ECC56F36B5D1B4B00CD54C569C8C ();
// 0x0000073A System.Void SimpleJSON.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_m47EF0165BA408039116DD9425494E275C6E44CE4 ();
// 0x0000073B System.Void SimpleJSON.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_mCC7A4E098A2C9E3EC0376864F21EB86BD970D968 ();
// 0x0000073C System.String SimpleJSON.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_mBB671040932E13A181B384601A7A7DB120AC6613 ();
// 0x0000073D System.Void SimpleJSON.JSONNode::SaveToBinaryFile(System.String)
extern void JSONNode_SaveToBinaryFile_mA67036201DC85761E63D918025560363F74F6817 ();
// 0x0000073E System.String SimpleJSON.JSONNode::SaveToBinaryBase64()
extern void JSONNode_SaveToBinaryBase64_m40440F8BA23F11C117A453618B36E1B91B05E2E7 ();
// 0x0000073F SimpleJSON.JSONNode SimpleJSON.JSONNode::DeserializeBinary(System.IO.BinaryReader)
extern void JSONNode_DeserializeBinary_m2CB021FCF7C11A10B651DFC45E8161C6FE3BA553 ();
// 0x00000740 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_m8684CBA53C1F16EDFF54BA1C0ABDA4EC03816C17 ();
// 0x00000741 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_m7628D5B9C1E825BC3FD55CB076CBFA88CA5C6B3B ();
// 0x00000742 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_mE8A5BF0AE5DA2D7D89BAC66FA467FB5F371AC6B6 ();
// 0x00000743 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryStream(System.IO.Stream)
extern void JSONNode_LoadFromBinaryStream_m15171D43A9B1C20E75FB9B5C9A8265021C6042BE ();
// 0x00000744 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryFile(System.String)
extern void JSONNode_LoadFromBinaryFile_mECE8EDF7F81DF1C69665167DE310C89D6B0F5386 ();
// 0x00000745 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryBase64(System.String)
extern void JSONNode_LoadFromBinaryBase64_mB2B8731A36BA0653B058DFCDC82BD25BE4D5D5B8 ();
// 0x00000746 SimpleJSON.JSONNode SimpleJSON.JSONNode::GetContainer(SimpleJSON.JSONContainerType)
extern void JSONNode_GetContainer_mC045258C0713687F80B9976597A1F1B2F0AF09CD ();
// 0x00000747 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector2)
extern void JSONNode_op_Implicit_m9113462A7F4D578753C4BCF2BAC0573E3C7FEAD1 ();
// 0x00000748 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector3)
extern void JSONNode_op_Implicit_mFDD4394339A55B3B4865D438A8D0B3B3C1B3C489 ();
// 0x00000749 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector4)
extern void JSONNode_op_Implicit_mECD9914EE18E579DC4D1BD3E8896AA1C07FDDF2F ();
// 0x0000074A SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Quaternion)
extern void JSONNode_op_Implicit_m31CD7EA9D614FB833BC2F9564011C24E09065D81 ();
// 0x0000074B SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Rect)
extern void JSONNode_op_Implicit_m62D6E2D3BFDBE38120835A337A26309C4B6EAFA7 ();
// 0x0000074C SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.RectOffset)
extern void JSONNode_op_Implicit_mACFF071F011662617B33FE94252FB4607F6CF768 ();
// 0x0000074D UnityEngine.Vector2 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mFD11306570BA7BB3505D63EAAD6692AC7DC486EA ();
// 0x0000074E UnityEngine.Vector3 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mBA174BF9355190BEA2E7097E92A1714BDDEF0D1B ();
// 0x0000074F UnityEngine.Vector4 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m16D35DFF53015DD806F859A05788B9CAFAC3CD4C ();
// 0x00000750 UnityEngine.Quaternion SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mF372B584DA87585A34A1B7191F2ECD9D76523FED ();
// 0x00000751 UnityEngine.Rect SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mC04609A15471AC61282014B58D94B50E853E4625 ();
// 0x00000752 UnityEngine.RectOffset SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mC07699E85A9E1181813370FD8910D3C391361737 ();
// 0x00000753 UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2(UnityEngine.Vector2)
extern void JSONNode_ReadVector2_mF8CFEBEEB5F5CE34671CE0842C0B02551FE48B18 ();
// 0x00000754 UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2(System.String,System.String)
extern void JSONNode_ReadVector2_mFF88B6348B92867DA5C4251157732CAB5B5B47EF ();
// 0x00000755 UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2()
extern void JSONNode_ReadVector2_mB2870328357B8263ACCB246CD92A8A0D478378D9 ();
// 0x00000756 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector2(UnityEngine.Vector2,System.String,System.String)
extern void JSONNode_WriteVector2_m788274FC2E65F87EA6C402EEE3CC53BBC7F3B16C ();
// 0x00000757 UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3(UnityEngine.Vector3)
extern void JSONNode_ReadVector3_m43DE5D4DAB69313FDABA7836772957133304B86F ();
// 0x00000758 UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3(System.String,System.String,System.String)
extern void JSONNode_ReadVector3_m3525AF93F091FE98ED5CEC950BE8685FADB58823 ();
// 0x00000759 UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3()
extern void JSONNode_ReadVector3_m0CDF256AF6311DD29A996A2D16C5379941BA2C53 ();
// 0x0000075A SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector3(UnityEngine.Vector3,System.String,System.String,System.String)
extern void JSONNode_WriteVector3_mFB14429368B75FB3645845DB760DCCBA75D97CB3 ();
// 0x0000075B UnityEngine.Vector4 SimpleJSON.JSONNode::ReadVector4(UnityEngine.Vector4)
extern void JSONNode_ReadVector4_m82D5CDBA10795C70B5579D0F194EBA1C50A80D04 ();
// 0x0000075C UnityEngine.Vector4 SimpleJSON.JSONNode::ReadVector4()
extern void JSONNode_ReadVector4_mC3B5F4BC65CCEFF9442226420C4F412F07411E6D ();
// 0x0000075D SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector4(UnityEngine.Vector4)
extern void JSONNode_WriteVector4_m60C2E43877B04836235CCCD130AA7E14B0C7A548 ();
// 0x0000075E UnityEngine.Quaternion SimpleJSON.JSONNode::ReadQuaternion(UnityEngine.Quaternion)
extern void JSONNode_ReadQuaternion_mC629B1F75B005F5210406DA71AF735E79D06A984 ();
// 0x0000075F UnityEngine.Quaternion SimpleJSON.JSONNode::ReadQuaternion()
extern void JSONNode_ReadQuaternion_mCCDF5602F2AD5B02F6D87AA0EB7475D24A40EE1D ();
// 0x00000760 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteQuaternion(UnityEngine.Quaternion)
extern void JSONNode_WriteQuaternion_m8A0BE1AB55D78210F61002B227FD4DDA9209DA53 ();
// 0x00000761 UnityEngine.Rect SimpleJSON.JSONNode::ReadRect(UnityEngine.Rect)
extern void JSONNode_ReadRect_mC04789622553132C464D6E2A23ED51EF29BA906F ();
// 0x00000762 UnityEngine.Rect SimpleJSON.JSONNode::ReadRect()
extern void JSONNode_ReadRect_mC4FD3AB27019DF45930FD4F63B4FE15FCD6A9A38 ();
// 0x00000763 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteRect(UnityEngine.Rect)
extern void JSONNode_WriteRect_m612BE6C780A7E2E09B864A2F7A319E9B870C91DB ();
// 0x00000764 UnityEngine.RectOffset SimpleJSON.JSONNode::ReadRectOffset(UnityEngine.RectOffset)
extern void JSONNode_ReadRectOffset_mC3F47466BFAC630661DE2A223A20311BECCFC544 ();
// 0x00000765 UnityEngine.RectOffset SimpleJSON.JSONNode::ReadRectOffset()
extern void JSONNode_ReadRectOffset_m4895D264D617FB2D4BFAC134E2315581E75F86E4 ();
// 0x00000766 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteRectOffset(UnityEngine.RectOffset)
extern void JSONNode_WriteRectOffset_m38055DE4B945CFCFFDD633AAC49EC012CB213E2D ();
// 0x00000767 UnityEngine.Matrix4x4 SimpleJSON.JSONNode::ReadMatrix()
extern void JSONNode_ReadMatrix_m3F1D25649F13760AD415BA9545B2ABAC46640CC5 ();
// 0x00000768 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteMatrix(UnityEngine.Matrix4x4)
extern void JSONNode_WriteMatrix_m032272CAA06ACF4F50D63D95D7E4AFAE4CD385DB ();
// 0x00000769 System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_mDA1406BFCE5698F9E1B5C45D166CAEADF75F155D ();
// 0x0000076A System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_m790455736E92967EBA6E8FFA0B1362926347DE4F ();
// 0x0000076B System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_m03E7B2428299099D1B36AA5E73178F483DA259A9 ();
// 0x0000076C System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_m4536E214EBF308E1B0AC178C55E3CC74BBA6E03B ();
// 0x0000076D SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_m51CD8C530354A4AEA428DC009FD95C27D4AB53CE ();
// 0x0000076E System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_mD6801BD0AF511E59314A86453C4CAD8DBDE72E61 ();
// 0x0000076F SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_mB01C410D721F8B673A311AFE4913BD2F1C8E37F9 ();
// 0x00000770 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mD189F066FE043F56F6E93A83A60D924DD847BC15 ();
// 0x00000771 System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m3B6450ABE093284900E2149194902DBF55F9F5E0 ();
// 0x00000772 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m60A4763A82D74D05A95B9B18491B15A94B3AE815 ();
// 0x00000773 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m47ABB630C7E81AB9ACC8542DCF2257B93527A383 ();
// 0x00000774 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m82BAED8423A11B317E4570FD9B95D4DC5C761E15 ();
// 0x00000775 System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_m66FC2CA7BCFB73690DD520A8F084FC48148412AF ();
// 0x00000776 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_mE574BD569800529B233BB15FFCB34F6E1632E004 ();
// 0x00000777 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_m36719C3D5868724800574C299C59A3398347D049 ();
// 0x00000778 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_mB4825990061A2E134CCB2226C37015379F9D3813 ();
// 0x00000779 System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m4F59ACBFEDB5A8D24423CA77FA960190A2FA6343 ();
// 0x0000077A System.Void SimpleJSON.JSONArray::SerializeBinary(System.IO.BinaryWriter)
extern void JSONArray_SerializeBinary_m00A6632EEB6BD975D622461EC2DDDDDA0CD3DB7D ();
// 0x0000077B System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m419E98169E6E82840EDE150C26627DAD3BC02447 ();
// 0x0000077C System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_mBD83D2F3C131B9FD58B0F3986CD918A43351F885 ();
// 0x0000077D System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_m462E287E1C98354480FF58056463B67C19884389 ();
// 0x0000077E SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m24A4D8474C6803E5B36B9C7B16CC35F48C00C486 ();
// 0x0000077F System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_mFEE416FD7C685628DCA3E797F81BD27BC1EE8348 ();
// 0x00000780 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_m89934764DE9D9DB8D007DE2FDB956B35A483CE2A ();
// 0x00000781 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_mE598F65336946DC41AB6F406AC57D2A54E457094 ();
// 0x00000782 System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m9D055BC7B0CD7C57CD0F317EC412CF75CD70F287 ();
// 0x00000783 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_mBC3297959D58E83AAB9F274DF9CED1B39624D9C1 ();
// 0x00000784 System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mD7301713F70EE5DBC64223FCBA6D5C1680390B16 ();
// 0x00000785 System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m48D5936D2F942420881CBD8F8553E951894FB7FE ();
// 0x00000786 System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m13B3F60A99B68E441677FB6CBA55A24C0E8360CA ();
// 0x00000787 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m9E4AA7514241F96E7996D4943AB92C20E94801AB ();
// 0x00000788 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m6E8D738F1570D159422CD7E7447DD133DB0DBEA2 ();
// 0x00000789 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m9943021B70272C24F3FE7D87421F3DD6E1EADB15 ();
// 0x0000078A System.Boolean SimpleJSON.JSONObject::HasKey(System.String)
extern void JSONObject_HasKey_mB270F9CDD8F8F4C6AE2D9DB57270F50248BD49D6 ();
// 0x0000078B SimpleJSON.JSONNode SimpleJSON.JSONObject::GetValueOrDefault(System.String,SimpleJSON.JSONNode)
extern void JSONObject_GetValueOrDefault_mA1655EE4E0926B292C715B26F788A992EA1F0C6D ();
// 0x0000078C System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_m395D5E98AF99D012572A6BD30560FFC988513A94 ();
// 0x0000078D System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_m00318F24352A12B9BE48DCB37144F61E9A9E7A80 ();
// 0x0000078E System.Void SimpleJSON.JSONObject::SerializeBinary(System.IO.BinaryWriter)
extern void JSONObject_SerializeBinary_mD79062C206D52C42C0E2740B5F89719547EEDF1E ();
// 0x0000078F System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_mF56546672BF37A9578123B0629537C3AC53CB3B9 ();
// 0x00000790 SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m04F409B5247C8DCD38D24808764CF24D869E6185 ();
// 0x00000791 System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_m41461FC38EC677B2A2A4723ED126565DACE3BBF9 ();
// 0x00000792 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_mF8D0B322B157094DFDC176D6B8BD0E2C4BE626E4 ();
// 0x00000793 System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m2BAAD8CD245735A369E1D5A04E5AC99FC842144F ();
// 0x00000794 System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_m1D74B9A40B791CBE98B9238BE557C27BD8817FE6 ();
// 0x00000795 System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_m688A1C2FCB9A049611A22A577D0F5F929DD16DC2 ();
// 0x00000796 System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_mEE202723B371E8D9398588A41D7027FEB98F138B ();
// 0x00000797 System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_mC77793855A6E9F114EAC12AAC463851ACCB45D6E ();
// 0x00000798 System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_m89888E65EF5DEE90FCCC9329B1DF0530A9D2330E ();
// 0x00000799 System.Void SimpleJSON.JSONString::SerializeBinary(System.IO.BinaryWriter)
extern void JSONString_SerializeBinary_m4614AA84E91FFF710A5AC1ADF43980213513F5BD ();
// 0x0000079A SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_m5810C73995BD124FB40ADDFEB7172AE1295D3C3F ();
// 0x0000079B System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_m3338ED62D7ED2856B1935977E01878B3D987C840 ();
// 0x0000079C SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m3DDB7A5F04D7C6C314D6969ECDB3314385E0F8C0 ();
// 0x0000079D System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_mCDE93C672C6D1E8603CEE081B787653116F0C5D1 ();
// 0x0000079E System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m2FA9BB60166964B7571500874EDCE169652B55C3 ();
// 0x0000079F System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m904B18C9A5EEF144DECE1F977D3052D133BAD259 ();
// 0x000007A0 System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_m59A02A71F416EE06E82DFC3286973ECC6E883F8B ();
// 0x000007A1 System.Int64 SimpleJSON.JSONNumber::get_AsLong()
extern void JSONNumber_get_AsLong_m061F72F7488EDFAB69FB74EB03E37D7E396FA29F ();
// 0x000007A2 System.Void SimpleJSON.JSONNumber::set_AsLong(System.Int64)
extern void JSONNumber_set_AsLong_mFDFD4785E74F3B6A4B57B88AF178388E304AECC5 ();
// 0x000007A3 System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_m3FDBA525AA84C626AA138395026B0C3AFB16FB4D ();
// 0x000007A4 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_m344B7D623878EE8D6249F8BF36B9BF78DE8F55AA ();
// 0x000007A5 System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_m81EFE52CD14E8796D39AA681AB060BA6008FE572 ();
// 0x000007A6 System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_mFCA17E38F4DF08CD58BE8E73D12950187D76737F ();
// 0x000007A7 System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_m7422BF9055C6B4FD465DE5C75ADD94B948DE5F06 ();
// 0x000007A8 System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_mB7291C3F22A8DF1F87D50828A431D283120730EC ();
// 0x000007A9 System.Void SimpleJSON.JSONNumber::SerializeBinary(System.IO.BinaryWriter)
extern void JSONNumber_SerializeBinary_mEED3A5E999A683B6E7BA8A7B9CBC5C302F0BBD8B ();
// 0x000007AA SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_mCBABB544B93C712D3F57CC8FBEC49260FEA9284A ();
// 0x000007AB System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_mCDD8754F4069AEBBBDD72335239B96DF0AD642E7 ();
// 0x000007AC SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_m4B8C0CC079A97EB3B0331ECFFB15F7DD0AB7A180 ();
// 0x000007AD System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_mF541D03698837F4C55687CF378DB66D6B00BC674 ();
// 0x000007AE System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_mE16685714754D7DE0E3F743629DEB474B0EFCD5A ();
// 0x000007AF System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_m39BB40C9E1DBD2670BD855394E331FFA55C12D26 ();
// 0x000007B0 System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_mDD6055ECDF4A1CF22B2D65D636356CF99142BFF9 ();
// 0x000007B1 System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_mB88FECCC74D382AE4A348C49AFB4E5C448AE8A87 ();
// 0x000007B2 System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_m52AF787FC93A51B469544D2DB188050518F3D742 ();
// 0x000007B3 System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_m4E0D9DFB665C9F9EB27B9FC0DA6083196D23CF5D ();
// 0x000007B4 System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m2DB3C7743315F4B0FA9F14D443B7E58DD19EF0AB ();
// 0x000007B5 System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m1D7BA88B9CBD6B5D399DBDC2A7886B7AA96BA618 ();
// 0x000007B6 System.Void SimpleJSON.JSONBool::SerializeBinary(System.IO.BinaryWriter)
extern void JSONBool_SerializeBinary_mE1F815089714C926FF51210566CA6E902FE776A4 ();
// 0x000007B7 SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_mFCEA43022679C084EA7BEB23AD203E1B9B33E1D7 ();
// 0x000007B8 System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_m8AF0B0484A8A9FC431DB9B6883EEF915975B5B20 ();
// 0x000007B9 SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m498F7F5444421EDA491F023F0B76AC1D1D735936 ();
// 0x000007BA System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m9C8DBDFF050BF6A89FDD24043B8A1EFF59D8ADB2 ();
// 0x000007BB SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m390B90BACB37AC50B726CE28D07463748A202A79 ();
// 0x000007BC System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m2EA54A160B17857C04FB1910DB687A46D8E7E857 ();
// 0x000007BD System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_mABE2A52BD423BFC85745E40DEA74F9F7CA7ABC54 ();
// 0x000007BE System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m78D9E804A03BDA154D1DB75AE32464E862BD33F2 ();
// 0x000007BF System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m38D6D1A90957947C3EC3A36AC2E58C827CEEF265 ();
// 0x000007C0 System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m21EC18AAC8A2178A6CEF6F64DB7B543B98E457C4 ();
// 0x000007C1 System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_mB20A1C1F9E89AF788BFF8678C06154C687A93AFF ();
// 0x000007C2 System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_m493344BBA7539C1BFEE6D6874484201D2133CC18 ();
// 0x000007C3 System.Void SimpleJSON.JSONNull::SerializeBinary(System.IO.BinaryWriter)
extern void JSONNull_SerializeBinary_m676A4B3E89715D9AADDF18FA5E5D374C06EF00A0 ();
// 0x000007C4 System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_m9DAB4F5E47D29AA56DA3E8EF2ED598C75D5062D8 ();
// 0x000007C5 SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_mC1AC02C25A03BF67F0D89FD9900CBC6EEB090A5D ();
// 0x000007C6 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_mDAA175BC448F491BD873169D142E119DF6733ED6 ();
// 0x000007C7 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_mA570856315FC9EEEE91DAF46A48EB14C6826EC4A ();
// 0x000007C8 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_m9CDA16B464A5E27785A473454576FA84E46A073C ();
// 0x000007C9 T SimpleJSON.JSONLazyCreator::Set(T)
// 0x000007CA SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_mF6E7B8A894C9EA1C74F824B0A2A7C87E0C8FA7E6 ();
// 0x000007CB System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m499E09B3FBE0202D91CE3CFD6BF358AD4ED04CBD ();
// 0x000007CC SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_mB8E238EEBD247A0812FE72F3A4282ECDCAC03E3D ();
// 0x000007CD System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m3D3E4624F5FE38306E1B4F3E920DFBFCA7F436DA ();
// 0x000007CE System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_mBFCD7C0BCB389E8A82D9215DCF56160C5FA8C7D9 ();
// 0x000007CF System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m8F2A319671C485DC69149CAB865561800D78B2AE ();
// 0x000007D0 System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_m067147F94A931909B66E17538F5260449CE37E35 ();
// 0x000007D1 System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m8C8F222FA5074241144AC5687C6C366CB1381474 ();
// 0x000007D2 System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_mAF996627BB20C25E8498ED9CDABDF356083C2E5D ();
// 0x000007D3 System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_mB426CFC7DAF4BF8840B2D6FB42C0A32E2E24E467 ();
// 0x000007D4 System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m2386A4715579021A9945D3A20A592F9DC11F8B41 ();
// 0x000007D5 System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_mD0ADB2CC1876C83289C11C423CE4D18BD265B02F ();
// 0x000007D6 System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m64B9AE4A38878105DC50B040593E18DF635EB299 ();
// 0x000007D7 System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_mDD395E8C6D3F5BACEE1B8FBC51EEBC6539673CA9 ();
// 0x000007D8 System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m98C72926205FC366C09E20FA8A76F072194CFE00 ();
// 0x000007D9 System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m36E2A3EC65F2A7322E897FF97F9D5332B67FF130 ();
// 0x000007DA System.Int64 SimpleJSON.JSONLazyCreator::get_AsLong()
extern void JSONLazyCreator_get_AsLong_mE9FA02797096C099BC226988CA5C9220A2D00FB4 ();
// 0x000007DB System.Void SimpleJSON.JSONLazyCreator::set_AsLong(System.Int64)
extern void JSONLazyCreator_set_AsLong_mE6DC7B42DF99DC9128EFFBB044F8604F95D41476 ();
// 0x000007DC System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_m6393AB23401275975AB2E6663FF8EC904648A0C7 ();
// 0x000007DD System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m00EBC804C6B2847280FBB7D07D1A7615BFFE8AD0 ();
// 0x000007DE SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_m7ED16496F0BC83E265C51066E586108D22850C5D ();
// 0x000007DF SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m49378AF7ED532AAB2664C02C21E537630528B9C2 ();
// 0x000007E0 System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m7092C7335B29323434A169BB19440B45FD62DA34 ();
// 0x000007E1 System.Void SimpleJSON.JSONLazyCreator::SerializeBinary(System.IO.BinaryWriter)
extern void JSONLazyCreator_SerializeBinary_m1B326A3B5172608A297970C38F6C1CE6DA8C6BCB ();
// 0x000007E2 SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_m76895CDE48710A8477CB7E932635FFFC3B0BD5AE ();
// 0x000007E3 System.String EasyButtons.ButtonAttribute::get_Name()
extern void ButtonAttribute_get_Name_mF7725CF6B38706C1C0F6342A023062E3B77D446E ();
// 0x000007E4 EasyButtons.ButtonMode EasyButtons.ButtonAttribute::get_Mode()
extern void ButtonAttribute_get_Mode_m5F60CB7BFB8BAF1DE24DBD937FBD475FB310E86B ();
// 0x000007E5 System.Void EasyButtons.ButtonAttribute::.ctor()
extern void ButtonAttribute__ctor_m759C496E4B71385B76C568C729B6E841D74FB6E8 ();
// 0x000007E6 System.Void EasyButtons.ButtonAttribute::.ctor(System.String)
extern void ButtonAttribute__ctor_mBBF697374FA5A534874A0BC9EEE3241AEA3EE623 ();
// 0x000007E7 System.Void EasyButtons.ButtonAttribute::.ctor(System.String,EasyButtons.ButtonMode)
extern void ButtonAttribute__ctor_mD688CA6FFC6CA5FB2F616B7FFF1523A992F0370C ();
// 0x000007E8 System.Void EasyButtons.ButtonAttribute::.ctor(EasyButtons.ButtonMode)
extern void ButtonAttribute__ctor_m44B86987627520D9AC2E02E5DDFB61CAB4AC5722 ();
// 0x000007E9 System.Void EasyButtons.ButtonsExample::SayMyName()
extern void ButtonsExample_SayMyName_mC4E0C0C9EECCE2A82441B74AAD62AE0989436AF1 ();
// 0x000007EA System.Void EasyButtons.ButtonsExample::SayHelloEditor()
extern void ButtonsExample_SayHelloEditor_m941661705AE6D661DB2593A015724410FC28C64C ();
// 0x000007EB System.Void EasyButtons.ButtonsExample::SayHelloInRuntime()
extern void ButtonsExample_SayHelloInRuntime_mE617F877A04E86615BFC6E37619265B440F4A16F ();
// 0x000007EC System.Void EasyButtons.ButtonsExample::TestButtonName()
extern void ButtonsExample_TestButtonName_mD0065BEEEB402F88167290EA4CC4194C9B9613C8 ();
// 0x000007ED System.Void EasyButtons.ButtonsExample::TestButtonNameEditorOnly()
extern void ButtonsExample_TestButtonNameEditorOnly_m3487DA3DE75C25CA1C77F6927A2D507A750C5F6C ();
// 0x000007EE System.Void EasyButtons.ButtonsExample::.ctor()
extern void ButtonsExample__ctor_mF59A3B0B71987AF3410778FBE766AE778E72FE83 ();
// 0x000007EF System.Void EasyButtons.CustomEditorButtonsExample::SayHello()
extern void CustomEditorButtonsExample_SayHello_mD5A217BB8B801C976BC381A301F7DA6D5224C483 ();
// 0x000007F0 System.Void EasyButtons.CustomEditorButtonsExample::.ctor()
extern void CustomEditorButtonsExample__ctor_mC83D9B21212851DBEEDFE69ACC9E3FC801DFC0E8 ();
// 0x000007F1 DefaultUtility.CurveData DefaultUtility.UIRotateObject::get_Curve()
extern void UIRotateObject_get_Curve_mB5AE3ACF2EB4B86FAD61B3D650E89476F7914B01 ();
// 0x000007F2 System.Void DefaultUtility.UIRotateObject::set_Curve(DefaultUtility.CurveData)
extern void UIRotateObject_set_Curve_m40CF0BEA12FEAA5FF4DE4DE0F06053CF7CA42EC1 ();
// 0x000007F3 System.Void DefaultUtility.UIRotateObject::add_startAction(System.Action)
extern void UIRotateObject_add_startAction_m3CC8E6D28C3E90135BB1394FF6BF8B21BD55C585 ();
// 0x000007F4 System.Void DefaultUtility.UIRotateObject::remove_startAction(System.Action)
extern void UIRotateObject_remove_startAction_mA4C3DA5603DC70AF008CA13D6C157F93AC70253A ();
// 0x000007F5 System.Void DefaultUtility.UIRotateObject::add_finishAction(System.Action)
extern void UIRotateObject_add_finishAction_mF98DFABF410CD8DE4E79B5F37014DBA571874C6C ();
// 0x000007F6 System.Void DefaultUtility.UIRotateObject::remove_finishAction(System.Action)
extern void UIRotateObject_remove_finishAction_m4D698EF2F803F12B2148220A2D47F84DB1BAAFA7 ();
// 0x000007F7 System.Void DefaultUtility.UIRotateObject::Awake()
extern void UIRotateObject_Awake_mD8327B0BF909A88D4494CAA44CBF3E8404DB9619 ();
// 0x000007F8 System.Void DefaultUtility.UIRotateObject::OnEnable()
extern void UIRotateObject_OnEnable_mEF506152B6D694B0465734881AEB2BC3B05988CE ();
// 0x000007F9 System.Void DefaultUtility.UIRotateObject::StopCoroutine()
extern void UIRotateObject_StopCoroutine_m008FD2E153BF8AA0717E5CFC8BDE29506A2D2867 ();
// 0x000007FA System.Void DefaultUtility.UIRotateObject::StartRotation(System.Single)
extern void UIRotateObject_StartRotation_m1B3E8C6FCF897367BCC55BC6B61CBD977094183D ();
// 0x000007FB System.Void DefaultUtility.UIRotateObject::StartRotation()
extern void UIRotateObject_StartRotation_mD8EC12A40EFD50EF86276CC33BD4E4FF2E0696C7 ();
// 0x000007FC System.Void DefaultUtility.UIRotateObject::Rotate(System.Single,System.Boolean)
extern void UIRotateObject_Rotate_m0E0043200519E676796BD82B183222FD58002AEB ();
// 0x000007FD System.Collections.IEnumerator DefaultUtility.UIRotateObject::RotateLoop(System.Single)
extern void UIRotateObject_RotateLoop_m4A706C05451677CDAB0934C198B5212C882BA885 ();
// 0x000007FE System.Void DefaultUtility.UIRotateObject::OnValidate()
extern void UIRotateObject_OnValidate_m6D842D8F3940E31D8CE6C1AE4ECEE54FE9C20DA4 ();
// 0x000007FF System.Void DefaultUtility.UIRotateObject::.ctor()
extern void UIRotateObject__ctor_m21B38074A2ABE379BF52B2697202CA832A602661 ();
// 0x00000800 System.Void DefaultUtility.UIScalableObject::add_finishAction(System.Action)
extern void UIScalableObject_add_finishAction_mF27D4C5B5E241D20157DAB7F6712DBF84ADBC877 ();
// 0x00000801 System.Void DefaultUtility.UIScalableObject::remove_finishAction(System.Action)
extern void UIScalableObject_remove_finishAction_mFA146BF2630633853CD0D28BA2B0024B52C57FE8 ();
// 0x00000802 System.Void DefaultUtility.UIScalableObject::Awake()
extern void UIScalableObject_Awake_mB26F4F6D852F8916D8F0949FFC033307840476F9 ();
// 0x00000803 System.Void DefaultUtility.UIScalableObject::OnEnable()
extern void UIScalableObject_OnEnable_m3E1CE351D3DE9C547E968C12395D86590BB0DC81 ();
// 0x00000804 System.Void DefaultUtility.UIScalableObject::StopCoroutine()
extern void UIScalableObject_StopCoroutine_mFB9FBBF0D702528505506D6CA4D39B9A69405C50 ();
// 0x00000805 System.Void DefaultUtility.UIScalableObject::StartScaling(System.Single)
extern void UIScalableObject_StartScaling_m08A0DB58B07B098A8D235B4D81BC7F6E6A536951 ();
// 0x00000806 System.Void DefaultUtility.UIScalableObject::StartScaling()
extern void UIScalableObject_StartScaling_m70B9F1FC2030D4918FB6167C91D35A5F340E0CEF ();
// 0x00000807 System.Void DefaultUtility.UIScalableObject::Scale(System.Single)
extern void UIScalableObject_Scale_m921CE5B6F5B470683C24B7EC28ACB80C747A5BBF ();
// 0x00000808 System.Collections.IEnumerator DefaultUtility.UIScalableObject::ScaleLoop(System.Single)
extern void UIScalableObject_ScaleLoop_mB6F450C4A0D67ED0A0D4EF0AD53B197E71B43046 ();
// 0x00000809 System.Void DefaultUtility.UIScalableObject::.ctor()
extern void UIScalableObject__ctor_mE7FA08AE154F0CFF9CE8560BCA39D6B9623BA670 ();
// 0x0000080A System.Single DefaultUtility.UIMovableObject::get_CurrentDuration()
extern void UIMovableObject_get_CurrentDuration_mECA77F79D9489E7E442D6BAA44094093E948762F ();
// 0x0000080B System.Single DefaultUtility.UIMovableObject::get_MoveTowardsSpeed()
extern void UIMovableObject_get_MoveTowardsSpeed_mD203795C17E7FA8918F5445BC79A4CFC019AAAA0 ();
// 0x0000080C System.Void DefaultUtility.UIMovableObject::add_startMove(System.Action)
extern void UIMovableObject_add_startMove_mFE47D3DB4B82524EC7B354B03F9C76F77A2E97ED ();
// 0x0000080D System.Void DefaultUtility.UIMovableObject::remove_startMove(System.Action)
extern void UIMovableObject_remove_startMove_m05F82C82FC799675B3368F3E1B4836886C468527 ();
// 0x0000080E System.Void DefaultUtility.UIMovableObject::add_stopMove(System.Action)
extern void UIMovableObject_add_stopMove_mD2DFE25A8DFE82FE7E877F7E3079BFCEB9641BF0 ();
// 0x0000080F System.Void DefaultUtility.UIMovableObject::remove_stopMove(System.Action)
extern void UIMovableObject_remove_stopMove_m494E89320A68C602E7CA61FD09902F66F00FD322 ();
// 0x00000810 UnityEngine.Vector3 DefaultUtility.UIMovableObject::get_FinalPosition()
extern void UIMovableObject_get_FinalPosition_mF39BBDD1D7F50F3361C5CDB47D8481E9348811CE ();
// 0x00000811 System.Void DefaultUtility.UIMovableObject::Awake()
extern void UIMovableObject_Awake_m0B28E3C7C0EA12D04AD73006AD12422312586E9C ();
// 0x00000812 System.Void DefaultUtility.UIMovableObject::Create(UnityEngine.Vector3,UnityEngine.Transform,System.Single)
extern void UIMovableObject_Create_m7BD079B32A5B847F2E1D545F10C4CE555CD69BB2 ();
// 0x00000813 System.Void DefaultUtility.UIMovableObject::Create(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void UIMovableObject_Create_m2A88BD5AE9274AEB81CBC39F2A79EF26326D72F5 ();
// 0x00000814 System.Void DefaultUtility.UIMovableObject::Create(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,DefaultUtility.CurveData)
extern void UIMovableObject_Create_m6CC0D6F90B9AA0296789537E1E7412B34077396A ();
// 0x00000815 System.Void DefaultUtility.UIMovableObject::Create(UnityEngine.Vector3,UnityEngine.Transform,System.Single,DefaultUtility.CurveData)
extern void UIMovableObject_Create_mB10DADB394CDC48B71CDD54FB1153614759DABFC ();
// 0x00000816 System.Void DefaultUtility.UIMovableObject::StartMovement()
extern void UIMovableObject_StartMovement_m97AF06C08D023888457B84BB5793F42C2908C00A ();
// 0x00000817 System.Void DefaultUtility.UIMovableObject::StopMovement()
extern void UIMovableObject_StopMovement_m7FA4C1362C4EB23FCCBCF6294E2D76A78B8CE2B4 ();
// 0x00000818 System.Boolean DefaultUtility.UIMovableObject::get_IsFinish()
extern void UIMovableObject_get_IsFinish_m698DCE6D5666A48DE91C9A10D4CCAF6DECDF0722 ();
// 0x00000819 System.Void DefaultUtility.UIMovableObject::Move(System.Single)
extern void UIMovableObject_Move_m0C215DA795627A24E53E3FBDEE2C774B9606C57E ();
// 0x0000081A System.Void DefaultUtility.UIMovableObject::SetPosition(UnityEngine.Vector3)
extern void UIMovableObject_SetPosition_mC69F9C8AE2B30332CAB004BF7A342B84DBE119A1 ();
// 0x0000081B System.Void DefaultUtility.UIMovableObject::.ctor()
extern void UIMovableObject__ctor_mE4DFC347F6C48CEBA65154A8378DC2EF52C25C16 ();
// 0x0000081C DefaultUtility.UIMovableObject DefaultUtility.UIMoveObjectToPointController::CreateMovableObject(UnityEngine.Vector3)
extern void UIMoveObjectToPointController_CreateMovableObject_mC6F1C4D3B25A40CD0E4B5BEF13E53C949EBE3E6D ();
// 0x0000081D DefaultUtility.UIMovableObject DefaultUtility.UIMoveObjectToPointController::CreateMovableObject(UnityEngine.Vector3,UnityEngine.Transform)
extern void UIMoveObjectToPointController_CreateMovableObject_m510B7D8878201C19512EE46D92EA69A09870BBDA ();
// 0x0000081E System.Void DefaultUtility.UIMoveObjectToPointController::InitStartEvents(DefaultUtility.UIMovableObject)
extern void UIMoveObjectToPointController_InitStartEvents_mCA94CD706E8EDB2E7965C8290E415AC0C2B3ACCC ();
// 0x0000081F System.Void DefaultUtility.UIMoveObjectToPointController::InitStopEvents(DefaultUtility.UIMovableObject)
extern void UIMoveObjectToPointController_InitStopEvents_m77F2D8CD49D05BB5AA93CF0384FB069F34B38664 ();
// 0x00000820 System.Void DefaultUtility.UIMoveObjectToPointController::InitPositions(DefaultUtility.UIMovableObject,UnityEngine.Vector3,UnityEngine.Transform)
extern void UIMoveObjectToPointController_InitPositions_mDF68CBB9B163D0F623740307712C9631E52B24E3 ();
// 0x00000821 System.Void DefaultUtility.UIMoveObjectToPointController::StartMovement(DefaultUtility.UIMovableObject)
extern void UIMoveObjectToPointController_StartMovement_m0AAFC5563FE70DEA202571AB7A28A5AF3BD728F2 ();
// 0x00000822 System.Collections.IEnumerator DefaultUtility.UIMoveObjectToPointController::MoveCoinToScore(DefaultUtility.UIMovableObject,System.Single)
extern void UIMoveObjectToPointController_MoveCoinToScore_m64C03844127FA2A26A0B81FE965A310860FFF552 ();
// 0x00000823 System.Void DefaultUtility.UIMoveObjectToPointController::.ctor()
extern void UIMoveObjectToPointController__ctor_m3D9959CE0D5108F39AEAFACBE1D7F5C4161C3296 ();
// 0x00000824 System.Void DefaultUtility.AbstractScroll::CheckComponents()
// 0x00000825 System.Void DefaultUtility.AbstractScroll::InitScrollContainer(System.Collections.Generic.List`1<T>)
// 0x00000826 System.Void DefaultUtility.AbstractScroll::.ctor()
extern void AbstractScroll__ctor_mC935C38D5DD3B0BECC1736AF8EE5FEB364280AFF ();
// 0x00000827 System.Void DefaultUtility.ScrollElementData::.ctor()
extern void ScrollElementData__ctor_m45660941DD7309B83F569D9E80E9C843996CADDF ();
// 0x00000828 System.Boolean DefaultUtility.IElement::get_IsSelected()
extern void IElement_get_IsSelected_m0A39C1E8DF628F2C4E26449E19235BF6F1F4652B ();
// 0x00000829 System.Void DefaultUtility.IElement::set_IsSelected(System.Boolean)
extern void IElement_set_IsSelected_m6C8BC8452C4F36F18F2ED1D6117196B0CE0AFBFC ();
// 0x0000082A System.Void DefaultUtility.IElement::Init(System.Object)
extern void IElement_Init_m8B3C0A77850DFCCE979C36DD5185951781840F91 ();
// 0x0000082B System.Void DefaultUtility.IElement::Select()
// 0x0000082C System.Void DefaultUtility.IElement::UnSelect()
// 0x0000082D System.Void DefaultUtility.IElement::Click()
// 0x0000082E System.Void DefaultUtility.IElement::.ctor()
extern void IElement__ctor_m32DDCC7F6E8C214E3D3101A2C8E5CDB247DC23AD ();
// 0x0000082F System.Void DefaultUtility.ScrollElementButtonController::Init(System.Object)
extern void ScrollElementButtonController_Init_m7BCBD96360E11A2E50841959A5EB6AFB697D18F7 ();
// 0x00000830 System.Void DefaultUtility.ScrollElementButtonController::Click()
extern void ScrollElementButtonController_Click_m0347A532C48F7538C76D1DC8E89C47F58B0A18B8 ();
// 0x00000831 System.Void DefaultUtility.ScrollElementButtonController::Select()
extern void ScrollElementButtonController_Select_mA50C374E4B60DB29CA8134DFC80A3F9412F5EF45 ();
// 0x00000832 System.Void DefaultUtility.ScrollElementButtonController::UnSelect()
extern void ScrollElementButtonController_UnSelect_mC59037156AD407C7821BD38CB7A2AE90DD590B22 ();
// 0x00000833 System.Void DefaultUtility.ScrollElementButtonController::.ctor()
extern void ScrollElementButtonController__ctor_mCAF90DA09B3B7E4D40F172482B8BCA43567026D8 ();
// 0x00000834 System.Void DefaultUtility.ScrollElementExampleController::Click()
extern void ScrollElementExampleController_Click_m246511A2BFDDDB07AE2B83B4DDB421A7A152347F ();
// 0x00000835 System.Void DefaultUtility.ScrollElementExampleController::Select()
extern void ScrollElementExampleController_Select_m56F810BB02473138B5044780BB33DBB147598A0E ();
// 0x00000836 System.Void DefaultUtility.ScrollElementExampleController::UnSelect()
extern void ScrollElementExampleController_UnSelect_m27536927689A236F98C13EE6FB92B2B5D26E86FE ();
// 0x00000837 System.Void DefaultUtility.ScrollElementExampleController::.ctor()
extern void ScrollElementExampleController__ctor_m17F51D9E06A8642C140934ADC5EE6FD090E33D1B ();
// 0x00000838 System.Void DefaultUtility.ScrollController::CheckComponents()
extern void ScrollController_CheckComponents_m49DD4F19A40010EB18AF48B09C5EE70E1EF167F8 ();
// 0x00000839 System.Void DefaultUtility.ScrollController::HandleSwipe()
extern void ScrollController_HandleSwipe_mEE12D5F0C32D86F1EF21A75A205A952EA5331A97 ();
// 0x0000083A System.Void DefaultUtility.ScrollController::DefaultState()
extern void ScrollController_DefaultState_mF2C5245E2AC2B3562C2FCF2CC8E24159F35888F3 ();
// 0x0000083B System.Void DefaultUtility.ScrollController::MoveState()
extern void ScrollController_MoveState_mD3519B1F0ECD7844B962C80605D80489685E2D0C ();
// 0x0000083C System.Void DefaultUtility.ScrollController::.ctor()
extern void ScrollController__ctor_m40A0E554998398C3F444174D28AC98BC6F179A1A ();
// 0x0000083D System.Void DefaultUtility.ScrollExampleInitializer::Start()
extern void ScrollExampleInitializer_Start_m10731D246D0F216AE1A7A96846A24306E725263D ();
// 0x0000083E System.Void DefaultUtility.ScrollExampleInitializer::Init()
extern void ScrollExampleInitializer_Init_m8F8D0B0BE0BCCA49C2AB1AFE189779560695E697 ();
// 0x0000083F System.Void DefaultUtility.ScrollExampleInitializer::OnValidate()
extern void ScrollExampleInitializer_OnValidate_m05D9B9EB3F60AD15995D49BDB5B5AE26D42DE73E ();
// 0x00000840 System.Void DefaultUtility.ScrollExampleInitializer::.ctor()
extern void ScrollExampleInitializer__ctor_m21DF79F062B0BD380DEA34692690A4BCBB612E24 ();
// 0x00000841 System.Collections.Generic.List`1<DefaultUtility.IElement> DefaultUtility.TriggerScrollController::get_Elements()
extern void TriggerScrollController_get_Elements_mE4A8F791B934FFF6DB18AFD5E25AF66DB08776E1 ();
// 0x00000842 System.Int32 DefaultUtility.TriggerScrollController::get_CountElements()
extern void TriggerScrollController_get_CountElements_m1CC2C74788E0C2CB8B3B0C857464B21A42157311 ();
// 0x00000843 System.Single DefaultUtility.TriggerScrollController::get_MinDistance()
extern void TriggerScrollController_get_MinDistance_m3F23FFFA24E14D211E1E0E25975D1E4511C4C0A0 ();
// 0x00000844 System.Int32 DefaultUtility.TriggerScrollController::get_SelectedElementId()
extern void TriggerScrollController_get_SelectedElementId_m481FFA2596F23F6B5CD91A2FC72CB540E1C2624A ();
// 0x00000845 System.Void DefaultUtility.TriggerScrollController::set_SelectedElementId(System.Int32)
extern void TriggerScrollController_set_SelectedElementId_mD591358BCF464AA0A159D11659A29E7A339FA754 ();
// 0x00000846 DefaultUtility.IElement DefaultUtility.TriggerScrollController::get_SelectedElement()
extern void TriggerScrollController_get_SelectedElement_mCD3957BC3C7A5CDB1BFE1D702FCA7358A91B34AD ();
// 0x00000847 System.Boolean DefaultUtility.TriggerScrollController::get_Drag()
extern void TriggerScrollController_get_Drag_m6A2950AD1EB59A1D679A0EB6F0E619B6C8152812 ();
// 0x00000848 System.Void DefaultUtility.TriggerScrollController::set_Drag(System.Boolean)
extern void TriggerScrollController_set_Drag_m8067BE708B2B253C3A17882B2E339A61C6393E47 ();
// 0x00000849 System.Boolean DefaultUtility.TriggerScrollController::get_Active()
extern void TriggerScrollController_get_Active_m862C2798610A6FEE9B12CB4535BA40C08FADBD06 ();
// 0x0000084A System.Void DefaultUtility.TriggerScrollController::set_Active(System.Boolean)
extern void TriggerScrollController_set_Active_mF60A20F04970094925D9147DA75252836304BFCD ();
// 0x0000084B System.Void DefaultUtility.TriggerScrollController::Awake()
extern void TriggerScrollController_Awake_m628FA786CAA88DF43C11572B066029FBEAB29A84 ();
// 0x0000084C System.Void DefaultUtility.TriggerScrollController::Start()
extern void TriggerScrollController_Start_m92229D42DDA141F47BB2FE1326C6F6B5E4087A07 ();
// 0x0000084D System.Void DefaultUtility.TriggerScrollController::CheckComponents()
extern void TriggerScrollController_CheckComponents_m8AC90D8B264FE6B5A5449AA6632597A511CF2C70 ();
// 0x0000084E System.Void DefaultUtility.TriggerScrollController::CheckImage()
extern void TriggerScrollController_CheckImage_m17AFB7A9202D85FF5BC89CE12722F96FD9EECF7D ();
// 0x0000084F System.Void DefaultUtility.TriggerScrollController::CheckEventTrigger()
extern void TriggerScrollController_CheckEventTrigger_m99C8A8A65A28FDEAE9FB9E8B42A4EADF1339526A ();
// 0x00000850 System.Void DefaultUtility.TriggerScrollController::InitScrollContainer(System.Collections.Generic.List`1<T>)
// 0x00000851 System.Void DefaultUtility.TriggerScrollController::FixedUpdate()
extern void TriggerScrollController_FixedUpdate_m6541420A12F484EB3E26D182B8129D498EE10E65 ();
// 0x00000852 System.Void DefaultUtility.TriggerScrollController::DefaultState()
extern void TriggerScrollController_DefaultState_mD3399393E5B650A10341478ADC180EB8385D9A99 ();
// 0x00000853 System.Void DefaultUtility.TriggerScrollController::MoveState()
extern void TriggerScrollController_MoveState_mB35C1E9AA92ABD64F092386DF53A66E4EF310556 ();
// 0x00000854 System.Void DefaultUtility.TriggerScrollController::MoveToNext()
extern void TriggerScrollController_MoveToNext_mE23337021EAF800F4BA4B5747E5F3836B88F1E1B ();
// 0x00000855 System.Void DefaultUtility.TriggerScrollController::MoveToConcrete()
extern void TriggerScrollController_MoveToConcrete_m0723CDB70A87AF4D5FE445BA8D357E06CEED5161 ();
// 0x00000856 System.Void DefaultUtility.TriggerScrollController::OnDrag()
extern void TriggerScrollController_OnDrag_mAD3DFDFF6C340C96A10B2603580CFA3ACB66EADA ();
// 0x00000857 System.Void DefaultUtility.TriggerScrollController::TouchDown()
extern void TriggerScrollController_TouchDown_m6BB0417DF5914EDE0407F6F2EDE62501EC686D0F ();
// 0x00000858 System.Void DefaultUtility.TriggerScrollController::TouchUp()
extern void TriggerScrollController_TouchUp_m3003243DB76931E436C20905167DFF9FF89771B7 ();
// 0x00000859 System.Void DefaultUtility.TriggerScrollController::StartDrag()
extern void TriggerScrollController_StartDrag_mBAD1C9AAE2551D52735AE261A2A621340C150F86 ();
// 0x0000085A System.Void DefaultUtility.TriggerScrollController::BeginDrag()
extern void TriggerScrollController_BeginDrag_m2AA63696C9A6E8B5EE02D3A8F9AF4CC7EA8B0906 ();
// 0x0000085B System.Void DefaultUtility.TriggerScrollController::EndDrag()
extern void TriggerScrollController_EndDrag_mD551842CFBE25C13DFADC4F6674006C501A3562C ();
// 0x0000085C System.Void DefaultUtility.TriggerScrollController::MoveScrollContainer(System.Single)
extern void TriggerScrollController_MoveScrollContainer_mC3C2F5744F62CFE89B44C9C47EC26DC853D722D1 ();
// 0x0000085D System.Void DefaultUtility.TriggerScrollController::SetState(DefaultUtility.ScrollState)
extern void TriggerScrollController_SetState_m4881FC003EAB2E1B5CE20BF996CD94C45390A8F2 ();
// 0x0000085E System.Void DefaultUtility.TriggerScrollController::SetIndexAndPosition(System.Int32)
extern void TriggerScrollController_SetIndexAndPosition_m412D0719B9A47B345190AFDA07E2B1DA1442BB26 ();
// 0x0000085F System.Void DefaultUtility.TriggerScrollController::ClaculateDistance()
extern void TriggerScrollController_ClaculateDistance_mEFE9EC7843ACBE526FFEF3AD60821EF9B26ACD4F ();
// 0x00000860 System.Void DefaultUtility.TriggerScrollController::UpdateSelectedElement()
extern void TriggerScrollController_UpdateSelectedElement_mD1DE1A9306718A5EC779021754FD688A96274663 ();
// 0x00000861 System.Int32 DefaultUtility.TriggerScrollController::GetSelectedIndex(System.Single&)
extern void TriggerScrollController_GetSelectedIndex_mF7CD11EE645B7A8EC703B78CFD32A4C348AE46AF ();
// 0x00000862 System.Void DefaultUtility.TriggerScrollController::LerpToElement(System.Int32)
extern void TriggerScrollController_LerpToElement_m9F61BDF5DD05E9CE149C5F46C7A7E1F5B69CD4C7 ();
// 0x00000863 System.Single DefaultUtility.TriggerScrollController::GetIndexPosition(System.Int32)
extern void TriggerScrollController_GetIndexPosition_m796B0FA0D1A39BB9171999B71E823BD90F4DAD6A ();
// 0x00000864 System.Void DefaultUtility.TriggerScrollController::UpdatePosition(System.Single)
extern void TriggerScrollController_UpdatePosition_mFE69FA2729B98AD1F90B4F7494F11BA2A1B0E9D2 ();
// 0x00000865 System.Void DefaultUtility.TriggerScrollController::.ctor()
extern void TriggerScrollController__ctor_m7BF8CFD7E74CFF907C256D8F32418CB85BF9422D ();
// 0x00000866 System.Void DefaultUtility.TriggerScrollController::<CheckEventTrigger>b__38_0(UnityEngine.EventSystems.BaseEventData)
extern void TriggerScrollController_U3CCheckEventTriggerU3Eb__38_0_m9188476274C4673302665F482BB790A6355D167C ();
// 0x00000867 System.Void DefaultUtility.TriggerScrollController::<CheckEventTrigger>b__38_1(UnityEngine.EventSystems.BaseEventData)
extern void TriggerScrollController_U3CCheckEventTriggerU3Eb__38_1_m5BC46B320B9104F72CBC76FB01B8933D644E1BBF ();
// 0x00000868 System.Void DefaultUtility.TriggerScrollController::<CheckEventTrigger>b__38_2(UnityEngine.EventSystems.BaseEventData)
extern void TriggerScrollController_U3CCheckEventTriggerU3Eb__38_2_m074165D1EA30BFC7F1A392CCD33639EFDD025EE4 ();
// 0x00000869 System.Void DefaultUtility.CoroutineUtility::WaitAndStart(UnityEngine.MonoBehaviour,System.Action,System.Single)
extern void CoroutineUtility_WaitAndStart_m96BE4B6BCF6F0D5DFFA971D00575B7B14C4702AC ();
// 0x0000086A System.Collections.IEnumerator DefaultUtility.CoroutineUtility::WaitAndStartCoroutine(System.Action,System.Single)
extern void CoroutineUtility_WaitAndStartCoroutine_m9505F1118FC727B5690979B36B6A88B2EAFCD080 ();
// 0x0000086B UnityEngine.AnimationCurve DefaultUtility.CurveData::get_Curve()
extern void CurveData_get_Curve_m407AED502C77E75FD1B81065397B557240B122CB ();
// 0x0000086C System.Single DefaultUtility.CurveData::GetValue(System.Single)
extern void CurveData_GetValue_m35BEFF2CD70F711196628D9792B8055DE2E0FB0F ();
// 0x0000086D System.Single DefaultUtility.CurveData::GetDltValue(System.Single)
extern void CurveData_GetDltValue_mBFAA6440789AA4794AFB5740E0CD290F6B74BBF3 ();
// 0x0000086E System.Void DefaultUtility.CurveData::.ctor()
extern void CurveData__ctor_mD259DA00F28DCD22F318C47D75FFA466EAE0D26A ();
// 0x0000086F System.Void DefaultUtility.CurvesData::.ctor()
extern void CurvesData__ctor_m88BF4709A62ED5A97CB24607C7539B0406ED1AF2 ();
// 0x00000870 System.Void DefaultUtility.DailyVisit::SessionStarted()
extern void DailyVisit_SessionStarted_mF9AF2A53086120AB1A1A4BCAD12763AFCF25319C ();
// 0x00000871 System.Int32 DefaultUtility.DailyVisit::get_NumberDaysVisited()
extern void DailyVisit_get_NumberDaysVisited_m9F1B6F6367AE1B89EAFE5670D2E7C4BBFF822C6E ();
// 0x00000872 System.Void DefaultUtility.DailyVisit::set_NumberDaysVisited(System.Int32)
extern void DailyVisit_set_NumberDaysVisited_mCEDC1B088E1389BDDA81F97DE2ABFCD07B4A2BA4 ();
// 0x00000873 System.Int32 DefaultUtility.DailyVisit::get_NumberDaysVisitedInARow()
extern void DailyVisit_get_NumberDaysVisitedInARow_m0CFA417FAE21E0E513237BF853211F04D215AF28 ();
// 0x00000874 System.Void DefaultUtility.DailyVisit::set_NumberDaysVisitedInARow(System.Int32)
extern void DailyVisit_set_NumberDaysVisitedInARow_mD561A63B1E7134277B9C9947A723B0863DCE058A ();
// 0x00000875 System.Int32 DefaultUtility.DailyVisit::get_NumberDaysSinceTheFirstLaunch()
extern void DailyVisit_get_NumberDaysSinceTheFirstLaunch_m7BBA90613B773B2546C7DFBCD6FA0BC8AA761058 ();
// 0x00000876 System.Int32 DefaultUtility.DailyVisit::GetNumberDaysVisited(UnityEngine.MonoBehaviour)
extern void DailyVisit_GetNumberDaysVisited_m21785AAFD6E9F25D359AE44DDD6E71EB03ED6C6E ();
// 0x00000877 System.Int32 DefaultUtility.DailyVisit::GetNumberDaysVisitedInRow(UnityEngine.MonoBehaviour)
extern void DailyVisit_GetNumberDaysVisitedInRow_m77A11F16D4BF235B9AD4A531A90BBEF8FABAFEBB ();
// 0x00000878 System.Int32 DefaultUtility.DailyVisit::GetNumberDaysSinceTheFirstLaunch(UnityEngine.MonoBehaviour)
extern void DailyVisit_GetNumberDaysSinceTheFirstLaunch_mE6E0CD63DF6525DD0CEC91C3B19E3B28CF87509C ();
// 0x00000879 System.DateTime DefaultUtility.DailyVisit::GetLastVisitDate(UnityEngine.MonoBehaviour)
extern void DailyVisit_GetLastVisitDate_m775B0B4B019BDF7A29D5F110590625BD436AF7C0 ();
// 0x0000087A System.DateTime DefaultUtility.DailyVisit::get_LastVisitDate()
extern void DailyVisit_get_LastVisitDate_mB38A9E77167D8246065A31E714D48C1E0CD97387 ();
// 0x0000087B System.Void DefaultUtility.DailyVisit::set_LastVisitDate(System.DateTime)
extern void DailyVisit_set_LastVisitDate_mB1B3B146AC9B2E0649B4FBC4A3D6847C6470D13F ();
// 0x0000087C System.DateTime DefaultUtility.DailyVisit::GetFirstVisitDate(UnityEngine.MonoBehaviour)
extern void DailyVisit_GetFirstVisitDate_m7CBC1C122B5D41BE52A0517744CC279109B866C0 ();
// 0x0000087D System.DateTime DefaultUtility.DailyVisit::get_FirstVisitDate()
extern void DailyVisit_get_FirstVisitDate_mCA2A6B38293AE35B275ACCC386875DD8390D1E95 ();
// 0x0000087E System.Void DefaultUtility.DailyVisit::set_FirstVisitDate(System.DateTime)
extern void DailyVisit_set_FirstVisitDate_m1DDDAD166C73838466C550653D872B250C4E8514 ();
// 0x0000087F DefaultUtility.DayType DefaultUtility.DailyVisit::get_CurrentDayType()
extern void DailyVisit_get_CurrentDayType_mE47A2B1972C047BEF61F0EB64D4345DA41DF6E21 ();
// 0x00000880 System.Boolean DefaultUtility.DailyVisit::get_IsFirstLaunch()
extern void DailyVisit_get_IsFirstLaunch_m1D90519EA6D60EC309C8411E688D515FEE0D8F1E ();
// 0x00000881 DefaultUtility.DayType DefaultUtility.DailyVisit::InitDayType()
extern void DailyVisit_InitDayType_m146DADFE0ECE078FA01ED01CD13206F1BC1AF231 ();
// 0x00000882 System.Void DefaultUtility.DailyVisit::.cctor()
extern void DailyVisit__cctor_m2FF920EC1AFB60B65CBB277B53517FEF1E2660F5 ();
// 0x00000883 System.Void DefaultUtility.TypeEditor::.ctor()
extern void TypeEditor__ctor_m2F9C442469687F8151855C4C73F1DA66210CD2C5 ();
// 0x00000884 System.String DefaultUtility.ButtonAttribute::get_Name()
extern void ButtonAttribute_get_Name_m2F4A9DE7D28C840EB14F123622FE547226053E2B ();
// 0x00000885 DefaultUtility.ButtonMode DefaultUtility.ButtonAttribute::get_Mode()
extern void ButtonAttribute_get_Mode_mDA1AE218809EA6C6C34FA5C76B8B9C6C800026BB ();
// 0x00000886 System.Void DefaultUtility.ButtonAttribute::.ctor()
extern void ButtonAttribute__ctor_m2E636AF0841414A4F34DED0F0BCB83065922752E ();
// 0x00000887 System.Void DefaultUtility.ButtonAttribute::.ctor(System.String)
extern void ButtonAttribute__ctor_m59F3A244ECA19241994AEBB13377772C4D0917F0 ();
// 0x00000888 System.Void DefaultUtility.ButtonAttribute::.ctor(System.String,DefaultUtility.ButtonMode)
extern void ButtonAttribute__ctor_mB97F286037008F315D2C4BB5C2172BD489109F8F ();
// 0x00000889 System.Void DefaultUtility.ButtonAttribute::.ctor(DefaultUtility.ButtonMode)
extern void ButtonAttribute__ctor_mB72CED897A81108626B5D1531EF120D5EB856AD1 ();
// 0x0000088A System.Void DefaultUtility.ButtonsExample::SayMyName()
extern void ButtonsExample_SayMyName_m7989BAD34353A21297191C5042E9AF1A82987177 ();
// 0x0000088B System.Void DefaultUtility.ButtonsExample::SayHelloEditor()
extern void ButtonsExample_SayHelloEditor_mC94A22279F120FDF2597EDE5B86C12B3911B2F80 ();
// 0x0000088C System.Void DefaultUtility.ButtonsExample::SayHelloInRuntime()
extern void ButtonsExample_SayHelloInRuntime_mCD7660785CA16916846594E5A1F2639130F220F7 ();
// 0x0000088D System.Void DefaultUtility.ButtonsExample::TestButtonName()
extern void ButtonsExample_TestButtonName_m5497ADEB0DAF81DDB259A7126D543757BA38648A ();
// 0x0000088E System.Void DefaultUtility.ButtonsExample::TestButtonNameEditorOnly()
extern void ButtonsExample_TestButtonNameEditorOnly_m591681CE3FE5D1CC75E375BE51DB52235B30B083 ();
// 0x0000088F System.Void DefaultUtility.ButtonsExample::.ctor()
extern void ButtonsExample__ctor_mCE016453E8A7C762432E180F069EFCB95CBA9AD0 ();
// 0x00000890 System.String DefaultUtility.StyledText::SetColor(System.String,UnityEngine.Color)
extern void StyledText_SetColor_mCB590BEAC07153907E51EECD3C7EB629E14479A2 ();
// 0x00000891 System.String DefaultUtility.StyledText::SetSize(System.String,System.Int32)
extern void StyledText_SetSize_mF6103AFCB9C1CCEB3AD637FF1F36DAA1A58F8039 ();
// 0x00000892 System.String DefaultUtility.StyledText::Italic(System.String)
extern void StyledText_Italic_m28EBA602DF2A32E4361A3451ADAC4B6EF8E8ACDA ();
// 0x00000893 System.String DefaultUtility.StyledText::Bold(System.String)
extern void StyledText_Bold_m65F145E7BAF0EE5B2D8D74D1225B50FD6E38DD45 ();
// 0x00000894 System.Void DefaultUtility.StyledTextExample::ShowTextInConsole()
extern void StyledTextExample_ShowTextInConsole_m7B675053055A5133BEBDF78C705200B866014B5A ();
// 0x00000895 System.Void DefaultUtility.StyledTextExample::.ctor()
extern void StyledTextExample__ctor_m5838E571141CBA2F210576993BBCAF2B0F0D0D72 ();
// 0x00000896 System.Single DefaultUtility.VectorUtility::GetRandom(UnityEngine.Vector2)
extern void VectorUtility_GetRandom_mABC347C545B92D7AAF98B941402C13CAB81E28F9 ();
// 0x00000897 CustomService.AdsManager CustomService.AdsController::get_ADS()
extern void AdsController_get_ADS_m30C7641876FC935617173AD0C1A0568BC50154E7 ();
// 0x00000898 System.Void CustomService.AdsController::add_setNoAdsState(System.Action`1<System.Boolean>)
extern void AdsController_add_setNoAdsState_mEB1D4D648AE5F4A82C5207594BE359886AB4B003 ();
// 0x00000899 System.Void CustomService.AdsController::remove_setNoAdsState(System.Action`1<System.Boolean>)
extern void AdsController_remove_setNoAdsState_mEA41239F92A2DE34CE1C54DF673EC77CAEE8BC48 ();
// 0x0000089A System.Boolean CustomService.AdsController::get_NoADS()
extern void AdsController_get_NoADS_m66B645C8EDE53730C42B8A404062039120715C1F ();
// 0x0000089B System.Void CustomService.AdsController::set_NoADS(System.Boolean)
extern void AdsController_set_NoADS_m3E6E5962118629AEC9A1D9CC44E9F7B9234BA7E0 ();
// 0x0000089C System.Void CustomService.AdsController::Awake()
extern void AdsController_Awake_m7AD9333777EED8BEF4D23E72EF362B27FC96B9EC ();
// 0x0000089D System.Void CustomService.AdsController::Start()
extern void AdsController_Start_m4A8CC725484C29E2BE7D8BCDE88E9EA97FD5DFFF ();
// 0x0000089E System.Void CustomService.AdsController::SetNoADS(System.Boolean)
extern void AdsController_SetNoADS_m1D74E3EE50F44778168312E5DD5169D6BE6A11F2 ();
// 0x0000089F System.Void CustomService.AdsController::InitAdsManager()
extern void AdsController_InitAdsManager_m9D0A6E06BCE8D4F386E769D1162935EE5E70B203 ();
// 0x000008A0 System.Void CustomService.AdsController::DisableAudioListenerOnAds()
extern void AdsController_DisableAudioListenerOnAds_m8F4B628CE6EC45044C5E7B5A96C8F05A7D80BA8B ();
// 0x000008A1 System.Void CustomService.AdsController::EnableAdsModule()
extern void AdsController_EnableAdsModule_m169BADA1F93DFEB81AA5E060E13A5B19FA806708 ();
// 0x000008A2 System.Void CustomService.AdsController::SetActiveBanner(System.Boolean,AdsPlacement)
extern void AdsController_SetActiveBanner_m791F57B6E4419C00F07AF853FD8A614BBDC398F7 ();
// 0x000008A3 System.Void CustomService.AdsController::ShowNative()
extern void AdsController_ShowNative_mCC42C17279DAD6B6AB2FE695F26D6AA7EA0B7CBA ();
// 0x000008A4 System.Void CustomService.AdsController::HideNative()
extern void AdsController_HideNative_mA088864F37998BE43F61F4F25D85522C6BBFBF49 ();
// 0x000008A5 System.Void CustomService.AdsController::CacheInterstitial(AdsPlacement)
extern void AdsController_CacheInterstitial_mF469D1D946D982DB65320A837C9A51E9AD775CBB ();
// 0x000008A6 System.Boolean CustomService.AdsController::ShowInterstitial(AdsPlacement,System.Action,System.Action)
extern void AdsController_ShowInterstitial_mAD62D21026D701C376D5F32785545FDF83A52326 ();
// 0x000008A7 System.Void CustomService.AdsController::CacheRewarded(AdsPlacement)
extern void AdsController_CacheRewarded_m74AB80859F31749811DC1FC232E829D8AFDA0A8F ();
// 0x000008A8 System.Boolean CustomService.AdsController::InterstitialIsReady(AdsPlacement)
extern void AdsController_InterstitialIsReady_mB33768B42B14D04148E7900ED8E75A7859D409BF ();
// 0x000008A9 System.Void CustomService.AdsController::ShowRewarded(AdsPlacement,System.Action`1<System.Boolean>,System.Action)
extern void AdsController_ShowRewarded_mED835DE227469213210F027ED34D01E0FE666696 ();
// 0x000008AA System.Boolean CustomService.AdsController::RewardedIsReady(AdsPlacement)
extern void AdsController_RewardedIsReady_m36045AEB7CF76F033D579D42F65A6411C66F97ED ();
// 0x000008AB System.Void CustomService.AdsController::.ctor()
extern void AdsController__ctor_mF49F237B65051AA8606F4A1E9830EEACB3ED345C ();
// 0x000008AC System.Void CustomService.AdsManager::add_BannerClick(System.Action`1<System.String>)
extern void AdsManager_add_BannerClick_m4B6DD473B334ECA47D31AEA75B8D3A27378B08E9 ();
// 0x000008AD System.Void CustomService.AdsManager::remove_BannerClick(System.Action`1<System.String>)
extern void AdsManager_remove_BannerClick_mE5DB571CE2F5F82E2A81AD9017E8016915800527 ();
// 0x000008AE System.Void CustomService.AdsManager::add_InterstitialShow(System.Action)
extern void AdsManager_add_InterstitialShow_m3B2B9C494105DB63872208AE48E3EC238CFF4140 ();
// 0x000008AF System.Void CustomService.AdsManager::remove_InterstitialShow(System.Action)
extern void AdsManager_remove_InterstitialShow_mB4F47C7F18252DBF8DC972DBE863409BE8EDD1D1 ();
// 0x000008B0 System.Void CustomService.AdsManager::add_InterstitialShown(System.Action`1<System.String>)
extern void AdsManager_add_InterstitialShown_mDC6DF556265F0D69382DE43E6F10C4CCDC44CFAF ();
// 0x000008B1 System.Void CustomService.AdsManager::remove_InterstitialShown(System.Action`1<System.String>)
extern void AdsManager_remove_InterstitialShown_mFBD0FA90893564A935FB74E89E4AE819AE9D10C1 ();
// 0x000008B2 System.Void CustomService.AdsManager::add_InterstitialClick(System.Action`1<System.String>)
extern void AdsManager_add_InterstitialClick_m76D45436D22AD1F47850D4EC3F4F34FB2B69E3A1 ();
// 0x000008B3 System.Void CustomService.AdsManager::remove_InterstitialClick(System.Action`1<System.String>)
extern void AdsManager_remove_InterstitialClick_mB20C092FA193072128FC752D38275E967F3FF344 ();
// 0x000008B4 System.Void CustomService.AdsManager::add_InterstitialClosed(System.Action)
extern void AdsManager_add_InterstitialClosed_m7B2D4BAFD91A8FC0AA42E0DF1E23B556D2E301EA ();
// 0x000008B5 System.Void CustomService.AdsManager::remove_InterstitialClosed(System.Action)
extern void AdsManager_remove_InterstitialClosed_m6E942DF085473067CEF37B862891B762A5109717 ();
// 0x000008B6 System.Void CustomService.AdsManager::add_RewardedShown(System.Action`1<System.String>)
extern void AdsManager_add_RewardedShown_m3A81224E1F6AD4015909ABF830F12C51D3E8DE97 ();
// 0x000008B7 System.Void CustomService.AdsManager::remove_RewardedShown(System.Action`1<System.String>)
extern void AdsManager_remove_RewardedShown_mB05AAF97A5B850E803F9111E42EDA8BBF710BFE6 ();
// 0x000008B8 System.Void CustomService.AdsManager::add_RewardedComplete(System.Action`1<System.String>)
extern void AdsManager_add_RewardedComplete_mF94FE824B202C7AD746E61B1C474D3EB02F31FB2 ();
// 0x000008B9 System.Void CustomService.AdsManager::remove_RewardedComplete(System.Action`1<System.String>)
extern void AdsManager_remove_RewardedComplete_m456431CA9920BDB9B6FFF14ADC48507652BAD283 ();
// 0x000008BA System.Void CustomService.AdsManager::add_RewardedClick(System.Action`1<System.String>)
extern void AdsManager_add_RewardedClick_m755E475A9AB61D1950816D4B82FEE1187948EA47 ();
// 0x000008BB System.Void CustomService.AdsManager::remove_RewardedClick(System.Action`1<System.String>)
extern void AdsManager_remove_RewardedClick_m9FDA7B0B312ED5DAE2369E105FB435172A2895FB ();
// 0x000008BC System.Void CustomService.AdsManager::add_RewardedClose(System.Action)
extern void AdsManager_add_RewardedClose_m633AB2341D00CD5507EB8306FB985AA55C206206 ();
// 0x000008BD System.Void CustomService.AdsManager::remove_RewardedClose(System.Action)
extern void AdsManager_remove_RewardedClose_mD60A8B1A62ABDD90AAE92DD0FBBDCC5B1900AC40 ();
// 0x000008BE System.Void CustomService.AdsManager::add_AdsStart(System.Action)
extern void AdsManager_add_AdsStart_mFADDB1F7C654A5A61A5F570BF02293496175933E ();
// 0x000008BF System.Void CustomService.AdsManager::remove_AdsStart(System.Action)
extern void AdsManager_remove_AdsStart_mEA8D77210698170DB0C964AEC5038548738D7EF8 ();
// 0x000008C0 System.Void CustomService.AdsManager::add_AdsStop(System.Action)
extern void AdsManager_add_AdsStop_mF36870F9C10C3E850182D7EFC14D9C686176FEF6 ();
// 0x000008C1 System.Void CustomService.AdsManager::remove_AdsStop(System.Action)
extern void AdsManager_remove_AdsStop_m0BF5A702806CF48664801100CB7A109F044C51AC ();
// 0x000008C2 System.Boolean CustomService.AdsManager::get_IsInternetAvailable()
extern void AdsManager_get_IsInternetAvailable_mF0E12C825082F24C0C0A5E0441F5261EFDA6FFC4 ();
// 0x000008C3 System.Void CustomService.AdsManager::.ctor(CustomService.IAdsProvider)
extern void AdsManager__ctor_m41AF346593F3131EAEF86784C6F99AC2309808C2 ();
// 0x000008C4 System.Void CustomService.AdsManager::InitModule()
extern void AdsManager_InitModule_mBA8F517A5CED70BF22CD6A8FBB8FEB9F7A4DE035 ();
// 0x000008C5 System.Boolean CustomService.AdsManager::IsBannerHidden(System.String)
extern void AdsManager_IsBannerHidden_mEC2D3A39427F97CF8E0F36D3769B41D950F899D7 ();
// 0x000008C6 System.Void CustomService.AdsManager::CacheInterstitial(System.String)
extern void AdsManager_CacheInterstitial_mD6302D2AABA5F1E6CD0B3E5C7BA1AFB82AE0FB69 ();
// 0x000008C7 System.Boolean CustomService.AdsManager::IsInterstitialReady(System.String)
extern void AdsManager_IsInterstitialReady_mB742669E384685891A8D73D525AF176AA173D405 ();
// 0x000008C8 System.Boolean CustomService.AdsManager::ShowInterstitial(System.String)
extern void AdsManager_ShowInterstitial_mBF19AE55FEF5AE489AF94FA177261C7773DFB087 ();
// 0x000008C9 System.Void CustomService.AdsManager::CacheRewarded(System.String)
extern void AdsManager_CacheRewarded_m9C7BD806D07B792A15C42179A55CEC41081ABEF5 ();
// 0x000008CA System.Boolean CustomService.AdsManager::IsRewardedReady(System.String)
extern void AdsManager_IsRewardedReady_m97AE4E774A5E38F420BF0DF79B5A61BA55B68AD8 ();
// 0x000008CB System.Void CustomService.AdsManager::ShowRewardedVideo(System.String)
extern void AdsManager_ShowRewardedVideo_m3D99F90C6CDC162BFA9F18DD9B791FDE312EF23C ();
// 0x000008CC System.Void CustomService.AdsManager::SetAdsSoundsMuted(System.Boolean)
extern void AdsManager_SetAdsSoundsMuted_m8DBB87DA4AA960E5B2C2B874EA81194FBED02F9A ();
// 0x000008CD System.Void CustomService.AdsManager::SetAdsEnable(System.Boolean)
extern void AdsManager_SetAdsEnable_m51819F8595ED0ADA158AEBF8D45FFB8B1DBB3D31 ();
// 0x000008CE System.Void CustomService.AdsManager::ShowBanner(System.String)
extern void AdsManager_ShowBanner_mFD5839F63579E3DCD98386452428BB6855049F86 ();
// 0x000008CF System.Void CustomService.AdsManager::HideBanner(System.String)
extern void AdsManager_HideBanner_m2B857DA7E291D9F3A796F8098AF97484E9E62349 ();
// 0x000008D0 System.Void CustomService.AdsManager::OnBannerClick(System.String)
extern void AdsManager_OnBannerClick_m08BF88870544A08A9FAA2C889CC487FFE3001D35 ();
// 0x000008D1 System.Void CustomService.AdsManager::OnInterstitialClosed()
extern void AdsManager_OnInterstitialClosed_mF905B37513FBDAAD1FB7AD3E445D7B16F2619F09 ();
// 0x000008D2 System.Void CustomService.AdsManager::OnInterstitialShow()
extern void AdsManager_OnInterstitialShow_m88C3F4727C4688EA19B58A2BD4F418A0F390B8B8 ();
// 0x000008D3 System.Void CustomService.AdsManager::OnInterstitialShown(System.String)
extern void AdsManager_OnInterstitialShown_m5A65CE1F630DB81AC0792EEE3050EB8B9EFE0D6C ();
// 0x000008D4 System.Void CustomService.AdsManager::OnInterstitialClick(System.String)
extern void AdsManager_OnInterstitialClick_m70F1EAE6658A83BAB93F36A8F6829DA6DFF39DE7 ();
// 0x000008D5 System.Void CustomService.AdsManager::ClearInterstitialCallbacks()
extern void AdsManager_ClearInterstitialCallbacks_m9B8886C8B95C08ECD97611A632233C40CC8C858C ();
// 0x000008D6 System.Void CustomService.AdsManager::OnRewardedShown(System.String)
extern void AdsManager_OnRewardedShown_m574DC7EAB5D39CCA275E3C95EE7C34D48B8FD6F1 ();
// 0x000008D7 System.Void CustomService.AdsManager::AddRewardedCompleteCallback(System.Action`1<System.Boolean>)
extern void AdsManager_AddRewardedCompleteCallback_m1D9EC4FE762DD251D0C48C311B4468FD1CF42F59 ();
// 0x000008D8 System.Void CustomService.AdsManager::AddRewardedCloseCallback(System.Action)
extern void AdsManager_AddRewardedCloseCallback_m7B29E98F3A267E018EC6BF4CCA148FD5D5AC51F0 ();
// 0x000008D9 System.Void CustomService.AdsManager::OnRewardedComplete(System.String)
extern void AdsManager_OnRewardedComplete_mE872F3BED1C5C149B9D66C4476A70D549C0C2E56 ();
// 0x000008DA System.Void CustomService.AdsManager::OnRewardedClick(System.String)
extern void AdsManager_OnRewardedClick_m0ECB0F341FECB54B16B36211008FC2B69F4C021A ();
// 0x000008DB System.Void CustomService.AdsManager::OnRewardedClose()
extern void AdsManager_OnRewardedClose_mDB4B64AD14A4FFD8613EBD868024292660CB1524 ();
// 0x000008DC System.Void CustomService.AdsManager::ClearRewardedCallbacks()
extern void AdsManager_ClearRewardedCallbacks_m0CBAF925568281BB420A1F6EEB41ED048DC8D310 ();
// 0x000008DD System.Void CustomService.IAdsProvider::add_BannerClick(System.Action`1<System.String>)
// 0x000008DE System.Void CustomService.IAdsProvider::remove_BannerClick(System.Action`1<System.String>)
// 0x000008DF System.Void CustomService.IAdsProvider::add_InterstitialShow(System.Action)
// 0x000008E0 System.Void CustomService.IAdsProvider::remove_InterstitialShow(System.Action)
// 0x000008E1 System.Void CustomService.IAdsProvider::add_InterstitialShown(System.Action`1<System.String>)
// 0x000008E2 System.Void CustomService.IAdsProvider::remove_InterstitialShown(System.Action`1<System.String>)
// 0x000008E3 System.Void CustomService.IAdsProvider::add_InterstitialClick(System.Action`1<System.String>)
// 0x000008E4 System.Void CustomService.IAdsProvider::remove_InterstitialClick(System.Action`1<System.String>)
// 0x000008E5 System.Void CustomService.IAdsProvider::add_InterstitialClosed(System.Action)
// 0x000008E6 System.Void CustomService.IAdsProvider::remove_InterstitialClosed(System.Action)
// 0x000008E7 System.Void CustomService.IAdsProvider::add_RewardedShown(System.Action`1<System.String>)
// 0x000008E8 System.Void CustomService.IAdsProvider::remove_RewardedShown(System.Action`1<System.String>)
// 0x000008E9 System.Void CustomService.IAdsProvider::add_RewardedComplete(System.Action`1<System.String>)
// 0x000008EA System.Void CustomService.IAdsProvider::remove_RewardedComplete(System.Action`1<System.String>)
// 0x000008EB System.Void CustomService.IAdsProvider::add_RewardedClick(System.Action`1<System.String>)
// 0x000008EC System.Void CustomService.IAdsProvider::remove_RewardedClick(System.Action`1<System.String>)
// 0x000008ED System.Void CustomService.IAdsProvider::add_RewardedClose(System.Action)
// 0x000008EE System.Void CustomService.IAdsProvider::remove_RewardedClose(System.Action)
// 0x000008EF System.Void CustomService.IAdsProvider::InitModule()
// 0x000008F0 System.Void CustomService.IAdsProvider::SetAdsEnable(System.Boolean)
// 0x000008F1 System.Boolean CustomService.IAdsProvider::IsBannerHidden(System.String)
// 0x000008F2 System.Void CustomService.IAdsProvider::ShowBanner(System.String)
// 0x000008F3 System.Void CustomService.IAdsProvider::HideBanner(System.String)
// 0x000008F4 System.Void CustomService.IAdsProvider::CacheInterstitial(System.String)
// 0x000008F5 System.Boolean CustomService.IAdsProvider::IsInterstitialReady(System.String)
// 0x000008F6 System.Boolean CustomService.IAdsProvider::ShowInterstitial(System.String)
// 0x000008F7 System.Void CustomService.IAdsProvider::CacheRewarded(System.String)
// 0x000008F8 System.Boolean CustomService.IAdsProvider::IsRewardedReady(System.String)
// 0x000008F9 System.Void CustomService.IAdsProvider::ShowRewardedVideo(System.String)
// 0x000008FA System.Void CustomService.IAdsProvider::SetAdsSoundsMuted(System.Boolean)
// 0x000008FB System.Void CustomService.MockAdsProvider::add_BannerClick(System.Action`1<System.String>)
extern void MockAdsProvider_add_BannerClick_mDCF22F4EDA20CEEF722685E9A20F3C3DE7F17210 ();
// 0x000008FC System.Void CustomService.MockAdsProvider::remove_BannerClick(System.Action`1<System.String>)
extern void MockAdsProvider_remove_BannerClick_mD0278E020FAD9A8BDC20BF94ADF1DC76335224F1 ();
// 0x000008FD System.Void CustomService.MockAdsProvider::add_InterstitialShow(System.Action)
extern void MockAdsProvider_add_InterstitialShow_mC342BEE88B1BFFAF48FDE331B2D8B993561E14FE ();
// 0x000008FE System.Void CustomService.MockAdsProvider::remove_InterstitialShow(System.Action)
extern void MockAdsProvider_remove_InterstitialShow_m9D3E95D74B01504E8B4788B3B9A1EB0ED1D49C39 ();
// 0x000008FF System.Void CustomService.MockAdsProvider::add_InterstitialShown(System.Action`1<System.String>)
extern void MockAdsProvider_add_InterstitialShown_mC77DEE07EA315CB1EA93DADD8A09E717A975A9AB ();
// 0x00000900 System.Void CustomService.MockAdsProvider::remove_InterstitialShown(System.Action`1<System.String>)
extern void MockAdsProvider_remove_InterstitialShown_mF3E6E4718222FAC1E9122DFFA87839513F8CBF62 ();
// 0x00000901 System.Void CustomService.MockAdsProvider::add_InterstitialClick(System.Action`1<System.String>)
extern void MockAdsProvider_add_InterstitialClick_m14C2EC1165D80D563B32C451C77625828B180F6E ();
// 0x00000902 System.Void CustomService.MockAdsProvider::remove_InterstitialClick(System.Action`1<System.String>)
extern void MockAdsProvider_remove_InterstitialClick_m4C2BE0D63D913B2EABC5251D8577C3E69ACC7D94 ();
// 0x00000903 System.Void CustomService.MockAdsProvider::add_InterstitialClosed(System.Action)
extern void MockAdsProvider_add_InterstitialClosed_m94D90A204A1BA4DC89000DB504CFCD3EDF60375A ();
// 0x00000904 System.Void CustomService.MockAdsProvider::remove_InterstitialClosed(System.Action)
extern void MockAdsProvider_remove_InterstitialClosed_m2B309AE29F69BE484CC34DE29689D5630A2FCC3C ();
// 0x00000905 System.Void CustomService.MockAdsProvider::add_RewardedShown(System.Action`1<System.String>)
extern void MockAdsProvider_add_RewardedShown_mC21127B345D1F8DF2895D4683CCB4B8E54D24AEC ();
// 0x00000906 System.Void CustomService.MockAdsProvider::remove_RewardedShown(System.Action`1<System.String>)
extern void MockAdsProvider_remove_RewardedShown_m49DBE6F399B1E95BA55BC41839B1735E508670BD ();
// 0x00000907 System.Void CustomService.MockAdsProvider::add_RewardedComplete(System.Action`1<System.String>)
extern void MockAdsProvider_add_RewardedComplete_m9E8FE49C5C5E56C1962374EA69261EC09A6CFABB ();
// 0x00000908 System.Void CustomService.MockAdsProvider::remove_RewardedComplete(System.Action`1<System.String>)
extern void MockAdsProvider_remove_RewardedComplete_m7CFF4FF56655335F7510D8D6E111EEAB60A53991 ();
// 0x00000909 System.Void CustomService.MockAdsProvider::add_RewardedClick(System.Action`1<System.String>)
extern void MockAdsProvider_add_RewardedClick_m946658AF5FC048A2FA859B59F7B55E57DED0425B ();
// 0x0000090A System.Void CustomService.MockAdsProvider::remove_RewardedClick(System.Action`1<System.String>)
extern void MockAdsProvider_remove_RewardedClick_mD9B0F1A6D5D0F25BEFF44D17EF9393B0FECBD865 ();
// 0x0000090B System.Void CustomService.MockAdsProvider::add_RewardedClose(System.Action)
extern void MockAdsProvider_add_RewardedClose_mB53BFE650444C05881D4FC90B962DF373FCB8AA5 ();
// 0x0000090C System.Void CustomService.MockAdsProvider::remove_RewardedClose(System.Action)
extern void MockAdsProvider_remove_RewardedClose_mCB3745E8D3A17EA4F3CC385B51B40259EBD058B3 ();
// 0x0000090D System.Void CustomService.MockAdsProvider::InitModule()
extern void MockAdsProvider_InitModule_mC28B6006CB8B686091667EA49D5CF6695A72D4D0 ();
// 0x0000090E System.Void CustomService.MockAdsProvider::HideBanner(System.String)
extern void MockAdsProvider_HideBanner_mB4FBC33763DC87D526F20CD5F06D6D2C837314F4 ();
// 0x0000090F System.Boolean CustomService.MockAdsProvider::IsInterstitialReady(System.String)
extern void MockAdsProvider_IsInterstitialReady_m37EC12BC03717A507B286B0DD0AD75B4B072FC10 ();
// 0x00000910 System.Void CustomService.MockAdsProvider::CacheInterstitial(System.String)
extern void MockAdsProvider_CacheInterstitial_m15AD748B7A287BB359FBA13CC7E57E39EB63D72D ();
// 0x00000911 System.Void CustomService.MockAdsProvider::CacheRewarded(System.String)
extern void MockAdsProvider_CacheRewarded_m5857DC8552125EAAD49D700C66AA74E85BA927F5 ();
// 0x00000912 System.Boolean CustomService.MockAdsProvider::IsRewardedReady(System.String)
extern void MockAdsProvider_IsRewardedReady_m8DFDB96FAB2BD5766C92DA31ED80FE44176C0FC7 ();
// 0x00000913 System.Void CustomService.MockAdsProvider::SetAdsEnable(System.Boolean)
extern void MockAdsProvider_SetAdsEnable_m6D1A30E5579AB67E9CBB1016210E26B165592E80 ();
// 0x00000914 System.Boolean CustomService.MockAdsProvider::ShowInterstitial(System.String)
extern void MockAdsProvider_ShowInterstitial_m8DD0989B79D6D8068BED37D2102E5624FBDA13AC ();
// 0x00000915 System.Void CustomService.MockAdsProvider::ShowBanner(System.String)
extern void MockAdsProvider_ShowBanner_m6469950490DC3EDE140E6AE391A7DB6F103E4AEE ();
// 0x00000916 System.Void CustomService.MockAdsProvider::ShowRewardedVideo(System.String)
extern void MockAdsProvider_ShowRewardedVideo_m54E79D55845A0BB64B7D5DC5CCB8B8761A38BC82 ();
// 0x00000917 System.Void CustomService.MockAdsProvider::SetAdsSoundsMuted(System.Boolean)
extern void MockAdsProvider_SetAdsSoundsMuted_m56BCEC8F44E073D8C24703F6E6CCC023BE55C82A ();
// 0x00000918 System.Boolean CustomService.MockAdsProvider::IsBannerHidden(System.String)
extern void MockAdsProvider_IsBannerHidden_m3755AE2F873FA72E270218B0407556198A5A2D9B ();
// 0x00000919 System.Void CustomService.MockAdsProvider::OnRewardedCompelte()
extern void MockAdsProvider_OnRewardedCompelte_m387E4C323ECD6D0981D58B23AAF72C6D4ECD636D ();
// 0x0000091A System.Void CustomService.MockAdsProvider::OnInterstitialClosed()
extern void MockAdsProvider_OnInterstitialClosed_m0B28126F507D2D2BD48DDE5ABF95F27D76DC12A8 ();
// 0x0000091B System.Void CustomService.MockAdsProvider::.ctor()
extern void MockAdsProvider__ctor_m66B047F7675116469A65729DE923009B76F0A850 ();
// 0x0000091C AdsPlacement CustomService.MoPubAdsProvider::get_banner()
extern void MoPubAdsProvider_get_banner_mA0A63250432E6A7D8191BD538E55DC4AB3F34BBE ();
// 0x0000091D AdsPlacement[] CustomService.MoPubAdsProvider::get_interstitials()
extern void MoPubAdsProvider_get_interstitials_m9C200F746129F820DB72D4D5C7694D649C2DCD08 ();
// 0x0000091E AdsPlacement[] CustomService.MoPubAdsProvider::get_rewarded()
extern void MoPubAdsProvider_get_rewarded_m079C4D2B9735C75E1FA82C2CEFAD0FCF1BAA6EE4 ();
// 0x0000091F System.Void CustomService.MoPubAdsProvider::add_BannerClick(System.Action`1<System.String>)
extern void MoPubAdsProvider_add_BannerClick_m83863B059BC4729A5A33CCD478BDD67D9329B083 ();
// 0x00000920 System.Void CustomService.MoPubAdsProvider::remove_BannerClick(System.Action`1<System.String>)
extern void MoPubAdsProvider_remove_BannerClick_m45CA73BA553D626C930387465DEC7EA738A9E41D ();
// 0x00000921 System.Void CustomService.MoPubAdsProvider::add_InterstitialShow(System.Action)
extern void MoPubAdsProvider_add_InterstitialShow_m72F59C143DAB1663D3FE90372999F548D02BFF9F ();
// 0x00000922 System.Void CustomService.MoPubAdsProvider::remove_InterstitialShow(System.Action)
extern void MoPubAdsProvider_remove_InterstitialShow_m48DA6E60E383E1A082C133F67600BF66D00B35B6 ();
// 0x00000923 System.Void CustomService.MoPubAdsProvider::add_InterstitialShown(System.Action`1<System.String>)
extern void MoPubAdsProvider_add_InterstitialShown_m076C60508B9D826BC55FD5AADEC3D659C6D76FC7 ();
// 0x00000924 System.Void CustomService.MoPubAdsProvider::remove_InterstitialShown(System.Action`1<System.String>)
extern void MoPubAdsProvider_remove_InterstitialShown_m99CBEF315A783DAA4B487CC588451E49A741133B ();
// 0x00000925 System.Void CustomService.MoPubAdsProvider::add_InterstitialClick(System.Action`1<System.String>)
extern void MoPubAdsProvider_add_InterstitialClick_m4799F06F8B573D785EB740DDE02446AEC7BBA4AC ();
// 0x00000926 System.Void CustomService.MoPubAdsProvider::remove_InterstitialClick(System.Action`1<System.String>)
extern void MoPubAdsProvider_remove_InterstitialClick_m1B2259112516F753979AD51B9C71446EC1E3F715 ();
// 0x00000927 System.Void CustomService.MoPubAdsProvider::add_InterstitialClosed(System.Action)
extern void MoPubAdsProvider_add_InterstitialClosed_mFAF25A6BEA639FF87B630C979C6B728BABC78169 ();
// 0x00000928 System.Void CustomService.MoPubAdsProvider::remove_InterstitialClosed(System.Action)
extern void MoPubAdsProvider_remove_InterstitialClosed_m0DF8F71FC95B8512549C6162BBF9BD5059F4B56F ();
// 0x00000929 System.Void CustomService.MoPubAdsProvider::add_RewardedShown(System.Action`1<System.String>)
extern void MoPubAdsProvider_add_RewardedShown_m8ABDE02450FA061E2B9DF5C2FE6491FE99FC01A8 ();
// 0x0000092A System.Void CustomService.MoPubAdsProvider::remove_RewardedShown(System.Action`1<System.String>)
extern void MoPubAdsProvider_remove_RewardedShown_m28FF331672AEF47DF7FC480B04E8C16B4A9ADE7C ();
// 0x0000092B System.Void CustomService.MoPubAdsProvider::add_RewardedComplete(System.Action`1<System.String>)
extern void MoPubAdsProvider_add_RewardedComplete_mB7B3D1E21C1F801383F67C17BBF63D084B77C8EA ();
// 0x0000092C System.Void CustomService.MoPubAdsProvider::remove_RewardedComplete(System.Action`1<System.String>)
extern void MoPubAdsProvider_remove_RewardedComplete_m1ABA2FC0D86D6E5A89974E101D5E963510C8C0E8 ();
// 0x0000092D System.Void CustomService.MoPubAdsProvider::add_RewardedClick(System.Action`1<System.String>)
extern void MoPubAdsProvider_add_RewardedClick_mA001A9ED4BEE12B2489D7E82E8B4599EAF88F914 ();
// 0x0000092E System.Void CustomService.MoPubAdsProvider::remove_RewardedClick(System.Action`1<System.String>)
extern void MoPubAdsProvider_remove_RewardedClick_m929D2A0BC9862268262186290F184D138022B6CD ();
// 0x0000092F System.Void CustomService.MoPubAdsProvider::add_RewardedClose(System.Action)
extern void MoPubAdsProvider_add_RewardedClose_m8B0F119F2477F30FB3B661FB135CB5C4C74ACD1E ();
// 0x00000930 System.Void CustomService.MoPubAdsProvider::remove_RewardedClose(System.Action)
extern void MoPubAdsProvider_remove_RewardedClose_mA0BE8DD43AD522C954D1B4CFBB1CBF5A9B1ACA1A ();
// 0x00000931 System.Void CustomService.MoPubAdsProvider::InitModule()
extern void MoPubAdsProvider_InitModule_mE2D3053A1C02C0755BCC3685E51ECA5A53FCA539 ();
// 0x00000932 System.Void CustomService.MoPubAdsProvider::RequestInitPlacements()
extern void MoPubAdsProvider_RequestInitPlacements_m5C50CC0F313D086B93C7E2F0744A463C28AEA2C0 ();
// 0x00000933 System.Void CustomService.MoPubAdsProvider::RequestLoadPlacements()
extern void MoPubAdsProvider_RequestLoadPlacements_m2906112A4B183B46686CB9EFA9C806536444B242 ();
// 0x00000934 System.Void CustomService.MoPubAdsProvider::InitCallbacks()
extern void MoPubAdsProvider_InitCallbacks_m735363703B094AAF8381489B035BF4666465C60E ();
// 0x00000935 System.Boolean CustomService.MoPubAdsProvider::IsBannerHidden(System.String)
extern void MoPubAdsProvider_IsBannerHidden_mDD76794ADCF32D340040316353249BF2A81CF0DB ();
// 0x00000936 System.Void CustomService.MoPubAdsProvider::SetAdsEnable(System.Boolean)
extern void MoPubAdsProvider_SetAdsEnable_m403710918A766AADF8A70014F57661EFACF8BC92 ();
// 0x00000937 System.Void CustomService.MoPubAdsProvider::ShowBanner(System.String)
extern void MoPubAdsProvider_ShowBanner_m3B1766B8AB5271FC187DA04373EE91E29D4DA8FA ();
// 0x00000938 System.Void CustomService.MoPubAdsProvider::HideBanner(System.String)
extern void MoPubAdsProvider_HideBanner_m8D4F20C31F40563A89B1F4B26BEB453098C46614 ();
// 0x00000939 System.Boolean CustomService.MoPubAdsProvider::IsInterstitialReady(System.String)
extern void MoPubAdsProvider_IsInterstitialReady_mB9975999122EA77FE4E010F39E31B394014D10F9 ();
// 0x0000093A System.Void CustomService.MoPubAdsProvider::CacheInterstitial(System.String)
extern void MoPubAdsProvider_CacheInterstitial_mA5EA5E60BAC1C48620101BA03F6B929CD0F7FBC0 ();
// 0x0000093B System.Boolean CustomService.MoPubAdsProvider::ShowInterstitial(System.String)
extern void MoPubAdsProvider_ShowInterstitial_mF87C59E6991461B46E4B3BBA4E2EC051F4BA4779 ();
// 0x0000093C System.Void CustomService.MoPubAdsProvider::CacheRewarded(System.String)
extern void MoPubAdsProvider_CacheRewarded_mB40C02A5534F6FE383FE4793373DEE0978C6C45B ();
// 0x0000093D System.Boolean CustomService.MoPubAdsProvider::IsRewardedReady(System.String)
extern void MoPubAdsProvider_IsRewardedReady_m4AA82E439A11BD7280F157036ADC77661EE95362 ();
// 0x0000093E System.Void CustomService.MoPubAdsProvider::ShowRewardedVideo(System.String)
extern void MoPubAdsProvider_ShowRewardedVideo_m369D8ECBE58A51DF330D1E7EE5AED5D60F3DC697 ();
// 0x0000093F System.Void CustomService.MoPubAdsProvider::SetAdsSoundsMuted(System.Boolean)
extern void MoPubAdsProvider_SetAdsSoundsMuted_m6F609DFCB16727157EA104E121938A96BF6DF1A1 ();
// 0x00000940 AdsPlacement CustomService.MoPubAdsProvider::InterPlacement(System.String)
extern void MoPubAdsProvider_InterPlacement_mC87091D0ED2FDEE698E10A19BC9AEE8FF75CFCF0 ();
// 0x00000941 AdsPlacement CustomService.MoPubAdsProvider::RewardedPlacement(System.String)
extern void MoPubAdsProvider_RewardedPlacement_m482C03EC53AAF1B6753CA65541661C20187FADDC ();
// 0x00000942 System.Void CustomService.MoPubAdsProvider::onSdkInited(System.String)
extern void MoPubAdsProvider_onSdkInited_m5F8E482DDA1B517EE73FC060F0CB7858D143735D ();
// 0x00000943 System.Void CustomService.MoPubAdsProvider::OnBannerLoaded(System.String)
extern void MoPubAdsProvider_OnBannerLoaded_m7E895D40FCA942CAEB8BCA1C83C0384EFC10A848 ();
// 0x00000944 System.Void CustomService.MoPubAdsProvider::OnBannerClick(System.String)
extern void MoPubAdsProvider_OnBannerClick_mE027AE8E2528CED1DE12E4AC0EC190254EF4A680 ();
// 0x00000945 System.Void CustomService.MoPubAdsProvider::onInterstitialLoaded(System.String)
extern void MoPubAdsProvider_onInterstitialLoaded_m74908B9D46B8FBB68DAAD86E76AEAB3E1E349D3D ();
// 0x00000946 System.Void CustomService.MoPubAdsProvider::onInterstitialFailed(System.String,System.String)
extern void MoPubAdsProvider_onInterstitialFailed_m7C9CEF4FC45E5AD2AEE49C3C2BF527EED487E040 ();
// 0x00000947 System.Void CustomService.MoPubAdsProvider::onInterstitialDismissed(System.String)
extern void MoPubAdsProvider_onInterstitialDismissed_mF9D3E1BEE030AF6FF8E19B8E0BE40463559CEEB1 ();
// 0x00000948 System.Void CustomService.MoPubAdsProvider::interstitialDidExpire(System.String)
extern void MoPubAdsProvider_interstitialDidExpire_m3578CA044B4850109A61D0D9B95AB8FBFC5314AE ();
// 0x00000949 System.Void CustomService.MoPubAdsProvider::onInterstitialShown(System.String)
extern void MoPubAdsProvider_onInterstitialShown_m7FA384F3DC393035F7F2B13F83E1165922820DE8 ();
// 0x0000094A System.Void CustomService.MoPubAdsProvider::onInterstitialClicked(System.String)
extern void MoPubAdsProvider_onInterstitialClicked_m353EB8F85A385DB85F67543B7736FE641E406BB0 ();
// 0x0000094B System.Void CustomService.MoPubAdsProvider::onRewardedVideoClick(System.String)
extern void MoPubAdsProvider_onRewardedVideoClick_m3D7BE31DE0BF1C61C1D83BE82BC524F6A210B79D ();
// 0x0000094C System.Void CustomService.MoPubAdsProvider::onRewardedVideoLoaded(System.String)
extern void MoPubAdsProvider_onRewardedVideoLoaded_mCBDEEED2757D67F16BB04BBC3EB529AA02B3B769 ();
// 0x0000094D System.Void CustomService.MoPubAdsProvider::onRewardedVideoFailed(System.String,System.String)
extern void MoPubAdsProvider_onRewardedVideoFailed_m6E7210FFAEC7A7D40E4FF958FC988B9F33186B56 ();
// 0x0000094E System.Void CustomService.MoPubAdsProvider::onRewardedVideoExpired(System.String)
extern void MoPubAdsProvider_onRewardedVideoExpired_m009B57010D047CE841690FF1D0319D2F831B84C7 ();
// 0x0000094F System.Void CustomService.MoPubAdsProvider::onRewardedVideoShown(System.String)
extern void MoPubAdsProvider_onRewardedVideoShown_mFF4E863E2C65AE58320B61FDF787E62F38ECC36D ();
// 0x00000950 System.Void CustomService.MoPubAdsProvider::onRewardedVideoFailedToPlay(System.String,System.String)
extern void MoPubAdsProvider_onRewardedVideoFailedToPlay_mFF24AF47F8980AD19C8AB946B20FC1EE76A08FBB ();
// 0x00000951 System.Void CustomService.MoPubAdsProvider::onRewardedVideoReceivedReward(System.String,System.String,System.Single)
extern void MoPubAdsProvider_onRewardedVideoReceivedReward_mEDB7267AEF8A48EC862D5135CB7A7DF85D2E25F1 ();
// 0x00000952 System.Void CustomService.MoPubAdsProvider::onRewardedVideoClosed(System.String)
extern void MoPubAdsProvider_onRewardedVideoClosed_m6FACC21C3B0A06EDA15EE997CFEEADDAE4A031CE ();
// 0x00000953 System.Void CustomService.MoPubAdsProvider::onRewardedVideoLeavingApplication(System.String)
extern void MoPubAdsProvider_onRewardedVideoLeavingApplication_mD55DED44A4E6D8D006CEBDB742F84950277AFA51 ();
// 0x00000954 System.Void CustomService.MoPubAdsProvider::.ctor()
extern void MoPubAdsProvider__ctor_m1E8722BD003FF1CCA1221B375E259343DDC70EC0 ();
// 0x00000955 System.Void CustomService.Analytics.EasyBrainAnalytics::SendEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void EasyBrainAnalytics_SendEvent_mDADA0924A92BFD8B5EF75E642D7B4AABF1A3A869 ();
// 0x00000956 System.Void CustomService.Analytics.EasyBrainAnalytics::SendEvent(AnalyticsSystems,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void EasyBrainAnalytics_SendEvent_mAC8C5F0E65D9A8C7E48D509A1A0FEDA7E7284110 ();
// 0x00000957 System.String CustomService.Analytics.EasyBrainAnalytics::DictionaryToJson(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void EasyBrainAnalytics_DictionaryToJson_m5BCD8E796B7FFBADF23936D37DF4EA6A6C740649 ();
// 0x00000958 System.Void AdmobAdsProvider_<>c::.cctor()
extern void U3CU3Ec__cctor_m214484930B73709D4F5301991709F64F351CFD8F ();
// 0x00000959 System.Void AdmobAdsProvider_<>c::.ctor()
extern void U3CU3Ec__ctor_m52A864AE518DC9181763D5D16992D46A4E80460A ();
// 0x0000095A System.Boolean AdmobAdsProvider_<>c::<GetNativePanel>b__37_0(NativeAdsPanel)
extern void U3CU3Ec_U3CGetNativePanelU3Eb__37_0_mE27470625052B665F65155047280166DD8F0907C ();
// 0x0000095B System.Void GDPRController_<LoadScene>d__4::.ctor(System.Int32)
extern void U3CLoadSceneU3Ed__4__ctor_m740271F33225B08C3513BC3F6B59B8F1450DC1BF ();
// 0x0000095C System.Void GDPRController_<LoadScene>d__4::System.IDisposable.Dispose()
extern void U3CLoadSceneU3Ed__4_System_IDisposable_Dispose_mA097F7A77FF3232A5CCFDA3E3EE84C8E90835E06 ();
// 0x0000095D System.Boolean GDPRController_<LoadScene>d__4::MoveNext()
extern void U3CLoadSceneU3Ed__4_MoveNext_m76235F77326C197380E4024007F0F96B71729184 ();
// 0x0000095E System.Object GDPRController_<LoadScene>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSceneU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB7169CC88D097E7A22A250C09F19377AF2548AF ();
// 0x0000095F System.Void GDPRController_<LoadScene>d__4::System.Collections.IEnumerator.Reset()
extern void U3CLoadSceneU3Ed__4_System_Collections_IEnumerator_Reset_m761D97B7598ACFE5C22A799BCFA5A1DCE9118B46 ();
// 0x00000960 System.Object GDPRController_<LoadScene>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSceneU3Ed__4_System_Collections_IEnumerator_get_Current_mA40036D0A4622E3745894A08D1BE42F4C77BA7C9 ();
// 0x00000961 System.Void LocalNotification_Action::.ctor(System.String,System.String,UnityEngine.MonoBehaviour)
extern void Action__ctor_m32F21522DBE3B5661FA2EE53F77C9E706F129455 ();
// 0x00000962 System.Void AppleService_<>c::.cctor()
extern void U3CU3Ec__cctor_m99087ECA36C4F12887BE5CF4979BFD19A3EFD9A0 ();
// 0x00000963 System.Void AppleService_<>c::.ctor()
extern void U3CU3Ec__ctor_m337FB881A87DD6A331769F8F1CA274491870AB32 ();
// 0x00000964 System.String AppleService_<>c::<CreateText>b__16_0(LocalizedFrase)
extern void U3CU3Ec_U3CCreateTextU3Eb__16_0_mF09B12E2EB7D6041EA990F2D3981D850D12EE44E ();
// 0x00000965 System.Void CoroutineExtention_<WaitAndPlay>d__3::.ctor(System.Int32)
extern void U3CWaitAndPlayU3Ed__3__ctor_m18B36EE87FC3928ECE5C82BD76728ECDA17093DC ();
// 0x00000966 System.Void CoroutineExtention_<WaitAndPlay>d__3::System.IDisposable.Dispose()
extern void U3CWaitAndPlayU3Ed__3_System_IDisposable_Dispose_m9AD16942A9E6D79D1919E21EA9A9D5D4F12D5438 ();
// 0x00000967 System.Boolean CoroutineExtention_<WaitAndPlay>d__3::MoveNext()
extern void U3CWaitAndPlayU3Ed__3_MoveNext_m21787CB8FE32BF3D88D7D5A0604A8CAB10F30A33 ();
// 0x00000968 System.Object CoroutineExtention_<WaitAndPlay>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndPlayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m632DC50AF14738EBDFDE3B36BA618331C52C572C ();
// 0x00000969 System.Void CoroutineExtention_<WaitAndPlay>d__3::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndPlayU3Ed__3_System_Collections_IEnumerator_Reset_m3F3B7BCCBE4045208ABC64A2C476D31785702D00 ();
// 0x0000096A System.Object CoroutineExtention_<WaitAndPlay>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndPlayU3Ed__3_System_Collections_IEnumerator_get_Current_m8F44381262CB765F0D5A5F01BEBC5B97EA895374 ();
// 0x0000096B System.Void CoroutineExtention_<WaitAndOfFramePlay>d__4::.ctor(System.Int32)
extern void U3CWaitAndOfFramePlayU3Ed__4__ctor_m514829B08C1832F148648DCD1799DF6C38C88C54 ();
// 0x0000096C System.Void CoroutineExtention_<WaitAndOfFramePlay>d__4::System.IDisposable.Dispose()
extern void U3CWaitAndOfFramePlayU3Ed__4_System_IDisposable_Dispose_m0A6CDABC6181A2439606BF85693544384C023010 ();
// 0x0000096D System.Boolean CoroutineExtention_<WaitAndOfFramePlay>d__4::MoveNext()
extern void U3CWaitAndOfFramePlayU3Ed__4_MoveNext_m31D4C3A4E75ECECD8981B89BD0CAD8B29123150D ();
// 0x0000096E System.Object CoroutineExtention_<WaitAndOfFramePlay>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndOfFramePlayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA734C7C26A71C97655C355E6766C23088CFB0570 ();
// 0x0000096F System.Void CoroutineExtention_<WaitAndOfFramePlay>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndOfFramePlayU3Ed__4_System_Collections_IEnumerator_Reset_m0807477B0BCE0520DD18859595DAEBF40549E35F ();
// 0x00000970 System.Object CoroutineExtention_<WaitAndOfFramePlay>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndOfFramePlayU3Ed__4_System_Collections_IEnumerator_get_Current_mE728D8371B203B5E1063076E1CE4BAD3E45CDDBE ();
// 0x00000971 System.Void CoroutineExtention_<Lerp>d__5::.ctor(System.Int32)
extern void U3CLerpU3Ed__5__ctor_m3C57875C4287B4288841E7958A7328D6281E9A41 ();
// 0x00000972 System.Void CoroutineExtention_<Lerp>d__5::System.IDisposable.Dispose()
extern void U3CLerpU3Ed__5_System_IDisposable_Dispose_mD06BCCB05C8594F3A744370DC241160B790FDC8B ();
// 0x00000973 System.Boolean CoroutineExtention_<Lerp>d__5::MoveNext()
extern void U3CLerpU3Ed__5_MoveNext_mC8A7461F62EF42329ABBDF8086868C1330325928 ();
// 0x00000974 System.Object CoroutineExtention_<Lerp>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLerpU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB2CB7378663BCCAA13DCCB6982FBDE55D519AE1 ();
// 0x00000975 System.Void CoroutineExtention_<Lerp>d__5::System.Collections.IEnumerator.Reset()
extern void U3CLerpU3Ed__5_System_Collections_IEnumerator_Reset_mCCCB3067431E1038C152055E021D126366EB34CA ();
// 0x00000976 System.Object CoroutineExtention_<Lerp>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CLerpU3Ed__5_System_Collections_IEnumerator_get_Current_mE4C07B5FD00E151A153115AE02740480A33BC86F ();
// 0x00000977 System.Void InputController_<>c::.cctor()
extern void U3CU3Ec__cctor_mAE5D7B5EE088B27CB0D9AB34760C1456591E036B ();
// 0x00000978 System.Void InputController_<>c::.ctor()
extern void U3CU3Ec__ctor_mFE33514F4C921196F198DF21063EB1F28B193F0E ();
// 0x00000979 System.Void InputController_<>c::<Start>b__34_0()
extern void U3CU3Ec_U3CStartU3Eb__34_0_m91554E1E59FDEC8101E910D410BE889A7372CF39 ();
// 0x0000097A System.Void InputController_<>c::<Start>b__34_1(UnityEngine.Vector3)
extern void U3CU3Ec_U3CStartU3Eb__34_1_m216070042866E12F02B54F2444085288EA87B303 ();
// 0x0000097B System.Void InputController_<>c::<Start>b__34_2(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__34_2_m4D37DDDD1A3946847DF7EE945DB6F36B6640DFD3 ();
// 0x0000097C System.Void InputController_<>c::<Start>b__34_3(System.Single)
extern void U3CU3Ec_U3CStartU3Eb__34_3_m32968282A6AC5D5D8EE03BB8D07E7C0EC7EEF888 ();
// 0x0000097D System.Void InputController_<>c::<Start>b__34_4(UnityEngine.Vector3)
extern void U3CU3Ec_U3CStartU3Eb__34_4_m49D99AADBEC08B8A537888FEEE2D1AE7E247BA4D ();
// 0x0000097E System.Void InputController_<>c::<Start>b__34_5()
extern void U3CU3Ec_U3CStartU3Eb__34_5_m3BEBD66747CA8EF043510A6B5C71CBAE59BED3D8 ();
// 0x0000097F System.Void InvokableCallback`1_<>c::.cctor()
// 0x00000980 System.Void InvokableCallback`1_<>c::.ctor()
// 0x00000981 TReturn InvokableCallback`1_<>c::<.ctor>b__3_0()
// 0x00000982 System.Void InvokableCallback`2_<>c::.cctor()
// 0x00000983 System.Void InvokableCallback`2_<>c::.ctor()
// 0x00000984 TReturn InvokableCallback`2_<>c::<.ctor>b__3_0(T0)
// 0x00000985 System.Void InvokableCallback`3_<>c::.cctor()
// 0x00000986 System.Void InvokableCallback`3_<>c::.ctor()
// 0x00000987 TReturn InvokableCallback`3_<>c::<.ctor>b__3_0(T0,T1)
// 0x00000988 System.Void InvokableCallback`4_<>c::.cctor()
// 0x00000989 System.Void InvokableCallback`4_<>c::.ctor()
// 0x0000098A TReturn InvokableCallback`4_<>c::<.ctor>b__3_0(T0,T1,T2)
// 0x0000098B System.Void InvokableCallback`5_<>c::.cctor()
// 0x0000098C System.Void InvokableCallback`5_<>c::.ctor()
// 0x0000098D TReturn InvokableCallback`5_<>c::<.ctor>b__3_0(T0,T1,T2,T3)
// 0x0000098E System.Void InvokableEvent_<>c::.cctor()
extern void U3CU3Ec__cctor_m038623D71333A252CE1B406D2D61CEE642A56E3A ();
// 0x0000098F System.Void InvokableEvent_<>c::.ctor()
extern void U3CU3Ec__ctor_m838C2D9A61CF690528B20387CF4DB2C9EBA2CA97 ();
// 0x00000990 System.Void InvokableEvent_<>c::<.ctor>b__3_0()
extern void U3CU3Ec_U3C_ctorU3Eb__3_0_m196874FD63EA078E3E4C623F63D6A1CD00358F06 ();
// 0x00000991 System.Void InvokableEvent`1_<>c::.cctor()
// 0x00000992 System.Void InvokableEvent`1_<>c::.ctor()
// 0x00000993 System.Void InvokableEvent`1_<>c::<.ctor>b__3_0(T0)
// 0x00000994 System.Void InvokableEvent`2_<>c::.cctor()
// 0x00000995 System.Void InvokableEvent`2_<>c::.ctor()
// 0x00000996 System.Void InvokableEvent`2_<>c::<.ctor>b__3_0(T0,T1)
// 0x00000997 System.Void InvokableEvent`3_<>c::.cctor()
// 0x00000998 System.Void InvokableEvent`3_<>c::.ctor()
// 0x00000999 System.Void InvokableEvent`3_<>c::<.ctor>b__3_0(T0,T1,T2)
// 0x0000099A System.Void InvokableEvent`4_<>c::.cctor()
// 0x0000099B System.Void InvokableEvent`4_<>c::.ctor()
// 0x0000099C System.Void InvokableEvent`4_<>c::<.ctor>b__3_0(T0,T1,T2,T3)
// 0x0000099D System.Void SerializableCallbackBase_<>c::.cctor()
extern void U3CU3Ec__cctor_m9AB8D2268F2317F06D072C5D4867F8B76ED8054D ();
// 0x0000099E System.Void SerializableCallbackBase_<>c::.ctor()
extern void U3CU3Ec__ctor_mC2A0F0A5DEDF9E498C987FB8A008A8AAC9582227 ();
// 0x0000099F System.Object SerializableCallbackBase_<>c::<get_Args>b__7_0(Arg)
extern void U3CU3Ec_U3Cget_ArgsU3Eb__7_0_m5DF1F932707D0B6E9B3413236F344241E1F610CA ();
// 0x000009A0 System.Type SerializableCallbackBase_<>c::<get_ArgTypes>b__10_0(Arg)
extern void U3CU3Ec_U3Cget_ArgTypesU3Eb__10_0_mCF898E1E6759B1C9C4609FCE87AC1FFE4EE8AE47 ();
// 0x000009A1 System.Void StorageData`1_SetsListenerDelegate::.ctor(System.Object,System.IntPtr)
// 0x000009A2 System.Void StorageData`1_SetsListenerDelegate::Invoke()
// 0x000009A3 System.IAsyncResult StorageData`1_SetsListenerDelegate::BeginInvoke(System.AsyncCallback,System.Object)
// 0x000009A4 System.Void StorageData`1_SetsListenerDelegate::EndInvoke(System.IAsyncResult)
// 0x000009A5 System.Void StorageData`1_GetterDelegate::.ctor(System.Object,System.IntPtr)
// 0x000009A6 T StorageData`1_GetterDelegate::Invoke()
// 0x000009A7 System.IAsyncResult StorageData`1_GetterDelegate::BeginInvoke(System.AsyncCallback,System.Object)
// 0x000009A8 T StorageData`1_GetterDelegate::EndInvoke(System.IAsyncResult)
// 0x000009A9 System.Void StorageData`1_SetterDelegate::.ctor(System.Object,System.IntPtr)
// 0x000009AA System.Void StorageData`1_SetterDelegate::Invoke(T)
// 0x000009AB System.IAsyncResult StorageData`1_SetterDelegate::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000009AC System.Void StorageData`1_SetterDelegate::EndInvoke(System.IAsyncResult)
// 0x000009AD System.Void Timer_<>c::.cctor()
extern void U3CU3Ec__cctor_m72DB59322C7C192D3F4506F34E3C06F28E53D3DB ();
// 0x000009AE System.Void Timer_<>c::.ctor()
extern void U3CU3Ec__ctor_mE87573A50889DBC66C22A7C00C2690F189854B32 ();
// 0x000009AF System.Void Timer_<>c::<InitEvents>b__27_1(System.Int32)
extern void U3CU3Ec_U3CInitEventsU3Eb__27_1_mD7585AEDB0823C00C1812AB622A5BB4B13CA9A9E ();
// 0x000009B0 System.Void TimerManager_<Ticker>d__5::.ctor(System.Int32)
extern void U3CTickerU3Ed__5__ctor_m6506A274E76933FB14B2D80126EB4CDF4C93FE52 ();
// 0x000009B1 System.Void TimerManager_<Ticker>d__5::System.IDisposable.Dispose()
extern void U3CTickerU3Ed__5_System_IDisposable_Dispose_m28F47615CC8D3541C662D3AA4EA295FFC9E5F0B0 ();
// 0x000009B2 System.Boolean TimerManager_<Ticker>d__5::MoveNext()
extern void U3CTickerU3Ed__5_MoveNext_m5932E832B4560B9DD5CA58EF54C3F85A01852475 ();
// 0x000009B3 System.Object TimerManager_<Ticker>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTickerU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1D998FD79959A083E7BAC3ED3798DB903647F8F ();
// 0x000009B4 System.Void TimerManager_<Ticker>d__5::System.Collections.IEnumerator.Reset()
extern void U3CTickerU3Ed__5_System_Collections_IEnumerator_Reset_mC0E414321BE252B62603D8E6DC8F996B0FE4AF7B ();
// 0x000009B5 System.Object TimerManager_<Ticker>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CTickerU3Ed__5_System_Collections_IEnumerator_get_Current_mCB60A525C9D216A4FE0543C499301866E55ED4DA ();
// 0x000009B6 System.Void vp_Activity_Callback::.ctor(System.Object,System.IntPtr)
extern void Callback__ctor_m3B9F1F167DC8FCDF4C5811F465A073CDEA894AED ();
// 0x000009B7 System.Void vp_Activity_Callback::Invoke()
extern void Callback_Invoke_m6D3D986C1EBEE9E090B94E57869CA3FEA6D68FF7 ();
// 0x000009B8 System.IAsyncResult vp_Activity_Callback::BeginInvoke(System.AsyncCallback,System.Object)
extern void Callback_BeginInvoke_mB892560A04FB24A3A18A2B8D7A04292AFD864256 ();
// 0x000009B9 System.Void vp_Activity_Callback::EndInvoke(System.IAsyncResult)
extern void Callback_EndInvoke_m07E1FA189D55E7227328479F71E7E9953822E64A ();
// 0x000009BA System.Void vp_Activity_Condition::.ctor(System.Object,System.IntPtr)
extern void Condition__ctor_m580BF058D886A9E785A7765CC50863F5F3F79CBF ();
// 0x000009BB System.Boolean vp_Activity_Condition::Invoke()
extern void Condition_Invoke_m680DFC9E62DC66301607D6F90C20C02EFADA7AED ();
// 0x000009BC System.IAsyncResult vp_Activity_Condition::BeginInvoke(System.AsyncCallback,System.Object)
extern void Condition_BeginInvoke_m46715EBC5923A61C0BF8939F130E9D5CA1B150AA ();
// 0x000009BD System.Boolean vp_Activity_Condition::EndInvoke(System.IAsyncResult)
extern void Condition_EndInvoke_mF1CDA8DCA7C162302EE549310A0196EDD945E4BE ();
// 0x000009BE System.Void vp_Attempt_Tryer::.ctor(System.Object,System.IntPtr)
extern void Tryer__ctor_m73C1FA3A45C39E4E699764440A93769E8F3FD531 ();
// 0x000009BF System.Boolean vp_Attempt_Tryer::Invoke()
extern void Tryer_Invoke_m58F98D306E1E48997BAE64205FCE05473F9A44B7 ();
// 0x000009C0 System.IAsyncResult vp_Attempt_Tryer::BeginInvoke(System.AsyncCallback,System.Object)
extern void Tryer_BeginInvoke_m3CF369F558DEC667DEFF6FC12570779B83741BC8 ();
// 0x000009C1 System.Boolean vp_Attempt_Tryer::EndInvoke(System.IAsyncResult)
extern void Tryer_EndInvoke_m6769E533D6676E48CC99175B1DCF58A368D1F6D0 ();
// 0x000009C2 System.Void vp_Attempt`1_Tryer`1::.ctor(System.Object,System.IntPtr)
// 0x000009C3 System.Boolean vp_Attempt`1_Tryer`1::Invoke(T)
// 0x000009C4 System.IAsyncResult vp_Attempt`1_Tryer`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000009C5 System.Boolean vp_Attempt`1_Tryer`1::EndInvoke(System.IAsyncResult)
// 0x000009C6 System.Void vp_EventHandler_ScriptMethods::.ctor(System.Type)
extern void ScriptMethods__ctor_m562578213FDD763B1C1161ACCBE72D7673EDB66B ();
// 0x000009C7 System.Collections.Generic.List`1<System.Reflection.MethodInfo> vp_EventHandler_ScriptMethods::GetMethods(System.Type)
extern void ScriptMethods_GetMethods_m3E6E02F5B86C26756D6C4C5781259F9616C6FDD5 ();
// 0x000009C8 System.Void vp_GlobalEventInternal_UnregisterException::.ctor(System.String)
extern void UnregisterException__ctor_m452F6AD6AC1664861D8D038C79EB553EDEED558E ();
// 0x000009C9 System.Void vp_GlobalEventInternal_SendException::.ctor(System.String)
extern void SendException__ctor_m02E88335A30B39B623F73613DDD999950338E265 ();
// 0x000009CA System.Void vp_Message_Sender::.ctor(System.Object,System.IntPtr)
extern void Sender__ctor_m50218838A9C6B69714950576DFA75F4D592FD05D ();
// 0x000009CB System.Void vp_Message_Sender::Invoke()
extern void Sender_Invoke_m3116C57F31119A05504CC4C2F764F5757EE6A7DB ();
// 0x000009CC System.IAsyncResult vp_Message_Sender::BeginInvoke(System.AsyncCallback,System.Object)
extern void Sender_BeginInvoke_m574BC87830A5BBD07D8FA8F28C34FC5723E5578B ();
// 0x000009CD System.Void vp_Message_Sender::EndInvoke(System.IAsyncResult)
extern void Sender_EndInvoke_mD361DE1FF0F0E790382FC3E62B81E4A48EF6F1E0 ();
// 0x000009CE System.Void vp_Message`1_Sender`1::.ctor(System.Object,System.IntPtr)
// 0x000009CF System.Void vp_Message`1_Sender`1::Invoke(T)
// 0x000009D0 System.IAsyncResult vp_Message`1_Sender`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000009D1 System.Void vp_Message`1_Sender`1::EndInvoke(System.IAsyncResult)
// 0x000009D2 System.Void vp_Message`2_Sender`2::.ctor(System.Object,System.IntPtr)
// 0x000009D3 TResult vp_Message`2_Sender`2::Invoke(T)
// 0x000009D4 System.IAsyncResult vp_Message`2_Sender`2::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000009D5 TResult vp_Message`2_Sender`2::EndInvoke(System.IAsyncResult)
// 0x000009D6 System.Void vp_Value`1_Getter`1::.ctor(System.Object,System.IntPtr)
// 0x000009D7 T vp_Value`1_Getter`1::Invoke()
// 0x000009D8 System.IAsyncResult vp_Value`1_Getter`1::BeginInvoke(System.AsyncCallback,System.Object)
// 0x000009D9 T vp_Value`1_Getter`1::EndInvoke(System.IAsyncResult)
// 0x000009DA System.Void vp_Value`1_Setter`1::.ctor(System.Object,System.IntPtr)
// 0x000009DB System.Void vp_Value`1_Setter`1::Invoke(T)
// 0x000009DC System.IAsyncResult vp_Value`1_Setter`1::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000009DD System.Void vp_Value`1_Setter`1::EndInvoke(System.IAsyncResult)
// 0x000009DE System.Void vp_PoolManager_vp_PreloadedPrefab::.ctor()
extern void vp_PreloadedPrefab__ctor_m4CDA3A6564B496C3AB953931679478A566FE5EA7 ();
// 0x000009DF System.Void vp_PoolManager_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mCD9905465795BBD53A86044608E804E02BD1CC92 ();
// 0x000009E0 System.Void vp_PoolManager_<>c__DisplayClass15_0::<Despawn>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDespawnU3Eb__0_m614760325CBAC4DCBC63E34505056E274811C831 ();
// 0x000009E1 System.Void vp_Timer_Callback::.ctor(System.Object,System.IntPtr)
extern void Callback__ctor_m56FE6C8553E18184859AD3DF01727565DF7F910C ();
// 0x000009E2 System.Void vp_Timer_Callback::Invoke()
extern void Callback_Invoke_m3A5AA72A5D0FB5CCFB9DF56B2B5E4A4627804E9A ();
// 0x000009E3 System.IAsyncResult vp_Timer_Callback::BeginInvoke(System.AsyncCallback,System.Object)
extern void Callback_BeginInvoke_m13FDE19DB4B0C59FFAB25B0BB810C18DC385E763 ();
// 0x000009E4 System.Void vp_Timer_Callback::EndInvoke(System.IAsyncResult)
extern void Callback_EndInvoke_mA093EDF2D1B815FE2DDFD7FF7EEBF6614A092FE8 ();
// 0x000009E5 System.Void vp_Timer_ArgCallback::.ctor(System.Object,System.IntPtr)
extern void ArgCallback__ctor_m54F3568C2236D0C945D04D797C17BFFD6252E04C ();
// 0x000009E6 System.Void vp_Timer_ArgCallback::Invoke(System.Object)
extern void ArgCallback_Invoke_mFC4A16689B521E5415EB72EA62090F7AFF7E19C0 ();
// 0x000009E7 System.IAsyncResult vp_Timer_ArgCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void ArgCallback_BeginInvoke_m5BDEFF08A315C338C6B0226CF602C0FD37E1BF3E ();
// 0x000009E8 System.Void vp_Timer_ArgCallback::EndInvoke(System.IAsyncResult)
extern void ArgCallback_EndInvoke_m4480BDE54488767C93DEC1F81332BFFAF778928E ();
// 0x000009E9 System.Void vp_Timer_Event::Execute()
extern void Event_Execute_m2224A3F8441A1B5740F0B81AD62CF8452FACBDF6 ();
// 0x000009EA System.Void vp_Timer_Event::Recycle()
extern void Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E ();
// 0x000009EB System.Void vp_Timer_Event::Destroy()
extern void Event_Destroy_m54F753C2DB0D932A829D856B16E14C11F7F18AF9 ();
// 0x000009EC System.Void vp_Timer_Event::Error(System.String)
extern void Event_Error_mB82C87213850999FBA9FF7CC1AF57504C07CADA5 ();
// 0x000009ED System.String vp_Timer_Event::get_MethodName()
extern void Event_get_MethodName_m2E7322780EA4E933FB1C38D9E0863E8C719DBC2E ();
// 0x000009EE System.String vp_Timer_Event::get_MethodInfo()
extern void Event_get_MethodInfo_m6D8C37E774DC62EF53B32C7200739E777903BBB3 ();
// 0x000009EF System.Void vp_Timer_Event::.ctor()
extern void Event__ctor_m8A8093020EB76395E72386F4BCFA01A44DA356EB ();
// 0x000009F0 System.Boolean vp_Timer_Handle::get_Paused()
extern void Handle_get_Paused_m562EB5C50C38188FF7F7F259F226F0413431A21C ();
// 0x000009F1 System.Void vp_Timer_Handle::set_Paused(System.Boolean)
extern void Handle_set_Paused_mFB84304A3C08EC30C7DCFAC8798AB87B54BD03D9 ();
// 0x000009F2 System.Single vp_Timer_Handle::get_TimeOfInitiation()
extern void Handle_get_TimeOfInitiation_mCF50BA6AB357EC8F5C48CD34CBB3BA215E0DF8D2 ();
// 0x000009F3 System.Single vp_Timer_Handle::get_TimeOfFirstIteration()
extern void Handle_get_TimeOfFirstIteration_mA39FE629D19E11FB05828A3270347DC5D9394868 ();
// 0x000009F4 System.Single vp_Timer_Handle::get_TimeOfNextIteration()
extern void Handle_get_TimeOfNextIteration_mBFD62F4ED09C4A9F171C38186FDF15C340D575D9 ();
// 0x000009F5 System.Single vp_Timer_Handle::get_TimeOfLastIteration()
extern void Handle_get_TimeOfLastIteration_mF336E13CE195013F54C2B844BB36C0786283FCE4 ();
// 0x000009F6 System.Single vp_Timer_Handle::get_Delay()
extern void Handle_get_Delay_mC0587B87BD995E4254FF0376B9C6E452A7B739A9 ();
// 0x000009F7 System.Single vp_Timer_Handle::get_Interval()
extern void Handle_get_Interval_m69485C36EEA899AD123B4611691EFF45B2769B73 ();
// 0x000009F8 System.Single vp_Timer_Handle::get_TimeUntilNextIteration()
extern void Handle_get_TimeUntilNextIteration_m10569D96A53447028A4808698BAA5C7E4723DDAC ();
// 0x000009F9 System.Single vp_Timer_Handle::get_DurationLeft()
extern void Handle_get_DurationLeft_m6C99B4B8342D947F4B702426095273A300A394C5 ();
// 0x000009FA System.Single vp_Timer_Handle::get_DurationTotal()
extern void Handle_get_DurationTotal_m3A09F8D669E6A690108BC47DE33BFCDCBEEEC982 ();
// 0x000009FB System.Single vp_Timer_Handle::get_Duration()
extern void Handle_get_Duration_m5F17B06185F2C5274416BE67918561ECC787B026 ();
// 0x000009FC System.Int32 vp_Timer_Handle::get_IterationsTotal()
extern void Handle_get_IterationsTotal_m4AB3FA38F14F019FD2C636E80C907904D3760CF4 ();
// 0x000009FD System.Int32 vp_Timer_Handle::get_IterationsLeft()
extern void Handle_get_IterationsLeft_m55653075F94469335A328CDA19F24AC02595BF11 ();
// 0x000009FE System.Int32 vp_Timer_Handle::get_Id()
extern void Handle_get_Id_m94F85EB949AF983F520F10F557ECBE62B7E7725E ();
// 0x000009FF System.Void vp_Timer_Handle::set_Id(System.Int32)
extern void Handle_set_Id_m035AAB0529283D2F19A200F186C5AFDAB19F666D ();
// 0x00000A00 System.Boolean vp_Timer_Handle::get_Active()
extern void Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C ();
// 0x00000A01 System.String vp_Timer_Handle::get_MethodName()
extern void Handle_get_MethodName_m0AE43AEDF241A34CC0BB638AA8B92945CDE5F88F ();
// 0x00000A02 System.String vp_Timer_Handle::get_MethodInfo()
extern void Handle_get_MethodInfo_m9F6A97E4B626742350A556D1C05029023B8AFA3F ();
// 0x00000A03 System.Boolean vp_Timer_Handle::get_CancelOnLoad()
extern void Handle_get_CancelOnLoad_mFB1F3942A62413B5DDAD1C54AAAF0C56FFB27BF2 ();
// 0x00000A04 System.Void vp_Timer_Handle::set_CancelOnLoad(System.Boolean)
extern void Handle_set_CancelOnLoad_mE921CED0AF28227617B3E4F315749E4205211216 ();
// 0x00000A05 System.Void vp_Timer_Handle::Cancel()
extern void Handle_Cancel_mFE3C59AD52725C2AF4777A670FB61E2CA05D54EF ();
// 0x00000A06 System.Void vp_Timer_Handle::Execute()
extern void Handle_Execute_m9B76827FC4A9073BB28118B740366FA064C0FF2A ();
// 0x00000A07 System.Void vp_Timer_Handle::.ctor()
extern void Handle__ctor_m685E26AAB613CA402290CFD650E608B180BC91EC ();
// 0x00000A08 System.Void vp_Timer_<>c::.cctor()
extern void U3CU3Ec__cctor_mE2FD9741E9B6569B6DF9BAFC826AA9FAACEB1964 ();
// 0x00000A09 System.Void vp_Timer_<>c::.ctor()
extern void U3CU3Ec__ctor_mF1EA0DF766594CC8579CF02CAF353208E57AD94E ();
// 0x00000A0A System.Void vp_Timer_<>c::<Start>b__24_0()
extern void U3CU3Ec_U3CStartU3Eb__24_0_mB0C59AB47BA9810AF8601A63A7289233362D339D ();
// 0x00000A0B System.Void vp_Utility_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m8FAC57006AAA255FFF14C50AEDCE44E0836232BA ();
// 0x00000A0C System.Void vp_Utility_<>c__DisplayClass18_0::<Destroy>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDestroyU3Eb__0_m325D201A9C46B0BEAA9E5AEC6E86866FCF616943 ();
// 0x00000A0D System.Void UIEventsUtility_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mF43CB9A9E4D08847E6D5EEC6029818F47412160F ();
// 0x00000A0E System.Void UIEventsUtility_<>c__DisplayClass0_0::<AddEvent>b__0(UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec__DisplayClass0_0_U3CAddEventU3Eb__0_m0FCC9FCAADCBDEA562747CB8171EEAB51BBC9980 ();
// 0x00000A0F System.Void VisualSetter_<SwithcVisual>d__18::.ctor(System.Int32)
extern void U3CSwithcVisualU3Ed__18__ctor_m298249329919476E31CF4FFF4CB2F41BD046215E ();
// 0x00000A10 System.Void VisualSetter_<SwithcVisual>d__18::System.IDisposable.Dispose()
extern void U3CSwithcVisualU3Ed__18_System_IDisposable_Dispose_m6C17DFC43B78B5B601FA23A6691C19AF3B2EF105 ();
// 0x00000A11 System.Boolean VisualSetter_<SwithcVisual>d__18::MoveNext()
extern void U3CSwithcVisualU3Ed__18_MoveNext_mFB5988015BCF65DAC7DFDD8CE73751BACF1559E6 ();
// 0x00000A12 System.Object VisualSetter_<SwithcVisual>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSwithcVisualU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1AC45232834AE473FCA8BC71E4FC381CAE5305C ();
// 0x00000A13 System.Void VisualSetter_<SwithcVisual>d__18::System.Collections.IEnumerator.Reset()
extern void U3CSwithcVisualU3Ed__18_System_Collections_IEnumerator_Reset_m476CFF7A7792C90AA554F55D5195EFAB82180401 ();
// 0x00000A14 System.Object VisualSetter_<SwithcVisual>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CSwithcVisualU3Ed__18_System_Collections_IEnumerator_get_Current_m948D76E9579C4B2627E79640719CA213C93AB374 ();
// 0x00000A15 System.Void iOSMusic_<LoadMusic>d__35::.ctor(System.Int32)
extern void U3CLoadMusicU3Ed__35__ctor_m0B02DD6D0AA2B569773BE89F51B635BE6C200119 ();
// 0x00000A16 System.Void iOSMusic_<LoadMusic>d__35::System.IDisposable.Dispose()
extern void U3CLoadMusicU3Ed__35_System_IDisposable_Dispose_m5ED4E5A7E5FDAF1721EE0B6BF12D306CFF37FAEF ();
// 0x00000A17 System.Boolean iOSMusic_<LoadMusic>d__35::MoveNext()
extern void U3CLoadMusicU3Ed__35_MoveNext_m19DEEE2C2F9662E92F5425E60E6A056D5DEEDD59 ();
// 0x00000A18 System.Object iOSMusic_<LoadMusic>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadMusicU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m368EF12CAD7A0ECB24BB685951FA34493C6249AB ();
// 0x00000A19 System.Void iOSMusic_<LoadMusic>d__35::System.Collections.IEnumerator.Reset()
extern void U3CLoadMusicU3Ed__35_System_Collections_IEnumerator_Reset_m53A2F85EEF37E07259A530350494978BE9C3CD69 ();
// 0x00000A1A System.Object iOSMusic_<LoadMusic>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CLoadMusicU3Ed__35_System_Collections_IEnumerator_get_Current_mD44FFC92C2104744377E78AC73287B9C466DE042 ();
// 0x00000A1B System.Void FindButtons_<>c::.cctor()
extern void U3CU3Ec__cctor_mE0F2E0A923AE9F143BCB511E8D47F1A736E11283 ();
// 0x00000A1C System.Void FindButtons_<>c::.ctor()
extern void U3CU3Ec__ctor_m65124A33D43C64BF99CE90D3B26EDBE1BFE8AD31 ();
// 0x00000A1D System.Void FindButtons_<>c::<Start>b__2_0()
extern void U3CU3Ec_U3CStartU3Eb__2_0_mCE858024AA3B8EE96DFF873C62445A7EA1584FF5 ();
// 0x00000A1E System.Void PuzzleField_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m4CF85D6EB0AB25B0EED7EA3DB448237E727F9EAD ();
// 0x00000A1F System.Void PuzzleField_<>c__DisplayClass6_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CInitU3Eb__0_m9053BED81441A6BA5601C87AC4F33B4F85C57E2F ();
// 0x00000A20 System.Void RoadsField_<>c::.cctor()
extern void U3CU3Ec__cctor_m29E10ED3D479A5B303C91B5ACECCC5708BA756EA ();
// 0x00000A21 System.Void RoadsField_<>c::.ctor()
extern void U3CU3Ec__ctor_mE234A715652514E5C6187EFC7871C1E1DC7CDF6C ();
// 0x00000A22 System.Void RoadsField_<>c::<PlayAnimation>b__22_0()
extern void U3CU3Ec_U3CPlayAnimationU3Eb__22_0_m342AF2CAFB727BF50E54117DAF656D8B99A64186 ();
// 0x00000A23 System.Void RoadsField_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m32C724584CBAD3CA55E6C021B202BFDF8DCAC3D4 ();
// 0x00000A24 System.Void RoadsField_<>c__DisplayClass26_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CInitU3Eb__0_m35F584C51C85EED13C9E4C29041A1487E3732A1C ();
// 0x00000A25 System.Void RotatePicturesField_<>c::.cctor()
extern void U3CU3Ec__cctor_m3537837388E9168F44B49CC1F3A3FA936452A4FF ();
// 0x00000A26 System.Void RotatePicturesField_<>c::.ctor()
extern void U3CU3Ec__ctor_m2D3B5946F8138CE48CAA3FF9E7C1BB4CAAD22582 ();
// 0x00000A27 System.Void RotatePicturesField_<>c::<PlayAnimation>b__12_0()
extern void U3CU3Ec_U3CPlayAnimationU3Eb__12_0_mCE9630156D3FBB3BF18D60C1A49E6B0A14EC0C84 ();
// 0x00000A28 System.Void RotatePicturesField_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mE7A9FB7FC02ECC034AAD7C67FD9A198243012CB4 ();
// 0x00000A29 System.Void RotatePicturesField_<>c__DisplayClass16_0::<Init>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CInitU3Eb__0_mDD20FEF534A2F92BE4C161E1D49667B9E40672AD ();
// 0x00000A2A System.Void NativeAdsController_<>c::.cctor()
extern void U3CU3Ec__cctor_m15BB7DB6A26FFF10DBB041EA3E03BFCA520427DF ();
// 0x00000A2B System.Void NativeAdsController_<>c::.ctor()
extern void U3CU3Ec__ctor_mFD5583F72D234F5D0C4B73F64D96D7F67C903D7C ();
// 0x00000A2C System.Void NativeAdsController_<>c::<set_IsShowedNative>b__4_0()
extern void U3CU3Ec_U3Cset_IsShowedNativeU3Eb__4_0_m70D97E494D48AD558DF01741A5D0FAC415D826FF ();
// 0x00000A2D System.Void NativeAdsController_<>c::<set_IsShowedNative>b__4_1()
extern void U3CU3Ec_U3Cset_IsShowedNativeU3Eb__4_1_m3F02D703082B981F44E3AF6FD33E452D1DCC0D33 ();
// 0x00000A2E System.Void RemoteSettingsCallbacksInitializer_<WaitEndOfFrame>d__4::.ctor(System.Int32)
extern void U3CWaitEndOfFrameU3Ed__4__ctor_mD14B6667DFDD079BD44612E69BB55CD93389FB5B ();
// 0x00000A2F System.Void RemoteSettingsCallbacksInitializer_<WaitEndOfFrame>d__4::System.IDisposable.Dispose()
extern void U3CWaitEndOfFrameU3Ed__4_System_IDisposable_Dispose_m4C8432B484493DC8CD436A040844A8755BFA18E8 ();
// 0x00000A30 System.Boolean RemoteSettingsCallbacksInitializer_<WaitEndOfFrame>d__4::MoveNext()
extern void U3CWaitEndOfFrameU3Ed__4_MoveNext_m2872B6D76B202B2EF3600CD2226FEB2105C02305 ();
// 0x00000A31 System.Object RemoteSettingsCallbacksInitializer_<WaitEndOfFrame>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitEndOfFrameU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAB37B45A9D7820D01759B041AF2660F76894750 ();
// 0x00000A32 System.Void RemoteSettingsCallbacksInitializer_<WaitEndOfFrame>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitEndOfFrameU3Ed__4_System_Collections_IEnumerator_Reset_mE5EA86DFE2C037E790ED4B2282D536C9DA16A714 ();
// 0x00000A33 System.Object RemoteSettingsCallbacksInitializer_<WaitEndOfFrame>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitEndOfFrameU3Ed__4_System_Collections_IEnumerator_get_Current_m141736F70D9CCAD73020F45661F894B12115BFCE ();
// 0x00000A34 System.Void PerformanceController_<PerformanceControlRoutine>d__13::.ctor(System.Int32)
extern void U3CPerformanceControlRoutineU3Ed__13__ctor_mA4331522DC6ACF6FFB7FB757E7B59EF09A0CF06A ();
// 0x00000A35 System.Void PerformanceController_<PerformanceControlRoutine>d__13::System.IDisposable.Dispose()
extern void U3CPerformanceControlRoutineU3Ed__13_System_IDisposable_Dispose_m688A2604FBD6187F56F06A5E0273F48358B5D0DD ();
// 0x00000A36 System.Boolean PerformanceController_<PerformanceControlRoutine>d__13::MoveNext()
extern void U3CPerformanceControlRoutineU3Ed__13_MoveNext_m6DC330C7AF8671D4EA2B30626F3233C1AF045A6E ();
// 0x00000A37 System.Object PerformanceController_<PerformanceControlRoutine>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPerformanceControlRoutineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94B213A582357FEC1661E6C28506C966CBE16F6E ();
// 0x00000A38 System.Void PerformanceController_<PerformanceControlRoutine>d__13::System.Collections.IEnumerator.Reset()
extern void U3CPerformanceControlRoutineU3Ed__13_System_Collections_IEnumerator_Reset_m548CA7D9D2CF1A2E5F776BF68A0BAD535B2244DE ();
// 0x00000A39 System.Object PerformanceController_<PerformanceControlRoutine>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CPerformanceControlRoutineU3Ed__13_System_Collections_IEnumerator_get_Current_m73E9E45B12BDB6F5BE6AF3CC68109AA519F649CE ();
// 0x00000A3A System.Void AppleAlbumPreview_<>c::.cctor()
extern void U3CU3Ec__cctor_m14D2EB0A18819561027351B1B2511EEAF997EEBD ();
// 0x00000A3B System.Void AppleAlbumPreview_<>c::.ctor()
extern void U3CU3Ec__ctor_m73A046E9BD32731619597CA5F79E2CC32AAD57E9 ();
// 0x00000A3C System.Void AppleAlbumPreview_<>c::<PlayClip>b__27_0()
extern void U3CU3Ec_U3CPlayClipU3Eb__27_0_m07FEEE292EE21A34624AF93903D025BD3C04DF81 ();
// 0x00000A3D System.Void AppleAlbumPreview_<LoadAlbumDataRoutine>d__35::.ctor(System.Int32)
extern void U3CLoadAlbumDataRoutineU3Ed__35__ctor_mCED84ED9E8EB6615D9351B797E185BBEDECA925A ();
// 0x00000A3E System.Void AppleAlbumPreview_<LoadAlbumDataRoutine>d__35::System.IDisposable.Dispose()
extern void U3CLoadAlbumDataRoutineU3Ed__35_System_IDisposable_Dispose_m6FE46F5F5EFA0A988A8B4C0F1DB76ECC9678F1BA ();
// 0x00000A3F System.Boolean AppleAlbumPreview_<LoadAlbumDataRoutine>d__35::MoveNext()
extern void U3CLoadAlbumDataRoutineU3Ed__35_MoveNext_m7639F6DEDFAF1EC965F52425D3798D389C0D8319 ();
// 0x00000A40 System.Void AppleAlbumPreview_<LoadAlbumDataRoutine>d__35::<>m__Finally1()
extern void U3CLoadAlbumDataRoutineU3Ed__35_U3CU3Em__Finally1_mFD90409B33C30290502A368A0A5E020550A7326C ();
// 0x00000A41 System.Object AppleAlbumPreview_<LoadAlbumDataRoutine>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAlbumDataRoutineU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE123234CE30C9A775EC5D042B524B7CECE4D292 ();
// 0x00000A42 System.Void AppleAlbumPreview_<LoadAlbumDataRoutine>d__35::System.Collections.IEnumerator.Reset()
extern void U3CLoadAlbumDataRoutineU3Ed__35_System_Collections_IEnumerator_Reset_mECB6415647573B2696DE752548D1F2F0B47BA580 ();
// 0x00000A43 System.Object AppleAlbumPreview_<LoadAlbumDataRoutine>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAlbumDataRoutineU3Ed__35_System_Collections_IEnumerator_get_Current_mB06CD4BEA87A4590FFD0856858E708ECDBEC5CFD ();
// 0x00000A44 System.Void AppleMusicScreen_<LoadAlbumsWWW>d__20::.ctor(System.Int32)
extern void U3CLoadAlbumsWWWU3Ed__20__ctor_m40E3CD8B09E1AEF506F8397B73F41D8B71986E3E ();
// 0x00000A45 System.Void AppleMusicScreen_<LoadAlbumsWWW>d__20::System.IDisposable.Dispose()
extern void U3CLoadAlbumsWWWU3Ed__20_System_IDisposable_Dispose_mFDC0C0C3A57E094C6CD182815464511028CA5BF9 ();
// 0x00000A46 System.Boolean AppleMusicScreen_<LoadAlbumsWWW>d__20::MoveNext()
extern void U3CLoadAlbumsWWWU3Ed__20_MoveNext_m1F67DFE8042C68BF89852C619AD9B257D4CFD20B ();
// 0x00000A47 System.Object AppleMusicScreen_<LoadAlbumsWWW>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadAlbumsWWWU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2C386D726127B605BA39FCD5A9F15800CCEFCF7 ();
// 0x00000A48 System.Void AppleMusicScreen_<LoadAlbumsWWW>d__20::System.Collections.IEnumerator.Reset()
extern void U3CLoadAlbumsWWWU3Ed__20_System_Collections_IEnumerator_Reset_mD8296B57EEA26932A6052F910ED09FE3C9DD5CCA ();
// 0x00000A49 System.Object AppleMusicScreen_<LoadAlbumsWWW>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CLoadAlbumsWWWU3Ed__20_System_Collections_IEnumerator_get_Current_m8B4DC545DAC87F6A444165F1260B0983866C5DB2 ();
// 0x00000A4A System.Void AppleSongPanel_<LoadClip>d__10::.ctor(System.Int32)
extern void U3CLoadClipU3Ed__10__ctor_mF47FEECD16118BDAEE7F358CD579B3CD7766B3CE ();
// 0x00000A4B System.Void AppleSongPanel_<LoadClip>d__10::System.IDisposable.Dispose()
extern void U3CLoadClipU3Ed__10_System_IDisposable_Dispose_m1ECDEF3DE70F8FDBC3B2C8D41E02CFFDDEB43236 ();
// 0x00000A4C System.Boolean AppleSongPanel_<LoadClip>d__10::MoveNext()
extern void U3CLoadClipU3Ed__10_MoveNext_m2E2FBC46CFA9107CF27F33F2BC6F094A6C16101D ();
// 0x00000A4D System.Void AppleSongPanel_<LoadClip>d__10::<>m__Finally1()
extern void U3CLoadClipU3Ed__10_U3CU3Em__Finally1_mC78B1A9DDC2C8063F8E4E3C72E140335B66D23A2 ();
// 0x00000A4E System.Object AppleSongPanel_<LoadClip>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadClipU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CE591E181710A83BC56B0B29606FB93DB8A481A ();
// 0x00000A4F System.Void AppleSongPanel_<LoadClip>d__10::System.Collections.IEnumerator.Reset()
extern void U3CLoadClipU3Ed__10_System_Collections_IEnumerator_Reset_m2BDE0512E4E706D206AD7D63F409F7376F379572 ();
// 0x00000A50 System.Object AppleSongPanel_<LoadClip>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CLoadClipU3Ed__10_System_Collections_IEnumerator_get_Current_m12CE771044CD81A542760B4D40743CF3D0ACF6F2 ();
// 0x00000A51 System.Void BookScreen_<Swap>d__24::.ctor(System.Int32)
extern void U3CSwapU3Ed__24__ctor_m88A690C417A9B51429316B1559DEACA90E4C7B8C ();
// 0x00000A52 System.Void BookScreen_<Swap>d__24::System.IDisposable.Dispose()
extern void U3CSwapU3Ed__24_System_IDisposable_Dispose_mA2E36D3B5D11A4F31D2FEDE6EF08B140BDFCB203 ();
// 0x00000A53 System.Boolean BookScreen_<Swap>d__24::MoveNext()
extern void U3CSwapU3Ed__24_MoveNext_mABFACD7FAE68888B45B0ABDE3F697CBFECB9E653 ();
// 0x00000A54 System.Object BookScreen_<Swap>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSwapU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88A5D357CB75A47736DCB9E04E9FB41A1845AEA3 ();
// 0x00000A55 System.Void BookScreen_<Swap>d__24::System.Collections.IEnumerator.Reset()
extern void U3CSwapU3Ed__24_System_Collections_IEnumerator_Reset_m5321CC915BE87ECC53CB1433A5BF3817AD73C974 ();
// 0x00000A56 System.Object BookScreen_<Swap>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CSwapU3Ed__24_System_Collections_IEnumerator_get_Current_m2A8BCE3CA7BF8E69726D9234D8737506ED4AD2EE ();
// 0x00000A57 System.Void GlobalMainScreen_<LoadPageWWW>d__47::.ctor(System.Int32)
extern void U3CLoadPageWWWU3Ed__47__ctor_mD4CEC79EF48453FA4911FCE54E44F1EEA7CBFDFC ();
// 0x00000A58 System.Void GlobalMainScreen_<LoadPageWWW>d__47::System.IDisposable.Dispose()
extern void U3CLoadPageWWWU3Ed__47_System_IDisposable_Dispose_m1337A22408B7DE74B5DCE775850EAD6C54DC302B ();
// 0x00000A59 System.Boolean GlobalMainScreen_<LoadPageWWW>d__47::MoveNext()
extern void U3CLoadPageWWWU3Ed__47_MoveNext_mC363634DC0396A8BB888B5627CCEAF33138AF4E9 ();
// 0x00000A5A System.Object GlobalMainScreen_<LoadPageWWW>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadPageWWWU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD834A6B6DE0BEC639AB5DEA72E67F239A3704C6A ();
// 0x00000A5B System.Void GlobalMainScreen_<LoadPageWWW>d__47::System.Collections.IEnumerator.Reset()
extern void U3CLoadPageWWWU3Ed__47_System_Collections_IEnumerator_Reset_mC29E03FC9FEB81DB681E42AB3D39FAE35742C9E4 ();
// 0x00000A5C System.Object GlobalMainScreen_<LoadPageWWW>d__47::System.Collections.IEnumerator.get_Current()
extern void U3CLoadPageWWWU3Ed__47_System_Collections_IEnumerator_get_Current_m7ABDD6434A7727D7A0A0D21CA778BCE0FFFCB816 ();
// 0x00000A5D System.Void LevelPreviewPanel_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m81E6626B141825DC2F39630C1CFA037547EF8922 ();
// 0x00000A5E System.Void LevelPreviewPanel_<>c__DisplayClass2_0::<InitLevel>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CInitLevelU3Eb__0_m3AD9E893AAA56A59F69969F457B162F6B4629311 ();
// 0x00000A5F System.Void SouvenirPanel_<LoadImageRoutine>d__14::.ctor(System.Int32)
extern void U3CLoadImageRoutineU3Ed__14__ctor_mAB537BBAC365067BE5C3194EE536A9747C8FC651 ();
// 0x00000A60 System.Void SouvenirPanel_<LoadImageRoutine>d__14::System.IDisposable.Dispose()
extern void U3CLoadImageRoutineU3Ed__14_System_IDisposable_Dispose_m04C269C3F49C97A3575DEF5449CDC2D102E3EF1C ();
// 0x00000A61 System.Boolean SouvenirPanel_<LoadImageRoutine>d__14::MoveNext()
extern void U3CLoadImageRoutineU3Ed__14_MoveNext_m4156B2D0BF65A672C1096CD7483B6E83E2E2DB77 ();
// 0x00000A62 System.Object SouvenirPanel_<LoadImageRoutine>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadImageRoutineU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3890A30888F67654E456A4A6D7FDE2E0E809491 ();
// 0x00000A63 System.Void SouvenirPanel_<LoadImageRoutine>d__14::System.Collections.IEnumerator.Reset()
extern void U3CLoadImageRoutineU3Ed__14_System_Collections_IEnumerator_Reset_m538443F2A42F05CAAEA03E6562BCE8D5B85E6797 ();
// 0x00000A64 System.Object SouvenirPanel_<LoadImageRoutine>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CLoadImageRoutineU3Ed__14_System_Collections_IEnumerator_get_Current_mC4CAA1BBC1D8EEC4BFD0108D3AC1997165926EC4 ();
// 0x00000A65 System.Void SouvenirsScreen_<LoadPageWWW>d__30::.ctor(System.Int32)
extern void U3CLoadPageWWWU3Ed__30__ctor_m5540967811A0C74A5281E9D775A0572E7203ACC5 ();
// 0x00000A66 System.Void SouvenirsScreen_<LoadPageWWW>d__30::System.IDisposable.Dispose()
extern void U3CLoadPageWWWU3Ed__30_System_IDisposable_Dispose_m157C0EAD859880EAAB5E7FB22FD9870CB4F26080 ();
// 0x00000A67 System.Boolean SouvenirsScreen_<LoadPageWWW>d__30::MoveNext()
extern void U3CLoadPageWWWU3Ed__30_MoveNext_m0A01E0857CD0D44360C2110005469294EC539946 ();
// 0x00000A68 System.Object SouvenirsScreen_<LoadPageWWW>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadPageWWWU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7362B63039FB2A00F5B0BD4D713140F5B61A98F ();
// 0x00000A69 System.Void SouvenirsScreen_<LoadPageWWW>d__30::System.Collections.IEnumerator.Reset()
extern void U3CLoadPageWWWU3Ed__30_System_Collections_IEnumerator_Reset_m8AA4BEE8B0717748481C94144180E4AF30F1744E ();
// 0x00000A6A System.Object SouvenirsScreen_<LoadPageWWW>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CLoadPageWWWU3Ed__30_System_Collections_IEnumerator_get_Current_m9E613FC37019E060E967449481071B157BEDB14C ();
// 0x00000A6B System.Void SouvenirsScreen_<LoadTotalPagesWWW>d__31::.ctor(System.Int32)
extern void U3CLoadTotalPagesWWWU3Ed__31__ctor_m59EA9C888EC797ECF01EDC23372B3B748F1FF492 ();
// 0x00000A6C System.Void SouvenirsScreen_<LoadTotalPagesWWW>d__31::System.IDisposable.Dispose()
extern void U3CLoadTotalPagesWWWU3Ed__31_System_IDisposable_Dispose_m3577DA2DD158F31CD3AC9EE8A9C1C36A575C7FCE ();
// 0x00000A6D System.Boolean SouvenirsScreen_<LoadTotalPagesWWW>d__31::MoveNext()
extern void U3CLoadTotalPagesWWWU3Ed__31_MoveNext_m18ADE94EFE4B071281E9AB7046AC2F9C97EB9372 ();
// 0x00000A6E System.Object SouvenirsScreen_<LoadTotalPagesWWW>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadTotalPagesWWWU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E157703C0623B7BE0960AD088811E18F94F0E4B ();
// 0x00000A6F System.Void SouvenirsScreen_<LoadTotalPagesWWW>d__31::System.Collections.IEnumerator.Reset()
extern void U3CLoadTotalPagesWWWU3Ed__31_System_Collections_IEnumerator_Reset_mE9C96B61D18E6855FE5F174D2EEC33F297789B18 ();
// 0x00000A70 System.Object SouvenirsScreen_<LoadTotalPagesWWW>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CLoadTotalPagesWWWU3Ed__31_System_Collections_IEnumerator_get_Current_mA3E1BA4642C7C8FE821EF35023698C318E1955F1 ();
// 0x00000A71 System.Boolean SimpleJSON.JSONNode_Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_mA1C807DA248ACDC8D69570C215151BE536992A4D_AdjustorThunk ();
// 0x00000A72 System.Void SimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m8F4C8C2129D9E66DE6816818E2330B0C9D9197A9_AdjustorThunk ();
// 0x00000A73 System.Void SimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m9C36DB211444EA1718700F20E00050B5C04C7BA4_AdjustorThunk ();
// 0x00000A74 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode_Enumerator::get_Current()
extern void Enumerator_get_Current_m69AAA5B91F3B6161F15A6266A97158CEB7FBEB4F_AdjustorThunk ();
// 0x00000A75 System.Boolean SimpleJSON.JSONNode_Enumerator::MoveNext()
extern void Enumerator_MoveNext_mECE6AA8D79CC2BE34F339D542CAF81109ECBD734_AdjustorThunk ();
// 0x00000A76 System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_mE53FF2A7721BECA429D382FCE284B28E35FFE8BD_AdjustorThunk ();
// 0x00000A77 System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m2E66B11990EF05ACD8D47DD745BD8F09E296D586_AdjustorThunk ();
// 0x00000A78 System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(SimpleJSON.JSONNode_Enumerator)
extern void ValueEnumerator__ctor_mF172DE3A3EDB539FCB2DA64B73EF2DA57011A657_AdjustorThunk ();
// 0x00000A79 SimpleJSON.JSONNode SimpleJSON.JSONNode_ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_m151356ECDEFD55E1FFF524FE25A0F489878A10AB_AdjustorThunk ();
// 0x00000A7A System.Boolean SimpleJSON.JSONNode_ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_mA238C0869778636B32013EE14948492CDCC59F2C_AdjustorThunk ();
// 0x00000A7B SimpleJSON.JSONNode_ValueEnumerator SimpleJSON.JSONNode_ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_m3C2328208D593CFC614F7D42AC30DDB1047A9E1A_AdjustorThunk ();
// 0x00000A7C System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m4D48B705B3A7E4FF9EE283162B70332130540C50_AdjustorThunk ();
// 0x00000A7D System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mDF5D9D0D168C15E28B499F5C7A1837844568B2E0_AdjustorThunk ();
// 0x00000A7E System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(SimpleJSON.JSONNode_Enumerator)
extern void KeyEnumerator__ctor_m774690E38FB0F738D9427AE75203F3D199FC4513_AdjustorThunk ();
// 0x00000A7F System.String SimpleJSON.JSONNode_KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_m1778115C5302BE7AF526622909BDB6CB2660D4F1_AdjustorThunk ();
// 0x00000A80 System.Boolean SimpleJSON.JSONNode_KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_m06FB9DCBA7E68D21D18B09DA090C9B2646ADA57A_AdjustorThunk ();
// 0x00000A81 SimpleJSON.JSONNode_KeyEnumerator SimpleJSON.JSONNode_KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_mF33634E45FC6CCD3ACDE273AEB14CAAA35009A91_AdjustorThunk ();
// 0x00000A82 System.Void SimpleJSON.JSONNode_LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m239C73954D0CE2FA9B4C6321A33103DAEE146E5F ();
// 0x00000A83 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode_LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_mF0328C3C3F6CF43D5741818EDC8AFFE536BAA7FB ();
// 0x00000A84 System.Object SimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m5B8A3AABAB68E3A1DF96059FAA974A049F5B692F ();
// 0x00000A85 System.Boolean SimpleJSON.JSONNode_LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_mCA6FF832D795450D0D85F75259E3DAC95DF20FCD ();
// 0x00000A86 System.Void SimpleJSON.JSONNode_LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_m5A53643F1E33F79F145800DFEBA329647B7080C6 ();
// 0x00000A87 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode_LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_m23D6C6A7C8F5F358DB73B741E09507C5FEB80850 ();
// 0x00000A88 System.Void SimpleJSON.JSONNode_LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_m18AA07757D98DA78B7CD6DDA118EB0AA23B45ED9 ();
// 0x00000A89 System.Collections.IEnumerator SimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m24D839B8C290A001DE43D9D9B918DF35F1F7C694 ();
// 0x00000A8A System.Void SimpleJSON.JSONNode_<get_Children>d__41::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__41__ctor_m8E8E8F5BDB3F0FE0B146C3F7F3783958FD4F0A63 ();
// 0x00000A8B System.Void SimpleJSON.JSONNode_<get_Children>d__41::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__41_System_IDisposable_Dispose_mA700C935BC870C909B7BAAA7329927D0167399A3 ();
// 0x00000A8C System.Boolean SimpleJSON.JSONNode_<get_Children>d__41::MoveNext()
extern void U3Cget_ChildrenU3Ed__41_MoveNext_mF8D6574470598B7918BB1FEF83168B6ABF86BA2C ();
// 0x00000A8D SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_Children>d__41::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5A0B2D41DF2E406A9BC4DA0D80A352A9063A3DEB ();
// 0x00000A8E System.Void SimpleJSON.JSONNode_<get_Children>d__41::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mBF2656EDEB2D2A8ED821927C2A9ABE22E3E1B428 ();
// 0x00000A8F System.Object SimpleJSON.JSONNode_<get_Children>d__41::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m2C65131B2B2B11C072B4122ED6A0DA74504DA629 ();
// 0x00000A90 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_Children>d__41::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m9C9EF20C587371412C4089A8F5A772C527FD419F ();
// 0x00000A91 System.Collections.IEnumerator SimpleJSON.JSONNode_<get_Children>d__41::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m0A48E1C7F0095F0517E7FAF8FA8B785AF66376CB ();
// 0x00000A92 System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__43::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__43__ctor_m7FFFD7C525229E37D79ECDBDA820BFC3F501FFA4 ();
// 0x00000A93 System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__43::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__43_System_IDisposable_Dispose_mE872945D248E72AFDADCDA09699AFB2639AA1425 ();
// 0x00000A94 System.Boolean SimpleJSON.JSONNode_<get_DeepChildren>d__43::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__43_MoveNext_mEE97082D54099A9AA86AE7303E82E19FE4DA1134 ();
// 0x00000A95 System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__43::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__43_U3CU3Em__Finally1_m7F0C4DC120A57E0B62ABCF30B18A853255324B97 ();
// 0x00000A96 System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__43::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__43_U3CU3Em__Finally2_m8EB01DC002CF736E57053B4F7306EDCD16E06793 ();
// 0x00000A97 SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_DeepChildren>d__43::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__43_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mC7E7C1AAA6493F4277AD6F50D59C26EB524A8E49 ();
// 0x00000A98 System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__43::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__43_System_Collections_IEnumerator_Reset_m0013F10527DAEBB84D2E86AC688E4C413EACA6A4 ();
// 0x00000A99 System.Object SimpleJSON.JSONNode_<get_DeepChildren>d__43::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__43_System_Collections_IEnumerator_get_Current_m89AEB95BD1C1220D6A0B2CBCB0D1E2EA2A6A887E ();
// 0x00000A9A System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_DeepChildren>d__43::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__43_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mFE99AB7665C57E5D53691D6AFA47AAE5756838DB ();
// 0x00000A9B System.Collections.IEnumerator SimpleJSON.JSONNode_<get_DeepChildren>d__43::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__43_System_Collections_IEnumerable_GetEnumerator_mEA59685745103AEBADC05AA05FE04EEF38F05C93 ();
// 0x00000A9C System.Void SimpleJSON.JSONArray_<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_m395094D63E499CE83BFE7BEA57F2AAC327C0896D ();
// 0x00000A9D System.Void SimpleJSON.JSONArray_<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m428B887F7109FE19549A407335CA673F0877CFC4 ();
// 0x00000A9E System.Boolean SimpleJSON.JSONArray_<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_mD6CDE794B06D36428C6EA7A2CA67D8FCA3FB3079 ();
// 0x00000A9F System.Void SimpleJSON.JSONArray_<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m59DFB9AC110EC20ABC19034CD0308545E01DCAA1 ();
// 0x00000AA0 SimpleJSON.JSONNode SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5D1768596B7404BC7B060F244F5D88C3528E52BB ();
// 0x00000AA1 System.Void SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m1F10658E4ABDC538024A08C254CE4F9B019FB5A8 ();
// 0x00000AA2 System.Object SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_mAAAA2B88545650436E60B70B652267A1836DCAD2 ();
// 0x00000AA3 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mE9567310EFE3EB91CDA68AB43D87C4D01F640C37 ();
// 0x00000AA4 System.Collections.IEnumerator SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m1CC85EA09099DB444D2CEB569C0EE029B91FBC76 ();
// 0x00000AA5 System.Void SimpleJSON.JSONObject_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m613C3625A3BD4BA186F316B692F7C8D5EB4923BF ();
// 0x00000AA6 System.Boolean SimpleJSON.JSONObject_<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_mC0192D6A330495FE80E67D90A913CA143B7F8EEF ();
// 0x00000AA7 System.Void SimpleJSON.JSONObject_<get_Children>d__25::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__25__ctor_m3EE9D3A4D0EDB7EDD21050DB036528D1421B9D7C ();
// 0x00000AA8 System.Void SimpleJSON.JSONObject_<get_Children>d__25::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__25_System_IDisposable_Dispose_m72D96F3E7EF6E3A1C9FF6A2136867490C446C42B ();
// 0x00000AA9 System.Boolean SimpleJSON.JSONObject_<get_Children>d__25::MoveNext()
extern void U3Cget_ChildrenU3Ed__25_MoveNext_m50B969A9AE504FC0F6F24D6D9CD2A91F5BE90291 ();
// 0x00000AAA System.Void SimpleJSON.JSONObject_<get_Children>d__25::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__25_U3CU3Em__Finally1_m502C05547A22BDF91298F2B541AEAFE26007128D ();
// 0x00000AAB SimpleJSON.JSONNode SimpleJSON.JSONObject_<get_Children>d__25::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__25_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m181DF40535B1A5A4B5EF98CE7E3E0F2D5ACE6084 ();
// 0x00000AAC System.Void SimpleJSON.JSONObject_<get_Children>d__25::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__25_System_Collections_IEnumerator_Reset_m92C6A429195F76E2702A3325ABD90EAE1DC8BFD4 ();
// 0x00000AAD System.Object SimpleJSON.JSONObject_<get_Children>d__25::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__25_System_Collections_IEnumerator_get_Current_m0400579D20FC6B1A893529EB2A11620E71F84F64 ();
// 0x00000AAE System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject_<get_Children>d__25::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__25_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m8F552CBEF8520EB8CF857F5A16A8027493D5FCC2 ();
// 0x00000AAF System.Collections.IEnumerator SimpleJSON.JSONObject_<get_Children>d__25::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__25_System_Collections_IEnumerable_GetEnumerator_mF6077287D096F45C708B0F7BA77749CEA1957270 ();
// 0x00000AB0 System.Void DefaultUtility.UIRotateObject_<RotateLoop>d__30::.ctor(System.Int32)
extern void U3CRotateLoopU3Ed__30__ctor_mFDEAD87EDE102EB31FF327AE73F958E457FC9FD7 ();
// 0x00000AB1 System.Void DefaultUtility.UIRotateObject_<RotateLoop>d__30::System.IDisposable.Dispose()
extern void U3CRotateLoopU3Ed__30_System_IDisposable_Dispose_m7B1E6C464438B8417A40174354B3C7601DAB847B ();
// 0x00000AB2 System.Boolean DefaultUtility.UIRotateObject_<RotateLoop>d__30::MoveNext()
extern void U3CRotateLoopU3Ed__30_MoveNext_m646DB27D1645A6FA001215B0B1976CFD6979493C ();
// 0x00000AB3 System.Object DefaultUtility.UIRotateObject_<RotateLoop>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRotateLoopU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1732E622798AE9D4E8703BB6FE814496054BF111 ();
// 0x00000AB4 System.Void DefaultUtility.UIRotateObject_<RotateLoop>d__30::System.Collections.IEnumerator.Reset()
extern void U3CRotateLoopU3Ed__30_System_Collections_IEnumerator_Reset_mE73D86C79C82BB4FF602E6AFF4298ECA2D3C76A1 ();
// 0x00000AB5 System.Object DefaultUtility.UIRotateObject_<RotateLoop>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CRotateLoopU3Ed__30_System_Collections_IEnumerator_get_Current_m444FA2997D49051D98499AE6B32523E26B215676 ();
// 0x00000AB6 System.Void DefaultUtility.UIScalableObject_<ScaleLoop>d__21::.ctor(System.Int32)
extern void U3CScaleLoopU3Ed__21__ctor_mC4C7AE33BF54E66E5F4BF671C134B2B6D7F9F283 ();
// 0x00000AB7 System.Void DefaultUtility.UIScalableObject_<ScaleLoop>d__21::System.IDisposable.Dispose()
extern void U3CScaleLoopU3Ed__21_System_IDisposable_Dispose_mDDE44E4854A9F1B178C346324681AD308C09B5F4 ();
// 0x00000AB8 System.Boolean DefaultUtility.UIScalableObject_<ScaleLoop>d__21::MoveNext()
extern void U3CScaleLoopU3Ed__21_MoveNext_mE68D5B0774F325725C4198101350521915BBC6A3 ();
// 0x00000AB9 System.Object DefaultUtility.UIScalableObject_<ScaleLoop>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScaleLoopU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9259BBDE0A6A86A02DA156A67BFC711FB28ADCC2 ();
// 0x00000ABA System.Void DefaultUtility.UIScalableObject_<ScaleLoop>d__21::System.Collections.IEnumerator.Reset()
extern void U3CScaleLoopU3Ed__21_System_Collections_IEnumerator_Reset_m5AD905B2CD21CFFEBD9AEF7EFC6CCE732A0D740B ();
// 0x00000ABB System.Object DefaultUtility.UIScalableObject_<ScaleLoop>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CScaleLoopU3Ed__21_System_Collections_IEnumerator_get_Current_mDE7D535E617F6CC6DBC2DF9196B0D8EDC29EAEE1 ();
// 0x00000ABC System.Void DefaultUtility.UIMoveObjectToPointController_<>c::.cctor()
extern void U3CU3Ec__cctor_m5EC70CADD01FA899DEC2D921DA3D02F9F009E919 ();
// 0x00000ABD System.Void DefaultUtility.UIMoveObjectToPointController_<>c::.ctor()
extern void U3CU3Ec__ctor_m0DDDA10FC5F5534EEDEDC48344C5560DDF399BD4 ();
// 0x00000ABE System.Void DefaultUtility.UIMoveObjectToPointController_<>c::<InitStartEvents>b__15_0()
extern void U3CU3Ec_U3CInitStartEventsU3Eb__15_0_m22E3C03E533E74FC381E1D1C2559C6A97252BAD5 ();
// 0x00000ABF System.Void DefaultUtility.UIMoveObjectToPointController_<>c::<InitStopEvents>b__16_1()
extern void U3CU3Ec_U3CInitStopEventsU3Eb__16_1_mBE93AB88D92939DA4554ACC535EA395C177894B2 ();
// 0x00000AC0 System.Void DefaultUtility.UIMoveObjectToPointController_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m73FE5A20D51017316BF96A9B009F19FF6F466074 ();
// 0x00000AC1 System.Void DefaultUtility.UIMoveObjectToPointController_<>c__DisplayClass16_0::<InitStopEvents>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CInitStopEventsU3Eb__0_m689EF9916E6CF5E196B2381265C9ABB3A002E499 ();
// 0x00000AC2 System.Void DefaultUtility.UIMoveObjectToPointController_<MoveCoinToScore>d__19::.ctor(System.Int32)
extern void U3CMoveCoinToScoreU3Ed__19__ctor_m1897FFC7E36D5E8237408CB5CF2055E1792B53FB ();
// 0x00000AC3 System.Void DefaultUtility.UIMoveObjectToPointController_<MoveCoinToScore>d__19::System.IDisposable.Dispose()
extern void U3CMoveCoinToScoreU3Ed__19_System_IDisposable_Dispose_mC587A2A3A9498D7753589D19910AF163E5DBC44A ();
// 0x00000AC4 System.Boolean DefaultUtility.UIMoveObjectToPointController_<MoveCoinToScore>d__19::MoveNext()
extern void U3CMoveCoinToScoreU3Ed__19_MoveNext_m425A4C244FD323E9BECB40FBD8493C373D1420EC ();
// 0x00000AC5 System.Object DefaultUtility.UIMoveObjectToPointController_<MoveCoinToScore>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveCoinToScoreU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E66CFB9171902F256DAC022DB84F36B5E35DDB2 ();
// 0x00000AC6 System.Void DefaultUtility.UIMoveObjectToPointController_<MoveCoinToScore>d__19::System.Collections.IEnumerator.Reset()
extern void U3CMoveCoinToScoreU3Ed__19_System_Collections_IEnumerator_Reset_m1F016766162142F1FEAC200031F975E4A45EE82C ();
// 0x00000AC7 System.Object DefaultUtility.UIMoveObjectToPointController_<MoveCoinToScore>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CMoveCoinToScoreU3Ed__19_System_Collections_IEnumerator_get_Current_mAEDB850E33A92251C40335DB0A0C712A2F8ED949 ();
// 0x00000AC8 System.Void DefaultUtility.CoroutineUtility_<WaitAndStartCoroutine>d__1::.ctor(System.Int32)
extern void U3CWaitAndStartCoroutineU3Ed__1__ctor_m24A3B1D20DE39AFFA1A7F0358D0D1EE82A8CDE72 ();
// 0x00000AC9 System.Void DefaultUtility.CoroutineUtility_<WaitAndStartCoroutine>d__1::System.IDisposable.Dispose()
extern void U3CWaitAndStartCoroutineU3Ed__1_System_IDisposable_Dispose_m879B92762BD598AF850B4B4326BA5BE2C280E52A ();
// 0x00000ACA System.Boolean DefaultUtility.CoroutineUtility_<WaitAndStartCoroutine>d__1::MoveNext()
extern void U3CWaitAndStartCoroutineU3Ed__1_MoveNext_mF5A0DD2BEC0B199C9B9095ABFD7CBCA03812EBD4 ();
// 0x00000ACB System.Object DefaultUtility.CoroutineUtility_<WaitAndStartCoroutine>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitAndStartCoroutineU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m37131E6832CAE28F32E12CBDB34A981EEAB127F0 ();
// 0x00000ACC System.Void DefaultUtility.CoroutineUtility_<WaitAndStartCoroutine>d__1::System.Collections.IEnumerator.Reset()
extern void U3CWaitAndStartCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_mDAC5D9148AE0870BC9D368DFE856B56E7632070B ();
// 0x00000ACD System.Object DefaultUtility.CoroutineUtility_<WaitAndStartCoroutine>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CWaitAndStartCoroutineU3Ed__1_System_Collections_IEnumerator_get_Current_m3032B08B2E771E99DC72DB850F80996AD71E4967 ();
// 0x00000ACE System.Void CustomService.AdsController_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m74E6793F0381673A6BC729D92B778771BF010556 ();
// 0x00000ACF System.Void CustomService.AdsController_<>c__DisplayClass25_0::<ShowInterstitial>b__0(System.String)
extern void U3CU3Ec__DisplayClass25_0_U3CShowInterstitialU3Eb__0_m002FB2EA1F750B61A5ADB51DDF023EFCBA75B93D ();
// 0x00000AD0 System.Void CustomService.AdsManager_<>c::.cctor()
extern void U3CU3Ec__cctor_m939CF65CC4A1E0A1424351B4454FA6F3463ED7C3 ();
// 0x00000AD1 System.Void CustomService.AdsManager_<>c::.ctor()
extern void U3CU3Ec__ctor_mD052D62EBD026E6DFC6895F1B952E54F7075E8E1 ();
// 0x00000AD2 System.Void CustomService.AdsManager_<>c::<.ctor>b__39_0(System.Boolean)
extern void U3CU3Ec_U3C_ctorU3Eb__39_0_mCE9CC879498A9E16FFD45FC98D75306133245684 ();
// 0x00000AD3 System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass57_0::.ctor()
extern void U3CU3Ec__DisplayClass57_0__ctor_m877836C7A514073462D0C55F8B82C7F673A7E08B ();
// 0x00000AD4 System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass57_0::<onInterstitialDismissed>b__0()
extern void U3CU3Ec__DisplayClass57_0_U3ConInterstitialDismissedU3Eb__0_m338F2A5B7DBA400308EF101B5C6DA7915E407921 ();
// 0x00000AD5 System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass58_0::.ctor()
extern void U3CU3Ec__DisplayClass58_0__ctor_mC2AF890F7407F12AB0E67B850986CE8031A152D1 ();
// 0x00000AD6 System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass58_0::<interstitialDidExpire>b__0()
extern void U3CU3Ec__DisplayClass58_0_U3CinterstitialDidExpireU3Eb__0_mADA427D2C22537C788AA0BE9BCD5CF7E3FF67CB6 ();
// 0x00000AD7 System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_m1C74A18A169F12F22ED45E86349B3F2093EA60DC ();
// 0x00000AD8 System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass63_0::<onRewardedVideoFailed>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3ConRewardedVideoFailedU3Eb__0_m5095E87C8BE53B44D256CC60FBC6F6A5A417915A ();
// 0x00000AD9 System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass64_0::.ctor()
extern void U3CU3Ec__DisplayClass64_0__ctor_mBA42FA5D1BAD493A4CB3BB6F617EE67EEE2DCBEB ();
// 0x00000ADA System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass64_0::<onRewardedVideoExpired>b__0()
extern void U3CU3Ec__DisplayClass64_0_U3ConRewardedVideoExpiredU3Eb__0_m5B8434485DA075F73EB1B177AF41975F43BF901E ();
// 0x00000ADB System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass65_0::.ctor()
extern void U3CU3Ec__DisplayClass65_0__ctor_mA92A3313B3654E2A17A5A530F4021D4D6A6FCDB2 ();
// 0x00000ADC System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass65_0::<onRewardedVideoShown>b__0()
extern void U3CU3Ec__DisplayClass65_0_U3ConRewardedVideoShownU3Eb__0_mE325F23CE91E81A551B4848AF08D4141E32533AF ();
// 0x00000ADD System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m1D6D4675E013FF727787AC4B087466951B12E519 ();
// 0x00000ADE System.Void CustomService.MoPubAdsProvider_<>c__DisplayClass66_0::<onRewardedVideoFailedToPlay>b__0()
extern void U3CU3Ec__DisplayClass66_0_U3ConRewardedVideoFailedToPlayU3Eb__0_mF9B6055C65F772A633273F50A4C2A3B8F7E4EC5B ();
static Il2CppMethodPointer s_methodPointers[2782] = 
{
	AutoFlip_Start_m01AB668B7BE49B6B8D5E8AE67DEA6F2A16612223,
	AutoFlip_FlipRightPage_mA997777EC4EEBD7F40028396CD1BBE715CC11D30,
	AutoFlip_FlipLeftPage_m46E84CBA2EB43508D3AB06E5D98B7A26BD98BCE1,
	AutoFlip_StartFlipping_m31A7BF6B5820BFF2159090B8F35762AA9581A36C,
	AutoFlip_Update_mF1346CE4DD20B9B3FFF797BC733BE0F66DBD8ADC,
	AutoFlip__ctor_mF21E55DD008C4DF704AB05E018FD4E3901C47FC8,
	AutoFlip_U3CFlipRightPageU3Eb__11_0_m554CF55CEF4979B505B85F051A724E82C61E23BA,
	AutoFlip_U3CFlipLeftPageU3Eb__12_0_mCBDB6DDCBA4B83A5B8AC16E6447D3DC5C11A0EF6,
	AutoFlip_U3CUpdateU3Eb__14_0_mE1D065A8F4FEA087C9CCFF2064143274B96A2387,
	BookPro_get_CurrentPaper_m4BA16E66F8940CA2D55212061318D1F384FD27CB,
	BookPro_set_CurrentPaper_mE57503571C7DD82218AC2B867D77D40EED64ED44,
	BookPro_get_EndBottomLeft_m0C537E707C07B779A42BC8A57FB9BFE48A341C71,
	BookPro_get_EndBottomRight_mC4CE8871749C59A6DB60D7B63FA2F84EA80DFE91,
	BookPro_get_Height_m5F7AF0E7CC2BF72C20ED117E0F0EF680FECB6006,
	BookPro_Start_m0016FC2B02C8DDCD51E1614B0911095D42F83D05,
	BookPro_transformPoint_m2DA13110CA5832CBC626F6320510EFE729607565,
	BookPro_transformPointMousePosition_mF5CC6865A721DFB72F01BFB7D829E71AB5F4DFF0,
	BookPro_UpdatePages_m0B48A85F5F20D11B4E81CFF80DE77E9A4349DB3E,
	BookPro_OnMouseDragRightPage_mFA5E474B1B84DE2607B1418E0F541F2E3B718179,
	BookPro_DragRightPageToPoint_mAF63C0F9B712A1AEC041CD138D7A1C0C3507212F,
	BookPro_OnMouseDragLeftPage_mB75CA39C0A47C89E92B73BC11CB158D4FB09D27D,
	BookPro_DragLeftPageToPoint_mEA29B04AF2F9FBE2116CAFA8920B63FDA2A3A9A3,
	BookPro_OnMouseRelease_mA7DF0235E63ECEDB72D259D99006EB54A1734D05,
	BookPro_ReleasePage_mE9311368B4F7D3543E43403ED8C1D20A2F90ED73,
	BookPro_Update_mECC4660231134BEC1615B204F77FD9B8382A7F52,
	BookPro_UpdateBook_m652F9BC6088195DFFE85FC4C00D724D420357B41,
	BookPro_Flip_mCE8D5618B901DA709DE02736CCD28B5DD2388E95,
	BookPro_TweenForward_m336192BE2436C71FE6B5C2E68291C92762850D4A,
	BookPro_TweenUpdate_m2C448DC3998556D3DA983784B3F1F7B4756B5730,
	BookPro_TweenBack_m8F91AAD6C5D61490BD1BEAF6857D51BCC2E45610,
	BookPro_CalcCurlCriticalPoints_mC18BE87EC3DC60AC081FDC4458E883CB92B76DEE,
	BookPro_UpdateBookRTLToPoint_m4F160C1B58849500274987B942CEAD50B80AE0E7,
	BookPro_UpdateBookLTRToPoint_m1385692494C44BFE4BD9C4C08C191E85793459D1,
	BookPro_Calc_T0_T1_Angle_mBCC8F28ED8B2F154B364DF1602464E7A490B63D4,
	BookPro_normalizeT1X_m8ED16A7194521FB39B79CB43EBCE09FD170D6147,
	BookPro_Calc_C_Position_m2E9A873F01885FC57F9058F872D1DCF49564DADF,
	BookPro__ctor_mDB2E9ECFB22FFB243455DF64DD90A16EFF360237,
	BookPro_U3CTweenForwardU3Eb__44_0_m66C2692C442AB36FD15B652405E61FB0D8886684,
	BookPro_U3CTweenForwardU3Eb__44_1_m04B59166F3241893DE9D839730616CC8DC91FC14,
	BookPro_U3CTweenBackU3Eb__46_0_m6578FD266D90B73EFE45ACA3F46772BD5441BBE8,
	BookPro_U3CTweenBackU3Eb__46_1_m1DEEB6F9362D840A30BD2030143189C53F6E567B,
	Paper__ctor_m0506238F94550527B163FDE556824D5090B4D30B,
	BookUtility_ShowPage_m154A4C7E83373A1BACAC225EDC112240CF69E701,
	BookUtility_HidePage_m583D3D5EAAEE7FB227E39CF08DE930A54A7D1CB9,
	BookUtility_CopyTransform_m51AA42F868F62126F30C45D064305CAC70859D5E,
	PageFlipper_FlipPage_mA0ECB1485A0BC35AA73D7D4A7F01D59FB6F9B024,
	PageFlipper_Update_m93B38C5AAE76A901CA463B9AD02F1E3FC3D0CD50,
	PageFlipper__ctor_mAD3B51264788BB7125B8321B5ACBFA3FB7D2DABC,
	Tween_ValueTo_m7B9736EF9C900313F010745FDCD43B733B4AF832,
	Tween_QuadOut_m45498F438E4793BC40E82F9542C8B9D5A6A6BFB6,
	Tween_Update_mDD53C69E707081BEEF17715DBD9AC8C2101EA085,
	Tween__ctor_m965A158BB8A20C8C0DC8418B58B51BDA94780CCF,
	AdmobAdsProvider_add_BannerClick_mF91D4A4A39F7B63308919D45CBA733AA88A3C868,
	AdmobAdsProvider_remove_BannerClick_mDB26A5DC70FB98662DE29C33A17E377A5A1342E6,
	AdmobAdsProvider_add_InterstitialShow_m3F6247B376CFF267DC9DFA581A12139178BD949C,
	AdmobAdsProvider_remove_InterstitialShow_mE447677FE34930D0E37B58803264ADA3653B647F,
	AdmobAdsProvider_add_InterstitialShown_m608C1CA3CD4C502179F0557C898D54CD54BF1CD9,
	AdmobAdsProvider_remove_InterstitialShown_m756A822DAE9307391F28323F749869F08E507AF4,
	AdmobAdsProvider_add_InterstitialClick_mA093622799F59B31DEF6D56E2B0F465044B848C6,
	AdmobAdsProvider_remove_InterstitialClick_mDCB995AC76B3DCB25DD7F20ED502FB0FF908E6C5,
	AdmobAdsProvider_add_InterstitialClosed_mCD1A24CF8BB52ECE50372900802B0649E61820E4,
	AdmobAdsProvider_remove_InterstitialClosed_m4A97797A1BB3D5485578FDFF2F7B18A23ACDBC1A,
	AdmobAdsProvider_add_RewardedShown_m1B38C6481FF532D11CDAC2A9618D77AAF870D136,
	AdmobAdsProvider_remove_RewardedShown_m45D6426B596B943D5E09AB6C350B2E013E80CDFC,
	AdmobAdsProvider_add_RewardedComplete_m92F3FE4D1C58B1BAE4CE40BE6E410E09A384DCA5,
	AdmobAdsProvider_remove_RewardedComplete_m3F894115B3539CEDEFE5DA67EE175DE37F3708CA,
	AdmobAdsProvider_add_RewardedClick_mE5AE970353DF8526F7D930BB73FE2FFC87A41A81,
	AdmobAdsProvider_remove_RewardedClick_m5DB606A784B4D6A8F4F75EC13A3DD3847DDA4ACA,
	AdmobAdsProvider_add_RewardedClose_m342B970DEB40568BABCE9799ADC28852686261F8,
	AdmobAdsProvider_remove_RewardedClose_m94171A61060F961DE0F8B8D4D2B75AB654C5AA93,
	AdmobAdsProvider_get_adsPanels_mB2AB155E0223EC9F7701384DF5288E35D8621731,
	AdmobAdsProvider_InitModule_mEF5B8813680DDF40BA0E118C408BE947380D3405,
	AdmobAdsProvider_InitNativeAds_mDFE2B4021FA6116A19FE10928E67E59DEFDB016A,
	AdmobAdsProvider_LoadNativeAds_m566B8268E39BF03A6B24BE1ECD6F5A6980865801,
	AdmobAdsProvider_LoadNativeAds_mD4757D82F8DE1F17558C021D7C9791EB4C1C4282,
	AdmobAdsProvider_GetNativePanel_m6C3D5B3F44F1F1800B6CFC138A770AC8B7AFD4DC,
	AdmobAdsProvider_NativeAdsLoaded_m9CBE837178F4657DFE21C3FD17FD8190D324E808,
	AdmobAdsProvider_HandleCustomNativeAdLoaded_mC8120DB3765C8DD0A64771DC0CAA1823CEDC6A9C,
	AdmobAdsProvider_HandleNativeAdFailedToLoad_mD53EC132BEEACB0CBFD610C3B2F823EF9BB009FE,
	AdmobAdsProvider_HideBanner_m457D242BD7C7336877078D6335E731B41C916DC4,
	AdmobAdsProvider_ShowBanner_mAF4252E100D82011424352F953190D3ABE068837,
	AdmobAdsProvider_IsInterstitialReady_m9128BC235AE4DFE438D0686782BE0942CD20767A,
	AdmobAdsProvider_CacheInterstitial_mBF1466AFD50B7F03FC0F95837DF178B4A55BE85F,
	AdmobAdsProvider_CacheRewarded_mE597AD518AB7FD5D0D2F0648CF809B815E6230C2,
	AdmobAdsProvider_IsRewardedReady_m9E39B80B9A0CDE69017D2BA63224180291B039C1,
	AdmobAdsProvider_SetAdsEnable_mF9D1D5E270CEE48D0814874F54C248B927A4C04F,
	AdmobAdsProvider_ShowInterstitial_m33D20D84A9CC5115B57DC4E672C8FF844E55D271,
	AdmobAdsProvider_ShowRewardedVideo_m0AF4E869FB73FF148A195B4EEBB48F918DA91C4F,
	AdmobAdsProvider_SetAdsSoundsMuted_mDEAE62DE91ADE38B8D710C0FC625B2F513201AE6,
	AdmobAdsProvider_IsBannerHidden_mB9696A5FCE647B420E1AB0C9A41873303750FF07,
	AdmobAdsProvider_OnRewardedCompelte_m29023C8FE6753CABD67B90394ABF3A42819932C2,
	AdmobAdsProvider_OnInterstitialClosed_m48FD620F4408CAADCBF1FE513BF737816CC5BE49,
	AdmobAdsProvider__ctor_m641BC5FCDC26CE08AD29CDA3877EA2F058C04C9D,
	AdsPlacement_add_load_m4BA367D7343AE575485AE0B2737CC4C64286DA2A,
	AdsPlacement_remove_load_m47924DF76851F14B77DAD16D880B160571B04B9D,
	AdsPlacement_get_Key_m1EDB9139B2919E5A7A923B2038412C678C8417A1,
	AdsPlacement_get_IsLoaded_m9F0055E58EC09166662EE6312FCBF3D0175AD9E0,
	AdsPlacement_set_IsLoaded_m864448E3DB080CBEBA7E6C63FF3A8C78DA47BE1B,
	AdsPlacement__ctor_mB70E961744DBC5147B2375E920D54EF8ABACD44F,
	CrashTest_Crash2_mE8B990BF86B0700FD69D722C863AB9922F72EE79,
	CrashTest_CrashInfinityLoop_m41AE71F8B3DFC55E21FD88CC7D619859928EED2C,
	CrashTest__ctor_m4E053781DADF81F35932A544056B20D59BB13326,
	PromoObject__ctor_m6FC77B94401E3B8B4501D414ED5F3FECAD7C22EA,
	PromoPanel_Awake_m0F03E086F05376D2F0504037A492E73B9AD38438,
	PromoPanel_InitPromo_m0D10FBA8F8C6D686CCB7D2D12EE89229CEC6E98D,
	PromoPanel_Open_m915DCF411341A33E54BB1704BFB2CFA109C8E924,
	PromoPanel__ctor_mC3322BDC6A8E2494456142957F5F5FC0772CAD52,
	SmallCrosspromo_Awake_m6F32B5C2781242914457C6DEE32809A9DF2385E4,
	SmallCrosspromo_UpdatePromo_m2465F1E742BC05A73F21AEB2BC8C78E67130B9E9,
	SmallCrosspromo__ctor_m36E83FE4CB7ACC6F449D45DCDEE96F946B52A2F7,
	GDPRController_Awake_m1CA639148D2634F7BF572D06F64D1F3D80840789,
	GDPRController_LoadMainScene_m6D83D18EFCEF1A1DCC64E424FCEC1D3156D285E2,
	GDPRController_LoadScene_m2489F9B6C5F402E75514AC9745AF4BF306C2E5B3,
	GDPRController__ctor_mFCB466CCCFA17F83BD69E7C5E6621647AFC903C7,
	AbstractOpenObjectMethods_AvailableGetType_mE6B8D6750396BFACB17B9E70C2D7932ECEA4600A,
	AbstractOpenObjectMethods_VideoIsReady_m557003849904002C51341FFDD315AC6B46BE44F5,
	NULL,
	AbstractOpenObjectMethods__ctor_m5277ABE30ADA4C88E0A123AFA0440EF64F16BD34,
	GetObjectSettings__ctor_mC7C7EF30DA5B38210602DB29D18632B22F438EEC,
	Analytics_Start_m6CA75838A74C03B6B5F8286E02F87BB020E37DA1,
	Analytics_SendEvent_mBE2C61E768FE0AAB6F9A2ADA699F60283B23FEC9,
	Analytics_SendEvent_mF7E9CB226853EEC6FDF04197E0F0848CEFC1D43F,
	Analytics_LevelEvent_m457E4F71BC0CC2E03C98110EEBA0D907434EC8CE,
	Analytics_FacebookEvent_mB052A9900347839D1AC566BDC47AC70A95E978AA,
	Analytics_AppMetricEvent_m8A407BCA94D7F1AD351DABBF2BA31D0C632A178E,
	Analytics_DictionaryToJson_m27088D9589299470475D5EB4D8A6D5D72EE82084,
	Analytics__ctor_m64150A191B65F9CC3C3B7AE379B8495940178B91,
	AppMetricaInitialize_Start_m51F6172769E74C9A2529AB5F7942BD8CF7458AB8,
	AppMetricaInitialize__ctor_mB29A56BDD92C80629E56551D572F07E2B7972249,
	AppsFlyerController_Awake_m848995739119937BA031DC759E3DB1EB435004BA,
	AppsFlyerController_Purchase_m6835F0189DCDFF1C491FBC22F84E8AF35AF456BB,
	AppsFlyerController__ctor_mE9EAF2B4448B4C6BEE08251B50463607DECC053F,
	GameAnalyticsInitialize_Awake_m481E8A10A67233F674982E61E502A3EC08E45411,
	GameAnalyticsInitialize__ctor_mB0D29CF70357CC63CE02873AD37D90F91806A41B,
	AndroidNotificationProvider_Init_m9F64A8AB168228C02439AE411542F1116D13B468,
	AndroidNotificationProvider_CancelAll_m1BF32348326161DCE1CF0C3C7E81E24DC1DE1991,
	AndroidNotificationProvider_Schedule_m9535727B25B8F428C25B1F49737BF6BA99B004B6,
	AndroidNotificationProvider_ScheduleNotification_m88CFA28F78D97D65C8A3B08C7C5EA541AC037EED,
	AndroidNotificationProvider__ctor_mFF58DF87D230F63F3418473875E7AFEC93E97311,
	LocalNotification_get_bundleIdentifier_m22D7ECC8C83BD0BDC3E013BB7A81DA3EFDDE49EC,
	LocalNotification_SendNotification_mF5C2332E3363491AB791451D7300290CF1665AB7,
	LocalNotification_SendNotification_mDBCA5AD594ED1E5856040FC56FA6417301CBD13C,
	LocalNotification_SendNotification_mECBF898CA837B9BF973E975FF79BD889CF62A7A8,
	LocalNotification_SendRepeatingNotification_m5C5AB2FFEDC30825AA29E484669D127F1B60A228,
	LocalNotification_SendRepeatingNotification_mFDDE60CD888F8BC23A92848DB35E955BDB810785,
	LocalNotification_SendRepeatingNotification_m2EF52557CFCD0928E61A58A449EC4FC15FE4976F,
	LocalNotification_CancelNotification_m316C5F79AB8AA74F26800EB83D147CB703F3A1B1,
	LocalNotification_ClearNotifications_m1299558C57C44D2238494C886D9DD7DD60F85272,
	LocalNotification_CreateChannel_m93DBBF02AB3FFE4DDE244ECD790328437F165AA8,
	LocalNotification_ToInt_mEB9AE16A95389ECB2B097CC11FFD5E264C911718,
	LocalNotification__ctor_mCEEF8F5CB896AF2FD2AF59B781DF90EEE85222B4,
	NULL,
	NULL,
	NULL,
	NULL,
	DesktopNotificationProvider_CancelAll_m10E4A13B737A9A0D0443F16F317D8DCD22D0E69A,
	DesktopNotificationProvider_Init_mC44616D69A218E7EC4F832B2F6CCF061B14E7FD6,
	DesktopNotificationProvider_Schedule_mC6631AF6EBC6459D7B4D441BE849C4E45FAC8F8B,
	DesktopNotificationProvider__ctor_m533BA3436CC3F78F5F495EAE9A5AD2D67160DB82,
	IosNotificationProvider_Init_mD5BE945ECCE0922B5F1D200E058AD77572B34EBC,
	IosNotificationProvider_CancelAll_mF23C01C15292A788A66073BBE45B00953E886E19,
	IosNotificationProvider_Schedule_m4D0EAEAB8B0A2D964F309E5D08B742815DB6411A,
	IosNotificationProvider_ScheduleNotification_mB40C85E3CA84AF0A21F3A8BC3BB1D4CCAF7D7CAB,
	IosNotificationProvider__ctor_mEA641CCF0705147D5F7DC10AC2A376DEF48DA570,
	Notification_IsNeedToSchedule_m270A375DB85E44F4F42786E2D4A875FCD9B90E94,
	Notification__ctor_m9F0C402670F8031E2026708896013C8D0DDD00FC,
	Condition__ctor_mD9035D258C3585B729579CB52CBD5E6AD2907893,
	NotificationConditions_AlwaysTrue_m3471A830BD4FBAF7B3EA600959D3E16A876969A0,
	NotificationConditions_HasCoinsForSkin_mB59932BFCD045F5A9C4B2547EFD2E84F6B52B73A,
	NotificationConditions_CloseToHighScore_mBD4B565B2EB73481EFA420A78B0CECCDC692BFC6,
	NotificationConditions_CloseToCompleteMission_m1D9558CE3BF5769414A7A90E7F4CDD03FCD23C90,
	NotificationConditions__ctor_m52267CD99A77FEA3117C55D96EE0A9A65C5A2EF4,
	NotificationDatabse__ctor_mA88105913BC223CEFBECA6A2B16427C9FF27D2CC,
	NotificationDay_GetContent_m00E7AD57EBE70A6ACB6B1C52ACC04F62A942FC4A_AdjustorThunk,
	NotificationManager_get_Provider_m6E75DECFA0EF273B889E59D6F838E158AEF7D4CC,
	NotificationManager_Awake_mCB2197ACC711DF07F51B9695226D2DB4DCAB7612,
	NotificationManager_Start_m264342E06D8E4167BA1A15441EDB54B48931D25F,
	NotificationManager_InitProvider_m6F8344E38735441D7D85E1C06B9FB532CB90930B,
	NotificationManager_Init_m4351004C520F0BEA59157DE0C43C322FCA85F7B4,
	NotificationManager_UpdateAllNotification_m25F85A4E6FF661066C1637A73184DE76E7DB9A89,
	NotificationManager_RegitserNotification_m682EA29D60B38529B28FF1D018ECCF18442C0A0A,
	NotificationManager_IsAllowableTime_mA4A25D0F03DB9893EA6BB922A87F264D60A46390,
	NotificationManager__ctor_mA5D17B8FBA3E2E413E8CB767722581E3BC5C99D0,
	PrivacyButtons_Start_m87DDDF1B31CCCB6B72EA76BC9FF54E3638289A84,
	PrivacyButtons_Privacy_mC197CD7290951DD18F216739727553FAA5690CD2,
	PrivacyButtons_Terms_mBFD9FCCEDF5FB3D9EE95C156E09809D6F70E7B79,
	PrivacyButtons__ctor_mA49F1E8E28F5567C63CE200D231F42CD725E6FC0,
	Purchaser_Start_mCD122FAB158727577ED229C323774D10737D1835,
	Purchaser_BuyConsumable_m337D51C7A828D939DDFD67D25A47741770617A20,
	Purchaser_BuyNoAds_mF6A2D2079F487DB864106CDCFBAE226443D2220C,
	Purchaser_BuyProductID_mBCDAC5023D5830F87D3E0116AE9DC5447065385E,
	Purchaser_RestorePurchases_m24A1E9C7782A272240B01EB09EBC8828ED550846,
	Purchaser__ctor_m526162775A909925C95DBABA1943472F883DFDDA,
	BuyProduct_get_Key_m03CB5B8BF0ACE2E4A080DC9DA7819E962D85F6BC,
	BuyProduct__ctor_mD54F1EE2FFDF1C4A7CA829BD426A7754E648F7B3,
	RateController_Rate_mC373682F066ACBBAD430220E13196816D3A3D9FA,
	RateController_Rate_m8E81D21FE03B946D4EDF47BDC708B4B945BCAD51,
	RateController_get_IsRated_mEF0EB181CDB60E284FBE653D25E6375A20F62C9A,
	RateController_set_IsRated_mB42C0A4E0FAA30E1ED996C49186BAA2EE6DE3461,
	RateController__ctor_mFE520BA9C072290F3C91C73258425F81A69BD1CB,
	RateController__cctor_mE7BCA0EFF6348620B4D8454BB6748DF2DB1E976C,
	RateStarsPanel_get_IsRated_m48B1E188C500A9403468942BC418DBE97250F42C,
	RateStarsPanel_get_RateNumber_m1C5DAED06D3392BE824D57C4E247E01B6F523AB5,
	RateStarsPanel_Awake_mFC52278B39221E7C33464BABA2FF2B217A017B13,
	RateStarsPanel_InitButtons_m45C0ED6930F9A5C3A9446E3B659BD43C11739B37,
	RateStarsPanel_Reset_m57FEC7BE5502DB158A6906DF7F4552F901692E21,
	RateStarsPanel_Rate_mC53B303B9DD0E33B9EDFA4EE1AB34E6DF1EF8C20,
	RateStarsPanel_SelectStars_mC4E5D328EECC84C84A6EEACD719E6D8195781514,
	RateStarsPanel_Clear_m9A35C388B595349C3797FDBB4FDE464CB703E18F,
	RateStarsPanel__ctor_mDBB548B175EB25FFA800E7D080BE1D9E5E499E55,
	RateStarsPanel_U3CInitButtonsU3Eb__10_0_mB56F0367D361B3E1C7D14E333791984DE4D8D9EA,
	RateStarsPanel_U3CInitButtonsU3Eb__10_1_mEF6E780A8A3241829E1219D37611E1A514D94133,
	RateStarsPanel_U3CInitButtonsU3Eb__10_2_mBEBC145F3A09C899AFF4577928AE3A4E9BC744A8,
	RateStarsPanel_U3CInitButtonsU3Eb__10_3_m89921336493EDA4A359A17EFDAEA12E341DE818D,
	RateStarsPanel_U3CInitButtonsU3Eb__10_4_m388AF14D95CAE08260A9A229C7FA6FF2AE3C998B,
	ServiceData__ctor_m832551047FDB100BD15FE1E07E50FBEB4C05DC46,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Service__ctor_mD0EE68016BA53028B56AB0A1DA9FC274FA0228BF,
	ServiceController_get_ShareCount_mA310E86F97344B853D996A132A5A0EF2FA8E7463,
	ServiceController_set_ShareCount_m2EDE8709E934A3FECC75D8CCA2319908711561EA,
	ServiceController_get_RateState_m50CA2CD5D14F2BED7C1380C9870FEA31EB044430,
	ServiceController_set_RateState_m127187BBDCA7A6321E6FE9163D1A84BDE3261DF8,
	ServiceController_Awake_m984499D373A823726EE8FCED41686D3927BE8635,
	ServiceController_InitShareCounter_m7C096F1468A97E1D12F9FB0D6BD81A245029A60D,
	ServiceController_Start_m84BE68C2C00B774811BB9D98E0F43AEFF80CB3A0,
	ServiceController_Init_m9DA7E973ECE8416226608CAA83243F7CCBAEEB8F,
	ServiceController_Login_m4419FECA65454D0F20D0D0865B577BEAEC33218F,
	ServiceController_Logout_mA0AF0352E7635D2ED2136E8D9D7D529DB4F30D8E,
	ServiceController_ShowTop_m4E4E9CBD599905F1BFFD7757EB1EC5A1F91D0EAA,
	ServiceController_SetTop_m8D20AC64E981EDD675C83F95181559F57C5DCDE0,
	ServiceController_ShowAchivs_m54BAD7B2213CEE0EDD4F7C7E17C33F87A50E40F8,
	ServiceController_Rate_m70324EDA33209BD86E547B004C0149BAE06BA3FE,
	ServiceController_Share_mF1F934A034A08FB81672EFDD1C75996A56B5ED57,
	ServiceController_Share_m702012FC1693E73085E4A48B56A5EAB62021418F,
	ServiceController_Share_mB9539E869D856CB0C23333512452BB58EC080DD8,
	ServiceController__ctor_mE2F9FFC917B3ADDECED24F3BD434001518F47A39,
	ServiceController__cctor_mB115A2AD7245D6F3F3458DE3FEEC8CA8E22B0197,
	AndroidService_Login_mBFD962A647BBACC0256DFA85F6CDCAF376BA1D8C,
	AndroidService_Logout_m83A5088C9BC4A09731C08519C94160F747C61B49,
	AndroidService_ShowTop_mCBFE77F9DF3917CF96D0A75DA34D476E49C4C698,
	AndroidService_SetTop_m1BC9B4108E9C5134E5C162994D5D4C2BC383648C,
	AndroidService_ShowAchivs_m9FE93AF6424AC905B8B70843F95727C03C0981A5,
	AndroidService_GetLoadUrl_m282ECF635CE1888C144891C2E5A03036B41BD171,
	AndroidService_Rate_mD8D731AE4EA737E34347679DCCD6149F7051D731,
	AndroidService_Share_m1AFD2DF6E235C02A73E4E4B6DFC4696DDBA98559,
	AndroidService_Share_m93009612F11243F7D1138870508018B05D9909F3,
	AndroidService_Share_m60ED46C237D1C101AB5D7364A7693BE174D969A2,
	AndroidService_CreateText_m73D60C846057526D4F505C9A75D3A9679F81946C,
	AndroidService_AndroidShare_m1592ADE8187C6F7CF6B4E07A800A5FAED107203F,
	AndroidService__ctor_m8ED9F93D992C45DB02A4DCDF2BA47BA0A2483F56,
	AppleService_Login_m4D36739A0F4D949FE5D896330C6B06F0324AC710,
	AppleService_ShowTop_mDB0C709246F95277C270854F2762FFD274A2CD8C,
	AppleService_SetTop_m1BE231B6898FEBDB3A9E52B1EEEF587E8C6FB341,
	AppleService_Logout_mA17AA76487A66D4360B30152C19855085A909D64,
	AppleService_ShowAchivs_m9591BC68FD34122FBDA42B509D2ACFFF1F6B62C7,
	AppleService_GetLoadUrl_m9F7EFDBFF7C62F49C87FFE3E58B05A8E9DABD0DC,
	AppleService_Rate_m408DCCF74CA8BE9386B9FED45ECA4B269DE24DA0,
	AppleService_Share_m5C1D6B4FA2B5D7B5FBFC6FF76A677A4B4A0BBB64,
	AppleService_Share_mF457014AFDDE3C0C4C30B6832A3BAB6FECF1883F,
	AppleService_Share_m067110B6C8008AC4EFD3EE621D2F1A3CB778D1D1,
	AppleService_ShareMsg_m418F3A762B19706E139437807BBF5751E7FD247E,
	AppleService_ShareMsgAndTexture_mCB2270252E3A5C5B7E3CD55D3D8930AEF334F836,
	AppleService_CreateText_mF6F45823E9F30E78405EE60EC58F4E56F3153859,
	AppleService__ctor_m98802E4CED4A51DF16659B9A9DE0FA8A0589E48E,
	DesktopService_Login_mF4EFD0B66CF3E437107A6178DEC1A5032BF35AA6,
	DesktopService_Logout_m0F802F8D45EA638A9EDD97C3F2C5F42218DBDD3C,
	DesktopService_Rate_m8CE79BF4691C4EB5A110E3679AD6FC33A79B9458,
	DesktopService_SetTop_m9266AF4EA73DE801F9A5151281DE70E288E56C9E,
	DesktopService_Share_mBD13BBF9D3653D07AC7AC14E89C64D1DF369318D,
	DesktopService_Share_m25752E9EF62116F0F00B6A3B553E43241883F89D,
	DesktopService_Share_mBE755DCA647E419061BBFF219779A3D74EC92FCD,
	DesktopService_ShowAchivs_mFEDC9F40D372101E95221140164CBC745B4C6327,
	DesktopService_ShowTop_mD26D65C62131241F3BEDF5301F99DAF09F15C878,
	DesktopService__ctor_m769B0C9EDC9E609D5D4E56340B8D189DFD18CC72,
	MusicButtonController_Awake_mFCA7AFAC9631FFE73B0197E7D84E9B2627B733CA,
	MusicButtonController_Click_mA9B12F01201CB1B6404DEB76BC25D6CA7865C156,
	MusicButtonController_CheckImage_m70178D2FC4585003FFAFFB08B560661A56330193,
	MusicButtonController__ctor_m94B4CA0EE304545F78FD039267C2B091C5BF034C,
	MusicManager_get_Music_m3CFC65B4B577F7DCDCA7AB1A2533E0DF221C534D,
	MusicManager_set_Music_m5EFE605773E19B1DFE4D72D47AE8D8138464C128,
	MusicManager_Awake_mADDA160267BBC2EAF3DDE1F8E9EBA58031DC6693,
	MusicManager_Start_mF77913A780CD343FC5CC6FE204CF7AFCD8043272,
	MusicManager_Pause_mD4286D00D332087F3A8495CD04DB4A4AEF63D1FA,
	MusicManager_UnPause_m1A921FB699F8718AF6F88EE9E09921F37E5119CB,
	MusicManager_Mute_m3F2604CAD54BCF2BDDD5E845B33072F74006204C,
	MusicManager_UnMute_m63AB004E2D0D527BDCB2066466EC7019C3A5EFBE,
	MusicManager_SetMute_m46E95835E2FE107ECA8A384923D15DC7911C349D,
	MusicManager_Check_mF96F5AA5E5B7815BBB5607E55CE532479C224AAE,
	MusicManager_AdsStart_m17A38DEDED2AD39A474B2ECDFCDC04F91285DC4B,
	MusicManager_AdsFinish_m2F4933100A79B5EEF96DF88961238733893BB563,
	MusicManager__ctor_m0910B32A2F10526CE5FCB5831384F261A434549B,
	SoundButtonController_Awake_m6CC890FAED1D5A2035E0DF094D48DEBF8D0D5BFB,
	SoundButtonController_Click_m4B5AA0FC60F4157EAB2756E75F5F066C214833C8,
	SoundButtonController_ClickAll_mB614E51A48AD0EC720BD171A5FAFA40F86F34007,
	SoundButtonController_CheckImage_mFAE8B3560410E0AC212E73E5EC2890494146835C,
	SoundButtonController__ctor_m47C03C3AE594A7476BF7867CD2955B3524B4A002,
	SoundController_get_Audio_mC2072AA5CA65BD09C510B8F9EF6A4C73E1233447,
	SoundController_Awake_m7D4556CC74ED46B01483640F957997A2F5BC903D,
	SoundController_InitSound_m5DF6881D341789E871F292506AE79D2A322C0CB7,
	SoundController_Play_m446C595ED7953A99C2D603DE872CC30C8681CA2F,
	SoundController_Stop_mA3FD56D4C15C5164FA3D893D7288AFE4C5FE0699,
	SoundController_Play_m738AA37918DC26C17A0F8B3EDB2D25FF19C066B9,
	SoundController_Mute_m04646E7CB24FCEB2E4567B220F2BFC5B1EAC9865,
	SoundController_UnMute_m4F664DFE1DECAA03FB3DDAE718C68A836A237D1A,
	SoundController_Pause_m28CB244AF5FD9D4861FE4E995AB35EDC5CB45113,
	SoundController_UnPause_m36E21E8AD736783D9F921B22A81A4BC5CF94224B,
	SoundController__ctor_m6D8FCBBD8F2A1B7AC1D0D87211405ED5CEE72EDB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AbstractSoundController__ctor_m81237B6F992471992F2D96E24606F9E73C185DC8,
	SoundManager_get_Sounds_m377A4A05228E38A85C232420C004255E35295D9E,
	SoundManager_set_Sounds_mE69D99293F44DF21B8535E84F1AC718C3C1AF069,
	SoundManager_Awake_mFAD1AEBF8B8957E85CE445F65107AC6A5DDDFEEC,
	SoundManager_Pause_mC5E8C368906F319F528B804061EED2796F4377FB,
	SoundManager_UnPause_mF061B9F7B64299E1DD828FB00EE5B19DE77F4A0C,
	SoundManager_Mute_mA2C18D5940898B0A056534C5AE3EB234B53BD541,
	SoundManager_UnMute_m8C067EFF4B80DE9546B278FFA112BB90A67A46F0,
	SoundManager_PlaySound_mF0B7231A196F591C25BC19455496627A3FD52FB4,
	SoundManager_Play_mC6F1EE6E97A7983831C3709D42089F61EC0FAFF4,
	SoundManager_PlayAndSave_m11AE985ED651689317E007711FA5194498863AF0,
	SoundManager_PlayLast_mE65B6686557B5DD69BC3A7A773C14295A6CAB961,
	SoundManager_Check_m8E0330C0D003E513D29A7513C4403CAE60B57253,
	SoundManager_AdsStart_mA13866A4BC2FFF61DDA2D84BB50611CC86E4991A,
	SoundManager_AdsFinish_m0E7190D4826B3B4D02543B324DA75EBABA92916A,
	SoundManager__ctor_mFF8C696A5B666ABC1E2344581FE7FB06E038D422,
	TypeEditor__ctor_m2F33E56E5B2A2AA7942DEE43FA819CC9A367804B,
	UiMoveToCenter_get_mySize_m440F76CE0E856E0A93B1F87E299092E0B7921956,
	UiMoveToCenter_get_HorizontalBorders_mB03DA147E3AC9927C12110CACD610290138C801E,
	UiMoveToCenter_Awake_m8BE4620CB582C4967CFDF08D80D0EDEFE78745D6,
	UiMoveToCenter_Update_m268E1BD6D9A54DF34F0C4CC0F539DAFF408C2268,
	UiMoveToCenter_UpdatePosition_mF5B0CBA6CD0DE0167D30812AA4939A740AB44242,
	UiMoveToCenter_MoveToCenterOfScreen_mEEC12112D1A08430DDB3B38BDA3DDDBE85F0CD15,
	UiMoveToCenter_MoveToLeftOfParent_m6D8926211E7C18CDFCEF8F453D12A7DC1B83ACB5,
	UiMoveToCenter_MoveToRightOfParent_m510E6CF2BA46217CC2CE101807C5EDDC6AEF97F5,
	UiMoveToCenter_MoveToCenterOfParent_m04BDA9C3826E53D1427F726E7EF379D8872C265B,
	UiMoveToCenter_MoveTo_mF7EE4FC6AD65497E597B80858DD407B5A44418C4,
	UiMoveToCenter_MoveToPosition_mAED5EE04D1DAAF0ECAE9F1BEA67B50877343CFB2,
	UiMoveToCenter__ctor_m708928E4B860E340C69547298CB62A5AA4E4139D,
	UIUtils_GetCenterPosition_m5B9F0760FCB17E047C40AC587BA57624E8FE043D,
	UIUtils_get_ScreenCenter_m02176DAC8E9980DC1FD992A05EEEF54F515C9520,
	UIUtils__cctor_m1D3C3B9DF1D9FCAAD652B92B4D5140B50706B7BD,
	CoroutineExtention_WaitAndStart_mDDA6FB19C39CD2E7D9D349AFB499EB0CFA3DAF18,
	CoroutineExtention_WaitEndOfFrameAndStart_m00C7D00B93AFA36C0DC111FEE68E864E160E0CF0,
	CoroutineExtention_StartLerp_m3A727FA7E154581A38903D6A7668FF3DB369B1DA,
	CoroutineExtention_WaitAndPlay_m3DBB69F972409FB0881EFB0C167C1BAF6EFF700C,
	CoroutineExtention_WaitAndOfFramePlay_m82A9CFC80E3EF6A3A4828A1E2D90BA57207D43FB,
	CoroutineExtention_Lerp_mD0BE507B0538EAB128A0F52582B707ACBD4D4FFE,
	SessionsCounter_get_Sessions_mD1946F0ABE19453AE81D1BEC6D4D11F29DF04529,
	SessionsCounter_set_Sessions_m0F62F9781C171429775C43E226D5EB98EFC4DF44,
	SessionsCounter_Awake_mE4B58141A35F713412CB3A5F0D85DD0688747CF6,
	SessionsCounter_Start_mFD924F5BB6CCAF13EDC35B12DE4673B3E269BFA0,
	SessionsCounter_SendDataToAnalytic_m2BF02AD8069496E83579868D964C6698EB1FD026,
	SessionsCounter__ctor_mE12217A8A0CBDBC39495E5C693311863EAF09D53,
	DevLogs_Log_m43F965BDAA60B013A15DA6168AD04B58D65E855E,
	DevLogs_Log_mD14BD431CB72EA489C496029839BB59B0E644971,
	DevLogs_LogError_mC57A1EEEFC741E751327A7EF2F4B0DB6F4226BB7,
	DevLogs_LogEvent_m774F075AF5F9D5A3D4AD9EBF6393618D5C56C36B,
	DevLogs_LogEvent_mCA3FFECED59A147FD0482C67FD4872C9E1062B36,
	DevLogs_LogData_mB44B6D038B30592AD981B810E4231E6B6320456A,
	DevLogs_LogData_mE513523D2ECF2D9E5088AAE3FC5E0F766DACEC86,
	DontDestroyComponent_Awake_mAE5A6523863A8525E6D246239B3C0D53FFDCDF9C,
	DontDestroyComponent__ctor_mA976254583C9655FB2F8319584B561A8D9DF5F77,
	GameObjectsUtility_UpOnHierarchy_mD336C0B8F76440EB4AA6B1FCCD31CD4BB14CB373,
	GameObjectsUtility_DownOnHierarchy_mF41E49D2BF11FCED993286FB8D7DDCBC97BCC1CC,
	GameObjectsUtility_UnderTargetOnHierarchy_m29689323B6BE4005A625C7EF19F8776CB6154576,
	GuidObject_get_guid_m786BE73B53F1005C7E7B2B55417C0E53F5F21A1B,
	GuidObject_Generate_m7406BB4B5FDFB7EEA45C398D69415CC992CB625B,
	GuidObject_OnEnable_mA13A63365E58C19A8655823F871E59FF418E5B07,
	GuidObject_OnValidate_mC21295F229124552E836C2559E99A4FA53CDB0A8,
	GuidObject__ctor_m3705EB457F4A8C8F944F6E63A5CD8A6C6F4710D2,
	ConditionalFieldAttribute__ctor_mBC1F544EBF98448EE92A6CB8446C3FA75E98A7C4,
	ConditionalFieldAttribute__ctor_m51BD30BC1B1E2E5FFF2B00C7E544EBF38BB2CC0C,
	Field__ctor_mBE57624831A929B7965804B943EFFAD041EE4C00,
	ConditionalHideAttribute__ctor_mC4D45E6B80839B169402F47880FEEE5BC552C9F5,
	ConditionalHideAttribute__ctor_m4EE8D31175B6C1192F47E3204DADF4BDF2C86C69,
	ConditionalHideAttribute__ctor_m4D7F45A915DCC213567EAAD6F37F94C98B1FD91A,
	ConditionalHideAttribute__ctor_m646B25A1D570E19127AFDAD45650AEC3B3AA8D91,
	InputController_add_swipeX_mF0B8F9C68FE4296C35DEA528C6B98EDCE6D3304A,
	InputController_remove_swipeX_m27E8CB1ECBBE131F37DCB2D8EFBE9F02ED8AB071,
	InputController_add_swipeY_m04A6A3B62E171D641620EC3DDA2A98B66130069E,
	InputController_remove_swipeY_m7EDEBF2290CD6CFE579382D0A668D4A0C0BB973D,
	InputController_add_touch_mDB5470CE9BEA65E9F555339709685CF5710C8665,
	InputController_remove_touch_m87AD8498377A84890C13C0FF76D55188C6E5C314,
	InputController_add_touchDown_mB9EC81A8B1543D6DCC3129F7021FF18A269755AA,
	InputController_remove_touchDown_mF5AF2AAB0C9CC8FD2695361E2135BEBEDD77C4C5,
	InputController_add_click_mDC5276BD655855E2B43E1B90BEF58EE24C4E143A,
	InputController_remove_click_mD8FADE9873A15028B1629B59CD7CC5601B57DC66,
	InputController_add_touchUp_m33EFE563583F5D92F01A7237A8D93121B4CF092D,
	InputController_remove_touchUp_mF0B84CE802A9AA7ADF9389891B0BED0A83244140,
	InputController_SubscribeOnSwipeX_m40BA3C566BC0FC55A70B014E6D65654C7804458F,
	InputController_UnsubscribeOnSwipeX_m17FCF57DC2EC6042A50CE1C2E60B80A5B2269DDB,
	InputController_SubscribeOnSwipeY_m49A9F991002F1C83DB2F4F1ED122894BA364253A,
	InputController_UnsubscribeOnSwipeY_mC6B8BA90CBB8B9DBE1D52E516E4C519E0045E09E,
	InputController_SubscribeOnTouch_mEF5E4DF74E647268957394C02513AEA7C0DE50C7,
	InputController_UnsubscribeOnTouch_m6D4817B299B52171CFF0BD4D9D4C3885DE5344B4,
	InputController_SubscribeOnTouchDown_mDB80DBCD65735389FBBE0759FCE1BFDF04407E54,
	InputController_UnsubscribeOnTouchDown_m64941D513809904B5CEF2A816991E493FC1B905E,
	InputController_SubscribeOnTouchUp_m6DD555F9EC0E86FF62591DA75E2E8AE952C30340,
	InputController_UnsubscribeOnTouchUp_m7CF09321167120D83525134B0367FEC34E01E627,
	InputController_Start_mF29B2ACC31D1AE10D448EB64E1F1DD7843D85583,
	InputController_Update_mC6D90E8BA29328DC1D52D36DBAF7282E7817B69E,
	InputController_FixedUpdate_mD586509262AA97C61AD36A2C50895D3CA59F6DE1,
	InputController_HasUiOnTouch_mD4EA7E9576A3D4E14BE2AEF4EA9ED0C486CD8A6C,
	InputController__ctor_m7DD1C972F88216307D215651602075F4A4000355,
	NativeShare__ctor_m4F97EF40BE83F4E69A330C5B2B8CC064AD26E4D7,
	NativeShare_SetSubject_m4B9B7E447C2FA1B53821BB6D9A1A08DDF8116F72,
	NativeShare_SetText_m73B4F78EAB07B4ECAC0DDDC041FC917B7311ED7A,
	NativeShare_SetTitle_m331DC8B2DA3E8E0DF9ABD0EB421EDE13193C8480,
	NativeShare_SetTarget_mDAFC39AF5273D396ADDCB5DA2B1BBB9064284BA6,
	NativeShare_AddFile_m4E424757402F103C17EB778C15E4BFA82B949CB4,
	NativeShare_Share_mA50E72CF1EC13DCF69341E47A40C4635120251CF,
	NativeShare_TargetExists_mE4D5C90044C666BC46B7E571DE483C8EB8942811,
	PlayerPrefsUtility_IsEncryptedKey_m23EAF5D4E911E063154F35C2C31F2DFE63B0FEDD,
	PlayerPrefsUtility_DecryptKey_mBD8EF37CF3681750B8375F3B5F73C313938961AC,
	PlayerPrefsUtility_SetEncryptedFloat_mE9553831B3CF6E1CB18C1EB8871BEE0156C2C60E,
	PlayerPrefsUtility_SetEncryptedInt_mE764BB1EBC28C026B9781DF58FBAACD09424AD08,
	PlayerPrefsUtility_SetEncryptedString_m22FE88DF758B29F9225518219BE4A9EF2460ACB4,
	PlayerPrefsUtility_GetEncryptedValue_m21A1E2983297B03300AD724EB9432C8CF22E7E26,
	PlayerPrefsUtility_GetEncryptedFloat_m27F51A1DD8463DD3EAB7BD7C902811CF529DB9D7,
	PlayerPrefsUtility_GetEncryptedInt_mBD711FBAAE2DA81757E577405309C919188FF4F2,
	PlayerPrefsUtility_GetEncryptedString_m65C7FBDB5F58C3C5D84A5AFC1D7F03697B0A9BCB,
	PlayerPrefsUtility_SetBool_m96615FCD6D0182E9CC3AB74D26011B2604ABE738,
	PlayerPrefsUtility_GetBool_mBF2A9A26A1324970B35E277C461FBE2779A43FAB,
	PlayerPrefsUtility_SetEnum_m3B7BB83069907F088F73FF01C4EAE199EC555E59,
	NULL,
	PlayerPrefsUtility_GetEnum_m460014287E03B31DAEF84723875EB30DDC5E91FE,
	PlayerPrefsUtility_SetDateTime_mC9B22968B6E337BCCB5524F34D825BCA4E7E3E24,
	PlayerPrefsUtility_GetDateTime_m8333C41CDA4D8C13730846DEB612A900FA6D14EA,
	PlayerPrefsUtility_SetTimeSpan_m8CE72AE327105109D7F21B03F529BBC8603D3F1B,
	PlayerPrefsUtility_GetTimeSpan_mA74507D75E3A2680446B0EED6FCCD47E16D7A0BE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ImagePool__ctor_m0EEAF2330CF435557065DC2581DC654FAEDC43BE,
	MonoBehaviourPool__ctor_mD0EE8199D2A001CD1CFF3F8192D0FF063C5C179B,
	ParticlesPool_Spawn_m850772CC32D0FCCB4ABDB69033DE556A636329D2,
	ParticlesPool_Despawn_m9143C790A7DF258165CC0C5CB642D6195E3CD983,
	ParticlesPool__ctor_m5DFCDCCCB4828CC37878887E0AED28A821A30D44,
	TextPool__ctor_mC2D89A6043E0650CCC6911C79C46D059F37CA3E7,
	RandomUtility_GetRandom_mCF1A329EEAF4623156FE59D5AB03A2604478B617,
	RandomUtility_GetRandom_m05A1147D22E7FFB68FC58939F600A71BA4DD4A36,
	ScreenOrientation_get_IsVertical_mC8EAA6475439CB955476868606688F2F00F76012,
	ScreenOrientation_GetMainGameViewSize_mA5B38393754887DC824399E681FA9B41A623BB74,
	CameraScreenshooter__ctor_m92DA049F11F3345EC6E0A93C9502781753C3DCC4,
	TargetConstraintAttribute__ctor_m46B1A1BC2C6C2966E070AF063E50132B7AF10532,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InvokableEvent_Invoke_m20BB6875C0AE752AFAFAE44C2F626F8E66A2E862,
	InvokableEvent_Invoke_m8673CE9D075B952E7389D9FAA829BC815B9BD44F,
	InvokableEvent__ctor_m3BC3009ED31135D4AF34A436158469393A70D650,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InvokableEventBase__ctor_m3D3870E953D0DCDA9A06BB6DE5FAD288D8C79428,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializableCallbackBase_get_target_mD7C4940393FA0D66A7DAA96AADCDE9312B4A8F19,
	SerializableCallbackBase_set_target_m3F4BE939AFA6440BE133C54F60E98D1C0B5A3324,
	SerializableCallbackBase_get_methodName_mDBF935E0552D4242A8116EF1B1DA7B6683B517E4,
	SerializableCallbackBase_set_methodName_m55478D20291E555F9CD27CF5FDF3A3257E61FDC2,
	SerializableCallbackBase_get_Args_mE2540440CD6B8C2664EA692F14B214B28236778B,
	SerializableCallbackBase_get_ArgTypes_m43D4E0E895546DCFDF7CA01632EFE40BC5C76CBD,
	SerializableCallbackBase_get_dynamic_m59E43E27B0DD1B889DC7DE466C863142C2DCCB0A,
	SerializableCallbackBase_set_dynamic_mF1E2F025F26D48595B4CC8A144DF91F2A3C35F17,
	SerializableCallbackBase_ClearCache_mC38BFF1D60BB2F8871B83FD35AD8A3A591C0C281,
	SerializableCallbackBase_SetMethod_m0F58A4494D86F3B8B6809D245A7C205F49F9080B,
	NULL,
	SerializableCallbackBase_OnBeforeSerialize_m89E573793043132B90660076F3B7B97F09ABF843,
	SerializableCallbackBase_OnAfterDeserialize_m8DE492E4E800D46946D6EC872B289C01B204F711,
	SerializableCallbackBase__ctor_mD14E4A83A68F826ABF7869024D711FFE24F09E95,
	Arg_GetValue_mD74C36F6C755AB044DC20ECF0CD4D5396896EFA8_AdjustorThunk,
	Arg_GetValue_mD70CB98B049A3BE216251B26833C98D02E24942C_AdjustorThunk,
	Arg_RealType_m2A2F631F1878EB212A7465D164230068DB32079C,
	Arg_FromRealType_m1A86C61ED0F7E81413C7844C2A4A46108F3F5173,
	Arg_IsSupported_mCDF0DB7E36D6FFE5FE79A0A63C71982B183EB903,
	SerializableEvent_Invoke_mE60EF64DA2071762602A8E6D2E36CCD630E859A6,
	SerializableEvent_Cache_m410988D57ADC667418639D1C1AEE894CB1D6C6E2,
	SerializableEvent__ctor_m83DC30062D0F27C66F02CC78B6EE7AE79FD55E73,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializableEventBase_ClearCache_m84253DAAFD81F37E4AC12B3627C609CAD7D118E8,
	SerializableEventBase_GetPersistentMethod_m07A3B8FAE4050AF2DEF91E41A88336B09B267118,
	SerializableEventBase__ctor_m0BEC3D730F07C5E3A7DFBB18E481385106629BE9,
	ServiceUtils_OpenUrl_m289BC09F15CCB13AA2CFC859B6318964CB2291B9,
	NULL,
	NULL,
	StandardShaderUtils_ChangeRenderMode_mB9CF3B509DBDE18954C1464C9B315FE051B05A8E,
	BoolStorageData__ctor_mD151C642C30D2BB439F1092C0DD20B5FC5D9D943,
	BoolStorageData__ctor_mBA223D0250F4353CBB0793B290254E5E96075CDE,
	BoolStorageData__ctor_m3A6C4BD2787FA8DAF0D908B2E94278AE677A445A,
	BoolStorageData_Init_m7D115A6E379845BD4FC9AF74B77C86A6A0336D26,
	BoolStorageData_Save_m3F1C81550B43682DC2E7EA7BEEA603BC7710827D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DateTimeStorageData__ctor_mBCCFE90C68EDAAEFBEF621119CC8841A08F8462D,
	DateTimeStorageData__ctor_m89C4BF7244E93BA4BE4A06DCF15F29149C47D9BE,
	DateTimeStorageData_Init_m05068D2D5F6BB86718BE39CBFFAAA853C984A576,
	DateTimeStorageData_Save_m234DDE18B926562A9E5422811692DEEFF4428D65,
	FloatStorageData__ctor_mBB4A3BCDCF8B56948D1FA47A1AC93E52A510770B,
	FloatStorageData__ctor_mF20E9E37D7AB58FF362D3F4EC1BB6AD4FF764E4F,
	FloatStorageData_Init_mCE58B40959A4148E22C0EB3FECDEA8AED807F579,
	FloatStorageData_Save_m1961DE9E7575E485924C54AAFA9E72A8291DCAAB,
	IntStorageData__ctor_m4635B6F0F8421C4E1E334A64D4957512341F3C62,
	IntStorageData_Init_m89529AD6BFB757A9476FE273B38B9A25D1D7A105,
	IntStorageData_Save_mD052ACEA756F910700912192298CB931F2C706DC,
	StringStorageData__ctor_mD982C2B866CCDEB211422C052C1AFDC4B6E82D0D,
	StringStorageData__ctor_m2481FBA08EDA6B7193940B2661C343AC4B7B5978,
	StringStorageData__ctor_m81A17949BBB1C9AF49ECCE50CFFDA215F29337A5,
	StringStorageData_Init_m27C4FDB0A7F80BCD4C97D1971D6373D248A0C1BA,
	StringStorageData_Save_mC96CA10EB2F5DA271C9292E29F234EB35A186BAD,
	Timer_get_TriggerDate_m5179FD168A24F54B94DC1323B7B1153C0C1A0980,
	Timer_add_timerTriggerAction_mA9FA7FA0B68CA2903CC26845812F02EBEE20C595,
	Timer_remove_timerTriggerAction_m650C0F4EB2F70C611132DFE753395EC6CA24066C,
	Timer_add_timerUpdateAction_mA6061193C68DDA2DEE634FD6AB4139D6B93E2467,
	Timer_remove_timerUpdateAction_m5C8B5AE51AAB428970A8A6C11B6207E8A1C488B5,
	Timer_get_Seconds_m3BA996BF755EFD6DB2B59641D1CBD4696D472E57,
	Timer_get_IsFirstLaunch_m928E1294BD4642770120EA9A14952F35E4EF5A0D,
	Timer_get_IsStarted_m925CCA799EFFCCC164629379367083CEFBE81181,
	Timer__ctor_m15C77C4F2E9DFA1ECF01BB4692A650F5523C6C79,
	Timer_Start_mEFDCF17412717C132D11A41F14C8F9E96A4B78E4,
	Timer_Tick_mDEA0EABC83566AEFA72ECAF67011774B88E110B3,
	Timer_SecondsToTrigger_m779D80779CB5634D86B1E769EF823CE401FE1FD6,
	Timer_SetNextTriggerTime_m1938E93B3FFE1A7C33D7724D4F9DF00BB5EE1F76,
	Timer_InitDate_mA09EAD975CC83FD8CB30CE87B18FC221767F422F,
	Timer_InitEvents_m15CAA532FD224DC4C04720E861554886FCE10CE4,
	Timer_RegisterNextTriggerDate_m50A0BF798FC89118058C66FD6C94B1EF611879A7,
	Timer_U3CInitEventsU3Eb__27_0_m92C1657F316FE1E69E3BE691D18173A6BD999F3A,
	TimerManager_Register_m96ECADBC8F2294868F5D9ECFB9C7C202BBB526B5,
	TimerManager_RegisterTimer_m8D0494786E76F783C8E3A0C562767C1FBC01632A,
	TimerManager_IsStarted_m1B61C536FCF962C161B368C6673E3A90796117F0,
	TimerManager_StartTicker_m7C6AA77F6E57D1F4A995532706A83EEBB7972CC9,
	TimerManager_Ticker_mD17229B13330973D3F4130CA9904597F859B1E08,
	TimerManager__ctor_mCBA279B0F7D07D6FAF9511FCC5FD739927B72AA3,
	vp_Activity_Empty_m1CCC6F70B1E97A3193847E35DF652ADF4C572093,
	vp_Activity_AlwaysOK_m2C4ADE579DB601543E98BF65E5733E78C6CF6B35,
	vp_Activity__ctor_mB9C79F12E59E862CCA79D2ECBEECF19B0C4F484B,
	vp_Activity_get_MinPause_mEF6E369277C5FFCAFD1F01F7CA84DDF124800A5D,
	vp_Activity_set_MinPause_mDA5C90ADED913F2CBD7CE4FA29429E2F821FA8B1,
	vp_Activity_get_MinDuration_m201E2CAB9F6EBF53C69CB52D383091A931FD28BE,
	vp_Activity_set_MinDuration_m833A8C332C4D9B8F9426DE4BCDFF8FAD20EC613E,
	vp_Activity_get_AutoDuration_m83DBE1A23C39CA13D18BB0AC9DE1D1F5C84AD72C,
	vp_Activity_set_AutoDuration_mE8DB9262C7F9E826DDCC266DB773996C621710DB,
	vp_Activity_get_Argument_mC62FDB8F72F4770FC18D51649AC14BA2DE075A0F,
	vp_Activity_set_Argument_m0302CDD118A3C3794CA7D14E3FAB6C58471F7032,
	vp_Activity_InitFields_m7430B8B5D663D5823542CBE37529E1AD2A8D2DDE,
	vp_Activity_Register_m025A7BD21446A5F1520A4F7AB4BF0A9E9BD3DF6D,
	vp_Activity_Unregister_m52DB0C66F687DE2C090BD70E262DCEB998D58A13,
	vp_Activity_TryStart_m0EC8FAE9DDF548D26F1BFD594989AA816B0057A8,
	vp_Activity_TryStop_mE37B0E95A65E7A3B7EF398BD852E13E3B99E1617,
	vp_Activity_set_Active_m3066D0EC9BABFA7CC9FDEA4D127029D181571FFB,
	vp_Activity_get_Active_m86244ADC8E6FAB827A025DB6529BC1380CE915DE,
	vp_Activity_Start_mE6A5F6FCD03AB51038647E2C886667EA3A4BF330,
	vp_Activity_Stop_m35325F560384D71C36E272DCF497D5F56E6B3FED,
	vp_Activity_Disallow_mB5372995AF5AAF24F510592F58BD6242407DCFE9,
	vp_Activity_U3Cset_ActiveU3Eb__37_0_m9ADE28DE480CE352EDC151D875333C65ABB8F253,
	NULL,
	NULL,
	vp_Attempt_AlwaysOK_m80CC125F243D6FA1583EBBCD1D182B0DA2092A0E,
	vp_Attempt__ctor_m3118443A4EB35B402D253E8FAE20B39A99096F26,
	vp_Attempt_InitFields_mDCCE6F2DAC058EC1FCA58CC9E923891ED655A610,
	vp_Attempt_Register_mAE67F6C5DD17EE988742057D09097A37F596C5F0,
	vp_Attempt_Unregister_mE6C437049AA0E089AABE43E7678AFB590871701E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	vp_Event_get_EventName_m62614B846C0E4BB14613C4FBFC79CB8DABAF4E42,
	vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183,
	vp_Event_get_ArgumentType_mE22F5DF43AA062B9D94CFBA26DBD26DF4F6F6958,
	vp_Event_get_ReturnType_m2F546B33B4B596CF789400F4AFB23B9B360F8F06,
	NULL,
	NULL,
	NULL,
	vp_Event__ctor_mBAC4F17FDA57FE7F9766E60433C5A10BF198D4E7,
	vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF,
	vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403,
	vp_Event_SetFieldToExternalMethod_mA9E3D6BBEDCBA637A2FA457E5DC0820B5BF3D9FE,
	vp_Event_AddExternalMethodToField_m167005BC363AE73C25FF8679197A5C8E13EF63E7,
	vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C,
	vp_Event_RemoveExternalMethodFromField_mE1961E2F41C102B5B7FDC0C24539E47F7B5D5041,
	vp_Event_GetStaticGenericMethod_mC5A9BB11EAE19B27D7A4F37BEDB9C0C38BCA98F7,
	vp_Event_get_GetArgumentType_mC46CB784CDC20810B34182F12FF38DE1536611D4,
	vp_Event_get_GetGenericReturnType_mC788697DC4D5638D32A31F774CCF112AA22F2B16,
	vp_Event_GetParameterType_mDBFB1CE4D2C3A1BC89BF0103976F24AF0FF2148A,
	vp_Event_GetReturnType_mB691B6F9F31097CC9B6F14A0EA44D21900DC70BF,
	vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D,
	vp_EventDump_Dump_mA71BDE5B4877A8388A569E50608A2AAB8D0B153B,
	vp_EventDump_DumpEventsOfType_m41DA6EEEEC4BF8BDDCC2183F2B5702FDFFAB9AB2,
	vp_EventDump_DumpEventListeners_mE78660A26959E527A851634D572E8B26600538AC,
	vp_EventDump_GetMethodNames_m9C32399E4F83E07AC1EB8D21EF07AF4993382410,
	vp_EventDump_RemoveDelegatesFromList_mBC7EACE77CD372CFF33AF3DF4F665E7116A01A20,
	vp_EventDump_DumpDelegateNames_m49FFFF6E77963E45B7FADF29CE27B632E6E762FD,
	vp_EventDump__ctor_mA5DAF99C1397ECE410E7DD3135E34FF3A9498AF8,
	vp_EventHandler_Awake_m71C0B2100844C710B13F313A90C45402850714F6,
	vp_EventHandler_StoreHandlerEvents_mB9C45AC4A615E27D8FAC220F66D1F00D51045FF0,
	vp_EventHandler_GetFields_m5737E607BCE7CA8A0CAA50A665ECE9636FC9C5C4,
	vp_EventHandler_Register_m97E15AE2CBA236F376306CAF1B88A6CA8A52DD3A,
	vp_EventHandler_Unregister_mCD10136B77EE7EB6DC70F39A4694755C4D13C8E0,
	vp_EventHandler_CompareMethodSignatures_m3E143FC4F85E6441AADE35DCC93C83624065B65C,
	vp_EventHandler_GetScriptMethods_m283D015F14209320667DED777FD89F5F9582C8E9,
	vp_EventHandler__ctor_m9AA2633E5AC8D8B862D0B21D2A50880758A7EBD5,
	vp_EventHandler__cctor_mA6869EA1932DF2FBED78E01A282E0103F6F4D39C,
	vp_GlobalCallback__ctor_m5C8660C98B8E87B2CDA39E537FC8EFFC1E0D7C4E,
	vp_GlobalCallback_Invoke_mCF95398C8DFBF4B64CA962CF5EE074C52F7D9CD2,
	vp_GlobalCallback_BeginInvoke_m0DD17507DF7F8F5576EB7F2524E86DE02E21D86F,
	vp_GlobalCallback_EndInvoke_m6D5C22925F7D6F5E41F9370301C938D1FD8FC7F3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	vp_GlobalEventInternal_ShowUnregisterException_m810BE4C8FFC0EB3E34618AFC1E7AB0E19A4B83AD,
	vp_GlobalEventInternal_ShowSendException_mF09E5E95D8B262DC894ABFAB236DFF4CC5856146,
	vp_GlobalEventInternal__cctor_m1B8C0006AF7A3634EC11278B4381162061FB1F81,
	vp_GlobalEvent_Register_m18794A689E9DFE6B77F25016D958DA0CF3B25979,
	vp_GlobalEvent_Unregister_mF3CE96C87744C19E2BDAD568C5C2394DEB6CB292,
	vp_GlobalEvent_Send_mB5B8C3AEED968C3EC07B5ACE2E75083EF5E32A68,
	vp_GlobalEvent_Send_mD5E8C75F156165C85A2A0BFD573B28B21CE637E8,
	vp_GlobalEvent__cctor_m084F833C446A30B2DAC8720820C6DEF0A738412C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	vp_Message__ctor_m940941685CF5B3BC416C682A02E2B3B9FAE373D8,
	vp_Message_InitFields_m1CEDD9995A593C2344716725E4B278F6888DEC42,
	vp_Message_Register_m5CBF6BB8FBA90640DE2004608E8EB9BB2C602E66,
	vp_Message_Unregister_mD7641886B494A6B19A8A6BF8A71847404DAACC1D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	vp_TargetEventHandler_get_TargetDict_m9CE29A757BCB5A120FCB159FC908CCF2F815DA1D,
	vp_TargetEventHandler_Register_m40F794D992530514FE3446357BA1A370F3BE2FCA,
	vp_TargetEventHandler_Unregister_m20C3AA22709E8B32180291C20902D3BF6D02B9FC,
	vp_TargetEventHandler_Unregister_m8455936D2115A5A13E469F8FFBF9BA879F2587E6,
	vp_TargetEventHandler_Unregister_mF4B3CDD0C6A342B4A9A7C9065726EA454AAF91FD,
	vp_TargetEventHandler_Unregister_mED0A2341DE0F53D91987FAF0A99F4D8C30A1AACF,
	vp_TargetEventHandler_UnregisterAll_m255675AD648C47A425CF17D2B3F29326B8B49B03,
	vp_TargetEventHandler_GetCallback_m32C931FFAD8070B40750532482F490C2838E2603,
	vp_TargetEventHandler_OnNoReceiver_mA945B2CCCE1DFB858D2F3C6E82B7737B61EAEE5F,
	vp_TargetEventHandler_Dump_m81C5615B9AAE7D7AEAAB587A163BB239429A3E11,
	vp_TargetEventHandler__cctor_m0190AEA27131B57C6CD02774ED5CBAA9D9023679,
	vp_TargetEvent_Register_mF601E2295D6DD4E3C877C6F6AACC5E1F3644BA3B,
	vp_TargetEvent_Unregister_mC6189B616A3806D03B00D03E90B616BD01FAEDDA,
	vp_TargetEvent_Unregister_m5732856C8E935B78FFF383E4DBB212AFE8463B18,
	vp_TargetEvent_Unregister_mCD1BD34A01620CB41F71E758E482E8B3DDC44E3F,
	vp_TargetEvent_Send_m3C0C8C3E5A862D21D2D3B60D2E483AC62990A400,
	vp_TargetEvent_SendUpwards_m9A3E5E0BE42370BDEB05413505B4E0F3B4392112,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	vp_AudioUtility_PlayRandomSound_m718D90E2F96884AE683DB5C6238BC081A688D679,
	vp_AudioUtility_PlayRandomSound_mB4EBABD957F1B1A623B11BA162A9D5CF44544B85,
	vp_Gizmo_get_Collider_m50838D4AEE5208DA3558B7B2C7B2190A8E8C01AB,
	vp_Gizmo_OnDrawGizmos_m72D9B85D54E4245CB6E0B18D33FE2FD88CDF2487,
	vp_Gizmo_OnDrawGizmosSelected_m328B883A4ABA6BA3A4512DAF805FF08A2A652BD8,
	vp_Gizmo__ctor_mEA37331D262875D90CBDE1425220C1C0B64F16CE,
	vp_MathUtility_NaNSafeFloat_mC2E450334305656976CD7023307543F73B36CA1E,
	vp_MathUtility_NaNSafeVector2_mB76FBE51DC33256052F1EF4DFC82B52B63D5C91A,
	vp_MathUtility_NaNSafeVector3_mFDF04B17988F61F88472AA9D09966A19B57167C3,
	vp_MathUtility_NaNSafeQuaternion_m990106F4E6243B39B88331AB6B4A92A69A51C113,
	vp_MathUtility_SnapToZero_mB6888CB6174E67467052E7FF4293258F9056D75B,
	vp_MathUtility_SnapToZero_m636A89CA5826C9E460B24BC0384A656E77C85DBF,
	vp_MathUtility_ReduceDecimals_m73D14324760C853842688538EDD1B2D1BC199A5B,
	vp_MathUtility_IsOdd_m1E4C718EDF739E54B2FE5EED375042F1099FFDFF,
	vp_MathUtility_IsUniform_m9B9629499D7ECB547E33652E3CF8A8C2347086E0,
	vp_MathUtility_IsUniform_m8AEC1B14193A1B72DF426E127305FF02C6B1E5DF,
	vp_MathUtility_Sinus_m0C8B44D6C3263A2691776CE76ABFAC17F13E7DDF,
	vp_MathUtility_SetSeed_mB8283134E10A3DDDC23BA7D2EF6ACE6DA4AB6FA8,
	vp_MathUtility_SetSeed_m09FB77883391E271D8D980AA3C0AE9D4E1A186C6,
	vp_MathUtility_RadianToVector2_m993F3109FD44B127F7FBA812326AFE2242FC7DB0,
	vp_MathUtility_DegreeToVector2_m095F40861602E639E339051F9C4036B140B54223,
	vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076,
	vp_PoolManager_Awake_m7D8D67DA29554A79E318327316CF971A843E9932,
	vp_PoolManager_Start_m09371DAA63223410A778DD3B460D030E80FB339A,
	vp_PoolManager_AddToPool_m8195887F0F99262373A1B9D0E91C349D42C120D8,
	vp_PoolManager_Spawn_mCC73C86FEAD328B569F91955193CD4FB0299867B,
	vp_PoolManager_SpawnInternal_m9E77BCA2021A7EAB46CCB03ECCF513E2D11838DE,
	vp_PoolManager_Despawn_m9870CB2ADCBE296E93D5EF5D220359A2FD2FCE37,
	vp_PoolManager_Despawn_mC1BC1EA7AD2EDDA5739DF4AD58ED8050FAC4E5DF,
	vp_PoolManager_DespawnInternal_mBB770EAFF2D1C835704B87A261330296F185EB88,
	vp_PoolManager_AddIgnoredPrefab_m9535A7B6DDB8C1E1DDC96DC93633FCADE2371A67,
	vp_PoolManager_GetUniqueNameOf_m05FB19A94767E15279CB27ABD8C83001530D4215,
	vp_PoolManager__ctor_m760A92216A6665930C39F5BD09931EE6D4791D7B,
	vp_PoolManager__cctor_m7EFD7CD996A1E3C88C5D4E1D4496CC7655BE9BC5,
	vp_Timer_get_WasAddedCorrectly_mCB27C1BD5AC27B6A186B8FA5980F4A321CE8AA77,
	vp_Timer_Awake_m734E356C6199AC96E3802473310E601640AC9F7B,
	vp_Timer_OnEnable_mBED9298A21C89E42C7EF8FB3B534C14449A10E6D,
	vp_Timer_OnDisable_m0154E0C36FCCF3FECACC1AEB9B5EFB4F65C701EE,
	vp_Timer_Update_mCADAC09F16FA853E322CCBAF10445FFB37EA654C,
	vp_Timer_In_m6C38B0240730B0B7637A6ABE382178AD62AE600D,
	vp_Timer_In_mB9BADD2F31DAFDE8AC00B9DB14F0B3FE0A079C86,
	vp_Timer_In_m45D94814124056CCA4F15DB39851538AA8C56FA5,
	vp_Timer_In_m7B539EDFB4CAC928F5B3BA918334A91A48B653A0,
	vp_Timer_In_m1CE790478236A71309DBD4E95E6942C1C569DC3B,
	vp_Timer_In_mEF0C1B2CF80800DB8A069329E795AABCAD88F132,
	vp_Timer_Start_mB29B8806258029B318D347D4737073C30381CC94,
	vp_Timer_Schedule_mFA0E037BA960CF5A56850CB830E47D66E104DD51,
	vp_Timer_Cancel_mF17D30C62DBCF46555779B803D5F8098120BD3EB,
	vp_Timer_CancelAll_m3D5F986AC5424082974E9D063C01E2520A59A1BC,
	vp_Timer_CancelAll_m303B8675E314A56E8C8CA289D722CAF0C859D9E4,
	vp_Timer_DestroyAll_mC5F253CF9832EB2F7791460CE29F36523565C1C5,
	vp_Timer_OnLevelLoad_m8AEC3C9B0E4D301F84509FD6E721088729533D1B,
	vp_Timer_EditorGetStats_m28F9B475F1EC5A417A92F9577EFE5D168B523A1A,
	vp_Timer_EditorGetMethodInfo_m9E7A3690DFB13744FA2EC27383327956B3772A09,
	vp_Timer_EditorGetMethodId_m6DF107E430DF2469FBF032C47E80F59E43A55D1C,
	vp_Timer_OnApplicationQuit_m6DE892CB30A7EADE81EEB772E4200981EE336A78,
	vp_Timer__ctor_mF24734A1699F33BA69395AD871501D198E510E9B,
	vp_Timer__cctor_m5061568939E4753BAF3FEC384682A0EFC1C20D2B,
	vp_TimeUtility_get_TimeScale_mF03190B70748141A530A01FA8854E71C73F9F861,
	vp_TimeUtility_set_TimeScale_mB4E1922238BEAB40F01F686915C79F22A8014BA3,
	vp_TimeUtility_get_AdjustedTimeScale_m7D08D72631625292E84165D84B110EB71F63E834,
	vp_TimeUtility_FadeTimeScale_m1A76222655DB398DF7454ACAAF7CA42ED7B01C4D,
	vp_TimeUtility_ClampTimeScale_mF25E1E3C8A3E3FBA4A7527C258B1C05D74BA11F0,
	vp_TimeUtility_get_Paused_m7D499A7C137BE65A65EF3B04752A5718920782E2,
	vp_TimeUtility_set_Paused_m201DFD8830D9C4E1B618090EA20C3F350138BD41,
	vp_TimeUtility__cctor_mC36EE9FFDF64B061EE1B5989A6AA96181520CC3C,
	vp_Utility_GetErrorLocation_m6F3429DFF080043CBB6719F998CEDF3D76E631B9,
	vp_Utility_GetTypeAlias_m0B105E1F6666B82F789B1ED67B46ED98EED4832B,
	vp_Utility_Activate_m1D822960AA0CE9BC101420C0E0C683D92A0F91F4,
	vp_Utility_IsActive_m774BACB2A81DCC0F782DFE3A6783917DF9EE2BC5,
	vp_Utility_get_LockCursor_m856042AC6591FFF4A2D95322877B06E00C612030,
	vp_Utility_set_LockCursor_mFB089E7E19E8FE41EADE38F7D5C743B7CCC97ADF,
	NULL,
	NULL,
	NULL,
	vp_Utility_IsDescendant_m2383C0A44C9B19957CF9C867D00DF32E10CEEDAD,
	vp_Utility_GetParent_m23DF7AA74CBF3783F4D11A3C8159BCFD32C629C3,
	vp_Utility_GetTransformByNameInChildren_m4BAEFE6AB1211944BBC646B73A3C319EA810BE8A,
	vp_Utility_GetTransformByNameInAncestors_m85889B83D8D4B16EFBB1C417E7964480C825AEE4,
	vp_Utility_Instantiate_m40BDAB73E95DD2EA41F4C96D424CFE9E08B4E30E,
	vp_Utility_Instantiate_mB3FC1BB39D06ACF32E0409CAA385DDB0915B65E7,
	vp_Utility_Destroy_m4AE89615D99B095E1DAAF80E913F5AA6B9C4539F,
	vp_Utility_Destroy_mEF5255E8C4A8FA62DC4D9C33A1FB6F48B7B22B27,
	vp_Utility_get_UniqueID_mAE63167533A8BA058CFD0BFE90DF746219A3384F,
	vp_Utility_ClearUniqueIDs_m559A9F7D03CF00423358623E578D00A918B328DC,
	vp_Utility_PositionToID_m872F990B5C216FA657F53092D380CE04F341467A,
	vp_Utility_PlayRandomSound_mD2386BAC0DBFDC710BD1D921264DF06C8002BB52,
	vp_Utility_PlayRandomSound_m963E0566C1B780DA5B3025C846FE02239E98AC2B,
	vp_Utility_GetSprite_m269DD9BE9682187F7237CA44522DB667D6373F69,
	vp_Utility__cctor_m9B1303501DD378DBFA457D8B8A5C3F0F3AE0E5B5,
	vp_WaypointGizmo_OnDrawGizmos_m9CF12CE032519F0999581DCA4C6755F0ACA65028,
	vp_WaypointGizmo_OnDrawGizmosSelected_m5424ACE12543A130DF01A80B4079580B9C37A740,
	vp_WaypointGizmo__ctor_m6C7A345F5F27B690591D1B7EF78FD13769B8A25B,
	UIEventsUtility_AddEvent_mC95ABDE52F1129FEE122C3411C64E804121CA6E0,
	Splitter_GetRange_m72C3859C7FC21EEBAEACE317B549A496023C48D7,
	Splitter_GetLength_m36B893414D7C5C147779E594A1DEBC2D3337A926,
	Splitter_GetIndexOfChance_mE527221462A0EEAD95827B03679E0FE5C25877F3,
	Splitter_UpdatePointValue_mD0804DDDB95C9221D8E4355CE601B8406FDFFA67,
	Splitter_IndexOnRange_m03EDB3D8142EE8FFBEA70EDE558C3830B3D136C5,
	NULL,
	Splitter__ctor_mFA7A1B4FAE29B30C71E7FB447DB1187F9E57654D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ValueIntSplitter__ctor_m74D45F149EA4AA417FB7988792415E4E21FFC33D,
	ValuesColorSplitter__ctor_mF1CAF44414449EF470EE35F74E288A15863657D2,
	ValuesStringSplitter__ctor_m2670D0AEE133BD7CF5621C07FE6751AAD0B85F14,
	AndroidVibrate_Play_m4C3192115457819789396F9217482D22007AD27B,
	AndroidVibrate_Vibrate_m443F3B6730BB9CA30E423538C94268C959897C63,
	AndroidVibrate_Vibrate_m470978482D0207E81589CACF8DE3377BE2E5B99C,
	AndroidVibrate_Vibrate_m1F614D0C0E1BB1A94A043991FC6C61CD4F937DFC,
	AndroidVibrate_Cancel_mF05CC89E2E9CC91C14D2D165BDADC7AD14ED6D53,
	Vibrate_Play_m278053BB1618451EEEB0FFE9D1F8B9C74DBF23C5,
	Vibrate_get_Vibro_m3E1731591C30819C69C668B7389DC73064035566,
	Vibrate_set_Vibro_m59B5C889697523DF195C5E4609F33623939D3378,
	VibrateButton_Play_m80475E24A84451B2F85BBFDB7608FC0CD8413763,
	VibrateButton__ctor_mA73649857FDEEC68D9023F36103DE921BB1E99D9,
	VibroButtonController_Awake_m35BF5CAEEC17D16BCD6952B0789103E5FEF18EB6,
	VibroButtonController_Click_m61BE639058E8D9B897F5444460A3C8BD7C73A22F,
	VibroButtonController_CheckImage_m2B1335F9903891166523BF6F1349B85A78BDDF11,
	VibroButtonController__ctor_m761F78EEE9B47774A0CBD790A679527D8B96808E,
	AmbientSwitcher_Prepare_mFE7BF5473BA926732833B40D5F3755602FF6ACA8,
	AmbientSwitcher_SetVisual_mCD45EB5AFF872F31E5432FAA358B6FD1B796C442,
	AmbientSwitcher__ctor_m13FB52A42608898047241091D0AC039C7598B2AE,
	DirectionLightSwitcher_Prepare_m82A92E9046E295225971D4000C1DBA3FB53D5491,
	DirectionLightSwitcher_SetVisual_m051F1DC388F7E1C2CC42681A2ECDFAAF26EF9522,
	DirectionLightSwitcher__ctor_mC82786F6B8C0BB77E420CEC1BAED8ADA8BCB3386,
	FlashLampSwitcher_AddLight_m5CA0AAA357CF73ABCB398875ADAC385F0DBBDE22,
	FlashLampSwitcher_RemoveLight_m903A342D975B639E0EBB3AB4C054424D80DCA785,
	FlashLampSwitcher_Prepare_m6DFC0AC90B2994F20E64E81C66742D9C1B509817,
	FlashLampSwitcher_SetVisual_mC5D3C0255EDE672B6104A2BFAB151BB2BA615E78,
	FlashLampSwitcher_SetActiveLight_m326F0D5B8E8FF0FB2A9E1C28965B42C890FBE51A,
	FlashLampSwitcher_SetIntensityLight_m4EA148B7B7D80192B7A675E0E50E43A245769AC7,
	FlashLampSwitcher_IsEnableLight_m6A2F95A19D9D306E4B1B12BCF15096CB6B0A8F0B,
	FlashLampSwitcher__ctor_mB36B5B49B82E87B5B80C9DF9E1FF7287D3F7BDF9,
	FlashLightController_OnEnable_m1C073FC5FA19C6BFE18700D0740E474861CA8ABA,
	FlashLightController_OnDisable_m867C796A0982128770CF0D34C73194C75C153EE6,
	FlashLightController_SetState_m071A94625E272BE17708000833CC0324C0DADAA9,
	FlashLightController_SetIntensity_mBBD91DC8F93FE270EBB2D26739BCF68418132609,
	FlashLightController__ctor_mF1C2CFD7B49F8C04CB67B25C927010FEF5067638,
	MaterialsSwitcher_Prepare_m06CD8EA3CD81ED54C619CA0B3E19E5F13C148013,
	MaterialsSwitcher_SetVisual_mBA045F788590786ABAC71971580F5F9C914D0565,
	MaterialsSwitcher_SetColor_mA2640A2CB47AA558AC719252745039A498D0CA2A,
	MaterialsSwitcher_SetFloat_m694379818804B98436772C950908288FB32A9733,
	MaterialsSwitcher_GetColorFields_m6620977D6EE7DBC5A29B6D69EAFD01F99B8E8F01,
	MaterialsSwitcher_GetFloatFields_mC136C374AFDCEBEAEB4352D5DC741A26D0DE309F,
	MaterialsSwitcher__ctor_m6D1B82953B6C7529C9F2851DCD537DED6F39EDA8,
	MaterialColor__ctor_m9C0D28AFB2F3E7C1F051F2C637A1685998EBB4B6_AdjustorThunk,
	MaterialFloat__ctor_m4F5C48D61FF899A376B92A499A661F73C43B7B18_AdjustorThunk,
	VisualProvider_Prepare_m4E0A1E3F83A4F3B360F9298D72A0D4BFCFDEAECF,
	NULL,
	NULL,
	VisualProvider__ctor_m24BA09E0D18B5C1951359FFEDEF09A562F461B30,
	VisualSetter_Awake_m9518B0754CBD18526C9D0884B740AADD356F593E,
	VisualSetter_Start_m910DF5AD57D53B7F729625B73F0F369367638B10,
	VisualSetter_Next_m332E8078B15C299B86E1C54C0C4A322F099B3EB1,
	VisualSetter_NextVisual_m402EA6DAA7836D0B7A5179B6E8C91FC999F8CEEB,
	VisualSetter_UpdateCurrentPalette_mCE0468F15EC6BAA58055896C115D15DFBB475BC1,
	VisualSetter_UpdateVisual_mF6ABF28C7573C17FE6EB0E5C003EE96629BA9EFE,
	VisualSetter_UpdateVisual_mB7F7C47D20D4C3612644D1F336D66A3C3A9D29D3,
	VisualSetter_PrepareVisual_m5A8770D3643B1511B9A7826D269651E259B3BD8D,
	VisualSetter_SetVisual_m4588932365DBC0E97EF3F3E45FA2062EDC6F9616,
	VisualSetter_SwithcVisual_mC4DC7F2C5F09D3866CBEC43159393A36BB17BBDA,
	VisualSetter__ctor_m91DABE3B976CC8F5DE594815C326247615854973,
	Palette__ctor_m42E818B0D090CAE72F647EF026DDD934EE1D91E5,
	iOSMusic_get_MusicLoader_m8B10885E281BDA7ADD9792969558D8F308BFECF4,
	iOSMusic_set_MusicLoader_m19295714103C9930B7CEE0EAAD1A63897201FC34,
	iOSMusic_get_iOSMusicAudioSource_m9F49589F3FFCB0429696F7F7B2483FDC1D5673BE,
	iOSMusic_set_iOSMusicAudioSource_m993D0656B954CAA02AED952A14744CBB714E2DA1,
	iOSMusic_get_iOSMusicClip_m39A590849F0B06FA6DFEBCC2018AEA548F90D827,
	iOSMusic_set_iOSMusicClip_m31C1A83A7E093B8A4DECD778AA26BB4D530DAA9C,
	iOSMusic_get_HasAudioClipStartedPlaying_mCACEF6B9C3483C85E7B271C6C85734B910A33C0C,
	iOSMusic_set_HasAudioClipStartedPlaying_m97B195A060373D26B7A18649D5F6A0B64B60F1F6,
	iOSMusic_get_IsAudioClipPaused_m639A9EA784B1C2BCCE6F9636B9D18C8948BEEB4C,
	iOSMusic_set_IsAudioClipPaused_m2F61FA5DF748F7CEB7AE3480D50D2D21930324C3,
	iOSMusic_get_ShouldAppendToPlaylist_m6EC1216BE54ED3BC89F7B95E15EE6C96067D26AD,
	iOSMusic_set_ShouldAppendToPlaylist_mBEE9914912C2FE1DBCE54800D91F437A0841686E,
	iOSMusic_Awake_mDAEB3E61B7036F6BB08DA7C7D3F132E2F1FFD68F,
	iOSMusic_Start_m597F0BD7435A8768FDD1BE8A3A3EE479E8230E7B,
	iOSMusic_Update_mE0A629BFAA7DDD24B5B5B6ADF3C0917187AC0DD1,
	iOSMusic_CheckAudioSourcePlayback_m9C63AF1C24663247FA6FDFF2E36512462A912016,
	iOSMusic_PauseAudio_m1DD9659BE72D033A169C443CD18C3024CA468D8D,
	iOSMusic_UnPauseAudio_m7ABBC1C0ADBA12002250D1E4A480301B8D192F7B,
	iOSMusic_HandleAppendToggleChange_mAC20656B9EE6323CE152686679560D56940CBE6C,
	iOSMusic_ResetButtonStates_mF34677F66D9434189A3F97A04B8CE1EEF46DFBC6,
	iOSMusic_LoadAudioClip_m89B77832AFD6B18D8EB6C49D5CEBEE7E4FD57265,
	iOSMusic_LoadMusic_m69A96886113ABDDBA2628C5B5CE59C6633B99CC6,
	iOSMusic_ExtractTitle_m155852802CF14CDAB5F3B09D79FFE30C26718B42,
	iOSMusic_ExtractArtist_m1D405DF45B0FAD108A2F4F9B3B9ECBEB5E1266C9,
	iOSMusic_ExtractAlbumTitle_m403F8AFED5A02CABEA2D432E1956875335416223,
	iOSMusic_ExtractBPM_m84D947BC9425524187EF0DAFA3F893A65BACBED6,
	iOSMusic_ExtractGenre_m4EC57A8C49A3F8514BC6C3510CED06E32AE48AED,
	iOSMusic_ExtractLyrics_mC1158A1AA168089FCBC3846945B0FEF2A6B84143,
	iOSMusic_ExtractDuration_m41DA794440E24FDD43B76438546201CA83B3CAF6,
	iOSMusic_ExtractArtwork_mCE2EF273C5F89A01929C1C2DB6C76263821C25A4,
	iOSMusic_UserDidCancel_m3A5B01D6CADCAE864A5F9A6198EBD2A13F669269,
	iOSMusic__ctor_m36E10D6E5FE742C14B07C4EE26869E414C4D50D6,
	iOSMusic__cctor_mE5F8433E085CC9594A2D37A8A85928B8FAD1D471,
	iOSMusicGUI_ShowMusicLibrary_mB5D1F5A300EADB30426A89790B1FDC308E614B0E,
	iOSMusicGUI_PlayPause_m2454AB6E7786A020CA3021B06395554AFA6C8931,
	iOSMusicGUI_PauseAudioSource_m2867055A59421712C47869F58B3848F0D990B8B0,
	iOSMusicGUI_UnPauseAudioSource_mC7F5A2EF096D1BCE6DD12C1164F52D90856922A9,
	iOSMusicGUI_LoadAudioClip_m12C092D403D0428183A9034A46DE648EEF9E3B68,
	iOSMusicGUI_NextSong_m6A00E2507B6A005994AE4C9C3ACF7D0EECC1EB51,
	iOSMusicGUI_PreviousSong_m426D9927F088616AB019331E78DB4ACD92A3B2E9,
	iOSMusicGUI_DisableButtons_m9C0EA4819C2F3C01DA79F44FC7CE993950A3F6C8,
	iOSMusicGUI_GoToScene2_mB8648D2A95ABE59BE08003C953E0440F86C10B89,
	iOSMusicGUI_GoToMainScene_m698E579204B142C87FEC4E0C6C2F95964AA107D0,
	iOSMusicGUI__ctor_m0D4ABE334B6AE15AD31262FBEB37360A3BFBCAE4,
	musicManager__openNativeMusicPicker_m42D5CA4E039D2754C5E39F24BB0D98E4DF92F11D,
	musicManager_openNativeMusicPicker_m74EE859F41D88D7F36969ADA1B2DFE138033F240,
	musicManager__playPause_mAEF69B31AE2458E981774E91733B25AD95B0DC8E,
	musicManager_playPause_m0276C3B7C47209A31CFED839A7820D7E52C10319,
	musicManager__loadAudioClip_m48E843591B5EB97E9AB966199BFDDC278ADEAFAB,
	musicManager_loadAudioClip_m176F786929C7A84DD2AC43C9B8885DCC8A15BF54,
	musicManager__previousSong_m73E9EF2453C040E2B721424A0392E3DDB834E213,
	musicManager_previousSong_m9C52822B1E2F07B11D2A92C48E526A65372CC8EB,
	musicManager__nextSong_m7A659B7A19CF0EC46D275592CBDF177594EA8A4D,
	musicManager_nextSong_m4445C1E59AF31A332B321A9FBCC07AFDFA41C578,
	musicManager__queryAppleMusic_mAEFE8D9E49BAC81EA8C662B8B4F6FA06B7D248FA,
	musicManager_queryAppleMusic_m7ABB23AFCE1EA38DA6C880094827B7149422C3F8,
	musicManager__stopAppleMusic_m1D04FA6900EF505436DA28976C73BFB7699B4EDF,
	musicManager_stopAppleMusic_mC3EE745D3F6907B7261CCA6A33FB2CFA526CCF1E,
	musicManager__ctor_m399CB614BF0BF0C2F7DB10B7EE31DC6BB08C2477,
	AbstractLocalizedTextController_Awake_mB3EF38A853B5DA5D765ED81C2CC312044EA5B279,
	AbstractLocalizedTextController_InitController_m614C34A50B8F0C72B0A8E53B692CC37DE3D920C1,
	NULL,
	AbstractLocalizedTextController_SetLanguage_m8C246B55A184750C158BAC7E36EFDD04C40B34BD,
	AbstractLocalizedTextController__ctor_m91D1C2A31026FE4F5A894D8B7997E3AFD5C33C86,
	LanguageController_get_Language_m42952E381F0EA58E179B88FE0B2307BD78A97FBF,
	LanguageController_get_SavedLanguage_mEC0984116E11AD3D6906CDF30F2097C6D2B4B132,
	LanguageController_set_SavedLanguage_m1BECBEFB6B606893E2D616C5FFE10954CC45BFC9,
	LanguageController_Setlanguage_mADFA97CFAAA90435B2F4186629F377ED339008E3,
	LanguageController__ctor_m61CBB3F8B8347AF091E19176601B962611D69E51,
	LocalizedFrase_get_Value_mA7C5609F537C7026147FFA86052655751C3C3452,
	LocalizedFrase_GetValue_m4AA1D55306AAC7D2F89E43067A196B66A529939F,
	LocalizedFrase_GetLanguageId_mF6672182F1CD572BAFC6FC69B5BFF85BE7EA8FE1,
	LocalizedFrase_HasLanguage_mC22F1927F134FC8F749CEBEC1A070E07D12C9E19,
	LocalizedFrase_AddLanguage_m2AFB688B5BB1B8ABA76CA650BC7FF04A8732EF55,
	LocalizedFrase_GetValueByLang_m68E73B47CC258C6AE66A14BDD1E4DC212E0FA36A,
	LocalizedFrase_SetLangValue_m60681A116537E07DA01DD4B0F447167E4165C906,
	LocalizedFrase_GetValue_m2B66BAD333324B812F02C065EFAD209FE7B924EE,
	LocalizedFrase_get_Font_m1F4ADBD14104475772F80D79A2A6FFBD00A3B38F,
	LocalizedFrase__ctor_m0B12FC3B3BFC04540040EF1B2ACCDF6A057BEAC0,
	LocalizedFrasesDatabase_add_setLanguage_m924E1637BE1CB43AF2392B13558B67848B0E9E74,
	LocalizedFrasesDatabase_remove_setLanguage_mE7C41A587DFEE94A305C0A0709AA03CFC29604C5,
	LocalizedFrasesDatabase_AddSetLanguageEvent_m20317898CA1F857C509727E9B80E855E7FA51366,
	LocalizedFrasesDatabase_Setlanguage_mC749DFA13867B375BE09422BF1FE1BD19D9BD698,
	LocalizedFrasesDatabase_GetFrase_m28ED5007B896C5A8B48277250CECDBDAB1B7BB11,
	LocalizedFrasesDatabase__ctor_mA495DC622D2A1AB1C1D833F5CFCDA93618A593F7,
	LocalizedMeshController_get_FraseText_mE74311269BFF53B082BC1C2B8BC528672420F525,
	LocalizedMeshController_SetText_m7A6EE491246448819552E798A689A77CE101F81A,
	LocalizedMeshController__ctor_m868E848B4B9FDA3F5611725330B1A486F422D83E,
	LocalizedTextController_get_FraseText_m99ED2012B021E8A15C496284FC0CBD2D0EF2DF59,
	LocalizedTextController_SetText_m1BCB8AAD7FE6325583FFDD200BD3D02A7AC111DF,
	LocalizedTextController__ctor_m2E2C94E118225BB58C6A9A69D5E1002F417C899C,
	LocalizedTextMeshController_get_FraseText_m4A8978D27DF4D7570A44C78E6652C216A7E5B72B,
	LocalizedTextMeshController_SetText_m29DD6C644B7A154A9DC54DCADAF3D675A32BEE5E,
	LocalizedTextMeshController__ctor_mD9478751059335D9C418B641488C549306A8A820,
	AppleSubscriptions_ShowSubscriptionScreen_m4B31192D087AA1BD6037774DEC800C80118681A8,
	AppleSubscriptions_ShowSubscription_m61BC58C340F43DB951354A42BC57F23DAA676730,
	CrashlyticInit_Start_m378A4F28AC380BBAD658185D65857DF5E434D23A,
	CrashlyticInit__ctor_mD135467CCC1E57B58314665D648493B48E8706D5,
	FindButtons_Find_m57EFDA38BFC6B0A83A5F9DD06B7FAC26D8DA3FC1,
	FindButtons_Start_m3979CA10C44CF73054046CAC2D79AE258A0081E7,
	FindButtons__ctor_mDAAF20627F316FD3262581BA7B7BE11F91543712,
	FirebaseManager__ctor_m195CD6E7DD27AD966EBEB150DC1D161C2A7EF38E,
	Book_get_CurrentState_mA7CFA752B79E5189ECDD2147799F48A9E6D73B0C,
	Book_get_CurrentStateIcon_m7F336D495D9E75EB5F5FF17183CA9D2FD7543924,
	Book__ctor_mC5EF0C93595AD5E495DEE999F4E5D7E75F5FFEEB,
	BookPage__ctor_m2787C765538695D75C8C12F93187D24BA014EC95,
	MiniGameData__ctor_m10576CB492664E21AD6851F9194B85EE7E511954,
	GameController_Awake_m0F7A2663599EE488BA03E9010D12DF3D9EF623BE,
	GameController_Start_m229BE9A914ADF299ACFF1EFD5F15F27FF146D3C0,
	GameController_InitUIEvents_mD712073351E17B5FB03A6CAE743B721F42DB16FB,
	GameController_MainFromSettings_m27275DDEC5BCE9DD2CDFB453AF4BA79F46FC8DB5,
	GameController_MainFromSouvenirs_m38E730873B8287C568E8BB32C576DB4EC2C85610,
	GameController_MainFromShop_mAAD4EA08A2A050127ACB1F4C10C7C08DB9A46C92,
	GameController_MainFromBook_m366A997DE313E725E536FD7477A80BC7D863299F,
	GameController_ShopFromMain_m5131CF9D5B501A6BBA071B944E4A0BED2B89855F,
	GameController_ShopFromShopShopBookPreview_mB056A0C36811F9ECA60B56DEE93AFB16D7C730A9,
	GameController_BookPreviewFromBookContent_m3DFDC2325A56CEA1A486077F39B71BBF2C119A5E,
	GameController_BookFromBookContent_m1147D1707BCF9EAEB2D2076EF1F1C9AA48DB6076,
	GameController_BookContentFromBook_m1BDB14674F3D7FC44991B9D99014EF13230991B2,
	GameController_SettingsFromMain_m5D3CAEBC5C1C27004F8E471C675E9B336DE639A4,
	GameController_SettingsFromLocalization_mE88F090CBA7CEB9EA51510B29CB723E50677C802,
	GameController_SettingsFromAbout_m8AAA24238F4AB5066D654B20CDA397C35408944D,
	GameController_AboutFromSettings_m95A2352F24EB85FC0D962E19AD806E0F8C38CA8F,
	GameController_SouvenirsFromMain_m7547EAAF632FCF38E39972B3E1F3A581D2736182,
	GameController_LocalizationFromSettings_mBEE2B2C165108951C02EB8392D1F28BC94D3D366,
	GameController__ctor_m839A9CFB9635B009C1DA139BEAD0E38467E57464,
	DragableElement_TouchUp_m9743F990C60A3A6B494998CBE3EFBA787FA08684,
	DragableElement_Init_m2F3EE9099D5C8A5A9920FF86E27415F4530313F7,
	DragableElement_Fall_m25109712514B83AFB8BD4A2976F42874DE962C67,
	DragableElement__ctor_m1DB02098A80CC0BA43B95015F2198AACA2AF9F2E,
	DragableElement_U3CTouchUpU3Eb__5_0_m981583A5D35D2A82B928B5B8B6F1006361969FE2,
	ImageController_add_clickAction_m90D85E2AD366713B5E2B5EE2CEA401A8999ABB81,
	ImageController_remove_clickAction_m8038C213433C33AFB2EDC5FA0747151F855C7F16,
	ImageController_add_firstClickAction_m04EBC59B15F7A1D7A479B6E565F0CFE88521BB39,
	ImageController_remove_firstClickAction_mB257D872C6CB36CA0BAC8D996F2D99DB5367F1F9,
	ImageController_Reset_m82B007748A013D447A863BEFF52DCBE94B9DFB1C,
	ImageController_Click_m3D8630E042B3157212611D2743F9DD86E8D80E22,
	ImageController_OnPointerClick_mA834FB02FB295FED043892B78156186447C384D6,
	ImageController__ctor_m4C6CA6E7CB52930B2A60BC1BABFE1C243169534B,
	MiniGameAbstractController_PlayLevel_mF23DD7A356C616037F1C842A59EAFCFF3A05E198,
	MiniGameAbstractController_Close_mE627B18ADE52989B98F6F8AC56C7BB4BF0265478,
	MiniGameAbstractController_ToLevelsList_m425DA0B9236EC249FC298CB76C75A00246262A78,
	MiniGameAbstractController_ResizeRect_mA7FE3AE3E07BEBE563F0A1E0E725B3CE6F20EA5E,
	MiniGameAbstractController_InitButtons_mB24FF9D465A789ED8F4A5DEB6437E7B71719B74C,
	MiniGameAbstractController_InitGame_m2FEEC8ABAC3AE8D00269835FF9A8252467A801F1,
	NULL,
	NULL,
	MiniGameAbstractController_StartGame_mBE7951FE61141018AFE536AC4CE9B18D716AA733,
	NULL,
	NULL,
	MiniGameAbstractController_NextLevel_m4922C386AFA325B8AF02CBBE7504B63A5C55AFC2,
	MiniGameAbstractController__ctor_m3C597F284CF2D168E10887E08103C86342478E0C,
	MiniGamePreset__ctor_m30EDB015DE08F758F654602ED6A965B89535224F,
	FindPictureField_FindImages_mEF3519293F31DEFBCEEF62CD39C27B53C32A1E22,
	FindPictureField__ctor_mE85C42763D55F8EF651593E0E8B3A51E3F236FA0,
	FindPictureImageController_OpenHiddenElement_mF5A185505B2EDFCD07A672D81B8042C8896D4E4E,
	FindPictureImageController__ctor_m367A8A87BEB0280D3CC88BF45C4DF4B583D62855,
	FindPictureList_Reset_m7E7F208E1173554AC18C2E2B473C7062D840CDB4,
	FindPictureList_AddElement_mC627EF70488B95961CCEE14908A7A02DF085984D,
	FindPictureList__ctor_mE78DA7DC17364977DB82A7B5925852295211CC0D,
	FindPictureListElement_get_IsOpen_mF80F7422E5B47F77A8E243DFDC96629D11B4A9E1,
	FindPictureListElement_set_IsOpen_mD29D538B1AA28752CC966F7066DBB078E4C161D2,
	FindPictureListElement__ctor_m6BF17C06228D6FFE54A1F43A04DE91A900BEF410,
	FindPicturePreset__ctor_mBBE4E68FBF89064DFC97EE84B6C7B685A934FAD3,
	MiniGameFindPicture_InitGame_mBA32BA8C1F87A4198D97FECAAA799D7CA444B42E,
	MiniGameFindPicture_InitLevel_mE6565443776B0663C646588F8C18A0276ECC6A72,
	MiniGameFindPicture_InitLevel_m9D002439374AB00DCCBC9EFB195889476208BCED,
	MiniGameFindPicture_InitElementsOnField_m1FFCB24F4828B45E23F48125A20D2C1DF5A01CAF,
	MiniGameFindPicture_OpenElement_mDEEA54D18CF848A2923ED8CE08BDA2963B688A60,
	MiniGameFindPicture_Reset_mFF179DFF7B34C6397F28619675914C03DAC09D57,
	MiniGameFindPicture_RestartGame_mD7CE2DAD5411AC6C28C4D671F451C97A9B4E9DD6,
	MiniGameFindPicture_CheckGameState_mD86092E7765CE82ADB9B9364B0719775B56393E0,
	MiniGameFindPicture__ctor_m50ECA599803E245B766329528EC6C48DDAE0669D,
	PuzzleAbstractField_Start_m6CCA11B8574641E514EF738E8DFD9CD4866BA404,
	PuzzleAbstractField_InitMiniGame_m282F06AF131D7359E12856B733DAD81C7CB20254,
	NULL,
	NULL,
	PuzzleAbstractField__ctor_mD460EFC133C9C9C6BD10A0704BAFB2DC18AF7012,
	PuzzleDragableElement__ctor_mF49301612B9ADEFC43FF9248EDA77E28DCE5EEAB,
	PuzzleElement_get_IsOpen_m38A1E7F16E57EC5A0D3BC2A7C6DD027F760B3C53,
	PuzzleElement_set_IsOpen_mD0986B98264A5C37C173DEEACC29D5725961A7F2,
	PuzzleElement_TryOpen_mBA56A69CC9FAE69658CAF16D1DB6788483C466CE,
	PuzzleElement_Init_mBE9FD4E1E814F737B7ED744F27C15B2FE1C9D470,
	PuzzleElement_DebugOpen_mF0CB5BE5280D37C1F1293AD77709F32A6BFB9909,
	PuzzleElement__ctor_m2416D8EC5481DD3E7963791F23107E6BBF4839F1,
	PuzzleElementsList_Reset_m67C05CDB46FD42143194A705F62D3E6B528EF0C3,
	PuzzleElementsList_Init_m73195F564ED491B1B48FF1987AB321EBD8422DB2,
	PuzzleElementsList_Shuffle_mE02AEAD70A3AB34DE344CA8D56DDADE242217598,
	PuzzleElementsList_ItemSelected_mB2F24A50DF6FF216CA8AE722CF37AFC5C081374F,
	PuzzleElementsList_ItemUnselect_m8551DA25070E2139552877B45D418834B3988C2A,
	PuzzleElementsList_ResetoreElement_mA2DD8C7A7F2329AA080CE7CF3D230AE99E73E526,
	PuzzleElementsList_ResetoreElement_m3D6688DE7FB29A4B58F95A2F614BFAA17C92FE2D,
	PuzzleElementsList__ctor_m12E939FF7A5BCEB5E333138E4E2E4588A66B7C92,
	PuzzleField_Reset_m5A0FC0428916DEFD10AD83C2A5484700C430CB24,
	PuzzleField_Init_mB42A732CC5ECCC34A41FB0EB23217B3DE1074AF5,
	PuzzleField_InitSettings_m382ABEB5797060535EABB6AE209F9531D4ECD027,
	PuzzleField_TryOpen_m48EDD9E3A87271D64E7EEEFE5CC51B32587687BD,
	PuzzleField__ctor_m356E547F7971680A258A7085252A0B16F9FB5DDA,
	PuzzleListElement_Init_m4729C2DCEAB3BEB83515F5B62EA504DFEE47E293,
	PuzzleListElement_Click_m32D3F85834B4D9E68A9CD76F5ABE0C0A7B0784A2,
	PuzzleListElement_MoveToBottom_m715D8E43BB3194C5BA84017DEF35D2F07876A655,
	PuzzleListElement_OnPointerDown_m79283E6D3BABAB1DC5C25393FC814333130361F0,
	PuzzleListElement__ctor_m897F49723BD269A228218AF89EEF71DBC52BBBAD,
	PuzzleMiniGame_Start_mD27B608C9118F1EAB1848AF9204536FA628748FF,
	PuzzleMiniGame_InitGame_m57FA432443C7F00463F0142596B4E24BE824E9CF,
	PuzzleMiniGame_InitLevel_m2088C691EC1214109B75B63CEF3B0572EC2FBA4B,
	PuzzleMiniGame_InitLevel_mAF934C325E059D0E1C9345439B3F745AF234F238,
	PuzzleMiniGame_CheckGameState_m9AB0EFAFCA57BEC6283EF588E0E9776F14C98ADC,
	PuzzleMiniGame_FindedPuzzle_mB28B44A26B6492FEE1FAC586E1DEEC2A3F4B2993,
	PuzzleMiniGame_SelectElement_mC767CB400E6D434FBB0396272951D338006357FA,
	PuzzleMiniGame_Update_m6619EE8A58964E89276252DE51EE0681A9B72C34,
	PuzzleMiniGame_TouchUp_m3C7A0E1D8D4A54D2CED03EED935833C264D78066,
	PuzzleMiniGame_Reset_mAA9497DB046B1EBB562CF555B7D6BEAA3640FD0C,
	PuzzleMiniGame_RestartGame_mB30C955B2B1C7599EA80299E5B0E0098E5A1C363,
	PuzzleMiniGame__ctor_m189CF17B1DCD4767FFDC5E61CEABE46BF620E7BD,
	PuzzlePreset__ctor_m441EEC0E918E00132D854C4FDB61799F86F7565E,
	RoadsCharacter__ctor_m8161CFE5FB0A4CAA4183DEFBBE76C51432E4D9F3,
	RoadsElement_get_Ceil_m0414C61B96FC7017CBD603C326D7A97AE0EEAFE5,
	RoadsElement_Init_mA0884313602E31F63A522FF9EF78763A60B7DCE9,
	RoadsElement_DrawCeil_m8D508D86D962847B7F602F27F75B739DE0114F9A,
	RoadsElement_Rotate_m192E60C971C2C5BD92DE262760F81E50D2EDB063,
	RoadsElement_Click_mC45D5F0942BB180CD6EF33D0D73D05C59B0DC215,
	RoadsElement_OnPointerDown_m6DD8C58D733ADB7E7D5E25543104F5034B897392,
	RoadsElement__ctor_m5BED9D2824D09A790181E95BB5026D7F5DA681DA,
	RoadsField_get_RowsCount_m295C2B32EDABFF70E8C1960A3A21AD34923DC4FF,
	RoadsField_get_WinRoad_m2387EAA899E49BEC308319A31189A809954E71AF,
	RoadsField_get_IsCompleted_m82170DD4C3355A987B402248A79CA167CD2E0878,
	RoadsField_FieldState_mE4ACA5BAF4B2F538F3CC34F56F04013CF46D2DAA,
	RoadsField_IsConnectedCeils_m3A4E96058DBAF0DAEC740D090EC08179B9873DD8,
	RoadsField_TryMoveTo_m98537C7AE3DBAD6C0477777512052A4DC6E4579D,
	RoadsField_HasCeil_m965B4EF30E9C0B56616B6CC9DCD4254C70E0825B,
	RoadsField_GetCeil_m8983A615A1247C989E7AA585AB4C81D13FCA5021,
	RoadsField_PlayAnimation_mBB129F431B4A6B0365333D91EFF7BF04610145EA,
	RoadsField_AnimationLerp_m82BF4247951287335B52DE6BE81A4509FF5973F8,
	RoadsField_InitMiniGame_m59FDAB0D055538723F43DE4FAEB288853DFABB18,
	RoadsField_Reset_m3502006A622C077F193657BC60EE74B7BDCD9391,
	RoadsField_Init_m6C344F5705F478EE6CDCCA438E49316A52A0915B,
	RoadsField_InitCharacters_mE76DECF810403226AFECB94E19E6B508B6F972E1,
	RoadsField_InitSettings_m8D99715F8458FF048981A52A89FA2741EA48D301,
	RoadsField__ctor_m17FF3BB2B998FBEA736E5BE20A37BA2B0C07AB22,
	RoadsMinigame_CheckGameState_m06FCD6873D5C06616568057B28BE713A3CA0FEE8,
	RoadsMinigame_InitGame_m8A282A620FE7F6842460CAF1A006ADB8F9E1D4F3,
	RoadsMinigame_InitLevel_mC13E8E8FC4CB6C7D131A0B638CD66CAE0C913DE9,
	RoadsMinigame_InitLevel_m51731BD6EE81ECF742A3D331CD37E3F1302558EC,
	RoadsMinigame_Reset_m5E78FB3C7019B6C10D53D7560C708416B14D1EB0,
	RoadsMinigame_RestartGame_m1B6DB8FA48674ADD0281533025F023C444EEF995,
	RoadsMinigame__ctor_m68E9953E7341C7648A36AC7CF05BFD19A2D9CCD7,
	RoadsMinigame_U3CCheckGameStateU3Eb__3_0_m2C851C277FA8434074F52C826EA616B4F496DD1C,
	RoadsPreset_Generate_m7A24ACB1527590252D290343A6E17FDA37C87138,
	RoadsPreset__ctor_m83707AF4299FFC2E2E852871D47B15E669358E50,
	RoadsGrid_Generate_m43DDDD4E5FF3C0644A5933311F76AFD8FEFF4BF0,
	RoadsGrid_Generate_m450398321582926D65E9C80AABF2EA6FC2AEFC87,
	RoadsGrid_Prepare_mD97111D775D350B003994F38EB4DD13AD1F4247B,
	RoadsGrid_GenerateAndConnect_m9CA9F7DB4B5F263C8E71C5B7CE37A68C56FD2BFD,
	RoadsGrid_Shuffle_m05B9445CECFA67AAE64047F07E422EBD0DEBDA2E,
	RoadsGrid_IsConnectedCeils_m5A5AD221A8760A673BE082AC4E170BD43665876F,
	RoadsGrid_TryMoveTo_m7BCDE03EDB59EA9BF36303045E6F07CCB1DB09B0,
	RoadsGrid_GetCeil_m50FC227B16FC2A13E4184F6FC7C76AD6D892F74A,
	RoadsGrid__ctor_m89EBD187DA4349DEC57E6E485F6B2714AC453FA6,
	RoadsCeil_get_WayCount_m683B11F1EF86A4253AA463FA48CFC0251C33DBDE,
	RoadsCeil_get_ImageAngle_mACDF93184272C86E356774224EC6870459798212,
	RoadsCeil_get_Chance_m973B158864D1E8B66C37570EE61513B13C61B337,
	RoadsCeil_InitRandom_mCAB9505A448B5ADCADC328C74250764E2AA808CE,
	RoadsCeil_Rotate_mA4B3B82134A306134542151267C81043F842D40A,
	RoadsCeil_Clone_m4C231C055375EC4FD74FDC9DB792FFBEBE1247BF,
	RoadsCeil__ctor_m99E999DAF60A8B8892D2D665E488B613E6A5125C,
	RoadsSpritesPreset_GetRoad_m3A71751E12303908B160821BCE8376C92DA06828,
	RoadsSpritesPreset_GetBackgroung_m5DECD9DD24FCCA98BDF80DB6CBB120C4158252F5,
	RoadsSpritesPreset__ctor_m769E93F366451910D198FA569A5E17C6682AF0D0,
	RotatePicturesElement_Init_m140EE2612A6F51203ABB20615F538A62B9550310,
	RotatePicturesElement_Shuffle_m7480567E0A06FA2A26C429C8B58A41A136353C1F,
	RotatePicturesElement_Rotate_m91DF233A4D4CD4AA45CE31C4B6F5ADCD5DC793C0,
	RotatePicturesElement_GetNextRotation_m6AE5778223EFB3E6164A1C30DFF929094936581D,
	RotatePicturesElement_Click_mA662C17BBFC06073CF55A6B62FF1B400A7A676AC,
	RotatePicturesElement_OnPointerDown_mCD93DE4DCB50491AE0B19542E87D580E0D3961F5,
	RotatePicturesElement__ctor_mBBC2D9661D4113A5CC76EA60A6DF6E95EE61F07C,
	RotatePicturesField_get_IsCompleted_m165B79A933FB10E14B851ADCC42738CAE0E355C8,
	RotatePicturesField_PlayAnimation_m8D28C2ECE32B805D1F91221E58447470B35D3022,
	RotatePicturesField_AnimationLerp_m618D85272F87BDAF2F45A69EE5352473F65D4C58,
	RotatePicturesField_InitMiniGame_mFF70398119817B92C8F84A0EB2FBA11C5356C66D,
	RotatePicturesField_Reset_m6E0AAF527E7B979DA46639277DEB3F052B1AE209,
	RotatePicturesField_Init_m15C887AC55D604B41717E98CD239CE009BB92F49,
	RotatePicturesField_InitCharacters_mDE9914340B406EF211F4FF2A2C7CD0C704A2E3AE,
	RotatePicturesField_GetCeilPosition_mFE482B20BADA0732A1DC5BD34B46E1EB9A962293,
	RotatePicturesField_InitSettings_m6A30F8DB001EEB9B41B41C7C2F3211213A2C6D19,
	RotatePicturesField__ctor_m5F5CA040EC9805B2754669A4BE0593D2E6A9A586,
	RotatePicturesMiniGame_CheckGameState_mC8A1E199893BD17C290C66542B796ABD80A1BD72,
	RotatePicturesMiniGame_InitGame_m2AE4AD856DFFB2674FAEC0F4C33E862ED6998A9B,
	RotatePicturesMiniGame_InitLevel_mEC4ED164F7AA5175274FFC5E9085FD43119B623B,
	RotatePicturesMiniGame_InitLevel_mE11F98F251065C4C39341A6B5F8302F0313756A1,
	RotatePicturesMiniGame_Reset_m836BE583BA2AAEB08902567177EA74A548071757,
	RotatePicturesMiniGame_RestartGame_m22EEE478EAC8EB3A4E936FFC444486F285BE5FF0,
	RotatePicturesMiniGame__ctor_m5837BE5699C53B2A0182B6EE350273E5AE33A889,
	RotatePicturesMiniGame_U3CCheckGameStateU3Eb__3_0_m1A1EA191872EBF507C3B0B936D04B266B839B280,
	RotatePicturesPreset__ctor_m348714746CE961CBA41F8DD07335CFEDC111BF4A,
	CharacterPresetPosition__ctor_m85BDE4A914D20D6A44361FE91E5CEDA3DE56187E,
	StickersDragableElement__ctor_mA96E2710C814ED88F3391496DFFC9577D4C04614,
	StickersElement_get_IsOpen_mA644FAB2E97BEF63E6A3A53C86BCFD88619FE15E,
	StickersElement_set_IsOpen_mDA6C548DEAF950A1E9D97130FE7C3A30C4665319,
	StickersElement_TryOpen_mB5B945067FC5BE44C12EFE4CC7B0033D4A92833D,
	StickersElement_Init_m92764F2C9BCED64ED119447FFF588C9DD075AB22,
	StickersElement_DebugOpen_mB612BAF31D2F18857368B95188B524C3303D1A08,
	StickersElement_DebugInit_mA9BD8F8D36E94FF7F4DC14D9CBC00A67BA7909D7,
	StickersElement__ctor_m98BD06CB56D5782B733121406F057D6611251FEB,
	StickersElementsList_Reset_m2AAFA1DF0D79D17C0FEDCF2615B8EEE244017F41,
	StickersElementsList_Init_m39A4C738D3E6799F59568060A4A00CF8B9317906,
	StickersElementsList_Shuffle_m2C467A4662239F87FC7002EAEB8A3199552C0C2F,
	StickersElementsList_ItemSelected_m239E82D7863142BE7BBBD17C6FFE573BF83584DD,
	StickersElementsList_ItemUnselect_m0F107154A910CE28E4D86D9B98187FB0C3EDC5F7,
	StickersElementsList_ResetoreElement_m4520764B8D1B4A1315B1D9779E256A41D1988938,
	StickersElementsList_ResetoreElement_mFE53702FBE8090E44DBF02E7A8A95131FC36F789,
	StickersElementsList__ctor_mE28A0EC89FCAE56FFFDE8FF05523A772397719B3,
	StickersField_Init_m91D8632E726B847E321DDE54E441605C9AB4F44A,
	StickersField_InitAnchors_m447AB784B9B978CFA8E40208D2155AEE8EC04641,
	StickersField__ctor_mADBE3872C35F2006530D8E4D72CB71958B8DE016,
	StickersListElement_Init_mF2546B578EB50889EDD38C720D279A1A024636C8,
	StickersListElement_Click_mF10F15AAD2B7904A9CF6AC40A1675E1104036AEE,
	StickersListElement_MoveToBottom_m731B04EB2F9266B572DB5BD6B8CFC368D2B8F49D,
	StickersListElement_OnPointerDown_mD4D8A5D4031CBE45D17F4AF5422A1ED073B05D78,
	StickersListElement__ctor_mBD1DE945B2DA4CB86115F3D9BA66F403D6A83031,
	StickersMiniGame_Start_m01DC39389EFCC6659E2EBA0F8FBDB6552D40D118,
	StickersMiniGame_InitGame_m52921DCCD1E939EC44005F758AB19246AB7AA59E,
	StickersMiniGame_InitLevel_m54A4CECE797B7FCC49EE9BAD67D736CA0ABD3C01,
	StickersMiniGame_InitLevel_m834D37D0B8C501BEFDFE3476E47EAB6A9D9C7F75,
	StickersMiniGame_CheckGameState_m5F810C2001744D84E058C58519387A8EC1CEE542,
	StickersMiniGame_FindedPuzzle_mA544EDAB5A65461E3CC3759F0D05AA0B88DF53DF,
	StickersMiniGame_SelectElement_mA0EE99250D2636B71D8BB7F778088367F1357737,
	StickersMiniGame_Update_mF7E1B4B56E333372576D412558A5BD6216FC84B1,
	StickersMiniGame_TouchUp_m0E6E66A9F38DA5E1D141513BFB25BF8A87BF2B76,
	StickersMiniGame_Reset_m78AC47BD41C778CE6BF40CADCC75DED25344F051,
	StickersMiniGame_RestartGame_m7E7CAC1FBB4151B97EB812925CF0EFD61EA7B95A,
	StickersMiniGame__ctor_m61C910FBE21D14F9AAAB27A7985296986586184E,
	StickersPreset__ctor_m9332A6A03A5C5E09D18EE564AD9AFE27B3B91917,
	NativeAdsController_get_IsShowedNative_m5FCE899AAFA75FA76DDBEB37B230138E066DA53D,
	NativeAdsController_set_IsShowedNative_m19FCE37F450163E36A45431A0208043E14DCB9E6,
	NativeAdsController_Start_mD4C8522CDB79D737D270F4192BAE4EFE376BC17F,
	NativeAdsController_ShowNative_m9DD1A0AC4A890D1DB5E01C4C8ECF19CF1219D1FA,
	NativeAdsController_HideNative_mFDAEDEDA067288E7D777B6ED75B359A9000A18DF,
	NativeAdsController__ctor_m5CF03B296BA7B6B59C32B1B9FCD1DDDF9E0FC441,
	PlayerActivityController_FixedUpdate_m27347879F7EACDFAD5897AEF07E62DE5597E7847,
	PlayerActivityController__ctor_m7CD3FCF4B9618F4CE3A540B374EC9B928C93B111,
	GameSettings_get_Data_m3DD3435C94F29A159BA6CF7D53BE1C58C9401ACF,
	GameSettings_get_ServiceData_m6E5E7F6BDB79D8C115F14ECEA7A83DB43121F45A,
	GameSettings_get_UI_m32B6364E127C138CB50478CF86DC6080A4835BEF,
	GameSettings_Awake_m9808AD61E6CC8098228E5D4D5BBB40067FB5C048,
	GameSettings__ctor_m01D23515D458DD3A0BBD6CD02F9AA5418AAE8DAF,
	GameSettingsData_get_InitableSettings_mE5AE769E3DEBC0041EEB10DEE007318CF2E821E7,
	GameSettingsData__ctor_mD80D7677A8C336E63461267E01C88B46604F7FB7,
	RemoteSettingsCallbacksInitializer_InitCallbacks_m4E5DFAF80E88CF41DC4779E1E1E892CD4FB9AE4C,
	RemoteSettingsCallbacksInitializer_UpdateConfigVersion_mFB09ACE7F31C3E23F03EF2769A6FE58207BF0362,
	RemoteSettingsCallbacksInitializer_Updat_m1CB5D4B3B13FC2FE9EFCFB76CC9987D0E7097D48,
	RemoteSettingsCallbacksInitializer_WaitEndOfFrame_m9AFDD481B04AC211B3A5938D38A273FCECF534ED,
	RemoteSettingsCallbacksInitializer__ctor_m0E1BEF33C1C395A00FCF7042DDD1775EA1704F35,
	NULL,
	InitableSettings__ctor_m391A42B42D211528D7F49D87B8D7BF0E06406A93,
	DebugSettings_Init_mA0097759B4D0F80B35F0FB23F7B954F614602ED6,
	DebugSettings__ctor_mF032F7D6B6837B546A3DF276663AA4FCA37A9D50,
	GamePlaySettings_Init_m64DE8C3FEC95FDCCC681F6DE6AF5D9C19A636626,
	GamePlaySettings__ctor_mB8632AA5A01FB47960FD319F93565F83D240B963,
	CameraClippingPerformance_get_DeltaClippingDistance_mD77D70A56FA5BCCCE41F90D75EACCC294F9F3616,
	CameraClippingPerformance_Start_m92DEE5F109CE0F1751644ECBD04638CB75B6DEBC,
	CameraClippingPerformance_SetDistance_m5AFAC9123FC15F6A5E0A98CC87F4C7BDE584546A,
	CameraClippingPerformance__ctor_m7C270BEAA24F194747014A277891E73775B30C6D,
	DirectionLightPerformance_Start_mDDF130714F50E988AC5102F3775DBD701136CAEC,
	DirectionLightPerformance_UpdateQuality_mF9093FF8FBEB1F914735A3A1405123B22225F3AC,
	DirectionLightPerformance__ctor_m6044B42DE918A4C4CC3434D62E495772F176F4C0,
	PerfomanceSettings_get_TargetFPS_mD2A9A10E4267753F7DC742E5C26673151FBA0CB9,
	PerfomanceSettings_get_MinFPS_mABEC4E177FBB7E67BA2BC9CD09DEF50A9846901F,
	PerfomanceSettings_get_CurrentResolutionScale_mA780F8339791094E4B0BAED3D6FA8E54897ED500,
	PerfomanceSettings_Init_m131D24EDC3AAE190581DA7C8D417F6BEE47B20BE,
	PerfomanceSettings_DecreeseResolution_mD138F3F93C7E1612CFA3CE9ACD4301773A29F320,
	PerfomanceSettings_SetResolutionScale_m4287BD355EAE84E8A8D0F6A50D1020D128800AD9,
	PerfomanceSettings_GetPriority_mFB3AF5D948A8C3CFBE644BAE5825E56CD30F2EB4,
	PerfomanceSettings__ctor_m697A96205237C8C96037011FA5A1052EBC7A16E2,
	PerformancePriorityInstructions__ctor_mB2C605B5778464A0F0D928F41664665AFBAD4D5D,
	PerformanceController_get_EnabledPerformanceController_m08BB0A1D84CF06AF45CB8FFAA0088A3980AE10C6,
	PerformanceController_set_EnabledPerformanceController_m5C85B1AC81BD207E90B7CD7315D8495AF3BC2CBF,
	PerformanceController_Start_m17F87E05ED27C9FD624EAE4A6CE4006015D41DA9,
	PerformanceController_StartController_m76FA3EF4452531270BEAD47EA6335221787787AF,
	PerformanceController_StopController_m01752451638F30F8DD680CDA7EFA72F97C828D7E,
	PerformanceController_PerformanceControlRoutine_m0BC775CAAFBEDB88D6834F3BCF2E2C749B174368,
	PerformanceController_StepForUpPerformance_mD52FEEF99B073341736D3B5F7EB20F355BD03576,
	PerformanceController_NextPerformanceStep_mF19B2E02CDA6E94E07967798527499D8812A761D,
	PerformanceController_ResolutionScale_m3B75481F52DBA5382AC2AF8447DB9BAC6527F566,
	PerformanceController_ShadowsQuality_m27DF7B43FDD8CCE36F1C50D78E3280B5FB915979,
	PerformanceController_ClippingPlaneDistance_mBBDCD0AFA3F4E8B0C759697DA92020945AE27E2B,
	PerformanceController__ctor_m06E47BDABFCB2AA4C7F8D90A348C5E95FF3FFE06,
	RemoteSettings_get_ProductsAdress_m7D753A76B0BA1498A61353D00871032B47C8BE52,
	RemoteSettings_get_AlbumsAdress_mB165CDB81C304428D46A3D2B8A6846F8C7F2EEF5,
	RemoteSettings_Init_m6CCBA2305AFD237CFB4A9585852FCB41696623B7,
	RemoteSettings__ctor_mD5221A1AC741ACD5616400987B54BC75975DA5D4,
	UISettings_Init_m6971E9379602F21C3C630A475853AD96CC0B705E,
	UISettings__ctor_m02AC0D0225C14350248CAF0FA4EB07BFDFB43D19,
	AbstractScreen_get_IsShow_m92E88A49BAEC447CF66ADFC8882FE1922A34E372,
	AbstractScreen_Awake_m8B61461FE6C8B334BF081E61A30588B448A9B5C8,
	AbstractScreen_UIAwake_mBC57B452BFAAC07402686E36CB3A4849784EAA1C,
	AbstractScreen_Start_mC223D37C1F6D15FC8FFF2897A0E48CB1B61BE93C,
	AbstractScreen_UIStart_m8FD85444A56EFCD469AA6E9C83843DB62A7618BF,
	NULL,
	NULL,
	AbstractScreen_InitButtons_m8D04EF4F7B52910556EC7A21768B77AA605D9D88,
	AbstractScreen__ctor_mCACCEE2F5869CAB7A748BC9ED5181E16A3E41FDA,
	AbstractUIScreen_Awake_m0A4AB13784B8937670045771EEC6E56F4CF93F1A,
	AbstractUIScreen_Init_m508314953ED8AA851252A5304318BFEF85458D09,
	AbstractUIScreen_FindAllPages_m2F1451FB2A5A8343FC22C79DBD27202592E3D9F4,
	AbstractUIScreen_GetTransitionScreen_m6A25E69C48653D467316904F60475CD4026FCFD9,
	NULL,
	NULL,
	NULL,
	AbstractUIScreen__ctor_m9C70A32F8CED09CAAAFB62FCFB9BC89459D5066B,
	AbstractUIScreenController_get_CurrentScreen_m30680D995DB68112CFD191C148DE97455ECDF595,
	AbstractUIScreenController_set_CurrentScreen_mBAB36560B1E293CAFA640A08BF4631CB50BB419A,
	NULL,
	AbstractUIScreenController__ctor_m362B44D1AAC9657AE350000261C2F833743CEFD7,
	AnimatedScreen_Hide_m0A5F6AA19CF548011390CC23DF3911182D1C1350,
	AnimatedScreen_Show_m466070CA4336905EE9435A521BD1DBDEE4A9B63F,
	AnimatedScreen_UIAwake_mF252AB2792A7FFA16DCF31D2586FD3571CA1D7B3,
	AnimatedScreen_InitAnimatorParameters_m40158E0A7D8536A39CAB72827D0B9B3835AE4745,
	AnimatedScreen__ctor_m6542A85D76EDE01B70EA6F3F76F6E776F6730B09,
	AnimatedScreen_U3CHideU3Eb__3_0_m3C7BA0ACC50F176C16399CFCC0D278512F2AFB36,
	CachedTransitionScreen_UIAwake_m6F8EB57E442308F55724B0AB869C320390483B0D,
	CachedTransitionScreen_Show_mBE5A7D66E3D400F5D3CFA55BBAF868938FE88673,
	NULL,
	CachedTransitionScreen_BackRule_mE9CA4A4463C8D5DD55DD396DE0B09F64417CF45A,
	CachedTransitionScreen_Back_m40B2093331738A2C041875A764006CF725B2CDA9,
	CachedTransitionScreen_BecomeCurrentScreen_m4DC90FB9D4FF5A2CEF12FEE87C15F93FE0685C42,
	CachedTransitionScreen__ctor_m6EA1C82D42236CAF771E035CA5D84F0115D7A5AA,
	ButtonAndAction_get_Rect_m2EE10F62B37EB1ADA17BFB1FEF8C83149EE3439D,
	ButtonAndAction_RegisterAction_m9129D456790373AC1918A2CCAD3786159F4E7786,
	ButtonAndAction_UnregisterAction_m769C484DEA53A6A0DEFF2D5CAB6C19E71FD8F5EE,
	ButtonAndAction_ClickAction_m4FAB35779E39DFD7BEB2666CCB12504D19545A60,
	ButtonAndAction__ctor_m07CE44EA88D2F5BD6F9ECC819559CD24B9C44B61,
	GridResizer_Start_m0E3286DA02B7E723593D39786AB8C1DCCE7BB00C,
	GridResizer_Resize_m2E62C86B4E29EC3F60FEDE47F3E85D0FAF735044,
	GridResizer_SetGridSize_m174EEBF66266239DDDA507FD268098FED168FA96,
	GridResizer_SetCeilsSize_m3DDB19CBDF16415F7413EEBEB7E1A41408C16104,
	GridResizer__ctor_m0C246A42BF2614F44DB0DA4DBD8089B06405ECE7,
	NULL,
	NULL,
	ProgressController_Start_m55EB43E49082303F5B9B646DF9340C7A6661D9E6,
	ProgressController_Reset_m8D7E35C6EBB4C50611E95F9B4BCC0765DBE67E14,
	ProgressController_MyReset_m04CA8C07F8A265B1928E25187E976F18EDC802F9,
	ProgressController_UpdatePercents_mF52A987C5A6AACC6F82AE5EBB291C97F435EDAED,
	ProgressController_MoveArrow_m9CE49A405B0ED1D258183B35675F3AD762D30690,
	ProgressController_SetPercents_m52C321A9128F795F8F2EF34F9B45E9C8FC4611E1,
	ProgressController_Update_mE9A54B5A3FB460358CC790BDA65F3561930B8B4F,
	ProgressController__ctor_mF2BF60EBF6C3888D0D0848E03E02769B899BEAE2,
	ResizeController_Start_m985853BE8C1677830F9A2A0CB4C4902A1A24E750,
	ResizeController_Resize_m8E98468BD64A34A311A4A250CF2FE87F82DEF7AF,
	ResizeController_Save_mCAAE9B23C69B1B34FC45EC9C74B888E9E9BAE1B2,
	ResizeController__ctor_m1CE0A9D81C38E053DAA6D61B526DE684582D2A55,
	ResolutionSettings_Resize_m22E8A43A276DECE9E90FBC3E445DF48260B3D8A6,
	ResolutionSettings_Save_m906A8E2CF8B9C69318647CCBF856F3F44B62D06D,
	ResolutionSettings_get_ScreenCoeff_mB419912325DBD299B022B9676A1D4C51149FCC86,
	ResolutionSettings_get_IsScreenSize_m8241F0A04E53D9A299722615EC1D8685BEE293F5,
	ResolutionSettings_get_SystemSize_m0E40214E225476807BEF249638CA25D5EDC154DE,
	ResolutionSettings_get_CurrentSize_m13A4667BE230222B3B057BB23CE13FBD15922E43,
	ResolutionSettings__ctor_m31FB973C6E73B89A8449DF9F5C83FA531C1C5544,
	ObjectForResize_Resize_m4ED8227ED7C7D553D9DF6EE31826E2913F8F8107,
	ObjectForResize_Save_mB2622CDCB003093701F6B66FABA4A0C945791178,
	ObjectForResize__ctor_mB632B47E2A231A5251C1F9EEE469D2C40E617DEE,
	GridForResize_Resize_mDF94C7791AD18A3FB81B45E56E516DF55A20AED2,
	GridForResize_Save_m4B7BF56BA2E577D335F73862B8518C790BD719EA,
	GridForResize__ctor_m531CA6FC8110687EA08413778498CE1292B3CB5E,
	AboutScreen_InitButtons_m5B659544CFF560DB87DB9CEECFEAB46D4BF41279,
	AboutScreen_ShowRule_m878379B24F5B7EF2687B80CC8ED71AA8E2F2B3BE,
	AboutScreen__ctor_m32EB273FD90A18E625A9785602C8735DEDD2DCE4,
	AppleAlbumPreview_get_affiliateToken_mE34BECC115ED8A8C245C30998E7766542583A7FD,
	AppleAlbumPreview_get_Loading_m48178171D20E6A069AD434E61A9634FF847A1824,
	AppleAlbumPreview_set_Loading_mF6901497AEEA20C776C4162FF26C6E2E0D9E506F,
	AppleAlbumPreview_PlayClip_m1FE8B99F73E64D1F12D969C7A98D9D448D2F885F,
	AppleAlbumPreview_StopSongs_mAA242A00B64CBC1B03FFFE686375C89FB6B0C2D1,
	AppleAlbumPreview_UnloadAudio_mF1F03A7FFCB109BCD36C1761922540C3D259B907,
	AppleAlbumPreview_Init_m208E7E6DDBBBB4DFE6B57B34B57156CA689B17C4,
	AppleAlbumPreview_RedirectToAppleMusic_m9BE203B975666297B4F1C76E25E74A32F9E41E27,
	AppleAlbumPreview_LoadAlbumData_m24E4590D273885BAAD513F270A215409580D8BEC,
	AppleAlbumPreview_InitAlbumData_mA8436F73B77D27C01E6C109BB02B96D4BA9A3EEE,
	AppleAlbumPreview_InitSongData_m3B0DA64C2961F06DD1972A9E4EBEC5500FA96204,
	AppleAlbumPreview_LoadAlbumDataRoutine_m1E2ECC165E61E9C023F9B04398C4301AB12BEB69,
	AppleAlbumPreview__ctor_m728E15E22215CA38BE6E68F1F29C8C60222BCCBC,
	AppleAlbumPreview_U3CInitU3Eb__30_0_mEE5279F741B91AA9AD3654D22DE499ED97B5F70F,
	AppleMusicAlbumsData_ParceFromJSON_mF4E2057A5AA40F6A6121336A261101FDCF5A3B09,
	AppleMusicAlbumsData__ctor_m035A58012AC8144778ECDD286269552B4ED23192,
	AppleMusicAlbumsData__cctor_m3EE11843D0D5594B94EB3C264B372A03485A17E3,
	AppleMusicScreen_InitButtons_mD7199705941BDDA3DA4DA106CCAE9D2E3E758DC7,
	AppleMusicScreen_Show_mDA29AD0DD8016A38EEFE2F2C5DDFD5128015CA59,
	AppleMusicScreen_ShowRule_mC0C5C1FA4AF23373971062A299C79D131051C2B7,
	AppleMusicScreen_Hide_mE7D1C898C7636D5EB8802B5E5CBE4ABF6E58BABE,
	AppleMusicScreen_InitAlbumsLocal_mAA2D482E7F7AE3E984FCC61BB6849428E0E5D4D5,
	AppleMusicScreen_InitAlbumsRemote_m16E8166AEFD50BA44524DA622A0086FE8258BA3D,
	AppleMusicScreen_LoadAlbum_mBAD2D005A34CB794A9C56D66498CB61D86FF565B,
	AppleMusicScreen_OpenTestScene_m1EB2303B19E91239BD784D2E7734CA1DD60C1C7A,
	AppleMusicScreen_PauseAudio_mFC0BD6959F1E6BFA8B93817D4302B6455435E48C,
	AppleMusicScreen_UnloadAudio_m757D089CBBE883A1D9732ABB8BBDD9E696F43DA4,
	AppleMusicScreen_LoadAlbumsWWW_mE2A511065C9B5DF97B27BB21421D7C08209D516A,
	AppleMusicScreen__ctor_mCAE51A452D41D690497CF4E8AA4ACEF08B1E4614,
	AppleMusicUtility_BuildAbumDataUrl_m09388B4036A8029D533111CDA98B61F38B9F3CEC,
	AppleMusicUtility_GoToAlbum_mFAA2C296010BD09D6669D39EB3AFB27D5D2FD17B,
	AppleMusicUtility__ctor_m38AADA1227762739166D210E940A372A31B90631,
	AppleSongPanel_Init_mFCCE82CF7C642C844BF505FC6B7F113BACE93F7D,
	AppleSongPanel_Click_m40D39CB9F338B7F109A12F85F5DB42A06B9A6E8C,
	AppleSongPanel_LoadClip_mB91CEAF69B32A19EB70B0145BA31E1CC0B584C93,
	AppleSongPanel_UnloadClip_m7CFAD8800CBCB508872CB4314C3E57127A7590C4,
	AppleSongPanel_StopPlay_m89FACA4871EAC02F321002C180BA44F2DE3F9E4A,
	AppleSongPanel_PlayClip_m0AB6C754827AF79077E7BD3B3B9CF0B07802101E,
	AppleSongPanel__ctor_mD0C726449C7304BBF1233C34439A365374D7EECF,
	BookContentScreen_Reset_m391B661F3159A65FF644E6B00041035732F9CC47,
	BookContentScreen_InitButtons_m2836AC5A6E9A8748B56042529C57766D19A7839F,
	BookContentScreen_ShowRule_m047388095169354AF46B5D97B34CDF9A231A7E9D,
	BookContentScreen_Show_m77C0BA7F8023B2F32D32259AAEA52385CCF69AF5,
	BookContentScreen_InitContent_m01F0D421DF47DCD736042B3B080F73AE714648E2,
	BookContentScreen__ctor_m61DFA4B8734EEC87C3991D8E34443E4FB2AD190E,
	BookPagePreview_Start_m3011D77050A03A8BC91F68E14C5BBD4941DA96F8,
	BookPagePreview_Init_m48293F3B7DCD3130A2006A14D3B5771E5BBED170,
	BookPagePreview_ReadPage_mCFE2AED56C1D4F214C113975D4A078990CF6B92B,
	BookPagePreview__ctor_m1DAE7311F1A6B6878FFAEBCAB27A302CDBC25349,
	BookPagesList_Reset_mD4FD3F22A9F30989ED98DE8E8FCE6CCAACDC642C,
	BookPagesList_Init_m2E6EE145F716699657672B00A61BA06C04003A04,
	BookPagesList__ctor_m7E2F01746C1E643F8DE6E1A06DB4021D59EC9C32,
	BookPreviewScreen_InitButtons_m75788D5D4499188EC35C319EDE30DA84C8E5996A,
	BookPreviewScreen_ShowRule_mA40AAE0D740EBBFB4B6B5BE93FBDE4CD29AE30BE,
	BookPreviewScreen_Show_m5890CA7C6D3104B1F31027BB1E9E90474482B2BB,
	BookPreviewScreen_InitGames_m989A490B9CA935183AFA126FF1438B8378EB0E32,
	BookPreviewScreen_ReadBook_m53494A0FA0539B95DE7FCC0A0551723A473525D6,
	BookPreviewScreen_PlayGame_mF8074571D569F1123AF916E92486BBB52A14BB6A,
	BookPreviewScreen__ctor_mEC694384CE230D3FA40532F5E04078DA97106AB0,
	BookPreviewScreen_U3CInitButtonsU3Eb__7_0_mD97BB31B984B8CA66248BE981812A9F78280DB42,
	BookPreviewScreen_U3CInitButtonsU3Eb__7_1_mBFC39D416AAE8DB8D5420BB9AC058A06A2CB8F56,
	BookPreviewScreen_U3CInitButtonsU3Eb__7_2_m455F384B2FE521D14058028DB8E2E8AE9948E510,
	BookPreviewScreen_U3CInitButtonsU3Eb__7_3_m5AFFB70FDC955DA3322844B2D16D815568D15555,
	GamePreviewPanel_InitGame_mB6977834171667A954697D43A3A15F09727BA22C,
	GamePreviewPanel__ctor_m49E34DEE69DA28501AB66D62EEB78C883077EBA4,
	BookPageController_Reset_m2E727FF6BAC7BA817484DED14FEF9EEF45C53453,
	BookPageController_Init_mEF922176FE14E7954302FBFAE44D89F3D1DCD234,
	BookPageController_ClearText_m7E996D8581213D656194D4DC4CFB52081761E50A,
	BookPageController__ctor_mD7EEC0A26174A3961404FC0EF1AEA2E5C7ADE540,
	BookScreen_get_CurrentBook_mDDF3A25D782E2E9C8A31146F57DDC874BDCFE1C4,
	BookScreen_Reset_m637BAD1BDA2AEA7B027A19E7FAEEEB826002F09A,
	BookScreen_InitButtons_m597B787C07287DC745AACA267D20FCF280AA3271,
	BookScreen_ShowRule_m26A2B295C551491842B00F726AA3D28C691FD940,
	BookScreen_Hide_mBCE5A88A309666C9A6C4C8680F25A514BB1B2AA1,
	BookScreen_Show_m02AB3B5D49F61D92DDF4245A384C338911C901F4,
	BookScreen_InitContent_m03645CB16B5C065166C3211FDF72B1E6B13F40BF,
	BookScreen_InitPagesPositions_mD19929418FD5693CA5D1BDACEBAE3A5BC66EB196,
	BookScreen_CheckButtonsState_mCF5E0622C1CC17530EC6C70D53E6BCBD89F0801B,
	BookScreen_SwapLeft_m80E31D87FB64469A0E3EBA19A81B9284ACB610B3,
	BookScreen_SwapRight_mBD3074FA6A359232D71EBD848F6E5BA0BC70D78F,
	BookScreen_Swap_m87BF4DEB8D1698ECCF5F926ECB69CB3222CFB9EF,
	BookScreen_Update_mC1664C65DBBDA9E69B5ED1051B4FC1FAC2A51A61,
	BookScreen_SwipeProcess_mA557731B625AD08E4ABCC86C6DBAA797BA98CA19,
	BookScreen_SwipeStart_m919883CB81D1A2D9C189ACBAC4838FFCB9365FA4,
	BookScreen_SwipeStop_m599C4EA07C2A85E3E57372B9595E9FA02668F7F6,
	BookScreen_AnimationLerp_mEDF6C655FE6871CB0E7A8262A9E8497D35F383B7,
	BookScreen_SetButtonInteractable_mC377F21CCFD2B0C851CE4BD4C1EE03A27458193B,
	BookScreen__ctor_m262F09B2D32E61F725C9D8EE808583FAC6EB2DBC,
	ConfettiEffectScreen_ShowRule_m49991BA3CE60C66C5AA42F1B9C751CCF782A6743,
	ConfettiEffectScreen_Show_m1274B6E20D9BE2397E2549165290F2595A209030,
	ConfettiEffectScreen_Hide_mC040B28C7026918981D131CEB5651BED2A226F24,
	ConfettiEffectScreen__ctor_m658D9333446C79E9DA72DE05376C544B9257579A,
	GlobalMainScreen_get_DistanceToCenter_mC69D6ABD5AC5E2EB4E74E7D6CF3E27C93FF8FBEA,
	GlobalMainScreen_get_ActiveScroll_m1781D40978F84920924034506AF82A2427FDE60E,
	GlobalMainScreen_set_ActiveScroll_m9CE0B31C53223013EF6A61865614F38D580DF88D,
	GlobalMainScreen_ShowRule_m282561979D8FC26C3CEEC25D74274FFD271B0865,
	GlobalMainScreen_InitButtons_mFA620DD597231AC3C14324A3C7D66002D6236608,
	GlobalMainScreen_CalculateAnchorPoints_m1AADED996E711DA6034727EE9507AAF1F5F3FDCE,
	GlobalMainScreen_SetScrollPosition_mF4D13F7FADA79D2A7CD96E25752C370FBEA06151,
	GlobalMainScreen_ResetSenterPosition_m0C0D7E9904F8F72D5FD3A7144BCE1C3CB405989B,
	GlobalMainScreen_SwipeLerp_mFF06E0D32E84FC4FE787EC252CAF57957FD144E4,
	GlobalMainScreen_AutoSwipe_mC294F1E41D76D9BFF5FFC442D8F930E807371F54,
	GlobalMainScreen_SouvenirsClick_mFEE8DABB3E11F62882115E4BE640A449B21B3AFA,
	GlobalMainScreen_ShopClick_m869334180E3C7376EFE700A1A3ADBF8B81F128F3,
	GlobalMainScreen_OnBeginDrag_mEC914F613246B01D844461BC507D64F5F35306D2,
	GlobalMainScreen_Drag_m71C47D72E1826721AA3019E8D563F5F8A1FA5D68,
	GlobalMainScreen_SwipeAction_mA0CAE525D4766162521FEC37DBAC62A2D6B8549F,
	GlobalMainScreen_LoadSouvenirs_m1B18D9BB371FCFF44CF9BABD8EE72D0777D1047F,
	GlobalMainScreen_LoadPageWWW_m845C99379E87EB47690133FF124D01E82D5CEA0C,
	GlobalMainScreen_Update_m3FE7D6E6D5ECCC0F35758E77209C0AB5B1163556,
	GlobalMainScreen_InitLoadedSouvenirs_m6F4D24722200F385A2937385E775D28DB737C940,
	GlobalMainScreen_InitSouvenir_m5A847A0D25781F41717B732803D8F3546392CA9D,
	GlobalMainScreen_Show_m6586C1649180B0E0D980CAEF1CDD32737B7D42F7,
	GlobalMainScreen_Show_mC32977611DBFA8B1E7606294E8C3AE542971AACE,
	GlobalMainScreen_InitGames_m42C43ABE71A744B9EE5246CFFE7A2ABF7744A8CE,
	GlobalMainScreen_ReadBook_m68820834B6EB88848C40DBBACE435A6142DA9A88,
	GlobalMainScreen_PlayGame_mC4F7D99045B7CE25F810A1BE78CBE36A13EA55AC,
	GlobalMainScreen__ctor_mFC7354D41024BE964A1391D1A9A8C2678EC53799,
	GlobalMainScreen_U3CInitButtonsU3Eb__34_0_m2E54C78E41672E8162512826DFCA71794911FED3,
	GlobalMainScreen_U3CInitButtonsU3Eb__34_1_mBD94D4305775579D06635D9BFB027D62E8FCD66A,
	GlobalMainScreen_U3CInitButtonsU3Eb__34_2_mB11DE6CAEC99871942A3BEE750EF562C4EC7161B,
	GlobalMainScreen_U3CInitButtonsU3Eb__34_3_m159602D530F2B85873967AF99058E62B8A647DD1,
	GlobalMainScreen_U3CAutoSwipeU3Eb__39_0_m14653CDCA5FF1BB57726B608E1E1B12CE31B54F6,
	LocalizationScreen_InitButtons_mA930FDA5A6B773FB0D70C71BAAC8B37CDE9BF565,
	LocalizationScreen_ShowRule_m23AB9C73628FB0D7EF15E0FDDC35A610003E1834,
	LocalizationScreen_InitSetLanguageButtons_m1D9BFFE7072CCC12A5D8C2DFEC34EC77205BA18D,
	LocalizationScreen_CheckSelectedButton_mED78401A9B476941289E95CB03D1D97A658AEA63,
	LocalizationScreen__ctor_mE338667793BADEF6DC09DCEC050F60CF870BC775,
	SetLanguageButton_Awake_m2B092EDADD726F8E554E0C20D39AE84CA6A8CC00,
	SetLanguageButton_Init_m9A2EE998C8AEBDF94056C9815CD5B0E49A4DEBD1,
	SetLanguageButton_CheckIsSelected_m60908D50BE8181DBADEFBF60709F2A00C3A9DE41,
	SetLanguageButton_Click_mAC48343370C587D017D788026542C0E672369729,
	SetLanguageButton__ctor_m421393F7A097E54CF6D8FEC8E8C49834EB54E372,
	LanguagePresset__ctor_m585DE85CD0A3075BDD91209FA92F8D54675A71F0,
	BookSmallPreview_Awake_m8026CD4825660CDA05FEBA93FA53C9BA854E6891,
	BookSmallPreview_InitBook_m63AA09F84B34FF59DFF7421BE9C7169DA4A197CF,
	BookSmallPreview_UpdateValues_m2E1E940F38E2930E490B37EC99EBB69C19FBE13F,
	BookSmallPreview_OpenBookPreview_mA72A76A96BA12A47FA1E3842D10D114446295C83,
	BookSmallPreview__ctor_mC97307BB44408334D8BD4B5F0297D633B7EC2A32,
	BooksPanelController_Init_mCBC63B13DB8C9A0AE52CE2EC8725169E22C03918,
	BooksPanelController_Refresh_m5091393D729585F847A88367E6BF37B22E9E20E8,
	BooksPanelController__ctor_m65C8D007BE767C137E27F13C563528281B0B5E40,
	MainScreen_InitButtons_mCCF6D755DC43DC7B8732FEE12FCBECF63C88B19D,
	MainScreen_ShowRule_mA37ED6342B5A783FBC1EB23B673AAEF60061D266,
	MainScreen_Show_mBBD39021CD75F5CFA5FF3C9288A6A121CDF8E249,
	MainScreen_InitGames_m033DF8CA08FE6F8B7E11981AF69B26ADCF100264,
	MainScreen_ReadBook_m79C2CBB707B6C364BD4522C51E4C31407E62DA30,
	MainScreen_PlayGame_mD24DA6CBEB0B797DDC665ECB3AE16C9A61BB4AD2,
	MainScreen_Show_m8CF769428C3165719DB59874A690243E16097409,
	MainScreen_Hide2_m1B3844763C008A17E5A5BE6A0FD397709F380CE3,
	MainScreen_Hide3_mDAE53CDD25777EB09895B17FD53281194D3FF5C6,
	MainScreen_InitAnimatorParameters_m70D1FC4945B37D530DDD2F93ED324461DBD9F1D0,
	MainScreen__ctor_mDEB4484ACEBC62EAE5EE73C12D8025C37A69F9B4,
	MainScreen_U3CInitButtonsU3Eb__12_0_mA5DC60B2C4B6B684CE8D3B7889A5331F6BCE1B2C,
	MainScreen_U3CInitButtonsU3Eb__12_1_mBBD4F70C1503713EF12F4A46DF6CD385BE95F1FD,
	MainScreen_U3CInitButtonsU3Eb__12_2_mC4275219B7FA830BFD70C7CAFEF3293B0C87A19D,
	MainScreen_U3CInitButtonsU3Eb__12_3_m9D6AEF79C1D5F92E1A714B0C4044ECC0C6A6E946,
	ClickManager_Update_m2543286AFDEE71E29CD5A7A82CA5C5B742D9439C,
	ClickManager__ctor_m3D021C5A2EDCE75C51BA0134ED9E090B2DEA1D1A,
	NativeAdsPanel_get_Inited_mE1A6C62912AECB9B46D14C53277329F649C8E2C8,
	NativeAdsPanel_set_Inited_mC255445B322A2E090C8DF804B0C5272A849339B1,
	NativeAdsPanel_get_Interactable_m05C3037E20948362416A7C20B4CF66DA33AA0BBE,
	NativeAdsPanel_set_Interactable_mB903D8203D23D57C241038202AEC798CCEDC5230,
	NativeAdsPanel_get_Alpha_m81DCADCCF3387B15E8A60BE9280CB1C25815F184,
	NativeAdsPanel_set_Alpha_mF429D57FCACAE020CECF543B28D7B61CA509774E,
	NativeAdsPanel_InitCollidePos_mD35861FA29A772A49C530F835A6EF46183A56DEF,
	NativeAdsPanel__ctor_m3D58F1424554E03B112BABE48C40FD3C155536FA,
	NativeAdsScreen_get_Selected_m37218932829AA808F7749B618583EC5EBD324642,
	NativeAdsScreen_set_Selected_m417E55893EC15BFEEDD20E5A9FDD2572C3F270F6,
	NativeAdsScreen_ShowRule_mB88E4AACC308BD345E08ED313EC2256732D122DC,
	NativeAdsScreen_Show_mE9CD18E7396E34971884E670FF332D6C7D4456AB,
	NativeAdsScreen_InitButtons_m6EA6D0814D1D7D86C943E9960FB861090F5AB92D,
	NativeAdsScreen_InitIndicators_m8EF012E06B72EDB5ADFFA2264E9F1177EC5EC5B6,
	NativeAdsScreen_Updateindicators_m6365AA628A49E1CAD9DB27B666F1198BFA5F86CE,
	NativeAdsScreen_Update_mD4A8A296FECEDA05F586B61DB8043BFB8B081F17,
	NativeAdsScreen_SwipeProcess_m7CBB88C8859362E7DDD55C2CB6F517B6285350FC,
	NativeAdsScreen_SwipeStart_mD85708797FEEB4DD352ACD9936E978B863BA7248,
	NativeAdsScreen_SwipeStop_m73F07966CBAE93AD82477DA4C2920EE24C1307F4,
	NativeAdsScreen_AnimationLerp_m2EE85C50D0D0FBF15DE5580E76D9F48CC24316A1,
	NativeAdsScreen__ctor_m488EDA06817CA9C2F79549BAA8CA775FAB9C0BC8,
	LevelPreviewPanel_InitLevel_m4CAFA565EAE708FD8D9E335136C5929DCEAA9E36,
	LevelPreviewPanel__ctor_mD9F78DA641CDF83318F4FB926752C822B5AAD665,
	LevelsPanelController_Init_mB37E31C3C901AD4576AD36EED9E66595E3C71CE7,
	LevelsPanelController_ClearPanel_mD07329C98762879AB4EE45CB6ADF735A18BE8F8B,
	LevelsPanelController_PlayGame_m3ABC3C81AEA6B72956C4C5A0ED47E90B35B49C2F,
	LevelsPanelController__ctor_mA3B690893634CDF6336B20A02DA265B5974C5D74,
	SelectLevelScreen_InitButtons_m476B978255DFBBBC9671586421338BD74B74D022,
	SelectLevelScreen_ShowRule_mC81BB8AD7972554E842486E1B39573CB9044EADB,
	SelectLevelScreen_Show_mD7B8DCD8AD13D196093BAEF9209432AAA772D4E5,
	SelectLevelScreen_Back_mD94E7A46778C7758A82BBC9B537CE690A5E1B3A4,
	SelectLevelScreen__ctor_mA800D4B5C5D20A6CB9D32F8BEF5B1018E5F54BBE,
	SettingsButtonsScreen_InitButtons_mC2B0CD46D4A0B7B38736B8921C5E94E72B86F42B,
	SettingsButtonsScreen_ShowRule_m09F5F5C7A679A50B21E95B1931AC6492CFF47C8A,
	SettingsButtonsScreen__ctor_m9240C83C93D28D124104A7CF26C462B19C68D38C,
	SettingsScreen_InitButtons_mBBB724757FAA705385DCC764CDBF1C675F666603,
	SettingsScreen_ShowRule_mD33B83CC54DBD319ADC573C305ADCD1DE4EC4FD4,
	SettingsScreen__ctor_m0A1661824FB957CC3FEC0B710A38771DA2D30F85,
	ShopScreen_InitButtons_mECB21DCB4A44B06B8FD6162DC2C8FB6C08871D19,
	ShopScreen_ShowRule_mB81BDABD8E9D021A677DC18A8143B6157A2D470E,
	ShopScreen_Show_m63847FCD91F45410522F8F578286D12115A77474,
	ShopScreen_Hide_m8A905C44B9A74FED74A4E029C93092036A49570C,
	ShopScreen__ctor_m64C014F7CBF5FE12C521D935CF54E55C016FF9B7,
	ShopBookPreviewScreen_InitButtons_mC3C6A9DCD97152D5A7CE35986CF46C615CC56B94,
	ShopBookPreviewScreen_ShowRule_m1B58F154D8DE31560D34BD05FE8FF8782CF17922,
	ShopBookPreviewScreen_Show_m035D151D14CC52D549037E44D5DE7F6B867AF4D2,
	ShopBookPreviewScreen_InitButton_m3F05A2679E57FEDCEDCDEC403C5B313CCF62CC1B,
	ShopBookPreviewScreen_Read_m3E3C35B6D511CD126236E886FCA0145A03D61DF3,
	ShopBookPreviewScreen_Buy_mC776467A42AD3EB7C1FB286CAD4D5D160A55E743,
	ShopBookPreviewScreen_InitBook_m15F4FAC4259AFAEEBD3B79BDB5A46A81ED718E3E,
	ShopBookPreviewScreen_Show_mE1BC33956C350E2198140ABD3D0C396ED139EBBF,
	ShopBookPreviewScreen_Hide2_mDACCCBEC3A32B2F46320A33EB1F872B592A8ACC3,
	ShopBookPreviewScreen_InitAnimatorParameters_mE7F7F4764CBE71FB3D27D3E784825A02C4B71CE3,
	ShopBookPreviewScreen__ctor_mAC31DD250B23324F38F014168ACD57504CBEF8ED,
	SouvenirPanel_Awake_m11C0C645F5DE25DBE4790AE11C0BC5D53C43002A,
	SouvenirPanel_StopLoading_m4E53E9C8DD7BA9C1FF8FBD815BD8CC3CAB38F1FE,
	SouvenirPanel_Load_mF24AEAE4965F73C5867BC12B235CABA3678D3624,
	SouvenirPanel_Load_mC0D27888D841254662889331A6A64C84AE65CCE6,
	SouvenirPanel_OpenLink_m04DC7991ED6C5AC90A31B60D3F68A91671C86032,
	SouvenirPanel_LoadImageRoutine_m638E1A97C506D993F04362FD5040DF3E97CE374D,
	SouvenirPanel__ctor_m1E4D1C0B0D66EC93192BBCF871DFDD0C92DD9EA2,
	SouvenirsData_ParceFromJSON_mA586C12F6CA433FB8D60E920A7FD133DC5E12DA4,
	SouvenirsData__ctor_m11843D8F79E5BBA82A87BA2B8BEA52A744907987,
	PaginationData_ParceFromJSON_m453A11F22765695A6224F96F44087A3F3139D239,
	PaginationData__ctor_m9B45F12EC39A94245DFE34B65E7EE5E9B2CE1927,
	SouvenirData_ParceFromJSON_m80053471E25AAB87AA6C71F80291B71DE3CDE658,
	SouvenirData__ctor_m2E0728DAC746015CA76FA356FB8146586746FEA3,
	SouvenirsScreen_get_IsLoading_m44D5DCA25944EF4F43EF84D066CADE8D1E5F5183,
	SouvenirsScreen_set_IsLoading_m7C840EDE9ED0E2CD608AD247CA23213C30D88285,
	SouvenirsScreen_InitButtons_m2D9227C1B565651E246265080EB3EA0B1FFCD57D,
	SouvenirsScreen_ShowRule_m986BF9D0F53ACA2E5A2B582F7FD87C528E5DC9D7,
	SouvenirsScreen_get_HasNextPage_mE628C9F728192E737457B1348B61D9A54DC3FF58,
	SouvenirsScreen_get_HasPreviousPage_mD4E1BD2C4AC27E2B386184D3970B47BA4E14160F,
	SouvenirsScreen_CheckShowButtonsState_mC2D4DCCB9EA21879E97A9925B837D11BCDD8632F,
	SouvenirsScreen_PreviousPage_mFF8B07766628A602119AFA86743FAF3D576C2B06,
	SouvenirsScreen_NextPage_m77CE1DB4F34D90D4E1A059A16004A8ECE48E045E,
	SouvenirsScreen_HideAllSouvenirs_mD77BF9E5D94F129157E2258621C9748063560D12,
	SouvenirsScreen_Show_m57999321E3A4BFFA4DF641D1400C5797D04D850D,
	SouvenirsScreen_Hide_mF4DE824E9A5AD91A4669F6DEA1B9E928148F52DA,
	SouvenirsScreen_LoadPage_m623C4E4246E1A3D4E7B8718DE6B97A0474B5BE6D,
	SouvenirsScreen_ShowSouvenirs_m616A104CAC8B7A608CE6CF4188202A1F7443B268,
	SouvenirsScreen_LoadSouvenirs_mAB3D9004E21F0202858682E646A680B85581AD36,
	SouvenirsScreen_LoadPageWWW_m54635DDBA8567C9DC7D2FE03888D1CF30313B7E5,
	SouvenirsScreen_LoadTotalPagesWWW_m0C2405F14B741A3D4E502049B653B79343ED5886,
	SouvenirsScreen_InitLoadedSouvenirs_mCDD9780C00F5655D78E720FEF1A4724B05CDFB54,
	SouvenirsScreen_InitSouvenir_m3037073ADB738611F477BA6F755CCC97BF076AF7,
	SouvenirsScreen__ctor_mB69BF4E33BC8085C1131E5327F71FA5D81AD59F8,
	ElementController_Init_m46522E8FEE8FFD3B802E0A4DE6CF70B94B1FD39B,
	NULL,
	NULL,
	NULL,
	ElementController__ctor_m2741CE9DD3D1B3B9FF217ECBA477BFE64F03F75E,
	NULL,
	NULL,
	NULL,
	ElementData__ctor_m95BEC41782F46165CEAECE88932A74A93CA37576,
	ScrollController_get_Elements_m524C91A0E432F8955BB59EF703DAD3CF00CD427C,
	ScrollController_get_CountElements_m77C0425D4B3FAFA6BCA7D385406CB076503EE11B,
	ScrollController_get_MinDistance_m9AD91B5466612F14031E4621FB480839022B977C,
	ScrollController_get_SelectedElement_m7516811E6B14BA6FD190B5225FC374EB7037422E,
	ScrollController_set_SelectedElement_m5D2E0A4680ED388546527E6DAC13A4BFE4DCB12D,
	ScrollController_get_Drag_m42AF87D4062ED95047CF8834F7E3FCE08CF63E85,
	ScrollController_set_Drag_m8125F800243DD41936C29FDEE5E4115C295DE72F,
	ScrollController_get_Active_mA69617C3EFDD447794E82166F2661D40616CCD04,
	ScrollController_set_Active_m75FD8BD9C7573E08D6186628567CF66226F8C51C,
	ScrollController_Start_m0F7BB9EFE6E2FED3910729F583FD0CCFC499AE54,
	NULL,
	ScrollController_SetState_mFECDE0DB69F5CDDC86DEA6E492E0618F7C768731,
	ScrollController_Update_m8AB5B791EB30A39535C87E78C5290AE21B12E3D2,
	ScrollController_HandleSwipe_mEA9F4DD223E4C88F2276EA365C4F9CD4143D390B,
	ScrollController_MoveScrollContainer_mA1E53B55F73B79B31C0BA557D9E15840717BBEA0,
	ScrollController_DefaultState_m9C25694D341CAC2046DF3AFBAC4AC0B005509DE9,
	ScrollController_MoveState_mC464C80C550DD6F154DA13A6FB51F81932FB52D7,
	ScrollController_MoveToNext_mDE022874226C17756EF04D9382A60F331BEAAE36,
	ScrollController_MoveToConcrete_m4B44DAF544D6168ED541ADC93C953B26A976ADBB,
	ScrollController_SetIndexAndPosition_m9FFEF3E255CEC30D146849ED842A8C48CFF57F0D,
	ScrollController_ClaculateDistance_m0111165AE231BFC1CAD11EB23F9B8D4D4EB1E424,
	ScrollController_UpdateSelectedElement_m7AF8388A17C7AEFE5F70DF5C38458A4DB240EE55,
	ScrollController_GetSelectedIndex_mF153F1907379F8581198F6EA41785B6CA4851FCE,
	ScrollController_LerpToElement_mC790E3A6847FF3C83DE0C4C3FFC0449FEBDB89E4,
	ScrollController_GetIndexPosition_mA43B827CDDB3715E44DE53F0657B9D097098B44D,
	ScrollController_UpdatePosition_m4E8219421AFADC9ECF0B5F39503E5849F4A6D27B,
	ScrollController__ctor_mEF7841B8C2874853CEAF28FF88156BE189C0478D,
	UIScreenController_Init_mDBE33C55996AD574517FD9EBAA0B498D9B19B8FA,
	UIScreenController_Start_m9ACFE52D2D1D82F576B1F6E087E153BD3574816E,
	UIScreenController_Update_mE66631F107F8A2C0635B5CD95CCD9F98AAF3C3DD,
	UIScreenController__ctor_m130BBB871C70778511D9843C15E2D6095B7FBFE2,
	GoogleMobileAdsClientFactory_BuildBannerClient_mA8AF2737B3F09009A1404FFC62A28214B93C7C72,
	GoogleMobileAdsClientFactory_BuildInterstitialClient_mBF9E81825CF751719C531957E81D6639D026958B,
	GoogleMobileAdsClientFactory_BuildRewardBasedVideoAdClient_mFB5500581014FDE1CD791874FF848A3BCFAF66E0,
	GoogleMobileAdsClientFactory_BuildRewardedAdClient_m298968A6201AC11F7680F8C170C66626D9D252FB,
	GoogleMobileAdsClientFactory_BuildAdLoaderClient_mEC47887E28349071E4AD72A333A3E28513B10C10,
	GoogleMobileAdsClientFactory_MobileAdsInstance_mB25F2EE6B6E324BC739E76B74DF8CEC55A5317AE,
	GoogleMobileAdsClientFactory__ctor_mCCE39F56BB450D301B0F962D5AE78538C659A70A,
	SimpleEncryption_SetupProvider_mC863A52E9C7526DFF2E603FC96B8C732D2DCD2B4,
	SimpleEncryption_EncryptString_mEE6A4BBF3C99429C2FC716ABE124ABB2063AD9F9,
	SimpleEncryption_DecryptString_m6F6B5937F650C14138BC52FC2D1420DC6673F863,
	SimpleEncryption_EncryptFloat_m67214DD4F1D52A66C76ABBE941F34295BC59BD9B,
	SimpleEncryption_EncryptInt_m50E64A6BB1E8E8045A53EF9B7C7673F5A600182D,
	SimpleEncryption_DecryptFloat_m0DCADF0D1C2981796D2D18CC008997CA8BF1CFB0,
	SimpleEncryption_DecryptInt_m4D82D2479B7486926F0F9C7D0821EC494AD25493,
	SimpleEncryption__cctor_m8D7B9FA5FFCCD79515F2710F2B8194A79256174E,
	NULL,
	JSONNode_get_Item_mB1C047B24BE0AF89233BA913B7296D715ECECE66,
	JSONNode_set_Item_m9A59D1E938897BB96CDB88A894BBF986DB55BB81,
	JSONNode_get_Item_mB58A2DB9757AD59BB8D7D059C628C6A8C2D31656,
	JSONNode_set_Item_m041CFF2071DA73C5EEB8EF1E19656D288C244805,
	JSONNode_get_Value_m8D7724B6B2ABC5543A2040DC3B3225917C9F470E,
	JSONNode_set_Value_mD543D0CF821CBC592A531B6470267EBCC33B74C1,
	JSONNode_get_Count_mD636C660E848709A976C52D654CDA38AD8B60DB3,
	JSONNode_get_IsNumber_m7CD6ADBF3C7AA462BE8A7C590CF446F6BDEBE455,
	JSONNode_get_IsString_mC3F677750EDA5702CE80E204B3CC982FB2BE012B,
	JSONNode_get_IsBoolean_mE4ED5ED271F80376D089C731A702F2B7DA621AAA,
	JSONNode_get_IsNull_m043B2335CE7DED87923518A2F7A22556629B09EA,
	JSONNode_get_IsArray_m6E7E190C4ACE70EF73C6B5661F1339282B72BE0E,
	JSONNode_get_IsObject_mA64A31CFC4EE01CD89631773CE9687A40A1DA97A,
	JSONNode_get_Inline_m24EABC4751202A6E6891F91AB42C29C2AD029C67,
	JSONNode_set_Inline_mA3761A6300453FEEA6906DC9556F868DA2B5775F,
	JSONNode_Add_mBF1590D1428C7E27030229A0E89DD2DD39565FE8,
	JSONNode_Add_mB5CBFEBDFCB6751B5AA62F68795E4DF199575D8C,
	JSONNode_Remove_m2D15754A3173CA5108A343620D269E85FBD1E6AA,
	JSONNode_Remove_m865852918806702BF99DFB1D45CEF588F5073C2F,
	JSONNode_Remove_mEC2EA47AADD914B3468F912E6EED1DA02821D118,
	JSONNode_get_Children_m55A54D8FB3B8A6F40CCF0AE53B453009EEE65D98,
	JSONNode_get_DeepChildren_mEF156F8DF47F0D0A7374565ACE2853D43518238E,
	JSONNode_HasKey_m4921F263458F57F6FD46978C498A4DB2E06E790B,
	JSONNode_GetValueOrDefault_m6D65C7DAA5D98388BBD231CA2A2EA4B84ACF048B,
	JSONNode_ToString_mAC01EBB7D1362129DA253A871849052E332E0377,
	JSONNode_ToString_m9E8E9ABDDE0890D23693A750409993A36096EB89,
	NULL,
	NULL,
	JSONNode_get_Linq_m784D6111F5ACCE6368133742D1813D8E753A8249,
	JSONNode_get_Keys_mAEC584E7C7F1CCC000C51E279CEA435552023ED3,
	JSONNode_get_Values_m647ABB3BC12AFD94CE4C23531628EFC29A7981D5,
	JSONNode_get_AsDouble_mC9E31CD6FE5BD5E3A6C530431FDFA573055AC868,
	JSONNode_set_AsDouble_m358FF197E7599255C7D54081760A6BD741BF9FCF,
	JSONNode_get_AsInt_m40A4B4D25131548ABAD1B970733AA945A9E21C3D,
	JSONNode_set_AsInt_m7E6C063E6F82171FE5CC7B3C84E731663B097FDD,
	JSONNode_get_AsFloat_mC9EEF428EBB07D9DE9A1F333D69D77693BDB7F6A,
	JSONNode_set_AsFloat_mA5447099D7BA4A3A1BC5CB30DFC975538386376E,
	JSONNode_get_AsBool_m7FB9DFADD7A844A974CC3CF30C182E3BE29A0EE5,
	JSONNode_set_AsBool_mCF8C1B1F1AD2055C3113AC99AFAD75EC55874A50,
	JSONNode_get_AsLong_mAF124ADAAD7EFD2BC48FF366677F7FCA1D125E36,
	JSONNode_set_AsLong_m4CDD763725E88AAA9B846E200E12FD48C9CC54FC,
	JSONNode_get_AsArray_m7DF6AB373218A86EFF6A9478A824D5BB8C01421A,
	JSONNode_get_AsObject_m8BC40A325C24DE488E73E7DAFEA530B270BBD95B,
	JSONNode_op_Implicit_m40DE306D66005FAA0D687E7EE5A12F42B339B9B7,
	JSONNode_op_Implicit_mE47E54F268F957AB45154A95C2220957638643AB,
	JSONNode_op_Implicit_m18C4C44499594B370DD624777FFE07A175061F95,
	JSONNode_op_Implicit_mA3673761129AAC5ECBE1615E23720682B076A979,
	JSONNode_op_Implicit_mDDC8416832229DDFFC10D0277679BA61C70592B6,
	JSONNode_op_Implicit_m10EEE49EFF8F8A60DBA8E69B37C0CF0B6F3DFE89,
	JSONNode_op_Implicit_mE55A9825BA41EA4F747100D3FCC73AD487B0BAA5,
	JSONNode_op_Implicit_m02B4DE0D9177961B8B6873C4D70DF3CDDD4F5675,
	JSONNode_op_Implicit_m4B504E4E4C1728B756DF77CE4DBACB4BF3B1264C,
	JSONNode_op_Implicit_mD00FCC82967070F98EE01F1FB872C01128224D96,
	JSONNode_op_Implicit_m6C8D9D6D66148F4D1CDB43116466494932C50E62,
	JSONNode_op_Implicit_m807DE24F0FBC154EEB0AD5CB7E219D4B6BC1B565,
	JSONNode_op_Implicit_m3E839EC521DE01F951029A6D2D61E373B9820795,
	JSONNode_op_Equality_m3A6677DC55026CC5AA512264155DFE1B7063191A,
	JSONNode_op_Inequality_m0CA548C7D983A4500B0995949573E7BB5B306C17,
	JSONNode_Equals_m5861E3F57661E1A8A4FA327AA82FA1C6701BA297,
	JSONNode_GetHashCode_mD2E8E05BCE5DB4A68102650F1A73A8421CB79E7D,
	JSONNode_get_EscapeBuilder_m9F97D45BF2608A59C0C833C6369AA07F7E3CE4B0,
	JSONNode_Escape_m9EEEF7BF606E6143EB14ADC81BF29B34B0692E97,
	JSONNode_ParseElement_m2E4B9B42B7BDE678F8939103A7B16B0EDDB51556,
	JSONNode_Parse_mE166F5AC3F23BEA21858D3BF96FE725694E995E5,
	NULL,
	JSONNode_SaveToBinaryStream_m801BA0D52C73ECC56F36B5D1B4B00CD54C569C8C,
	JSONNode_SaveToCompressedStream_m47EF0165BA408039116DD9425494E275C6E44CE4,
	JSONNode_SaveToCompressedFile_mCC7A4E098A2C9E3EC0376864F21EB86BD970D968,
	JSONNode_SaveToCompressedBase64_mBB671040932E13A181B384601A7A7DB120AC6613,
	JSONNode_SaveToBinaryFile_mA67036201DC85761E63D918025560363F74F6817,
	JSONNode_SaveToBinaryBase64_m40440F8BA23F11C117A453618B36E1B91B05E2E7,
	JSONNode_DeserializeBinary_m2CB021FCF7C11A10B651DFC45E8161C6FE3BA553,
	JSONNode_LoadFromCompressedFile_m8684CBA53C1F16EDFF54BA1C0ABDA4EC03816C17,
	JSONNode_LoadFromCompressedStream_m7628D5B9C1E825BC3FD55CB076CBFA88CA5C6B3B,
	JSONNode_LoadFromCompressedBase64_mE8A5BF0AE5DA2D7D89BAC66FA467FB5F371AC6B6,
	JSONNode_LoadFromBinaryStream_m15171D43A9B1C20E75FB9B5C9A8265021C6042BE,
	JSONNode_LoadFromBinaryFile_mECE8EDF7F81DF1C69665167DE310C89D6B0F5386,
	JSONNode_LoadFromBinaryBase64_mB2B8731A36BA0653B058DFCDC82BD25BE4D5D5B8,
	JSONNode_GetContainer_mC045258C0713687F80B9976597A1F1B2F0AF09CD,
	JSONNode_op_Implicit_m9113462A7F4D578753C4BCF2BAC0573E3C7FEAD1,
	JSONNode_op_Implicit_mFDD4394339A55B3B4865D438A8D0B3B3C1B3C489,
	JSONNode_op_Implicit_mECD9914EE18E579DC4D1BD3E8896AA1C07FDDF2F,
	JSONNode_op_Implicit_m31CD7EA9D614FB833BC2F9564011C24E09065D81,
	JSONNode_op_Implicit_m62D6E2D3BFDBE38120835A337A26309C4B6EAFA7,
	JSONNode_op_Implicit_mACFF071F011662617B33FE94252FB4607F6CF768,
	JSONNode_op_Implicit_mFD11306570BA7BB3505D63EAAD6692AC7DC486EA,
	JSONNode_op_Implicit_mBA174BF9355190BEA2E7097E92A1714BDDEF0D1B,
	JSONNode_op_Implicit_m16D35DFF53015DD806F859A05788B9CAFAC3CD4C,
	JSONNode_op_Implicit_mF372B584DA87585A34A1B7191F2ECD9D76523FED,
	JSONNode_op_Implicit_mC04609A15471AC61282014B58D94B50E853E4625,
	JSONNode_op_Implicit_mC07699E85A9E1181813370FD8910D3C391361737,
	JSONNode_ReadVector2_mF8CFEBEEB5F5CE34671CE0842C0B02551FE48B18,
	JSONNode_ReadVector2_mFF88B6348B92867DA5C4251157732CAB5B5B47EF,
	JSONNode_ReadVector2_mB2870328357B8263ACCB246CD92A8A0D478378D9,
	JSONNode_WriteVector2_m788274FC2E65F87EA6C402EEE3CC53BBC7F3B16C,
	JSONNode_ReadVector3_m43DE5D4DAB69313FDABA7836772957133304B86F,
	JSONNode_ReadVector3_m3525AF93F091FE98ED5CEC950BE8685FADB58823,
	JSONNode_ReadVector3_m0CDF256AF6311DD29A996A2D16C5379941BA2C53,
	JSONNode_WriteVector3_mFB14429368B75FB3645845DB760DCCBA75D97CB3,
	JSONNode_ReadVector4_m82D5CDBA10795C70B5579D0F194EBA1C50A80D04,
	JSONNode_ReadVector4_mC3B5F4BC65CCEFF9442226420C4F412F07411E6D,
	JSONNode_WriteVector4_m60C2E43877B04836235CCCD130AA7E14B0C7A548,
	JSONNode_ReadQuaternion_mC629B1F75B005F5210406DA71AF735E79D06A984,
	JSONNode_ReadQuaternion_mCCDF5602F2AD5B02F6D87AA0EB7475D24A40EE1D,
	JSONNode_WriteQuaternion_m8A0BE1AB55D78210F61002B227FD4DDA9209DA53,
	JSONNode_ReadRect_mC04789622553132C464D6E2A23ED51EF29BA906F,
	JSONNode_ReadRect_mC4FD3AB27019DF45930FD4F63B4FE15FCD6A9A38,
	JSONNode_WriteRect_m612BE6C780A7E2E09B864A2F7A319E9B870C91DB,
	JSONNode_ReadRectOffset_mC3F47466BFAC630661DE2A223A20311BECCFC544,
	JSONNode_ReadRectOffset_m4895D264D617FB2D4BFAC134E2315581E75F86E4,
	JSONNode_WriteRectOffset_m38055DE4B945CFCFFDD633AAC49EC012CB213E2D,
	JSONNode_ReadMatrix_m3F1D25649F13760AD415BA9545B2ABAC46640CC5,
	JSONNode_WriteMatrix_m032272CAA06ACF4F50D63D95D7E4AFAE4CD385DB,
	JSONNode__ctor_mDA1406BFCE5698F9E1B5C45D166CAEADF75F155D,
	JSONNode__cctor_m790455736E92967EBA6E8FFA0B1362926347DE4F,
	JSONArray_get_Inline_m03E7B2428299099D1B36AA5E73178F483DA259A9,
	JSONArray_set_Inline_m4536E214EBF308E1B0AC178C55E3CC74BBA6E03B,
	JSONArray_get_Tag_m51CD8C530354A4AEA428DC009FD95C27D4AB53CE,
	JSONArray_get_IsArray_mD6801BD0AF511E59314A86453C4CAD8DBDE72E61,
	JSONArray_GetEnumerator_mB01C410D721F8B673A311AFE4913BD2F1C8E37F9,
	JSONArray_get_Item_mD189F066FE043F56F6E93A83A60D924DD847BC15,
	JSONArray_set_Item_m3B6450ABE093284900E2149194902DBF55F9F5E0,
	JSONArray_get_Item_m60A4763A82D74D05A95B9B18491B15A94B3AE815,
	JSONArray_set_Item_m47ABB630C7E81AB9ACC8542DCF2257B93527A383,
	JSONArray_get_Count_m82BAED8423A11B317E4570FD9B95D4DC5C761E15,
	JSONArray_Add_m66FC2CA7BCFB73690DD520A8F084FC48148412AF,
	JSONArray_Remove_mE574BD569800529B233BB15FFCB34F6E1632E004,
	JSONArray_Remove_m36719C3D5868724800574C299C59A3398347D049,
	JSONArray_get_Children_mB4825990061A2E134CCB2226C37015379F9D3813,
	JSONArray_WriteToStringBuilder_m4F59ACBFEDB5A8D24423CA77FA960190A2FA6343,
	JSONArray_SerializeBinary_m00A6632EEB6BD975D622461EC2DDDDDA0CD3DB7D,
	JSONArray__ctor_m419E98169E6E82840EDE150C26627DAD3BC02447,
	JSONObject_get_Inline_mBD83D2F3C131B9FD58B0F3986CD918A43351F885,
	JSONObject_set_Inline_m462E287E1C98354480FF58056463B67C19884389,
	JSONObject_get_Tag_m24A4D8474C6803E5B36B9C7B16CC35F48C00C486,
	JSONObject_get_IsObject_mFEE416FD7C685628DCA3E797F81BD27BC1EE8348,
	JSONObject_GetEnumerator_m89934764DE9D9DB8D007DE2FDB956B35A483CE2A,
	JSONObject_get_Item_mE598F65336946DC41AB6F406AC57D2A54E457094,
	JSONObject_set_Item_m9D055BC7B0CD7C57CD0F317EC412CF75CD70F287,
	JSONObject_get_Item_mBC3297959D58E83AAB9F274DF9CED1B39624D9C1,
	JSONObject_set_Item_mD7301713F70EE5DBC64223FCBA6D5C1680390B16,
	JSONObject_get_Count_m48D5936D2F942420881CBD8F8553E951894FB7FE,
	JSONObject_Add_m13B3F60A99B68E441677FB6CBA55A24C0E8360CA,
	JSONObject_Remove_m9E4AA7514241F96E7996D4943AB92C20E94801AB,
	JSONObject_Remove_m6E8D738F1570D159422CD7E7447DD133DB0DBEA2,
	JSONObject_Remove_m9943021B70272C24F3FE7D87421F3DD6E1EADB15,
	JSONObject_HasKey_mB270F9CDD8F8F4C6AE2D9DB57270F50248BD49D6,
	JSONObject_GetValueOrDefault_mA1655EE4E0926B292C715B26F788A992EA1F0C6D,
	JSONObject_get_Children_m395D5E98AF99D012572A6BD30560FFC988513A94,
	JSONObject_WriteToStringBuilder_m00318F24352A12B9BE48DCB37144F61E9A9E7A80,
	JSONObject_SerializeBinary_mD79062C206D52C42C0E2740B5F89719547EEDF1E,
	JSONObject__ctor_mF56546672BF37A9578123B0629537C3AC53CB3B9,
	JSONString_get_Tag_m04F409B5247C8DCD38D24808764CF24D869E6185,
	JSONString_get_IsString_m41461FC38EC677B2A2A4723ED126565DACE3BBF9,
	JSONString_GetEnumerator_mF8D0B322B157094DFDC176D6B8BD0E2C4BE626E4,
	JSONString_get_Value_m2BAAD8CD245735A369E1D5A04E5AC99FC842144F,
	JSONString_set_Value_m1D74B9A40B791CBE98B9238BE557C27BD8817FE6,
	JSONString__ctor_m688A1C2FCB9A049611A22A577D0F5F929DD16DC2,
	JSONString_WriteToStringBuilder_mEE202723B371E8D9398588A41D7027FEB98F138B,
	JSONString_Equals_mC77793855A6E9F114EAC12AAC463851ACCB45D6E,
	JSONString_GetHashCode_m89888E65EF5DEE90FCCC9329B1DF0530A9D2330E,
	JSONString_SerializeBinary_m4614AA84E91FFF710A5AC1ADF43980213513F5BD,
	JSONNumber_get_Tag_m5810C73995BD124FB40ADDFEB7172AE1295D3C3F,
	JSONNumber_get_IsNumber_m3338ED62D7ED2856B1935977E01878B3D987C840,
	JSONNumber_GetEnumerator_m3DDB7A5F04D7C6C314D6969ECDB3314385E0F8C0,
	JSONNumber_get_Value_mCDE93C672C6D1E8603CEE081B787653116F0C5D1,
	JSONNumber_set_Value_m2FA9BB60166964B7571500874EDCE169652B55C3,
	JSONNumber_get_AsDouble_m904B18C9A5EEF144DECE1F977D3052D133BAD259,
	JSONNumber_set_AsDouble_m59A02A71F416EE06E82DFC3286973ECC6E883F8B,
	JSONNumber_get_AsLong_m061F72F7488EDFAB69FB74EB03E37D7E396FA29F,
	JSONNumber_set_AsLong_mFDFD4785E74F3B6A4B57B88AF178388E304AECC5,
	JSONNumber__ctor_m3FDBA525AA84C626AA138395026B0C3AFB16FB4D,
	JSONNumber__ctor_m344B7D623878EE8D6249F8BF36B9BF78DE8F55AA,
	JSONNumber_WriteToStringBuilder_m81EFE52CD14E8796D39AA681AB060BA6008FE572,
	JSONNumber_IsNumeric_mFCA17E38F4DF08CD58BE8E73D12950187D76737F,
	JSONNumber_Equals_m7422BF9055C6B4FD465DE5C75ADD94B948DE5F06,
	JSONNumber_GetHashCode_mB7291C3F22A8DF1F87D50828A431D283120730EC,
	JSONNumber_SerializeBinary_mEED3A5E999A683B6E7BA8A7B9CBC5C302F0BBD8B,
	JSONBool_get_Tag_mCBABB544B93C712D3F57CC8FBEC49260FEA9284A,
	JSONBool_get_IsBoolean_mCDD8754F4069AEBBBDD72335239B96DF0AD642E7,
	JSONBool_GetEnumerator_m4B8C0CC079A97EB3B0331ECFFB15F7DD0AB7A180,
	JSONBool_get_Value_mF541D03698837F4C55687CF378DB66D6B00BC674,
	JSONBool_set_Value_mE16685714754D7DE0E3F743629DEB474B0EFCD5A,
	JSONBool_get_AsBool_m39BB40C9E1DBD2670BD855394E331FFA55C12D26,
	JSONBool_set_AsBool_mDD6055ECDF4A1CF22B2D65D636356CF99142BFF9,
	JSONBool__ctor_mB88FECCC74D382AE4A348C49AFB4E5C448AE8A87,
	JSONBool__ctor_m52AF787FC93A51B469544D2DB188050518F3D742,
	JSONBool_WriteToStringBuilder_m4E0D9DFB665C9F9EB27B9FC0DA6083196D23CF5D,
	JSONBool_Equals_m2DB3C7743315F4B0FA9F14D443B7E58DD19EF0AB,
	JSONBool_GetHashCode_m1D7BA88B9CBD6B5D399DBDC2A7886B7AA96BA618,
	JSONBool_SerializeBinary_mE1F815089714C926FF51210566CA6E902FE776A4,
	JSONNull_CreateOrGet_mFCEA43022679C084EA7BEB23AD203E1B9B33E1D7,
	JSONNull__ctor_m8AF0B0484A8A9FC431DB9B6883EEF915975B5B20,
	JSONNull_get_Tag_m498F7F5444421EDA491F023F0B76AC1D1D735936,
	JSONNull_get_IsNull_m9C8DBDFF050BF6A89FDD24043B8A1EFF59D8ADB2,
	JSONNull_GetEnumerator_m390B90BACB37AC50B726CE28D07463748A202A79,
	JSONNull_get_Value_m2EA54A160B17857C04FB1910DB687A46D8E7E857,
	JSONNull_set_Value_mABE2A52BD423BFC85745E40DEA74F9F7CA7ABC54,
	JSONNull_get_AsBool_m78D9E804A03BDA154D1DB75AE32464E862BD33F2,
	JSONNull_set_AsBool_m38D6D1A90957947C3EC3A36AC2E58C827CEEF265,
	JSONNull_Equals_m21EC18AAC8A2178A6CEF6F64DB7B543B98E457C4,
	JSONNull_GetHashCode_mB20A1C1F9E89AF788BFF8678C06154C687A93AFF,
	JSONNull_WriteToStringBuilder_m493344BBA7539C1BFEE6D6874484201D2133CC18,
	JSONNull_SerializeBinary_m676A4B3E89715D9AADDF18FA5E5D374C06EF00A0,
	JSONNull__cctor_m9DAB4F5E47D29AA56DA3E8EF2ED598C75D5062D8,
	JSONLazyCreator_get_Tag_mC1AC02C25A03BF67F0D89FD9900CBC6EEB090A5D,
	JSONLazyCreator_GetEnumerator_mDAA175BC448F491BD873169D142E119DF6733ED6,
	JSONLazyCreator__ctor_mA570856315FC9EEEE91DAF46A48EB14C6826EC4A,
	JSONLazyCreator__ctor_m9CDA16B464A5E27785A473454576FA84E46A073C,
	NULL,
	JSONLazyCreator_get_Item_mF6E7B8A894C9EA1C74F824B0A2A7C87E0C8FA7E6,
	JSONLazyCreator_set_Item_m499E09B3FBE0202D91CE3CFD6BF358AD4ED04CBD,
	JSONLazyCreator_get_Item_mB8E238EEBD247A0812FE72F3A4282ECDCAC03E3D,
	JSONLazyCreator_set_Item_m3D3E4624F5FE38306E1B4F3E920DFBFCA7F436DA,
	JSONLazyCreator_Add_mBFCD7C0BCB389E8A82D9215DCF56160C5FA8C7D9,
	JSONLazyCreator_Add_m8F2A319671C485DC69149CAB865561800D78B2AE,
	JSONLazyCreator_op_Equality_m067147F94A931909B66E17538F5260449CE37E35,
	JSONLazyCreator_op_Inequality_m8C8F222FA5074241144AC5687C6C366CB1381474,
	JSONLazyCreator_Equals_mAF996627BB20C25E8498ED9CDABDF356083C2E5D,
	JSONLazyCreator_GetHashCode_mB426CFC7DAF4BF8840B2D6FB42C0A32E2E24E467,
	JSONLazyCreator_get_AsInt_m2386A4715579021A9945D3A20A592F9DC11F8B41,
	JSONLazyCreator_set_AsInt_mD0ADB2CC1876C83289C11C423CE4D18BD265B02F,
	JSONLazyCreator_get_AsFloat_m64B9AE4A38878105DC50B040593E18DF635EB299,
	JSONLazyCreator_set_AsFloat_mDD395E8C6D3F5BACEE1B8FBC51EEBC6539673CA9,
	JSONLazyCreator_get_AsDouble_m98C72926205FC366C09E20FA8A76F072194CFE00,
	JSONLazyCreator_set_AsDouble_m36E2A3EC65F2A7322E897FF97F9D5332B67FF130,
	JSONLazyCreator_get_AsLong_mE9FA02797096C099BC226988CA5C9220A2D00FB4,
	JSONLazyCreator_set_AsLong_mE6DC7B42DF99DC9128EFFBB044F8604F95D41476,
	JSONLazyCreator_get_AsBool_m6393AB23401275975AB2E6663FF8EC904648A0C7,
	JSONLazyCreator_set_AsBool_m00EBC804C6B2847280FBB7D07D1A7615BFFE8AD0,
	JSONLazyCreator_get_AsArray_m7ED16496F0BC83E265C51066E586108D22850C5D,
	JSONLazyCreator_get_AsObject_m49378AF7ED532AAB2664C02C21E537630528B9C2,
	JSONLazyCreator_WriteToStringBuilder_m7092C7335B29323434A169BB19440B45FD62DA34,
	JSONLazyCreator_SerializeBinary_m1B326A3B5172608A297970C38F6C1CE6DA8C6BCB,
	JSON_Parse_m76895CDE48710A8477CB7E932635FFFC3B0BD5AE,
	ButtonAttribute_get_Name_mF7725CF6B38706C1C0F6342A023062E3B77D446E,
	ButtonAttribute_get_Mode_m5F60CB7BFB8BAF1DE24DBD937FBD475FB310E86B,
	ButtonAttribute__ctor_m759C496E4B71385B76C568C729B6E841D74FB6E8,
	ButtonAttribute__ctor_mBBF697374FA5A534874A0BC9EEE3241AEA3EE623,
	ButtonAttribute__ctor_mD688CA6FFC6CA5FB2F616B7FFF1523A992F0370C,
	ButtonAttribute__ctor_m44B86987627520D9AC2E02E5DDFB61CAB4AC5722,
	ButtonsExample_SayMyName_mC4E0C0C9EECCE2A82441B74AAD62AE0989436AF1,
	ButtonsExample_SayHelloEditor_m941661705AE6D661DB2593A015724410FC28C64C,
	ButtonsExample_SayHelloInRuntime_mE617F877A04E86615BFC6E37619265B440F4A16F,
	ButtonsExample_TestButtonName_mD0065BEEEB402F88167290EA4CC4194C9B9613C8,
	ButtonsExample_TestButtonNameEditorOnly_m3487DA3DE75C25CA1C77F6927A2D507A750C5F6C,
	ButtonsExample__ctor_mF59A3B0B71987AF3410778FBE766AE778E72FE83,
	CustomEditorButtonsExample_SayHello_mD5A217BB8B801C976BC381A301F7DA6D5224C483,
	CustomEditorButtonsExample__ctor_mC83D9B21212851DBEEDFE69ACC9E3FC801DFC0E8,
	UIRotateObject_get_Curve_mB5AE3ACF2EB4B86FAD61B3D650E89476F7914B01,
	UIRotateObject_set_Curve_m40CF0BEA12FEAA5FF4DE4DE0F06053CF7CA42EC1,
	UIRotateObject_add_startAction_m3CC8E6D28C3E90135BB1394FF6BF8B21BD55C585,
	UIRotateObject_remove_startAction_mA4C3DA5603DC70AF008CA13D6C157F93AC70253A,
	UIRotateObject_add_finishAction_mF98DFABF410CD8DE4E79B5F37014DBA571874C6C,
	UIRotateObject_remove_finishAction_m4D698EF2F803F12B2148220A2D47F84DB1BAAFA7,
	UIRotateObject_Awake_mD8327B0BF909A88D4494CAA44CBF3E8404DB9619,
	UIRotateObject_OnEnable_mEF506152B6D694B0465734881AEB2BC3B05988CE,
	UIRotateObject_StopCoroutine_m008FD2E153BF8AA0717E5CFC8BDE29506A2D2867,
	UIRotateObject_StartRotation_m1B3E8C6FCF897367BCC55BC6B61CBD977094183D,
	UIRotateObject_StartRotation_mD8EC12A40EFD50EF86276CC33BD4E4FF2E0696C7,
	UIRotateObject_Rotate_m0E0043200519E676796BD82B183222FD58002AEB,
	UIRotateObject_RotateLoop_m4A706C05451677CDAB0934C198B5212C882BA885,
	UIRotateObject_OnValidate_m6D842D8F3940E31D8CE6C1AE4ECEE54FE9C20DA4,
	UIRotateObject__ctor_m21B38074A2ABE379BF52B2697202CA832A602661,
	UIScalableObject_add_finishAction_mF27D4C5B5E241D20157DAB7F6712DBF84ADBC877,
	UIScalableObject_remove_finishAction_mFA146BF2630633853CD0D28BA2B0024B52C57FE8,
	UIScalableObject_Awake_mB26F4F6D852F8916D8F0949FFC033307840476F9,
	UIScalableObject_OnEnable_m3E1CE351D3DE9C547E968C12395D86590BB0DC81,
	UIScalableObject_StopCoroutine_mFB9FBBF0D702528505506D6CA4D39B9A69405C50,
	UIScalableObject_StartScaling_m08A0DB58B07B098A8D235B4D81BC7F6E6A536951,
	UIScalableObject_StartScaling_m70B9F1FC2030D4918FB6167C91D35A5F340E0CEF,
	UIScalableObject_Scale_m921CE5B6F5B470683C24B7EC28ACB80C747A5BBF,
	UIScalableObject_ScaleLoop_mB6F450C4A0D67ED0A0D4EF0AD53B197E71B43046,
	UIScalableObject__ctor_mE7FA08AE154F0CFF9CE8560BCA39D6B9623BA670,
	UIMovableObject_get_CurrentDuration_mECA77F79D9489E7E442D6BAA44094093E948762F,
	UIMovableObject_get_MoveTowardsSpeed_mD203795C17E7FA8918F5445BC79A4CFC019AAAA0,
	UIMovableObject_add_startMove_mFE47D3DB4B82524EC7B354B03F9C76F77A2E97ED,
	UIMovableObject_remove_startMove_m05F82C82FC799675B3368F3E1B4836886C468527,
	UIMovableObject_add_stopMove_mD2DFE25A8DFE82FE7E877F7E3079BFCEB9641BF0,
	UIMovableObject_remove_stopMove_m494E89320A68C602E7CA61FD09902F66F00FD322,
	UIMovableObject_get_FinalPosition_mF39BBDD1D7F50F3361C5CDB47D8481E9348811CE,
	UIMovableObject_Awake_m0B28E3C7C0EA12D04AD73006AD12422312586E9C,
	UIMovableObject_Create_m7BD079B32A5B847F2E1D545F10C4CE555CD69BB2,
	UIMovableObject_Create_m2A88BD5AE9274AEB81CBC39F2A79EF26326D72F5,
	UIMovableObject_Create_m6CC0D6F90B9AA0296789537E1E7412B34077396A,
	UIMovableObject_Create_mB10DADB394CDC48B71CDD54FB1153614759DABFC,
	UIMovableObject_StartMovement_m97AF06C08D023888457B84BB5793F42C2908C00A,
	UIMovableObject_StopMovement_m7FA4C1362C4EB23FCCBCF6294E2D76A78B8CE2B4,
	UIMovableObject_get_IsFinish_m698DCE6D5666A48DE91C9A10D4CCAF6DECDF0722,
	UIMovableObject_Move_m0C215DA795627A24E53E3FBDEE2C774B9606C57E,
	UIMovableObject_SetPosition_mC69F9C8AE2B30332CAB004BF7A342B84DBE119A1,
	UIMovableObject__ctor_mE4DFC347F6C48CEBA65154A8378DC2EF52C25C16,
	UIMoveObjectToPointController_CreateMovableObject_mC6F1C4D3B25A40CD0E4B5BEF13E53C949EBE3E6D,
	UIMoveObjectToPointController_CreateMovableObject_m510B7D8878201C19512EE46D92EA69A09870BBDA,
	UIMoveObjectToPointController_InitStartEvents_mCA94CD706E8EDB2E7965C8290E415AC0C2B3ACCC,
	UIMoveObjectToPointController_InitStopEvents_m77F2D8CD49D05BB5AA93CF0384FB069F34B38664,
	UIMoveObjectToPointController_InitPositions_mDF68CBB9B163D0F623740307712C9631E52B24E3,
	UIMoveObjectToPointController_StartMovement_m0AAFC5563FE70DEA202571AB7A28A5AF3BD728F2,
	UIMoveObjectToPointController_MoveCoinToScore_m64C03844127FA2A26A0B81FE965A310860FFF552,
	UIMoveObjectToPointController__ctor_m3D9959CE0D5108F39AEAFACBE1D7F5C4161C3296,
	NULL,
	NULL,
	AbstractScroll__ctor_mC935C38D5DD3B0BECC1736AF8EE5FEB364280AFF,
	ScrollElementData__ctor_m45660941DD7309B83F569D9E80E9C843996CADDF,
	IElement_get_IsSelected_m0A39C1E8DF628F2C4E26449E19235BF6F1F4652B,
	IElement_set_IsSelected_m6C8BC8452C4F36F18F2ED1D6117196B0CE0AFBFC,
	IElement_Init_m8B3C0A77850DFCCE979C36DD5185951781840F91,
	NULL,
	NULL,
	NULL,
	IElement__ctor_m32DDCC7F6E8C214E3D3101A2C8E5CDB247DC23AD,
	ScrollElementButtonController_Init_m7BCBD96360E11A2E50841959A5EB6AFB697D18F7,
	ScrollElementButtonController_Click_m0347A532C48F7538C76D1DC8E89C47F58B0A18B8,
	ScrollElementButtonController_Select_mA50C374E4B60DB29CA8134DFC80A3F9412F5EF45,
	ScrollElementButtonController_UnSelect_mC59037156AD407C7821BD38CB7A2AE90DD590B22,
	ScrollElementButtonController__ctor_mCAF90DA09B3B7E4D40F172482B8BCA43567026D8,
	ScrollElementExampleController_Click_m246511A2BFDDDB07AE2B83B4DDB421A7A152347F,
	ScrollElementExampleController_Select_m56F810BB02473138B5044780BB33DBB147598A0E,
	ScrollElementExampleController_UnSelect_m27536927689A236F98C13EE6FB92B2B5D26E86FE,
	ScrollElementExampleController__ctor_m17F51D9E06A8642C140934ADC5EE6FD090E33D1B,
	ScrollController_CheckComponents_m49DD4F19A40010EB18AF48B09C5EE70E1EF167F8,
	ScrollController_HandleSwipe_mEE12D5F0C32D86F1EF21A75A205A952EA5331A97,
	ScrollController_DefaultState_mF2C5245E2AC2B3562C2FCF2CC8E24159F35888F3,
	ScrollController_MoveState_mD3519B1F0ECD7844B962C80605D80489685E2D0C,
	ScrollController__ctor_m40A0E554998398C3F444174D28AC98BC6F179A1A,
	ScrollExampleInitializer_Start_m10731D246D0F216AE1A7A96846A24306E725263D,
	ScrollExampleInitializer_Init_m8F8D0B0BE0BCCA49C2AB1AFE189779560695E697,
	ScrollExampleInitializer_OnValidate_m05D9B9EB3F60AD15995D49BDB5B5AE26D42DE73E,
	ScrollExampleInitializer__ctor_m21DF79F062B0BD380DEA34692690A4BCBB612E24,
	TriggerScrollController_get_Elements_mE4A8F791B934FFF6DB18AFD5E25AF66DB08776E1,
	TriggerScrollController_get_CountElements_m1CC2C74788E0C2CB8B3B0C857464B21A42157311,
	TriggerScrollController_get_MinDistance_m3F23FFFA24E14D211E1E0E25975D1E4511C4C0A0,
	TriggerScrollController_get_SelectedElementId_m481FFA2596F23F6B5CD91A2FC72CB540E1C2624A,
	TriggerScrollController_set_SelectedElementId_mD591358BCF464AA0A159D11659A29E7A339FA754,
	TriggerScrollController_get_SelectedElement_mCD3957BC3C7A5CDB1BFE1D702FCA7358A91B34AD,
	TriggerScrollController_get_Drag_m6A2950AD1EB59A1D679A0EB6F0E619B6C8152812,
	TriggerScrollController_set_Drag_m8067BE708B2B253C3A17882B2E339A61C6393E47,
	TriggerScrollController_get_Active_m862C2798610A6FEE9B12CB4535BA40C08FADBD06,
	TriggerScrollController_set_Active_mF60A20F04970094925D9147DA75252836304BFCD,
	TriggerScrollController_Awake_m628FA786CAA88DF43C11572B066029FBEAB29A84,
	TriggerScrollController_Start_m92229D42DDA141F47BB2FE1326C6F6B5E4087A07,
	TriggerScrollController_CheckComponents_m8AC90D8B264FE6B5A5449AA6632597A511CF2C70,
	TriggerScrollController_CheckImage_m17AFB7A9202D85FF5BC89CE12722F96FD9EECF7D,
	TriggerScrollController_CheckEventTrigger_m99C8A8A65A28FDEAE9FB9E8B42A4EADF1339526A,
	NULL,
	TriggerScrollController_FixedUpdate_m6541420A12F484EB3E26D182B8129D498EE10E65,
	TriggerScrollController_DefaultState_mD3399393E5B650A10341478ADC180EB8385D9A99,
	TriggerScrollController_MoveState_mB35C1E9AA92ABD64F092386DF53A66E4EF310556,
	TriggerScrollController_MoveToNext_mE23337021EAF800F4BA4B5747E5F3836B88F1E1B,
	TriggerScrollController_MoveToConcrete_m0723CDB70A87AF4D5FE445BA8D357E06CEED5161,
	TriggerScrollController_OnDrag_mAD3DFDFF6C340C96A10B2603580CFA3ACB66EADA,
	TriggerScrollController_TouchDown_m6BB0417DF5914EDE0407F6F2EDE62501EC686D0F,
	TriggerScrollController_TouchUp_m3003243DB76931E436C20905167DFF9FF89771B7,
	TriggerScrollController_StartDrag_mBAD1C9AAE2551D52735AE261A2A621340C150F86,
	TriggerScrollController_BeginDrag_m2AA63696C9A6E8B5EE02D3A8F9AF4CC7EA8B0906,
	TriggerScrollController_EndDrag_mD551842CFBE25C13DFADC4F6674006C501A3562C,
	TriggerScrollController_MoveScrollContainer_mC3C2F5744F62CFE89B44C9C47EC26DC853D722D1,
	TriggerScrollController_SetState_m4881FC003EAB2E1B5CE20BF996CD94C45390A8F2,
	TriggerScrollController_SetIndexAndPosition_m412D0719B9A47B345190AFDA07E2B1DA1442BB26,
	TriggerScrollController_ClaculateDistance_mEFE9EC7843ACBE526FFEF3AD60821EF9B26ACD4F,
	TriggerScrollController_UpdateSelectedElement_mD1DE1A9306718A5EC779021754FD688A96274663,
	TriggerScrollController_GetSelectedIndex_mF7CD11EE645B7A8EC703B78CFD32A4C348AE46AF,
	TriggerScrollController_LerpToElement_m9F61BDF5DD05E9CE149C5F46C7A7E1F5B69CD4C7,
	TriggerScrollController_GetIndexPosition_m796B0FA0D1A39BB9171999B71E823BD90F4DAD6A,
	TriggerScrollController_UpdatePosition_mFE69FA2729B98AD1F90B4F7494F11BA2A1B0E9D2,
	TriggerScrollController__ctor_m7BF8CFD7E74CFF907C256D8F32418CB85BF9422D,
	TriggerScrollController_U3CCheckEventTriggerU3Eb__38_0_m9188476274C4673302665F482BB790A6355D167C,
	TriggerScrollController_U3CCheckEventTriggerU3Eb__38_1_m5BC46B320B9104F72CBC76FB01B8933D644E1BBF,
	TriggerScrollController_U3CCheckEventTriggerU3Eb__38_2_m074165D1EA30BFC7F1A392CCD33639EFDD025EE4,
	CoroutineUtility_WaitAndStart_m96BE4B6BCF6F0D5DFFA971D00575B7B14C4702AC,
	CoroutineUtility_WaitAndStartCoroutine_m9505F1118FC727B5690979B36B6A88B2EAFCD080,
	CurveData_get_Curve_m407AED502C77E75FD1B81065397B557240B122CB,
	CurveData_GetValue_m35BEFF2CD70F711196628D9792B8055DE2E0FB0F,
	CurveData_GetDltValue_mBFAA6440789AA4794AFB5740E0CD290F6B74BBF3,
	CurveData__ctor_mD259DA00F28DCD22F318C47D75FFA466EAE0D26A,
	CurvesData__ctor_m88BF4709A62ED5A97CB24607C7539B0406ED1AF2,
	DailyVisit_SessionStarted_mF9AF2A53086120AB1A1A4BCAD12763AFCF25319C,
	DailyVisit_get_NumberDaysVisited_m9F1B6F6367AE1B89EAFE5670D2E7C4BBFF822C6E,
	DailyVisit_set_NumberDaysVisited_mCEDC1B088E1389BDDA81F97DE2ABFCD07B4A2BA4,
	DailyVisit_get_NumberDaysVisitedInARow_m0CFA417FAE21E0E513237BF853211F04D215AF28,
	DailyVisit_set_NumberDaysVisitedInARow_mD561A63B1E7134277B9C9947A723B0863DCE058A,
	DailyVisit_get_NumberDaysSinceTheFirstLaunch_m7BBA90613B773B2546C7DFBCD6FA0BC8AA761058,
	DailyVisit_GetNumberDaysVisited_m21785AAFD6E9F25D359AE44DDD6E71EB03ED6C6E,
	DailyVisit_GetNumberDaysVisitedInRow_m77A11F16D4BF235B9AD4A531A90BBEF8FABAFEBB,
	DailyVisit_GetNumberDaysSinceTheFirstLaunch_mE6E0CD63DF6525DD0CEC91C3B19E3B28CF87509C,
	DailyVisit_GetLastVisitDate_m775B0B4B019BDF7A29D5F110590625BD436AF7C0,
	DailyVisit_get_LastVisitDate_mB38A9E77167D8246065A31E714D48C1E0CD97387,
	DailyVisit_set_LastVisitDate_mB1B3B146AC9B2E0649B4FBC4A3D6847C6470D13F,
	DailyVisit_GetFirstVisitDate_m7CBC1C122B5D41BE52A0517744CC279109B866C0,
	DailyVisit_get_FirstVisitDate_mCA2A6B38293AE35B275ACCC386875DD8390D1E95,
	DailyVisit_set_FirstVisitDate_m1DDDAD166C73838466C550653D872B250C4E8514,
	DailyVisit_get_CurrentDayType_mE47A2B1972C047BEF61F0EB64D4345DA41DF6E21,
	DailyVisit_get_IsFirstLaunch_m1D90519EA6D60EC309C8411E688D515FEE0D8F1E,
	DailyVisit_InitDayType_m146DADFE0ECE078FA01ED01CD13206F1BC1AF231,
	DailyVisit__cctor_m2FF920EC1AFB60B65CBB277B53517FEF1E2660F5,
	TypeEditor__ctor_m2F9C442469687F8151855C4C73F1DA66210CD2C5,
	ButtonAttribute_get_Name_m2F4A9DE7D28C840EB14F123622FE547226053E2B,
	ButtonAttribute_get_Mode_mDA1AE218809EA6C6C34FA5C76B8B9C6C800026BB,
	ButtonAttribute__ctor_m2E636AF0841414A4F34DED0F0BCB83065922752E,
	ButtonAttribute__ctor_m59F3A244ECA19241994AEBB13377772C4D0917F0,
	ButtonAttribute__ctor_mB97F286037008F315D2C4BB5C2172BD489109F8F,
	ButtonAttribute__ctor_mB72CED897A81108626B5D1531EF120D5EB856AD1,
	ButtonsExample_SayMyName_m7989BAD34353A21297191C5042E9AF1A82987177,
	ButtonsExample_SayHelloEditor_mC94A22279F120FDF2597EDE5B86C12B3911B2F80,
	ButtonsExample_SayHelloInRuntime_mCD7660785CA16916846594E5A1F2639130F220F7,
	ButtonsExample_TestButtonName_m5497ADEB0DAF81DDB259A7126D543757BA38648A,
	ButtonsExample_TestButtonNameEditorOnly_m591681CE3FE5D1CC75E375BE51DB52235B30B083,
	ButtonsExample__ctor_mCE016453E8A7C762432E180F069EFCB95CBA9AD0,
	StyledText_SetColor_mCB590BEAC07153907E51EECD3C7EB629E14479A2,
	StyledText_SetSize_mF6103AFCB9C1CCEB3AD637FF1F36DAA1A58F8039,
	StyledText_Italic_m28EBA602DF2A32E4361A3451ADAC4B6EF8E8ACDA,
	StyledText_Bold_m65F145E7BAF0EE5B2D8D74D1225B50FD6E38DD45,
	StyledTextExample_ShowTextInConsole_m7B675053055A5133BEBDF78C705200B866014B5A,
	StyledTextExample__ctor_m5838E571141CBA2F210576993BBCAF2B0F0D0D72,
	VectorUtility_GetRandom_mABC347C545B92D7AAF98B941402C13CAB81E28F9,
	AdsController_get_ADS_m30C7641876FC935617173AD0C1A0568BC50154E7,
	AdsController_add_setNoAdsState_mEB1D4D648AE5F4A82C5207594BE359886AB4B003,
	AdsController_remove_setNoAdsState_mEA41239F92A2DE34CE1C54DF673EC77CAEE8BC48,
	AdsController_get_NoADS_m66B645C8EDE53730C42B8A404062039120715C1F,
	AdsController_set_NoADS_m3E6E5962118629AEC9A1D9CC44E9F7B9234BA7E0,
	AdsController_Awake_m7AD9333777EED8BEF4D23E72EF362B27FC96B9EC,
	AdsController_Start_m4A8CC725484C29E2BE7D8BCDE88E9EA97FD5DFFF,
	AdsController_SetNoADS_m1D74E3EE50F44778168312E5DD5169D6BE6A11F2,
	AdsController_InitAdsManager_m9D0A6E06BCE8D4F386E769D1162935EE5E70B203,
	AdsController_DisableAudioListenerOnAds_m8F4B628CE6EC45044C5E7B5A96C8F05A7D80BA8B,
	AdsController_EnableAdsModule_m169BADA1F93DFEB81AA5E060E13A5B19FA806708,
	AdsController_SetActiveBanner_m791F57B6E4419C00F07AF853FD8A614BBDC398F7,
	AdsController_ShowNative_mCC42C17279DAD6B6AB2FE695F26D6AA7EA0B7CBA,
	AdsController_HideNative_mA088864F37998BE43F61F4F25D85522C6BBFBF49,
	AdsController_CacheInterstitial_mF469D1D946D982DB65320A837C9A51E9AD775CBB,
	AdsController_ShowInterstitial_mAD62D21026D701C376D5F32785545FDF83A52326,
	AdsController_CacheRewarded_m74AB80859F31749811DC1FC232E829D8AFDA0A8F,
	AdsController_InterstitialIsReady_mB33768B42B14D04148E7900ED8E75A7859D409BF,
	AdsController_ShowRewarded_mED835DE227469213210F027ED34D01E0FE666696,
	AdsController_RewardedIsReady_m36045AEB7CF76F033D579D42F65A6411C66F97ED,
	AdsController__ctor_mF49F237B65051AA8606F4A1E9830EEACB3ED345C,
	AdsManager_add_BannerClick_m4B6DD473B334ECA47D31AEA75B8D3A27378B08E9,
	AdsManager_remove_BannerClick_mE5DB571CE2F5F82E2A81AD9017E8016915800527,
	AdsManager_add_InterstitialShow_m3B2B9C494105DB63872208AE48E3EC238CFF4140,
	AdsManager_remove_InterstitialShow_mB4F47C7F18252DBF8DC972DBE863409BE8EDD1D1,
	AdsManager_add_InterstitialShown_mDC6DF556265F0D69382DE43E6F10C4CCDC44CFAF,
	AdsManager_remove_InterstitialShown_mFBD0FA90893564A935FB74E89E4AE819AE9D10C1,
	AdsManager_add_InterstitialClick_m76D45436D22AD1F47850D4EC3F4F34FB2B69E3A1,
	AdsManager_remove_InterstitialClick_mB20C092FA193072128FC752D38275E967F3FF344,
	AdsManager_add_InterstitialClosed_m7B2D4BAFD91A8FC0AA42E0DF1E23B556D2E301EA,
	AdsManager_remove_InterstitialClosed_m6E942DF085473067CEF37B862891B762A5109717,
	AdsManager_add_RewardedShown_m3A81224E1F6AD4015909ABF830F12C51D3E8DE97,
	AdsManager_remove_RewardedShown_mB05AAF97A5B850E803F9111E42EDA8BBF710BFE6,
	AdsManager_add_RewardedComplete_mF94FE824B202C7AD746E61B1C474D3EB02F31FB2,
	AdsManager_remove_RewardedComplete_m456431CA9920BDB9B6FFF14ADC48507652BAD283,
	AdsManager_add_RewardedClick_m755E475A9AB61D1950816D4B82FEE1187948EA47,
	AdsManager_remove_RewardedClick_m9FDA7B0B312ED5DAE2369E105FB435172A2895FB,
	AdsManager_add_RewardedClose_m633AB2341D00CD5507EB8306FB985AA55C206206,
	AdsManager_remove_RewardedClose_mD60A8B1A62ABDD90AAE92DD0FBBDCC5B1900AC40,
	AdsManager_add_AdsStart_mFADDB1F7C654A5A61A5F570BF02293496175933E,
	AdsManager_remove_AdsStart_mEA8D77210698170DB0C964AEC5038548738D7EF8,
	AdsManager_add_AdsStop_mF36870F9C10C3E850182D7EFC14D9C686176FEF6,
	AdsManager_remove_AdsStop_m0BF5A702806CF48664801100CB7A109F044C51AC,
	AdsManager_get_IsInternetAvailable_mF0E12C825082F24C0C0A5E0441F5261EFDA6FFC4,
	AdsManager__ctor_m41AF346593F3131EAEF86784C6F99AC2309808C2,
	AdsManager_InitModule_mBA8F517A5CED70BF22CD6A8FBB8FEB9F7A4DE035,
	AdsManager_IsBannerHidden_mEC2D3A39427F97CF8E0F36D3769B41D950F899D7,
	AdsManager_CacheInterstitial_mD6302D2AABA5F1E6CD0B3E5C7BA1AFB82AE0FB69,
	AdsManager_IsInterstitialReady_mB742669E384685891A8D73D525AF176AA173D405,
	AdsManager_ShowInterstitial_mBF19AE55FEF5AE489AF94FA177261C7773DFB087,
	AdsManager_CacheRewarded_m9C7BD806D07B792A15C42179A55CEC41081ABEF5,
	AdsManager_IsRewardedReady_m97AE4E774A5E38F420BF0DF79B5A61BA55B68AD8,
	AdsManager_ShowRewardedVideo_m3D99F90C6CDC162BFA9F18DD9B791FDE312EF23C,
	AdsManager_SetAdsSoundsMuted_m8DBB87DA4AA960E5B2C2B874EA81194FBED02F9A,
	AdsManager_SetAdsEnable_m51819F8595ED0ADA158AEBF8D45FFB8B1DBB3D31,
	AdsManager_ShowBanner_mFD5839F63579E3DCD98386452428BB6855049F86,
	AdsManager_HideBanner_m2B857DA7E291D9F3A796F8098AF97484E9E62349,
	AdsManager_OnBannerClick_m08BF88870544A08A9FAA2C889CC487FFE3001D35,
	AdsManager_OnInterstitialClosed_mF905B37513FBDAAD1FB7AD3E445D7B16F2619F09,
	AdsManager_OnInterstitialShow_m88C3F4727C4688EA19B58A2BD4F418A0F390B8B8,
	AdsManager_OnInterstitialShown_m5A65CE1F630DB81AC0792EEE3050EB8B9EFE0D6C,
	AdsManager_OnInterstitialClick_m70F1EAE6658A83BAB93F36A8F6829DA6DFF39DE7,
	AdsManager_ClearInterstitialCallbacks_m9B8886C8B95C08ECD97611A632233C40CC8C858C,
	AdsManager_OnRewardedShown_m574DC7EAB5D39CCA275E3C95EE7C34D48B8FD6F1,
	AdsManager_AddRewardedCompleteCallback_m1D9EC4FE762DD251D0C48C311B4468FD1CF42F59,
	AdsManager_AddRewardedCloseCallback_m7B29E98F3A267E018EC6BF4CCA148FD5D5AC51F0,
	AdsManager_OnRewardedComplete_mE872F3BED1C5C149B9D66C4476A70D549C0C2E56,
	AdsManager_OnRewardedClick_m0ECB0F341FECB54B16B36211008FC2B69F4C021A,
	AdsManager_OnRewardedClose_mDB4B64AD14A4FFD8613EBD868024292660CB1524,
	AdsManager_ClearRewardedCallbacks_m0CBAF925568281BB420A1F6EEB41ED048DC8D310,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MockAdsProvider_add_BannerClick_mDCF22F4EDA20CEEF722685E9A20F3C3DE7F17210,
	MockAdsProvider_remove_BannerClick_mD0278E020FAD9A8BDC20BF94ADF1DC76335224F1,
	MockAdsProvider_add_InterstitialShow_mC342BEE88B1BFFAF48FDE331B2D8B993561E14FE,
	MockAdsProvider_remove_InterstitialShow_m9D3E95D74B01504E8B4788B3B9A1EB0ED1D49C39,
	MockAdsProvider_add_InterstitialShown_mC77DEE07EA315CB1EA93DADD8A09E717A975A9AB,
	MockAdsProvider_remove_InterstitialShown_mF3E6E4718222FAC1E9122DFFA87839513F8CBF62,
	MockAdsProvider_add_InterstitialClick_m14C2EC1165D80D563B32C451C77625828B180F6E,
	MockAdsProvider_remove_InterstitialClick_m4C2BE0D63D913B2EABC5251D8577C3E69ACC7D94,
	MockAdsProvider_add_InterstitialClosed_m94D90A204A1BA4DC89000DB504CFCD3EDF60375A,
	MockAdsProvider_remove_InterstitialClosed_m2B309AE29F69BE484CC34DE29689D5630A2FCC3C,
	MockAdsProvider_add_RewardedShown_mC21127B345D1F8DF2895D4683CCB4B8E54D24AEC,
	MockAdsProvider_remove_RewardedShown_m49DBE6F399B1E95BA55BC41839B1735E508670BD,
	MockAdsProvider_add_RewardedComplete_m9E8FE49C5C5E56C1962374EA69261EC09A6CFABB,
	MockAdsProvider_remove_RewardedComplete_m7CFF4FF56655335F7510D8D6E111EEAB60A53991,
	MockAdsProvider_add_RewardedClick_m946658AF5FC048A2FA859B59F7B55E57DED0425B,
	MockAdsProvider_remove_RewardedClick_mD9B0F1A6D5D0F25BEFF44D17EF9393B0FECBD865,
	MockAdsProvider_add_RewardedClose_mB53BFE650444C05881D4FC90B962DF373FCB8AA5,
	MockAdsProvider_remove_RewardedClose_mCB3745E8D3A17EA4F3CC385B51B40259EBD058B3,
	MockAdsProvider_InitModule_mC28B6006CB8B686091667EA49D5CF6695A72D4D0,
	MockAdsProvider_HideBanner_mB4FBC33763DC87D526F20CD5F06D6D2C837314F4,
	MockAdsProvider_IsInterstitialReady_m37EC12BC03717A507B286B0DD0AD75B4B072FC10,
	MockAdsProvider_CacheInterstitial_m15AD748B7A287BB359FBA13CC7E57E39EB63D72D,
	MockAdsProvider_CacheRewarded_m5857DC8552125EAAD49D700C66AA74E85BA927F5,
	MockAdsProvider_IsRewardedReady_m8DFDB96FAB2BD5766C92DA31ED80FE44176C0FC7,
	MockAdsProvider_SetAdsEnable_m6D1A30E5579AB67E9CBB1016210E26B165592E80,
	MockAdsProvider_ShowInterstitial_m8DD0989B79D6D8068BED37D2102E5624FBDA13AC,
	MockAdsProvider_ShowBanner_m6469950490DC3EDE140E6AE391A7DB6F103E4AEE,
	MockAdsProvider_ShowRewardedVideo_m54E79D55845A0BB64B7D5DC5CCB8B8761A38BC82,
	MockAdsProvider_SetAdsSoundsMuted_m56BCEC8F44E073D8C24703F6E6CCC023BE55C82A,
	MockAdsProvider_IsBannerHidden_m3755AE2F873FA72E270218B0407556198A5A2D9B,
	MockAdsProvider_OnRewardedCompelte_m387E4C323ECD6D0981D58B23AAF72C6D4ECD636D,
	MockAdsProvider_OnInterstitialClosed_m0B28126F507D2D2BD48DDE5ABF95F27D76DC12A8,
	MockAdsProvider__ctor_m66B047F7675116469A65729DE923009B76F0A850,
	MoPubAdsProvider_get_banner_mA0A63250432E6A7D8191BD538E55DC4AB3F34BBE,
	MoPubAdsProvider_get_interstitials_m9C200F746129F820DB72D4D5C7694D649C2DCD08,
	MoPubAdsProvider_get_rewarded_m079C4D2B9735C75E1FA82C2CEFAD0FCF1BAA6EE4,
	MoPubAdsProvider_add_BannerClick_m83863B059BC4729A5A33CCD478BDD67D9329B083,
	MoPubAdsProvider_remove_BannerClick_m45CA73BA553D626C930387465DEC7EA738A9E41D,
	MoPubAdsProvider_add_InterstitialShow_m72F59C143DAB1663D3FE90372999F548D02BFF9F,
	MoPubAdsProvider_remove_InterstitialShow_m48DA6E60E383E1A082C133F67600BF66D00B35B6,
	MoPubAdsProvider_add_InterstitialShown_m076C60508B9D826BC55FD5AADEC3D659C6D76FC7,
	MoPubAdsProvider_remove_InterstitialShown_m99CBEF315A783DAA4B487CC588451E49A741133B,
	MoPubAdsProvider_add_InterstitialClick_m4799F06F8B573D785EB740DDE02446AEC7BBA4AC,
	MoPubAdsProvider_remove_InterstitialClick_m1B2259112516F753979AD51B9C71446EC1E3F715,
	MoPubAdsProvider_add_InterstitialClosed_mFAF25A6BEA639FF87B630C979C6B728BABC78169,
	MoPubAdsProvider_remove_InterstitialClosed_m0DF8F71FC95B8512549C6162BBF9BD5059F4B56F,
	MoPubAdsProvider_add_RewardedShown_m8ABDE02450FA061E2B9DF5C2FE6491FE99FC01A8,
	MoPubAdsProvider_remove_RewardedShown_m28FF331672AEF47DF7FC480B04E8C16B4A9ADE7C,
	MoPubAdsProvider_add_RewardedComplete_mB7B3D1E21C1F801383F67C17BBF63D084B77C8EA,
	MoPubAdsProvider_remove_RewardedComplete_m1ABA2FC0D86D6E5A89974E101D5E963510C8C0E8,
	MoPubAdsProvider_add_RewardedClick_mA001A9ED4BEE12B2489D7E82E8B4599EAF88F914,
	MoPubAdsProvider_remove_RewardedClick_m929D2A0BC9862268262186290F184D138022B6CD,
	MoPubAdsProvider_add_RewardedClose_m8B0F119F2477F30FB3B661FB135CB5C4C74ACD1E,
	MoPubAdsProvider_remove_RewardedClose_mA0BE8DD43AD522C954D1B4CFBB1CBF5A9B1ACA1A,
	MoPubAdsProvider_InitModule_mE2D3053A1C02C0755BCC3685E51ECA5A53FCA539,
	MoPubAdsProvider_RequestInitPlacements_m5C50CC0F313D086B93C7E2F0744A463C28AEA2C0,
	MoPubAdsProvider_RequestLoadPlacements_m2906112A4B183B46686CB9EFA9C806536444B242,
	MoPubAdsProvider_InitCallbacks_m735363703B094AAF8381489B035BF4666465C60E,
	MoPubAdsProvider_IsBannerHidden_mDD76794ADCF32D340040316353249BF2A81CF0DB,
	MoPubAdsProvider_SetAdsEnable_m403710918A766AADF8A70014F57661EFACF8BC92,
	MoPubAdsProvider_ShowBanner_m3B1766B8AB5271FC187DA04373EE91E29D4DA8FA,
	MoPubAdsProvider_HideBanner_m8D4F20C31F40563A89B1F4B26BEB453098C46614,
	MoPubAdsProvider_IsInterstitialReady_mB9975999122EA77FE4E010F39E31B394014D10F9,
	MoPubAdsProvider_CacheInterstitial_mA5EA5E60BAC1C48620101BA03F6B929CD0F7FBC0,
	MoPubAdsProvider_ShowInterstitial_mF87C59E6991461B46E4B3BBA4E2EC051F4BA4779,
	MoPubAdsProvider_CacheRewarded_mB40C02A5534F6FE383FE4793373DEE0978C6C45B,
	MoPubAdsProvider_IsRewardedReady_m4AA82E439A11BD7280F157036ADC77661EE95362,
	MoPubAdsProvider_ShowRewardedVideo_m369D8ECBE58A51DF330D1E7EE5AED5D60F3DC697,
	MoPubAdsProvider_SetAdsSoundsMuted_m6F609DFCB16727157EA104E121938A96BF6DF1A1,
	MoPubAdsProvider_InterPlacement_mC87091D0ED2FDEE698E10A19BC9AEE8FF75CFCF0,
	MoPubAdsProvider_RewardedPlacement_m482C03EC53AAF1B6753CA65541661C20187FADDC,
	MoPubAdsProvider_onSdkInited_m5F8E482DDA1B517EE73FC060F0CB7858D143735D,
	MoPubAdsProvider_OnBannerLoaded_m7E895D40FCA942CAEB8BCA1C83C0384EFC10A848,
	MoPubAdsProvider_OnBannerClick_mE027AE8E2528CED1DE12E4AC0EC190254EF4A680,
	MoPubAdsProvider_onInterstitialLoaded_m74908B9D46B8FBB68DAAD86E76AEAB3E1E349D3D,
	MoPubAdsProvider_onInterstitialFailed_m7C9CEF4FC45E5AD2AEE49C3C2BF527EED487E040,
	MoPubAdsProvider_onInterstitialDismissed_mF9D3E1BEE030AF6FF8E19B8E0BE40463559CEEB1,
	MoPubAdsProvider_interstitialDidExpire_m3578CA044B4850109A61D0D9B95AB8FBFC5314AE,
	MoPubAdsProvider_onInterstitialShown_m7FA384F3DC393035F7F2B13F83E1165922820DE8,
	MoPubAdsProvider_onInterstitialClicked_m353EB8F85A385DB85F67543B7736FE641E406BB0,
	MoPubAdsProvider_onRewardedVideoClick_m3D7BE31DE0BF1C61C1D83BE82BC524F6A210B79D,
	MoPubAdsProvider_onRewardedVideoLoaded_mCBDEEED2757D67F16BB04BBC3EB529AA02B3B769,
	MoPubAdsProvider_onRewardedVideoFailed_m6E7210FFAEC7A7D40E4FF958FC988B9F33186B56,
	MoPubAdsProvider_onRewardedVideoExpired_m009B57010D047CE841690FF1D0319D2F831B84C7,
	MoPubAdsProvider_onRewardedVideoShown_mFF4E863E2C65AE58320B61FDF787E62F38ECC36D,
	MoPubAdsProvider_onRewardedVideoFailedToPlay_mFF24AF47F8980AD19C8AB946B20FC1EE76A08FBB,
	MoPubAdsProvider_onRewardedVideoReceivedReward_mEDB7267AEF8A48EC862D5135CB7A7DF85D2E25F1,
	MoPubAdsProvider_onRewardedVideoClosed_m6FACC21C3B0A06EDA15EE997CFEEADDAE4A031CE,
	MoPubAdsProvider_onRewardedVideoLeavingApplication_mD55DED44A4E6D8D006CEBDB742F84950277AFA51,
	MoPubAdsProvider__ctor_m1E8722BD003FF1CCA1221B375E259343DDC70EC0,
	EasyBrainAnalytics_SendEvent_mDADA0924A92BFD8B5EF75E642D7B4AABF1A3A869,
	EasyBrainAnalytics_SendEvent_mAC8C5F0E65D9A8C7E48D509A1A0FEDA7E7284110,
	EasyBrainAnalytics_DictionaryToJson_m5BCD8E796B7FFBADF23936D37DF4EA6A6C740649,
	U3CU3Ec__cctor_m214484930B73709D4F5301991709F64F351CFD8F,
	U3CU3Ec__ctor_m52A864AE518DC9181763D5D16992D46A4E80460A,
	U3CU3Ec_U3CGetNativePanelU3Eb__37_0_mE27470625052B665F65155047280166DD8F0907C,
	U3CLoadSceneU3Ed__4__ctor_m740271F33225B08C3513BC3F6B59B8F1450DC1BF,
	U3CLoadSceneU3Ed__4_System_IDisposable_Dispose_mA097F7A77FF3232A5CCFDA3E3EE84C8E90835E06,
	U3CLoadSceneU3Ed__4_MoveNext_m76235F77326C197380E4024007F0F96B71729184,
	U3CLoadSceneU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB7169CC88D097E7A22A250C09F19377AF2548AF,
	U3CLoadSceneU3Ed__4_System_Collections_IEnumerator_Reset_m761D97B7598ACFE5C22A799BCFA5A1DCE9118B46,
	U3CLoadSceneU3Ed__4_System_Collections_IEnumerator_get_Current_mA40036D0A4622E3745894A08D1BE42F4C77BA7C9,
	Action__ctor_m32F21522DBE3B5661FA2EE53F77C9E706F129455,
	U3CU3Ec__cctor_m99087ECA36C4F12887BE5CF4979BFD19A3EFD9A0,
	U3CU3Ec__ctor_m337FB881A87DD6A331769F8F1CA274491870AB32,
	U3CU3Ec_U3CCreateTextU3Eb__16_0_mF09B12E2EB7D6041EA990F2D3981D850D12EE44E,
	U3CWaitAndPlayU3Ed__3__ctor_m18B36EE87FC3928ECE5C82BD76728ECDA17093DC,
	U3CWaitAndPlayU3Ed__3_System_IDisposable_Dispose_m9AD16942A9E6D79D1919E21EA9A9D5D4F12D5438,
	U3CWaitAndPlayU3Ed__3_MoveNext_m21787CB8FE32BF3D88D7D5A0604A8CAB10F30A33,
	U3CWaitAndPlayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m632DC50AF14738EBDFDE3B36BA618331C52C572C,
	U3CWaitAndPlayU3Ed__3_System_Collections_IEnumerator_Reset_m3F3B7BCCBE4045208ABC64A2C476D31785702D00,
	U3CWaitAndPlayU3Ed__3_System_Collections_IEnumerator_get_Current_m8F44381262CB765F0D5A5F01BEBC5B97EA895374,
	U3CWaitAndOfFramePlayU3Ed__4__ctor_m514829B08C1832F148648DCD1799DF6C38C88C54,
	U3CWaitAndOfFramePlayU3Ed__4_System_IDisposable_Dispose_m0A6CDABC6181A2439606BF85693544384C023010,
	U3CWaitAndOfFramePlayU3Ed__4_MoveNext_m31D4C3A4E75ECECD8981B89BD0CAD8B29123150D,
	U3CWaitAndOfFramePlayU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA734C7C26A71C97655C355E6766C23088CFB0570,
	U3CWaitAndOfFramePlayU3Ed__4_System_Collections_IEnumerator_Reset_m0807477B0BCE0520DD18859595DAEBF40549E35F,
	U3CWaitAndOfFramePlayU3Ed__4_System_Collections_IEnumerator_get_Current_mE728D8371B203B5E1063076E1CE4BAD3E45CDDBE,
	U3CLerpU3Ed__5__ctor_m3C57875C4287B4288841E7958A7328D6281E9A41,
	U3CLerpU3Ed__5_System_IDisposable_Dispose_mD06BCCB05C8594F3A744370DC241160B790FDC8B,
	U3CLerpU3Ed__5_MoveNext_mC8A7461F62EF42329ABBDF8086868C1330325928,
	U3CLerpU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEB2CB7378663BCCAA13DCCB6982FBDE55D519AE1,
	U3CLerpU3Ed__5_System_Collections_IEnumerator_Reset_mCCCB3067431E1038C152055E021D126366EB34CA,
	U3CLerpU3Ed__5_System_Collections_IEnumerator_get_Current_mE4C07B5FD00E151A153115AE02740480A33BC86F,
	U3CU3Ec__cctor_mAE5D7B5EE088B27CB0D9AB34760C1456591E036B,
	U3CU3Ec__ctor_mFE33514F4C921196F198DF21063EB1F28B193F0E,
	U3CU3Ec_U3CStartU3Eb__34_0_m91554E1E59FDEC8101E910D410BE889A7372CF39,
	U3CU3Ec_U3CStartU3Eb__34_1_m216070042866E12F02B54F2444085288EA87B303,
	U3CU3Ec_U3CStartU3Eb__34_2_m4D37DDDD1A3946847DF7EE945DB6F36B6640DFD3,
	U3CU3Ec_U3CStartU3Eb__34_3_m32968282A6AC5D5D8EE03BB8D07E7C0EC7EEF888,
	U3CU3Ec_U3CStartU3Eb__34_4_m49D99AADBEC08B8A537888FEEE2D1AE7E247BA4D,
	U3CU3Ec_U3CStartU3Eb__34_5_m3BEBD66747CA8EF043510A6B5C71CBAE59BED3D8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m038623D71333A252CE1B406D2D61CEE642A56E3A,
	U3CU3Ec__ctor_m838C2D9A61CF690528B20387CF4DB2C9EBA2CA97,
	U3CU3Ec_U3C_ctorU3Eb__3_0_m196874FD63EA078E3E4C623F63D6A1CD00358F06,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m9AB8D2268F2317F06D072C5D4867F8B76ED8054D,
	U3CU3Ec__ctor_mC2A0F0A5DEDF9E498C987FB8A008A8AAC9582227,
	U3CU3Ec_U3Cget_ArgsU3Eb__7_0_m5DF1F932707D0B6E9B3413236F344241E1F610CA,
	U3CU3Ec_U3Cget_ArgTypesU3Eb__10_0_mCF898E1E6759B1C9C4609FCE87AC1FFE4EE8AE47,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_m72DB59322C7C192D3F4506F34E3C06F28E53D3DB,
	U3CU3Ec__ctor_mE87573A50889DBC66C22A7C00C2690F189854B32,
	U3CU3Ec_U3CInitEventsU3Eb__27_1_mD7585AEDB0823C00C1812AB622A5BB4B13CA9A9E,
	U3CTickerU3Ed__5__ctor_m6506A274E76933FB14B2D80126EB4CDF4C93FE52,
	U3CTickerU3Ed__5_System_IDisposable_Dispose_m28F47615CC8D3541C662D3AA4EA295FFC9E5F0B0,
	U3CTickerU3Ed__5_MoveNext_m5932E832B4560B9DD5CA58EF54C3F85A01852475,
	U3CTickerU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC1D998FD79959A083E7BAC3ED3798DB903647F8F,
	U3CTickerU3Ed__5_System_Collections_IEnumerator_Reset_mC0E414321BE252B62603D8E6DC8F996B0FE4AF7B,
	U3CTickerU3Ed__5_System_Collections_IEnumerator_get_Current_mCB60A525C9D216A4FE0543C499301866E55ED4DA,
	Callback__ctor_m3B9F1F167DC8FCDF4C5811F465A073CDEA894AED,
	Callback_Invoke_m6D3D986C1EBEE9E090B94E57869CA3FEA6D68FF7,
	Callback_BeginInvoke_mB892560A04FB24A3A18A2B8D7A04292AFD864256,
	Callback_EndInvoke_m07E1FA189D55E7227328479F71E7E9953822E64A,
	Condition__ctor_m580BF058D886A9E785A7765CC50863F5F3F79CBF,
	Condition_Invoke_m680DFC9E62DC66301607D6F90C20C02EFADA7AED,
	Condition_BeginInvoke_m46715EBC5923A61C0BF8939F130E9D5CA1B150AA,
	Condition_EndInvoke_mF1CDA8DCA7C162302EE549310A0196EDD945E4BE,
	Tryer__ctor_m73C1FA3A45C39E4E699764440A93769E8F3FD531,
	Tryer_Invoke_m58F98D306E1E48997BAE64205FCE05473F9A44B7,
	Tryer_BeginInvoke_m3CF369F558DEC667DEFF6FC12570779B83741BC8,
	Tryer_EndInvoke_m6769E533D6676E48CC99175B1DCF58A368D1F6D0,
	NULL,
	NULL,
	NULL,
	NULL,
	ScriptMethods__ctor_m562578213FDD763B1C1161ACCBE72D7673EDB66B,
	ScriptMethods_GetMethods_m3E6E02F5B86C26756D6C4C5781259F9616C6FDD5,
	UnregisterException__ctor_m452F6AD6AC1664861D8D038C79EB553EDEED558E,
	SendException__ctor_m02E88335A30B39B623F73613DDD999950338E265,
	Sender__ctor_m50218838A9C6B69714950576DFA75F4D592FD05D,
	Sender_Invoke_m3116C57F31119A05504CC4C2F764F5757EE6A7DB,
	Sender_BeginInvoke_m574BC87830A5BBD07D8FA8F28C34FC5723E5578B,
	Sender_EndInvoke_mD361DE1FF0F0E790382FC3E62B81E4A48EF6F1E0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	vp_PreloadedPrefab__ctor_m4CDA3A6564B496C3AB953931679478A566FE5EA7,
	U3CU3Ec__DisplayClass15_0__ctor_mCD9905465795BBD53A86044608E804E02BD1CC92,
	U3CU3Ec__DisplayClass15_0_U3CDespawnU3Eb__0_m614760325CBAC4DCBC63E34505056E274811C831,
	Callback__ctor_m56FE6C8553E18184859AD3DF01727565DF7F910C,
	Callback_Invoke_m3A5AA72A5D0FB5CCFB9DF56B2B5E4A4627804E9A,
	Callback_BeginInvoke_m13FDE19DB4B0C59FFAB25B0BB810C18DC385E763,
	Callback_EndInvoke_mA093EDF2D1B815FE2DDFD7FF7EEBF6614A092FE8,
	ArgCallback__ctor_m54F3568C2236D0C945D04D797C17BFFD6252E04C,
	ArgCallback_Invoke_mFC4A16689B521E5415EB72EA62090F7AFF7E19C0,
	ArgCallback_BeginInvoke_m5BDEFF08A315C338C6B0226CF602C0FD37E1BF3E,
	ArgCallback_EndInvoke_m4480BDE54488767C93DEC1F81332BFFAF778928E,
	Event_Execute_m2224A3F8441A1B5740F0B81AD62CF8452FACBDF6,
	Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E,
	Event_Destroy_m54F753C2DB0D932A829D856B16E14C11F7F18AF9,
	Event_Error_mB82C87213850999FBA9FF7CC1AF57504C07CADA5,
	Event_get_MethodName_m2E7322780EA4E933FB1C38D9E0863E8C719DBC2E,
	Event_get_MethodInfo_m6D8C37E774DC62EF53B32C7200739E777903BBB3,
	Event__ctor_m8A8093020EB76395E72386F4BCFA01A44DA356EB,
	Handle_get_Paused_m562EB5C50C38188FF7F7F259F226F0413431A21C,
	Handle_set_Paused_mFB84304A3C08EC30C7DCFAC8798AB87B54BD03D9,
	Handle_get_TimeOfInitiation_mCF50BA6AB357EC8F5C48CD34CBB3BA215E0DF8D2,
	Handle_get_TimeOfFirstIteration_mA39FE629D19E11FB05828A3270347DC5D9394868,
	Handle_get_TimeOfNextIteration_mBFD62F4ED09C4A9F171C38186FDF15C340D575D9,
	Handle_get_TimeOfLastIteration_mF336E13CE195013F54C2B844BB36C0786283FCE4,
	Handle_get_Delay_mC0587B87BD995E4254FF0376B9C6E452A7B739A9,
	Handle_get_Interval_m69485C36EEA899AD123B4611691EFF45B2769B73,
	Handle_get_TimeUntilNextIteration_m10569D96A53447028A4808698BAA5C7E4723DDAC,
	Handle_get_DurationLeft_m6C99B4B8342D947F4B702426095273A300A394C5,
	Handle_get_DurationTotal_m3A09F8D669E6A690108BC47DE33BFCDCBEEEC982,
	Handle_get_Duration_m5F17B06185F2C5274416BE67918561ECC787B026,
	Handle_get_IterationsTotal_m4AB3FA38F14F019FD2C636E80C907904D3760CF4,
	Handle_get_IterationsLeft_m55653075F94469335A328CDA19F24AC02595BF11,
	Handle_get_Id_m94F85EB949AF983F520F10F557ECBE62B7E7725E,
	Handle_set_Id_m035AAB0529283D2F19A200F186C5AFDAB19F666D,
	Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C,
	Handle_get_MethodName_m0AE43AEDF241A34CC0BB638AA8B92945CDE5F88F,
	Handle_get_MethodInfo_m9F6A97E4B626742350A556D1C05029023B8AFA3F,
	Handle_get_CancelOnLoad_mFB1F3942A62413B5DDAD1C54AAAF0C56FFB27BF2,
	Handle_set_CancelOnLoad_mE921CED0AF28227617B3E4F315749E4205211216,
	Handle_Cancel_mFE3C59AD52725C2AF4777A670FB61E2CA05D54EF,
	Handle_Execute_m9B76827FC4A9073BB28118B740366FA064C0FF2A,
	Handle__ctor_m685E26AAB613CA402290CFD650E608B180BC91EC,
	U3CU3Ec__cctor_mE2FD9741E9B6569B6DF9BAFC826AA9FAACEB1964,
	U3CU3Ec__ctor_mF1EA0DF766594CC8579CF02CAF353208E57AD94E,
	U3CU3Ec_U3CStartU3Eb__24_0_mB0C59AB47BA9810AF8601A63A7289233362D339D,
	U3CU3Ec__DisplayClass18_0__ctor_m8FAC57006AAA255FFF14C50AEDCE44E0836232BA,
	U3CU3Ec__DisplayClass18_0_U3CDestroyU3Eb__0_m325D201A9C46B0BEAA9E5AEC6E86866FCF616943,
	U3CU3Ec__DisplayClass0_0__ctor_mF43CB9A9E4D08847E6D5EEC6029818F47412160F,
	U3CU3Ec__DisplayClass0_0_U3CAddEventU3Eb__0_m0FCC9FCAADCBDEA562747CB8171EEAB51BBC9980,
	U3CSwithcVisualU3Ed__18__ctor_m298249329919476E31CF4FFF4CB2F41BD046215E,
	U3CSwithcVisualU3Ed__18_System_IDisposable_Dispose_m6C17DFC43B78B5B601FA23A6691C19AF3B2EF105,
	U3CSwithcVisualU3Ed__18_MoveNext_mFB5988015BCF65DAC7DFDD8CE73751BACF1559E6,
	U3CSwithcVisualU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1AC45232834AE473FCA8BC71E4FC381CAE5305C,
	U3CSwithcVisualU3Ed__18_System_Collections_IEnumerator_Reset_m476CFF7A7792C90AA554F55D5195EFAB82180401,
	U3CSwithcVisualU3Ed__18_System_Collections_IEnumerator_get_Current_m948D76E9579C4B2627E79640719CA213C93AB374,
	U3CLoadMusicU3Ed__35__ctor_m0B02DD6D0AA2B569773BE89F51B635BE6C200119,
	U3CLoadMusicU3Ed__35_System_IDisposable_Dispose_m5ED4E5A7E5FDAF1721EE0B6BF12D306CFF37FAEF,
	U3CLoadMusicU3Ed__35_MoveNext_m19DEEE2C2F9662E92F5425E60E6A056D5DEEDD59,
	U3CLoadMusicU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m368EF12CAD7A0ECB24BB685951FA34493C6249AB,
	U3CLoadMusicU3Ed__35_System_Collections_IEnumerator_Reset_m53A2F85EEF37E07259A530350494978BE9C3CD69,
	U3CLoadMusicU3Ed__35_System_Collections_IEnumerator_get_Current_mD44FFC92C2104744377E78AC73287B9C466DE042,
	U3CU3Ec__cctor_mE0F2E0A923AE9F143BCB511E8D47F1A736E11283,
	U3CU3Ec__ctor_m65124A33D43C64BF99CE90D3B26EDBE1BFE8AD31,
	U3CU3Ec_U3CStartU3Eb__2_0_mCE858024AA3B8EE96DFF873C62445A7EA1584FF5,
	U3CU3Ec__DisplayClass6_0__ctor_m4CF85D6EB0AB25B0EED7EA3DB448237E727F9EAD,
	U3CU3Ec__DisplayClass6_0_U3CInitU3Eb__0_m9053BED81441A6BA5601C87AC4F33B4F85C57E2F,
	U3CU3Ec__cctor_m29E10ED3D479A5B303C91B5ACECCC5708BA756EA,
	U3CU3Ec__ctor_mE234A715652514E5C6187EFC7871C1E1DC7CDF6C,
	U3CU3Ec_U3CPlayAnimationU3Eb__22_0_m342AF2CAFB727BF50E54117DAF656D8B99A64186,
	U3CU3Ec__DisplayClass26_0__ctor_m32C724584CBAD3CA55E6C021B202BFDF8DCAC3D4,
	U3CU3Ec__DisplayClass26_0_U3CInitU3Eb__0_m35F584C51C85EED13C9E4C29041A1487E3732A1C,
	U3CU3Ec__cctor_m3537837388E9168F44B49CC1F3A3FA936452A4FF,
	U3CU3Ec__ctor_m2D3B5946F8138CE48CAA3FF9E7C1BB4CAAD22582,
	U3CU3Ec_U3CPlayAnimationU3Eb__12_0_mCE9630156D3FBB3BF18D60C1A49E6B0A14EC0C84,
	U3CU3Ec__DisplayClass16_0__ctor_mE7A9FB7FC02ECC034AAD7C67FD9A198243012CB4,
	U3CU3Ec__DisplayClass16_0_U3CInitU3Eb__0_mDD20FEF534A2F92BE4C161E1D49667B9E40672AD,
	U3CU3Ec__cctor_m15BB7DB6A26FFF10DBB041EA3E03BFCA520427DF,
	U3CU3Ec__ctor_mFD5583F72D234F5D0C4B73F64D96D7F67C903D7C,
	U3CU3Ec_U3Cset_IsShowedNativeU3Eb__4_0_m70D97E494D48AD558DF01741A5D0FAC415D826FF,
	U3CU3Ec_U3Cset_IsShowedNativeU3Eb__4_1_m3F02D703082B981F44E3AF6FD33E452D1DCC0D33,
	U3CWaitEndOfFrameU3Ed__4__ctor_mD14B6667DFDD079BD44612E69BB55CD93389FB5B,
	U3CWaitEndOfFrameU3Ed__4_System_IDisposable_Dispose_m4C8432B484493DC8CD436A040844A8755BFA18E8,
	U3CWaitEndOfFrameU3Ed__4_MoveNext_m2872B6D76B202B2EF3600CD2226FEB2105C02305,
	U3CWaitEndOfFrameU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEAB37B45A9D7820D01759B041AF2660F76894750,
	U3CWaitEndOfFrameU3Ed__4_System_Collections_IEnumerator_Reset_mE5EA86DFE2C037E790ED4B2282D536C9DA16A714,
	U3CWaitEndOfFrameU3Ed__4_System_Collections_IEnumerator_get_Current_m141736F70D9CCAD73020F45661F894B12115BFCE,
	U3CPerformanceControlRoutineU3Ed__13__ctor_mA4331522DC6ACF6FFB7FB757E7B59EF09A0CF06A,
	U3CPerformanceControlRoutineU3Ed__13_System_IDisposable_Dispose_m688A2604FBD6187F56F06A5E0273F48358B5D0DD,
	U3CPerformanceControlRoutineU3Ed__13_MoveNext_m6DC330C7AF8671D4EA2B30626F3233C1AF045A6E,
	U3CPerformanceControlRoutineU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94B213A582357FEC1661E6C28506C966CBE16F6E,
	U3CPerformanceControlRoutineU3Ed__13_System_Collections_IEnumerator_Reset_m548CA7D9D2CF1A2E5F776BF68A0BAD535B2244DE,
	U3CPerformanceControlRoutineU3Ed__13_System_Collections_IEnumerator_get_Current_m73E9E45B12BDB6F5BE6AF3CC68109AA519F649CE,
	U3CU3Ec__cctor_m14D2EB0A18819561027351B1B2511EEAF997EEBD,
	U3CU3Ec__ctor_m73A046E9BD32731619597CA5F79E2CC32AAD57E9,
	U3CU3Ec_U3CPlayClipU3Eb__27_0_m07FEEE292EE21A34624AF93903D025BD3C04DF81,
	U3CLoadAlbumDataRoutineU3Ed__35__ctor_mCED84ED9E8EB6615D9351B797E185BBEDECA925A,
	U3CLoadAlbumDataRoutineU3Ed__35_System_IDisposable_Dispose_m6FE46F5F5EFA0A988A8B4C0F1DB76ECC9678F1BA,
	U3CLoadAlbumDataRoutineU3Ed__35_MoveNext_m7639F6DEDFAF1EC965F52425D3798D389C0D8319,
	U3CLoadAlbumDataRoutineU3Ed__35_U3CU3Em__Finally1_mFD90409B33C30290502A368A0A5E020550A7326C,
	U3CLoadAlbumDataRoutineU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE123234CE30C9A775EC5D042B524B7CECE4D292,
	U3CLoadAlbumDataRoutineU3Ed__35_System_Collections_IEnumerator_Reset_mECB6415647573B2696DE752548D1F2F0B47BA580,
	U3CLoadAlbumDataRoutineU3Ed__35_System_Collections_IEnumerator_get_Current_mB06CD4BEA87A4590FFD0856858E708ECDBEC5CFD,
	U3CLoadAlbumsWWWU3Ed__20__ctor_m40E3CD8B09E1AEF506F8397B73F41D8B71986E3E,
	U3CLoadAlbumsWWWU3Ed__20_System_IDisposable_Dispose_mFDC0C0C3A57E094C6CD182815464511028CA5BF9,
	U3CLoadAlbumsWWWU3Ed__20_MoveNext_m1F67DFE8042C68BF89852C619AD9B257D4CFD20B,
	U3CLoadAlbumsWWWU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2C386D726127B605BA39FCD5A9F15800CCEFCF7,
	U3CLoadAlbumsWWWU3Ed__20_System_Collections_IEnumerator_Reset_mD8296B57EEA26932A6052F910ED09FE3C9DD5CCA,
	U3CLoadAlbumsWWWU3Ed__20_System_Collections_IEnumerator_get_Current_m8B4DC545DAC87F6A444165F1260B0983866C5DB2,
	U3CLoadClipU3Ed__10__ctor_mF47FEECD16118BDAEE7F358CD579B3CD7766B3CE,
	U3CLoadClipU3Ed__10_System_IDisposable_Dispose_m1ECDEF3DE70F8FDBC3B2C8D41E02CFFDDEB43236,
	U3CLoadClipU3Ed__10_MoveNext_m2E2FBC46CFA9107CF27F33F2BC6F094A6C16101D,
	U3CLoadClipU3Ed__10_U3CU3Em__Finally1_mC78B1A9DDC2C8063F8E4E3C72E140335B66D23A2,
	U3CLoadClipU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CE591E181710A83BC56B0B29606FB93DB8A481A,
	U3CLoadClipU3Ed__10_System_Collections_IEnumerator_Reset_m2BDE0512E4E706D206AD7D63F409F7376F379572,
	U3CLoadClipU3Ed__10_System_Collections_IEnumerator_get_Current_m12CE771044CD81A542760B4D40743CF3D0ACF6F2,
	U3CSwapU3Ed__24__ctor_m88A690C417A9B51429316B1559DEACA90E4C7B8C,
	U3CSwapU3Ed__24_System_IDisposable_Dispose_mA2E36D3B5D11A4F31D2FEDE6EF08B140BDFCB203,
	U3CSwapU3Ed__24_MoveNext_mABFACD7FAE68888B45B0ABDE3F697CBFECB9E653,
	U3CSwapU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m88A5D357CB75A47736DCB9E04E9FB41A1845AEA3,
	U3CSwapU3Ed__24_System_Collections_IEnumerator_Reset_m5321CC915BE87ECC53CB1433A5BF3817AD73C974,
	U3CSwapU3Ed__24_System_Collections_IEnumerator_get_Current_m2A8BCE3CA7BF8E69726D9234D8737506ED4AD2EE,
	U3CLoadPageWWWU3Ed__47__ctor_mD4CEC79EF48453FA4911FCE54E44F1EEA7CBFDFC,
	U3CLoadPageWWWU3Ed__47_System_IDisposable_Dispose_m1337A22408B7DE74B5DCE775850EAD6C54DC302B,
	U3CLoadPageWWWU3Ed__47_MoveNext_mC363634DC0396A8BB888B5627CCEAF33138AF4E9,
	U3CLoadPageWWWU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD834A6B6DE0BEC639AB5DEA72E67F239A3704C6A,
	U3CLoadPageWWWU3Ed__47_System_Collections_IEnumerator_Reset_mC29E03FC9FEB81DB681E42AB3D39FAE35742C9E4,
	U3CLoadPageWWWU3Ed__47_System_Collections_IEnumerator_get_Current_m7ABDD6434A7727D7A0A0D21CA778BCE0FFFCB816,
	U3CU3Ec__DisplayClass2_0__ctor_m81E6626B141825DC2F39630C1CFA037547EF8922,
	U3CU3Ec__DisplayClass2_0_U3CInitLevelU3Eb__0_m3AD9E893AAA56A59F69969F457B162F6B4629311,
	U3CLoadImageRoutineU3Ed__14__ctor_mAB537BBAC365067BE5C3194EE536A9747C8FC651,
	U3CLoadImageRoutineU3Ed__14_System_IDisposable_Dispose_m04C269C3F49C97A3575DEF5449CDC2D102E3EF1C,
	U3CLoadImageRoutineU3Ed__14_MoveNext_m4156B2D0BF65A672C1096CD7483B6E83E2E2DB77,
	U3CLoadImageRoutineU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3890A30888F67654E456A4A6D7FDE2E0E809491,
	U3CLoadImageRoutineU3Ed__14_System_Collections_IEnumerator_Reset_m538443F2A42F05CAAEA03E6562BCE8D5B85E6797,
	U3CLoadImageRoutineU3Ed__14_System_Collections_IEnumerator_get_Current_mC4CAA1BBC1D8EEC4BFD0108D3AC1997165926EC4,
	U3CLoadPageWWWU3Ed__30__ctor_m5540967811A0C74A5281E9D775A0572E7203ACC5,
	U3CLoadPageWWWU3Ed__30_System_IDisposable_Dispose_m157C0EAD859880EAAB5E7FB22FD9870CB4F26080,
	U3CLoadPageWWWU3Ed__30_MoveNext_m0A01E0857CD0D44360C2110005469294EC539946,
	U3CLoadPageWWWU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD7362B63039FB2A00F5B0BD4D713140F5B61A98F,
	U3CLoadPageWWWU3Ed__30_System_Collections_IEnumerator_Reset_m8AA4BEE8B0717748481C94144180E4AF30F1744E,
	U3CLoadPageWWWU3Ed__30_System_Collections_IEnumerator_get_Current_m9E613FC37019E060E967449481071B157BEDB14C,
	U3CLoadTotalPagesWWWU3Ed__31__ctor_m59EA9C888EC797ECF01EDC23372B3B748F1FF492,
	U3CLoadTotalPagesWWWU3Ed__31_System_IDisposable_Dispose_m3577DA2DD158F31CD3AC9EE8A9C1C36A575C7FCE,
	U3CLoadTotalPagesWWWU3Ed__31_MoveNext_m18ADE94EFE4B071281E9AB7046AC2F9C97EB9372,
	U3CLoadTotalPagesWWWU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E157703C0623B7BE0960AD088811E18F94F0E4B,
	U3CLoadTotalPagesWWWU3Ed__31_System_Collections_IEnumerator_Reset_mE9C96B61D18E6855FE5F174D2EEC33F297789B18,
	U3CLoadTotalPagesWWWU3Ed__31_System_Collections_IEnumerator_get_Current_mA3E1BA4642C7C8FE821EF35023698C318E1955F1,
	Enumerator_get_IsValid_mA1C807DA248ACDC8D69570C215151BE536992A4D_AdjustorThunk,
	Enumerator__ctor_m8F4C8C2129D9E66DE6816818E2330B0C9D9197A9_AdjustorThunk,
	Enumerator__ctor_m9C36DB211444EA1718700F20E00050B5C04C7BA4_AdjustorThunk,
	Enumerator_get_Current_m69AAA5B91F3B6161F15A6266A97158CEB7FBEB4F_AdjustorThunk,
	Enumerator_MoveNext_mECE6AA8D79CC2BE34F339D542CAF81109ECBD734_AdjustorThunk,
	ValueEnumerator__ctor_mE53FF2A7721BECA429D382FCE284B28E35FFE8BD_AdjustorThunk,
	ValueEnumerator__ctor_m2E66B11990EF05ACD8D47DD745BD8F09E296D586_AdjustorThunk,
	ValueEnumerator__ctor_mF172DE3A3EDB539FCB2DA64B73EF2DA57011A657_AdjustorThunk,
	ValueEnumerator_get_Current_m151356ECDEFD55E1FFF524FE25A0F489878A10AB_AdjustorThunk,
	ValueEnumerator_MoveNext_mA238C0869778636B32013EE14948492CDCC59F2C_AdjustorThunk,
	ValueEnumerator_GetEnumerator_m3C2328208D593CFC614F7D42AC30DDB1047A9E1A_AdjustorThunk,
	KeyEnumerator__ctor_m4D48B705B3A7E4FF9EE283162B70332130540C50_AdjustorThunk,
	KeyEnumerator__ctor_mDF5D9D0D168C15E28B499F5C7A1837844568B2E0_AdjustorThunk,
	KeyEnumerator__ctor_m774690E38FB0F738D9427AE75203F3D199FC4513_AdjustorThunk,
	KeyEnumerator_get_Current_m1778115C5302BE7AF526622909BDB6CB2660D4F1_AdjustorThunk,
	KeyEnumerator_MoveNext_m06FB9DCBA7E68D21D18B09DA090C9B2646ADA57A_AdjustorThunk,
	KeyEnumerator_GetEnumerator_mF33634E45FC6CCD3ACDE273AEB14CAAA35009A91_AdjustorThunk,
	LinqEnumerator__ctor_m239C73954D0CE2FA9B4C6321A33103DAEE146E5F,
	LinqEnumerator_get_Current_mF0328C3C3F6CF43D5741818EDC8AFFE536BAA7FB,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m5B8A3AABAB68E3A1DF96059FAA974A049F5B692F,
	LinqEnumerator_MoveNext_mCA6FF832D795450D0D85F75259E3DAC95DF20FCD,
	LinqEnumerator_Dispose_m5A53643F1E33F79F145800DFEBA329647B7080C6,
	LinqEnumerator_GetEnumerator_m23D6C6A7C8F5F358DB73B741E09507C5FEB80850,
	LinqEnumerator_Reset_m18AA07757D98DA78B7CD6DDA118EB0AA23B45ED9,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m24D839B8C290A001DE43D9D9B918DF35F1F7C694,
	U3Cget_ChildrenU3Ed__41__ctor_m8E8E8F5BDB3F0FE0B146C3F7F3783958FD4F0A63,
	U3Cget_ChildrenU3Ed__41_System_IDisposable_Dispose_mA700C935BC870C909B7BAAA7329927D0167399A3,
	U3Cget_ChildrenU3Ed__41_MoveNext_mF8D6574470598B7918BB1FEF83168B6ABF86BA2C,
	U3Cget_ChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5A0B2D41DF2E406A9BC4DA0D80A352A9063A3DEB,
	U3Cget_ChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mBF2656EDEB2D2A8ED821927C2A9ABE22E3E1B428,
	U3Cget_ChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m2C65131B2B2B11C072B4122ED6A0DA74504DA629,
	U3Cget_ChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m9C9EF20C587371412C4089A8F5A772C527FD419F,
	U3Cget_ChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m0A48E1C7F0095F0517E7FAF8FA8B785AF66376CB,
	U3Cget_DeepChildrenU3Ed__43__ctor_m7FFFD7C525229E37D79ECDBDA820BFC3F501FFA4,
	U3Cget_DeepChildrenU3Ed__43_System_IDisposable_Dispose_mE872945D248E72AFDADCDA09699AFB2639AA1425,
	U3Cget_DeepChildrenU3Ed__43_MoveNext_mEE97082D54099A9AA86AE7303E82E19FE4DA1134,
	U3Cget_DeepChildrenU3Ed__43_U3CU3Em__Finally1_m7F0C4DC120A57E0B62ABCF30B18A853255324B97,
	U3Cget_DeepChildrenU3Ed__43_U3CU3Em__Finally2_m8EB01DC002CF736E57053B4F7306EDCD16E06793,
	U3Cget_DeepChildrenU3Ed__43_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mC7E7C1AAA6493F4277AD6F50D59C26EB524A8E49,
	U3Cget_DeepChildrenU3Ed__43_System_Collections_IEnumerator_Reset_m0013F10527DAEBB84D2E86AC688E4C413EACA6A4,
	U3Cget_DeepChildrenU3Ed__43_System_Collections_IEnumerator_get_Current_m89AEB95BD1C1220D6A0B2CBCB0D1E2EA2A6A887E,
	U3Cget_DeepChildrenU3Ed__43_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mFE99AB7665C57E5D53691D6AFA47AAE5756838DB,
	U3Cget_DeepChildrenU3Ed__43_System_Collections_IEnumerable_GetEnumerator_mEA59685745103AEBADC05AA05FE04EEF38F05C93,
	U3Cget_ChildrenU3Ed__22__ctor_m395094D63E499CE83BFE7BEA57F2AAC327C0896D,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m428B887F7109FE19549A407335CA673F0877CFC4,
	U3Cget_ChildrenU3Ed__22_MoveNext_mD6CDE794B06D36428C6EA7A2CA67D8FCA3FB3079,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m59DFB9AC110EC20ABC19034CD0308545E01DCAA1,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5D1768596B7404BC7B060F244F5D88C3528E52BB,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m1F10658E4ABDC538024A08C254CE4F9B019FB5A8,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_mAAAA2B88545650436E60B70B652267A1836DCAD2,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mE9567310EFE3EB91CDA68AB43D87C4D01F640C37,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m1CC85EA09099DB444D2CEB569C0EE029B91FBC76,
	U3CU3Ec__DisplayClass21_0__ctor_m613C3625A3BD4BA186F316B692F7C8D5EB4923BF,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_mC0192D6A330495FE80E67D90A913CA143B7F8EEF,
	U3Cget_ChildrenU3Ed__25__ctor_m3EE9D3A4D0EDB7EDD21050DB036528D1421B9D7C,
	U3Cget_ChildrenU3Ed__25_System_IDisposable_Dispose_m72D96F3E7EF6E3A1C9FF6A2136867490C446C42B,
	U3Cget_ChildrenU3Ed__25_MoveNext_m50B969A9AE504FC0F6F24D6D9CD2A91F5BE90291,
	U3Cget_ChildrenU3Ed__25_U3CU3Em__Finally1_m502C05547A22BDF91298F2B541AEAFE26007128D,
	U3Cget_ChildrenU3Ed__25_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m181DF40535B1A5A4B5EF98CE7E3E0F2D5ACE6084,
	U3Cget_ChildrenU3Ed__25_System_Collections_IEnumerator_Reset_m92C6A429195F76E2702A3325ABD90EAE1DC8BFD4,
	U3Cget_ChildrenU3Ed__25_System_Collections_IEnumerator_get_Current_m0400579D20FC6B1A893529EB2A11620E71F84F64,
	U3Cget_ChildrenU3Ed__25_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m8F552CBEF8520EB8CF857F5A16A8027493D5FCC2,
	U3Cget_ChildrenU3Ed__25_System_Collections_IEnumerable_GetEnumerator_mF6077287D096F45C708B0F7BA77749CEA1957270,
	U3CRotateLoopU3Ed__30__ctor_mFDEAD87EDE102EB31FF327AE73F958E457FC9FD7,
	U3CRotateLoopU3Ed__30_System_IDisposable_Dispose_m7B1E6C464438B8417A40174354B3C7601DAB847B,
	U3CRotateLoopU3Ed__30_MoveNext_m646DB27D1645A6FA001215B0B1976CFD6979493C,
	U3CRotateLoopU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1732E622798AE9D4E8703BB6FE814496054BF111,
	U3CRotateLoopU3Ed__30_System_Collections_IEnumerator_Reset_mE73D86C79C82BB4FF602E6AFF4298ECA2D3C76A1,
	U3CRotateLoopU3Ed__30_System_Collections_IEnumerator_get_Current_m444FA2997D49051D98499AE6B32523E26B215676,
	U3CScaleLoopU3Ed__21__ctor_mC4C7AE33BF54E66E5F4BF671C134B2B6D7F9F283,
	U3CScaleLoopU3Ed__21_System_IDisposable_Dispose_mDDE44E4854A9F1B178C346324681AD308C09B5F4,
	U3CScaleLoopU3Ed__21_MoveNext_mE68D5B0774F325725C4198101350521915BBC6A3,
	U3CScaleLoopU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9259BBDE0A6A86A02DA156A67BFC711FB28ADCC2,
	U3CScaleLoopU3Ed__21_System_Collections_IEnumerator_Reset_m5AD905B2CD21CFFEBD9AEF7EFC6CCE732A0D740B,
	U3CScaleLoopU3Ed__21_System_Collections_IEnumerator_get_Current_mDE7D535E617F6CC6DBC2DF9196B0D8EDC29EAEE1,
	U3CU3Ec__cctor_m5EC70CADD01FA899DEC2D921DA3D02F9F009E919,
	U3CU3Ec__ctor_m0DDDA10FC5F5534EEDEDC48344C5560DDF399BD4,
	U3CU3Ec_U3CInitStartEventsU3Eb__15_0_m22E3C03E533E74FC381E1D1C2559C6A97252BAD5,
	U3CU3Ec_U3CInitStopEventsU3Eb__16_1_mBE93AB88D92939DA4554ACC535EA395C177894B2,
	U3CU3Ec__DisplayClass16_0__ctor_m73FE5A20D51017316BF96A9B009F19FF6F466074,
	U3CU3Ec__DisplayClass16_0_U3CInitStopEventsU3Eb__0_m689EF9916E6CF5E196B2381265C9ABB3A002E499,
	U3CMoveCoinToScoreU3Ed__19__ctor_m1897FFC7E36D5E8237408CB5CF2055E1792B53FB,
	U3CMoveCoinToScoreU3Ed__19_System_IDisposable_Dispose_mC587A2A3A9498D7753589D19910AF163E5DBC44A,
	U3CMoveCoinToScoreU3Ed__19_MoveNext_m425A4C244FD323E9BECB40FBD8493C373D1420EC,
	U3CMoveCoinToScoreU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E66CFB9171902F256DAC022DB84F36B5E35DDB2,
	U3CMoveCoinToScoreU3Ed__19_System_Collections_IEnumerator_Reset_m1F016766162142F1FEAC200031F975E4A45EE82C,
	U3CMoveCoinToScoreU3Ed__19_System_Collections_IEnumerator_get_Current_mAEDB850E33A92251C40335DB0A0C712A2F8ED949,
	U3CWaitAndStartCoroutineU3Ed__1__ctor_m24A3B1D20DE39AFFA1A7F0358D0D1EE82A8CDE72,
	U3CWaitAndStartCoroutineU3Ed__1_System_IDisposable_Dispose_m879B92762BD598AF850B4B4326BA5BE2C280E52A,
	U3CWaitAndStartCoroutineU3Ed__1_MoveNext_mF5A0DD2BEC0B199C9B9095ABFD7CBCA03812EBD4,
	U3CWaitAndStartCoroutineU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m37131E6832CAE28F32E12CBDB34A981EEAB127F0,
	U3CWaitAndStartCoroutineU3Ed__1_System_Collections_IEnumerator_Reset_mDAC5D9148AE0870BC9D368DFE856B56E7632070B,
	U3CWaitAndStartCoroutineU3Ed__1_System_Collections_IEnumerator_get_Current_m3032B08B2E771E99DC72DB850F80996AD71E4967,
	U3CU3Ec__DisplayClass25_0__ctor_m74E6793F0381673A6BC729D92B778771BF010556,
	U3CU3Ec__DisplayClass25_0_U3CShowInterstitialU3Eb__0_m002FB2EA1F750B61A5ADB51DDF023EFCBA75B93D,
	U3CU3Ec__cctor_m939CF65CC4A1E0A1424351B4454FA6F3463ED7C3,
	U3CU3Ec__ctor_mD052D62EBD026E6DFC6895F1B952E54F7075E8E1,
	U3CU3Ec_U3C_ctorU3Eb__39_0_mCE9CC879498A9E16FFD45FC98D75306133245684,
	U3CU3Ec__DisplayClass57_0__ctor_m877836C7A514073462D0C55F8B82C7F673A7E08B,
	U3CU3Ec__DisplayClass57_0_U3ConInterstitialDismissedU3Eb__0_m338F2A5B7DBA400308EF101B5C6DA7915E407921,
	U3CU3Ec__DisplayClass58_0__ctor_mC2AF890F7407F12AB0E67B850986CE8031A152D1,
	U3CU3Ec__DisplayClass58_0_U3CinterstitialDidExpireU3Eb__0_mADA427D2C22537C788AA0BE9BCD5CF7E3FF67CB6,
	U3CU3Ec__DisplayClass63_0__ctor_m1C74A18A169F12F22ED45E86349B3F2093EA60DC,
	U3CU3Ec__DisplayClass63_0_U3ConRewardedVideoFailedU3Eb__0_m5095E87C8BE53B44D256CC60FBC6F6A5A417915A,
	U3CU3Ec__DisplayClass64_0__ctor_mBA42FA5D1BAD493A4CB3BB6F617EE67EEE2DCBEB,
	U3CU3Ec__DisplayClass64_0_U3ConRewardedVideoExpiredU3Eb__0_m5B8434485DA075F73EB1B177AF41975F43BF901E,
	U3CU3Ec__DisplayClass65_0__ctor_mA92A3313B3654E2A17A5A530F4021D4D6A6FCDB2,
	U3CU3Ec__DisplayClass65_0_U3ConRewardedVideoShownU3Eb__0_mE325F23CE91E81A551B4848AF08D4141E32533AF,
	U3CU3Ec__DisplayClass66_0__ctor_m1D6D4675E013FF727787AC4B087466951B12E519,
	U3CU3Ec__DisplayClass66_0_U3ConRewardedVideoFailedToPlayU3Eb__0_mF9B6055C65F772A633273F50A4C2A3B8F7E4EC5B,
};
static const int32_t s_InvokerIndices[2782] = 
{
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	18,
	9,
	1051,
	1051,
	658,
	13,
	1008,
	1008,
	13,
	13,
	1052,
	13,
	1052,
	13,
	13,
	13,
	13,
	13,
	13,
	1052,
	13,
	13,
	1052,
	1052,
	1667,
	1668,
	1008,
	13,
	13,
	13,
	13,
	13,
	13,
	30,
	30,
	122,
	1669,
	13,
	13,
	1670,
	1671,
	13,
	13,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	14,
	13,
	13,
	13,
	4,
	6,
	4,
	23,
	23,
	4,
	4,
	32,
	4,
	4,
	32,
	44,
	32,
	4,
	44,
	32,
	13,
	13,
	13,
	4,
	4,
	14,
	17,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	14,
	13,
	22,
	32,
	61,
	13,
	13,
	13,
	1672,
	1673,
	1077,
	1672,
	122,
	0,
	13,
	13,
	13,
	13,
	144,
	13,
	13,
	13,
	13,
	13,
	1674,
	1674,
	13,
	19,
	1675,
	1676,
	1677,
	1678,
	1679,
	1680,
	121,
	8,
	1681,
	1682,
	13,
	17,
	13,
	13,
	1674,
	13,
	13,
	1674,
	13,
	13,
	13,
	1674,
	1674,
	13,
	17,
	13,
	13,
	17,
	17,
	17,
	17,
	13,
	13,
	14,
	14,
	13,
	13,
	13,
	13,
	13,
	89,
	266,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	4,
	13,
	13,
	14,
	13,
	13,
	9,
	77,
	778,
	13,
	8,
	17,
	18,
	13,
	13,
	13,
	9,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	9,
	13,
	13,
	13,
	4,
	23,
	13,
	13,
	13,
	9,
	13,
	4,
	23,
	13,
	13,
	13,
	18,
	9,
	77,
	778,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	9,
	13,
	13,
	13,
	4,
	23,
	13,
	8,
	13,
	13,
	13,
	9,
	13,
	14,
	13,
	13,
	4,
	23,
	14,
	23,
	13,
	13,
	13,
	9,
	13,
	13,
	14,
	13,
	13,
	4,
	23,
	4,
	23,
	14,
	13,
	13,
	13,
	13,
	9,
	13,
	4,
	23,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	77,
	778,
	13,
	13,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	14,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	77,
	778,
	13,
	13,
	13,
	13,
	13,
	30,
	4,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	1063,
	1063,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	1052,
	277,
	13,
	1683,
	1171,
	8,
	544,
	1,
	1684,
	1685,
	0,
	378,
	518,
	121,
	13,
	13,
	13,
	13,
	1686,
	30,
	30,
	1686,
	30,
	1686,
	30,
	13,
	13,
	30,
	30,
	122,
	646,
	13,
	13,
	13,
	13,
	23,
	23,
	23,
	4,
	386,
	732,
	44,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	13,
	13,
	13,
	17,
	13,
	13,
	6,
	6,
	6,
	16,
	16,
	13,
	12,
	28,
	0,
	1211,
	311,
	122,
	1,
	1687,
	171,
	1,
	550,
	557,
	122,
	-1,
	2,
	322,
	1688,
	1689,
	1690,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	13,
	13,
	14,
	4,
	13,
	13,
	0,
	371,
	77,
	1171,
	13,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	13,
	4,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	13,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	4,
	14,
	4,
	14,
	14,
	17,
	44,
	13,
	1691,
	13,
	13,
	13,
	13,
	14,
	63,
	20,
	186,
	28,
	13,
	13,
	13,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	13,
	14,
	13,
	122,
	-1,
	-1,
	311,
	4,
	386,
	732,
	13,
	13,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	386,
	13,
	13,
	4,
	386,
	13,
	13,
	298,
	13,
	13,
	4,
	386,
	669,
	13,
	13,
	14,
	4,
	4,
	4,
	4,
	18,
	17,
	17,
	1693,
	13,
	13,
	18,
	864,
	9,
	13,
	13,
	13,
	30,
	4,
	32,
	13,
	14,
	13,
	8,
	77,
	4,
	658,
	277,
	658,
	277,
	658,
	277,
	14,
	4,
	13,
	587,
	4,
	176,
	176,
	44,
	17,
	277,
	277,
	277,
	13,
	-1,
	-1,
	77,
	4,
	13,
	587,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	14,
	587,
	4,
	13,
	4,
	13,
	6,
	373,
	373,
	156,
	23,
	202,
	14,
	14,
	63,
	63,
	13,
	1,
	2,
	1,
	0,
	0,
	0,
	13,
	13,
	13,
	14,
	4,
	4,
	838,
	6,
	13,
	8,
	163,
	13,
	16,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	0,
	0,
	8,
	122,
	122,
	30,
	311,
	8,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	13,
	587,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	19,
	1694,
	144,
	30,
	515,
	513,
	8,
	1695,
	311,
	19,
	8,
	144,
	144,
	30,
	30,
	154,
	154,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1696,
	122,
	14,
	13,
	13,
	13,
	372,
	1164,
	1142,
	1154,
	1146,
	372,
	372,
	74,
	1697,
	1698,
	1161,
	121,
	1699,
	1700,
	1700,
	19,
	13,
	13,
	1701,
	1210,
	1702,
	1211,
	30,
	4,
	4,
	6,
	13,
	8,
	17,
	13,
	13,
	13,
	13,
	1703,
	1704,
	1705,
	1706,
	1707,
	1708,
	30,
	1709,
	30,
	8,
	30,
	8,
	1710,
	1711,
	20,
	41,
	13,
	13,
	8,
	1075,
	1213,
	1075,
	1712,
	371,
	77,
	778,
	8,
	727,
	0,
	550,
	28,
	77,
	778,
	-1,
	-1,
	-1,
	12,
	0,
	1713,
	1713,
	0,
	1210,
	30,
	1211,
	518,
	8,
	1699,
	1696,
	122,
	0,
	8,
	13,
	13,
	13,
	428,
	1586,
	1083,
	417,
	916,
	61,
	9,
	13,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	13,
	13,
	13,
	121,
	8,
	1714,
	311,
	8,
	121,
	77,
	778,
	13,
	13,
	13,
	13,
	13,
	13,
	1715,
	277,
	13,
	1716,
	277,
	13,
	4,
	4,
	157,
	277,
	1717,
	277,
	61,
	13,
	13,
	13,
	44,
	277,
	13,
	1718,
	277,
	1085,
	845,
	6,
	6,
	13,
	1085,
	845,
	4,
	277,
	13,
	13,
	13,
	13,
	8,
	13,
	13,
	4,
	9,
	9,
	277,
	14,
	13,
	13,
	14,
	4,
	14,
	4,
	14,
	4,
	17,
	44,
	17,
	44,
	17,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	44,
	13,
	13,
	6,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	778,
	778,
	8,
	8,
	778,
	778,
	8,
	8,
	8,
	8,
	30,
	30,
	8,
	8,
	13,
	13,
	13,
	4,
	13,
	13,
	518,
	518,
	121,
	121,
	13,
	14,
	6,
	66,
	61,
	89,
	63,
	89,
	6,
	14,
	13,
	4,
	4,
	4,
	13,
	6,
	13,
	14,
	4,
	13,
	14,
	4,
	13,
	14,
	4,
	13,
	30,
	30,
	13,
	13,
	13,
	13,
	13,
	13,
	18,
	14,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	44,
	4,
	277,
	13,
	13,
	4,
	4,
	4,
	4,
	13,
	13,
	4,
	13,
	311,
	13,
	13,
	1719,
	13,
	1720,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	6,
	13,
	17,
	44,
	13,
	13,
	1720,
	9,
	4,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	17,
	44,
	61,
	661,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	4,
	9,
	13,
	13,
	4,
	9,
	9,
	13,
	661,
	13,
	13,
	4,
	13,
	13,
	1720,
	9,
	4,
	13,
	13,
	9,
	13,
	1052,
	13,
	13,
	13,
	13,
	13,
	14,
	1721,
	13,
	13,
	13,
	4,
	13,
	18,
	14,
	17,
	17,
	1242,
	1722,
	1723,
	1120,
	277,
	277,
	13,
	13,
	4,
	4,
	9,
	13,
	13,
	1720,
	9,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	9,
	1724,
	9,
	1061,
	1061,
	1242,
	1725,
	1120,
	13,
	18,
	658,
	658,
	13,
	13,
	14,
	13,
	1726,
	1727,
	13,
	23,
	13,
	13,
	1051,
	13,
	4,
	13,
	17,
	277,
	277,
	13,
	13,
	4,
	4,
	1728,
	9,
	13,
	13,
	1720,
	9,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	17,
	44,
	61,
	124,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	4,
	9,
	13,
	4,
	13,
	13,
	1729,
	13,
	13,
	4,
	13,
	13,
	1720,
	9,
	4,
	13,
	13,
	573,
	13,
	1052,
	13,
	13,
	13,
	13,
	17,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	19,
	19,
	19,
	13,
	13,
	14,
	13,
	13,
	13,
	4,
	6,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	658,
	13,
	13,
	13,
	13,
	13,
	13,
	18,
	18,
	658,
	13,
	13,
	277,
	63,
	13,
	13,
	17,
	44,
	13,
	13,
	13,
	14,
	4,
	13,
	13,
	13,
	13,
	13,
	14,
	14,
	13,
	13,
	13,
	13,
	17,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	6,
	-1,
	-1,
	-1,
	13,
	14,
	4,
	-1,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	4,
	13,
	13,
	13,
	13,
	14,
	4,
	13,
	13,
	13,
	13,
	44,
	1219,
	1730,
	13,
	13,
	13,
	13,
	13,
	13,
	277,
	277,
	277,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	658,
	17,
	1063,
	1063,
	13,
	277,
	277,
	13,
	277,
	277,
	13,
	13,
	4,
	13,
	14,
	17,
	44,
	4,
	13,
	13,
	4,
	13,
	13,
	4,
	4,
	6,
	13,
	13,
	0,
	13,
	8,
	13,
	13,
	4,
	13,
	13,
	4,
	4,
	13,
	13,
	13,
	14,
	13,
	1731,
	122,
	13,
	23,
	13,
	6,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	4,
	13,
	13,
	13,
	124,
	13,
	13,
	13,
	4,
	13,
	13,
	4,
	4,
	4,
	13,
	9,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	4,
	13,
	13,
	14,
	13,
	13,
	4,
	13,
	124,
	13,
	13,
	13,
	13,
	13,
	14,
	13,
	277,
	13,
	13,
	277,
	44,
	13,
	4,
	13,
	13,
	13,
	658,
	17,
	44,
	4,
	13,
	13,
	277,
	13,
	277,
	277,
	13,
	13,
	13,
	13,
	1219,
	9,
	63,
	13,
	4,
	4,
	4,
	13,
	4,
	13,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	307,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	4,
	4,
	13,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	17,
	44,
	17,
	44,
	658,
	277,
	13,
	13,
	18,
	9,
	4,
	13,
	13,
	13,
	13,
	13,
	277,
	13,
	13,
	277,
	13,
	587,
	13,
	4,
	13,
	9,
	13,
	13,
	4,
	4,
	13,
	13,
	13,
	4,
	13,
	13,
	4,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	4,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	6,
	13,
	0,
	13,
	0,
	13,
	0,
	13,
	17,
	44,
	13,
	4,
	17,
	17,
	13,
	13,
	13,
	13,
	13,
	13,
	9,
	9,
	9,
	63,
	14,
	4,
	4,
	13,
	4,
	13,
	17,
	13,
	13,
	4,
	17,
	13,
	13,
	14,
	18,
	658,
	18,
	9,
	17,
	44,
	17,
	44,
	13,
	-1,
	9,
	13,
	13,
	277,
	13,
	13,
	13,
	13,
	9,
	13,
	13,
	1642,
	9,
	1083,
	277,
	13,
	13,
	13,
	13,
	13,
	19,
	19,
	19,
	19,
	0,
	19,
	13,
	8,
	0,
	0,
	116,
	20,
	1045,
	186,
	8,
	18,
	63,
	89,
	6,
	23,
	14,
	4,
	18,
	17,
	17,
	17,
	17,
	17,
	17,
	17,
	44,
	23,
	4,
	6,
	63,
	6,
	14,
	14,
	32,
	16,
	14,
	63,
	150,
	1732,
	14,
	1733,
	1734,
	390,
	278,
	18,
	9,
	658,
	277,
	17,
	44,
	130,
	160,
	14,
	14,
	0,
	0,
	117,
	235,
	116,
	1045,
	20,
	186,
	828,
	26,
	745,
	28,
	1735,
	12,
	12,
	32,
	18,
	19,
	0,
	29,
	0,
	4,
	4,
	4,
	4,
	14,
	4,
	14,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	20,
	1736,
	1737,
	1738,
	1739,
	1740,
	0,
	1225,
	1741,
	1224,
	1742,
	1743,
	0,
	1456,
	1744,
	1063,
	1745,
	1008,
	1746,
	1051,
	1747,
	1748,
	1226,
	1749,
	1750,
	1220,
	1751,
	1752,
	1005,
	1753,
	6,
	14,
	6,
	1006,
	1754,
	13,
	8,
	17,
	44,
	18,
	17,
	1732,
	63,
	89,
	6,
	23,
	18,
	23,
	63,
	6,
	14,
	150,
	4,
	13,
	17,
	44,
	18,
	17,
	1732,
	6,
	23,
	63,
	89,
	18,
	23,
	6,
	63,
	6,
	32,
	16,
	14,
	150,
	4,
	13,
	18,
	17,
	1732,
	14,
	4,
	4,
	150,
	32,
	18,
	4,
	18,
	17,
	1732,
	14,
	4,
	390,
	278,
	130,
	160,
	278,
	4,
	150,
	28,
	32,
	18,
	4,
	18,
	17,
	1732,
	14,
	4,
	17,
	44,
	44,
	4,
	150,
	32,
	18,
	4,
	19,
	13,
	18,
	17,
	1732,
	14,
	4,
	17,
	44,
	32,
	18,
	150,
	4,
	8,
	18,
	1732,
	4,
	23,
	-1,
	63,
	89,
	6,
	23,
	4,
	23,
	12,
	12,
	32,
	18,
	18,
	9,
	658,
	277,
	390,
	278,
	130,
	160,
	17,
	44,
	14,
	14,
	150,
	4,
	0,
	14,
	18,
	13,
	4,
	124,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	14,
	4,
	4,
	4,
	4,
	4,
	13,
	13,
	13,
	277,
	13,
	1488,
	1455,
	13,
	13,
	4,
	4,
	13,
	13,
	13,
	277,
	13,
	277,
	1455,
	13,
	658,
	658,
	4,
	4,
	4,
	4,
	1051,
	13,
	1760,
	1761,
	1762,
	1763,
	13,
	13,
	17,
	277,
	1052,
	13,
	1119,
	1764,
	4,
	4,
	1765,
	4,
	1766,
	13,
	13,
	-1,
	13,
	13,
	17,
	44,
	4,
	13,
	13,
	13,
	13,
	4,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	14,
	18,
	658,
	18,
	9,
	14,
	17,
	44,
	17,
	44,
	13,
	13,
	13,
	13,
	13,
	-1,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	277,
	9,
	9,
	13,
	13,
	1642,
	9,
	1083,
	277,
	13,
	4,
	4,
	4,
	1235,
	1767,
	14,
	1003,
	1003,
	13,
	13,
	8,
	518,
	121,
	518,
	121,
	518,
	186,
	186,
	186,
	1768,
	270,
	1304,
	1768,
	270,
	1304,
	518,
	77,
	518,
	8,
	13,
	14,
	18,
	13,
	4,
	124,
	9,
	13,
	13,
	13,
	13,
	13,
	13,
	1769,
	152,
	0,
	0,
	13,
	13,
	1770,
	14,
	30,
	30,
	77,
	778,
	13,
	13,
	44,
	13,
	13,
	13,
	999,
	13,
	13,
	4,
	838,
	4,
	32,
	156,
	32,
	13,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	77,
	4,
	13,
	32,
	4,
	32,
	32,
	4,
	32,
	4,
	44,
	44,
	4,
	4,
	4,
	13,
	13,
	4,
	4,
	13,
	4,
	4,
	4,
	4,
	4,
	13,
	13,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	13,
	44,
	32,
	4,
	4,
	4,
	32,
	32,
	4,
	32,
	4,
	44,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	13,
	4,
	32,
	4,
	4,
	32,
	44,
	32,
	4,
	4,
	44,
	32,
	13,
	13,
	13,
	14,
	14,
	14,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	13,
	13,
	13,
	13,
	32,
	44,
	4,
	4,
	32,
	4,
	32,
	4,
	32,
	4,
	44,
	6,
	6,
	4,
	4,
	4,
	4,
	23,
	4,
	4,
	4,
	4,
	4,
	4,
	23,
	4,
	4,
	23,
	1771,
	4,
	4,
	13,
	122,
	1308,
	0,
	8,
	13,
	32,
	9,
	13,
	17,
	14,
	13,
	14,
	156,
	8,
	13,
	6,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	8,
	13,
	13,
	1052,
	277,
	277,
	1052,
	13,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	8,
	13,
	13,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	8,
	13,
	1692,
	1692,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	8,
	13,
	9,
	9,
	13,
	17,
	14,
	13,
	14,
	163,
	13,
	16,
	4,
	163,
	17,
	16,
	32,
	163,
	17,
	16,
	32,
	-1,
	-1,
	-1,
	-1,
	4,
	0,
	4,
	4,
	163,
	13,
	16,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	13,
	13,
	13,
	163,
	13,
	16,
	4,
	163,
	4,
	166,
	4,
	13,
	13,
	13,
	4,
	14,
	14,
	13,
	17,
	44,
	658,
	658,
	658,
	658,
	658,
	658,
	658,
	658,
	658,
	658,
	18,
	18,
	18,
	9,
	17,
	14,
	14,
	17,
	44,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	13,
	13,
	4,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	8,
	13,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	13,
	8,
	13,
	13,
	13,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	8,
	13,
	13,
	9,
	13,
	17,
	13,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	13,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	13,
	13,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	17,
	1755,
	1756,
	1757,
	17,
	1755,
	1756,
	1758,
	14,
	17,
	1734,
	1755,
	1756,
	1758,
	14,
	17,
	1733,
	4,
	1757,
	14,
	17,
	13,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	14,
	14,
	9,
	13,
	17,
	13,
	13,
	14,
	13,
	14,
	14,
	14,
	9,
	13,
	17,
	13,
	14,
	13,
	14,
	14,
	14,
	13,
	1759,
	9,
	13,
	17,
	13,
	14,
	13,
	14,
	14,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	8,
	13,
	13,
	13,
	13,
	13,
	9,
	13,
	17,
	14,
	13,
	14,
	9,
	13,
	17,
	14,
	13,
	14,
	13,
	4,
	8,
	13,
	44,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
	13,
};
static const Il2CppTokenRangePair s_rgctxIndices[60] = 
{
	{ 0x02000047, { 2, 4 } },
	{ 0x02000048, { 6, 12 } },
	{ 0x02000049, { 18, 5 } },
	{ 0x02000052, { 23, 8 } },
	{ 0x02000053, { 34, 9 } },
	{ 0x02000054, { 46, 10 } },
	{ 0x02000055, { 59, 11 } },
	{ 0x02000056, { 73, 12 } },
	{ 0x02000059, { 88, 7 } },
	{ 0x0200005A, { 98, 8 } },
	{ 0x0200005B, { 109, 9 } },
	{ 0x0200005C, { 121, 10 } },
	{ 0x0200005E, { 134, 7 } },
	{ 0x0200005F, { 141, 7 } },
	{ 0x02000060, { 148, 7 } },
	{ 0x02000061, { 155, 7 } },
	{ 0x02000062, { 162, 7 } },
	{ 0x02000063, { 169, 2 } },
	{ 0x02000067, { 171, 3 } },
	{ 0x02000068, { 174, 3 } },
	{ 0x02000069, { 177, 3 } },
	{ 0x0200006A, { 180, 3 } },
	{ 0x0200006D, { 183, 3 } },
	{ 0x02000070, { 186, 17 } },
	{ 0x02000089, { 204, 12 } },
	{ 0x0200008A, { 216, 13 } },
	{ 0x0200008B, { 229, 14 } },
	{ 0x0200008C, { 243, 11 } },
	{ 0x0200008D, { 254, 12 } },
	{ 0x0200008E, { 266, 13 } },
	{ 0x0200008F, { 279, 14 } },
	{ 0x02000096, { 293, 2 } },
	{ 0x02000097, { 295, 2 } },
	{ 0x02000098, { 297, 2 } },
	{ 0x02000099, { 299, 2 } },
	{ 0x0200009A, { 301, 2 } },
	{ 0x0200009B, { 303, 2 } },
	{ 0x0200009C, { 305, 2 } },
	{ 0x020000A8, { 317, 2 } },
	{ 0x02000186, { 31, 3 } },
	{ 0x02000187, { 43, 3 } },
	{ 0x02000188, { 56, 3 } },
	{ 0x02000189, { 70, 3 } },
	{ 0x0200018A, { 85, 3 } },
	{ 0x0200018C, { 95, 3 } },
	{ 0x0200018D, { 106, 3 } },
	{ 0x0200018E, { 118, 3 } },
	{ 0x0200018F, { 131, 3 } },
	{ 0x060001BE, { 0, 2 } },
	{ 0x0600028F, { 203, 1 } },
	{ 0x06000397, { 307, 3 } },
	{ 0x06000398, { 310, 5 } },
	{ 0x06000399, { 315, 2 } },
	{ 0x0600058D, { 319, 2 } },
	{ 0x0600058E, { 321, 2 } },
	{ 0x0600058F, { 323, 2 } },
	{ 0x06000593, { 325, 1 } },
	{ 0x060006D3, { 326, 5 } },
	{ 0x060007C9, { 331, 1 } },
	{ 0x06000850, { 332, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[337] = 
{
	{ (Il2CppRGCTXDataType)1, 16573 },
	{ (Il2CppRGCTXDataType)2, 16573 },
	{ (Il2CppRGCTXDataType)3, 10656 },
	{ (Il2CppRGCTXDataType)3, 10657 },
	{ (Il2CppRGCTXDataType)3, 10658 },
	{ (Il2CppRGCTXDataType)2, 16578 },
	{ (Il2CppRGCTXDataType)3, 10659 },
	{ (Il2CppRGCTXDataType)3, 10660 },
	{ (Il2CppRGCTXDataType)2, 16583 },
	{ (Il2CppRGCTXDataType)3, 10661 },
	{ (Il2CppRGCTXDataType)3, 10662 },
	{ (Il2CppRGCTXDataType)3, 10663 },
	{ (Il2CppRGCTXDataType)3, 10664 },
	{ (Il2CppRGCTXDataType)3, 10665 },
	{ (Il2CppRGCTXDataType)3, 10666 },
	{ (Il2CppRGCTXDataType)3, 10667 },
	{ (Il2CppRGCTXDataType)2, 18039 },
	{ (Il2CppRGCTXDataType)3, 10668 },
	{ (Il2CppRGCTXDataType)2, 16590 },
	{ (Il2CppRGCTXDataType)3, 10669 },
	{ (Il2CppRGCTXDataType)3, 10670 },
	{ (Il2CppRGCTXDataType)3, 10671 },
	{ (Il2CppRGCTXDataType)2, 16589 },
	{ (Il2CppRGCTXDataType)3, 10672 },
	{ (Il2CppRGCTXDataType)3, 10673 },
	{ (Il2CppRGCTXDataType)2, 16608 },
	{ (Il2CppRGCTXDataType)2, 18040 },
	{ (Il2CppRGCTXDataType)3, 10674 },
	{ (Il2CppRGCTXDataType)2, 18041 },
	{ (Il2CppRGCTXDataType)3, 10675 },
	{ (Il2CppRGCTXDataType)1, 18041 },
	{ (Il2CppRGCTXDataType)2, 18042 },
	{ (Il2CppRGCTXDataType)3, 10676 },
	{ (Il2CppRGCTXDataType)2, 18042 },
	{ (Il2CppRGCTXDataType)3, 10677 },
	{ (Il2CppRGCTXDataType)2, 16619 },
	{ (Il2CppRGCTXDataType)3, 10678 },
	{ (Il2CppRGCTXDataType)2, 16617 },
	{ (Il2CppRGCTXDataType)2, 18043 },
	{ (Il2CppRGCTXDataType)3, 10679 },
	{ (Il2CppRGCTXDataType)2, 18044 },
	{ (Il2CppRGCTXDataType)3, 10680 },
	{ (Il2CppRGCTXDataType)1, 18044 },
	{ (Il2CppRGCTXDataType)2, 18045 },
	{ (Il2CppRGCTXDataType)3, 10681 },
	{ (Il2CppRGCTXDataType)2, 18045 },
	{ (Il2CppRGCTXDataType)3, 10682 },
	{ (Il2CppRGCTXDataType)2, 16630 },
	{ (Il2CppRGCTXDataType)2, 16631 },
	{ (Il2CppRGCTXDataType)3, 10683 },
	{ (Il2CppRGCTXDataType)2, 16628 },
	{ (Il2CppRGCTXDataType)2, 18046 },
	{ (Il2CppRGCTXDataType)3, 10684 },
	{ (Il2CppRGCTXDataType)2, 18047 },
	{ (Il2CppRGCTXDataType)3, 10685 },
	{ (Il2CppRGCTXDataType)1, 18047 },
	{ (Il2CppRGCTXDataType)2, 18048 },
	{ (Il2CppRGCTXDataType)3, 10686 },
	{ (Il2CppRGCTXDataType)2, 18048 },
	{ (Il2CppRGCTXDataType)3, 10687 },
	{ (Il2CppRGCTXDataType)2, 16643 },
	{ (Il2CppRGCTXDataType)2, 16644 },
	{ (Il2CppRGCTXDataType)2, 16645 },
	{ (Il2CppRGCTXDataType)3, 10688 },
	{ (Il2CppRGCTXDataType)2, 16641 },
	{ (Il2CppRGCTXDataType)2, 18049 },
	{ (Il2CppRGCTXDataType)3, 10689 },
	{ (Il2CppRGCTXDataType)2, 18050 },
	{ (Il2CppRGCTXDataType)3, 10690 },
	{ (Il2CppRGCTXDataType)1, 18050 },
	{ (Il2CppRGCTXDataType)2, 18051 },
	{ (Il2CppRGCTXDataType)3, 10691 },
	{ (Il2CppRGCTXDataType)2, 18051 },
	{ (Il2CppRGCTXDataType)3, 10692 },
	{ (Il2CppRGCTXDataType)2, 16658 },
	{ (Il2CppRGCTXDataType)2, 16659 },
	{ (Il2CppRGCTXDataType)2, 16660 },
	{ (Il2CppRGCTXDataType)2, 16661 },
	{ (Il2CppRGCTXDataType)3, 10693 },
	{ (Il2CppRGCTXDataType)2, 16656 },
	{ (Il2CppRGCTXDataType)2, 18052 },
	{ (Il2CppRGCTXDataType)3, 10694 },
	{ (Il2CppRGCTXDataType)2, 18053 },
	{ (Il2CppRGCTXDataType)3, 10695 },
	{ (Il2CppRGCTXDataType)1, 18053 },
	{ (Il2CppRGCTXDataType)2, 18054 },
	{ (Il2CppRGCTXDataType)3, 10696 },
	{ (Il2CppRGCTXDataType)2, 18054 },
	{ (Il2CppRGCTXDataType)3, 10697 },
	{ (Il2CppRGCTXDataType)2, 16681 },
	{ (Il2CppRGCTXDataType)2, 18055 },
	{ (Il2CppRGCTXDataType)3, 10698 },
	{ (Il2CppRGCTXDataType)2, 18056 },
	{ (Il2CppRGCTXDataType)3, 10699 },
	{ (Il2CppRGCTXDataType)1, 18056 },
	{ (Il2CppRGCTXDataType)2, 18057 },
	{ (Il2CppRGCTXDataType)3, 10700 },
	{ (Il2CppRGCTXDataType)2, 18057 },
	{ (Il2CppRGCTXDataType)3, 10701 },
	{ (Il2CppRGCTXDataType)2, 16689 },
	{ (Il2CppRGCTXDataType)2, 16690 },
	{ (Il2CppRGCTXDataType)2, 18058 },
	{ (Il2CppRGCTXDataType)3, 10702 },
	{ (Il2CppRGCTXDataType)2, 18059 },
	{ (Il2CppRGCTXDataType)3, 10703 },
	{ (Il2CppRGCTXDataType)1, 18059 },
	{ (Il2CppRGCTXDataType)2, 18060 },
	{ (Il2CppRGCTXDataType)3, 10704 },
	{ (Il2CppRGCTXDataType)2, 18060 },
	{ (Il2CppRGCTXDataType)3, 10705 },
	{ (Il2CppRGCTXDataType)2, 16699 },
	{ (Il2CppRGCTXDataType)2, 16700 },
	{ (Il2CppRGCTXDataType)2, 16701 },
	{ (Il2CppRGCTXDataType)2, 18061 },
	{ (Il2CppRGCTXDataType)3, 10706 },
	{ (Il2CppRGCTXDataType)2, 18062 },
	{ (Il2CppRGCTXDataType)3, 10707 },
	{ (Il2CppRGCTXDataType)1, 18062 },
	{ (Il2CppRGCTXDataType)2, 18063 },
	{ (Il2CppRGCTXDataType)3, 10708 },
	{ (Il2CppRGCTXDataType)2, 18063 },
	{ (Il2CppRGCTXDataType)3, 10709 },
	{ (Il2CppRGCTXDataType)2, 16711 },
	{ (Il2CppRGCTXDataType)2, 16712 },
	{ (Il2CppRGCTXDataType)2, 16713 },
	{ (Il2CppRGCTXDataType)2, 16714 },
	{ (Il2CppRGCTXDataType)2, 18064 },
	{ (Il2CppRGCTXDataType)3, 10710 },
	{ (Il2CppRGCTXDataType)2, 18065 },
	{ (Il2CppRGCTXDataType)3, 10711 },
	{ (Il2CppRGCTXDataType)1, 18065 },
	{ (Il2CppRGCTXDataType)2, 18066 },
	{ (Il2CppRGCTXDataType)3, 10712 },
	{ (Il2CppRGCTXDataType)2, 18066 },
	{ (Il2CppRGCTXDataType)2, 18067 },
	{ (Il2CppRGCTXDataType)3, 10713 },
	{ (Il2CppRGCTXDataType)3, 10714 },
	{ (Il2CppRGCTXDataType)3, 10715 },
	{ (Il2CppRGCTXDataType)3, 10716 },
	{ (Il2CppRGCTXDataType)3, 10717 },
	{ (Il2CppRGCTXDataType)2, 16727 },
	{ (Il2CppRGCTXDataType)2, 18068 },
	{ (Il2CppRGCTXDataType)3, 10718 },
	{ (Il2CppRGCTXDataType)3, 10719 },
	{ (Il2CppRGCTXDataType)3, 10720 },
	{ (Il2CppRGCTXDataType)3, 10721 },
	{ (Il2CppRGCTXDataType)3, 10722 },
	{ (Il2CppRGCTXDataType)2, 16731 },
	{ (Il2CppRGCTXDataType)2, 18069 },
	{ (Il2CppRGCTXDataType)3, 10723 },
	{ (Il2CppRGCTXDataType)3, 10724 },
	{ (Il2CppRGCTXDataType)3, 10725 },
	{ (Il2CppRGCTXDataType)3, 10726 },
	{ (Il2CppRGCTXDataType)3, 10727 },
	{ (Il2CppRGCTXDataType)2, 16736 },
	{ (Il2CppRGCTXDataType)2, 18070 },
	{ (Il2CppRGCTXDataType)3, 10728 },
	{ (Il2CppRGCTXDataType)3, 10729 },
	{ (Il2CppRGCTXDataType)3, 10730 },
	{ (Il2CppRGCTXDataType)3, 10731 },
	{ (Il2CppRGCTXDataType)3, 10732 },
	{ (Il2CppRGCTXDataType)2, 16742 },
	{ (Il2CppRGCTXDataType)2, 18071 },
	{ (Il2CppRGCTXDataType)3, 10733 },
	{ (Il2CppRGCTXDataType)3, 10734 },
	{ (Il2CppRGCTXDataType)3, 10735 },
	{ (Il2CppRGCTXDataType)3, 10736 },
	{ (Il2CppRGCTXDataType)3, 10737 },
	{ (Il2CppRGCTXDataType)2, 16749 },
	{ (Il2CppRGCTXDataType)1, 16759 },
	{ (Il2CppRGCTXDataType)2, 16758 },
	{ (Il2CppRGCTXDataType)2, 18072 },
	{ (Il2CppRGCTXDataType)3, 10738 },
	{ (Il2CppRGCTXDataType)3, 10739 },
	{ (Il2CppRGCTXDataType)2, 18073 },
	{ (Il2CppRGCTXDataType)3, 10740 },
	{ (Il2CppRGCTXDataType)3, 10741 },
	{ (Il2CppRGCTXDataType)2, 18074 },
	{ (Il2CppRGCTXDataType)3, 10742 },
	{ (Il2CppRGCTXDataType)3, 10743 },
	{ (Il2CppRGCTXDataType)2, 18075 },
	{ (Il2CppRGCTXDataType)3, 10744 },
	{ (Il2CppRGCTXDataType)3, 10745 },
	{ (Il2CppRGCTXDataType)2, 18076 },
	{ (Il2CppRGCTXDataType)2, 16805 },
	{ (Il2CppRGCTXDataType)3, 10746 },
	{ (Il2CppRGCTXDataType)2, 16816 },
	{ (Il2CppRGCTXDataType)3, 10747 },
	{ (Il2CppRGCTXDataType)2, 16818 },
	{ (Il2CppRGCTXDataType)3, 10748 },
	{ (Il2CppRGCTXDataType)2, 16819 },
	{ (Il2CppRGCTXDataType)3, 10749 },
	{ (Il2CppRGCTXDataType)3, 10750 },
	{ (Il2CppRGCTXDataType)3, 10751 },
	{ (Il2CppRGCTXDataType)3, 10752 },
	{ (Il2CppRGCTXDataType)3, 10753 },
	{ (Il2CppRGCTXDataType)3, 10754 },
	{ (Il2CppRGCTXDataType)3, 10755 },
	{ (Il2CppRGCTXDataType)3, 10756 },
	{ (Il2CppRGCTXDataType)3, 10757 },
	{ (Il2CppRGCTXDataType)3, 10758 },
	{ (Il2CppRGCTXDataType)3, 10759 },
	{ (Il2CppRGCTXDataType)3, 10760 },
	{ (Il2CppRGCTXDataType)2, 16862 },
	{ (Il2CppRGCTXDataType)2, 18077 },
	{ (Il2CppRGCTXDataType)2, 18078 },
	{ (Il2CppRGCTXDataType)3, 10761 },
	{ (Il2CppRGCTXDataType)3, 10762 },
	{ (Il2CppRGCTXDataType)3, 10763 },
	{ (Il2CppRGCTXDataType)3, 10764 },
	{ (Il2CppRGCTXDataType)2, 16935 },
	{ (Il2CppRGCTXDataType)3, 10765 },
	{ (Il2CppRGCTXDataType)3, 10766 },
	{ (Il2CppRGCTXDataType)3, 10767 },
	{ (Il2CppRGCTXDataType)3, 10768 },
	{ (Il2CppRGCTXDataType)2, 18079 },
	{ (Il2CppRGCTXDataType)2, 18080 },
	{ (Il2CppRGCTXDataType)2, 18081 },
	{ (Il2CppRGCTXDataType)3, 10769 },
	{ (Il2CppRGCTXDataType)3, 10770 },
	{ (Il2CppRGCTXDataType)3, 10771 },
	{ (Il2CppRGCTXDataType)3, 10772 },
	{ (Il2CppRGCTXDataType)2, 16939 },
	{ (Il2CppRGCTXDataType)2, 16940 },
	{ (Il2CppRGCTXDataType)3, 10773 },
	{ (Il2CppRGCTXDataType)3, 10774 },
	{ (Il2CppRGCTXDataType)3, 10775 },
	{ (Il2CppRGCTXDataType)3, 10776 },
	{ (Il2CppRGCTXDataType)2, 18082 },
	{ (Il2CppRGCTXDataType)2, 18083 },
	{ (Il2CppRGCTXDataType)2, 18084 },
	{ (Il2CppRGCTXDataType)3, 10777 },
	{ (Il2CppRGCTXDataType)3, 10778 },
	{ (Il2CppRGCTXDataType)3, 10779 },
	{ (Il2CppRGCTXDataType)3, 10780 },
	{ (Il2CppRGCTXDataType)2, 16944 },
	{ (Il2CppRGCTXDataType)2, 16945 },
	{ (Il2CppRGCTXDataType)2, 16946 },
	{ (Il2CppRGCTXDataType)3, 10781 },
	{ (Il2CppRGCTXDataType)3, 10782 },
	{ (Il2CppRGCTXDataType)3, 10783 },
	{ (Il2CppRGCTXDataType)3, 10784 },
	{ (Il2CppRGCTXDataType)2, 18085 },
	{ (Il2CppRGCTXDataType)2, 18086 },
	{ (Il2CppRGCTXDataType)2, 18087 },
	{ (Il2CppRGCTXDataType)3, 10785 },
	{ (Il2CppRGCTXDataType)3, 10786 },
	{ (Il2CppRGCTXDataType)3, 10787 },
	{ (Il2CppRGCTXDataType)3, 10788 },
	{ (Il2CppRGCTXDataType)3, 10789 },
	{ (Il2CppRGCTXDataType)3, 10790 },
	{ (Il2CppRGCTXDataType)3, 10791 },
	{ (Il2CppRGCTXDataType)3, 10792 },
	{ (Il2CppRGCTXDataType)2, 18088 },
	{ (Il2CppRGCTXDataType)2, 18089 },
	{ (Il2CppRGCTXDataType)2, 18090 },
	{ (Il2CppRGCTXDataType)3, 10793 },
	{ (Il2CppRGCTXDataType)3, 10794 },
	{ (Il2CppRGCTXDataType)3, 10795 },
	{ (Il2CppRGCTXDataType)3, 10796 },
	{ (Il2CppRGCTXDataType)2, 16954 },
	{ (Il2CppRGCTXDataType)3, 10797 },
	{ (Il2CppRGCTXDataType)3, 10798 },
	{ (Il2CppRGCTXDataType)3, 10799 },
	{ (Il2CppRGCTXDataType)3, 10800 },
	{ (Il2CppRGCTXDataType)2, 18091 },
	{ (Il2CppRGCTXDataType)2, 18092 },
	{ (Il2CppRGCTXDataType)2, 18093 },
	{ (Il2CppRGCTXDataType)3, 10801 },
	{ (Il2CppRGCTXDataType)3, 10802 },
	{ (Il2CppRGCTXDataType)3, 10803 },
	{ (Il2CppRGCTXDataType)3, 10804 },
	{ (Il2CppRGCTXDataType)2, 16959 },
	{ (Il2CppRGCTXDataType)2, 16960 },
	{ (Il2CppRGCTXDataType)3, 10805 },
	{ (Il2CppRGCTXDataType)3, 10806 },
	{ (Il2CppRGCTXDataType)3, 10807 },
	{ (Il2CppRGCTXDataType)3, 10808 },
	{ (Il2CppRGCTXDataType)2, 18094 },
	{ (Il2CppRGCTXDataType)2, 18095 },
	{ (Il2CppRGCTXDataType)2, 18096 },
	{ (Il2CppRGCTXDataType)3, 10809 },
	{ (Il2CppRGCTXDataType)3, 10810 },
	{ (Il2CppRGCTXDataType)3, 10811 },
	{ (Il2CppRGCTXDataType)3, 10812 },
	{ (Il2CppRGCTXDataType)2, 16965 },
	{ (Il2CppRGCTXDataType)2, 16966 },
	{ (Il2CppRGCTXDataType)2, 16967 },
	{ (Il2CppRGCTXDataType)3, 10813 },
	{ (Il2CppRGCTXDataType)3, 10814 },
	{ (Il2CppRGCTXDataType)3, 10815 },
	{ (Il2CppRGCTXDataType)3, 10816 },
	{ (Il2CppRGCTXDataType)2, 18097 },
	{ (Il2CppRGCTXDataType)2, 16999 },
	{ (Il2CppRGCTXDataType)3, 10817 },
	{ (Il2CppRGCTXDataType)2, 17003 },
	{ (Il2CppRGCTXDataType)3, 10818 },
	{ (Il2CppRGCTXDataType)2, 17008 },
	{ (Il2CppRGCTXDataType)3, 10819 },
	{ (Il2CppRGCTXDataType)2, 17014 },
	{ (Il2CppRGCTXDataType)3, 10820 },
	{ (Il2CppRGCTXDataType)2, 17018 },
	{ (Il2CppRGCTXDataType)3, 10821 },
	{ (Il2CppRGCTXDataType)2, 17023 },
	{ (Il2CppRGCTXDataType)3, 10822 },
	{ (Il2CppRGCTXDataType)2, 17029 },
	{ (Il2CppRGCTXDataType)3, 10823 },
	{ (Il2CppRGCTXDataType)3, 10824 },
	{ (Il2CppRGCTXDataType)3, 10825 },
	{ (Il2CppRGCTXDataType)3, 10826 },
	{ (Il2CppRGCTXDataType)2, 17090 },
	{ (Il2CppRGCTXDataType)3, 10827 },
	{ (Il2CppRGCTXDataType)3, 10828 },
	{ (Il2CppRGCTXDataType)3, 10829 },
	{ (Il2CppRGCTXDataType)3, 10830 },
	{ (Il2CppRGCTXDataType)3, 10831 },
	{ (Il2CppRGCTXDataType)3, 10832 },
	{ (Il2CppRGCTXDataType)3, 10833 },
	{ (Il2CppRGCTXDataType)2, 18098 },
	{ (Il2CppRGCTXDataType)1, 17439 },
	{ (Il2CppRGCTXDataType)2, 17439 },
	{ (Il2CppRGCTXDataType)3, 10834 },
	{ (Il2CppRGCTXDataType)2, 18099 },
	{ (Il2CppRGCTXDataType)3, 10835 },
	{ (Il2CppRGCTXDataType)2, 18100 },
	{ (Il2CppRGCTXDataType)3, 10836 },
	{ (Il2CppRGCTXDataType)3, 10837 },
	{ (Il2CppRGCTXDataType)3, 10838 },
	{ (Il2CppRGCTXDataType)2, 17582 },
	{ (Il2CppRGCTXDataType)3, 10839 },
	{ (Il2CppRGCTXDataType)2, 18101 },
	{ (Il2CppRGCTXDataType)2, 17652 },
	{ (Il2CppRGCTXDataType)3, 10840 },
	{ (Il2CppRGCTXDataType)3, 10841 },
	{ (Il2CppRGCTXDataType)2, 17737 },
	{ (Il2CppRGCTXDataType)3, 10842 },
	{ (Il2CppRGCTXDataType)2, 18102 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	2782,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	60,
	s_rgctxIndices,
	337,
	s_rgctxValues,
	NULL,
};
