﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Object Google.MiniJSON.Json::Deserialize(System.String)
extern void Json_Deserialize_mD699722297018E092C83389766941F0529549E60 ();
// 0x00000002 System.String Google.MiniJSON.Json::Serialize(System.Object)
extern void Json_Serialize_m812D93C71C3FA6B3063656B173CEE1165F877881 ();
// 0x00000003 System.Void Google.MiniJSON.Json_Parser::.ctor(System.String)
extern void Parser__ctor_m916CFE212FBBEBFA158603FB6548B6CA0361EAAE ();
// 0x00000004 System.Boolean Google.MiniJSON.Json_Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_mA56D686E73A8A690F7812A696645712E9580CC35 ();
// 0x00000005 System.Object Google.MiniJSON.Json_Parser::Parse(System.String)
extern void Parser_Parse_mB6472FC35FA562056210D8EFC45A38C5D18CD6DC ();
// 0x00000006 System.Void Google.MiniJSON.Json_Parser::Dispose()
extern void Parser_Dispose_m37B29BB65B022FE063F4007FD639B63627DE3FF2 ();
// 0x00000007 System.Collections.Generic.Dictionary`2<System.String,System.Object> Google.MiniJSON.Json_Parser::ParseObject()
extern void Parser_ParseObject_m9539B6152B785B5A8BBDE819F07C5CD716A0B36D ();
// 0x00000008 System.Collections.Generic.List`1<System.Object> Google.MiniJSON.Json_Parser::ParseArray()
extern void Parser_ParseArray_mA2E28FF2721815E11BC2C1B2AE3DD6EF6EF86B48 ();
// 0x00000009 System.Object Google.MiniJSON.Json_Parser::ParseValue()
extern void Parser_ParseValue_mE9382FACE65E715DD2B560F55FAFDC3C6161F484 ();
// 0x0000000A System.Object Google.MiniJSON.Json_Parser::ParseByToken(Google.MiniJSON.Json_Parser_TOKEN)
extern void Parser_ParseByToken_m29A07098C4DC1CC2F2D29332B6B8F135E9B49331 ();
// 0x0000000B System.String Google.MiniJSON.Json_Parser::ParseString()
extern void Parser_ParseString_m7FE7D383B751B8E39E21903319B91709157980AD ();
// 0x0000000C System.Object Google.MiniJSON.Json_Parser::ParseNumber()
extern void Parser_ParseNumber_mA9B7CDB0673BE5B7C64FCA8F176D8840A44FAA4D ();
// 0x0000000D System.Void Google.MiniJSON.Json_Parser::EatWhitespace()
extern void Parser_EatWhitespace_mB59675843F9F28AE3140C97AA7B51CB4A4C6DF9A ();
// 0x0000000E System.Char Google.MiniJSON.Json_Parser::get_PeekChar()
extern void Parser_get_PeekChar_mD86ED0CC323770598B17362DA900149B9210CE9F ();
// 0x0000000F System.Char Google.MiniJSON.Json_Parser::get_NextChar()
extern void Parser_get_NextChar_m2553FEB9993011BED7AB4490BE8E19FF302AF984 ();
// 0x00000010 System.String Google.MiniJSON.Json_Parser::get_NextWord()
extern void Parser_get_NextWord_mE4E2AA81FD95EF52C134C8CBAB734FE1C5B4FA4A ();
// 0x00000011 Google.MiniJSON.Json_Parser_TOKEN Google.MiniJSON.Json_Parser::get_NextToken()
extern void Parser_get_NextToken_m10D54939930F8CFCEB19987B4255107376DDAD74 ();
// 0x00000012 System.Void Google.MiniJSON.Json_Serializer::.ctor()
extern void Serializer__ctor_mD420791CC0D2723F65F2D3550B32801D6002F8DA ();
// 0x00000013 System.String Google.MiniJSON.Json_Serializer::Serialize(System.Object)
extern void Serializer_Serialize_mFF11D2C9DE95A3BB9132FE2A50564A7E25C5100E ();
// 0x00000014 System.Void Google.MiniJSON.Json_Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_mBC4563AEE3C5D1CE9186D1F25CB9F7DFEF977137 ();
// 0x00000015 System.Void Google.MiniJSON.Json_Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m84BEC8075B43CFA38A8CD7E7BD92220557A076AF ();
// 0x00000016 System.Void Google.MiniJSON.Json_Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m6289013E1766271284D3A3424E2955541E85D4C0 ();
// 0x00000017 System.Void Google.MiniJSON.Json_Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m6A2053F4742789C7F22FE8CB3E1A0EFA1DDA6F27 ();
// 0x00000018 System.Void Google.MiniJSON.Json_Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_m115717B8346A3A026ED5B9959BC81CCB751E6F40 ();
static Il2CppMethodPointer s_methodPointers[24] = 
{
	Json_Deserialize_mD699722297018E092C83389766941F0529549E60,
	Json_Serialize_m812D93C71C3FA6B3063656B173CEE1165F877881,
	Parser__ctor_m916CFE212FBBEBFA158603FB6548B6CA0361EAAE,
	Parser_IsWordBreak_mA56D686E73A8A690F7812A696645712E9580CC35,
	Parser_Parse_mB6472FC35FA562056210D8EFC45A38C5D18CD6DC,
	Parser_Dispose_m37B29BB65B022FE063F4007FD639B63627DE3FF2,
	Parser_ParseObject_m9539B6152B785B5A8BBDE819F07C5CD716A0B36D,
	Parser_ParseArray_mA2E28FF2721815E11BC2C1B2AE3DD6EF6EF86B48,
	Parser_ParseValue_mE9382FACE65E715DD2B560F55FAFDC3C6161F484,
	Parser_ParseByToken_m29A07098C4DC1CC2F2D29332B6B8F135E9B49331,
	Parser_ParseString_m7FE7D383B751B8E39E21903319B91709157980AD,
	Parser_ParseNumber_mA9B7CDB0673BE5B7C64FCA8F176D8840A44FAA4D,
	Parser_EatWhitespace_mB59675843F9F28AE3140C97AA7B51CB4A4C6DF9A,
	Parser_get_PeekChar_mD86ED0CC323770598B17362DA900149B9210CE9F,
	Parser_get_NextChar_m2553FEB9993011BED7AB4490BE8E19FF302AF984,
	Parser_get_NextWord_mE4E2AA81FD95EF52C134C8CBAB734FE1C5B4FA4A,
	Parser_get_NextToken_m10D54939930F8CFCEB19987B4255107376DDAD74,
	Serializer__ctor_mD420791CC0D2723F65F2D3550B32801D6002F8DA,
	Serializer_Serialize_mFF11D2C9DE95A3BB9132FE2A50564A7E25C5100E,
	Serializer_SerializeValue_mBC4563AEE3C5D1CE9186D1F25CB9F7DFEF977137,
	Serializer_SerializeObject_m84BEC8075B43CFA38A8CD7E7BD92220557A076AF,
	Serializer_SerializeArray_m6289013E1766271284D3A3424E2955541E85D4C0,
	Serializer_SerializeString_m6A2053F4742789C7F22FE8CB3E1A0EFA1DDA6F27,
	Serializer_SerializeOther_m115717B8346A3A026ED5B9959BC81CCB751E6F40,
};
static const int32_t s_InvokerIndices[24] = 
{
	0,
	0,
	4,
	76,
	0,
	13,
	14,
	14,
	14,
	63,
	14,
	14,
	13,
	201,
	201,
	14,
	18,
	13,
	0,
	4,
	4,
	4,
	4,
	4,
};
extern const Il2CppCodeGenModule g_Google_MiniJsonCodeGenModule;
const Il2CppCodeGenModule g_Google_MiniJsonCodeGenModule = 
{
	"Google.MiniJson.dll",
	24,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
