﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Int32>[]
struct EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Int32>
struct KeyCollection_t666396E67E50284D48938851873CE562083D67F2;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Int32>
struct ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// vp_Event
struct vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A;
// vp_Value`1/Getter`1<System.Object,System.Object>
struct Getter_1_t937722DADE2AAED1621E908646316A6E193A266B;
// vp_Value`1/Setter`1<System.Object,System.Object>
struct Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255;
// vp_Value`1<System.Object>
struct vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479;

IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral2FC7D48A5356042700B80D3BAEE0C5E913825A7D;
IL2CPP_EXTERN_C String_t* _stringLiteral448AB73BA1C21E671E218FB91F2644C834F0C16F;
IL2CPP_EXTERN_C String_t* _stringLiteral85FC3B2ACC52E958D0512287C2D44629E7D2CBAF;
IL2CPP_EXTERN_C String_t* _stringLiteralBFFFD736CDDD08A4EEE689949C3399CB61DA773B;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Getter_1_tB9D2902E8954106EBBDFF14735F86C73EE7B7014_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Setter_1_t00D8F1B4DA85B1105AA10CBB32D04F58B802D121_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t vp_Value_1_InitFields_m79BCE6D0E34002CA25EB4A0DA15D2B559226FC41_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE;
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct  Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___entries_1)); }
	inline EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tAD4FDE2B2578C6625A7296B1C46DCB06DCB45186* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___keys_7)); }
	inline KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t666396E67E50284D48938851873CE562083D67F2 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t666396E67E50284D48938851873CE562083D67F2 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ___values_8)); }
	inline ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * get_values_8() const { return ___values_8; }
	inline ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t532E2FD863D0D47B87202BE6B4F7C7EDB5DD7CBF * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// vp_Event
struct  vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A  : public RuntimeObject
{
public:
	// System.String vp_Event::m_Name
	String_t* ___m_Name_0;
	// System.Type vp_Event::m_Type
	Type_t * ___m_Type_1;
	// System.Type vp_Event::m_ArgumentType
	Type_t * ___m_ArgumentType_2;
	// System.Type vp_Event::m_ReturnType
	Type_t * ___m_ReturnType_3;
	// System.Reflection.FieldInfo[] vp_Event::m_Fields
	FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* ___m_Fields_4;
	// System.Type[] vp_Event::m_DelegateTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___m_DelegateTypes_5;
	// System.Reflection.MethodInfo[] vp_Event::m_DefaultMethods
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* ___m_DefaultMethods_6;
	// System.String[] vp_Event::InvokerFieldNames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___InvokerFieldNames_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> vp_Event::Prefixes
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___Prefixes_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_Type_1)); }
	inline Type_t * get_m_Type_1() const { return ___m_Type_1; }
	inline Type_t ** get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(Type_t * value)
	{
		___m_Type_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ArgumentType_2() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_ArgumentType_2)); }
	inline Type_t * get_m_ArgumentType_2() const { return ___m_ArgumentType_2; }
	inline Type_t ** get_address_of_m_ArgumentType_2() { return &___m_ArgumentType_2; }
	inline void set_m_ArgumentType_2(Type_t * value)
	{
		___m_ArgumentType_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ArgumentType_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_ReturnType_3() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_ReturnType_3)); }
	inline Type_t * get_m_ReturnType_3() const { return ___m_ReturnType_3; }
	inline Type_t ** get_address_of_m_ReturnType_3() { return &___m_ReturnType_3; }
	inline void set_m_ReturnType_3(Type_t * value)
	{
		___m_ReturnType_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ReturnType_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Fields_4() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_Fields_4)); }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* get_m_Fields_4() const { return ___m_Fields_4; }
	inline FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE** get_address_of_m_Fields_4() { return &___m_Fields_4; }
	inline void set_m_Fields_4(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* value)
	{
		___m_Fields_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Fields_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_DelegateTypes_5() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_DelegateTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_m_DelegateTypes_5() const { return ___m_DelegateTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_m_DelegateTypes_5() { return &___m_DelegateTypes_5; }
	inline void set_m_DelegateTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___m_DelegateTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DelegateTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_DefaultMethods_6() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___m_DefaultMethods_6)); }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* get_m_DefaultMethods_6() const { return ___m_DefaultMethods_6; }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B** get_address_of_m_DefaultMethods_6() { return &___m_DefaultMethods_6; }
	inline void set_m_DefaultMethods_6(MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* value)
	{
		___m_DefaultMethods_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultMethods_6), (void*)value);
	}

	inline static int32_t get_offset_of_InvokerFieldNames_7() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___InvokerFieldNames_7)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_InvokerFieldNames_7() const { return ___InvokerFieldNames_7; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_InvokerFieldNames_7() { return &___InvokerFieldNames_7; }
	inline void set_InvokerFieldNames_7(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___InvokerFieldNames_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InvokerFieldNames_7), (void*)value);
	}

	inline static int32_t get_offset_of_Prefixes_8() { return static_cast<int32_t>(offsetof(vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A, ___Prefixes_8)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_Prefixes_8() const { return ___Prefixes_8; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_Prefixes_8() { return &___Prefixes_8; }
	inline void set_Prefixes_8(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___Prefixes_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Prefixes_8), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Reflection.FieldInfo
struct  FieldInfo_t  : public MemberInfo_t
{
public:

public:
};


// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// vp_Value`1<System.Object>
struct  vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479  : public vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A
{
public:
	// vp_Value`1_Getter`1<V,V> vp_Value`1::Get
	Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * ___Get_9;
	// vp_Value`1_Setter`1<V,V> vp_Value`1::Set
	Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * ___Set_10;

public:
	inline static int32_t get_offset_of_Get_9() { return static_cast<int32_t>(offsetof(vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479, ___Get_9)); }
	inline Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * get_Get_9() const { return ___Get_9; }
	inline Getter_1_t937722DADE2AAED1621E908646316A6E193A266B ** get_address_of_Get_9() { return &___Get_9; }
	inline void set_Get_9(Getter_1_t937722DADE2AAED1621E908646316A6E193A266B * value)
	{
		___Get_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Get_9), (void*)value);
	}

	inline static int32_t get_offset_of_Set_10() { return static_cast<int32_t>(offsetof(vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479, ___Set_10)); }
	inline Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * get_Set_10() const { return ___Set_10; }
	inline Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 ** get_address_of_Set_10() { return &___Set_10; }
	inline void set_Set_10(Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * value)
	{
		___Set_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Set_10), (void*)value);
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// vp_Value`1_Setter`1<System.Object,System.Object>
struct  Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Reflection.FieldInfo[]
struct FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) FieldInfo_t * m_Items[1];

public:
	inline FieldInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline FieldInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FieldInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FieldInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) MethodInfo_t * m_Items[1];

public:
	inline MethodInfo_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline MethodInfo_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline MethodInfo_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, MethodInfo_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m56FBD260A4D190AD833E9B108B1E80A574AA62C4_gshared (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m786A1D72D4E499C0776742D3B2921F47E3A54545_gshared (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * __this, RuntimeObject * ___key0, int32_t ___value1, const RuntimeMethod* method);

// System.Void vp_Event::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event__ctor_mBAC4F17FDA57FE7F9766E60433C5A10BF198D4E7 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Type vp_Event::get_Type()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, const RuntimeMethod* method);
// System.Reflection.FieldInfo System.Type::GetField(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FieldInfo_t * Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5 (Type_t * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void vp_Event::StoreInvokerFieldNames()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ___handle0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::.ctor()
inline void Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62 (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *, const RuntimeMethod*))Dictionary_2__ctor_m56FBD260A4D190AD833E9B108B1E80A574AA62C4_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1)
inline void Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76 (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * __this, String_t* ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *, String_t*, int32_t, const RuntimeMethod*))Dictionary_2_Add_m786A1D72D4E499C0776742D3B2921F47E3A54545_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Reflection.MethodInfo::op_Inequality(System.Reflection.MethodInfo,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237 (MethodInfo_t * ___left0, MethodInfo_t * ___right1, const RuntimeMethod* method);
// System.Type vp_Event::MakeGenericType(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403 (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, Type_t * ___type0, const RuntimeMethod* method);
// System.Void vp_Event::SetFieldToLocalMethod(System.Reflection.FieldInfo,System.Reflection.MethodInfo,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, FieldInfo_t * ___field0, MethodInfo_t * ___method1, Type_t * ___type2, const RuntimeMethod* method);
// System.Void vp_Event::SetFieldToExternalMethod(System.Object,System.Reflection.FieldInfo,System.String,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_SetFieldToExternalMethod_mA9E3D6BBEDCBA637A2FA457E5DC0820B5BF3D9FE (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, RuntimeObject * ___target0, FieldInfo_t * ___field1, String_t* ___method2, Type_t * ___type3, const RuntimeMethod* method);
// System.Void vp_Event::Refresh()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Value`1_Setter`1<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Setter_1__ctor_m2DDE9E8A55BAF07C4A4333D99F2C2108E1C21523_gshared (Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void vp_Value`1_Setter`1<System.Object,System.Object>::Invoke(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Setter_1_Invoke_mF5BB8CE97927F58322D3B7CD804B453C84F7AAE0_gshared (Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___o0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___o0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___o0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___o0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___o0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___o0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___o0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___o0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___o0);
					else
						GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___o0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___o0);
					else
						VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___o0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___o0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult vp_Value`1_Setter`1<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Setter_1_BeginInvoke_m5C8C661010544E9C1311D782ED0596DB50E8C3C6_gshared (Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * __this, RuntimeObject * ___o0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___o0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void vp_Value`1_Setter`1<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Setter_1_EndInvoke_mC5F905B27EE5CE687BEA972298718795B2BE66D1_gshared (Setter_1_t221EBC6F78E5AB485E7F4C13C9D7AE075E586255 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Reflection.FieldInfo[] vp_Value`1<System.Object>::get_Fields()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* vp_Value_1_get_Fields_mD18B7CB77F625798B9671CCC0B8EF4B27D7D164C_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, const RuntimeMethod* method)
{
	FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* V_0 = NULL;
	{
		// return m_Fields;
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		V_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_1 = V_0;
		return L_1;
	}
}
// System.Void vp_Value`1<System.Object>::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Value_1__ctor_m9D02D3D7AB6D8230F2907B56DE63C3F125B15C81_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	{
		// public vp_Value(string name) : base(name)
		String_t* L_0 = ___name0;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event__ctor_mBAC4F17FDA57FE7F9766E60433C5A10BF198D4E7((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (String_t*)L_0, /*hidden argument*/NULL);
		// InitFields();
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		VirtActionInvoker0::Invoke(6 /* System.Void vp_Event::InitFields() */, (vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		// }
		return;
	}
}
// System.Void vp_Value`1<System.Object>::InitFields()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Value_1_InitFields_m79BCE6D0E34002CA25EB4A0DA15D2B559226FC41_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Value_1_InitFields_m79BCE6D0E34002CA25EB4A0DA15D2B559226FC41_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	int32_t G_B8_0 = 0;
	{
		// m_Fields = new FieldInfo[] {    Type.GetField("Get"),
		//                                 Type.GetField("Set") };
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_0 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)SZArrayNew(FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE_il2cpp_TypeInfo_var, (uint32_t)2);
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_1 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)L_0;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_2 = vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_2);
		FieldInfo_t * L_3 = Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5((Type_t *)L_2, (String_t*)_stringLiteralBFFFD736CDDD08A4EEE689949C3399CB61DA773B, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (FieldInfo_t *)L_3);
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_4 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)L_1;
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_5 = vp_Event_get_Type_m40567438E0D7E341E944C17386D18B267238E183((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_5);
		FieldInfo_t * L_6 = Type_GetField_m564F7686385A6EA8C30F81C939250D5010DC0CA5((Type_t *)L_5, (String_t*)_stringLiteral448AB73BA1C21E671E218FB91F2644C834F0C16F, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (FieldInfo_t *)L_6);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_Fields_4(L_4);
		// StoreInvokerFieldNames();
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_StoreInvokerFieldNames_m5CCF87A31F9D14CCFD4185D539C7AA8C3C598CEF((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		// m_DelegateTypes = new Type[]{    typeof(vp_Value<>.Getter<>),
		//                             typeof(vp_Value<>.Setter<>)};
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_7 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)SZArrayNew(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F_il2cpp_TypeInfo_var, (uint32_t)2);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_7;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_9 = { reinterpret_cast<intptr_t> (Getter_1_tB9D2902E8954106EBBDFF14735F86C73EE7B7014_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_10);
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_11 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)L_8;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_12 = { reinterpret_cast<intptr_t> (Setter_1_t00D8F1B4DA85B1105AA10CBB32D04F58B802D121_0_0_0_var) };
		Type_t * L_13 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6((RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D )L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_13);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_m_DelegateTypes_5(L_11);
		// Prefixes = new Dictionary<string, int>() { { "get_OnValue_", 0 }, { "set_OnValue_", 1 } };
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_14 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)il2cpp_codegen_object_new(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62(L_14, /*hidden argument*/Dictionary_2__ctor_m20A5B6C6950ACF998FE28F7FACEA19C755593E62_RuntimeMethod_var);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_15 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_14;
		NullCheck((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_15);
		Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_15, (String_t*)_stringLiteral85FC3B2ACC52E958D0512287C2D44629E7D2CBAF, (int32_t)0, /*hidden argument*/Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var);
		Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * L_16 = (Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_15;
		NullCheck((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_16);
		Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76((Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB *)L_16, (String_t*)_stringLiteral2FC7D48A5356042700B80D3BAEE0C5E913825A7D, (int32_t)1, /*hidden argument*/Dictionary_2_Add_m5453726952CE3720733822DBF38A0091037F0F76_RuntimeMethod_var);
		((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->set_Prefixes_8(L_16);
		// if ((m_DefaultMethods != null) && (m_DefaultMethods[0] != null))
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_17 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_17)
		{
			goto IL_009d;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_18 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_18);
		int32_t L_19 = 0;
		MethodInfo_t * L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		bool L_21 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_20, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_21));
		goto IL_009e;
	}

IL_009d:
	{
		G_B3_0 = 0;
	}

IL_009e:
	{
		V_0 = (bool)G_B3_0;
		bool L_22 = V_0;
		if (!L_22)
		{
			goto IL_00c7;
		}
	}
	{
		// SetFieldToLocalMethod(m_Fields[0], m_DefaultMethods[0], MakeGenericType(m_DelegateTypes[0]));
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_23 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_23);
		int32_t L_24 = 0;
		FieldInfo_t * L_25 = (L_23)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_26 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_26);
		int32_t L_27 = 0;
		MethodInfo_t * L_28 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_29 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_29);
		int32_t L_30 = 0;
		Type_t * L_31 = (L_29)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_32 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_31, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_25, (MethodInfo_t *)L_28, (Type_t *)L_32, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		// if ((m_DefaultMethods != null) && (m_DefaultMethods[1] != null))
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_33 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_33)
		{
			goto IL_00df;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_34 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_34);
		int32_t L_35 = 1;
		MethodInfo_t * L_36 = (L_34)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		bool L_37 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_36, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_37));
		goto IL_00e0;
	}

IL_00df:
	{
		G_B8_0 = 0;
	}

IL_00e0:
	{
		V_1 = (bool)G_B8_0;
		bool L_38 = V_1;
		if (!L_38)
		{
			goto IL_0109;
		}
	}
	{
		// SetFieldToLocalMethod(m_Fields[1], m_DefaultMethods[1], MakeGenericType(m_DelegateTypes[1]));
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_39 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_39);
		int32_t L_40 = 1;
		FieldInfo_t * L_41 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_42 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_42);
		int32_t L_43 = 1;
		MethodInfo_t * L_44 = (L_42)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_45 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_45);
		int32_t L_46 = 1;
		Type_t * L_47 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_48 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_47, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_41, (MethodInfo_t *)L_44, (Type_t *)L_48, /*hidden argument*/NULL);
	}

IL_0109:
	{
		// }
		return;
	}
}
// System.Void vp_Value`1<System.Object>::Register(System.Object,System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Value_1_Register_m771D9AB76A1D367D1E5E1D913418FD28E7EFD261_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, RuntimeObject * ___t0, String_t* ___m1, int32_t ___v2, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// if (m == null)
		String_t* L_0 = ___m1;
		V_0 = (bool)((((RuntimeObject*)(String_t*)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_000b;
		}
	}
	{
		// return;
		goto IL_0031;
	}

IL_000b:
	{
		// SetFieldToExternalMethod(t, m_Fields[v], m, MakeGenericType(m_DelegateTypes[v]));
		RuntimeObject * L_2 = ___t0;
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_3 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		int32_t L_4 = ___v2;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		FieldInfo_t * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		String_t* L_7 = ___m1;
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_8 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		int32_t L_9 = ___v2;
		NullCheck(L_8);
		int32_t L_10 = L_9;
		Type_t * L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_12 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_11, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToExternalMethod_mA9E3D6BBEDCBA637A2FA457E5DC0820B5BF3D9FE((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (RuntimeObject *)L_2, (FieldInfo_t *)L_6, (String_t*)L_7, (Type_t *)L_12, /*hidden argument*/NULL);
		// Refresh();
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// }
		return;
	}
}
// System.Void vp_Value`1<System.Object>::Unregister(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Value_1_Unregister_mEB03FE35DCAAB0D3434615633CDADE335697E3EA_gshared (vp_Value_1_t456C7DDF27FB25A042D0E98D25CD2867A0B07479 * __this, RuntimeObject * ___t0, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	int32_t G_B8_0 = 0;
	{
		// if ((m_DefaultMethods != null) && (m_DefaultMethods[0] != null))
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_0 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_1 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_1);
		int32_t L_2 = 0;
		MethodInfo_t * L_3 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		bool L_4 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_3, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		V_0 = (bool)G_B3_0;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0043;
		}
	}
	{
		// SetFieldToLocalMethod(m_Fields[0], m_DefaultMethods[0], MakeGenericType(m_DelegateTypes[0]));
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_6 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_6);
		int32_t L_7 = 0;
		FieldInfo_t * L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_9 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_9);
		int32_t L_10 = 0;
		MethodInfo_t * L_11 = (L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_12 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_12);
		int32_t L_13 = 0;
		Type_t * L_14 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_15 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_14, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_8, (MethodInfo_t *)L_11, (Type_t *)L_15, /*hidden argument*/NULL);
	}

IL_0043:
	{
		// if ((m_DefaultMethods != null) && (m_DefaultMethods[1] != null))
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_16 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		if (!L_16)
		{
			goto IL_005b;
		}
	}
	{
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_17 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_17);
		int32_t L_18 = 1;
		MethodInfo_t * L_19 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		bool L_20 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237((MethodInfo_t *)L_19, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_20));
		goto IL_005c;
	}

IL_005b:
	{
		G_B8_0 = 0;
	}

IL_005c:
	{
		V_1 = (bool)G_B8_0;
		bool L_21 = V_1;
		if (!L_21)
		{
			goto IL_0085;
		}
	}
	{
		// SetFieldToLocalMethod(m_Fields[1], m_DefaultMethods[1], MakeGenericType(m_DelegateTypes[1]));
		FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE* L_22 = (FieldInfoU5BU5D_t9C36FA93372CA01DAF85946064B058CD9CE2E8BE*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_Fields_4();
		NullCheck(L_22);
		int32_t L_23 = 1;
		FieldInfo_t * L_24 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* L_25 = (MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DefaultMethods_6();
		NullCheck(L_25);
		int32_t L_26 = 1;
		MethodInfo_t * L_27 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_26));
		TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* L_28 = (TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F*)((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this)->get_m_DelegateTypes_5();
		NullCheck(L_28);
		int32_t L_29 = 1;
		Type_t * L_30 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_29));
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		Type_t * L_31 = vp_Event_MakeGenericType_m40D44B68A06408BE04A5D44DC5CCFD4D739BB403((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (Type_t *)L_30, /*hidden argument*/NULL);
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_SetFieldToLocalMethod_mD94CD31FF4E133B303F5FBBA2C3930980993108C((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, (FieldInfo_t *)L_24, (MethodInfo_t *)L_27, (Type_t *)L_31, /*hidden argument*/NULL);
	}

IL_0085:
	{
		// Refresh();
		NullCheck((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this);
		vp_Event_Refresh_m672B8CBFE140F6BB59CA7AAC3F2A7E010209402D((vp_Event_t8DF87E10FB5CADE8540A3EF5B7A067BAB66AF22A *)__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
