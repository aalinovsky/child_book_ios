﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Firebase.Unity.InstallRootCerts::.ctor()
extern void InstallRootCerts__ctor_m5E66DEF67729378EAF57CBF0DEBAEBA189EFB8C0 ();
// 0x00000002 System.Boolean Firebase.Unity.InstallRootCerts::get_InstallationRequired()
extern void InstallRootCerts_get_InstallationRequired_m9B0756A9F1C52E761BC4AADA71B09E62146FC240 ();
// 0x00000003 Firebase.Unity.InstallRootCerts Firebase.Unity.InstallRootCerts::get_Instance()
extern void InstallRootCerts_get_Instance_mB0904BD22EF5278939F2847E8CAF68F94828202B ();
// 0x00000004 System.Void Firebase.Unity.InstallRootCerts::AFunctionThatDoesNotExistInternal()
extern void InstallRootCerts_AFunctionThatDoesNotExistInternal_mC3D31F79F5AFAABB6821F7034F45E04E853E4C62 ();
// 0x00000005 System.Void Firebase.Unity.InstallRootCerts::AFunctionThatDoesNotExist()
extern void InstallRootCerts_AFunctionThatDoesNotExist_m47709A6958E5FA7A36F3763E03C2A452872E7F2C ();
// 0x00000006 System.Boolean Firebase.Unity.InstallRootCerts::IsCertBugPresent(UnityEngine.RuntimePlatform)
extern void InstallRootCerts_IsCertBugPresent_m22F37D714F73AB63EF893E8BD68525A4A481604D ();
// 0x00000007 System.Collections.Generic.List`1<System.Byte[]> Firebase.Unity.InstallRootCerts::DecodeBase64Blobs(System.String,System.String,System.String)
extern void InstallRootCerts_DecodeBase64Blobs_m29EDEB21AB6357AA10AD48DF0585BC46A49D2A38 ();
// 0x00000008 System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Unity.InstallRootCerts::DecodeCertificateCollectionFromString(System.String)
extern void InstallRootCerts_DecodeCertificateCollectionFromString_mF0C89E88BEA06136D539C2BB81941F9995B5A157 ();
// 0x00000009 System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Unity.InstallRootCerts::DecodeDefaultCollection()
extern void InstallRootCerts_DecodeDefaultCollection_m5F99CBE2450D0F4A65691C036F220BC393D99AF9 ();
// 0x0000000A System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Unity.InstallRootCerts::DecodeCollection(Firebase.Platform.IFirebaseAppPlatform)
extern void InstallRootCerts_DecodeCollection_m846B16E66820F63FC97C1D9D55A0A9B94943E83C ();
// 0x0000000B System.Void Firebase.Unity.InstallRootCerts::InstallDefaultCRLs(System.String,System.String)
extern void InstallRootCerts_InstallDefaultCRLs_m91C8AA55996B45FB59A7781F5BF28DCEEF79C7E9 ();
// 0x0000000C System.Void Firebase.Unity.InstallRootCerts::HackRefreshMonoRootStore()
extern void InstallRootCerts_HackRefreshMonoRootStore_m00267CDC5A9F5DFB0A9B8DF8A61249A16F6255B1 ();
// 0x0000000D System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Unity.InstallRootCerts::Install(Firebase.Platform.IFirebaseAppPlatform)
extern void InstallRootCerts_Install_mE11E7BF36DA4BBF605B38D859DB77A57C8617B1E ();
// 0x0000000E System.Void Firebase.Unity.InstallRootCerts::.cctor()
extern void InstallRootCerts__cctor_m232DBEB94A155625714EEABD355079CBB4CE7554 ();
// 0x0000000F <>__AnonType0`3<UnityEngine.RuntimePlatform,System.Boolean,System.Boolean> Firebase.Unity.InstallRootCerts::<InstallRootCerts>m__0()
extern void InstallRootCerts_U3CInstallRootCertsU3Em__0_m743263E1CEBBF1C13EBF8B7FB3ED7F1A05B433E2 ();
// 0x00000010 System.Type Firebase.Platform.FirebaseEditorDispatcher::get_EditorApplicationType()
extern void FirebaseEditorDispatcher_get_EditorApplicationType_m1F7FD788E0579F30AC63063A4AEBAD065A2ACE7E ();
// 0x00000011 System.Boolean Firebase.Platform.FirebaseEditorDispatcher::get_EditorIsPlaying()
extern void FirebaseEditorDispatcher_get_EditorIsPlaying_m8E80FCF745D38EF53E550955ED6634498B663E2B ();
// 0x00000012 System.Boolean Firebase.Platform.FirebaseEditorDispatcher::get_EditorIsPlayingOrWillChangePlaymode()
extern void FirebaseEditorDispatcher_get_EditorIsPlayingOrWillChangePlaymode_mC656FF2ABDE09D856D738444067F34B9D6F6E741 ();
// 0x00000013 System.Void Firebase.Platform.FirebaseEditorDispatcher::StartEditorUpdate()
extern void FirebaseEditorDispatcher_StartEditorUpdate_m9014FC7984B5CE57EBC0A23B1309C9B05997FD7B ();
// 0x00000014 System.Void Firebase.Platform.FirebaseEditorDispatcher::StopEditorUpdate()
extern void FirebaseEditorDispatcher_StopEditorUpdate_mED528078F895F70293A8DF07275180DF2F7C6E24 ();
// 0x00000015 System.Void Firebase.Platform.FirebaseEditorDispatcher::Update()
extern void FirebaseEditorDispatcher_Update_m1CE1E7BDDB5204EE82EC28C6C7B5F83AB64C5D63 ();
// 0x00000016 System.Void Firebase.Platform.FirebaseEditorDispatcher::ListenToPlayState(System.Boolean)
extern void FirebaseEditorDispatcher_ListenToPlayState_mFADB143214E7B22867C4D26CD6C1B384C5BA00FD ();
// 0x00000017 System.Void Firebase.Platform.FirebaseEditorDispatcher::PlayModeStateChanged()
extern void FirebaseEditorDispatcher_PlayModeStateChanged_mA77B4BC297642FC52BB8AB01FBB79E98B1C47288 ();
// 0x00000018 System.Void Firebase.Platform.FirebaseEditorDispatcher::PlayModeStateChangedWithArg(T)
// 0x00000019 System.Void Firebase.Platform.FirebaseEditorDispatcher::AddRemoveCallbackToField(System.Reflection.FieldInfo,System.Action,System.Object,System.Boolean,System.String)
extern void FirebaseEditorDispatcher_AddRemoveCallbackToField_m285003B251B4D5067546A49017F6B5F5C8405FA7 ();
// 0x0000001A System.Void Firebase.Platform.FirebaseHandler::.cctor()
extern void FirebaseHandler__cctor_mCC22861A54093E75FF8C063CA2B0AC1975F832B5 ();
// 0x0000001B System.Void Firebase.Platform.FirebaseHandler::.ctor()
extern void FirebaseHandler__ctor_mF69C13C09805BAF05E457BA75976181A117E091A ();
// 0x0000001C Firebase.Platform.IFirebaseAppUtils Firebase.Platform.FirebaseHandler::get_AppUtils()
extern void FirebaseHandler_get_AppUtils_m4B1DD0CA9C44AB6E9A12EED231FA8EF265948A7C ();
// 0x0000001D System.Void Firebase.Platform.FirebaseHandler::set_AppUtils(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_set_AppUtils_m91387365FFE85468A59A0A2433DA483F08602DBF ();
// 0x0000001E System.Int32 Firebase.Platform.FirebaseHandler::get_TickCount()
extern void FirebaseHandler_get_TickCount_m9A88A2C0949942CDFAC05C061004305B148E8019 ();
// 0x0000001F Firebase.Dispatcher Firebase.Platform.FirebaseHandler::get_ThreadDispatcher()
extern void FirebaseHandler_get_ThreadDispatcher_m576CC2F52B6A331E1D5D7ABDC493937253D51189 ();
// 0x00000020 System.Void Firebase.Platform.FirebaseHandler::set_ThreadDispatcher(Firebase.Dispatcher)
extern void FirebaseHandler_set_ThreadDispatcher_m7C1647AB2F233DD9D808C3AEA9B0469F19F653F2 ();
// 0x00000021 System.Boolean Firebase.Platform.FirebaseHandler::get_IsPlayMode()
extern void FirebaseHandler_get_IsPlayMode_m2784DFEDC9F23AE0072B174583BDEBB1D9F8B7FD ();
// 0x00000022 System.Void Firebase.Platform.FirebaseHandler::set_IsPlayMode(System.Boolean)
extern void FirebaseHandler_set_IsPlayMode_mDFC0BA0F8ADCB56155A7278823E9669758BE38B5 ();
// 0x00000023 System.Void Firebase.Platform.FirebaseHandler::StartMonoBehaviour()
extern void FirebaseHandler_StartMonoBehaviour_m6728F8C35C8A08C2933B871CA4716CD6C95314DA ();
// 0x00000024 System.Void Firebase.Platform.FirebaseHandler::StopMonoBehaviour()
extern void FirebaseHandler_StopMonoBehaviour_m741EC378D41A3166E8DE6532F03A195E88C033C2 ();
// 0x00000025 TResult Firebase.Platform.FirebaseHandler::RunOnMainThread(System.Func`1<TResult>)
// 0x00000026 System.Threading.Tasks.Task`1<TResult> Firebase.Platform.FirebaseHandler::RunOnMainThreadAsync(System.Func`1<TResult>)
// 0x00000027 Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseHandler::get_DefaultInstance()
extern void FirebaseHandler_get_DefaultInstance_mDE47F7545F78ED4AAA09566895610B8DD8A6FE02 ();
// 0x00000028 System.Void Firebase.Platform.FirebaseHandler::CreatePartialOnMainThread(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_CreatePartialOnMainThread_m96D4E360CF58711B4865E546DCE75CACCFA34C78 ();
// 0x00000029 System.Void Firebase.Platform.FirebaseHandler::Create(Firebase.Platform.IFirebaseAppUtils)
extern void FirebaseHandler_Create_m89DB93F791D7C61D45C79484E9207C4967374BEA ();
// 0x0000002A System.Void Firebase.Platform.FirebaseHandler::Update()
extern void FirebaseHandler_Update_m4A49185D21631806389823523179738DB897EB33 ();
// 0x0000002B System.Void Firebase.Platform.FirebaseHandler::OnApplicationFocus(System.Boolean)
extern void FirebaseHandler_OnApplicationFocus_m04ED5D86AF9BA18E761EACFEA9455E1829008B8E ();
// 0x0000002C System.Void Firebase.Platform.FirebaseHandler::OnMonoBehaviourDestroyed(Firebase.Platform.FirebaseMonoBehaviour)
extern void FirebaseHandler_OnMonoBehaviourDestroyed_m93E0EE038A3E5F19681DAFEE5445ABE6065AA086 ();
// 0x0000002D System.Boolean Firebase.Platform.FirebaseHandler::<StopMonoBehaviour>m__0()
extern void FirebaseHandler_U3CStopMonoBehaviourU3Em__0_mF2ACC3BC95F892A6B871A99E15543259938D56C6 ();
// 0x0000002E System.Void Firebase.Platform.FirebaseHandler::<Update>m__1()
extern void FirebaseHandler_U3CUpdateU3Em__1_m34B81AC76DADE8936F37C2F2F1F6C0952CCB7CA5 ();
// 0x0000002F System.Void Firebase.Platform.FirebaseHandler_ApplicationFocusChangedEventArgs::.ctor()
extern void ApplicationFocusChangedEventArgs__ctor_mB1717C3BC9E32E3336D360134B6073C8EE5E4EBA ();
// 0x00000030 System.Void Firebase.Platform.FirebaseHandler_ApplicationFocusChangedEventArgs::set_HasFocus(System.Boolean)
extern void ApplicationFocusChangedEventArgs_set_HasFocus_mEA4178D6C578686CE84F1E088EA370AB7225A330 ();
// 0x00000031 System.Void Firebase.Platform.FirebaseHandler_<CreatePartialOnMainThread>c__AnonStorey0::.ctor()
extern void U3CCreatePartialOnMainThreadU3Ec__AnonStorey0__ctor_m3D8C381BEF70E6FA9C1D6AF854ED6EAF156A001F ();
// 0x00000032 System.Void Firebase.Platform.FirebaseHandler_<CreatePartialOnMainThread>c__AnonStorey0::<>m__0()
extern void U3CCreatePartialOnMainThreadU3Ec__AnonStorey0_U3CU3Em__0_mBF2F55D34BE7497CD9E43AF262B6EA52BA7990EF ();
// 0x00000033 System.Boolean Firebase.Platform.FirebaseLogger::IsStackTraceLogTypeIncompatibleWithNativeLogs(UnityEngine.StackTraceLogType)
extern void FirebaseLogger_IsStackTraceLogTypeIncompatibleWithNativeLogs_m32A916D1E8992541EB447E499B85BFF15CEF559F ();
// 0x00000034 System.Boolean Firebase.Platform.FirebaseLogger::CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs()
extern void FirebaseLogger_CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs_m51B2392AEFCF6E04C363994806DCE5EAE27EDA2B ();
// 0x00000035 System.Boolean Firebase.Platform.FirebaseLogger::get_CanRedirectNativeLogs()
extern void FirebaseLogger_get_CanRedirectNativeLogs_m29C5B1ACE567CEA890B6DA19D91AF37D1EDEEBC8 ();
// 0x00000036 System.Void Firebase.Platform.FirebaseLogger::LogMessage(Firebase.Platform.PlatformLogLevel,System.String)
extern void FirebaseLogger_LogMessage_mEF2033A0588D651D162FFE29C9FA77A39E51233E ();
// 0x00000037 System.Void Firebase.Platform.FirebaseLogger::.cctor()
extern void FirebaseLogger__cctor_mA0565673CF29882DF40267833252A8B96AC85B77 ();
// 0x00000038 System.Void Firebase.Platform.FirebaseMonoBehaviour::.ctor()
extern void FirebaseMonoBehaviour__ctor_m137AF46B744D8805EC43DC70A0DCE49AF4F2B326 ();
// 0x00000039 Firebase.Platform.FirebaseHandler Firebase.Platform.FirebaseMonoBehaviour::GetFirebaseHandlerOrDestroyGameObject()
extern void FirebaseMonoBehaviour_GetFirebaseHandlerOrDestroyGameObject_m77DA35BC03EF563A295FDA20901881CFC161A89B ();
// 0x0000003A System.Void Firebase.Platform.FirebaseMonoBehaviour::OnEnable()
extern void FirebaseMonoBehaviour_OnEnable_mB3D6B00AA0A0A4FFD25FDA48E28AFCE52E359B80 ();
// 0x0000003B System.Void Firebase.Platform.FirebaseMonoBehaviour::Update()
extern void FirebaseMonoBehaviour_Update_mC3DFEB73F14CD64F3C26006141EF71839E463A65 ();
// 0x0000003C System.Void Firebase.Platform.FirebaseMonoBehaviour::OnApplicationFocus(System.Boolean)
extern void FirebaseMonoBehaviour_OnApplicationFocus_m84A811D74316C5E254EF57A6253755EBBD8585E9 ();
// 0x0000003D System.Void Firebase.Platform.FirebaseMonoBehaviour::OnDestroy()
extern void FirebaseMonoBehaviour_OnDestroy_mDCEE8FA7C5820AA947E110083347A7ED4BE71FD5 ();
// 0x0000003E System.Boolean Firebase.Platform.PlatformInformation::get_IsAndroid()
extern void PlatformInformation_get_IsAndroid_m61250012539009D27371075104AFBDE2549BB77E ();
// 0x0000003F System.Boolean Firebase.Platform.PlatformInformation::get_IsIOS()
extern void PlatformInformation_get_IsIOS_mEC53C8E1719192F4A5E5A5C919752D9772706573 ();
// 0x00000040 System.String Firebase.Platform.PlatformInformation::get_DefaultConfigLocation()
extern void PlatformInformation_get_DefaultConfigLocation_m7B32A608B4B90F6749B02AC1C0E87E4C904E9608 ();
// 0x00000041 System.Single Firebase.Platform.PlatformInformation::get_RealtimeSinceStartup()
extern void PlatformInformation_get_RealtimeSinceStartup_mA5552638B79722FD7378E430054A9EE21A7F4742 ();
// 0x00000042 System.Void Firebase.Platform.PlatformInformation::set_RealtimeSinceStartupSafe(System.Single)
extern void PlatformInformation_set_RealtimeSinceStartupSafe_mB37999FFE3AF262FB2234E0308CB8155D7827C47 ();
// 0x00000043 System.String Firebase.Platform.PlatformInformation::get_RuntimeName()
extern void PlatformInformation_get_RuntimeName_mCD019D8389883E820ED30D97642D63F0BA844EAD ();
// 0x00000044 System.String Firebase.Platform.PlatformInformation::get_RuntimeVersion()
extern void PlatformInformation_get_RuntimeVersion_m947E74AAC2129616DA7A9330BCE4B79EA8A422F0 ();
// 0x00000045 System.String Firebase.Platform.PlatformInformation::<get_DefaultConfigLocation>m__0()
extern void PlatformInformation_U3Cget_DefaultConfigLocationU3Em__0_m3E69FE6A0EE918EE10A2B590D4DE50A0CA922B01 ();
// 0x00000046 System.String Firebase.Platform.PlatformInformation::<get_RuntimeVersion>m__1()
extern void PlatformInformation_U3Cget_RuntimeVersionU3Em__1_m8405A3B38E79A93CCE9C1812CF66C8A3F8B19735 ();
// 0x00000047 System.Void Firebase.Platform.Default.UnityConfigExtensions::.ctor()
extern void UnityConfigExtensions__ctor_m4A9B319E06B0F2778FBADFE4C06F5EF95C9BBD68 ();
// 0x00000048 Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.UnityConfigExtensions::get_DefaultInstance()
extern void UnityConfigExtensions_get_DefaultInstance_m963AB0EF6217BB9E9569CF4D23E2A0F2753A7B35 ();
// 0x00000049 System.String Firebase.Platform.Default.UnityConfigExtensions::GetWriteablePath(Firebase.Platform.IFirebaseAppPlatform)
extern void UnityConfigExtensions_GetWriteablePath_mCF0DA0C393E09FD6D92E2218E9B3519647DDD3CC ();
// 0x0000004A System.Void Firebase.Platform.Default.UnityConfigExtensions::.cctor()
extern void UnityConfigExtensions__cctor_m3AE99E171092FB6DBB559F1ECF4E0A4B28497058 ();
// 0x0000004B System.String Firebase.Platform.Default.UnityConfigExtensions::<GetWriteablePath>m__0()
extern void UnityConfigExtensions_U3CGetWriteablePathU3Em__0_mD4E52B78753253D075126E01272C31E7CAD43AA7 ();
// 0x0000004C System.Void Firebase.Unity.UnityHttpFactoryService::.ctor()
extern void UnityHttpFactoryService__ctor_mE3C1BAFDBECF9E8725BE5A0016D715923BA75B83 ();
// 0x0000004D Firebase.Unity.UnityHttpFactoryService Firebase.Unity.UnityHttpFactoryService::get_Instance()
extern void UnityHttpFactoryService_get_Instance_m899AA0837373F574AC89B3065FF014F16A6C7D04 ();
// 0x0000004E System.Void Firebase.Unity.UnityHttpFactoryService::.cctor()
extern void UnityHttpFactoryService__cctor_m8549AF9CF9A865588FF3E848786880EA652E6C17 ();
// 0x0000004F System.Void Firebase.Unity.UnityLoggingService::.ctor()
extern void UnityLoggingService__ctor_m3BCC6353BEADE287DF1A0C9781B6187576523441 ();
// 0x00000050 Firebase.Unity.UnityLoggingService Firebase.Unity.UnityLoggingService::get_Instance()
extern void UnityLoggingService_get_Instance_m021810065C28BAD66B00CA2EC2DC55AD3EE85A4E ();
// 0x00000051 System.Void Firebase.Unity.UnityLoggingService::LogMessage(Firebase.Platform.PlatformLogLevel,System.String)
extern void UnityLoggingService_LogMessage_mE989034EB3A56D1F926589C7A987F7E04C6AC432 ();
// 0x00000052 System.Void Firebase.Unity.UnityLoggingService::.cctor()
extern void UnityLoggingService__cctor_m07430A774E2E27DE95B135565549505BC99BFAF3 ();
// 0x00000053 System.Void Firebase.Unity.UnityPlatformServices::SetupServices()
extern void UnityPlatformServices_SetupServices_mC37344AC6AB871E0DCEC44A26FBC98BB05776CE4 ();
// 0x00000054 System.Void Firebase.Unity.UnitySynchronizationContext::.ctor(UnityEngine.GameObject)
extern void UnitySynchronizationContext__ctor_m47697B161718D309E5BDEE021A7652F2610B3811 ();
// 0x00000055 System.Void Firebase.Unity.UnitySynchronizationContext::Create(UnityEngine.GameObject)
extern void UnitySynchronizationContext_Create_m457A7DF239D69F664A4C5A71A51831D10F8C5C9B ();
// 0x00000056 System.Void Firebase.Unity.UnitySynchronizationContext::Destroy()
extern void UnitySynchronizationContext_Destroy_m173DAA6D418F0749046FC98BE3244ECF0CD37188 ();
// 0x00000057 System.Threading.ManualResetEvent Firebase.Unity.UnitySynchronizationContext::GetThreadEvent()
extern void UnitySynchronizationContext_GetThreadEvent_mD3A2B8BC1310D4C5F17918280F14EF089DD8164A ();
// 0x00000058 System.Void Firebase.Unity.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Post_m4450F9D1EEF52CB7EED4C27ACEFC4F5324C6BFBF ();
// 0x00000059 System.Void Firebase.Unity.UnitySynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Send_m537EE81F72568A13624BBD4CB4D8796946722A6D ();
// 0x0000005A System.Void Firebase.Unity.UnitySynchronizationContext::.cctor()
extern void UnitySynchronizationContext__cctor_m3B5D4F04B0CEEF803ED5E341A4AFCD813F706402 ();
// 0x0000005B System.Void Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir::.ctor()
extern void SynchronizationContextBehavoir__ctor_m4DA714845D3AB1121B0A4EBE7065F59A49611B89 ();
// 0x0000005C System.Collections.Generic.Queue`1<System.Tuple`2<System.Threading.SendOrPostCallback,System.Object>> Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir::get_CallbackQueue()
extern void SynchronizationContextBehavoir_get_CallbackQueue_mE2EE07CB1868612C976725049F507F5D1B41D346 ();
// 0x0000005D System.Collections.IEnumerator Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir::Start()
extern void SynchronizationContextBehavoir_Start_m64A7A9B67267950EB78C38059D7A32ACBEA81BDD ();
// 0x0000005E System.Void Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>c__Iterator0::.ctor()
extern void U3CStartU3Ec__Iterator0__ctor_mDD3B386E3E7786CCE773C01C6A53026DC86236A4 ();
// 0x0000005F System.Boolean Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>c__Iterator0::MoveNext()
extern void U3CStartU3Ec__Iterator0_MoveNext_m328175782CEC1827C4BE9ED264EDDC699FE03914 ();
// 0x00000060 System.Object Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern void U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mEDB0AD40F4344F69604F7BFC7037760A6A87DF35 ();
// 0x00000061 System.Object Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m26EBD49E54EFC0704D7991C2EE1BE9897DFF846E ();
// 0x00000062 System.Void Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>c__Iterator0::Dispose()
extern void U3CStartU3Ec__Iterator0_Dispose_mA756A58331F3B4E68EBB6C33E8F8B8F2D06EB036 ();
// 0x00000063 System.Void Firebase.Unity.UnitySynchronizationContext_SynchronizationContextBehavoir_<Start>c__Iterator0::Reset()
extern void U3CStartU3Ec__Iterator0_Reset_mD96EC7F0BA275A6D00CCD79A3BBD8B687627CC4D ();
// 0x00000064 System.Void Firebase.Unity.UnitySynchronizationContext_<SignaledCoroutine>c__Iterator0::.ctor()
extern void U3CSignaledCoroutineU3Ec__Iterator0__ctor_mF997D780AE9CBE903A86F4DDD8A345BD598D91A0 ();
// 0x00000065 System.Boolean Firebase.Unity.UnitySynchronizationContext_<SignaledCoroutine>c__Iterator0::MoveNext()
extern void U3CSignaledCoroutineU3Ec__Iterator0_MoveNext_m079887CA72B97AA900D67CB3CB48EC0C7807D4AD ();
// 0x00000066 System.Object Firebase.Unity.UnitySynchronizationContext_<SignaledCoroutine>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern void U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mB982F27D316E6A73A9C6BFEF2FB947363EED9EA9 ();
// 0x00000067 System.Object Firebase.Unity.UnitySynchronizationContext_<SignaledCoroutine>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern void U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m302C721344DAF763C8ECB056ED744289DEA7D71F ();
// 0x00000068 System.Void Firebase.Unity.UnitySynchronizationContext_<SignaledCoroutine>c__Iterator0::Dispose()
extern void U3CSignaledCoroutineU3Ec__Iterator0_Dispose_m5864B129EB0690FFF9B3330A4111E9F1589AC067 ();
// 0x00000069 System.Void Firebase.Unity.UnitySynchronizationContext_<SignaledCoroutine>c__Iterator0::Reset()
extern void U3CSignaledCoroutineU3Ec__Iterator0_Reset_m312986B152936253BB73ED135A06CBA025EE9792 ();
// 0x0000006A System.Void Firebase.Unity.UnitySynchronizationContext_<SendCoroutine>c__AnonStorey1::.ctor()
extern void U3CSendCoroutineU3Ec__AnonStorey1__ctor_mA62651790837A434BE44A8B171FD13CC3A8D1C80 ();
// 0x0000006B System.Void Firebase.Unity.UnitySynchronizationContext_<SendCoroutine>c__AnonStorey2::.ctor()
extern void U3CSendCoroutineU3Ec__AnonStorey2__ctor_mC838ED0C34B77644FE3750548DF88D0DC7E742A7 ();
// 0x0000006C System.Void Firebase.Unity.UnitySynchronizationContext_<Send>c__AnonStorey3::.ctor()
extern void U3CSendU3Ec__AnonStorey3__ctor_m9DA398536AD41E24A0298DFF850C8AA3B484D48A ();
// 0x0000006D System.Void Firebase.Unity.UnitySynchronizationContext_<Send>c__AnonStorey4::.ctor()
extern void U3CSendU3Ec__AnonStorey4__ctor_mADD177ACDB3F1A48CCD98C465DF3750DFD0133A8 ();
// 0x0000006E System.Void Firebase.Unity.UnitySynchronizationContext_<Send>c__AnonStorey4::<>m__0(System.Object)
extern void U3CSendU3Ec__AnonStorey4_U3CU3Em__0_m39EC76752B64CAFBC5D6D17ED977868570FCF715 ();
// 0x0000006F System.Void Firebase.Platform.Default.AppConfigExtensions::.ctor()
extern void AppConfigExtensions__ctor_mC5065C76DA54B0C1822FC58959AC5FED94F64328 ();
// 0x00000070 Firebase.Platform.IAppConfigExtensions Firebase.Platform.Default.AppConfigExtensions::get_Instance()
extern void AppConfigExtensions_get_Instance_m9E23D31FAA5845CC6A8AB3C6A676DCCE0880C966 ();
// 0x00000071 System.String Firebase.Platform.Default.AppConfigExtensions::GetWriteablePath(Firebase.Platform.IFirebaseAppPlatform)
extern void AppConfigExtensions_GetWriteablePath_m204671D085AD529E35E180E84C8A000FFC77F838 ();
// 0x00000072 System.String Firebase.Platform.Default.AppConfigExtensions::GetCertPemFile(Firebase.Platform.IFirebaseAppPlatform)
extern void AppConfigExtensions_GetCertPemFile_m6893E9609F3F4E7FA0866945ED85224625BBDC84 ();
// 0x00000073 T Firebase.Platform.Default.AppConfigExtensions::GetState(Firebase.Platform.IFirebaseAppPlatform,System.Int32,System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.Dictionary`2<System.String,T>>)
// 0x00000074 System.Void Firebase.Platform.Default.AppConfigExtensions::.cctor()
extern void AppConfigExtensions__cctor_m6F6AA804ECFF8AFE261311924D919CAE0C075096 ();
// 0x00000075 System.Void Firebase.Platform.Default.BaseAuthService::.ctor()
extern void BaseAuthService__ctor_m7F9F1EFFAB9D91C1887DAEC04ABFA1542DB0A91E ();
// 0x00000076 Firebase.Platform.Default.BaseAuthService Firebase.Platform.Default.BaseAuthService::get_BaseInstance()
extern void BaseAuthService_get_BaseInstance_m1AB3EF619AE43E869D22473FD44A0404225BD366 ();
// 0x00000077 System.Void Firebase.Platform.Default.BaseAuthService::.cctor()
extern void BaseAuthService__cctor_m07047C89EE3756A2A9D221E334563592D33316CD ();
// 0x00000078 System.Void Firebase.Platform.Default.SystemClock::.ctor()
extern void SystemClock__ctor_mF0290527299F60106E6FCAB61B3697D77C856DDF ();
// 0x00000079 System.Void Firebase.Platform.Default.SystemClock::.cctor()
extern void SystemClock__cctor_m78C791FBA8496CA05264591D55F49610FFD4A33E ();
// 0x0000007A System.Collections.Generic.List`1<System.Exception> Firebase.ExceptionAggregator::get_Exceptions()
extern void ExceptionAggregator_get_Exceptions_mB8771E2A0D9AFC2BF5A29214B58FAE0621306BE2 ();
// 0x0000007B System.Exception Firebase.ExceptionAggregator::GetAndClearPendingExceptions()
extern void ExceptionAggregator_GetAndClearPendingExceptions_m0428C791FBC34F687E53E93C8283BF1BDAD52705 ();
// 0x0000007C System.Void Firebase.ExceptionAggregator::ThrowAndClearPendingExceptions()
extern void ExceptionAggregator_ThrowAndClearPendingExceptions_m43EDFA771E277D927E98305CF105F598C2A19D96 ();
// 0x0000007D System.Exception Firebase.ExceptionAggregator::LogException(System.Exception)
extern void ExceptionAggregator_LogException_m2D22E7AA0907E7185DDF0F7F5C2EE702265CB12C ();
// 0x0000007E System.Void Firebase.ExceptionAggregator::Wrap(System.Action)
extern void ExceptionAggregator_Wrap_m93A9BD0DC888F1D02E77860288095CB49D4CFFC2 ();
// 0x0000007F System.String Firebase.Platform.IAppConfigExtensions::GetWriteablePath(Firebase.Platform.IFirebaseAppPlatform)
// 0x00000080 System.String Firebase.Platform.IAppConfigExtensions::GetCertPemFile(Firebase.Platform.IFirebaseAppPlatform)
// 0x00000081 System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Platform.ICertificateService::Install(Firebase.Platform.IFirebaseAppPlatform)
// 0x00000082 System.Void Firebase.Platform.NoopCertificateService::.ctor()
extern void NoopCertificateService__ctor_mD188767256CCAB4CD8DDBC37C45825C4F2491B50 ();
// 0x00000083 Firebase.Platform.NoopCertificateService Firebase.Platform.NoopCertificateService::get_Instance()
extern void NoopCertificateService_get_Instance_mA210CAF63B7A85CCE844FC7C4E7F2146591FFF1A ();
// 0x00000084 System.Security.Cryptography.X509Certificates.X509CertificateCollection Firebase.Platform.NoopCertificateService::Install(Firebase.Platform.IFirebaseAppPlatform)
extern void NoopCertificateService_Install_m33FC2BAB7B4D61567F85C4C8394C9299450FE3D8 ();
// 0x00000085 System.Void Firebase.Platform.NoopCertificateService::.cctor()
extern void NoopCertificateService__cctor_mE6CF8FB9071FA36BDDE45474D96D4427B6F84F5D ();
// 0x00000086 System.Void Firebase.Platform.ILoggingService::LogMessage(Firebase.Platform.PlatformLogLevel,System.String)
// 0x00000087 System.Void Firebase.Platform.DebugLogger::.ctor()
extern void DebugLogger__ctor_m6A03E93E7B5D3F77F97E425B65396F7542998E26 ();
// 0x00000088 Firebase.Platform.DebugLogger Firebase.Platform.DebugLogger::get_Instance()
extern void DebugLogger_get_Instance_m06E28364924B44D279EA5EEB003DBA373D3FA2B7 ();
// 0x00000089 System.Void Firebase.Platform.DebugLogger::LogMessage(Firebase.Platform.PlatformLogLevel,System.String)
extern void DebugLogger_LogMessage_mE2F38D331077CFD6A38F3EE023AA8717CB9BF3BE ();
// 0x0000008A System.Void Firebase.Platform.DebugLogger::.cctor()
extern void DebugLogger__cctor_mD5064826923495415C2852B63AA70F8D8FA535B6 ();
// 0x0000008B System.Void Firebase.Platform.Services::.cctor()
extern void Services__cctor_m0A79A1B50E5F85FB2245D3586954AFB657ADE6C0 ();
// 0x0000008C Firebase.Platform.IAppConfigExtensions Firebase.Platform.Services::get_AppConfig()
extern void Services_get_AppConfig_mA0120A6A8C65ECC8886C56F6ADCF8276D7EC495A ();
// 0x0000008D System.Void Firebase.Platform.Services::set_AppConfig(Firebase.Platform.IAppConfigExtensions)
extern void Services_set_AppConfig_m8AC0B6FE015A987AF3EAA030436F69CAA74A2142 ();
// 0x0000008E System.Void Firebase.Platform.Services::set_Auth(Firebase.Platform.IAuthService)
extern void Services_set_Auth_mC7EA4640F2ECFFB45540109F9F8C52EBC96C32BF ();
// 0x0000008F Firebase.Platform.ICertificateService Firebase.Platform.Services::get_RootCerts()
extern void Services_get_RootCerts_mF05A0B117C8143BCD77947E94E231ECA2F267F7C ();
// 0x00000090 System.Void Firebase.Platform.Services::set_RootCerts(Firebase.Platform.ICertificateService)
extern void Services_set_RootCerts_mC25E7DE02CE948296C51159B4F9A16BA03D04768 ();
// 0x00000091 System.Void Firebase.Platform.Services::set_Clock(Firebase.Platform.IClockService)
extern void Services_set_Clock_mF9C55620425C2005E94108FD36420181CFBAE37C ();
// 0x00000092 System.Void Firebase.Platform.Services::set_HttpFactory(Firebase.Platform.IHttpFactoryService)
extern void Services_set_HttpFactory_mF76BB3BF6980250D0CC1690362CE23ED1B6B163C ();
// 0x00000093 Firebase.Platform.ILoggingService Firebase.Platform.Services::get_Logging()
extern void Services_get_Logging_m9F4D31D87491A8D014822BBFE8458D8E1671A323 ();
// 0x00000094 System.Void Firebase.Platform.Services::set_Logging(Firebase.Platform.ILoggingService)
extern void Services_set_Logging_mDAC80751B95ECC1750B80FAD942541095763D5EE ();
// 0x00000095 System.Void Firebase.Dispatcher::.ctor()
extern void Dispatcher__ctor_m524290773AE51A0B146046705A07E78B61C27268 ();
// 0x00000096 TResult Firebase.Dispatcher::Run(System.Func`1<TResult>)
// 0x00000097 System.Threading.Tasks.Task`1<TResult> Firebase.Dispatcher::RunAsync(System.Func`1<TResult>)
// 0x00000098 System.Threading.Tasks.Task`1<TResult> Firebase.Dispatcher::RunAsyncNow(System.Func`1<TResult>)
// 0x00000099 System.Boolean Firebase.Dispatcher::ManagesThisThread()
extern void Dispatcher_ManagesThisThread_mCC706F89C23B9407225833D81E5B359F6BDA2A3D ();
// 0x0000009A System.Void Firebase.Dispatcher::PollJobs()
extern void Dispatcher_PollJobs_m27F0E072ED9B5297E072A0AAFD37D3D76B748875 ();
// 0x0000009B System.Void Firebase.Dispatcher_CallbackStorage`1::.ctor()
// 0x0000009C TResult Firebase.Dispatcher_CallbackStorage`1::get_Result()
// 0x0000009D System.Void Firebase.Dispatcher_CallbackStorage`1::set_Result(TResult)
// 0x0000009E System.Exception Firebase.Dispatcher_CallbackStorage`1::get_Exception()
// 0x0000009F System.Void Firebase.Dispatcher_CallbackStorage`1::set_Exception(System.Exception)
// 0x000000A0 System.Void Firebase.Dispatcher_<Run>c__AnonStorey0`1::.ctor()
// 0x000000A1 System.Void Firebase.Dispatcher_<Run>c__AnonStorey0`1::<>m__0()
// 0x000000A2 System.Void Firebase.Dispatcher_<RunAsync>c__AnonStorey1`1::.ctor()
// 0x000000A3 System.Void Firebase.Dispatcher_<RunAsync>c__AnonStorey1`1::<>m__0()
// 0x000000A4 System.Void Firebase.Platform.IFirebaseAppUtils::TranslateDllNotFoundException(System.Action)
// 0x000000A5 System.Void Firebase.Platform.IFirebaseAppUtils::PollCallbacks()
// 0x000000A6 Firebase.Platform.IFirebaseAppPlatform Firebase.Platform.IFirebaseAppUtils::GetDefaultInstance()
// 0x000000A7 System.String Firebase.Platform.IFirebaseAppUtils::GetDefaultInstanceName()
// 0x000000A8 Firebase.Platform.PlatformLogLevel Firebase.Platform.IFirebaseAppUtils::GetLogLevel()
// 0x000000A9 System.Void Firebase.Platform.FirebaseAppUtilsStub::.ctor()
extern void FirebaseAppUtilsStub__ctor_mEB40FEB244E4D3938E937B53B2641FEACB13A0DB ();
// 0x000000AA Firebase.Platform.FirebaseAppUtilsStub Firebase.Platform.FirebaseAppUtilsStub::get_Instance()
extern void FirebaseAppUtilsStub_get_Instance_m257406CEB647CE6E7419C13FF14C99E617CECC1B ();
// 0x000000AB System.Void Firebase.Platform.FirebaseAppUtilsStub::TranslateDllNotFoundException(System.Action)
extern void FirebaseAppUtilsStub_TranslateDllNotFoundException_mF272BF92C2F0E8162A0D39A255FD16A3AB7127F0 ();
// 0x000000AC System.Void Firebase.Platform.FirebaseAppUtilsStub::PollCallbacks()
extern void FirebaseAppUtilsStub_PollCallbacks_m72BEEDCA8201C0F3F812207092B655865C78DD0E ();
// 0x000000AD Firebase.Platform.IFirebaseAppPlatform Firebase.Platform.FirebaseAppUtilsStub::GetDefaultInstance()
extern void FirebaseAppUtilsStub_GetDefaultInstance_m8CF2454E314233F175CF321CC0ADF4B7CB392F48 ();
// 0x000000AE System.String Firebase.Platform.FirebaseAppUtilsStub::GetDefaultInstanceName()
extern void FirebaseAppUtilsStub_GetDefaultInstanceName_mCB1A17F7BAFD51FE046A087C03D44605F9679BDE ();
// 0x000000AF Firebase.Platform.PlatformLogLevel Firebase.Platform.FirebaseAppUtilsStub::GetLogLevel()
extern void FirebaseAppUtilsStub_GetLogLevel_mB2B6B0AFBACB4724630BD1292849369B904D8C87 ();
// 0x000000B0 System.Void Firebase.Platform.FirebaseAppUtilsStub::.cctor()
extern void FirebaseAppUtilsStub__cctor_mE24FC5370D04DAFFEF7CA4C8FF4CC87EFD1F081C ();
// 0x000000B1 System.String Firebase.Platform.IFirebaseAppPlatform::get_Name()
// 0x000000B2 System.Void Firebase.Platform.MainThreadProperty`1::.ctor(System.Func`1<T>)
// 0x000000B3 T Firebase.Platform.MainThreadProperty`1::get_Value()
// 0x000000B4 T Firebase.Platform.MainThreadProperty`1::<get_Value>m__0()
// 0x000000B5 System.Void <>__AnonType0`3::.ctor(<Platform>__T,<IsEditor>__T,<InstallationRequired>__T)
// 0x000000B6 <Platform>__T <>__AnonType0`3::get_Platform()
// 0x000000B7 <IsEditor>__T <>__AnonType0`3::get_IsEditor()
// 0x000000B8 <InstallationRequired>__T <>__AnonType0`3::get_InstallationRequired()
// 0x000000B9 System.Boolean <>__AnonType0`3::Equals(System.Object)
// 0x000000BA System.Int32 <>__AnonType0`3::GetHashCode()
// 0x000000BB System.String <>__AnonType0`3::ToString()
static Il2CppMethodPointer s_methodPointers[187] = 
{
	InstallRootCerts__ctor_m5E66DEF67729378EAF57CBF0DEBAEBA189EFB8C0,
	InstallRootCerts_get_InstallationRequired_m9B0756A9F1C52E761BC4AADA71B09E62146FC240,
	InstallRootCerts_get_Instance_mB0904BD22EF5278939F2847E8CAF68F94828202B,
	InstallRootCerts_AFunctionThatDoesNotExistInternal_mC3D31F79F5AFAABB6821F7034F45E04E853E4C62,
	InstallRootCerts_AFunctionThatDoesNotExist_m47709A6958E5FA7A36F3763E03C2A452872E7F2C,
	InstallRootCerts_IsCertBugPresent_m22F37D714F73AB63EF893E8BD68525A4A481604D,
	InstallRootCerts_DecodeBase64Blobs_m29EDEB21AB6357AA10AD48DF0585BC46A49D2A38,
	InstallRootCerts_DecodeCertificateCollectionFromString_mF0C89E88BEA06136D539C2BB81941F9995B5A157,
	InstallRootCerts_DecodeDefaultCollection_m5F99CBE2450D0F4A65691C036F220BC393D99AF9,
	InstallRootCerts_DecodeCollection_m846B16E66820F63FC97C1D9D55A0A9B94943E83C,
	InstallRootCerts_InstallDefaultCRLs_m91C8AA55996B45FB59A7781F5BF28DCEEF79C7E9,
	InstallRootCerts_HackRefreshMonoRootStore_m00267CDC5A9F5DFB0A9B8DF8A61249A16F6255B1,
	InstallRootCerts_Install_mE11E7BF36DA4BBF605B38D859DB77A57C8617B1E,
	InstallRootCerts__cctor_m232DBEB94A155625714EEABD355079CBB4CE7554,
	InstallRootCerts_U3CInstallRootCertsU3Em__0_m743263E1CEBBF1C13EBF8B7FB3ED7F1A05B433E2,
	FirebaseEditorDispatcher_get_EditorApplicationType_m1F7FD788E0579F30AC63063A4AEBAD065A2ACE7E,
	FirebaseEditorDispatcher_get_EditorIsPlaying_m8E80FCF745D38EF53E550955ED6634498B663E2B,
	FirebaseEditorDispatcher_get_EditorIsPlayingOrWillChangePlaymode_mC656FF2ABDE09D856D738444067F34B9D6F6E741,
	FirebaseEditorDispatcher_StartEditorUpdate_m9014FC7984B5CE57EBC0A23B1309C9B05997FD7B,
	FirebaseEditorDispatcher_StopEditorUpdate_mED528078F895F70293A8DF07275180DF2F7C6E24,
	FirebaseEditorDispatcher_Update_m1CE1E7BDDB5204EE82EC28C6C7B5F83AB64C5D63,
	FirebaseEditorDispatcher_ListenToPlayState_mFADB143214E7B22867C4D26CD6C1B384C5BA00FD,
	FirebaseEditorDispatcher_PlayModeStateChanged_mA77B4BC297642FC52BB8AB01FBB79E98B1C47288,
	NULL,
	FirebaseEditorDispatcher_AddRemoveCallbackToField_m285003B251B4D5067546A49017F6B5F5C8405FA7,
	FirebaseHandler__cctor_mCC22861A54093E75FF8C063CA2B0AC1975F832B5,
	FirebaseHandler__ctor_mF69C13C09805BAF05E457BA75976181A117E091A,
	FirebaseHandler_get_AppUtils_m4B1DD0CA9C44AB6E9A12EED231FA8EF265948A7C,
	FirebaseHandler_set_AppUtils_m91387365FFE85468A59A0A2433DA483F08602DBF,
	FirebaseHandler_get_TickCount_m9A88A2C0949942CDFAC05C061004305B148E8019,
	FirebaseHandler_get_ThreadDispatcher_m576CC2F52B6A331E1D5D7ABDC493937253D51189,
	FirebaseHandler_set_ThreadDispatcher_m7C1647AB2F233DD9D808C3AEA9B0469F19F653F2,
	FirebaseHandler_get_IsPlayMode_m2784DFEDC9F23AE0072B174583BDEBB1D9F8B7FD,
	FirebaseHandler_set_IsPlayMode_mDFC0BA0F8ADCB56155A7278823E9669758BE38B5,
	FirebaseHandler_StartMonoBehaviour_m6728F8C35C8A08C2933B871CA4716CD6C95314DA,
	FirebaseHandler_StopMonoBehaviour_m741EC378D41A3166E8DE6532F03A195E88C033C2,
	NULL,
	NULL,
	FirebaseHandler_get_DefaultInstance_mDE47F7545F78ED4AAA09566895610B8DD8A6FE02,
	FirebaseHandler_CreatePartialOnMainThread_m96D4E360CF58711B4865E546DCE75CACCFA34C78,
	FirebaseHandler_Create_m89DB93F791D7C61D45C79484E9207C4967374BEA,
	FirebaseHandler_Update_m4A49185D21631806389823523179738DB897EB33,
	FirebaseHandler_OnApplicationFocus_m04ED5D86AF9BA18E761EACFEA9455E1829008B8E,
	FirebaseHandler_OnMonoBehaviourDestroyed_m93E0EE038A3E5F19681DAFEE5445ABE6065AA086,
	FirebaseHandler_U3CStopMonoBehaviourU3Em__0_mF2ACC3BC95F892A6B871A99E15543259938D56C6,
	FirebaseHandler_U3CUpdateU3Em__1_m34B81AC76DADE8936F37C2F2F1F6C0952CCB7CA5,
	ApplicationFocusChangedEventArgs__ctor_mB1717C3BC9E32E3336D360134B6073C8EE5E4EBA,
	ApplicationFocusChangedEventArgs_set_HasFocus_mEA4178D6C578686CE84F1E088EA370AB7225A330,
	U3CCreatePartialOnMainThreadU3Ec__AnonStorey0__ctor_m3D8C381BEF70E6FA9C1D6AF854ED6EAF156A001F,
	U3CCreatePartialOnMainThreadU3Ec__AnonStorey0_U3CU3Em__0_mBF2F55D34BE7497CD9E43AF262B6EA52BA7990EF,
	FirebaseLogger_IsStackTraceLogTypeIncompatibleWithNativeLogs_m32A916D1E8992541EB447E499B85BFF15CEF559F,
	FirebaseLogger_CurrentStackTraceLogTypeIsIncompatibleWithNativeLogs_m51B2392AEFCF6E04C363994806DCE5EAE27EDA2B,
	FirebaseLogger_get_CanRedirectNativeLogs_m29C5B1ACE567CEA890B6DA19D91AF37D1EDEEBC8,
	FirebaseLogger_LogMessage_mEF2033A0588D651D162FFE29C9FA77A39E51233E,
	FirebaseLogger__cctor_mA0565673CF29882DF40267833252A8B96AC85B77,
	FirebaseMonoBehaviour__ctor_m137AF46B744D8805EC43DC70A0DCE49AF4F2B326,
	FirebaseMonoBehaviour_GetFirebaseHandlerOrDestroyGameObject_m77DA35BC03EF563A295FDA20901881CFC161A89B,
	FirebaseMonoBehaviour_OnEnable_mB3D6B00AA0A0A4FFD25FDA48E28AFCE52E359B80,
	FirebaseMonoBehaviour_Update_mC3DFEB73F14CD64F3C26006141EF71839E463A65,
	FirebaseMonoBehaviour_OnApplicationFocus_m84A811D74316C5E254EF57A6253755EBBD8585E9,
	FirebaseMonoBehaviour_OnDestroy_mDCEE8FA7C5820AA947E110083347A7ED4BE71FD5,
	PlatformInformation_get_IsAndroid_m61250012539009D27371075104AFBDE2549BB77E,
	PlatformInformation_get_IsIOS_mEC53C8E1719192F4A5E5A5C919752D9772706573,
	PlatformInformation_get_DefaultConfigLocation_m7B32A608B4B90F6749B02AC1C0E87E4C904E9608,
	PlatformInformation_get_RealtimeSinceStartup_mA5552638B79722FD7378E430054A9EE21A7F4742,
	PlatformInformation_set_RealtimeSinceStartupSafe_mB37999FFE3AF262FB2234E0308CB8155D7827C47,
	PlatformInformation_get_RuntimeName_mCD019D8389883E820ED30D97642D63F0BA844EAD,
	PlatformInformation_get_RuntimeVersion_m947E74AAC2129616DA7A9330BCE4B79EA8A422F0,
	PlatformInformation_U3Cget_DefaultConfigLocationU3Em__0_m3E69FE6A0EE918EE10A2B590D4DE50A0CA922B01,
	PlatformInformation_U3Cget_RuntimeVersionU3Em__1_m8405A3B38E79A93CCE9C1812CF66C8A3F8B19735,
	UnityConfigExtensions__ctor_m4A9B319E06B0F2778FBADFE4C06F5EF95C9BBD68,
	UnityConfigExtensions_get_DefaultInstance_m963AB0EF6217BB9E9569CF4D23E2A0F2753A7B35,
	UnityConfigExtensions_GetWriteablePath_mCF0DA0C393E09FD6D92E2218E9B3519647DDD3CC,
	UnityConfigExtensions__cctor_m3AE99E171092FB6DBB559F1ECF4E0A4B28497058,
	UnityConfigExtensions_U3CGetWriteablePathU3Em__0_mD4E52B78753253D075126E01272C31E7CAD43AA7,
	UnityHttpFactoryService__ctor_mE3C1BAFDBECF9E8725BE5A0016D715923BA75B83,
	UnityHttpFactoryService_get_Instance_m899AA0837373F574AC89B3065FF014F16A6C7D04,
	UnityHttpFactoryService__cctor_m8549AF9CF9A865588FF3E848786880EA652E6C17,
	UnityLoggingService__ctor_m3BCC6353BEADE287DF1A0C9781B6187576523441,
	UnityLoggingService_get_Instance_m021810065C28BAD66B00CA2EC2DC55AD3EE85A4E,
	UnityLoggingService_LogMessage_mE989034EB3A56D1F926589C7A987F7E04C6AC432,
	UnityLoggingService__cctor_m07430A774E2E27DE95B135565549505BC99BFAF3,
	UnityPlatformServices_SetupServices_mC37344AC6AB871E0DCEC44A26FBC98BB05776CE4,
	UnitySynchronizationContext__ctor_m47697B161718D309E5BDEE021A7652F2610B3811,
	UnitySynchronizationContext_Create_m457A7DF239D69F664A4C5A71A51831D10F8C5C9B,
	UnitySynchronizationContext_Destroy_m173DAA6D418F0749046FC98BE3244ECF0CD37188,
	UnitySynchronizationContext_GetThreadEvent_mD3A2B8BC1310D4C5F17918280F14EF089DD8164A,
	UnitySynchronizationContext_Post_m4450F9D1EEF52CB7EED4C27ACEFC4F5324C6BFBF,
	UnitySynchronizationContext_Send_m537EE81F72568A13624BBD4CB4D8796946722A6D,
	UnitySynchronizationContext__cctor_m3B5D4F04B0CEEF803ED5E341A4AFCD813F706402,
	SynchronizationContextBehavoir__ctor_m4DA714845D3AB1121B0A4EBE7065F59A49611B89,
	SynchronizationContextBehavoir_get_CallbackQueue_mE2EE07CB1868612C976725049F507F5D1B41D346,
	SynchronizationContextBehavoir_Start_m64A7A9B67267950EB78C38059D7A32ACBEA81BDD,
	U3CStartU3Ec__Iterator0__ctor_mDD3B386E3E7786CCE773C01C6A53026DC86236A4,
	U3CStartU3Ec__Iterator0_MoveNext_m328175782CEC1827C4BE9ED264EDDC699FE03914,
	U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mEDB0AD40F4344F69604F7BFC7037760A6A87DF35,
	U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m26EBD49E54EFC0704D7991C2EE1BE9897DFF846E,
	U3CStartU3Ec__Iterator0_Dispose_mA756A58331F3B4E68EBB6C33E8F8B8F2D06EB036,
	U3CStartU3Ec__Iterator0_Reset_mD96EC7F0BA275A6D00CCD79A3BBD8B687627CC4D,
	U3CSignaledCoroutineU3Ec__Iterator0__ctor_mF997D780AE9CBE903A86F4DDD8A345BD598D91A0,
	U3CSignaledCoroutineU3Ec__Iterator0_MoveNext_m079887CA72B97AA900D67CB3CB48EC0C7807D4AD,
	U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_mB982F27D316E6A73A9C6BFEF2FB947363EED9EA9,
	U3CSignaledCoroutineU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m302C721344DAF763C8ECB056ED744289DEA7D71F,
	U3CSignaledCoroutineU3Ec__Iterator0_Dispose_m5864B129EB0690FFF9B3330A4111E9F1589AC067,
	U3CSignaledCoroutineU3Ec__Iterator0_Reset_m312986B152936253BB73ED135A06CBA025EE9792,
	U3CSendCoroutineU3Ec__AnonStorey1__ctor_mA62651790837A434BE44A8B171FD13CC3A8D1C80,
	U3CSendCoroutineU3Ec__AnonStorey2__ctor_mC838ED0C34B77644FE3750548DF88D0DC7E742A7,
	U3CSendU3Ec__AnonStorey3__ctor_m9DA398536AD41E24A0298DFF850C8AA3B484D48A,
	U3CSendU3Ec__AnonStorey4__ctor_mADD177ACDB3F1A48CCD98C465DF3750DFD0133A8,
	U3CSendU3Ec__AnonStorey4_U3CU3Em__0_m39EC76752B64CAFBC5D6D17ED977868570FCF715,
	AppConfigExtensions__ctor_mC5065C76DA54B0C1822FC58959AC5FED94F64328,
	AppConfigExtensions_get_Instance_m9E23D31FAA5845CC6A8AB3C6A676DCCE0880C966,
	AppConfigExtensions_GetWriteablePath_m204671D085AD529E35E180E84C8A000FFC77F838,
	AppConfigExtensions_GetCertPemFile_m6893E9609F3F4E7FA0866945ED85224625BBDC84,
	NULL,
	AppConfigExtensions__cctor_m6F6AA804ECFF8AFE261311924D919CAE0C075096,
	BaseAuthService__ctor_m7F9F1EFFAB9D91C1887DAEC04ABFA1542DB0A91E,
	BaseAuthService_get_BaseInstance_m1AB3EF619AE43E869D22473FD44A0404225BD366,
	BaseAuthService__cctor_m07047C89EE3756A2A9D221E334563592D33316CD,
	SystemClock__ctor_mF0290527299F60106E6FCAB61B3697D77C856DDF,
	SystemClock__cctor_m78C791FBA8496CA05264591D55F49610FFD4A33E,
	ExceptionAggregator_get_Exceptions_mB8771E2A0D9AFC2BF5A29214B58FAE0621306BE2,
	ExceptionAggregator_GetAndClearPendingExceptions_m0428C791FBC34F687E53E93C8283BF1BDAD52705,
	ExceptionAggregator_ThrowAndClearPendingExceptions_m43EDFA771E277D927E98305CF105F598C2A19D96,
	ExceptionAggregator_LogException_m2D22E7AA0907E7185DDF0F7F5C2EE702265CB12C,
	ExceptionAggregator_Wrap_m93A9BD0DC888F1D02E77860288095CB49D4CFFC2,
	NULL,
	NULL,
	NULL,
	NoopCertificateService__ctor_mD188767256CCAB4CD8DDBC37C45825C4F2491B50,
	NoopCertificateService_get_Instance_mA210CAF63B7A85CCE844FC7C4E7F2146591FFF1A,
	NoopCertificateService_Install_m33FC2BAB7B4D61567F85C4C8394C9299450FE3D8,
	NoopCertificateService__cctor_mE6CF8FB9071FA36BDDE45474D96D4427B6F84F5D,
	NULL,
	DebugLogger__ctor_m6A03E93E7B5D3F77F97E425B65396F7542998E26,
	DebugLogger_get_Instance_m06E28364924B44D279EA5EEB003DBA373D3FA2B7,
	DebugLogger_LogMessage_mE2F38D331077CFD6A38F3EE023AA8717CB9BF3BE,
	DebugLogger__cctor_mD5064826923495415C2852B63AA70F8D8FA535B6,
	Services__cctor_m0A79A1B50E5F85FB2245D3586954AFB657ADE6C0,
	Services_get_AppConfig_mA0120A6A8C65ECC8886C56F6ADCF8276D7EC495A,
	Services_set_AppConfig_m8AC0B6FE015A987AF3EAA030436F69CAA74A2142,
	Services_set_Auth_mC7EA4640F2ECFFB45540109F9F8C52EBC96C32BF,
	Services_get_RootCerts_mF05A0B117C8143BCD77947E94E231ECA2F267F7C,
	Services_set_RootCerts_mC25E7DE02CE948296C51159B4F9A16BA03D04768,
	Services_set_Clock_mF9C55620425C2005E94108FD36420181CFBAE37C,
	Services_set_HttpFactory_mF76BB3BF6980250D0CC1690362CE23ED1B6B163C,
	Services_get_Logging_m9F4D31D87491A8D014822BBFE8458D8E1671A323,
	Services_set_Logging_mDAC80751B95ECC1750B80FAD942541095763D5EE,
	Dispatcher__ctor_m524290773AE51A0B146046705A07E78B61C27268,
	NULL,
	NULL,
	NULL,
	Dispatcher_ManagesThisThread_mCC706F89C23B9407225833D81E5B359F6BDA2A3D,
	Dispatcher_PollJobs_m27F0E072ED9B5297E072A0AAFD37D3D76B748875,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FirebaseAppUtilsStub__ctor_mEB40FEB244E4D3938E937B53B2641FEACB13A0DB,
	FirebaseAppUtilsStub_get_Instance_m257406CEB647CE6E7419C13FF14C99E617CECC1B,
	FirebaseAppUtilsStub_TranslateDllNotFoundException_mF272BF92C2F0E8162A0D39A255FD16A3AB7127F0,
	FirebaseAppUtilsStub_PollCallbacks_m72BEEDCA8201C0F3F812207092B655865C78DD0E,
	FirebaseAppUtilsStub_GetDefaultInstance_m8CF2454E314233F175CF321CC0ADF4B7CB392F48,
	FirebaseAppUtilsStub_GetDefaultInstanceName_mCB1A17F7BAFD51FE046A087C03D44605F9679BDE,
	FirebaseAppUtilsStub_GetLogLevel_mB2B6B0AFBACB4724630BD1292849369B904D8C87,
	FirebaseAppUtilsStub__cctor_mE24FC5370D04DAFFEF7CA4C8FF4CC87EFD1F081C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[187] = 
{
	13,
	77,
	19,
	8,
	8,
	74,
	2,
	0,
	19,
	0,
	158,
	13,
	6,
	8,
	19,
	19,
	77,
	77,
	8,
	8,
	8,
	816,
	8,
	-1,
	1474,
	8,
	13,
	19,
	30,
	130,
	19,
	30,
	17,
	44,
	13,
	13,
	-1,
	-1,
	19,
	30,
	30,
	13,
	44,
	30,
	77,
	13,
	13,
	44,
	13,
	13,
	74,
	77,
	77,
	554,
	8,
	13,
	14,
	13,
	13,
	44,
	13,
	77,
	77,
	19,
	1174,
	1306,
	19,
	19,
	19,
	19,
	13,
	19,
	6,
	8,
	19,
	13,
	19,
	8,
	13,
	19,
	89,
	8,
	8,
	4,
	30,
	8,
	14,
	23,
	23,
	8,
	13,
	14,
	14,
	13,
	17,
	14,
	14,
	13,
	13,
	13,
	17,
	14,
	14,
	13,
	13,
	13,
	13,
	13,
	13,
	4,
	13,
	19,
	6,
	6,
	-1,
	8,
	13,
	19,
	8,
	13,
	8,
	19,
	19,
	8,
	0,
	30,
	6,
	6,
	6,
	13,
	19,
	6,
	8,
	89,
	13,
	19,
	89,
	8,
	8,
	19,
	30,
	30,
	19,
	30,
	30,
	30,
	19,
	30,
	13,
	-1,
	-1,
	-1,
	17,
	13,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	13,
	14,
	14,
	18,
	13,
	19,
	4,
	13,
	14,
	14,
	18,
	8,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x02000025, { 30, 3 } },
	{ 0x02000026, { 33, 3 } },
	{ 0x0200002B, { 36, 6 } },
	{ 0x0200002C, { 42, 16 } },
	{ 0x06000025, { 0, 2 } },
	{ 0x06000026, { 2, 2 } },
	{ 0x06000073, { 4, 5 } },
	{ 0x06000096, { 9, 8 } },
	{ 0x06000097, { 17, 7 } },
	{ 0x06000098, { 24, 6 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[58] = 
{
	{ (Il2CppRGCTXDataType)3, 11914 },
	{ (Il2CppRGCTXDataType)3, 11915 },
	{ (Il2CppRGCTXDataType)3, 11916 },
	{ (Il2CppRGCTXDataType)3, 11917 },
	{ (Il2CppRGCTXDataType)3, 11918 },
	{ (Il2CppRGCTXDataType)2, 16367 },
	{ (Il2CppRGCTXDataType)3, 11919 },
	{ (Il2CppRGCTXDataType)3, 11920 },
	{ (Il2CppRGCTXDataType)3, 11921 },
	{ (Il2CppRGCTXDataType)2, 19573 },
	{ (Il2CppRGCTXDataType)3, 11922 },
	{ (Il2CppRGCTXDataType)3, 11923 },
	{ (Il2CppRGCTXDataType)2, 19574 },
	{ (Il2CppRGCTXDataType)3, 11924 },
	{ (Il2CppRGCTXDataType)3, 11925 },
	{ (Il2CppRGCTXDataType)3, 11926 },
	{ (Il2CppRGCTXDataType)3, 11927 },
	{ (Il2CppRGCTXDataType)2, 19575 },
	{ (Il2CppRGCTXDataType)3, 11928 },
	{ (Il2CppRGCTXDataType)3, 11929 },
	{ (Il2CppRGCTXDataType)2, 19576 },
	{ (Il2CppRGCTXDataType)3, 11930 },
	{ (Il2CppRGCTXDataType)3, 11931 },
	{ (Il2CppRGCTXDataType)3, 11932 },
	{ (Il2CppRGCTXDataType)3, 11933 },
	{ (Il2CppRGCTXDataType)3, 11934 },
	{ (Il2CppRGCTXDataType)2, 19577 },
	{ (Il2CppRGCTXDataType)3, 11935 },
	{ (Il2CppRGCTXDataType)3, 11936 },
	{ (Il2CppRGCTXDataType)3, 11937 },
	{ (Il2CppRGCTXDataType)3, 11938 },
	{ (Il2CppRGCTXDataType)3, 11939 },
	{ (Il2CppRGCTXDataType)3, 11940 },
	{ (Il2CppRGCTXDataType)3, 11941 },
	{ (Il2CppRGCTXDataType)3, 11942 },
	{ (Il2CppRGCTXDataType)3, 11943 },
	{ (Il2CppRGCTXDataType)3, 11944 },
	{ (Il2CppRGCTXDataType)2, 16437 },
	{ (Il2CppRGCTXDataType)3, 11945 },
	{ (Il2CppRGCTXDataType)3, 11946 },
	{ (Il2CppRGCTXDataType)3, 11947 },
	{ (Il2CppRGCTXDataType)3, 11948 },
	{ (Il2CppRGCTXDataType)2, 19578 },
	{ (Il2CppRGCTXDataType)3, 11949 },
	{ (Il2CppRGCTXDataType)2, 19579 },
	{ (Il2CppRGCTXDataType)3, 11950 },
	{ (Il2CppRGCTXDataType)3, 11951 },
	{ (Il2CppRGCTXDataType)2, 19580 },
	{ (Il2CppRGCTXDataType)3, 11952 },
	{ (Il2CppRGCTXDataType)3, 11953 },
	{ (Il2CppRGCTXDataType)2, 19581 },
	{ (Il2CppRGCTXDataType)3, 11954 },
	{ (Il2CppRGCTXDataType)3, 11955 },
	{ (Il2CppRGCTXDataType)3, 11956 },
	{ (Il2CppRGCTXDataType)3, 11957 },
	{ (Il2CppRGCTXDataType)2, 16443 },
	{ (Il2CppRGCTXDataType)2, 16444 },
	{ (Il2CppRGCTXDataType)2, 16445 },
};
extern const Il2CppCodeGenModule g_Firebase_PlatformCodeGenModule;
const Il2CppCodeGenModule g_Firebase_PlatformCodeGenModule = 
{
	"Firebase.Platform.dll",
	187,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	10,
	s_rgctxIndices,
	58,
	s_rgctxValues,
	NULL,
};
