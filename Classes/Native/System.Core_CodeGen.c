﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000009 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000B TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000011 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000012 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000014 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000015 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000016 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000017 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000018 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000019 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000001A System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000001B System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000001C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000001D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000001E System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000001F System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000020 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000021 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000022 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000023 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000024 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000025 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000026 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000028 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000029 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000002E System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000033 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000034 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000037 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000038 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000039 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000003E System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000042 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000043 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000044 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000045 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000046 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000047 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000048 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000049 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000004A System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000004B System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000004C System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000004D System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x0000004E TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000004F System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000050 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000051 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000052 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000053 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000054 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000055 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000056 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000057 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000058 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000059 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000005A System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000005B System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000005C TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000005D System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000005E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000005F System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000060 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000061 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000062 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000063 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000064 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000065 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000066 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000067 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000068 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000069 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000006A System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000006B System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000006C System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006D System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000006E System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000006F System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000070 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000071 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000072 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000073 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000074 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x00000075 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000076 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000077 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000078 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000079 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x0000007A System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x0000007B System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000007C System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000007D System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000007E System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000007F T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000080 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000081 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[129] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[129] = 
{
	0,
	0,
	19,
	19,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[41] = 
{
	{ 0x02000004, { 67, 4 } },
	{ 0x02000005, { 71, 9 } },
	{ 0x02000006, { 82, 7 } },
	{ 0x02000007, { 91, 10 } },
	{ 0x02000008, { 103, 11 } },
	{ 0x02000009, { 117, 9 } },
	{ 0x0200000A, { 129, 12 } },
	{ 0x0200000B, { 144, 1 } },
	{ 0x0200000C, { 145, 2 } },
	{ 0x0200000E, { 147, 3 } },
	{ 0x0200000F, { 152, 5 } },
	{ 0x02000010, { 157, 7 } },
	{ 0x02000011, { 164, 3 } },
	{ 0x02000012, { 167, 7 } },
	{ 0x02000013, { 174, 4 } },
	{ 0x02000014, { 178, 34 } },
	{ 0x02000016, { 212, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 5 } },
	{ 0x06000008, { 25, 5 } },
	{ 0x06000009, { 30, 2 } },
	{ 0x0600000A, { 32, 1 } },
	{ 0x0600000B, { 33, 3 } },
	{ 0x0600000C, { 36, 2 } },
	{ 0x0600000D, { 38, 4 } },
	{ 0x0600000E, { 42, 4 } },
	{ 0x0600000F, { 46, 4 } },
	{ 0x06000010, { 50, 3 } },
	{ 0x06000011, { 53, 3 } },
	{ 0x06000012, { 56, 1 } },
	{ 0x06000013, { 57, 3 } },
	{ 0x06000014, { 60, 2 } },
	{ 0x06000015, { 62, 5 } },
	{ 0x06000025, { 80, 2 } },
	{ 0x0600002A, { 89, 2 } },
	{ 0x0600002F, { 101, 2 } },
	{ 0x06000035, { 114, 3 } },
	{ 0x0600003A, { 126, 3 } },
	{ 0x0600003F, { 141, 3 } },
	{ 0x06000049, { 150, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[214] = 
{
	{ (Il2CppRGCTXDataType)2, 17924 },
	{ (Il2CppRGCTXDataType)3, 10365 },
	{ (Il2CppRGCTXDataType)2, 17925 },
	{ (Il2CppRGCTXDataType)2, 17926 },
	{ (Il2CppRGCTXDataType)3, 10366 },
	{ (Il2CppRGCTXDataType)2, 17927 },
	{ (Il2CppRGCTXDataType)2, 17928 },
	{ (Il2CppRGCTXDataType)3, 10367 },
	{ (Il2CppRGCTXDataType)2, 17929 },
	{ (Il2CppRGCTXDataType)3, 10368 },
	{ (Il2CppRGCTXDataType)2, 17930 },
	{ (Il2CppRGCTXDataType)3, 10369 },
	{ (Il2CppRGCTXDataType)2, 17931 },
	{ (Il2CppRGCTXDataType)2, 17932 },
	{ (Il2CppRGCTXDataType)3, 10370 },
	{ (Il2CppRGCTXDataType)2, 17933 },
	{ (Il2CppRGCTXDataType)2, 17934 },
	{ (Il2CppRGCTXDataType)3, 10371 },
	{ (Il2CppRGCTXDataType)2, 17935 },
	{ (Il2CppRGCTXDataType)3, 10372 },
	{ (Il2CppRGCTXDataType)2, 17936 },
	{ (Il2CppRGCTXDataType)3, 10373 },
	{ (Il2CppRGCTXDataType)3, 10374 },
	{ (Il2CppRGCTXDataType)2, 12653 },
	{ (Il2CppRGCTXDataType)3, 10375 },
	{ (Il2CppRGCTXDataType)2, 17937 },
	{ (Il2CppRGCTXDataType)3, 10376 },
	{ (Il2CppRGCTXDataType)3, 10377 },
	{ (Il2CppRGCTXDataType)2, 12660 },
	{ (Il2CppRGCTXDataType)3, 10378 },
	{ (Il2CppRGCTXDataType)2, 17938 },
	{ (Il2CppRGCTXDataType)3, 10379 },
	{ (Il2CppRGCTXDataType)3, 10380 },
	{ (Il2CppRGCTXDataType)2, 17939 },
	{ (Il2CppRGCTXDataType)3, 10381 },
	{ (Il2CppRGCTXDataType)3, 10382 },
	{ (Il2CppRGCTXDataType)2, 12675 },
	{ (Il2CppRGCTXDataType)3, 10383 },
	{ (Il2CppRGCTXDataType)2, 17940 },
	{ (Il2CppRGCTXDataType)2, 17941 },
	{ (Il2CppRGCTXDataType)2, 12676 },
	{ (Il2CppRGCTXDataType)2, 17942 },
	{ (Il2CppRGCTXDataType)2, 17943 },
	{ (Il2CppRGCTXDataType)2, 17944 },
	{ (Il2CppRGCTXDataType)2, 12678 },
	{ (Il2CppRGCTXDataType)2, 17945 },
	{ (Il2CppRGCTXDataType)2, 17946 },
	{ (Il2CppRGCTXDataType)2, 17947 },
	{ (Il2CppRGCTXDataType)2, 12680 },
	{ (Il2CppRGCTXDataType)2, 17948 },
	{ (Il2CppRGCTXDataType)2, 12682 },
	{ (Il2CppRGCTXDataType)2, 17949 },
	{ (Il2CppRGCTXDataType)3, 10384 },
	{ (Il2CppRGCTXDataType)2, 17950 },
	{ (Il2CppRGCTXDataType)2, 12685 },
	{ (Il2CppRGCTXDataType)2, 17951 },
	{ (Il2CppRGCTXDataType)2, 12687 },
	{ (Il2CppRGCTXDataType)2, 12689 },
	{ (Il2CppRGCTXDataType)2, 17952 },
	{ (Il2CppRGCTXDataType)3, 10385 },
	{ (Il2CppRGCTXDataType)2, 17953 },
	{ (Il2CppRGCTXDataType)3, 10386 },
	{ (Il2CppRGCTXDataType)3, 10387 },
	{ (Il2CppRGCTXDataType)2, 17954 },
	{ (Il2CppRGCTXDataType)2, 12694 },
	{ (Il2CppRGCTXDataType)2, 17955 },
	{ (Il2CppRGCTXDataType)2, 12696 },
	{ (Il2CppRGCTXDataType)3, 10388 },
	{ (Il2CppRGCTXDataType)3, 10389 },
	{ (Il2CppRGCTXDataType)2, 12699 },
	{ (Il2CppRGCTXDataType)3, 10390 },
	{ (Il2CppRGCTXDataType)3, 10391 },
	{ (Il2CppRGCTXDataType)2, 12711 },
	{ (Il2CppRGCTXDataType)2, 17956 },
	{ (Il2CppRGCTXDataType)3, 10392 },
	{ (Il2CppRGCTXDataType)3, 10393 },
	{ (Il2CppRGCTXDataType)2, 12713 },
	{ (Il2CppRGCTXDataType)2, 17847 },
	{ (Il2CppRGCTXDataType)3, 10394 },
	{ (Il2CppRGCTXDataType)3, 10395 },
	{ (Il2CppRGCTXDataType)2, 17957 },
	{ (Il2CppRGCTXDataType)3, 10396 },
	{ (Il2CppRGCTXDataType)3, 10397 },
	{ (Il2CppRGCTXDataType)2, 12723 },
	{ (Il2CppRGCTXDataType)2, 17958 },
	{ (Il2CppRGCTXDataType)3, 10398 },
	{ (Il2CppRGCTXDataType)3, 10399 },
	{ (Il2CppRGCTXDataType)3, 9984 },
	{ (Il2CppRGCTXDataType)3, 10400 },
	{ (Il2CppRGCTXDataType)2, 17959 },
	{ (Il2CppRGCTXDataType)3, 10401 },
	{ (Il2CppRGCTXDataType)3, 10402 },
	{ (Il2CppRGCTXDataType)2, 12735 },
	{ (Il2CppRGCTXDataType)2, 17960 },
	{ (Il2CppRGCTXDataType)3, 10403 },
	{ (Il2CppRGCTXDataType)3, 10404 },
	{ (Il2CppRGCTXDataType)3, 10405 },
	{ (Il2CppRGCTXDataType)3, 10406 },
	{ (Il2CppRGCTXDataType)3, 10407 },
	{ (Il2CppRGCTXDataType)3, 9990 },
	{ (Il2CppRGCTXDataType)3, 10408 },
	{ (Il2CppRGCTXDataType)2, 17961 },
	{ (Il2CppRGCTXDataType)3, 10409 },
	{ (Il2CppRGCTXDataType)3, 10410 },
	{ (Il2CppRGCTXDataType)2, 12748 },
	{ (Il2CppRGCTXDataType)2, 17962 },
	{ (Il2CppRGCTXDataType)3, 10411 },
	{ (Il2CppRGCTXDataType)3, 10412 },
	{ (Il2CppRGCTXDataType)2, 12750 },
	{ (Il2CppRGCTXDataType)2, 17963 },
	{ (Il2CppRGCTXDataType)3, 10413 },
	{ (Il2CppRGCTXDataType)3, 10414 },
	{ (Il2CppRGCTXDataType)2, 17964 },
	{ (Il2CppRGCTXDataType)3, 10415 },
	{ (Il2CppRGCTXDataType)3, 10416 },
	{ (Il2CppRGCTXDataType)2, 17965 },
	{ (Il2CppRGCTXDataType)3, 10417 },
	{ (Il2CppRGCTXDataType)3, 10418 },
	{ (Il2CppRGCTXDataType)2, 12765 },
	{ (Il2CppRGCTXDataType)2, 17966 },
	{ (Il2CppRGCTXDataType)3, 10419 },
	{ (Il2CppRGCTXDataType)3, 10420 },
	{ (Il2CppRGCTXDataType)3, 10421 },
	{ (Il2CppRGCTXDataType)3, 10001 },
	{ (Il2CppRGCTXDataType)2, 17967 },
	{ (Il2CppRGCTXDataType)3, 10422 },
	{ (Il2CppRGCTXDataType)3, 10423 },
	{ (Il2CppRGCTXDataType)2, 17968 },
	{ (Il2CppRGCTXDataType)3, 10424 },
	{ (Il2CppRGCTXDataType)3, 10425 },
	{ (Il2CppRGCTXDataType)2, 12781 },
	{ (Il2CppRGCTXDataType)2, 17969 },
	{ (Il2CppRGCTXDataType)3, 10426 },
	{ (Il2CppRGCTXDataType)3, 10427 },
	{ (Il2CppRGCTXDataType)3, 10428 },
	{ (Il2CppRGCTXDataType)3, 10429 },
	{ (Il2CppRGCTXDataType)3, 10430 },
	{ (Il2CppRGCTXDataType)3, 10431 },
	{ (Il2CppRGCTXDataType)3, 10007 },
	{ (Il2CppRGCTXDataType)2, 17970 },
	{ (Il2CppRGCTXDataType)3, 10432 },
	{ (Il2CppRGCTXDataType)3, 10433 },
	{ (Il2CppRGCTXDataType)2, 17971 },
	{ (Il2CppRGCTXDataType)3, 10434 },
	{ (Il2CppRGCTXDataType)3, 10435 },
	{ (Il2CppRGCTXDataType)3, 10436 },
	{ (Il2CppRGCTXDataType)3, 10437 },
	{ (Il2CppRGCTXDataType)2, 17972 },
	{ (Il2CppRGCTXDataType)3, 10438 },
	{ (Il2CppRGCTXDataType)3, 10439 },
	{ (Il2CppRGCTXDataType)2, 17973 },
	{ (Il2CppRGCTXDataType)3, 10440 },
	{ (Il2CppRGCTXDataType)2, 17974 },
	{ (Il2CppRGCTXDataType)3, 10441 },
	{ (Il2CppRGCTXDataType)3, 10442 },
	{ (Il2CppRGCTXDataType)3, 10443 },
	{ (Il2CppRGCTXDataType)2, 12827 },
	{ (Il2CppRGCTXDataType)3, 10444 },
	{ (Il2CppRGCTXDataType)2, 12835 },
	{ (Il2CppRGCTXDataType)3, 10445 },
	{ (Il2CppRGCTXDataType)2, 17975 },
	{ (Il2CppRGCTXDataType)2, 17976 },
	{ (Il2CppRGCTXDataType)3, 10446 },
	{ (Il2CppRGCTXDataType)3, 10447 },
	{ (Il2CppRGCTXDataType)3, 10448 },
	{ (Il2CppRGCTXDataType)3, 10449 },
	{ (Il2CppRGCTXDataType)3, 10450 },
	{ (Il2CppRGCTXDataType)3, 10451 },
	{ (Il2CppRGCTXDataType)2, 12851 },
	{ (Il2CppRGCTXDataType)2, 17977 },
	{ (Il2CppRGCTXDataType)3, 10452 },
	{ (Il2CppRGCTXDataType)3, 10453 },
	{ (Il2CppRGCTXDataType)2, 12855 },
	{ (Il2CppRGCTXDataType)3, 10454 },
	{ (Il2CppRGCTXDataType)2, 17978 },
	{ (Il2CppRGCTXDataType)2, 12865 },
	{ (Il2CppRGCTXDataType)2, 12863 },
	{ (Il2CppRGCTXDataType)2, 17979 },
	{ (Il2CppRGCTXDataType)3, 10455 },
	{ (Il2CppRGCTXDataType)2, 17980 },
	{ (Il2CppRGCTXDataType)3, 10456 },
	{ (Il2CppRGCTXDataType)3, 10457 },
	{ (Il2CppRGCTXDataType)2, 12872 },
	{ (Il2CppRGCTXDataType)3, 10458 },
	{ (Il2CppRGCTXDataType)2, 12872 },
	{ (Il2CppRGCTXDataType)3, 10459 },
	{ (Il2CppRGCTXDataType)2, 12889 },
	{ (Il2CppRGCTXDataType)3, 10460 },
	{ (Il2CppRGCTXDataType)3, 10461 },
	{ (Il2CppRGCTXDataType)3, 10462 },
	{ (Il2CppRGCTXDataType)2, 17981 },
	{ (Il2CppRGCTXDataType)3, 10463 },
	{ (Il2CppRGCTXDataType)3, 10464 },
	{ (Il2CppRGCTXDataType)3, 10465 },
	{ (Il2CppRGCTXDataType)2, 12869 },
	{ (Il2CppRGCTXDataType)3, 10466 },
	{ (Il2CppRGCTXDataType)3, 10467 },
	{ (Il2CppRGCTXDataType)2, 12874 },
	{ (Il2CppRGCTXDataType)3, 10468 },
	{ (Il2CppRGCTXDataType)1, 17982 },
	{ (Il2CppRGCTXDataType)2, 12873 },
	{ (Il2CppRGCTXDataType)3, 10469 },
	{ (Il2CppRGCTXDataType)1, 12873 },
	{ (Il2CppRGCTXDataType)1, 12869 },
	{ (Il2CppRGCTXDataType)2, 17981 },
	{ (Il2CppRGCTXDataType)2, 12873 },
	{ (Il2CppRGCTXDataType)2, 12871 },
	{ (Il2CppRGCTXDataType)2, 12875 },
	{ (Il2CppRGCTXDataType)3, 10470 },
	{ (Il2CppRGCTXDataType)3, 10471 },
	{ (Il2CppRGCTXDataType)3, 10472 },
	{ (Il2CppRGCTXDataType)2, 12870 },
	{ (Il2CppRGCTXDataType)3, 10473 },
	{ (Il2CppRGCTXDataType)2, 12885 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	129,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	41,
	s_rgctxIndices,
	214,
	s_rgctxValues,
	NULL,
};
