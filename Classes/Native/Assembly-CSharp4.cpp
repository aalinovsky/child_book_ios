﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.Int32,System.Int32>[]
struct EntryU5BU5D_t771BA166F0450465AFAF66A1700FD2795A4BA1F9;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,System.String>[]
struct EntryU5BU5D_tAFCF55DB73794BBB9BBAF4742B5AAB8DF612E4D0;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t07175316A062A97612A0750CD9FCFAC6C13270FA;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,System.String>
struct KeyCollection_t9164FDD900F1F17EF7DD55E327D42804C606F8DA;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t4B03F142CD9DE8CA06D9CD246A5403B64C000D60;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,System.String>
struct ValueCollection_t6228FD8A5BABAB4311D50D2F6ABE219F466C2284;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>>
struct Dictionary_2_t2E3B0F582993D037C4B100452BE83B5BD4D25CF2;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8;
// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.Boolean>
struct Dictionary_2_t9C1587ED98B6EDD835FD487595042E39B8F7F79F;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t7B82AA0F8B96BAAA21E36DDF7A1FE4348BDDBE95;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t84A1E76CEF8A66F732C15925C1E1DBC7446DB3A4;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t3D4152882C54B77C712688E910390D5C8E030463;
// System.Collections.Generic.List`1<vp_PoolManager/vp_PreloadedPrefab>
struct List_1_tA5D3206C867C9FD4C8AAA3695CA8F1E286087B02;
// System.Collections.Generic.List`1<vp_Timer/Event>
struct List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackFrame[]
struct StackFrameU5BU5D_t5075A2805A51162E94A9CF2F2881DC51B78EA80D;
// System.Diagnostics.StackTrace
struct StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodBase
struct MethodBase_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AudioClip[]
struct AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// vp_PoolManager
struct vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D;
// vp_Timer/<>c
struct U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D;
// vp_Timer/ArgCallback
struct ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63;
// vp_Timer/Callback
struct Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6;
// vp_Timer/Event
struct Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8;
// vp_Timer/Event[]
struct EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63;
// vp_Timer/Handle
struct Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381;
// vp_Utility/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E;
// vp_WaypointGizmo
struct vp_WaypointGizmo_tE9568B03FC967DCFB790A051841EB56CD5D77FBB;

IL2CPP_EXTERN_C RuntimeClass* Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9;
IL2CPP_EXTERN_C String_t* _stringLiteral134F2C5A8EFE6B2B246EADA810862AAAB6969E63;
IL2CPP_EXTERN_C String_t* _stringLiteral1615307CC4523F183E777DF67F168C86908E8007;
IL2CPP_EXTERN_C String_t* _stringLiteral21A6A12B967DDEEC093A4DBAC3953726A4F48071;
IL2CPP_EXTERN_C String_t* _stringLiteral28ED3A797DA3C48C309A4EF792147F3C56CFEC40;
IL2CPP_EXTERN_C String_t* _stringLiteral29C5345848121BE0ED79301284519245F9C7A08B;
IL2CPP_EXTERN_C String_t* _stringLiteral3E2B500817A96FA5BAECB12EAFF42205003D74E6;
IL2CPP_EXTERN_C String_t* _stringLiteral401420FC0A7AF7CF7534FB9E78DD9FF06E9D0808;
IL2CPP_EXTERN_C String_t* _stringLiteral42CD4CC09795FD30CE71C875DA59944B9099B15F;
IL2CPP_EXTERN_C String_t* _stringLiteral45932D6FA98F39C5CD3F08CD951D8DC70FC5F7DE;
IL2CPP_EXTERN_C String_t* _stringLiteral46F8AB7C0CFF9DF7CD124852E26022A6BF89E315;
IL2CPP_EXTERN_C String_t* _stringLiteral5039D155A71C0A5F7A2B2654AD49CB7EE47A8980;
IL2CPP_EXTERN_C String_t* _stringLiteral52934C29CA4A1A7572C6905BD6AB705A2C135973;
IL2CPP_EXTERN_C String_t* _stringLiteral54F1C3D7731A3EF440BD8912D713A59E97E280A5;
IL2CPP_EXTERN_C String_t* _stringLiteral63EA88A95D11C675ECBFDF34069E492F0B9B2F1B;
IL2CPP_EXTERN_C String_t* _stringLiteral681D15313A93F5907C45ABC0E6A97EE59E13FABA;
IL2CPP_EXTERN_C String_t* _stringLiteral685E80366130387CB75C055248326976D16FDF8D;
IL2CPP_EXTERN_C String_t* _stringLiteral6BF8FB135FFFA4FAE44361B8B5524F0E172C610E;
IL2CPP_EXTERN_C String_t* _stringLiteral71FAFC4E2FC1E47E234762A96B80512B6B5534C2;
IL2CPP_EXTERN_C String_t* _stringLiteral7CFE65A1B11B9CEEBE65FE095DDF9C08B4D8C723;
IL2CPP_EXTERN_C String_t* _stringLiteral8CF1783FA99F62CA581F6FE8F3CD66B0F9AB9FC3;
IL2CPP_EXTERN_C String_t* _stringLiteral9AA073A8C8CD3CD43164FAFE2AF0D0D81F5F271B;
IL2CPP_EXTERN_C String_t* _stringLiteralA0F4EA7D91495DF92BBAC2E2149DFB850FE81396;
IL2CPP_EXTERN_C String_t* _stringLiteralA4178B659666B8A8CA4F335016681A0D46EA521B;
IL2CPP_EXTERN_C String_t* _stringLiteralBD3027FA569EA15CA76D84DB21C67E2D514C1A5A;
IL2CPP_EXTERN_C String_t* _stringLiteralBDB36BB22DEB169275B3094BA9005A29EEDDD195;
IL2CPP_EXTERN_C String_t* _stringLiteralC2B40AE733FE3E1BC6CCBBFEDE88932EE0DDBBA3;
IL2CPP_EXTERN_C String_t* _stringLiteralC476FD00CCF30C5701DE9368C551434D10BB9289;
IL2CPP_EXTERN_C String_t* _stringLiteralD3BC9A378DAAA1DDDBA1B19C1AA641D3E9683C46;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A;
IL2CPP_EXTERN_C String_t* _stringLiteralE9CEDE9B80EA3ABD89C755F1117337D429162C86;
IL2CPP_EXTERN_C String_t* _stringLiteralEBABC9B6E6F06B857E70FF9361A38C584DAFDB10;
IL2CPP_EXTERN_C String_t* _stringLiteralECB252044B5EA0F679EE78EC1A12904739E2904D;
IL2CPP_EXTERN_C String_t* _stringLiteralFAFBA073700296227A7D1B4785C63F0194C967FA;
IL2CPP_EXTERN_C String_t* _stringLiteralFCDBC880C3D7454F7AED7A672C34EBC2395648A2;
IL2CPP_EXTERN_C const RuntimeMethod* Array_IndexOf_TisRuntimeObject_m40554FA47BA74C45E33C913F60628DD0E83DB370_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mDC25A2F4744B497C39F127D97327CC7B56E68903_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Clear_m7DE8E346650F09FFAB65ABCACFEBCB2FAB2F4499_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_ContainsKey_mBC44DAB119CBE7A4B7820ABF12AD024A4654471E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_mF96A70AF9B37F114A6DAC1FF67D1EB8C5072E739_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m5B4A5BC34051AAA3F4C9590CF249F3A095332161_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mDE30E1D450337479C15FB7C79523406548F2D396_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Count_m73CF0E63105DEFE34A611344CF2C01B27AF83FFB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m8662D37E19174217C357332D01C091C6944740CF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m3E8E2290A4702A1A0394D255A0CE9D5B69B6304C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m3B48315259447A236D1AC04EDC08F3A906417DFC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m4E6521E5891DBB98559C82D481FA4F4945952B46_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass18_0_U3CDestroyU3Eb__0_m325D201A9C46B0BEAA9E5AEC6E86866FCF616943_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Boolean_tB53F6830F670160873277339AA58F15CAED4399C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RuntimeObject_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* SByte_t9070AEA2966184235653CB9B4D33B149CDA831DF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* String_t_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* UInt16_tAE45CEF73BF720100519F6867F32145D075F928E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* UInt64_tA02DF3B59C8FC4A849BD207DA11038CC64E4CB4E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t Event_Destroy_m54F753C2DB0D932A829D856B16E14C11F7F18AF9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Event_Error_mB82C87213850999FBA9FF7CC1AF57504C07CADA5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Event_Execute_m2224A3F8441A1B5740F0B81AD62CF8452FACBDF6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Event_get_MethodInfo_m6D8C37E774DC62EF53B32C7200739E777903BBB3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Event_get_MethodName_m2E7322780EA4E933FB1C38D9E0863E8C719DBC2E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Handle_Cancel_mFE3C59AD52725C2AF4777A670FB61E2CA05D54EF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Handle_get_Delay_mC0587B87BD995E4254FF0376B9C6E452A7B739A9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Handle_get_MethodInfo_m9F6A97E4B626742350A556D1C05029023B8AFA3F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Handle_get_MethodName_m0AE43AEDF241A34CC0BB638AA8B92945CDE5F88F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Handle_set_CancelOnLoad_mE921CED0AF28227617B3E4F315749E4205211216_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Handle_set_Id_m035AAB0529283D2F19A200F186C5AFDAB19F666D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__DisplayClass18_0_U3CDestroyU3Eb__0_m325D201A9C46B0BEAA9E5AEC6E86866FCF616943_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CU3Ec__cctor_mE2FD9741E9B6569B6DF9BAFC826AA9FAACEB1964_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_ClearUniqueIDs_m559A9F7D03CF00423358623E578D00A918B328DC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_Destroy_m4AE89615D99B095E1DAAF80E913F5AA6B9C4539F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_Destroy_mEF5255E8C4A8FA62DC4D9C33A1FB6F48B7B22B27_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_GetErrorLocation_m6F3429DFF080043CBB6719F998CEDF3D76E631B9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_GetParent_m23DF7AA74CBF3783F4D11A3C8159BCFD32C629C3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_GetTransformByNameInAncestors_m85889B83D8D4B16EFBB1C417E7964480C825AEE4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_GetTransformByNameInChildren_m4BAEFE6AB1211944BBC646B73A3C319EA810BE8A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_GetTypeAlias_m0B105E1F6666B82F789B1ED67B46ED98EED4832B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_Instantiate_m40BDAB73E95DD2EA41F4C96D424CFE9E08B4E30E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_Instantiate_mB3FC1BB39D06ACF32E0409CAA385DDB0915B65E7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_IsDescendant_m2383C0A44C9B19957CF9C867D00DF32E10CEEDAD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_PositionToID_m872F990B5C216FA657F53092D380CE04F341467A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility__cctor_m9B1303501DD378DBFA457D8B8A5C3F0F3AE0E5B5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_Utility_get_UniqueID_mAE63167533A8BA058CFD0BFE90DF746219A3384F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_WaypointGizmo_OnDrawGizmosSelected_m5424ACE12543A130DF01A80B4079580B9C37A740_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t vp_WaypointGizmo_OnDrawGizmos_m9CF12CE032519F0999581DCA4C6755F0ACA65028_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct  Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t771BA166F0450465AFAF66A1700FD2795A4BA1F9* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t07175316A062A97612A0750CD9FCFAC6C13270FA * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t4B03F142CD9DE8CA06D9CD246A5403B64C000D60 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___entries_1)); }
	inline EntryU5BU5D_t771BA166F0450465AFAF66A1700FD2795A4BA1F9* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t771BA166F0450465AFAF66A1700FD2795A4BA1F9** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t771BA166F0450465AFAF66A1700FD2795A4BA1F9* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___keys_7)); }
	inline KeyCollection_t07175316A062A97612A0750CD9FCFAC6C13270FA * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t07175316A062A97612A0750CD9FCFAC6C13270FA ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t07175316A062A97612A0750CD9FCFAC6C13270FA * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ___values_8)); }
	inline ValueCollection_t4B03F142CD9DE8CA06D9CD246A5403B64C000D60 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t4B03F142CD9DE8CA06D9CD246A5403B64C000D60 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t4B03F142CD9DE8CA06D9CD246A5403B64C000D60 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct  Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tAFCF55DB73794BBB9BBAF4742B5AAB8DF612E4D0* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t9164FDD900F1F17EF7DD55E327D42804C606F8DA * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t6228FD8A5BABAB4311D50D2F6ABE219F466C2284 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___entries_1)); }
	inline EntryU5BU5D_tAFCF55DB73794BBB9BBAF4742B5AAB8DF612E4D0* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tAFCF55DB73794BBB9BBAF4742B5AAB8DF612E4D0** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tAFCF55DB73794BBB9BBAF4742B5AAB8DF612E4D0* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___keys_7)); }
	inline KeyCollection_t9164FDD900F1F17EF7DD55E327D42804C606F8DA * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t9164FDD900F1F17EF7DD55E327D42804C606F8DA ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t9164FDD900F1F17EF7DD55E327D42804C606F8DA * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ___values_8)); }
	inline ValueCollection_t6228FD8A5BABAB4311D50D2F6ABE219F466C2284 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t6228FD8A5BABAB4311D50D2F6ABE219F466C2284 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t6228FD8A5BABAB4311D50D2F6ABE219F466C2284 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.AudioClip>
struct  List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1, ____items_1)); }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* get__items_1() const { return ____items_1; }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1_StaticFields, ____emptyArray_5)); }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* get__emptyArray_5() const { return ____emptyArray_5; }
	inline AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(AudioClipU5BU5D_t03931BD44BC339329210676E452B8ECD3EC171C2* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<vp_Timer_Event>
struct  List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E, ____items_1)); }
	inline EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63* get__items_1() const { return ____items_1; }
	inline EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E_StaticFields, ____emptyArray_5)); }
	inline EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63* get__emptyArray_5() const { return ____emptyArray_5; }
	inline EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(EventU5BU5D_tC9CA930139CCCE1C96AC31DEE8FDC67B2A034A63* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Diagnostics.StackFrame
struct  StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00  : public RuntimeObject
{
public:
	// System.Int32 System.Diagnostics.StackFrame::ilOffset
	int32_t ___ilOffset_1;
	// System.Int32 System.Diagnostics.StackFrame::nativeOffset
	int32_t ___nativeOffset_2;
	// System.Int64 System.Diagnostics.StackFrame::methodAddress
	int64_t ___methodAddress_3;
	// System.UInt32 System.Diagnostics.StackFrame::methodIndex
	uint32_t ___methodIndex_4;
	// System.Reflection.MethodBase System.Diagnostics.StackFrame::methodBase
	MethodBase_t * ___methodBase_5;
	// System.String System.Diagnostics.StackFrame::fileName
	String_t* ___fileName_6;
	// System.Int32 System.Diagnostics.StackFrame::lineNumber
	int32_t ___lineNumber_7;
	// System.Int32 System.Diagnostics.StackFrame::columnNumber
	int32_t ___columnNumber_8;
	// System.String System.Diagnostics.StackFrame::internalMethodName
	String_t* ___internalMethodName_9;

public:
	inline static int32_t get_offset_of_ilOffset_1() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___ilOffset_1)); }
	inline int32_t get_ilOffset_1() const { return ___ilOffset_1; }
	inline int32_t* get_address_of_ilOffset_1() { return &___ilOffset_1; }
	inline void set_ilOffset_1(int32_t value)
	{
		___ilOffset_1 = value;
	}

	inline static int32_t get_offset_of_nativeOffset_2() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___nativeOffset_2)); }
	inline int32_t get_nativeOffset_2() const { return ___nativeOffset_2; }
	inline int32_t* get_address_of_nativeOffset_2() { return &___nativeOffset_2; }
	inline void set_nativeOffset_2(int32_t value)
	{
		___nativeOffset_2 = value;
	}

	inline static int32_t get_offset_of_methodAddress_3() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___methodAddress_3)); }
	inline int64_t get_methodAddress_3() const { return ___methodAddress_3; }
	inline int64_t* get_address_of_methodAddress_3() { return &___methodAddress_3; }
	inline void set_methodAddress_3(int64_t value)
	{
		___methodAddress_3 = value;
	}

	inline static int32_t get_offset_of_methodIndex_4() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___methodIndex_4)); }
	inline uint32_t get_methodIndex_4() const { return ___methodIndex_4; }
	inline uint32_t* get_address_of_methodIndex_4() { return &___methodIndex_4; }
	inline void set_methodIndex_4(uint32_t value)
	{
		___methodIndex_4 = value;
	}

	inline static int32_t get_offset_of_methodBase_5() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___methodBase_5)); }
	inline MethodBase_t * get_methodBase_5() const { return ___methodBase_5; }
	inline MethodBase_t ** get_address_of_methodBase_5() { return &___methodBase_5; }
	inline void set_methodBase_5(MethodBase_t * value)
	{
		___methodBase_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___methodBase_5), (void*)value);
	}

	inline static int32_t get_offset_of_fileName_6() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___fileName_6)); }
	inline String_t* get_fileName_6() const { return ___fileName_6; }
	inline String_t** get_address_of_fileName_6() { return &___fileName_6; }
	inline void set_fileName_6(String_t* value)
	{
		___fileName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fileName_6), (void*)value);
	}

	inline static int32_t get_offset_of_lineNumber_7() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___lineNumber_7)); }
	inline int32_t get_lineNumber_7() const { return ___lineNumber_7; }
	inline int32_t* get_address_of_lineNumber_7() { return &___lineNumber_7; }
	inline void set_lineNumber_7(int32_t value)
	{
		___lineNumber_7 = value;
	}

	inline static int32_t get_offset_of_columnNumber_8() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___columnNumber_8)); }
	inline int32_t get_columnNumber_8() const { return ___columnNumber_8; }
	inline int32_t* get_address_of_columnNumber_8() { return &___columnNumber_8; }
	inline void set_columnNumber_8(int32_t value)
	{
		___columnNumber_8 = value;
	}

	inline static int32_t get_offset_of_internalMethodName_9() { return static_cast<int32_t>(offsetof(StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00, ___internalMethodName_9)); }
	inline String_t* get_internalMethodName_9() const { return ___internalMethodName_9; }
	inline String_t** get_address_of_internalMethodName_9() { return &___internalMethodName_9; }
	inline void set_internalMethodName_9(String_t* value)
	{
		___internalMethodName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___internalMethodName_9), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Diagnostics.StackFrame
struct StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00_marshaled_pinvoke
{
	int32_t ___ilOffset_1;
	int32_t ___nativeOffset_2;
	int64_t ___methodAddress_3;
	uint32_t ___methodIndex_4;
	MethodBase_t * ___methodBase_5;
	char* ___fileName_6;
	int32_t ___lineNumber_7;
	int32_t ___columnNumber_8;
	char* ___internalMethodName_9;
};
// Native definition for COM marshalling of System.Diagnostics.StackFrame
struct StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00_marshaled_com
{
	int32_t ___ilOffset_1;
	int32_t ___nativeOffset_2;
	int64_t ___methodAddress_3;
	uint32_t ___methodIndex_4;
	MethodBase_t * ___methodBase_5;
	Il2CppChar* ___fileName_6;
	int32_t ___lineNumber_7;
	int32_t ___columnNumber_8;
	Il2CppChar* ___internalMethodName_9;
};

// System.Diagnostics.StackTrace
struct  StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99  : public RuntimeObject
{
public:
	// System.Diagnostics.StackFrame[] System.Diagnostics.StackTrace::frames
	StackFrameU5BU5D_t5075A2805A51162E94A9CF2F2881DC51B78EA80D* ___frames_1;
	// System.Diagnostics.StackTrace[] System.Diagnostics.StackTrace::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_2;
	// System.Boolean System.Diagnostics.StackTrace::debug_info
	bool ___debug_info_3;

public:
	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99, ___frames_1)); }
	inline StackFrameU5BU5D_t5075A2805A51162E94A9CF2F2881DC51B78EA80D* get_frames_1() const { return ___frames_1; }
	inline StackFrameU5BU5D_t5075A2805A51162E94A9CF2F2881DC51B78EA80D** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(StackFrameU5BU5D_t5075A2805A51162E94A9CF2F2881DC51B78EA80D* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frames_1), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_2() { return static_cast<int32_t>(offsetof(StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99, ___captured_traces_2)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_2() const { return ___captured_traces_2; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_2() { return &___captured_traces_2; }
	inline void set_captured_traces_2(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_2), (void*)value);
	}

	inline static int32_t get_offset_of_debug_info_3() { return static_cast<int32_t>(offsetof(StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99, ___debug_info_3)); }
	inline bool get_debug_info_3() const { return ___debug_info_3; }
	inline bool* get_address_of_debug_info_3() { return &___debug_info_3; }
	inline void set_debug_info_3(bool value)
	{
		___debug_info_3 = value;
	}
};

struct StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99_StaticFields
{
public:
	// System.Boolean System.Diagnostics.StackTrace::isAotidSet
	bool ___isAotidSet_4;
	// System.String System.Diagnostics.StackTrace::aotid
	String_t* ___aotid_5;

public:
	inline static int32_t get_offset_of_isAotidSet_4() { return static_cast<int32_t>(offsetof(StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99_StaticFields, ___isAotidSet_4)); }
	inline bool get_isAotidSet_4() const { return ___isAotidSet_4; }
	inline bool* get_address_of_isAotidSet_4() { return &___isAotidSet_4; }
	inline void set_isAotidSet_4(bool value)
	{
		___isAotidSet_4 = value;
	}

	inline static int32_t get_offset_of_aotid_5() { return static_cast<int32_t>(offsetof(StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99_StaticFields, ___aotid_5)); }
	inline String_t* get_aotid_5() const { return ___aotid_5; }
	inline String_t** get_address_of_aotid_5() { return &___aotid_5; }
	inline void set_aotid_5(String_t* value)
	{
		___aotid_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aotid_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// vp_Timer_<>c
struct  U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D_StaticFields
{
public:
	// vp_Timer_<>c vp_Timer_<>c::<>9
	U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D * ___U3CU3E9_0;
	// vp_Timer_Callback vp_Timer_<>c::<>9__24_0
	Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * ___U3CU3E9__24_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__24_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D_StaticFields, ___U3CU3E9__24_0_1)); }
	inline Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * get_U3CU3E9__24_0_1() const { return ___U3CU3E9__24_0_1; }
	inline Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 ** get_address_of_U3CU3E9__24_0_1() { return &___U3CU3E9__24_0_1; }
	inline void set_U3CU3E9__24_0_1(Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * value)
	{
		___U3CU3E9__24_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__24_0_1), (void*)value);
	}
};


// vp_Timer_Event
struct  Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8  : public RuntimeObject
{
public:
	// System.Int32 vp_Timer_Event::Id
	int32_t ___Id_0;
	// vp_Timer_Callback vp_Timer_Event::Function
	Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * ___Function_1;
	// vp_Timer_ArgCallback vp_Timer_Event::ArgFunction
	ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * ___ArgFunction_2;
	// System.Object vp_Timer_Event::Arguments
	RuntimeObject * ___Arguments_3;
	// System.Int32 vp_Timer_Event::Iterations
	int32_t ___Iterations_4;
	// System.Single vp_Timer_Event::Interval
	float ___Interval_5;
	// System.Single vp_Timer_Event::DueTime
	float ___DueTime_6;
	// System.Single vp_Timer_Event::StartTime
	float ___StartTime_7;
	// System.Single vp_Timer_Event::LifeTime
	float ___LifeTime_8;
	// System.Boolean vp_Timer_Event::Paused
	bool ___Paused_9;
	// System.Boolean vp_Timer_Event::CancelOnLoad
	bool ___CancelOnLoad_10;

public:
	inline static int32_t get_offset_of_Id_0() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___Id_0)); }
	inline int32_t get_Id_0() const { return ___Id_0; }
	inline int32_t* get_address_of_Id_0() { return &___Id_0; }
	inline void set_Id_0(int32_t value)
	{
		___Id_0 = value;
	}

	inline static int32_t get_offset_of_Function_1() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___Function_1)); }
	inline Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * get_Function_1() const { return ___Function_1; }
	inline Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 ** get_address_of_Function_1() { return &___Function_1; }
	inline void set_Function_1(Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * value)
	{
		___Function_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Function_1), (void*)value);
	}

	inline static int32_t get_offset_of_ArgFunction_2() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___ArgFunction_2)); }
	inline ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * get_ArgFunction_2() const { return ___ArgFunction_2; }
	inline ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 ** get_address_of_ArgFunction_2() { return &___ArgFunction_2; }
	inline void set_ArgFunction_2(ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * value)
	{
		___ArgFunction_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ArgFunction_2), (void*)value);
	}

	inline static int32_t get_offset_of_Arguments_3() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___Arguments_3)); }
	inline RuntimeObject * get_Arguments_3() const { return ___Arguments_3; }
	inline RuntimeObject ** get_address_of_Arguments_3() { return &___Arguments_3; }
	inline void set_Arguments_3(RuntimeObject * value)
	{
		___Arguments_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Arguments_3), (void*)value);
	}

	inline static int32_t get_offset_of_Iterations_4() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___Iterations_4)); }
	inline int32_t get_Iterations_4() const { return ___Iterations_4; }
	inline int32_t* get_address_of_Iterations_4() { return &___Iterations_4; }
	inline void set_Iterations_4(int32_t value)
	{
		___Iterations_4 = value;
	}

	inline static int32_t get_offset_of_Interval_5() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___Interval_5)); }
	inline float get_Interval_5() const { return ___Interval_5; }
	inline float* get_address_of_Interval_5() { return &___Interval_5; }
	inline void set_Interval_5(float value)
	{
		___Interval_5 = value;
	}

	inline static int32_t get_offset_of_DueTime_6() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___DueTime_6)); }
	inline float get_DueTime_6() const { return ___DueTime_6; }
	inline float* get_address_of_DueTime_6() { return &___DueTime_6; }
	inline void set_DueTime_6(float value)
	{
		___DueTime_6 = value;
	}

	inline static int32_t get_offset_of_StartTime_7() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___StartTime_7)); }
	inline float get_StartTime_7() const { return ___StartTime_7; }
	inline float* get_address_of_StartTime_7() { return &___StartTime_7; }
	inline void set_StartTime_7(float value)
	{
		___StartTime_7 = value;
	}

	inline static int32_t get_offset_of_LifeTime_8() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___LifeTime_8)); }
	inline float get_LifeTime_8() const { return ___LifeTime_8; }
	inline float* get_address_of_LifeTime_8() { return &___LifeTime_8; }
	inline void set_LifeTime_8(float value)
	{
		___LifeTime_8 = value;
	}

	inline static int32_t get_offset_of_Paused_9() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___Paused_9)); }
	inline bool get_Paused_9() const { return ___Paused_9; }
	inline bool* get_address_of_Paused_9() { return &___Paused_9; }
	inline void set_Paused_9(bool value)
	{
		___Paused_9 = value;
	}

	inline static int32_t get_offset_of_CancelOnLoad_10() { return static_cast<int32_t>(offsetof(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8, ___CancelOnLoad_10)); }
	inline bool get_CancelOnLoad_10() const { return ___CancelOnLoad_10; }
	inline bool* get_address_of_CancelOnLoad_10() { return &___CancelOnLoad_10; }
	inline void set_CancelOnLoad_10(bool value)
	{
		___CancelOnLoad_10 = value;
	}
};


// vp_Timer_Handle
struct  Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381  : public RuntimeObject
{
public:
	// vp_Timer_Event vp_Timer_Handle::m_Event
	Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * ___m_Event_0;
	// System.Int32 vp_Timer_Handle::m_Id
	int32_t ___m_Id_1;
	// System.Int32 vp_Timer_Handle::m_StartIterations
	int32_t ___m_StartIterations_2;
	// System.Single vp_Timer_Handle::m_FirstDueTime
	float ___m_FirstDueTime_3;

public:
	inline static int32_t get_offset_of_m_Event_0() { return static_cast<int32_t>(offsetof(Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381, ___m_Event_0)); }
	inline Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * get_m_Event_0() const { return ___m_Event_0; }
	inline Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 ** get_address_of_m_Event_0() { return &___m_Event_0; }
	inline void set_m_Event_0(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * value)
	{
		___m_Event_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Event_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Id_1() { return static_cast<int32_t>(offsetof(Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381, ___m_Id_1)); }
	inline int32_t get_m_Id_1() const { return ___m_Id_1; }
	inline int32_t* get_address_of_m_Id_1() { return &___m_Id_1; }
	inline void set_m_Id_1(int32_t value)
	{
		___m_Id_1 = value;
	}

	inline static int32_t get_offset_of_m_StartIterations_2() { return static_cast<int32_t>(offsetof(Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381, ___m_StartIterations_2)); }
	inline int32_t get_m_StartIterations_2() const { return ___m_StartIterations_2; }
	inline int32_t* get_address_of_m_StartIterations_2() { return &___m_StartIterations_2; }
	inline void set_m_StartIterations_2(int32_t value)
	{
		___m_StartIterations_2 = value;
	}

	inline static int32_t get_offset_of_m_FirstDueTime_3() { return static_cast<int32_t>(offsetof(Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381, ___m_FirstDueTime_3)); }
	inline float get_m_FirstDueTime_3() const { return ___m_FirstDueTime_3; }
	inline float* get_address_of_m_FirstDueTime_3() { return &___m_FirstDueTime_3; }
	inline void set_m_FirstDueTime_3(float value)
	{
		___m_FirstDueTime_3 = value;
	}
};


// vp_Utility
struct  vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0  : public RuntimeObject
{
public:

public:
};

struct vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> vp_Utility::m_TypeAliases
	Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * ___m_TypeAliases_0;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> vp_Utility::m_UniqueIDs
	Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * ___m_UniqueIDs_1;

public:
	inline static int32_t get_offset_of_m_TypeAliases_0() { return static_cast<int32_t>(offsetof(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields, ___m_TypeAliases_0)); }
	inline Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * get_m_TypeAliases_0() const { return ___m_TypeAliases_0; }
	inline Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 ** get_address_of_m_TypeAliases_0() { return &___m_TypeAliases_0; }
	inline void set_m_TypeAliases_0(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * value)
	{
		___m_TypeAliases_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TypeAliases_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_UniqueIDs_1() { return static_cast<int32_t>(offsetof(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields, ___m_UniqueIDs_1)); }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * get_m_UniqueIDs_1() const { return ___m_UniqueIDs_1; }
	inline Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 ** get_address_of_m_UniqueIDs_1() { return &___m_UniqueIDs_1; }
	inline void set_m_UniqueIDs_1(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * value)
	{
		___m_UniqueIDs_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UniqueIDs_1), (void*)value);
	}
};


// vp_Utility_<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E  : public RuntimeObject
{
public:
	// UnityEngine.Object vp_Utility_<>c__DisplayClass18_0::obj
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj_0;
	// System.Single vp_Utility_<>c__DisplayClass18_0::t
	float ___t_1;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E, ___obj_0)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_obj_0() const { return ___obj_0; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___obj_0), (void*)value);
	}

	inline static int32_t get_offset_of_t_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E, ___t_1)); }
	inline float get_t_1() const { return ___t_1; }
	inline float* get_address_of_t_1() { return &___t_1; }
	inline void set_t_1(float value)
	{
		___t_1 = value;
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Char
struct  Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Reflection.MethodBase
struct  MethodBase_t  : public MemberInfo_t
{
public:

public:
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct  Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// UnityEngine.Matrix4x4
struct  Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// vp_Timer_Stats
struct  Stats_tF6AE11155E24051C0AA723C27DF52D63C16306FF 
{
public:
	// System.Int32 vp_Timer_Stats::Created
	int32_t ___Created_0;
	// System.Int32 vp_Timer_Stats::Inactive
	int32_t ___Inactive_1;
	// System.Int32 vp_Timer_Stats::Active
	int32_t ___Active_2;

public:
	inline static int32_t get_offset_of_Created_0() { return static_cast<int32_t>(offsetof(Stats_tF6AE11155E24051C0AA723C27DF52D63C16306FF, ___Created_0)); }
	inline int32_t get_Created_0() const { return ___Created_0; }
	inline int32_t* get_address_of_Created_0() { return &___Created_0; }
	inline void set_Created_0(int32_t value)
	{
		___Created_0 = value;
	}

	inline static int32_t get_offset_of_Inactive_1() { return static_cast<int32_t>(offsetof(Stats_tF6AE11155E24051C0AA723C27DF52D63C16306FF, ___Inactive_1)); }
	inline int32_t get_Inactive_1() const { return ___Inactive_1; }
	inline int32_t* get_address_of_Inactive_1() { return &___Inactive_1; }
	inline void set_Inactive_1(int32_t value)
	{
		___Inactive_1 = value;
	}

	inline static int32_t get_offset_of_Active_2() { return static_cast<int32_t>(offsetof(Stats_tF6AE11155E24051C0AA723C27DF52D63C16306FF, ___Active_2)); }
	inline int32_t get_Active_2() const { return ___Active_2; }
	inline int32_t* get_address_of_Active_2() { return &___Active_2; }
	inline void set_Active_2(int32_t value)
	{
		___Active_2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t
{
public:

public:
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.CursorLockMode
struct  CursorLockMode_tF9B28266D253124BE56C232B7ED2D9F7CC3D1E38 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorLockMode_tF9B28266D253124BE56C232B7ED2D9F7CC3D1E38, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Sprite
struct  Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct  Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Texture2D
struct  Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// vp_Timer_ArgCallback
struct  ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63  : public MulticastDelegate_t
{
public:

public:
};


// vp_Timer_Callback
struct  Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.AudioBehaviour
struct  AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.AudioSource
struct  AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C  : public AudioBehaviour_tC612EC4E17A648A5C568621F3FBF1DBD773C71C7
{
public:

public:
};


// vp_PoolManager
struct  vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<vp_PoolManager_vp_PreloadedPrefab> vp_PoolManager::PreloadedPrefabs
	List_1_tA5D3206C867C9FD4C8AAA3695CA8F1E286087B02 * ___PreloadedPrefabs_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> vp_PoolManager::IgnoredPrefabs
	List_1_t3D4152882C54B77C712688E910390D5C8E030463 * ___IgnoredPrefabs_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.GameObject,System.Boolean> vp_PoolManager::m_IgnoredPrefabsInternal
	Dictionary_2_t9C1587ED98B6EDD835FD487595042E39B8F7F79F * ___m_IgnoredPrefabsInternal_6;
	// UnityEngine.Transform vp_PoolManager::m_Transform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_Transform_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>> vp_PoolManager::m_AvailableObjects
	Dictionary_2_t2E3B0F582993D037C4B100452BE83B5BD4D25CF2 * ___m_AvailableObjects_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> vp_PoolManager::m_UniquePrefabNames
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___m_UniquePrefabNames_9;

public:
	inline static int32_t get_offset_of_PreloadedPrefabs_4() { return static_cast<int32_t>(offsetof(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D, ___PreloadedPrefabs_4)); }
	inline List_1_tA5D3206C867C9FD4C8AAA3695CA8F1E286087B02 * get_PreloadedPrefabs_4() const { return ___PreloadedPrefabs_4; }
	inline List_1_tA5D3206C867C9FD4C8AAA3695CA8F1E286087B02 ** get_address_of_PreloadedPrefabs_4() { return &___PreloadedPrefabs_4; }
	inline void set_PreloadedPrefabs_4(List_1_tA5D3206C867C9FD4C8AAA3695CA8F1E286087B02 * value)
	{
		___PreloadedPrefabs_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PreloadedPrefabs_4), (void*)value);
	}

	inline static int32_t get_offset_of_IgnoredPrefabs_5() { return static_cast<int32_t>(offsetof(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D, ___IgnoredPrefabs_5)); }
	inline List_1_t3D4152882C54B77C712688E910390D5C8E030463 * get_IgnoredPrefabs_5() const { return ___IgnoredPrefabs_5; }
	inline List_1_t3D4152882C54B77C712688E910390D5C8E030463 ** get_address_of_IgnoredPrefabs_5() { return &___IgnoredPrefabs_5; }
	inline void set_IgnoredPrefabs_5(List_1_t3D4152882C54B77C712688E910390D5C8E030463 * value)
	{
		___IgnoredPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___IgnoredPrefabs_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_IgnoredPrefabsInternal_6() { return static_cast<int32_t>(offsetof(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D, ___m_IgnoredPrefabsInternal_6)); }
	inline Dictionary_2_t9C1587ED98B6EDD835FD487595042E39B8F7F79F * get_m_IgnoredPrefabsInternal_6() const { return ___m_IgnoredPrefabsInternal_6; }
	inline Dictionary_2_t9C1587ED98B6EDD835FD487595042E39B8F7F79F ** get_address_of_m_IgnoredPrefabsInternal_6() { return &___m_IgnoredPrefabsInternal_6; }
	inline void set_m_IgnoredPrefabsInternal_6(Dictionary_2_t9C1587ED98B6EDD835FD487595042E39B8F7F79F * value)
	{
		___m_IgnoredPrefabsInternal_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_IgnoredPrefabsInternal_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Transform_7() { return static_cast<int32_t>(offsetof(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D, ___m_Transform_7)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_Transform_7() const { return ___m_Transform_7; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_Transform_7() { return &___m_Transform_7; }
	inline void set_m_Transform_7(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_Transform_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Transform_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_AvailableObjects_8() { return static_cast<int32_t>(offsetof(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D, ___m_AvailableObjects_8)); }
	inline Dictionary_2_t2E3B0F582993D037C4B100452BE83B5BD4D25CF2 * get_m_AvailableObjects_8() const { return ___m_AvailableObjects_8; }
	inline Dictionary_2_t2E3B0F582993D037C4B100452BE83B5BD4D25CF2 ** get_address_of_m_AvailableObjects_8() { return &___m_AvailableObjects_8; }
	inline void set_m_AvailableObjects_8(Dictionary_2_t2E3B0F582993D037C4B100452BE83B5BD4D25CF2 * value)
	{
		___m_AvailableObjects_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AvailableObjects_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_UniquePrefabNames_9() { return static_cast<int32_t>(offsetof(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D, ___m_UniquePrefabNames_9)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_m_UniquePrefabNames_9() const { return ___m_UniquePrefabNames_9; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_m_UniquePrefabNames_9() { return &___m_UniquePrefabNames_9; }
	inline void set_m_UniquePrefabNames_9(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___m_UniquePrefabNames_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UniquePrefabNames_9), (void*)value);
	}
};

struct vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_StaticFields
{
public:
	// vp_PoolManager vp_PoolManager::m_Instance
	vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * ___m_Instance_10;

public:
	inline static int32_t get_offset_of_m_Instance_10() { return static_cast<int32_t>(offsetof(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_StaticFields, ___m_Instance_10)); }
	inline vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * get_m_Instance_10() const { return ___m_Instance_10; }
	inline vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D ** get_address_of_m_Instance_10() { return &___m_Instance_10; }
	inline void set_m_Instance_10(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * value)
	{
		___m_Instance_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Instance_10), (void*)value);
	}
};


// vp_Timer
struct  vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields
{
public:
	// UnityEngine.GameObject vp_Timer::m_MainObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___m_MainObject_4;
	// System.Collections.Generic.List`1<vp_Timer_Event> vp_Timer::m_Active
	List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * ___m_Active_5;
	// System.Collections.Generic.List`1<vp_Timer_Event> vp_Timer::m_Pool
	List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * ___m_Pool_6;
	// vp_Timer_Event vp_Timer::m_NewEvent
	Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * ___m_NewEvent_7;
	// System.Int32 vp_Timer::m_EventCount
	int32_t ___m_EventCount_8;
	// System.Int32 vp_Timer::m_EventBatch
	int32_t ___m_EventBatch_9;
	// System.Int32 vp_Timer::m_EventIterator
	int32_t ___m_EventIterator_10;
	// System.Boolean vp_Timer::m_AppQuitting
	bool ___m_AppQuitting_11;
	// System.Int32 vp_Timer::MaxEventsPerFrame
	int32_t ___MaxEventsPerFrame_12;

public:
	inline static int32_t get_offset_of_m_MainObject_4() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___m_MainObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_m_MainObject_4() const { return ___m_MainObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_m_MainObject_4() { return &___m_MainObject_4; }
	inline void set_m_MainObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___m_MainObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MainObject_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Active_5() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___m_Active_5)); }
	inline List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * get_m_Active_5() const { return ___m_Active_5; }
	inline List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E ** get_address_of_m_Active_5() { return &___m_Active_5; }
	inline void set_m_Active_5(List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * value)
	{
		___m_Active_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Active_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Pool_6() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___m_Pool_6)); }
	inline List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * get_m_Pool_6() const { return ___m_Pool_6; }
	inline List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E ** get_address_of_m_Pool_6() { return &___m_Pool_6; }
	inline void set_m_Pool_6(List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * value)
	{
		___m_Pool_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Pool_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_NewEvent_7() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___m_NewEvent_7)); }
	inline Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * get_m_NewEvent_7() const { return ___m_NewEvent_7; }
	inline Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 ** get_address_of_m_NewEvent_7() { return &___m_NewEvent_7; }
	inline void set_m_NewEvent_7(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * value)
	{
		___m_NewEvent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_NewEvent_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_EventCount_8() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___m_EventCount_8)); }
	inline int32_t get_m_EventCount_8() const { return ___m_EventCount_8; }
	inline int32_t* get_address_of_m_EventCount_8() { return &___m_EventCount_8; }
	inline void set_m_EventCount_8(int32_t value)
	{
		___m_EventCount_8 = value;
	}

	inline static int32_t get_offset_of_m_EventBatch_9() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___m_EventBatch_9)); }
	inline int32_t get_m_EventBatch_9() const { return ___m_EventBatch_9; }
	inline int32_t* get_address_of_m_EventBatch_9() { return &___m_EventBatch_9; }
	inline void set_m_EventBatch_9(int32_t value)
	{
		___m_EventBatch_9 = value;
	}

	inline static int32_t get_offset_of_m_EventIterator_10() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___m_EventIterator_10)); }
	inline int32_t get_m_EventIterator_10() const { return ___m_EventIterator_10; }
	inline int32_t* get_address_of_m_EventIterator_10() { return &___m_EventIterator_10; }
	inline void set_m_EventIterator_10(int32_t value)
	{
		___m_EventIterator_10 = value;
	}

	inline static int32_t get_offset_of_m_AppQuitting_11() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___m_AppQuitting_11)); }
	inline bool get_m_AppQuitting_11() const { return ___m_AppQuitting_11; }
	inline bool* get_address_of_m_AppQuitting_11() { return &___m_AppQuitting_11; }
	inline void set_m_AppQuitting_11(bool value)
	{
		___m_AppQuitting_11 = value;
	}

	inline static int32_t get_offset_of_MaxEventsPerFrame_12() { return static_cast<int32_t>(offsetof(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields, ___MaxEventsPerFrame_12)); }
	inline int32_t get_MaxEventsPerFrame_12() const { return ___MaxEventsPerFrame_12; }
	inline int32_t* get_address_of_MaxEventsPerFrame_12() { return &___MaxEventsPerFrame_12; }
	inline void set_MaxEventsPerFrame_12(int32_t value)
	{
		___MaxEventsPerFrame_12 = value;
	}
};


// vp_WaypointGizmo
struct  vp_WaypointGizmo_tE9568B03FC967DCFB790A051841EB56CD5D77FBB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Color vp_WaypointGizmo::m_GizmoColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_GizmoColor_4;
	// UnityEngine.Color vp_WaypointGizmo::m_SelectedGizmoColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_SelectedGizmoColor_5;

public:
	inline static int32_t get_offset_of_m_GizmoColor_4() { return static_cast<int32_t>(offsetof(vp_WaypointGizmo_tE9568B03FC967DCFB790A051841EB56CD5D77FBB, ___m_GizmoColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_GizmoColor_4() const { return ___m_GizmoColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_GizmoColor_4() { return &___m_GizmoColor_4; }
	inline void set_m_GizmoColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_GizmoColor_4 = value;
	}

	inline static int32_t get_offset_of_m_SelectedGizmoColor_5() { return static_cast<int32_t>(offsetof(vp_WaypointGizmo_tE9568B03FC967DCFB790A051841EB56CD5D77FBB, ___m_SelectedGizmoColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_SelectedGizmoColor_5() const { return ___m_SelectedGizmoColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_SelectedGizmoColor_5() { return &___m_SelectedGizmoColor_5; }
	inline void set_m_SelectedGizmoColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_SelectedGizmoColor_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Array_IndexOf_TisRuntimeObject_m40554FA47BA74C45E33C913F60628DD0E83DB370_gshared (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___array0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m3455807C552312C60038DF52EF328C3687442DE3_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_ContainsKey_mBC44DAB119CBE7A4B7820ABF12AD024A4654471E_gshared (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Count_m73CF0E63105DEFE34A611344CF2C01B27AF83FFB_gshared (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_mDC25A2F4744B497C39F127D97327CC7B56E68903_gshared (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Clear_m7DE8E346650F09FFAB65ABCACFEBCB2FAB2F4499_gshared (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C7E51568033239B506E15E7804A0B8658246498_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_mC741BBB0A647C814227953DB9B23CB1BDF571C5B_gshared (Dictionary_2_t32F25F093828AA9F93CB11C2A2B4648FD62A09BA * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B4A5BC34051AAA3F4C9590CF249F3A095332161_gshared (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, const RuntimeMethod* method);

// System.Void vp_Timer/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mF1EA0DF766594CC8579CF02CAF353208E57AD94E (U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void vp_Timer/Event::Recycle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method);
// System.Void vp_Timer/Callback::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Callback_Invoke_m3A5AA72A5D0FB5CCFB9DF56B2B5E4A4627804E9A (Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * __this, const RuntimeMethod* method);
// System.Void vp_Timer/ArgCallback::Invoke(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgCallback_Invoke_mFC4A16689B521E5415EB72EA62090F7AFF7E19C0 (ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * __this, RuntimeObject * ___args0, const RuntimeMethod* method);
// System.Void vp_Timer/Event::Error(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Event_Error_mB82C87213850999FBA9FF7CC1AF57504C07CADA5 (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8 (const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<vp_Timer/Event>::Remove(!0)
inline bool List_1_Remove_m3E8E2290A4702A1A0394D255A0CE9D5B69B6304C (List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * __this, Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E *, Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 *, const RuntimeMethod*))List_1_Remove_m908B647BB9F807676DACE34E3E73475C3C3751D4_gshared)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<vp_Timer/Event>::Add(!0)
inline void List_1_Add_m8662D37E19174217C357332D01C091C6944740CF (List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * __this, Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E *, Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 *, const RuntimeMethod*))List_1_Add_m6930161974C7504C80F52EC379EF012387D43138_gshared)(__this, ___item0, method);
}
// System.String System.String::Concat(System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07 (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___args0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Reflection.MethodInfo System.Delegate::get_Method()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MethodInfo_t * Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048 (Delegate_t * __this, const RuntimeMethod* method);
// System.Boolean System.Reflection.MethodInfo::op_Inequality(System.Reflection.MethodInfo,System.Reflection.MethodInfo)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237 (MethodInfo_t * ___left0, MethodInfo_t * ___right1, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.String vp_Timer/Event::get_MethodName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Event_get_MethodName_m2E7322780EA4E933FB1C38D9E0863E8C719DBC2E (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229 (String_t* ___value0, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean System.Type::get_IsArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Type_get_IsArray_m0B4E20F93B1B34C0B5C4B089F543D1AA338DC9FE (Type_t * __this, const RuntimeMethod* method);
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
inline int32_t Array_IndexOf_TisRuntimeObject_m40554FA47BA74C45E33C913F60628DD0E83DB370 (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___array0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, RuntimeObject *, const RuntimeMethod*))Array_IndexOf_TisRuntimeObject_m40554FA47BA74C45E33C913F60628DD0E83DB370_gshared)(___array0, ___value1, method);
}
// System.String System.String::Concat(System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495 (RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);
// System.Boolean vp_Timer/Handle::get_Active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method);
// System.Single vp_Timer/Handle::get_DurationLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_DurationLeft_m6C99B4B8342D947F4B702426095273A300A394C5 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method);
// System.Single vp_Timer/Handle::get_TimeOfInitiation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_TimeOfInitiation_mCF50BA6AB357EC8F5C48CD34CBB3BA215E0DF8D2 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method);
// System.Single vp_Timer/Handle::get_TimeUntilNextIteration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_TimeUntilNextIteration_m10569D96A53447028A4808698BAA5C7E4723DDAC (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method);
// System.Single vp_Timer/Handle::get_Delay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_Delay_mC0587B87BD995E4254FF0376B9C6E452A7B739A9 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method);
// System.Single vp_Timer/Handle::get_Interval()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_Interval_m69485C36EEA899AD123B4611691EFF45B2769B73 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<vp_Timer/Event>::get_Count()
inline int32_t List_1_get_Count_m3B48315259447A236D1AC04EDC08F3A906417DFC_inline (List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<vp_Timer/Event>::get_Item(System.Int32)
inline Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * List_1_get_Item_m4E6521E5891DBB98559C82D481FA4F4945952B46_inline (List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * (*) (List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline)(__this, ___index0, method);
}
// System.Int32 vp_Timer/Handle::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Handle_get_Id_m94F85EB949AF983F520F10F557ECBE62B7E7725E (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method);
// System.String vp_Timer/Event::get_MethodInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Event_get_MethodInfo_m6D8C37E774DC62EF53B32C7200739E777903BBB3 (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC (RuntimeObject * ___arg00, RuntimeObject * ___arg11, RuntimeObject * ___arg22, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void vp_Timer::Cancel(vp_Timer/Handle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Timer_Cancel_mF17D30C62DBCF46555779B803D5F8098120BD3EB (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * ___handle0, const RuntimeMethod* method);
// System.Void System.Diagnostics.StackTrace::.ctor()
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void StackTrace__ctor_mB01121AE4B50E28777806A8C6BA844C2E564AB1F (StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// System.Int32 System.String::LastIndexOf(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20 (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// System.String System.String::Substring(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE (String_t* __this, int32_t ___startIndex0, const RuntimeMethod* method);
// System.String System.String::Replace(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_m970DFB0A280952FA7D3BA20AB7A8FB9F80CF6470 (String_t* __this, String_t* ___oldValue0, String_t* ___newValue1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,System.String>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_mF96A70AF9B37F114A6DAC1FF67D1EB8C5072E739 (Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * __this, Type_t * ___key0, String_t** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 *, Type_t *, String_t**, const RuntimeMethod*))Dictionary_2_TryGetValue_m3455807C552312C60038DF52EF328C3687442DE3_gshared)(__this, ___key0, ___value1, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeSelf()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeSelf_mFE1834886CAE59884AC2BE707A3B821A1DB61F44 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Cursor_get_lockState_mE0C93F496E3AA120AD168ED30371C35ED79C9DF1 (const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_visible(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cursor_set_visible_mDB51E60B3D7B14873A6F5FBE5E0A432D4A46C431 (bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cursor_set_lockState_m019E27A0FE021A28A1C672801416ACA5E770933F (int32_t ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Boolean vp_Utility::IsDescendant(UnityEngine.Transform,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vp_Utility_IsDescendant_m2383C0A44C9B19957CF9C867D00DF32E10CEEDAD (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___descendant0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___potentialAncestor1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.String System.String::ToLower()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8 (String_t* __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_m4488034AF8CB3EEA9A205EB8A1F25D438FF8704B (String_t* __this, String_t* ___value0, const RuntimeMethod* method);
// UnityEngine.Transform vp_Utility::GetTransformByNameInChildren(UnityEngine.Transform,System.String,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * vp_Utility_GetTransformByNameInChildren_m4BAEFE6AB1211944BBC646B73A3C319EA810BE8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trans0, String_t* ___name1, bool ___includeInactive2, bool ___subString3, const RuntimeMethod* method);
// UnityEngine.Transform vp_Utility::GetTransformByNameInAncestors(UnityEngine.Transform,System.String,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * vp_Utility_GetTransformByNameInAncestors_m85889B83D8D4B16EFBB1C417E7964480C825AEE4 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trans0, String_t* ___name1, bool ___includeInactive2, bool ___subString3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// UnityEngine.Object vp_Utility::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * vp_Utility_Instantiate_mB3FC1BB39D06ACF32E0409CAA385DDB0915B65E7 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method);
// vp_PoolManager vp_PoolManager::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_mAA0C9ED5A3D1589C1C8AA22636543528DB353CFB (Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * Object_Instantiate_mAF9C2662167396DEE640598515B60BE41B9D5082 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method);
// UnityEngine.GameObject vp_PoolManager::Spawn(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * vp_PoolManager_Spawn_mCC73C86FEAD328B569F91955193CD4FB0299867B (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void vp_PoolManager::Despawn(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_PoolManager_Despawn_mC1BC1EA7AD2EDDA5739DF4AD58ED8050FAC4E5DF (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj0, const RuntimeMethod* method);
// System.Void vp_Utility/<>c__DisplayClass18_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass18_0__ctor_m8FAC57006AAA255FFF14C50AEDCE44E0836232BA (U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, float ___t1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Max_m670AE0EC1B09ED1A56FF9606B0F954670319CB65 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Void vp_Timer/Callback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Callback__ctor_m56FE6C8553E18184859AD3DF01727565DF7F910C (Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void vp_Timer::In(System.Single,vp_Timer/Callback,vp_Timer/Handle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Timer_In_m6C38B0240730B0B7637A6ABE382178AD62AE600D (float ___delay0, Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * ___callback1, Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * ___timerHandle2, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780 (int32_t ___min0, int32_t ___max1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(!0)
inline bool Dictionary_2_ContainsKey_mBC44DAB119CBE7A4B7820ABF12AD024A4654471E (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 *, int32_t, const RuntimeMethod*))Dictionary_2_ContainsKey_mBC44DAB119CBE7A4B7820ABF12AD024A4654471E_gshared)(__this, ___key0, method);
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
inline int32_t Dictionary_2_get_Count_m73CF0E63105DEFE34A611344CF2C01B27AF83FFB (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 *, const RuntimeMethod*))Dictionary_2_get_Count_m73CF0E63105DEFE34A611344CF2C01B27AF83FFB_gshared)(__this, method);
}
// System.Void vp_Utility::ClearUniqueIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility_ClearUniqueIDs_m559A9F7D03CF00423358623E578D00A918B328DC (const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(!0,!1)
inline void Dictionary_2_Add_mDC25A2F4744B497C39F127D97327CC7B56E68903 (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 *, int32_t, int32_t, const RuntimeMethod*))Dictionary_2_Add_mDC25A2F4744B497C39F127D97327CC7B56E68903_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
inline void Dictionary_2_Clear_m7DE8E346650F09FFAB65ABCACFEBCB2FAB2F4499 (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 *, const RuntimeMethod*))Dictionary_2_Clear_m7DE8E346650F09FFAB65ABCACFEBCB2FAB2F4499_gshared)(__this, method);
}
// System.Void vp_AudioUtility::PlayRandomSound(UnityEngine.AudioSource,System.Collections.Generic.List`1<UnityEngine.AudioClip>,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_AudioUtility_PlayRandomSound_m718D90E2F96884AE683DB5C6238BC081A688D679 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource0, List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * ___sounds1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pitchRange2, const RuntimeMethod* method);
// System.Void vp_AudioUtility::PlayRandomSound(UnityEngine.AudioSource,System.Collections.Generic.List`1<UnityEngine.AudioClip>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_AudioUtility_PlayRandomSound_mB4EBABD957F1B1A623B11BA162A9D5CF44544B85 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource0, List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * ___sounds1, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280 (Rect_t35B976DE901B5423C11705E156938EA27AB402CE * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0 (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// UnityEngine.Sprite UnityEngine.Sprite::Create(UnityEngine.Texture2D,UnityEngine.Rect,UnityEngine.Vector2,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * Sprite_Create_m84A724DB0F0D73AEBE5296B4324D61EBCA72A843 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___rect1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pivot2, float ___pixelsPerUnit3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.String>::.ctor()
inline void Dictionary_2__ctor_mDE30E1D450337479C15FB7C79523406548F2D396 (Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 *, const RuntimeMethod*))Dictionary_2__ctor_m2C7E51568033239B506E15E7804A0B8658246498_gshared)(__this, method);
}
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6 (RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ___handle0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Type,System.String>::Add(!0,!1)
inline void Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA (Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * __this, Type_t * ___key0, String_t* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 *, Type_t *, String_t*, const RuntimeMethod*))Dictionary_2_Add_mC741BBB0A647C814227953DB9B23CB1BDF571C5B_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
inline void Dictionary_2__ctor_m5B4A5BC34051AAA3F4C9590CF249F3A095332161 (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 *, const RuntimeMethod*))Dictionary_2__ctor_m5B4A5BC34051AAA3F4C9590CF249F3A095332161_gshared)(__this, method);
}
// System.Void vp_PoolManager::Despawn(UnityEngine.GameObject,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_PoolManager_Despawn_m9870CB2ADCBE296E93D5EF5D220359A2FD2FCE37 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj0, float ___delay1, const RuntimeMethod* method);
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  Transform_get_localToWorldMatrix_mBC86B8C7BA6F53DAB8E0120D77729166399A0EED (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_matrix(UnityEngine.Matrix4x4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_set_matrix_mA3B65EC8681EDF68BDD2A657F4B67C00C1C34565 (Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28 (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB (const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawCube_m55519F7455796C0858AE0D0FD7BC2AA62138957E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___center0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___size1, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C (Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D (const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawLine_m9515D59D2536571F4906A3C54E613A3986DFD892 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___from0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___to1, const RuntimeMethod* method);
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2 (Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61 (Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  ___c0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Timer_<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mE2FD9741E9B6569B6DF9BAFC826AA9FAACEB1964 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_mE2FD9741E9B6569B6DF9BAFC826AA9FAACEB1964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D * L_0 = (U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D *)il2cpp_codegen_object_new(U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mF1EA0DF766594CC8579CF02CAF353208E57AD94E(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void vp_Timer_<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mF1EA0DF766594CC8579CF02CAF353208E57AD94E (U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_Timer_<>c::<Start>b__24_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CStartU3Eb__24_0_mB0C59AB47BA9810AF8601A63A7289233362D339D (U3CU3Ec_tB8351F5212A543FC8A4713B1E39A0E958DE50E5D * __this, const RuntimeMethod* method)
{
	{
		// Schedule(315360000.0f, /* ten years, yo ;) */ delegate() { }, null, null, timerHandle, 1, -1.0f);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Timer_ArgCallback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgCallback__ctor_m54F3568C2236D0C945D04D797C17BFFD6252E04C (ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void vp_Timer_ArgCallback::Invoke(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgCallback_Invoke_mFC4A16689B521E5415EB72EA62090F7AFF7E19C0 (ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * __this, RuntimeObject * ___args0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___args0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___args0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___args0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___args0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___args0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___args0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___args0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___args0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___args0);
					else
						GenericVirtActionInvoker1< RuntimeObject * >::Invoke(targetMethod, targetThis, ___args0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___args0);
					else
						VirtActionInvoker1< RuntimeObject * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___args0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, RuntimeObject *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___args0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult vp_Timer_ArgCallback::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ArgCallback_BeginInvoke_m5BDEFF08A315C338C6B0226CF602C0FD37E1BF3E (ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * __this, RuntimeObject * ___args0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___args0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void vp_Timer_ArgCallback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgCallback_EndInvoke_m4480BDE54488767C93DEC1F81332BFFAF778928E (ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 (Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void vp_Timer_Callback::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Callback__ctor_m56FE6C8553E18184859AD3DF01727565DF7F910C (Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void vp_Timer_Callback::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Callback_Invoke_m3A5AA72A5D0FB5CCFB9DF56B2B5E4A4627804E9A (Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * __this, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef void (*FunctionPointerType) (const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult vp_Timer_Callback::BeginInvoke(System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Callback_BeginInvoke_m13FDE19DB4B0C59FFAB25B0BB810C18DC385E763 (Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * __this, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void vp_Timer_Callback::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Callback_EndInvoke_mA093EDF2D1B815FE2DDFD7FF7EEBF6614A092FE8 (Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Timer_Event::Execute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Event_Execute_m2224A3F8441A1B5740F0B81AD62CF8452FACBDF6 (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_Execute_m2224A3F8441A1B5740F0B81AD62CF8452FACBDF6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	int32_t G_B3_0 = 0;
	{
		// if (Id == 0 || DueTime == 0.0f)
		int32_t L_0 = __this->get_Id_0();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		float L_1 = __this->get_DueTime_6();
		G_B3_0 = ((((float)L_1) == ((float)(0.0f)))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 1;
	}

IL_0019:
	{
		V_0 = (bool)G_B3_0;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		// Recycle();
		Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E(__this, /*hidden argument*/NULL);
		// return;
		goto IL_00c6;
	}

IL_002a:
	{
		// if (Function != null)
		Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * L_3 = __this->get_Function_1();
		V_1 = (bool)((!(((RuntimeObject*)(Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 *)L_3) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0047;
		}
	}
	{
		// Function();
		Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * L_5 = __this->get_Function_1();
		NullCheck(L_5);
		Callback_Invoke_m3A5AA72A5D0FB5CCFB9DF56B2B5E4A4627804E9A(L_5, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0047:
	{
		// else if (ArgFunction != null)
		ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * L_6 = __this->get_ArgFunction_2();
		V_2 = (bool)((!(((RuntimeObject*)(ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 *)L_6) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0068;
		}
	}
	{
		// ArgFunction(Arguments);
		ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * L_8 = __this->get_ArgFunction_2();
		RuntimeObject * L_9 = __this->get_Arguments_3();
		NullCheck(L_8);
		ArgCallback_Invoke_mFC4A16689B521E5415EB72EA62090F7AFF7E19C0(L_8, L_9, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0068:
	{
		// Error("Aborted event because function is null.");
		Event_Error_mB82C87213850999FBA9FF7CC1AF57504C07CADA5(__this, _stringLiteralA4178B659666B8A8CA4F335016681A0D46EA521B, /*hidden argument*/NULL);
		// Recycle();
		Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E(__this, /*hidden argument*/NULL);
		// return;
		goto IL_00c6;
	}

IL_007e:
	{
		// if (Iterations > 0)
		int32_t L_10 = __this->get_Iterations_4();
		V_3 = (bool)((((int32_t)L_10) > ((int32_t)0))? 1 : 0);
		bool L_11 = V_3;
		if (!L_11)
		{
			goto IL_00b4;
		}
	}
	{
		// Iterations--;
		int32_t L_12 = __this->get_Iterations_4();
		__this->set_Iterations_4(((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1)));
		// if (Iterations < 1)
		int32_t L_13 = __this->get_Iterations_4();
		V_4 = (bool)((((int32_t)L_13) < ((int32_t)1))? 1 : 0);
		bool L_14 = V_4;
		if (!L_14)
		{
			goto IL_00b3;
		}
	}
	{
		// Recycle();
		Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E(__this, /*hidden argument*/NULL);
		// return;
		goto IL_00c6;
	}

IL_00b3:
	{
	}

IL_00b4:
	{
		// DueTime = Time.time + Interval;
		float L_15 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_16 = __this->get_Interval_5();
		__this->set_DueTime_6(((float)il2cpp_codegen_add((float)L_15, (float)L_16)));
	}

IL_00c6:
	{
		// }
		return;
	}
}
// System.Void vp_Timer_Event::Recycle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_Recycle_m0A6F90627F9093CF6F7962BA7C8A4A4402BAA36E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// Id = 0;
		__this->set_Id_0(0);
		// DueTime = 0.0f;
		__this->set_DueTime_6((0.0f));
		// StartTime = 0.0f;
		__this->set_StartTime_7((0.0f));
		// CancelOnLoad = true;
		__this->set_CancelOnLoad_10((bool)1);
		// Function = null;
		__this->set_Function_1((Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 *)NULL);
		// ArgFunction = null;
		__this->set_ArgFunction_2((ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 *)NULL);
		// Arguments = null;
		__this->set_Arguments_3(NULL);
		// if (vp_Timer.m_Active.Remove(this))
		IL2CPP_RUNTIME_CLASS_INIT(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var);
		List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * L_0 = ((vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields*)il2cpp_codegen_static_fields_for(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var))->get_m_Active_5();
		NullCheck(L_0);
		bool L_1 = List_1_Remove_m3E8E2290A4702A1A0394D255A0CE9D5B69B6304C(L_0, __this, /*hidden argument*/List_1_Remove_m3E8E2290A4702A1A0394D255A0CE9D5B69B6304C_RuntimeMethod_var);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		// m_Pool.Add(this);
		IL2CPP_RUNTIME_CLASS_INIT(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var);
		List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * L_3 = ((vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields*)il2cpp_codegen_static_fields_for(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var))->get_m_Pool_6();
		NullCheck(L_3);
		List_1_Add_m8662D37E19174217C357332D01C091C6944740CF(L_3, __this, /*hidden argument*/List_1_Add_m8662D37E19174217C357332D01C091C6944740CF_RuntimeMethod_var);
	}

IL_0055:
	{
		// }
		return;
	}
}
// System.Void vp_Timer_Event::Destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Event_Destroy_m54F753C2DB0D932A829D856B16E14C11F7F18AF9 (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_Destroy_m54F753C2DB0D932A829D856B16E14C11F7F18AF9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// vp_Timer.m_Active.Remove(this);
		IL2CPP_RUNTIME_CLASS_INIT(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var);
		List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * L_0 = ((vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields*)il2cpp_codegen_static_fields_for(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var))->get_m_Active_5();
		NullCheck(L_0);
		List_1_Remove_m3E8E2290A4702A1A0394D255A0CE9D5B69B6304C(L_0, __this, /*hidden argument*/List_1_Remove_m3E8E2290A4702A1A0394D255A0CE9D5B69B6304C_RuntimeMethod_var);
		// vp_Timer.m_Pool.Remove(this);
		List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * L_1 = ((vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields*)il2cpp_codegen_static_fields_for(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var))->get_m_Pool_6();
		NullCheck(L_1);
		List_1_Remove_m3E8E2290A4702A1A0394D255A0CE9D5B69B6304C(L_1, __this, /*hidden argument*/List_1_Remove_m3E8E2290A4702A1A0394D255A0CE9D5B69B6304C_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void vp_Timer_Event::Error(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Event_Error_mB82C87213850999FBA9FF7CC1AF57504C07CADA5 (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_Error_mB82C87213850999FBA9FF7CC1AF57504C07CADA5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// string msg = "Error: (" + this + ") " + message;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_0 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral29C5345848121BE0ED79301284519245F9C7A08B);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral29C5345848121BE0ED79301284519245F9C7A08B);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = L_1;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, __this);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)__this);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_3 = L_2;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, _stringLiteral3E2B500817A96FA5BAECB12EAFF42205003D74E6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral3E2B500817A96FA5BAECB12EAFF42205003D74E6);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_4 = L_3;
		String_t* L_5 = ___message0;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_5);
		String_t* L_6 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_4, /*hidden argument*/NULL);
		V_0 = L_6;
		// UnityEngine.Debug.LogError(msg);
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String vp_Timer_Event::get_MethodName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Event_get_MethodName_m2E7322780EA4E933FB1C38D9E0863E8C719DBC2E (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_get_MethodName_m2E7322780EA4E933FB1C38D9E0863E8C719DBC2E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	String_t* V_3 = NULL;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	{
		// if (Function != null)
		Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * L_0 = __this->get_Function_1();
		V_0 = (bool)((!(((RuntimeObject*)(Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 *)L_0) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0061;
		}
	}
	{
		// if (Function.Method != null)
		Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * L_2 = __this->get_Function_1();
		NullCheck(L_2);
		MethodInfo_t * L_3 = Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048(L_2, /*hidden argument*/NULL);
		bool L_4 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237(L_3, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_005e;
		}
	}
	{
		// if (Function.Method.Name[0] == '<')
		Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * L_6 = __this->get_Function_1();
		NullCheck(L_6);
		MethodInfo_t * L_7 = Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_7);
		NullCheck(L_8);
		Il2CppChar L_9 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_8, 0, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)((int32_t)60)))? 1 : 0);
		bool L_10 = V_2;
		if (!L_10)
		{
			goto IL_004b;
		}
	}
	{
		// return "delegate";
		V_3 = _stringLiteral52934C29CA4A1A7572C6905BD6AB705A2C135973;
		goto IL_00c9;
	}

IL_004b:
	{
		// else return Function.Method.Name;
		Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * L_11 = __this->get_Function_1();
		NullCheck(L_11);
		MethodInfo_t * L_12 = Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_12);
		V_3 = L_13;
		goto IL_00c9;
	}

IL_005e:
	{
		goto IL_00c5;
	}

IL_0061:
	{
		// else if (ArgFunction != null)
		ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * L_14 = __this->get_ArgFunction_2();
		V_4 = (bool)((!(((RuntimeObject*)(ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 *)L_14) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_15 = V_4;
		if (!L_15)
		{
			goto IL_00c5;
		}
	}
	{
		// if (ArgFunction.Method != null)
		ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * L_16 = __this->get_ArgFunction_2();
		NullCheck(L_16);
		MethodInfo_t * L_17 = Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048(L_16, /*hidden argument*/NULL);
		bool L_18 = MethodInfo_op_Inequality_m76AC38C8B8FB8F28C21E6F9A3F0268FF8E4CC237(L_17, (MethodInfo_t *)NULL, /*hidden argument*/NULL);
		V_5 = L_18;
		bool L_19 = V_5;
		if (!L_19)
		{
			goto IL_00c4;
		}
	}
	{
		// if (ArgFunction.Method.Name[0] == '<')
		ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * L_20 = __this->get_ArgFunction_2();
		NullCheck(L_20);
		MethodInfo_t * L_21 = Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_21);
		NullCheck(L_22);
		Il2CppChar L_23 = String_get_Chars_m14308AC3B95F8C1D9F1D1055B116B37D595F1D96(L_22, 0, /*hidden argument*/NULL);
		V_6 = (bool)((((int32_t)L_23) == ((int32_t)((int32_t)60)))? 1 : 0);
		bool L_24 = V_6;
		if (!L_24)
		{
			goto IL_00b1;
		}
	}
	{
		// return "delegate";
		V_3 = _stringLiteral52934C29CA4A1A7572C6905BD6AB705A2C135973;
		goto IL_00c9;
	}

IL_00b1:
	{
		// else return ArgFunction.Method.Name;
		ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 * L_25 = __this->get_ArgFunction_2();
		NullCheck(L_25);
		MethodInfo_t * L_26 = Delegate_get_Method_m0AC85D2B0C4CA63C471BC37FFDC3A5EA1E8ED048(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_26);
		V_3 = L_27;
		goto IL_00c9;
	}

IL_00c4:
	{
	}

IL_00c5:
	{
		// return null;
		V_3 = (String_t*)NULL;
		goto IL_00c9;
	}

IL_00c9:
	{
		// }
		String_t* L_28 = V_3;
		return L_28;
	}
}
// System.String vp_Timer_Event::get_MethodInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Event_get_MethodInfo_m6D8C37E774DC62EF53B32C7200739E777903BBB3 (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_get_MethodInfo_m6D8C37E774DC62EF53B32C7200739E777903BBB3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* V_4 = NULL;
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* V_5 = NULL;
	int32_t V_6 = 0;
	RuntimeObject * V_7 = NULL;
	bool V_8 = false;
	String_t* V_9 = NULL;
	{
		// string s = MethodName;
		String_t* L_0 = Event_get_MethodName_m2E7322780EA4E933FB1C38D9E0863E8C719DBC2E(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		// if (!string.IsNullOrEmpty(s))
		String_t* L_1 = V_0;
		bool L_2 = String_IsNullOrEmpty_m06A85A206AC2106D1982826C5665B9BD35324229(L_1, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_00ca;
		}
	}
	{
		// s += "(";
		String_t* L_4 = V_0;
		String_t* L_5 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_4, _stringLiteral28ED3A797DA3C48C309A4EF792147F3C56CFEC40, /*hidden argument*/NULL);
		V_0 = L_5;
		// if (Arguments != null)
		RuntimeObject * L_6 = __this->get_Arguments_3();
		V_2 = (bool)((!(((RuntimeObject*)(RuntimeObject *)L_6) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_00bb;
		}
	}
	{
		// if (Arguments.GetType().IsArray)
		RuntimeObject * L_8 = __this->get_Arguments_3();
		NullCheck(L_8);
		Type_t * L_9 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_10 = Type_get_IsArray_m0B4E20F93B1B34C0B5C4B089F543D1AA338DC9FE(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		bool L_11 = V_3;
		if (!L_11)
		{
			goto IL_00ad;
		}
	}
	{
		// object[] array = (object[])Arguments;
		RuntimeObject * L_12 = __this->get_Arguments_3();
		V_4 = ((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)Castclass((RuntimeObject*)L_12, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var));
		// foreach (object o in array)
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_13 = V_4;
		V_5 = L_13;
		V_6 = 0;
		goto IL_00a2;
	}

IL_0062:
	{
		// foreach (object o in array)
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_14 = V_5;
		int32_t L_15 = V_6;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_7 = L_17;
		// s += o.ToString();
		String_t* L_18 = V_0;
		RuntimeObject * L_19 = V_7;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		String_t* L_21 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_18, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		// if (Array.IndexOf(array, o) < array.Length - 1)
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_22 = V_4;
		RuntimeObject * L_23 = V_7;
		int32_t L_24 = Array_IndexOf_TisRuntimeObject_m40554FA47BA74C45E33C913F60628DD0E83DB370(L_22, L_23, /*hidden argument*/Array_IndexOf_TisRuntimeObject_m40554FA47BA74C45E33C913F60628DD0E83DB370_RuntimeMethod_var);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_25 = V_4;
		NullCheck(L_25);
		V_8 = (bool)((((int32_t)L_24) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_25)->max_length)))), (int32_t)1))))? 1 : 0);
		bool L_26 = V_8;
		if (!L_26)
		{
			goto IL_009b;
		}
	}
	{
		// s += ", ";
		String_t* L_27 = V_0;
		String_t* L_28 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_27, _stringLiteralD3BC9A378DAAA1DDDBA1B19C1AA641D3E9683C46, /*hidden argument*/NULL);
		V_0 = L_28;
	}

IL_009b:
	{
		int32_t L_29 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_00a2:
	{
		// foreach (object o in array)
		int32_t L_30 = V_6;
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_31 = V_5;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_31)->max_length)))))))
		{
			goto IL_0062;
		}
	}
	{
		goto IL_00ba;
	}

IL_00ad:
	{
		// s += Arguments;
		String_t* L_32 = V_0;
		RuntimeObject * L_33 = __this->get_Arguments_3();
		String_t* L_34 = String_Concat_mBB19C73816BDD1C3519F248E1ADC8E11A6FDB495(L_32, L_33, /*hidden argument*/NULL);
		V_0 = L_34;
	}

IL_00ba:
	{
	}

IL_00bb:
	{
		// s += ")";
		String_t* L_35 = V_0;
		String_t* L_36 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_35, _stringLiteralE7064F0B80F61DBC65915311032D27BAA569AE2A, /*hidden argument*/NULL);
		V_0 = L_36;
		goto IL_00d0;
	}

IL_00ca:
	{
		// s = "(function = null)";
		V_0 = _stringLiteral6BF8FB135FFFA4FAE44361B8B5524F0E172C610E;
	}

IL_00d0:
	{
		// return s;
		String_t* L_37 = V_0;
		V_9 = L_37;
		goto IL_00d5;
	}

IL_00d5:
	{
		// }
		String_t* L_38 = V_9;
		return L_38;
	}
}
// System.Void vp_Timer_Event::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Event__ctor_m8A8093020EB76395E72386F4BCFA01A44DA356EB (Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * __this, const RuntimeMethod* method)
{
	{
		// public Callback Function = null;
		__this->set_Function_1((Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 *)NULL);
		// public ArgCallback ArgFunction = null;
		__this->set_ArgFunction_2((ArgCallback_tEFDC47569080FF02F616D3B88E9A4F1B6477BE63 *)NULL);
		// public object Arguments = null;
		__this->set_Arguments_3(NULL);
		// public int Iterations = 1;
		__this->set_Iterations_4(1);
		// public float Interval = -1.0f;
		__this->set_Interval_5((-1.0f));
		// public float DueTime = 0.0f;
		__this->set_DueTime_6((0.0f));
		// public float StartTime = 0.0f;
		__this->set_StartTime_7((0.0f));
		// public float LifeTime = 0.0f;
		__this->set_LifeTime_8((0.0f));
		// public bool Paused = false;
		__this->set_Paused_9((bool)0);
		// public bool CancelOnLoad = true;
		__this->set_CancelOnLoad_10((bool)1);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean vp_Timer_Handle::get_Paused()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Handle_get_Paused_m562EB5C50C38188FF7F7F259F226F0413431A21C (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		// return Active && m_Event.Paused;
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_1 = __this->get_m_Event_0();
		NullCheck(L_1);
		bool L_2 = L_1->get_Paused_9();
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 0;
	}

IL_0017:
	{
		V_0 = (bool)G_B3_0;
		goto IL_001a;
	}

IL_001a:
	{
		// }
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void vp_Timer_Handle::set_Paused(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Handle_set_Paused_mFB84304A3C08EC30C7DCFAC8798AB87B54BD03D9 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, bool ___value0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		// m_Event.Paused = value;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		bool L_3 = ___value0;
		NullCheck(L_2);
		L_2->set_Paused_9(L_3);
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Single vp_Timer_Handle::get_TimeOfInitiation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_TimeOfInitiation_mCF50BA6AB357EC8F5C48CD34CBB3BA215E0DF8D2 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// return m_Event.StartTime;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		float L_3 = L_2->get_StartTime_7();
		V_1 = L_3;
		goto IL_0021;
	}

IL_0019:
	{
		// else return 0.0f;
		V_1 = (0.0f);
		goto IL_0021;
	}

IL_0021:
	{
		// }
		float L_4 = V_1;
		return L_4;
	}
}
// System.Single vp_Timer_Handle::get_TimeOfFirstIteration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_TimeOfFirstIteration_mA39FE629D19E11FB05828A3270347DC5D9394868 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		// return m_FirstDueTime;
		float L_2 = __this->get_m_FirstDueTime_3();
		V_1 = L_2;
		goto IL_001c;
	}

IL_0014:
	{
		// return 0.0f;
		V_1 = (0.0f);
		goto IL_001c;
	}

IL_001c:
	{
		// }
		float L_3 = V_1;
		return L_3;
	}
}
// System.Single vp_Timer_Handle::get_TimeOfNextIteration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_TimeOfNextIteration_mBFD62F4ED09C4A9F171C38186FDF15C340D575D9 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// return m_Event.DueTime;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		float L_3 = L_2->get_DueTime_6();
		V_1 = L_3;
		goto IL_0021;
	}

IL_0019:
	{
		// return 0.0f;
		V_1 = (0.0f);
		goto IL_0021;
	}

IL_0021:
	{
		// }
		float L_4 = V_1;
		return L_4;
	}
}
// System.Single vp_Timer_Handle::get_TimeOfLastIteration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_TimeOfLastIteration_mF336E13CE195013F54C2B844BB36C0786283FCE4 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// return Time.time + DurationLeft;
		float L_2 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		float L_3 = Handle_get_DurationLeft_m6C99B4B8342D947F4B702426095273A300A394C5(__this, /*hidden argument*/NULL);
		V_1 = ((float)il2cpp_codegen_add((float)L_2, (float)L_3));
		goto IL_0022;
	}

IL_001a:
	{
		// return 0.0f;
		V_1 = (0.0f);
		goto IL_0022;
	}

IL_0022:
	{
		// }
		float L_4 = V_1;
		return L_4;
	}
}
// System.Single vp_Timer_Handle::get_Delay()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_Delay_mC0587B87BD995E4254FF0376B9C6E452A7B739A9 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handle_get_Delay_mC0587B87BD995E4254FF0376B9C6E452A7B739A9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// return (Mathf.Round((m_FirstDueTime - TimeOfInitiation) * 1000.0f) / 1000.0f);
		float L_0 = __this->get_m_FirstDueTime_3();
		float L_1 = Handle_get_TimeOfInitiation_mCF50BA6AB357EC8F5C48CD34CBB3BA215E0DF8D2(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_2 = bankers_roundf(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)), (float)(1000.0f))));
		V_0 = ((float)((float)L_2/(float)(1000.0f)));
		goto IL_0022;
	}

IL_0022:
	{
		// }
		float L_3 = V_0;
		return L_3;
	}
}
// System.Single vp_Timer_Handle::get_Interval()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_Interval_m69485C36EEA899AD123B4611691EFF45B2769B73 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// return m_Event.Interval;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		float L_3 = L_2->get_Interval_5();
		V_1 = L_3;
		goto IL_0021;
	}

IL_0019:
	{
		// return 0.0f;
		V_1 = (0.0f);
		goto IL_0021;
	}

IL_0021:
	{
		// }
		float L_4 = V_1;
		return L_4;
	}
}
// System.Single vp_Timer_Handle::get_TimeUntilNextIteration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_TimeUntilNextIteration_m10569D96A53447028A4808698BAA5C7E4723DDAC (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// return m_Event.DueTime - Time.time;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		float L_3 = L_2->get_DueTime_6();
		float L_4 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		V_1 = ((float)il2cpp_codegen_subtract((float)L_3, (float)L_4));
		goto IL_0027;
	}

IL_001f:
	{
		// return 0.0f;
		V_1 = (0.0f);
		goto IL_0027;
	}

IL_0027:
	{
		// }
		float L_5 = V_1;
		return L_5;
	}
}
// System.Single vp_Timer_Handle::get_DurationLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_DurationLeft_m6C99B4B8342D947F4B702426095273A300A394C5 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		// return TimeUntilNextIteration + ((m_Event.Iterations - 1) * m_Event.Interval);
		float L_2 = Handle_get_TimeUntilNextIteration_m10569D96A53447028A4808698BAA5C7E4723DDAC(__this, /*hidden argument*/NULL);
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_3 = __this->get_m_Event_0();
		NullCheck(L_3);
		int32_t L_4 = L_3->get_Iterations_4();
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_5 = __this->get_m_Event_0();
		NullCheck(L_5);
		float L_6 = L_5->get_Interval_5();
		V_1 = ((float)il2cpp_codegen_add((float)L_2, (float)((float)il2cpp_codegen_multiply((float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1))))), (float)L_6))));
		goto IL_0037;
	}

IL_002f:
	{
		// return 0.0f;
		V_1 = (0.0f);
		goto IL_0037;
	}

IL_0037:
	{
		// }
		float L_7 = V_1;
		return L_7;
	}
}
// System.Single vp_Timer_Handle::get_DurationTotal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_DurationTotal_m3A09F8D669E6A690108BC47DE33BFCDCBEEEC982 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B3_1 = 0.0f;
	float G_B2_0 = 0.0f;
	float G_B2_1 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B4_1 = 0.0f;
	float G_B4_2 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		// return Delay +
		//     ((m_StartIterations) * ((m_StartIterations > 1) ? Interval : 0.0f));
		float L_2 = Handle_get_Delay_mC0587B87BD995E4254FF0376B9C6E452A7B739A9(__this, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_m_StartIterations_2();
		int32_t L_4 = __this->get_m_StartIterations_2();
		G_B2_0 = (((float)((float)L_3)));
		G_B2_1 = L_2;
		if ((((int32_t)L_4) > ((int32_t)1)))
		{
			G_B3_0 = (((float)((float)L_3)));
			G_B3_1 = L_2;
			goto IL_0029;
		}
	}
	{
		G_B4_0 = (0.0f);
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_002f;
	}

IL_0029:
	{
		float L_5 = Handle_get_Interval_m69485C36EEA899AD123B4611691EFF45B2769B73(__this, /*hidden argument*/NULL);
		G_B4_0 = L_5;
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_002f:
	{
		V_1 = ((float)il2cpp_codegen_add((float)G_B4_2, (float)((float)il2cpp_codegen_multiply((float)G_B4_1, (float)G_B4_0))));
		goto IL_003c;
	}

IL_0034:
	{
		// return 0.0f;
		V_1 = (0.0f);
		goto IL_003c;
	}

IL_003c:
	{
		// }
		float L_6 = V_1;
		return L_6;
	}
}
// System.Single vp_Timer_Handle::get_Duration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Handle_get_Duration_m5F17B06185F2C5274416BE67918561ECC787B026 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	float V_1 = 0.0f;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// return m_Event.LifeTime;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		float L_3 = L_2->get_LifeTime_8();
		V_1 = L_3;
		goto IL_0021;
	}

IL_0019:
	{
		// return 0.0f;
		V_1 = (0.0f);
		goto IL_0021;
	}

IL_0021:
	{
		// }
		float L_4 = V_1;
		return L_4;
	}
}
// System.Int32 vp_Timer_Handle::get_IterationsTotal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Handle_get_IterationsTotal_m4AB3FA38F14F019FD2C636E80C907904D3760CF4 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return m_StartIterations;
		int32_t L_0 = __this->get_m_StartIterations_2();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 vp_Timer_Handle::get_IterationsLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Handle_get_IterationsLeft_m55653075F94469335A328CDA19F24AC02595BF11 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// return m_Event.Iterations;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_Iterations_4();
		V_1 = L_3;
		goto IL_001d;
	}

IL_0019:
	{
		// return 0;
		V_1 = 0;
		goto IL_001d;
	}

IL_001d:
	{
		// }
		int32_t L_4 = V_1;
		return L_4;
	}
}
// System.Int32 vp_Timer_Handle::get_Id()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Handle_get_Id_m94F85EB949AF983F520F10F557ECBE62B7E7725E (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// return m_Id;
		int32_t L_0 = __this->get_m_Id_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void vp_Timer_Handle::set_Id(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Handle_set_Id_m035AAB0529283D2F19A200F186C5AFDAB19F666D (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handle_set_Id_m035AAB0529283D2F19A200F186C5AFDAB19F666D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	{
		// m_Id = value;
		int32_t L_0 = ___value0;
		__this->set_m_Id_1(L_0);
		// if (m_Id == 0)
		int32_t L_1 = __this->get_m_Id_1();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		// m_Event.DueTime = 0.0f;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_3 = __this->get_m_Event_0();
		NullCheck(L_3);
		L_3->set_DueTime_6((0.0f));
		// return;
		goto IL_00eb;
	}

IL_002b:
	{
		// m_Event = null;
		__this->set_m_Event_0((Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 *)NULL);
		// for (int t = vp_Timer.m_Active.Count - 1; t > -1; t--)
		IL2CPP_RUNTIME_CLASS_INIT(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var);
		List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * L_4 = ((vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields*)il2cpp_codegen_static_fields_for(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var))->get_m_Active_5();
		NullCheck(L_4);
		int32_t L_5 = List_1_get_Count_m3B48315259447A236D1AC04EDC08F3A906417DFC_inline(L_4, /*hidden argument*/List_1_get_Count_m3B48315259447A236D1AC04EDC08F3A906417DFC_RuntimeMethod_var);
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)1));
		goto IL_0077;
	}

IL_0041:
	{
		// if (vp_Timer.m_Active[t].Id == m_Id)
		IL2CPP_RUNTIME_CLASS_INIT(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var);
		List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * L_6 = ((vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields*)il2cpp_codegen_static_fields_for(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var))->get_m_Active_5();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_8 = List_1_get_Item_m4E6521E5891DBB98559C82D481FA4F4945952B46_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_m4E6521E5891DBB98559C82D481FA4F4945952B46_RuntimeMethod_var);
		NullCheck(L_8);
		int32_t L_9 = L_8->get_Id_0();
		int32_t L_10 = __this->get_m_Id_1();
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)L_10))? 1 : 0);
		bool L_11 = V_2;
		if (!L_11)
		{
			goto IL_0072;
		}
	}
	{
		// m_Event = vp_Timer.m_Active[t];
		IL2CPP_RUNTIME_CLASS_INIT(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var);
		List_1_tB2A83BA53A799850027BDA8C129C285A3AE1414E * L_12 = ((vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_StaticFields*)il2cpp_codegen_static_fields_for(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var))->get_m_Active_5();
		int32_t L_13 = V_1;
		NullCheck(L_12);
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_14 = List_1_get_Item_m4E6521E5891DBB98559C82D481FA4F4945952B46_inline(L_12, L_13, /*hidden argument*/List_1_get_Item_m4E6521E5891DBB98559C82D481FA4F4945952B46_RuntimeMethod_var);
		__this->set_m_Event_0(L_14);
		// break;
		goto IL_007f;
	}

IL_0072:
	{
		// for (int t = vp_Timer.m_Active.Count - 1; t > -1; t--)
		int32_t L_15 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)1));
	}

IL_0077:
	{
		// for (int t = vp_Timer.m_Active.Count - 1; t > -1; t--)
		int32_t L_16 = V_1;
		V_3 = (bool)((((int32_t)L_16) > ((int32_t)(-1)))? 1 : 0);
		bool L_17 = V_3;
		if (L_17)
		{
			goto IL_0041;
		}
	}

IL_007f:
	{
		// if (m_Event == null)
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_18 = __this->get_m_Event_0();
		V_4 = (bool)((((RuntimeObject*)(Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 *)L_18) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_19 = V_4;
		if (!L_19)
		{
			goto IL_00c9;
		}
	}
	{
		// UnityEngine.Debug.LogError("Error: (" + this + ") Failed to assign event with Id '" + m_Id + "'.");
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_20 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)SZArrayNew(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A_il2cpp_TypeInfo_var, (uint32_t)5);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_21 = L_20;
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, _stringLiteral29C5345848121BE0ED79301284519245F9C7A08B);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteral29C5345848121BE0ED79301284519245F9C7A08B);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_22 = L_21;
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, __this);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)__this);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_23 = L_22;
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, _stringLiteral21A6A12B967DDEEC093A4DBAC3953726A4F48071);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral21A6A12B967DDEEC093A4DBAC3953726A4F48071);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_24 = L_23;
		int32_t L_25 = __this->get_m_Id_1();
		int32_t L_26 = L_25;
		RuntimeObject * L_27 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_27);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_27);
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_28 = L_24;
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, _stringLiteralFAFBA073700296227A7D1B4785C63F0194C967FA);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)_stringLiteralFAFBA073700296227A7D1B4785C63F0194C967FA);
		String_t* L_29 = String_Concat_mB7BA84F13912303B2E5E40FBF0109E1A328ACA07(L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_29, /*hidden argument*/NULL);
	}

IL_00c9:
	{
		// m_StartIterations = m_Event.Iterations;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_30 = __this->get_m_Event_0();
		NullCheck(L_30);
		int32_t L_31 = L_30->get_Iterations_4();
		__this->set_m_StartIterations_2(L_31);
		// m_FirstDueTime = m_Event.DueTime;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_32 = __this->get_m_Event_0();
		NullCheck(L_32);
		float L_33 = L_32->get_DueTime_6();
		__this->set_m_FirstDueTime_3(L_33);
	}

IL_00eb:
	{
		// }
		return;
	}
}
// System.Boolean vp_Timer_Handle::get_Active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	int32_t G_B4_0 = 0;
	{
		// if (m_Event == null || Id == 0 || m_Event.Id == 0)
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_0 = __this->get_m_Event_0();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_1 = Handle_get_Id_m94F85EB949AF983F520F10F557ECBE62B7E7725E(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_Id_0();
		G_B4_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B4_0 = 1;
	}

IL_0022:
	{
		V_0 = (bool)G_B4_0;
		bool L_4 = V_0;
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_0040;
	}

IL_002a:
	{
		// return m_Event.Id == Id;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_5 = __this->get_m_Event_0();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_Id_0();
		int32_t L_7 = Handle_get_Id_m94F85EB949AF983F520F10F557ECBE62B7E7725E(__this, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_0040;
	}

IL_0040:
	{
		// }
		bool L_8 = V_1;
		return L_8;
	}
}
// System.String vp_Timer_Handle::get_MethodName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Handle_get_MethodName_m0AE43AEDF241A34CC0BB638AA8B92945CDE5F88F (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handle_get_MethodName_m0AE43AEDF241A34CC0BB638AA8B92945CDE5F88F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// return m_Event.MethodName;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		String_t* L_3 = Event_get_MethodName_m2E7322780EA4E933FB1C38D9E0863E8C719DBC2E(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0021;
	}

IL_0019:
	{
		// return "";
		V_1 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		goto IL_0021;
	}

IL_0021:
	{
		// }
		String_t* L_4 = V_1;
		return L_4;
	}
}
// System.String vp_Timer_Handle::get_MethodInfo()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Handle_get_MethodInfo_m9F6A97E4B626742350A556D1C05029023B8AFA3F (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handle_get_MethodInfo_m9F6A97E4B626742350A556D1C05029023B8AFA3F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = NULL;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// return m_Event.MethodInfo;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		String_t* L_3 = Event_get_MethodInfo_m6D8C37E774DC62EF53B32C7200739E777903BBB3(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0021;
	}

IL_0019:
	{
		// return "";
		V_1 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		goto IL_0021;
	}

IL_0021:
	{
		// }
		String_t* L_4 = V_1;
		return L_4;
	}
}
// System.Boolean vp_Timer_Handle::get_CancelOnLoad()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Handle_get_CancelOnLoad_mFB1F3942A62413B5DDAD1C54AAAF0C56FFB27BF2 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// return m_Event.CancelOnLoad;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		NullCheck(L_2);
		bool L_3 = L_2->get_CancelOnLoad_10();
		V_1 = L_3;
		goto IL_001d;
	}

IL_0019:
	{
		// return true;
		V_1 = (bool)1;
		goto IL_001d;
	}

IL_001d:
	{
		// }
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Void vp_Timer_Handle::set_CancelOnLoad(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Handle_set_CancelOnLoad_mE921CED0AF28227617B3E4F315749E4205211216 (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handle_set_CancelOnLoad_mE921CED0AF28227617B3E4F315749E4205211216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (Active)
		bool L_0 = Handle_get_Active_m7C0FFB095EEEDDF86EF935E023212AEB5EC9347C(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// m_Event.CancelOnLoad = value;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_2 = __this->get_m_Event_0();
		bool L_3 = ___value0;
		NullCheck(L_2);
		L_2->set_CancelOnLoad_10(L_3);
		// return;
		goto IL_0030;
	}

IL_001a:
	{
		// UnityEngine.Debug.LogWarning("Warning: (" + this + ") Tried to set CancelOnLoad on inactive timer handle.");
		String_t* L_4 = String_Concat_m2E1F71C491D2429CC80A28745488FEA947BB7AAC(_stringLiteral9AA073A8C8CD3CD43164FAFE2AF0D0D81F5F271B, __this, _stringLiteralC2B40AE733FE3E1BC6CCBBFEDE88932EE0DDBBA3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(L_4, /*hidden argument*/NULL);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void vp_Timer_Handle::Cancel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Handle_Cancel_mFE3C59AD52725C2AF4777A670FB61E2CA05D54EF (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Handle_Cancel_mFE3C59AD52725C2AF4777A670FB61E2CA05D54EF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// vp_Timer.Cancel(this);
		IL2CPP_RUNTIME_CLASS_INIT(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var);
		vp_Timer_Cancel_mF17D30C62DBCF46555779B803D5F8098120BD3EB(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void vp_Timer_Handle::Execute()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Handle_Execute_m9B76827FC4A9073BB28118B740366FA064C0FF2A (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	{
		// m_Event.DueTime = Time.time;
		Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 * L_0 = __this->get_m_Event_0();
		float L_1 = Time_get_time_m7863349C8845BBA36629A2B3F8EF1C3BEA350FD8(/*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_DueTime_6(L_1);
		// }
		return;
	}
}
// System.Void vp_Timer_Handle::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Handle__ctor_m685E26AAB613CA402290CFD650E608B180BC91EC (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 * __this, const RuntimeMethod* method)
{
	{
		// private vp_Timer.Event m_Event = null;    // timer we're pointing at
		__this->set_m_Event_0((Event_t1069C86BD4882FFD687B1957C55937E4566F9BC8 *)NULL);
		// private int m_Id = 0;                    // id associated with timer upon creation of this handle
		__this->set_m_Id_1(0);
		// private int m_StartIterations = 1;        // the amount of iterations of the event when started
		__this->set_m_StartIterations_2(1);
		// private float m_FirstDueTime = 0.0f;    // the initial execution delay of the event
		__this->set_m_FirstDueTime_3((0.0f));
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String vp_Utility::GetErrorLocation(System.Int32,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* vp_Utility_GetErrorLocation_m6F3429DFF080043CBB6719F998CEDF3D76E631B9 (int32_t ___level0, bool ___showOnlyLast1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_GetErrorLocation_m6F3429DFF080043CBB6719F998CEDF3D76E631B9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99 * V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00 * V_4 = NULL;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	String_t* V_9 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// StackTrace stackTrace = new StackTrace();
		StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99 * L_0 = (StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99 *)il2cpp_codegen_object_new(StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99_il2cpp_TypeInfo_var);
		StackTrace__ctor_mB01121AE4B50E28777806A8C6BA844C2E564AB1F(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// string result = "";
		V_1 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// string declaringType = "";
		V_2 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// for (int v = stackTrace.FrameCount - 1; v > level; v--)
		StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99 * L_1 = V_0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_1);
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1));
		goto IL_0098;
	}

IL_001e:
	{
		// if (v < stackTrace.FrameCount - 1)
		int32_t L_3 = V_3;
		StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Diagnostics.StackTrace::get_FrameCount() */, L_4);
		V_5 = (bool)((((int32_t)L_3) < ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)1))))? 1 : 0);
		bool L_6 = V_5;
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		// result += " --> ";
		String_t* L_7 = V_1;
		String_t* L_8 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE(L_7, _stringLiteral63EA88A95D11C675ECBFDF34069E492F0B9B2F1B, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_003c:
	{
		// StackFrame stackFrame = stackTrace.GetFrame(v);
		StackTrace_tD5D45826A379D8DF0CFB2CA206D992EE718C7E99 * L_9 = V_0;
		int32_t L_10 = V_3;
		NullCheck(L_9);
		StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00 * L_11 = VirtFuncInvoker1< StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00 *, int32_t >::Invoke(5 /* System.Diagnostics.StackFrame System.Diagnostics.StackTrace::GetFrame(System.Int32) */, L_9, L_10);
		V_4 = L_11;
		// if (stackFrame.GetMethod().DeclaringType.ToString() == declaringType)
		StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00 * L_12 = V_4;
		NullCheck(L_12);
		MethodBase_t * L_13 = VirtFuncInvoker0< MethodBase_t * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_12);
		NullCheck(L_13);
		Type_t * L_14 = VirtFuncInvoker0< Type_t * >::Invoke(8 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_13);
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		String_t* L_16 = V_2;
		bool L_17 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_15, L_16, /*hidden argument*/NULL);
		V_6 = L_17;
		bool L_18 = V_6;
		if (!L_18)
		{
			goto IL_0068;
		}
	}
	{
		// result = "";    // only report the last called method within every class
		V_1 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
	}

IL_0068:
	{
		// declaringType = stackFrame.GetMethod().DeclaringType.ToString();
		StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00 * L_19 = V_4;
		NullCheck(L_19);
		MethodBase_t * L_20 = VirtFuncInvoker0< MethodBase_t * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_19);
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(8 /* System.Type System.Reflection.MemberInfo::get_DeclaringType() */, L_20);
		NullCheck(L_21);
		String_t* L_22 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		V_2 = L_22;
		// result += declaringType + ":" + stackFrame.GetMethod().Name;
		String_t* L_23 = V_1;
		String_t* L_24 = V_2;
		StackFrame_tD06959DD2B585E9BEB73C60AB5C110DE69F23A00 * L_25 = V_4;
		NullCheck(L_25);
		MethodBase_t * L_26 = VirtFuncInvoker0< MethodBase_t * >::Invoke(7 /* System.Reflection.MethodBase System.Diagnostics.StackFrame::GetMethod() */, L_25);
		NullCheck(L_26);
		String_t* L_27 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_26);
		String_t* L_28 = String_Concat_mDD2E38332DED3A8C088D38D78A0E0BEB5091DA64(L_23, L_24, _stringLiteral05A79F06CF3F67F726DAE68D18A2290F6C9A50C9, L_27, /*hidden argument*/NULL);
		V_1 = L_28;
		// for (int v = stackTrace.FrameCount - 1; v > level; v--)
		int32_t L_29 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_29, (int32_t)1));
	}

IL_0098:
	{
		// for (int v = stackTrace.FrameCount - 1; v > level; v--)
		int32_t L_30 = V_3;
		int32_t L_31 = ___level0;
		V_7 = (bool)((((int32_t)L_30) > ((int32_t)L_31))? 1 : 0);
		bool L_32 = V_7;
		if (L_32)
		{
			goto IL_001e;
		}
	}
	{
		// if (showOnlyLast)
		bool L_33 = ___showOnlyLast1;
		V_8 = L_33;
		bool L_34 = V_8;
		if (!L_34)
		{
			goto IL_00da;
		}
	}
	{
	}

IL_00ad:
	try
	{ // begin try (depth: 1)
		// result = result.Substring(result.LastIndexOf(" --> "));
		String_t* L_35 = V_1;
		String_t* L_36 = V_1;
		NullCheck(L_36);
		int32_t L_37 = String_LastIndexOf_mC924D20DC71F85A7106D9DD09BF41497C6816E20(L_36, _stringLiteral63EA88A95D11C675ECBFDF34069E492F0B9B2F1B, /*hidden argument*/NULL);
		NullCheck(L_35);
		String_t* L_38 = String_Substring_m2C4AFF5E79DD8BADFD2DFBCF156BF728FBB8E1AE(L_35, L_37, /*hidden argument*/NULL);
		V_1 = L_38;
		// result = result.Replace(" --> ", "");
		String_t* L_39 = V_1;
		NullCheck(L_39);
		String_t* L_40 = String_Replace_m970DFB0A280952FA7D3BA20AB7A8FB9F80CF6470(L_39, _stringLiteral63EA88A95D11C675ECBFDF34069E492F0B9B2F1B, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		V_1 = L_40;
		goto IL_00d9;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (RuntimeObject_il2cpp_TypeInfo_var, il2cpp_codegen_object_class(e.ex)))
			goto CATCH_00d4;
		throw e;
	}

CATCH_00d4:
	{ // begin catch(System.Object)
		// catch
		goto IL_00d9;
	} // end catch (depth: 1)

IL_00d9:
	{
	}

IL_00da:
	{
		// return result;
		String_t* L_41 = V_1;
		V_9 = L_41;
		goto IL_00df;
	}

IL_00df:
	{
		// }
		String_t* L_42 = V_9;
		return L_42;
	}
}
// System.String vp_Utility::GetTypeAlias(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* vp_Utility_GetTypeAlias_m0B105E1F6666B82F789B1ED67B46ED98EED4832B (Type_t * ___type0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_GetTypeAlias_m0B105E1F6666B82F789B1ED67B46ED98EED4832B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	String_t* V_2 = NULL;
	{
		// string s = "";
		V_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// if (!m_TypeAliases.TryGetValue(type, out s))
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_0 = ((vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields*)il2cpp_codegen_static_fields_for(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var))->get_m_TypeAliases_0();
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		bool L_2 = Dictionary_2_TryGetValue_mF96A70AF9B37F114A6DAC1FF67D1EB8C5072E739(L_0, L_1, (String_t**)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_mF96A70AF9B37F114A6DAC1FF67D1EB8C5072E739_RuntimeMethod_var);
		V_1 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		// return type.ToString();
		Type_t * L_4 = ___type0;
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		V_2 = L_5;
		goto IL_0028;
	}

IL_0024:
	{
		// return s;
		String_t* L_6 = V_0;
		V_2 = L_6;
		goto IL_0028;
	}

IL_0028:
	{
		// }
		String_t* L_7 = V_2;
		return L_7;
	}
}
// System.Void vp_Utility::Activate(UnityEngine.GameObject,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility_Activate_m1D822960AA0CE9BC101420C0E0C683D92A0F91F4 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj0, bool ___activate1, const RuntimeMethod* method)
{
	{
		// obj.SetActive(activate);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___obj0;
		bool L_1 = ___activate1;
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean vp_Utility::IsActive(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vp_Utility_IsActive_m774BACB2A81DCC0F782DFE3A6783917DF9EE2BC5 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj0, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// return obj.activeSelf;
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = ___obj0;
		NullCheck(L_0);
		bool L_1 = GameObject_get_activeSelf_mFE1834886CAE59884AC2BE707A3B821A1DB61F44(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000a;
	}

IL_000a:
	{
		// }
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean vp_Utility::get_LockCursor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vp_Utility_get_LockCursor_m856042AC6591FFF4A2D95322877B06E00C612030 (const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		// return ((Cursor.lockState == CursorLockMode.Locked) ? true : false);
		int32_t L_0 = Cursor_get_lockState_mE0C93F496E3AA120AD168ED30371C35ED79C9DF1(/*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 1;
	}

IL_000d:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0010;
	}

IL_0010:
	{
		// }
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void vp_Utility::set_LockCursor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility_set_LockCursor_mFB089E7E19E8FE41EADE38F7D5C743B7CCC97ADF (bool ___value0, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		// Cursor.visible = !value;
		bool L_0 = ___value0;
		Cursor_set_visible_mDB51E60B3D7B14873A6F5FBE5E0A432D4A46C431((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		// Cursor.lockState = (value ? CursorLockMode.Locked : CursorLockMode.None);
		bool L_1 = ___value0;
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = 1;
	}

IL_0012:
	{
		Cursor_set_lockState_m019E27A0FE021A28A1C672801416ACA5E770933F(G_B3_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean vp_Utility::IsDescendant(UnityEngine.Transform,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool vp_Utility_IsDescendant_m2383C0A44C9B19957CF9C867D00DF32E10CEEDAD (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___descendant0, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___potentialAncestor1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_IsDescendant_m2383C0A44C9B19957CF9C867D00DF32E10CEEDAD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	{
		// if (descendant == null)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___descendant0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_0058;
	}

IL_0010:
	{
		// if (potentialAncestor == null)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = ___potentialAncestor1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_2 = L_4;
		bool L_5 = V_2;
		if (!L_5)
		{
			goto IL_001f;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_0058;
	}

IL_001f:
	{
		// if (descendant.parent == descendant)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_6 = ___descendant0;
		NullCheck(L_6);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_6, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = ___descendant0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		bool L_10 = V_3;
		if (!L_10)
		{
			goto IL_0033;
		}
	}
	{
		// return false;
		V_1 = (bool)0;
		goto IL_0058;
	}

IL_0033:
	{
		// if (descendant.parent == potentialAncestor)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = ___descendant0;
		NullCheck(L_11);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_11, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = ___potentialAncestor1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_12, L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		bool L_15 = V_4;
		if (!L_15)
		{
			goto IL_0049;
		}
	}
	{
		// return true;
		V_1 = (bool)1;
		goto IL_0058;
	}

IL_0049:
	{
		// return IsDescendant(descendant.parent, potentialAncestor);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_16 = ___descendant0;
		NullCheck(L_16);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_17 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_16, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = ___potentialAncestor1;
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		bool L_19 = vp_Utility_IsDescendant_m2383C0A44C9B19957CF9C867D00DF32E10CEEDAD(L_17, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		goto IL_0058;
	}

IL_0058:
	{
		// }
		bool L_20 = V_1;
		return L_20;
	}
}
// UnityEngine.Component vp_Utility::GetParent(UnityEngine.Component)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * vp_Utility_GetParent_m23DF7AA74CBF3783F4D11A3C8159BCFD32C629C3 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_GetParent_m23DF7AA74CBF3783F4D11A3C8159BCFD32C629C3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * V_1 = NULL;
	bool V_2 = false;
	{
		// if (target == null)
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_0 = ___target0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		// return null;
		V_1 = (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)NULL;
		goto IL_0037;
	}

IL_0010:
	{
		// if (target != target.transform)
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_3 = ___target0;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_4 = ___target0;
		NullCheck(L_4);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_5 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_3, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0029;
		}
	}
	{
		// return target.transform;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_8 = ___target0;
		NullCheck(L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0037;
	}

IL_0029:
	{
		// return target.transform.parent;
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_10 = ___target0;
		NullCheck(L_10);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		goto IL_0037;
	}

IL_0037:
	{
		// }
		Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * L_13 = V_1;
		return L_13;
	}
}
// UnityEngine.Transform vp_Utility::GetTransformByNameInChildren(UnityEngine.Transform,System.String,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * vp_Utility_GetTransformByNameInChildren_m4BAEFE6AB1211944BBC646B73A3C319EA810BE8A (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trans0, String_t* ___name1, bool ___includeInactive2, bool ___subString3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_GetTransformByNameInChildren_m4BAEFE6AB1211944BBC646B73A3C319EA810BE8A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_1 = NULL;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_5 = NULL;
	bool V_6 = false;
	bool V_7 = false;
	RuntimeObject* V_8 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 4);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B16_0 = 0;
	int32_t G_B18_0 = 0;
	{
		// name = name.ToLower();
		String_t* L_0 = ___name1;
		NullCheck(L_0);
		String_t* L_1 = String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8(L_0, /*hidden argument*/NULL);
		___name1 = L_1;
		// foreach (Transform t in trans)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_2 = ___trans0;
		NullCheck(L_2);
		RuntimeObject* L_3 = Transform_GetEnumerator_mE98B6C5F644AE362EC1D58C10506327D6A5878FC(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00b2;
		}

IL_0016:
		{
			// foreach (Transform t in trans)
			RuntimeObject* L_4 = V_0;
			NullCheck(L_4);
			RuntimeObject * L_5 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_4);
			V_1 = ((Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)CastclassClass((RuntimeObject*)L_5, Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_il2cpp_TypeInfo_var));
			// if(!subString)
			bool L_6 = ___subString3;
			V_3 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
			bool L_7 = V_3;
			if (!L_7)
			{
				goto IL_0061;
			}
		}

IL_002b:
		{
			// if ((t.name.ToLower() == name) && ((includeInactive) || t.gameObject.activeInHierarchy))
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = V_1;
			NullCheck(L_8);
			String_t* L_9 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_8, /*hidden argument*/NULL);
			NullCheck(L_9);
			String_t* L_10 = String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8(L_9, /*hidden argument*/NULL);
			String_t* L_11 = ___name1;
			bool L_12 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_10, L_11, /*hidden argument*/NULL);
			if (!L_12)
			{
				goto IL_0052;
			}
		}

IL_003f:
		{
			bool L_13 = ___includeInactive2;
			if (L_13)
			{
				goto IL_004f;
			}
		}

IL_0042:
		{
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_14 = V_1;
			NullCheck(L_14);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_15 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_14, /*hidden argument*/NULL);
			NullCheck(L_15);
			bool L_16 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF(L_15, /*hidden argument*/NULL);
			G_B7_0 = ((int32_t)(L_16));
			goto IL_0050;
		}

IL_004f:
		{
			G_B7_0 = 1;
		}

IL_0050:
		{
			G_B9_0 = G_B7_0;
			goto IL_0053;
		}

IL_0052:
		{
			G_B9_0 = 0;
		}

IL_0053:
		{
			V_4 = (bool)G_B9_0;
			bool L_17 = V_4;
			if (!L_17)
			{
				goto IL_005e;
			}
		}

IL_0059:
		{
			// return t;
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_18 = V_1;
			V_5 = L_18;
			IL2CPP_LEAVE(0xD9, FINALLY_00bf);
		}

IL_005e:
		{
			goto IL_0095;
		}

IL_0061:
		{
			// if ((t.name.ToLower().Contains(name)) && ((includeInactive) || t.gameObject.activeInHierarchy))
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = V_1;
			NullCheck(L_19);
			String_t* L_20 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_19, /*hidden argument*/NULL);
			NullCheck(L_20);
			String_t* L_21 = String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8(L_20, /*hidden argument*/NULL);
			String_t* L_22 = ___name1;
			NullCheck(L_21);
			bool L_23 = String_Contains_m4488034AF8CB3EEA9A205EB8A1F25D438FF8704B(L_21, L_22, /*hidden argument*/NULL);
			if (!L_23)
			{
				goto IL_0088;
			}
		}

IL_0075:
		{
			bool L_24 = ___includeInactive2;
			if (L_24)
			{
				goto IL_0085;
			}
		}

IL_0078:
		{
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_25 = V_1;
			NullCheck(L_25);
			GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_26 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_25, /*hidden argument*/NULL);
			NullCheck(L_26);
			bool L_27 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF(L_26, /*hidden argument*/NULL);
			G_B16_0 = ((int32_t)(L_27));
			goto IL_0086;
		}

IL_0085:
		{
			G_B16_0 = 1;
		}

IL_0086:
		{
			G_B18_0 = G_B16_0;
			goto IL_0089;
		}

IL_0088:
		{
			G_B18_0 = 0;
		}

IL_0089:
		{
			V_6 = (bool)G_B18_0;
			bool L_28 = V_6;
			if (!L_28)
			{
				goto IL_0094;
			}
		}

IL_008f:
		{
			// return t;
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_29 = V_1;
			V_5 = L_29;
			IL2CPP_LEAVE(0xD9, FINALLY_00bf);
		}

IL_0094:
		{
		}

IL_0095:
		{
			// Transform ct = GetTransformByNameInChildren(t, name, includeInactive, subString);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_30 = V_1;
			String_t* L_31 = ___name1;
			bool L_32 = ___includeInactive2;
			bool L_33 = ___subString3;
			IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_34 = vp_Utility_GetTransformByNameInChildren_m4BAEFE6AB1211944BBC646B73A3C319EA810BE8A(L_30, L_31, L_32, L_33, /*hidden argument*/NULL);
			V_2 = L_34;
			// if (ct != null)
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_35 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
			bool L_36 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_35, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
			V_7 = L_36;
			bool L_37 = V_7;
			if (!L_37)
			{
				goto IL_00b1;
			}
		}

IL_00ac:
		{
			// return ct;
			Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_38 = V_2;
			V_5 = L_38;
			IL2CPP_LEAVE(0xD9, FINALLY_00bf);
		}

IL_00b1:
		{
		}

IL_00b2:
		{
			// foreach (Transform t in trans)
			RuntimeObject* L_39 = V_0;
			NullCheck(L_39);
			bool L_40 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A_il2cpp_TypeInfo_var, L_39);
			if (L_40)
			{
				goto IL_0016;
			}
		}

IL_00bd:
		{
			IL2CPP_LEAVE(0xD4, FINALLY_00bf);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00bf;
	}

FINALLY_00bf:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_41 = V_0;
			V_8 = ((RuntimeObject*)IsInst((RuntimeObject*)L_41, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var));
			RuntimeObject* L_42 = V_8;
			if (!L_42)
			{
				goto IL_00d3;
			}
		}

IL_00cb:
		{
			RuntimeObject* L_43 = V_8;
			NullCheck(L_43);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t7218B22548186B208D65EA5B7870503810A2D15A_il2cpp_TypeInfo_var, L_43);
		}

IL_00d3:
		{
			IL2CPP_END_FINALLY(191)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(191)
	{
		IL2CPP_JUMP_TBL(0xD9, IL_00d9)
		IL2CPP_JUMP_TBL(0xD4, IL_00d4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00d4:
	{
		// return null;
		V_5 = (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)NULL;
		goto IL_00d9;
	}

IL_00d9:
	{
		// }
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_44 = V_5;
		return L_44;
	}
}
// UnityEngine.Transform vp_Utility::GetTransformByNameInAncestors(UnityEngine.Transform,System.String,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * vp_Utility_GetTransformByNameInAncestors_m85889B83D8D4B16EFBB1C417E7964480C825AEE4 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___trans0, String_t* ___name1, bool ___includeInactive2, bool ___subString3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_GetTransformByNameInAncestors_m85889B83D8D4B16EFBB1C417E7964480C825AEE4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_0 = NULL;
	bool V_1 = false;
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * V_2 = NULL;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	int32_t G_B16_0 = 0;
	int32_t G_B18_0 = 0;
	{
		// if (trans.parent == null)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = ___trans0;
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_1 = L_2;
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		// return null;
		V_2 = (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)NULL;
		goto IL_00c8;
	}

IL_0018:
	{
		// name = name.ToLower();
		String_t* L_4 = ___name1;
		NullCheck(L_4);
		String_t* L_5 = String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8(L_4, /*hidden argument*/NULL);
		___name1 = L_5;
		// if(!subString)
		bool L_6 = ___subString3;
		V_3 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_3;
		if (!L_7)
		{
			goto IL_0067;
		}
	}
	{
		// if ((trans.parent.name.ToLower() == name) && ((includeInactive) || trans.gameObject.activeInHierarchy))
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = ___trans0;
		NullCheck(L_8);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_9 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		String_t* L_11 = String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8(L_10, /*hidden argument*/NULL);
		String_t* L_12 = ___name1;
		bool L_13 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_11, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0054;
		}
	}
	{
		bool L_14 = ___includeInactive2;
		if (L_14)
		{
			goto IL_0051;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_15 = ___trans0;
		NullCheck(L_15);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_16 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		bool L_17 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF(L_16, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_17));
		goto IL_0052;
	}

IL_0051:
	{
		G_B7_0 = 1;
	}

IL_0052:
	{
		G_B9_0 = G_B7_0;
		goto IL_0055;
	}

IL_0054:
	{
		G_B9_0 = 0;
	}

IL_0055:
	{
		V_4 = (bool)G_B9_0;
		bool L_18 = V_4;
		if (!L_18)
		{
			goto IL_0064;
		}
	}
	{
		// return trans.parent;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_19 = ___trans0;
		NullCheck(L_19);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_20 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		goto IL_00c8;
	}

IL_0064:
	{
		goto IL_00a4;
	}

IL_0067:
	{
		// if ((trans.parent.name.ToLower().Contains(name)) && ((includeInactive) || trans.gameObject.activeInHierarchy))
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_21 = ___trans0;
		NullCheck(L_21);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_22 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		String_t* L_23 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24 = String_ToLower_m5287204D93C9DDC4DF84581ADD756D0FDE2BA5A8(L_23, /*hidden argument*/NULL);
		String_t* L_25 = ___name1;
		NullCheck(L_24);
		bool L_26 = String_Contains_m4488034AF8CB3EEA9A205EB8A1F25D438FF8704B(L_24, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0093;
		}
	}
	{
		bool L_27 = ___includeInactive2;
		if (L_27)
		{
			goto IL_0090;
		}
	}
	{
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_28 = ___trans0;
		NullCheck(L_28);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_29 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		bool L_30 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF(L_29, /*hidden argument*/NULL);
		G_B16_0 = ((int32_t)(L_30));
		goto IL_0091;
	}

IL_0090:
	{
		G_B16_0 = 1;
	}

IL_0091:
	{
		G_B18_0 = G_B16_0;
		goto IL_0094;
	}

IL_0093:
	{
		G_B18_0 = 0;
	}

IL_0094:
	{
		V_5 = (bool)G_B18_0;
		bool L_31 = V_5;
		if (!L_31)
		{
			goto IL_00a3;
		}
	}
	{
		// return trans.parent;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_32 = ___trans0;
		NullCheck(L_32);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_33 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_32, /*hidden argument*/NULL);
		V_2 = L_33;
		goto IL_00c8;
	}

IL_00a3:
	{
	}

IL_00a4:
	{
		// Transform ct = GetTransformByNameInAncestors(trans.parent, name, includeInactive, subString);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_34 = ___trans0;
		NullCheck(L_34);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_35 = Transform_get_parent_m8FA24E38A1FA29D90CBF3CDC9F9F017C65BB3403(L_34, /*hidden argument*/NULL);
		String_t* L_36 = ___name1;
		bool L_37 = ___includeInactive2;
		bool L_38 = ___subString3;
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_39 = vp_Utility_GetTransformByNameInAncestors_m85889B83D8D4B16EFBB1C417E7964480C825AEE4(L_35, L_36, L_37, L_38, /*hidden argument*/NULL);
		V_0 = L_39;
		// if (ct != null)
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_40 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_40, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		V_6 = L_41;
		bool L_42 = V_6;
		if (!L_42)
		{
			goto IL_00c4;
		}
	}
	{
		// return ct;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_43 = V_0;
		V_2 = L_43;
		goto IL_00c8;
	}

IL_00c4:
	{
		// return null;
		V_2 = (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *)NULL;
		goto IL_00c8;
	}

IL_00c8:
	{
		// }
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_44 = V_2;
		return L_44;
	}
}
// UnityEngine.Object vp_Utility::Instantiate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * vp_Utility_Instantiate_m40BDAB73E95DD2EA41F4C96D424CFE9E08B4E30E (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___original0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_Instantiate_m40BDAB73E95DD2EA41F4C96D424CFE9E08B4E30E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * V_0 = NULL;
	{
		// return vp_Utility.Instantiate(original, Vector3.zero, Quaternion.identity);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_2 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_3 = vp_Utility_Instantiate_mB3FC1BB39D06ACF32E0409CAA385DDB0915B65E7(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		// }
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Object vp_Utility::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * vp_Utility_Instantiate_mB3FC1BB39D06ACF32E0409CAA385DDB0915B65E7 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_Instantiate_mB3FC1BB39D06ACF32E0409CAA385DDB0915B65E7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * V_1 = NULL;
	int32_t G_B4_0 = 0;
	{
		// if (vp_PoolManager.Instance == null || !vp_PoolManager.Instance.enabled || !(original is GameObject))
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * L_0 = vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * L_2 = vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076(/*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_mAA0C9ED5A3D1589C1C8AA22636543528DB353CFB(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_4 = ___original0;
		G_B4_0 = ((((int32_t)((!(((RuntimeObject*)(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_4, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0029;
	}

IL_0028:
	{
		G_B4_0 = 1;
	}

IL_0029:
	{
		V_0 = (bool)G_B4_0;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		// UnityEngine.Debug.Log("Can'y instantiate by pool");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral54F1C3D7731A3EF440BD8912D713A59E97E280A5, /*hidden argument*/NULL);
		// return GameObject.Instantiate(original, position, rotation);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_6 = ___original0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = ___position1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_8 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_9 = Object_Instantiate_mAF9C2662167396DEE640598515B60BE41B9D5082(L_6, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0055;
	}

IL_0044:
	{
		// return vp_PoolManager.Spawn((original as GameObject), position, rotation);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_10 = ___original0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = ___position1;
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_12 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_13 = vp_PoolManager_Spawn_mCC73C86FEAD328B569F91955193CD4FB0299867B(((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_10, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var)), L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		goto IL_0055;
	}

IL_0055:
	{
		// }
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_14 = V_1;
		return L_14;
	}
}
// System.Void vp_Utility::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility_Destroy_m4AE89615D99B095E1DAAF80E913F5AA6B9C4539F (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_Destroy_m4AE89615D99B095E1DAAF80E913F5AA6B9C4539F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B4_0 = 0;
	{
		// if (vp_PoolManager.Instance == null || !vp_PoolManager.Instance.enabled || !(obj is GameObject))
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * L_0 = vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0028;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * L_2 = vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076(/*hidden argument*/NULL);
		NullCheck(L_2);
		bool L_3 = Behaviour_get_enabled_mAA0C9ED5A3D1589C1C8AA22636543528DB353CFB(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_4 = ___obj0;
		G_B4_0 = ((((int32_t)((!(((RuntimeObject*)(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_4, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0029;
	}

IL_0028:
	{
		G_B4_0 = 1;
	}

IL_0029:
	{
		V_0 = (bool)G_B4_0;
		bool L_5 = V_0;
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		// UnityEngine.Object.Destroy(obj);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_6 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_6, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_0036:
	{
		// vp_PoolManager.Despawn(obj as GameObject);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_7 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		vp_PoolManager_Despawn_mC1BC1EA7AD2EDDA5739DF4AD58ED8050FAC4E5DF(((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_7, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void vp_Utility::Destroy(UnityEngine.Object,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility_Destroy_mEF5255E8C4A8FA62DC4D9C33A1FB6F48B7B22B27 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, float ___t1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_Destroy_mEF5255E8C4A8FA62DC4D9C33A1FB6F48B7B22B27_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B4_0 = 0;
	{
		U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * L_0 = (U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass18_0__ctor_m8FAC57006AAA255FFF14C50AEDCE44E0836232BA(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * L_1 = V_0;
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_2 = ___obj0;
		NullCheck(L_1);
		L_1->set_obj_0(L_2);
		U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * L_3 = V_0;
		float L_4 = ___t1;
		NullCheck(L_3);
		L_3->set_t_1(L_4);
		// if (vp_PoolManager.Instance == null || !vp_PoolManager.Instance.enabled || !(obj is GameObject))
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * L_5 = vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_5, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D * L_7 = vp_PoolManager_get_Instance_mD3FC73051C8AFACD2F2827D848ACE63BB3E1E076(/*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = Behaviour_get_enabled_mAA0C9ED5A3D1589C1C8AA22636543528DB353CFB(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0041;
		}
	}
	{
		U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * L_9 = V_0;
		NullCheck(L_9);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_10 = L_9->get_obj_0();
		G_B4_0 = ((((int32_t)((!(((RuntimeObject*)(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_10, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0042;
	}

IL_0041:
	{
		G_B4_0 = 1;
	}

IL_0042:
	{
		V_1 = (bool)G_B4_0;
		bool L_11 = V_1;
		if (!L_11)
		{
			goto IL_005a;
		}
	}
	{
		// UnityEngine.Object.Destroy(obj, t);
		U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * L_12 = V_0;
		NullCheck(L_12);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_13 = L_12->get_obj_0();
		U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * L_14 = V_0;
		NullCheck(L_14);
		float L_15 = L_14->get_t_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m09F51D8BDECFD2E8C618498EF7377029B669030D(L_13, L_15, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_005a:
	{
		// vp_Timer.In(Mathf.Max(0, t), () =>
		//     {
		//         vp_PoolManager.Despawn(obj as GameObject, t);
		//     });
		U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * L_16 = V_0;
		NullCheck(L_16);
		float L_17 = L_16->get_t_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Max_m670AE0EC1B09ED1A56FF9606B0F954670319CB65((0.0f), L_17, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * L_19 = V_0;
		Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 * L_20 = (Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6 *)il2cpp_codegen_object_new(Callback_t8CBC8E4EC6CE257F696015F16AB883A2789C92E6_il2cpp_TypeInfo_var);
		Callback__ctor_m56FE6C8553E18184859AD3DF01727565DF7F910C(L_20, L_19, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass18_0_U3CDestroyU3Eb__0_m325D201A9C46B0BEAA9E5AEC6E86866FCF616943_RuntimeMethod_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(vp_Timer_tCE434F91725D99311F213E4BDB9FDAC480CC876C_il2cpp_TypeInfo_var);
		vp_Timer_In_m6C38B0240730B0B7637A6ABE382178AD62AE600D(L_18, L_20, (Handle_tE5B8C2AB0FABD69A713B2B97F05942C88F8E0381 *)NULL, /*hidden argument*/NULL);
	}

IL_007f:
	{
		// }
		return;
	}
}
// System.Int32 vp_Utility::get_UniqueID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t vp_Utility_get_UniqueID_mAE63167533A8BA058CFD0BFE90DF746219A3384F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_get_UniqueID_mAE63167533A8BA058CFD0BFE90DF746219A3384F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	{
	}

IL_0001:
	{
		// i = UnityEngine.Random.Range(0, 1000000000);
		int32_t L_0 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(0, ((int32_t)1000000000), /*hidden argument*/NULL);
		V_0 = L_0;
		// if (m_UniqueIDs.ContainsKey(i))    // likely won't happen (ever)
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * L_1 = ((vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields*)il2cpp_codegen_static_fields_for(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var))->get_m_UniqueIDs_1();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		bool L_3 = Dictionary_2_ContainsKey_mBC44DAB119CBE7A4B7820ABF12AD024A4654471E(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_mBC44DAB119CBE7A4B7820ABF12AD024A4654471E_RuntimeMethod_var);
		V_1 = L_3;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		// if (m_UniqueIDs.Count >= 1000000000)
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * L_5 = ((vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields*)il2cpp_codegen_static_fields_for(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var))->get_m_UniqueIDs_1();
		NullCheck(L_5);
		int32_t L_6 = Dictionary_2_get_Count_m73CF0E63105DEFE34A611344CF2C01B27AF83FFB(L_5, /*hidden argument*/Dictionary_2_get_Count_m73CF0E63105DEFE34A611344CF2C01B27AF83FFB_RuntimeMethod_var);
		V_2 = (bool)((((int32_t)((((int32_t)L_6) < ((int32_t)((int32_t)1000000000)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		// ClearUniqueIDs();
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		vp_Utility_ClearUniqueIDs_m559A9F7D03CF00423358623E578D00A918B328DC(/*hidden argument*/NULL);
		// UnityEngine.Debug.LogWarning("Warning (vp_Utility.UniqueID) More than 1 billion unique IDs have been generated. This seems like an awful lot for a game client. Clearing dictionary and starting over!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568(_stringLiteral401420FC0A7AF7CF7534FB9E78DD9FF06E9D0808, /*hidden argument*/NULL);
	}

IL_0049:
	{
		// goto reroll;
		goto IL_0001;
	}

IL_004b:
	{
		// m_UniqueIDs.Add(i, 0);
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * L_8 = ((vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields*)il2cpp_codegen_static_fields_for(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var))->get_m_UniqueIDs_1();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		Dictionary_2_Add_mDC25A2F4744B497C39F127D97327CC7B56E68903(L_8, L_9, 0, /*hidden argument*/Dictionary_2_Add_mDC25A2F4744B497C39F127D97327CC7B56E68903_RuntimeMethod_var);
		// return i;
		int32_t L_10 = V_0;
		V_3 = L_10;
		goto IL_005c;
	}

IL_005c:
	{
		// }
		int32_t L_11 = V_3;
		return L_11;
	}
}
// System.Void vp_Utility::ClearUniqueIDs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility_ClearUniqueIDs_m559A9F7D03CF00423358623E578D00A918B328DC (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_ClearUniqueIDs_m559A9F7D03CF00423358623E578D00A918B328DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// m_UniqueIDs.Clear();
		IL2CPP_RUNTIME_CLASS_INIT(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var);
		Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * L_0 = ((vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields*)il2cpp_codegen_static_fields_for(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var))->get_m_UniqueIDs_1();
		NullCheck(L_0);
		Dictionary_2_Clear_m7DE8E346650F09FFAB65ABCACFEBCB2FAB2F4499(L_0, /*hidden argument*/Dictionary_2_Clear_m7DE8E346650F09FFAB65ABCACFEBCB2FAB2F4499_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Int32 vp_Utility::PositionToID(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t vp_Utility_PositionToID_m872F990B5C216FA657F53092D380CE04F341467A (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility_PositionToID_m872F990B5C216FA657F53092D380CE04F341467A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// return (int)Mathf.Abs(
		//       (position.x * 10000)
		//     + (position.y * 1000)
		//     + (position.z * 100));
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = ___position0;
		float L_1 = L_0.get_x_2();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = ___position0;
		float L_3 = L_2.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = ___position0;
		float L_5 = L_4.get_z_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_6 = fabsf(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)(10000.0f))), (float)((float)il2cpp_codegen_multiply((float)L_3, (float)(1000.0f))))), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)(100.0f))))));
		V_0 = (((int32_t)((int32_t)L_6)));
		goto IL_0030;
	}

IL_0030:
	{
		// }
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Void vp_Utility::PlayRandomSound(UnityEngine.AudioSource,System.Collections.Generic.List`1<UnityEngine.AudioClip>,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility_PlayRandomSound_mD2386BAC0DBFDC710BD1D921264DF06C8002BB52 (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource0, List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * ___sounds1, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___pitchRange2, const RuntimeMethod* method)
{
	{
		// vp_AudioUtility.PlayRandomSound(audioSource, sounds, pitchRange);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_0 = ___audioSource0;
		List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * L_1 = ___sounds1;
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_2 = ___pitchRange2;
		vp_AudioUtility_PlayRandomSound_m718D90E2F96884AE683DB5C6238BC081A688D679(L_0, L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void vp_Utility::PlayRandomSound(UnityEngine.AudioSource,System.Collections.Generic.List`1<UnityEngine.AudioClip>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility_PlayRandomSound_m963E0566C1B780DA5B3025C846FE02239E98AC2B (AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource0, List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * ___sounds1, const RuntimeMethod* method)
{
	{
		// vp_AudioUtility.PlayRandomSound(audioSource, sounds);
		AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * L_0 = ___audioSource0;
		List_1_tACBFA0BC3113DBE0B10BD340D1E5E4F403D98EB1 * L_1 = ___sounds1;
		vp_AudioUtility_PlayRandomSound_mB4EBABD957F1B1A623B11BA162A9D5CF44544B85(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Sprite vp_Utility::GetSprite(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * vp_Utility_GetSprite_m269DD9BE9682187F7237CA44522DB667D6373F69 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, const RuntimeMethod* method)
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * V_0 = NULL;
	{
		// return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_0 = ___texture0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_1 = ___texture0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_1);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_3 = ___texture0;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_3);
		Rect_t35B976DE901B5423C11705E156938EA27AB402CE  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Rect__ctor_m50B92C75005C9C5A0D05E6E0EBB43AFAF7C66280((&L_5), (0.0f), (0.0f), (((float)((float)L_2))), (((float)((float)L_4))), /*hidden argument*/NULL);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_mEE8FB117AB1F8DB746FB8B3EB4C0DA3BF2A230D0((&L_6), (0.5f), (0.5f), /*hidden argument*/NULL);
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_7 = Sprite_Create_m84A724DB0F0D73AEBE5296B4324D61EBCA72A843(L_0, L_5, L_6, (100.0f), /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_003b;
	}

IL_003b:
	{
		// }
		Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * L_8 = V_0;
		return L_8;
	}
}
// System.Void vp_Utility::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_Utility__cctor_m9B1303501DD378DBFA457D8B8A5C3F0F3AE0E5B5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_Utility__cctor_m9B1303501DD378DBFA457D8B8A5C3F0F3AE0E5B5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * V_0 = NULL;
	{
		// private static readonly Dictionary<Type, string> m_TypeAliases = new Dictionary<Type, string>()
		// {
		// 
		//     { typeof(void), "void" },
		//     { typeof(byte), "byte" },
		//     { typeof(sbyte), "sbyte" },
		//     { typeof(short), "short" },
		//     { typeof(ushort), "ushort" },
		//     { typeof(int), "int" },
		//     { typeof(uint), "uint" },
		//     { typeof(long), "long" },
		//     { typeof(ulong), "ulong" },
		//     { typeof(float), "float" },
		//     { typeof(double), "double" },
		//     { typeof(decimal), "decimal" },
		//     { typeof(object), "object" },
		//     { typeof(bool), "bool" },
		//     { typeof(char), "char" },
		//     { typeof(string), "string" },
		//     { typeof(UnityEngine.Vector2), "Vector2" },
		//     { typeof(UnityEngine.Vector3), "Vector3" },
		//     { typeof(UnityEngine.Vector4), "Vector4" }
		// 
		// };
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_0 = (Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 *)il2cpp_codegen_object_new(Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mDE30E1D450337479C15FB7C79523406548F2D396(L_0, /*hidden argument*/Dictionary_2__ctor_mDE30E1D450337479C15FB7C79523406548F2D396_RuntimeMethod_var);
		V_0 = L_0;
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_1 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_2 = { reinterpret_cast<intptr_t> (Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_1, L_3, _stringLiteralE9CEDE9B80EA3ABD89C755F1117337D429162C86, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_4 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_5 = { reinterpret_cast<intptr_t> (Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07_0_0_0_var) };
		Type_t * L_6 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_4, L_6, _stringLiteral8CF1783FA99F62CA581F6FE8F3CD66B0F9AB9FC3, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_7 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_8 = { reinterpret_cast<intptr_t> (SByte_t9070AEA2966184235653CB9B4D33B149CDA831DF_0_0_0_var) };
		Type_t * L_9 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_7, L_9, _stringLiteral681D15313A93F5907C45ABC0E6A97EE59E13FABA, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_10 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_11 = { reinterpret_cast<intptr_t> (Int16_t823A20635DAF5A3D93A1E01CFBF3CBA27CF00B4D_0_0_0_var) };
		Type_t * L_12 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_10, L_12, _stringLiteralA0F4EA7D91495DF92BBAC2E2149DFB850FE81396, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_13 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_14 = { reinterpret_cast<intptr_t> (UInt16_tAE45CEF73BF720100519F6867F32145D075F928E_0_0_0_var) };
		Type_t * L_15 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_13, L_15, _stringLiteral42CD4CC09795FD30CE71C875DA59944B9099B15F, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_16 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_17 = { reinterpret_cast<intptr_t> (Int32_t585191389E07734F19F3156FF88FB3EF4800D102_0_0_0_var) };
		Type_t * L_18 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_16, L_18, _stringLiteral46F8AB7C0CFF9DF7CD124852E26022A6BF89E315, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_19 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_20 = { reinterpret_cast<intptr_t> (UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B_0_0_0_var) };
		Type_t * L_21 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_19, L_21, _stringLiteralFCDBC880C3D7454F7AED7A672C34EBC2395648A2, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_22 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_23 = { reinterpret_cast<intptr_t> (Int64_t7A386C2FF7B0280A0F516992401DDFCF0FF7B436_0_0_0_var) };
		Type_t * L_24 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_22, L_24, _stringLiteralBD3027FA569EA15CA76D84DB21C67E2D514C1A5A, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_25 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_26 = { reinterpret_cast<intptr_t> (UInt64_tA02DF3B59C8FC4A849BD207DA11038CC64E4CB4E_0_0_0_var) };
		Type_t * L_27 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_25, L_27, _stringLiteralEBABC9B6E6F06B857E70FF9361A38C584DAFDB10, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_28 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_29 = { reinterpret_cast<intptr_t> (Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1_0_0_0_var) };
		Type_t * L_30 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_28, L_30, _stringLiteral685E80366130387CB75C055248326976D16FDF8D, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_31 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_32 = { reinterpret_cast<intptr_t> (Double_t358B8F23BDC52A5DD700E727E204F9F7CDE12409_0_0_0_var) };
		Type_t * L_33 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_31, L_33, _stringLiteralBDB36BB22DEB169275B3094BA9005A29EEDDD195, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_34 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_35 = { reinterpret_cast<intptr_t> (Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_0_0_0_var) };
		Type_t * L_36 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_35, /*hidden argument*/NULL);
		NullCheck(L_34);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_34, L_36, _stringLiteral45932D6FA98F39C5CD3F08CD951D8DC70FC5F7DE, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_37 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_38 = { reinterpret_cast<intptr_t> (RuntimeObject_0_0_0_var) };
		Type_t * L_39 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_38, /*hidden argument*/NULL);
		NullCheck(L_37);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_37, L_39, _stringLiteral1615307CC4523F183E777DF67F168C86908E8007, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_40 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_41 = { reinterpret_cast<intptr_t> (Boolean_tB53F6830F670160873277339AA58F15CAED4399C_0_0_0_var) };
		Type_t * L_42 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_41, /*hidden argument*/NULL);
		NullCheck(L_40);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_40, L_42, _stringLiteral5039D155A71C0A5F7A2B2654AD49CB7EE47A8980, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_43 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_44 = { reinterpret_cast<intptr_t> (Char_tBF22D9FC341BE970735250BB6FF1A4A92BBA58B9_0_0_0_var) };
		Type_t * L_45 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_44, /*hidden argument*/NULL);
		NullCheck(L_43);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_43, L_45, _stringLiteral71FAFC4E2FC1E47E234762A96B80512B6B5534C2, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_46 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_47 = { reinterpret_cast<intptr_t> (String_t_0_0_0_var) };
		Type_t * L_48 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_47, /*hidden argument*/NULL);
		NullCheck(L_46);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_46, L_48, _stringLiteralECB252044B5EA0F679EE78EC1A12904739E2904D, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_49 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_50 = { reinterpret_cast<intptr_t> (Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_0_0_0_var) };
		Type_t * L_51 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_50, /*hidden argument*/NULL);
		NullCheck(L_49);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_49, L_51, _stringLiteralC476FD00CCF30C5701DE9368C551434D10BB9289, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_52 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_53 = { reinterpret_cast<intptr_t> (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_0_0_0_var) };
		Type_t * L_54 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_53, /*hidden argument*/NULL);
		NullCheck(L_52);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_52, L_54, _stringLiteral7CFE65A1B11B9CEEBE65FE095DDF9C08B4D8C723, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_55 = V_0;
		RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  L_56 = { reinterpret_cast<intptr_t> (Vector4_tD148D6428C3F8FF6CD998F82090113C2B490B76E_0_0_0_var) };
		Type_t * L_57 = Type_GetTypeFromHandle_m9DC58ADF0512987012A8A016FB64B068F3B1AFF6(L_56, /*hidden argument*/NULL);
		NullCheck(L_55);
		Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA(L_55, L_57, _stringLiteral134F2C5A8EFE6B2B246EADA810862AAAB6969E63, /*hidden argument*/Dictionary_2_Add_m79F3D2C6C640CE9421D0BCB894627B7706DE5FCA_RuntimeMethod_var);
		Dictionary_2_t8C62DC5343FD5E7FCDAF3EF8D82B0B2EB4AB9FC8 * L_58 = V_0;
		((vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields*)il2cpp_codegen_static_fields_for(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var))->set_m_TypeAliases_0(L_58);
		// private static Dictionary<int, int> m_UniqueIDs = new Dictionary<int, int>();
		Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 * L_59 = (Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8 *)il2cpp_codegen_object_new(Dictionary_2_tFE2A3F3BDE1290B85039D74816BB1FE1109BE0F8_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m5B4A5BC34051AAA3F4C9590CF249F3A095332161(L_59, /*hidden argument*/Dictionary_2__ctor_m5B4A5BC34051AAA3F4C9590CF249F3A095332161_RuntimeMethod_var);
		((vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_StaticFields*)il2cpp_codegen_static_fields_for(vp_Utility_t2345C89AC7773E3E85B59701A3F4450E94A0BBC0_il2cpp_TypeInfo_var))->set_m_UniqueIDs_1(L_59);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_Utility_<>c__DisplayClass18_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass18_0__ctor_m8FAC57006AAA255FFF14C50AEDCE44E0836232BA (U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void vp_Utility_<>c__DisplayClass18_0::<Destroy>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass18_0_U3CDestroyU3Eb__0_m325D201A9C46B0BEAA9E5AEC6E86866FCF616943 (U3CU3Ec__DisplayClass18_0_t8B24192DBE1FF65F61CF822866369353593D473E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass18_0_U3CDestroyU3Eb__0_m325D201A9C46B0BEAA9E5AEC6E86866FCF616943_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// vp_PoolManager.Despawn(obj as GameObject, t);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_0 = __this->get_obj_0();
		float L_1 = __this->get_t_1();
		IL2CPP_RUNTIME_CLASS_INIT(vp_PoolManager_t15CB033DE3A22865D622AADEC6B75A4F328A5F9D_il2cpp_TypeInfo_var);
		vp_PoolManager_Despawn_m9870CB2ADCBE296E93D5EF5D220359A2FD2FCE37(((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)IsInstSealed((RuntimeObject*)L_0, GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F_il2cpp_TypeInfo_var)), L_1, /*hidden argument*/NULL);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void vp_WaypointGizmo::OnDrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_WaypointGizmo_OnDrawGizmos_m9CF12CE032519F0999581DCA4C6755F0ACA65028 (vp_WaypointGizmo_tE9568B03FC967DCFB790A051841EB56CD5D77FBB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_WaypointGizmo_OnDrawGizmos_m9CF12CE032519F0999581DCA4C6755F0ACA65028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Gizmos.matrix = transform.localToWorldMatrix;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_1 = Transform_get_localToWorldMatrix_mBC86B8C7BA6F53DAB8E0120D77729166399A0EED(L_0, /*hidden argument*/NULL);
		Gizmos_set_matrix_mA3B65EC8681EDF68BDD2A657F4B67C00C1C34565(L_1, /*hidden argument*/NULL);
		// Gizmos.color = m_GizmoColor;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_2 = __this->get_m_GizmoColor_4();
		Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28(L_2, /*hidden argument*/NULL);
		// Gizmos.DrawCube(Vector3.zero,Vector3.one);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB(/*hidden argument*/NULL);
		Gizmos_DrawCube_m55519F7455796C0858AE0D0FD7BC2AA62138957E(L_3, L_4, /*hidden argument*/NULL);
		// Gizmos.color = new Color(0f,0f,0f,1f);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((&L_5), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28(L_5, /*hidden argument*/NULL);
		// Gizmos.DrawLine(Vector3.zero,Vector3.forward);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		Gizmos_DrawLine_m9515D59D2536571F4906A3C54E613A3986DFD892(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void vp_WaypointGizmo::OnDrawGizmosSelected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_WaypointGizmo_OnDrawGizmosSelected_m5424ACE12543A130DF01A80B4079580B9C37A740 (vp_WaypointGizmo_tE9568B03FC967DCFB790A051841EB56CD5D77FBB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (vp_WaypointGizmo_OnDrawGizmosSelected_m5424ACE12543A130DF01A80B4079580B9C37A740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Gizmos.matrix = transform.localToWorldMatrix;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Matrix4x4_t6BF60F70C9169DF14C9D2577672A44224B236ECA  L_1 = Transform_get_localToWorldMatrix_mBC86B8C7BA6F53DAB8E0120D77729166399A0EED(L_0, /*hidden argument*/NULL);
		Gizmos_set_matrix_mA3B65EC8681EDF68BDD2A657F4B67C00C1C34565(L_1, /*hidden argument*/NULL);
		// Gizmos.color = m_SelectedGizmoColor;
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_2 = __this->get_m_SelectedGizmoColor_5();
		Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28(L_2, /*hidden argument*/NULL);
		// Gizmos.DrawCube(Vector3.zero,Vector3.one);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_get_one_mA11B83037CB269C6076CBCF754E24C8F3ACEC2AB(/*hidden argument*/NULL);
		Gizmos_DrawCube_m55519F7455796C0858AE0D0FD7BC2AA62138957E(L_3, L_4, /*hidden argument*/NULL);
		// Gizmos.color = new Color(0f,0f,0f,1f);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_5;
		memset((&L_5), 0, sizeof(L_5));
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((&L_5), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		Gizmos_set_color_mFA6C199DF05FF557AEF662222CA60EC25DF54F28(L_5, /*hidden argument*/NULL);
		// Gizmos.DrawLine(Vector3.zero,Vector3.forward);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_get_forward_m3E2E192B3302130098738C308FA1EE1439449D0D(/*hidden argument*/NULL);
		Gizmos_DrawLine_m9515D59D2536571F4906A3C54E613A3986DFD892(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void vp_WaypointGizmo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void vp_WaypointGizmo__ctor_m6C7A345F5F27B690591D1B7EF78FD13769B8A25B (vp_WaypointGizmo_tE9568B03FC967DCFB790A051841EB56CD5D77FBB * __this, const RuntimeMethod* method)
{
	{
		// protected Color m_GizmoColor = new Color(1f,1f,1f,.4f);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m20DF490CEB364C4FC36D7EE392640DF5B7420D7C((&L_0), (1.0f), (1.0f), (1.0f), (0.4f), /*hidden argument*/NULL);
		__this->set_m_GizmoColor_4(L_0);
		// protected Color m_SelectedGizmoColor = new Color32(160, 255, 100, 100);
		Color32_t23ABC4AE0E0BDFD2E22EE1FA0DA3904FFE5F6E23  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Color32__ctor_m1AEF46FBBBE4B522E6984D081A3D158198E10AA2((&L_1), (uint8_t)((int32_t)160), (uint8_t)((int32_t)255), (uint8_t)((int32_t)100), (uint8_t)((int32_t)100), /*hidden argument*/NULL);
		Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  L_2 = Color32_op_Implicit_mA89CAD76E78975F51DF7374A67D18A5F6EF8DA61(L_1, /*hidden argument*/NULL);
		__this->set_m_SelectedGizmoColor_5(L_2);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_mBA2AF20A35144E0C43CD721A22EAC9FCA15D6550(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_2 = (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_2, (int32_t)L_3);
		return L_4;
	}
}
