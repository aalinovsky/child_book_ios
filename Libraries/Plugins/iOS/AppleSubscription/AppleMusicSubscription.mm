#import "AppleMusicSubscription.h"

@implementation SubscriptionVC

+ (instancetype)sharedInstance
{
    static SubscriptionVC *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[SubscriptionVC alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    [self authorizeAppleMusic];
    return self;
}

- (id)initWith:(NSString *)affilateToken {
    self = [super init];
    _affilateToken = affilateToken;
    return self;
}

- (void)authorizeAppleMusic {
    switch ([SKCloudServiceController authorizationStatus]) {
        case SKCloudServiceAuthorizationStatusNotDetermined:
            NSLog(@"auth not determined");
            [self requestAppleMusicAuthorization];
            break;
        case SKCloudServiceAuthorizationStatusDenied:
            NSLog(@"auth denied");
            break;
        case SKCloudServiceAuthorizationStatusRestricted:
            NSLog(@"auth restricted");
            break;
        case SKCloudServiceAuthorizationStatusAuthorized:
            NSLog(@"authorized");
            [self appleMusicCapabilities];
            break;
    }
}
    
- (void)requestAppleMusicAuthorization {
    [SKCloudServiceController requestAuthorization:^(SKCloudServiceAuthorizationStatus status) {
        if (status == SKCloudServiceAuthorizationStatusAuthorized) {
            [self appleMusicCapabilities];
            [self showAppleMusicSignup: _affilateToken];
        }
    }];
}
    
- (void)appleMusicCapabilities {
    SKCloudServiceController *cloudServiceController = [[SKCloudServiceController alloc] init];
    [cloudServiceController
     requestCapabilitiesWithCompletionHandler:^(SKCloudServiceCapability capabilities,
                                                NSError * _Nullable error) {
         switch (capabilities) {
             case SKCloudServiceCapabilityNone:
                 NSLog(@"apple music none");
                 break;
             case SKCloudServiceCapabilityMusicCatalogPlayback:
                 NSLog(@"apple music playback");
                 break;
             case SKCloudServiceCapabilityMusicCatalogSubscriptionEligible:
                 NSLog(@"apple music subscription eligible");
                 break;
             case SKCloudServiceCapabilityAddToCloudMusicLibrary:
                 NSLog(@"apple music add to cloud music library");
                 break;
         }
     }];
}

- (NSString *)showAppleMusicSignup:(NSString *)affilateToken {
    SKCloudServiceSetupViewController *vc = [[SKCloudServiceSetupViewController alloc] init];
    vc.delegate = self;
    NSDictionary *options = @{SKCloudServiceSetupOptionsActionKey: SKCloudServiceSetupActionSubscribe,
                              SKCloudServiceSetupOptionsAffiliateTokenKey: affilateToken,
                              SKCloudServiceSetupOptionsMessageIdentifierKey: SKCloudServiceSetupMessageIdentifierPlayMusic};
    
    [vc loadWithOptions:options completionHandler:^(BOOL result, NSError * _Nullable error) {
        if (result) {
            UIViewController *rootVC = [[[UIApplication sharedApplication] keyWindow] rootViewController];
            [rootVC presentViewController: vc animated: true completion:^{
                NSLog(@"Success present Signup Screen with affilateToken = %@", affilateToken);
            }];
        } else {
            NSLog(@"RESULT ERROR");
        }
    }];
    return @"Show Apple Music Signup";
}

- (void)cloudServiceSetupViewControllerDidDismiss:(SKCloudServiceSetupViewController *)cloudServiceSetupViewController {
    NSLog(@"CloudServiceSetupViewControllerDidDismiss");
}

@end

static SubscriptionVC* subscriptionVC = nil;

extern "C" {
    void ShowSubscriptionScreen (const char* affilateToken) {
        if (subscriptionVC == nil)
            subscriptionVC = [[SubscriptionVC alloc] init];
        
        NSString *s = [[NSString alloc] initWithCString: affilateToken encoding: NSUTF8StringEncoding];
        subscriptionVC.affilateToken = s;
        [subscriptionVC showAppleMusicSignup: s];
        NSLog(@"Success ShowSubscriptionScreen with affilateToken = %@", s);
    }
}
