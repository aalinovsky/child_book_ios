#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

@interface SubscriptionVC : UIViewController<SKCloudServiceSetupViewControllerDelegate>

@property (nonatomic, copy) NSString *affilateToken;

+ (instancetype)sharedInstance;

- (id)init;
- (id)initWith:(NSString *)affilateToken;
- (NSString *)showAppleMusicSignup: (NSString *)affilateToken;
- (void)cloudServiceSetupViewControllerDidDismiss:(SKCloudServiceSetupViewController *)cloudServiceSetupViewController;
- (void)requestAppleMusicAuthorization;
- (void)authorizeAppleMusic;
- (void)appleMusicCapabilities;

@end
